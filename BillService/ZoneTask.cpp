/**
 * @file   ZoneTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:50:56 2010
 * @version $Id $
 * @brief  游戏世界连接
 * 
 */

#include "ZoneTask.h"
#include "DBClient.h"
#include "BillTask.h"
#include "BillTick.h"
#include "Freetds.h"
#include "GlobalDB.h"
#include <fstream>
#include "s2rdb_cmd.h"

MODI_ZoneTask * MODI_ZoneTask::m_pInstance = NULL;
QWORD MODI_ZoneTask::m_qdBuySerial = 0;
std::string MODI_ZoneTask::m_strConsumeFile ="";
MODI_Encrypt MODI_ZoneTask::m_stEncrypt;

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_ZoneTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif
	
	if(pt_null_cmd->byCmd == MODI_TransCommand::m_bySCmd)
	{
		switch(pt_null_cmd->byParam)
		{
			/// 刷新显示
			case MODI_SRefreshMoney::m_bySParam :
			{
				MODI_SRefreshMoney * recv_cmd = (MODI_SRefreshMoney *)pt_null_cmd;
				
				MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
				if(! p_dbclient)
				{
					Global::logger->fatal("[db_client] zonetask %s net get dbclient", recv_cmd->m_cstrAccName);
					return false;
				}

				/// 通过lgadb 获取用户的钱
				MODI_BalanceInPara in_para;
				strncpy(in_para.publisherUserId, recv_cmd->m_cstrAccName, sizeof(in_para.publisherUserId) - 1);
				in_para.m_dwAccountId = recv_cmd->m_dwAccID;
				MODI_BalanceOutPara out_para;
				MODI_Freetds::GetInstance().ReqBalance(in_para, out_para);
				DWORD money_rmb = out_para.cashBalance;
				MODI_MRefreshMoney send_cmd;
				send_cmd.m_stAccID = recv_cmd->m_dwAccID;
				
				/// 比例先定义为1:1
				send_cmd.m_dwMoney = money_rmb;
				SendCmd(&send_cmd, sizeof(send_cmd));
				
				/// 写入数据库
				MODI_Record record_update;
				DWORD en_size = 0;
				char buf[255];
				memset(buf, 0, sizeof(buf));
				PublicFun::EncryptMoney(buf, en_size, send_cmd.m_stAccID, recv_cmd->m_cstrAccName);
		
				record_update.Put("moneyRMB", send_cmd.m_dwMoney);
				record_update.Put("otherdata", buf, en_size);
				std::ostringstream where;
				where << "accname=\'" << recv_cmd->m_cstrAccName << "\'";
				record_update.Put("where", where.str());
				p_dbclient->ExecUpdate(MODI_BillTask::m_pCharTable, &record_update);
				MODI_DBManager::GetInstance().PutHandle(p_dbclient);
				return true;
			}
			break;

			/// INAItem 扣钱
			case MODI_INAItemConsume::m_bySParam :
			{
				Put(pt_null_cmd, cmd_size);
				return true;
			}
			break;

			/// INAItem 赠送扣钱 
			case MODI_INAGiveItem::m_bySParam :
			{
				Put(pt_null_cmd, cmd_size);
				return true;
			}
			break;
			
			default:
			{
				Global::logger->fatal("zonetask recv a invalid param %d", pt_null_cmd->byParam);
			}
			break;
		}
		return true;
	}
	/// 家族升级扣钱, 
	else if(pt_null_cmd->byCmd == MAINCMD_S2RDB)
	{
		Put(pt_null_cmd, cmd_size);
				return true;
	}
	else
	{
		Global::logger->fatal("zonetask recv a invalid cmd %d", pt_null_cmd->byCmd);
		return false;
	}
	
	return true;
}


bool MODI_ZoneTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif

	if(pt_null_cmd->byCmd == MODI_TransCommand::m_bySCmd)
	{
		switch(pt_null_cmd->byParam)
		{
			/// SGP/INAItem 扣钱
			case MODI_INAItemConsume::m_bySParam :
			{
				const MODI_INAItemConsume * p_recv_cmd = ( const MODI_INAItemConsume *)(pt_null_cmd);
				if(p_recv_cmd->m_wdSize == 0)
				{
					MODI_ASSERT(0);
					return false;
				}
				
				const MODI_INAItemPrice * p_startaddr = p_recv_cmd->pData;
				
				for(WORD i=0; i<p_recv_cmd->m_wdSize; i++)
				{
					/// 直接询问对方，阻塞问
					MODI_OrderInPara in_para;
					strncpy(in_para.publisherUserId, p_recv_cmd->m_cstrAccName, sizeof(in_para.publisherUserId) - 1);
					in_para.m_dwAccountId = p_recv_cmd->m_dwAccountId;
					strncpy(in_para.productCode, p_startaddr->m_cstrItemId, sizeof(in_para.productCode) - 1);
					in_para.m_dwPrice = p_startaddr->m_dwPrice;
					strncpy(in_para.gameTransactionDate, MODI_BillTick::m_stTTime.GetStrTime(), sizeof(in_para.gameTransactionDate));
					
					std::ostringstream os;
					os<< p_recv_cmd->m_qdBuySerial;
					strncpy(in_para.gameTransactionId, os.str().c_str(), sizeof(in_para.gameTransactionId) - 1);
					
					MODI_OrderOutPara out_para;
					MODI_Freetds::GetInstance().ReqOrder(in_para, out_para);
					Global::logger->debug("[inaitem_result] <serial=%llu,accid=%u,result=%d,transactionId=%u>", p_recv_cmd->m_qdBuySerial,
										  p_recv_cmd->m_dwAccountId, out_para.returnId, out_para.transactionId);

					MODI_INAItemReturn send_cmd;
					send_cmd.m_byServerId = p_recv_cmd->m_byServerId;
					send_cmd.m_dwAccountId = p_recv_cmd->m_dwAccountId;
					send_cmd.m_qdBuySerial = p_recv_cmd->m_qdBuySerial;
					send_cmd.m_iResult = out_para.returnId;
					
					/// 充值成功
					if(out_para.returnId == 100)
					{
						send_cmd.pData = *p_startaddr;
						send_cmd.m_qdTransactionId = out_para.transactionId;
						
						/// 记录下来数据库和加密日志
						BuyRecord(in_para, out_para, p_startaddr->m_dwPrice);
					}

					SendCmd(&send_cmd, sizeof(send_cmd));
					p_startaddr++;
				}
			}
			break;

			/// 赠送扣钱
			case MODI_INAGiveItem::m_bySParam :
			{
				const MODI_INAGiveItem * p_recv_cmd = ( const MODI_INAGiveItem *)(pt_null_cmd);
				if(p_recv_cmd->m_wdSize == 0)
				{
					MODI_ASSERT(0);
					return false;
				}
				
				const MODI_INAItemPrice * p_startaddr = &(p_recv_cmd->pData);

				/// 赠送只有一种道具
				for(WORD i=0; i<1; i++)
				{
					/// 直接询问对方，阻塞问
					MODI_OrderInPara in_para;
					strncpy(in_para.publisherUserId, p_recv_cmd->m_cstrAccName, sizeof(in_para.publisherUserId) - 1);
					in_para.m_dwAccountId = p_recv_cmd->m_dwAccountId;
					/// INA的赠送要被赠送人的姓名
					strncpy(in_para.toPublisherUserId, p_recv_cmd->m_cstrGiftAccName, sizeof(in_para.toPublisherUserId) - 1 );
					in_para.orderTypeId = 2;
					
					strncpy(in_para.productCode, p_startaddr->m_cstrItemId, sizeof(in_para.productCode) - 1);
					in_para.m_dwPrice = p_startaddr->m_dwPrice;
					strncpy(in_para.gameTransactionDate, MODI_BillTick::m_stTTime.GetStrTime(), sizeof(in_para.gameTransactionDate));
					
					std::ostringstream os;
					os<< p_recv_cmd->m_qdBuySerial;
					strncpy(in_para.gameTransactionId, os.str().c_str(), sizeof(in_para.gameTransactionId) - 1);

					/// 返回
					char buf[Skt::MAX_USERDATASIZE];
					memset(buf, 0, sizeof(buf));

					MODI_INAGiveReturn * p_send_cmd = (MODI_INAGiveReturn *)buf;
					AutoConstruct(p_send_cmd);
					
					p_send_cmd->m_byServerId = p_recv_cmd->m_byServerId;
					p_send_cmd->m_dwAccountId = p_recv_cmd->m_dwAccountId;
					p_send_cmd->m_qdBuySerial = p_recv_cmd->m_qdBuySerial;

					
					MODI_OrderOutPara out_para;
					MODI_Freetds::GetInstance().ReqOrder(in_para, out_para);
					Global::logger->debug("[inaitem_result] <serial=%llu,accid=%u,result=%d,transactionId=%u>", p_recv_cmd->m_qdBuySerial,
										  p_recv_cmd->m_dwAccountId, out_para.returnId, out_para.transactionId);

					p_send_cmd->m_iResult = out_para.returnId;
					
					/// 充值成功
					if(out_para.returnId == 100)
					{
						p_send_cmd->m_qdTransactionId = out_para.transactionId;
						p_send_cmd->m_wdSize = cmd_size;
						memcpy(p_send_cmd->m_pGiveData, p_recv_cmd->m_pGiveData, p_recv_cmd->m_wdSize);
						
						/// 记录下来数据库和加密日志
						BuyRecord(in_para, out_para, p_startaddr->m_dwPrice);
						
					}

					SendCmd(p_send_cmd, sizeof(MODI_INAGiveReturn) + p_send_cmd->m_wdSize);
				}
			}
			break;

			default:
			{
				Global::logger->fatal("zonetask recv a invalid param %d", pt_null_cmd->byParam);
			}
			break;
		}
	}
	/// 家族升级扣钱, 
	else if(pt_null_cmd->byCmd == MAINCMD_S2RDB)
	{
		if(pt_null_cmd->byParam == MODI_S2RDB_FamilyLevel_Cmd::ms_SubCmd)
		{
			const MODI_S2RDB_FamilyLevel_Cmd * p_recv_cmd = (const MODI_S2RDB_FamilyLevel_Cmd *)pt_null_cmd;
			///INA模拟购买,新加坡直接扣钱
			if(Global::g_byGameShop > 0)
			{
				MODI_OrderInPara in_para;
				strncpy(in_para.publisherUserId, p_recv_cmd->m_cstrAccName, sizeof(in_para.publisherUserId) - 1);
				in_para.m_dwAccountId = p_recv_cmd->m_dwAccountID;
				std::ostringstream nn;
				nn<< "100000" << (WORD)(p_recv_cmd->m_byLevel + 1)<< "_3";
				std::string family_item_name = nn.str();
				strncpy(in_para.productCode, family_item_name.c_str(), sizeof(in_para.productCode) - 1);
				//in_para.m_dwPrice = nsFamilyLevel::LEVEL_MONEY;
				in_para.m_dwPrice = 1000; /// SGP说改成1000了
				strncpy(in_para.gameTransactionDate, MODI_BillTick::m_stTTime.GetStrTime(), sizeof(in_para.gameTransactionDate));
				strncpy(in_para.gameTransactionId, "100000000", sizeof(in_para.gameTransactionId) - 1);
					
				MODI_OrderOutPara out_para;
				MODI_Freetds::GetInstance().ReqOrder(in_para, out_para);
				Global::logger->debug("[inaitem_result] family level up <accid=%u,result=%d,transactionId=%u>",
									  p_recv_cmd->m_dwAccountID, out_para.returnId, out_para.transactionId);

				DWORD get_family_upgrade_price = 0;
				if(Global::g_byGameShop == 1)
				{
					MODI_PriceInPara in_para;
                    MODI_PriceOutPara out_para;
                    strncpy(in_para.productCode, nn.str().c_str(), sizeof(in_para.productCode));
					MODI_Freetds::GetInstance().ReqPrice(in_para, out_para);
					get_family_upgrade_price = out_para.cyberCashAmount;
					Global::logger->debug("[family_upgrade] get price <accid=%u,item=%s,price=%u>", p_recv_cmd->m_dwAccountID,
										  nn.str().c_str(), get_family_upgrade_price);
				}
				
				/// 扣钱成功
				if(out_para.returnId == 100)
				{
					MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
					MODI_ScopeHandlerRelease lock(p_dbclient);
					p_dbclient->BeginTransaction();
					bool ret_code = true;
					DWORD m_dwLastMoney = 0;
					DWORD money_rmb = 0;
					std::string acc_name;
					std::ostringstream where;
					WORD level = 0;
					do
					{
						/// first 扣钱
						MODI_RecordContainer record_select_char;
						where.str("");
						where << "accountid=" << p_recv_cmd->m_dwAccountID;
						MODI_Record select_field_char;
						select_field_char.Put("moneyRMB");
						select_field_char.Put("accname");

						if((p_dbclient->ExecSelect(record_select_char, GlobalDB::m_pCharacterTbl, where.str().c_str(), &select_field_char)) != 1)
						{
							Global::logger->fatal("[family_level] read money form character faield <accid=%u>",p_recv_cmd->m_dwAccountID);
							MODI_ASSERT(0);
							ret_code = false;
							break;
						}

						MODI_Record * p_select_record_char = record_select_char.GetRecord(0);
						if(p_select_record_char == NULL)
						{
							ret_code = false;
							break;
						}
						
						const MODI_VarType v_moneyRMB = p_select_record_char->GetValue("moneyRMB");
						if(v_moneyRMB.Empty())
						{
							ret_code = false;
							break;
						}

						const MODI_VarType v_accname = p_select_record_char->GetValue("accname");
						if(v_accname.Empty())
						{
							ret_code = false;
							break;
						}
						
						money_rmb = (DWORD)v_moneyRMB;
						acc_name = (const char *)v_accname;

						if(Global::g_byGameShop == 2)
						{
							if(1000 > money_rmb)
							{
								Global::logger->fatal("[assert_family_level] dec moneyrmb but moneyrmb not enought <havemoney=%u,accid=%u>",
												  money_rmb, p_recv_cmd->m_dwAccountID);
								ret_code = false;
								break;
							}
							else
							{
								m_dwLastMoney = money_rmb - 1000;
							}
						}
						else
						{
							if(get_family_upgrade_price > money_rmb)
							{
								Global::logger->fatal("[assert_family_level] dec moneyrmb but moneyrmb not enought <havemoney=%u,needmoney=%u,accid=%u>",
													  money_rmb, get_family_upgrade_price, p_recv_cmd->m_dwAccountID);
								ret_code = false;
								break;
							}
							else
							{
								m_dwLastMoney = money_rmb - get_family_upgrade_price;
							}
						}
						
						Global::logger->debug("[family_level] dec money rmb <name=%s,havemone=%u,lastmone=%u>",
												  acc_name.c_str(), money_rmb, m_dwLastMoney);
		
						MODI_Record record_update_money;
						DWORD en_size = 0;
						char buf[255];
						memset(buf, 0, sizeof(buf));
						PublicFun::EncryptMoney(buf, en_size, m_dwLastMoney, acc_name.c_str());
						
						record_update_money.Put("moneyRMB", m_dwLastMoney);
						record_update_money.Put("otherdata", buf, en_size);
						record_update_money.Put("where", where.str());
						
						if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update_money) != 1)
						{
							Global::logger->fatal("[assert_family_level] updata money rmb failed<accname=%s,lastmoney=%u,havemoney=%u>",
													  acc_name.c_str(), m_dwLastMoney, money_rmb);
							MODI_ASSERT(0);
							ret_code = false;
							break;
						}
						
						/// second 升级
						MODI_RecordContainer record_select_level;
						where.str("");
						//std::ostringstream where;
						where << "familyid=" << p_recv_cmd->m_dwFamilyID;
						MODI_Record select_field_level;
						select_field_level.Put("level");
	
						if((p_dbclient->ExecSelect(record_select_level, GlobalDB::m_pFamilyTbl, where.str().c_str(), &select_field_level)) != 1)
						{
							Global::logger->fatal("[family_level] read level from familys faield <familyid=%u>",p_recv_cmd->m_dwFamilyID);
							MODI_ASSERT(0);
							ret_code = false;
							break;
						}

						MODI_Record * p_select_record_level = record_select_level.GetRecord(0);
						if(p_select_record_level == NULL)
						{
							ret_code = false;
							break;
						}
						
						const MODI_VarType v_level = p_select_record_level->GetValue("level");
						if(v_level.Empty())
						{
							ret_code = false;
							break;
						}

						level = (WORD)v_level;
						if(level >= nsFamilyLevel::MAX_FAMILY_LEVEL)
						{
							Global::logger->fatal("[assert_family_level] inc level but level amx <level=%u,familyid=%u>",
												  level, p_recv_cmd->m_dwFamilyID);
							ret_code = false;
							break;
						}
						
						MODI_Record record_update_level;
						
						record_update_level.Put("level", level+1);
						record_update_level.Put("where", where.str());
						where.str("");
						where << "familyid=" << p_recv_cmd->m_dwFamilyID;
						if(p_dbclient->ExecUpdate(GlobalDB::m_pFamilyTbl, &record_update_level) != 1)
						{
							Global::logger->fatal("[assert_family_level] updata family level failed <familyid=%u,level=%u>", p_recv_cmd->m_dwFamilyID, level);
							MODI_ASSERT(0);
							ret_code = false;
							break;
						}
					}
					while(0);

					/// 告知结果
					MODI_RDB2S_Notify_FamilyLevel send_result;
					send_result.m_byResult = ret_code;
					send_result.m_dwFamilyID = p_recv_cmd->m_dwFamilyID;
					send_result.m_dwAccountID = p_recv_cmd->m_dwAccountID;
					SendCmd(&send_result, sizeof(send_result));

					if(ret_code)
					{
						p_dbclient->CommitTransaction();
						Global::logger->debug("[family_level_success] <familyid=%u,accid=%u,level=%u>",
											  p_recv_cmd->m_dwFamilyID, p_recv_cmd->m_dwAccountID, level+1);

						/// 刷新钱
						MODI_MRefreshMoney send_cmd;
						send_cmd.m_stAccID = p_recv_cmd->m_dwAccountID;
						send_cmd.m_dwMoney = m_dwLastMoney;
						SendCmd(&send_cmd, sizeof(send_cmd));
						return true;
					}
					else
					{
						p_dbclient->RollbackTransaction();
						Global::logger->debug("[family_level_failed] failed <familyid=%u,accid=%u,result=%d,transactionId=%u,level=%u>",
											  p_recv_cmd->m_dwFamilyID,
											  p_recv_cmd->m_dwAccountID, out_para.returnId, out_para.transactionId,
											  level+1);
						return false;
					}
				}
				/// 没有扣钱成功
				else
				{
					/// 告知结果
					MODI_RDB2S_Notify_FamilyLevel send_result;
					send_result.m_byResult = 0;
					send_result.m_dwFamilyID = p_recv_cmd->m_dwFamilyID;
					send_result.m_dwAccountID = p_recv_cmd->m_dwAccountID;
					SendCmd(&send_result, sizeof(send_result));
				}
			}
		}
	}
	return true;
}


/** 
 * @breif INA扣钱记录
 *
 * 
 */
void MODI_ZoneTask::BuyRecord(const MODI_OrderInPara & in_para, const MODI_OrderOutPara & out_para, const DWORD & money)
{
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	if(! p_db_client)
	{
		Global::logger->fatal("[ina_consume] <buy_serial=%llu,ina_serail=%ull,accname=%s,ina_item=%s,money=%u>",
								in_para.gameTransactionId, out_para.transactionId, in_para.publisherUserId, in_para.productCode, money);
		return;
	}
	
	MODI_ScopeHandlerRelease lock(p_db_client);
	
	MODI_Record insert_consume;
	insert_consume.Put("buy_serial", in_para.gameTransactionId);
	insert_consume.Put("consume_time", in_para.gameTransactionDate);
	insert_consume.Put("ina_serial", out_para.transactionId);
	insert_consume.Put("accname", in_para.publisherUserId);
	insert_consume.Put("ina_item", in_para.productCode);
	insert_consume.Put("money", money);

	MODI_RecordContainer insert_container;
	insert_container.Put(&insert_consume);

	if(p_db_client->ExecInsert(GlobalDB::m_pINAConsume, &insert_container) != 1)
	{
		Global::logger->fatal("[ina_consume] <buy_serial=%s,ina_serail=%u,accname=%s,ina_item=%s,money=%u>",
								in_para.gameTransactionId, out_para.transactionId, in_para.publisherUserId, in_para.productCode, money);
	}
	
	/// 加密日志, 每小时一个文件
	WORD year  = MODI_BillTick::m_stTTime.GetYear();
	WORD mon = MODI_BillTick::m_stTTime.GetMon();
	WORD day = MODI_BillTick::m_stTTime.GetMDay();
	WORD hour = MODI_BillTick::m_stTTime.GetHour();
	static std::ostringstream file_name;
	file_name.str("");
	if(year != m_wdOldYear || mon != m_wdOldMonth ||
	   day != m_wdOldDay || hour != m_wdOldHour)
	{
		m_wdOldYear = year;
		m_wdOldMonth = mon;
		m_wdOldDay = day;
		m_wdOldHour = hour;
	}

	file_name << m_strConsumeFile << "/"<< m_wdOldYear;
	if(m_wdOldMonth < 10)
	{
		file_name << "0" << m_wdOldMonth;
	}
	else
	{
		file_name << m_wdOldMonth;
	}

	if(m_wdOldDay < 10)
	{
		file_name << "0" << m_wdOldDay;
	}
	else
	{
		file_name << m_wdOldDay;
	}

	if(m_wdOldHour < 10)
	{
		file_name << "0" << m_wdOldHour;
	}
	else
	{
		file_name << m_wdOldHour;
	}
	
	static std::ofstream ofile;
	ofile.open(file_name.str().c_str(), std::ios_base::app| std::ios_base::binary);
	
	if(ofile.is_open())
	{
		char out[1024];
		memset(out, 0, sizeof(out));
		sprintf(out, "[%llu]<buy_serial=%s,ina_serail=%u,accname=%s,ina_item=%s,money=%u,datetime=%s>",
				m_qdBuySerial, 
				in_para.gameTransactionId,
				out_para.transactionId,
				in_para.publisherUserId,
				in_para.productCode,
				money, in_para.gameTransactionDate);

		DWORD enc_size = strlen(out);
		m_stEncrypt.Encode((unsigned char *)out, enc_size);
		ofile << enc_size;
		ofile.write(out, enc_size);
		ofile.close();
		m_qdBuySerial++;
	}
	else
	{
		MODI_ASSERT(0);
		Global::logger->fatal("[ina_item] not open item log file");
	}
	
}

/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_ZoneTask::RecycleConn()
{
	return 1;
}
