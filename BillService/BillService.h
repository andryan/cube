/**
 * @file   BillService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:43:13 2010
 * @version $Id $
 * @brief  充值服务器类
 * 
 */


#ifndef _MD_BILLSERVICE_H
#define _MD_BILLSERVICE_H

#include "ServiceTaskSched.h"
#include "MNetService.h"

const int MULTI_PORT_NUM = 2;

/// 端口索引下标
enum enPortIndex
{
	enBillPort,
	enZonePort,
	enMaxPort
};

/**
 * @brief 充值服务器类，多监听
 * 
 */
class MODI_BillService: public MODI_MNetService
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param vec 要绑定的端口
	 * @param name 服务器
	 * @param count 绑定的端口个数
	 * 
	 */
	MODI_BillService(std::vector<WORD> & vec, const std::vector<std::string> & vecip , 
			const char * name = "billservice", const int count = MULTI_PORT_NUM):MODI_MNetService(vec, vecip, name, count),
		m_stBillTaskSched(6,1), m_stZoneTaskSched(1,1)
	{
		m_pInstance = this;
		m_wdBillPort = 0;
		m_wdZonePort = 0;
		
		if(((int)vec.size() == count) && (count == MULTI_PORT_NUM))
		{
			m_wdBillPort = vec[enBillPort];
			m_wdZonePort = vec[enZonePort];
		}
	}

	~MODI_BillService()
	{
		Final();
	}

	/** 
	 * @brief 创建新任务
	 * 
	 * @param sock 任务相关的task
	 * @param addr 任务相关的地址
	 * @param port 任务相关的端口
	 * 
	 * @return 成功true 失败false
	 */		
	bool CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port);

	/** 
	 * @brief 初始化
	 *
	 * @return 成功true 失败false
	 */
	bool Init();

	static MODI_BillService & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_BillService(m_stPortVec, m_stIPVec);
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	/// 服务器连接的地址
	static std::vector<WORD> m_stPortVec;
	static std::vector<std::string> m_stIPVec;
	
 private:
	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();
	
	/// 充值客户端
	WORD m_wdBillPort;
	
	/// 游戏区端口
	WORD m_wdZonePort;

	/// 任务处理线程
	MODI_ServiceTaskSched m_stBillTaskSched;

	/// 游戏区的处理
	MODI_ServiceTaskSched m_stZoneTaskSched;

	/// 实体
	static MODI_BillService * m_pInstance;
};

#endif

