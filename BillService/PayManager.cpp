/**
 * @file   PayManager.cpp
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Wed Oct 13 14:08:47 2010
 * @version $Id:$
 * @brief  支付管理
 * 
 * 
 */

#include "DBStruct.h"
#include "DBClient.h"
#include "PayManager.h"
#include "BillTask.h"

MODI_PayManager * MODI_PayManager::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_PayManager::Init()
{
	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("platformserial");
	select_field.Put("keeptime");
	
	MODI_RTime now_time;
	QWORD now_msec = now_time.GetMSec();
	if(now_msec < (PAY_KEEP_TIME))
	{
		Global::logger->fatal("pay manager time error");
		return false;
	}
	
	std::ostringstream where;
	where << "keeptime>\'" << now_msec << "\'";
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->debug("[pay_init] paymanager not get dbclient");
		return false;
	}
	
	int rec_count = p_dbclient->ExecSelect(record_select, MODI_BillTask::m_pPayTable, where.str().c_str(), &select_field, true);
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("-->>>>>get %d record", rec_count);
#endif

	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		MODI_VarType v_platformserial = p_select_record->GetValue("platformserial");
		MODI_VarType v_keeptime = p_select_record->GetValue("keeptime");
		if(v_platformserial.Empty() || v_keeptime.Empty())
		{
			continue;
		}
		std::string platform_serial = (const char *)v_platformserial;
		QWORD keep_time = (QWORD)v_keeptime;
		MODI_PayData pay_data;
		pay_data.m_stRTime = keep_time;
		AddPayData(platform_serial.c_str(), pay_data);
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("add a pay data(<orderid=%s>,keep_time=%llu)", platform_serial.c_str(), keep_time);
#endif			
	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	if(! Start())
	{
		return false;
	}
	
	return true;
}



/**
 * @biref 更新
 * 
 */
void MODI_PayManager::Run()
{
	MODI_RTime m_stCurrentTime;
	while(! IsTTerminate())
	{
		::usleep(10000);
		m_stCurrentTime.GetNow();
		m_stLock.rdlock();
		if(m_PayDataMap.size() != 0)
		{
			defPayDataMapIter iter,next;
			for(next=iter=m_PayDataMap.begin(); iter != m_PayDataMap.end(); iter=next)
			{
				if(next != m_PayDataMap.end())
					next++;
				if(m_stCurrentTime > (iter->second.m_stRTime))
				{
					Global::logger->debug("%s time out", (iter->first).c_str());
					m_PayDataMap.erase(iter);
				}
			}
		}
		m_stLock.unlock();
	}
}


/** 
 * @brief 增加一个支付数据
 * 
 * @param pay_serial 支付序号
 * @param pay_data 支付数据
 * 
 * @return 成功ture,失败false
 */
bool MODI_PayManager::AddPayData(const char * pay_serial, MODI_PayData pay_data)
{
	std::pair<defPayDataMapIter, bool> ret_code;
	m_stLock.wrlock();
	ret_code = m_PayDataMap.insert(defPayDataMapValue(pay_serial, pay_data));
	m_stLock.unlock();
	return ret_code.second;
}


/** 
 * @brief 删除一个支付数据
 * 
 * @param pay_serial 支付数据
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_PayManager::RemovePayData(const char * pay_serial)
{
	m_stLock.wrlock();
	defPayDataMapIter iter = m_PayDataMap.find(pay_serial);
	if(iter == m_PayDataMap.end())
	{
		m_stLock.unlock();
		return false;
	}
	m_PayDataMap.erase(iter);
	m_stLock.unlock();
	return true;
}


/** 
 * @brief 是不是支付数据
 * 
 * @param pay_serial 支付数据
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_PayManager::IsPayData(const char * pay_serial)
{
	m_stLock.rdlock();
	defPayDataMapIter iter = m_PayDataMap.find(pay_serial);
	if(iter == m_PayDataMap.end())
	{
		m_stLock.unlock();
		return false;
	}
	m_stLock.unlock();
	return true;
}
