/**
 * @file   BillTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:54:10 2010
 * @version $Id $
 * @brief  充值连接
 * 
 */

#include <iostream>
#include "md5.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "FunctionTime.h"
#include "BillTask.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "PayManager.h"
#include "ZoneTask.h"
#include "base64.h"

MODI_TableStruct * MODI_BillTask::m_pPayTable = NULL;
MODI_TableStruct * MODI_BillTask::m_pCharTable = NULL;


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_BillTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	return true;
}


/** 
 * @brief 发送命令
 * 
 * @param send_cmd 要发送的命令 
 * @param cmd_size 发送命令的大小不能超过64K
 * 
 * @return 成功true 失败false
 */
bool MODI_BillTask::SendCmd(const char * send_cmd, const unsigned int cmd_size)
{
	if((! m_pSocket) || (cmd_size > RET_BUF_LENGTH))
	{
		Global::net_logger->fatal("[sock_data] send cmd to large");
		return false;
	}
	m_pSocket->SendCmdNoPacket(send_cmd, cmd_size);
	return true;
}


/**
 * @brief 接收到数据处理
 *
 * @return  继续等待true 成功关闭连接false
 *
 */
bool MODI_BillTask::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > 1000)
		{
			Global::net_logger->fatal("%s:%d recv command more than 1000", GetIP(), GetPort());
			ResetRecvBuf();
			return false;
		}

		const std::string m_strRecvContent = m_stRecvBuf;
		std::string::size_type method_pos = m_strRecvContent.find_first_of(' ', 0);
		std::string method;
		if(method_pos == std::string::npos)
		{
			if(m_strRecvContent.size() > 5)
			{
				Global::logger->fatal("%s:%d get method error", GetIP(), GetPort());
				ResetRecvBuf();
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			method = m_strRecvContent.substr(0, method_pos);
		}
		Global::logger->debug("getcommand=%s",m_strRecvContent.c_str());

		if(method == "GET")
		{
			ResetRecvBuf();
			return false;
		}
		
		else if(method == "POST")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n\r\n", 0, 4);
			if(end_pos == std::string::npos)
			{
				Global::logger->debug("not get content");
			}
			std::string content = m_strRecvContent.substr(end_pos + 4, (m_strRecvContent.size() - (end_pos+4)));
			Global::logger->debug("-->>>>content=%s", content.c_str());
			if(! ParsePay(content.c_str()))
			{
				Global::logger->fatal("pay failed");
			}
			ResetRecvBuf();
			return false;
		}
		else
		{
			Global::logger->fatal("method failed");
		}
		return false;
	}
	return true;
}


/** 
 * @brief 冲值处理
 * 
 * @param cmd_line 要处理的命令
 */
bool MODI_BillTask::ParsePay(const char * cmd_line)
{
	if( ! GetPayContent(cmd_line))
	{
		RetPayResult(-3);
		return false;
	}

	if(! CheckMD5())
	{
		RetPayResult(-1);
 		return false;
	}

	if(! DealPay())
	{
		return false;
	}

	return true;
	
}


/** 
 * @brief 分析冲值内容
 * 
 * @param pay_content 冲值内容
 */
bool MODI_BillTask::GetPayContent(const char * pay_content)
{
	MODI_SplitString deal_result;
	deal_result.Init(pay_content, "&");
	//http://192.168.2.1:38000/sync_bill.jsp?order_id=123456&player=guest11&currency=100&game_currency=200&game_money=300&ratio=1:100&order_time_u=123456789&sn=abcdefgh&action=1
	m_strOrderid = deal_result["order_id"];
	m_strEncodePlayer = deal_result["player"];
	m_strCurrency = deal_result["currency"];
	m_strGameCurrency = deal_result["game_currency"];
	m_strGameMoney = deal_result["game_money"];
	m_strRatio = deal_result["ratio"];
	std::string m_strAction = deal_result["action"];
	m_strTime = deal_result["order_time_u"];
	m_strSN = deal_result["sn"];

	if(m_strOrderid == "" || m_strOrderid.size() > 30 ||
	   m_strEncodePlayer == "" ||
	   m_strCurrency == "" || m_strGameMoney == "" || m_strGameCurrency == ""
	   || m_strSN == "" || m_strSN.size() > 32 || m_strTime == "" || m_strAction == "" || m_strAction.size() > 2) 
	   {
		   Global::logger->fatal("[pay_content] get pay content failed<orderid=%s,m_strEncodePlayer=%s,m_strCurrency=%s,m_strGameCurrency=%s,m_strGameMoney=%s,\
m_strSN=%s,m_strTime=%s,m_strAction=%s>", m_strOrderid.c_str(),m_strEncodePlayer.c_str(),m_strCurrency.c_str(),m_strGameCurrency.c_str(),m_strGameMoney.c_str(),
								 m_strSN.c_str(), m_strTime.c_str(), m_strAction.c_str());
		   return false;
	   }

	m_strURLDecodePlayer = PublicFun::StrDecode(m_strEncodePlayer.c_str());
	m_byAction = atoi(m_strAction.c_str());
	if(m_byAction == 0 || m_byAction > 3)
	{
		Global::logger->fatal("[pay_content] get pay contend failed <action=%d>", m_byAction);
		return false;
	}
	
	unsigned char buf[m_strEncodePlayer.size() + 1];
	memset(buf, 0, sizeof(buf));
	base64_decode(buf, m_strURLDecodePlayer.c_str());
	m_strDecodePlayer = (const char *)buf;

#ifdef _HRX_DEBUG	
	Global::logger->debug("order_id=%s,player=%s,curr=%s,game_curr=%s,game_money=%s,ratio=%s,time=%s,sn=%s, decode_player=%s,action=%d",
						  m_strOrderid.c_str(), m_strEncodePlayer.c_str(),
						  m_strCurrency.c_str(), m_strGameCurrency.c_str(),
						  m_strGameMoney.c_str(),
						  m_strRatio.c_str(), m_strTime.c_str(), m_strSN.c_str(), m_strDecodePlayer.c_str(), m_byAction);
#endif

	m_dwMoney = atoi(m_strGameCurrency.c_str()) + atoi(m_strGameMoney.c_str());
	
// 	MODI_TTime m_stTTime;
// 	WORD year = m_stTTime.GetYear();
// 	WORD mon = m_stTTime.GetMon();
// 	WORD day = m_stTTime.GetMDay();
// 	if((year == 2011 && mon == 12 && day>22) || (year == 2012 && mon == 1 && day<5))
// 	{
// 		m_dwGiftMoney = (m_dwMoney / 5000) * 500;
// 		Global::logger->debug("[gift_money] <order_id=%s,money=%u,giftmoney=%u>", m_strOrderid.c_str(), m_dwMoney, m_dwGiftMoney);
// 	}
	
	return true;
}


/** 
 * @brief 签名校验
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BillTask::CheckMD5()
{
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;

	std::string md5_player = PublicFun::StrDecode(m_strEncodePlayer.c_str());
	/// sn=md5(base64_encode(player)+order_id+KEY+currency+ game_currency+game_money+ratio+order_time_u)
	str_md5<<m_strURLDecodePlayer<<m_strOrderid<<"123456"<<m_strCurrency<<m_strGameCurrency<<m_strGameMoney<<m_strRatio<<m_strTime;
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("md5_src_str=%s", str_md5.str().c_str());
#endif
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	
	if(recv_check_str != m_strSN)
	{
		Global::logger->fatal("[check_md5] %s:%d md5 check failed(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
		return false;
	}
	Global::logger->fatal("[check_md5] %s:%d md5 check successful(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
	return true;
}


/** 
 * @brief 处理冲值
 * 
 * 
 * @return 成功true,失败false
 */
bool MODI_BillTask::DealPay()
{
	if(MODI_PayManager::GetInstance().IsPayData(m_strOrderid.c_str()))
	{
#ifdef _HRX_DEBUG
		Global::logger->debug("%s have pay ", m_strOrderid.c_str());
#endif		
		RetPayResult(-4);
		return false;
	}
	
	MODI_DBClient * p_game_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_game_dbclient)
	{
		Global::logger->debug("%s net get gamedb dbclient", m_strOrderid.c_str());
		RetPayResult(-5);
		return false;
	}

	MODI_MRefreshMoney send_cmd;
	/// 是否在充值活动时间内
	MODI_TTime m_stTTime;
	WORD year = m_stTTime.GetYear();
	WORD mon = m_stTTime.GetMon();
	WORD day = m_stTTime.GetMDay();
	
	if((year == 2012 && mon == 1 && day>15) && (year == 2012 && mon == 1 && day<23))
	{
		/// 是否是第一充值
		/*		std::ostringstream os;
		os << "accname=\'" << m_strDecodePlayer << "\'";
		unsigned int get_count = p_game_dbclient->GetCount(m_pPayTable, os.str().c_str());
		Global::logger->debug("get_count= <%u><%s>", get_count,os.str().c_str());
		if(get_count ==  0)
		{
			if(m_dwMoney >= 1000)
			{
				send_cmd.m_enReason = enMRefreshMoney_FirstPay;
			}
		}
		else */
		
		{
			std::ostringstream os20;
			std::ostringstream os30;
			std::ostringstream os60;
			std::ostringstream os100;
			std::ostringstream os200;
			std::ostringstream os500;
			
			DWORD begin_time = 1328842800; /// 2012-02-10 11:00:00
			//DWORD begin_time = 1328670000; /// 2012-02-8 11:00:00
			DWORD end_time = 1329192000; /// 2012-02-10 12:00:00
			
			os20<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=2000 and money<3000";

			os30<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=3000 and money<6000";

			os60<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=6000 and money<10000";

			os100<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=10000 and money<20000";

			os200<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=20000 and money<50000";

			os500<< "accname=\'" << m_strDecodePlayer << "\'"
			  <<" and paytime>" <<begin_time
			  << " and paytime<" <<end_time
			  <<" and money>=50000";
			
			unsigned int get_count_20 = p_game_dbclient->GetCount(m_pPayTable, os20.str().c_str());
			unsigned int get_count_30 = p_game_dbclient->GetCount(m_pPayTable, os30.str().c_str());
			unsigned int get_count_60 = p_game_dbclient->GetCount(m_pPayTable, os60.str().c_str());
			unsigned int get_count_100 = p_game_dbclient->GetCount(m_pPayTable, os100.str().c_str());
			unsigned int get_count_200 = p_game_dbclient->GetCount(m_pPayTable, os200.str().c_str());
			unsigned int get_count_500 = p_game_dbclient->GetCount(m_pPayTable, os500.str().c_str());
			
			Global::logger->debug("get_count_20=%u", get_count_20);
			Global::logger->debug("get_count_30=%u", get_count_30);
			Global::logger->debug("get_count_60=%u", get_count_60);
			Global::logger->debug("get_count_100=%u", get_count_100);
			Global::logger->debug("get_count_200=%u", get_count_200);
			Global::logger->debug("get_count_500=%u", get_count_500);
			
			if(get_count_20 == 0 &&  (m_dwMoney >= 2000 && m_dwMoney < 3000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay20;
			}
			else if(get_count_30 == 0 &&  (m_dwMoney >= 3000 && m_dwMoney < 6000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay30;
			}
			else if(get_count_60 == 0 &&  (m_dwMoney >= 6000 && m_dwMoney < 10000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay60;
			}
			else if(get_count_100 == 0 &&  (m_dwMoney >= 10000 && m_dwMoney < 20000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay100;
			}
			else if(get_count_200 == 0 &&  (m_dwMoney >= 20000 && m_dwMoney < 50000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay200;
			}
			else if(get_count_500 == 0 &&  (m_dwMoney >= 50000))
			{
				send_cmd.m_enReason = enMRefreshMoney_Pay500;
			}
		}
	}
			
	bool ret_code = true;
	p_game_dbclient->BeginTransaction();
	do
	{
		/// first
		MODI_PayData pay_data;
		ret_code &= MODI_PayManager::GetInstance().AddPayData(m_strOrderid.c_str(), pay_data);
		if(! ret_code)
		{			
			break;
		}

		/// second
		MODI_RecordContainer record_select;
		std::ostringstream where;
		where << "accname=\'" << m_strDecodePlayer << "\'";
		
		MODI_Record select_field;
		select_field.Put("moneyRMB");
		select_field.Put("accountid");
		select_field.Put("accname");

		if((p_game_dbclient->ExecSelect(record_select, m_pCharTable, where.str().c_str(), &select_field)) != 1)
		{
			Global::logger->fatal("[pay_db] read money form character faield <orderid=%s,player=%s>", m_strOrderid.c_str(), m_strDecodePlayer.c_str());
			ret_code = false;
			break;
		}

		MODI_Record * p_select_record = record_select.GetRecord(0);
		if(p_select_record == NULL)
		{
			ret_code = false;
			break;
		}
		
		MODI_VarType v_moneyRMB = p_select_record->GetValue("moneyRMB");
		if(v_moneyRMB.Empty())
		{
			ret_code = false;
			break;
		}
		
		DWORD money_rmb = (DWORD)v_moneyRMB;

		MODI_VarType v_accountid = p_select_record->GetValue("accountid");
		if(v_accountid.Empty())
		{
			ret_code = false;
			break;
		}
		
		m_dwAccountID = (defAccountID)v_accountid;


		MODI_VarType v_accname = p_select_record->GetValue("accname");
		if(v_accname.Empty())
		{
			ret_code = false;
			break;
		}

		m_strDecodePlayer = (const char *)v_accname;
		
		MODI_Record record_update;
		//m_dwLastMoney = m_dwMoney + money_rmb + m_dwGiftMoney;
		m_dwLastMoney = m_dwMoney + money_rmb;
		
		DWORD en_size = 0;
		char buf[255];
		memset(buf, 0, sizeof(buf));
		PublicFun::EncryptMoney(buf, en_size, m_dwLastMoney, m_strDecodePlayer.c_str());
		
		record_update.Put("moneyRMB", m_dwLastMoney);
		record_update.Put("otherdata", buf, en_size);
		record_update.Put("where", where.str());
		
		if(p_game_dbclient->ExecUpdate(m_pCharTable, &record_update) != 1)
		{
			Global::logger->fatal("[pay_db] updata money failed<orderid=%s,player=%s>", m_strOrderid.c_str(), m_strDecodePlayer.c_str());
			ret_code = false;
			break;
		}

		/// third
		MODI_Record record_insert;
		//MODI_RTime now_time;
		record_insert.Put("platformserial", m_strOrderid);
		record_insert.Put("accname", m_strDecodePlayer);
		record_insert.Put("accid", m_dwAccountID);
		record_insert.Put("zoneid", 1);
		record_insert.Put("action", m_byAction);
		record_insert.Put("money", m_dwMoney);
		record_insert.Put("paytime", m_strTime);
		record_insert.Put("keeptime", pay_data.m_stRTime.GetMSec());
		MODI_RecordContainer record_container;
		record_container.Put(&record_insert);
		if(p_game_dbclient->ExecInsert(m_pPayTable, &record_container) != 1)
		{
			Global::logger->fatal("[pay_db] %s insert to pay failed", m_strOrderid.c_str());
			ret_code = false;
			break;
		}
	}
	while(0);

	if(ret_code)
	{
		p_game_dbclient->CommitTransaction();
		RetPayResult(1);
		Global::logger->info("[pay_info] %s pay money=%u success", m_strOrderid.c_str(), m_dwMoney);
		MODI_ZoneTask * p_zone_task = MODI_ZoneTask::GetInstancePtr();
		if(p_zone_task)
		{
			send_cmd.m_stAccID = m_dwAccountID;
			send_cmd.m_dwMoney = m_dwLastMoney;
			send_cmd.m_dwPayMoney = m_dwMoney;
			p_zone_task->SendCmd(&send_cmd, sizeof(send_cmd));
		}
		MODI_DBManager::GetInstance().PutHandle(p_game_dbclient);
		return true;
	}
	else
	{
		MODI_PayManager::GetInstance().RemovePayData(m_strOrderid.c_str());
		p_game_dbclient->RollbackTransaction();
		MODI_DBManager::GetInstance().PutHandle(p_game_dbclient);
		RetPayResult(-5);
		Global::logger->fatal("[pay_info] %s pay failed and rollback", m_strOrderid.c_str());
		return false;
	}
}


/** 
 * @brief 返回冲值结果
 * 
 */
void MODI_BillTask::RetPayResult(int ret_code)
{
	std::string ret_result;
	if(ret_code == 1)
	{
		ret_result = "EXCHANGE_SUCCESS";
	}
	else if(ret_code == -3)
	{
		ret_result = "EXCHANGE_FAIL_PARAMETER_ERR";
	}
	else if(ret_code == -4)
	{
		ret_result = "EXCHANGE_FAIL_ORDERID_ERR";
	}
	else if(ret_code == -1)
	{
		ret_result = "EXCHANGE_FAIL_SIGNATURE_ERR";
	}
	else if(ret_code == -2)
	{
		ret_result = "EXCHANGE_FAIL_USER_ERR";
	}
	else if(ret_code == -5)
	{
		ret_result = "EXCHANGE_FAIL_SYSTEM_ERR";
	}

	std::ostringstream os;
	os<< "HTTP/1.1 200 OK\r\n"
	 	<<"Content-Type: text/html; charset=utf-8\r\n\r\n"
	  << ret_code << "&&" << m_strOrderid << "&&" << ret_result << "\r\n";
	
	SendCmd(os.str().c_str(), os.str().size());
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_BillTask::RecycleConn()
{
	return 1;
}



/** 
 * @brief 转换成md5
 * 
 * @param in_put 输入
 * 
 * @return 输出
 */
const char * MODI_BillTask::ToMD5(const char * in_put)
{
	if(!in_put)
	{
		std::string ret_str;
		return ret_str.c_str();
	}
	
	std::string in_str = in_put;
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(in_str.c_str()) , in_str.size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	return recv_check_str.c_str();
}



