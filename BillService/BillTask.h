/**
 * @file   BillTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:52:29 2010
 * @version $Id $
 * @brief  充值http连接
 * 
 */


#ifndef _MD_BILLTASK_H
#define _MD_BILLTASK_H

#include "ServiceTask.h"

/// 返回最大长度
const unsigned int RET_BUF_LENGTH = 4096;

/// 冲值返回
const WORD RET_PAY_SUCCESS = 1;
const WORD RET_PAY_FAILED = 2;

/// 兑换比
const unsigned int SET_PAY_RATE = 10;

class MODI_TableStruct;


/**
 * @brief 充值连接
 * 
 */
class MODI_BillTask: public MODI_ServiceTask
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sock 相关的sock
	 * @param addr 相关的地址
	 * 
	 */
	MODI_BillTask(const int sock, const struct sockaddr_in * addr): MODI_ServiceTask(sock, addr)
	{
		m_blCheckTimeOut = true;
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
		m_dwAccountID = 0;
		m_dwMoney = 0;
		m_dwLastMoney = 0;
		m_dwGiftMoney = 0;
	}

	/** 
	 * @brief 发送命令
	 * 
	 * @param send_cmd 要发送的命令 
	 * @param cmd_size 发送命令的大小不能超过64K
	 * 
	 * @return 成功true 失败false
	 */
	bool SendCmd(const char * send_cmd, const unsigned int cmd_size);
	
	/// 接收命令处理		
	bool RecvDataNoPoll();

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true 失败false
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);


	/** 
	 * @brief 是否可以回收
	 * 
	 * 
	 * @return 1可以回收 0继续等待
	 */
	int RecycleConn();

	/// 各个表指针
	static MODI_TableStruct * m_pPayTable;
	static MODI_TableStruct * m_pCharTable;

	/** 
	 * @brief 转换成md5
	 * 
	 * @param in_put 输入
	 * 
	 * @return 输出
	 */
	static const char * ToMD5(const char * in_put);
	
 private:
	/// 处理冲值
	bool ParsePay(const char *line_cmd);
	bool GetPayContent(const char * pay_content);
	bool DealPay();
	inline const char * GenLocalSerial();
	inline void RetPayResult(int ret_code);
	inline bool CheckMD5();

	/** 
	 * @brief 清空接收缓冲
	 * 
	 */
	inline void ResetRecvBuf()
	{
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}

	/// 接收缓冲
	char m_stRecvBuf[1024];

	/// 接收的个数
	unsigned int m_dwRecvCount;

	/// 冲值的内容
	std::string m_strOrderid;
	std::string m_strEncodePlayer;
	std::string m_strCurrency;
	std::string m_strGameCurrency;
	std::string m_strGameMoney;
	std::string m_strRatio;
	std::string m_strTime;
	std::string m_strSN;
	std::string m_strDecodePlayer;
	std::string m_strURLDecodePlayer;
	BYTE m_byAction;
	
	DWORD m_dwMoney;
	DWORD m_dwLastMoney;
	DWORD  m_dwAccountID;
	DWORD m_dwGiftMoney;
};

#endif
