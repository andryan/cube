/**
 * @file   BillTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Dec  6 14:30:29 2010
 * 
 * @brief  专门处理充值
 * 
 */

#include "BillTick.h"
#include "Global.h"
#include "ZoneTask.h"

MODI_BillTick * MODI_BillTick::m_pInstance = NULL;
MODI_RTime MODI_BillTick::m_stRTime;
MODI_TTime MODI_BillTick::m_stTTime;

unsigned long long showsize = 0;

MODI_BillTick::MODI_BillTick() : MODI_Thread("billtick")
{
	
}

MODI_BillTick::~MODI_BillTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_BillTick::Run()
{
    while(!IsTTerminate())
    {
		m_stRTime.GetNow();
		m_stTTime = m_stRTime;
    	::usleep(10000);

		MODI_ZoneTask * pTask = MODI_ZoneTask::GetInstancePtr();
		if(pTask)
		{
			pTask->Get(100);
		}
		
		if(m_stRTime.GetMSec() > showsize)
		{
			
		}
		
    }
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_BillTick::Final()
{
	
}
