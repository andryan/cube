/**
 * @file   BillTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Dec  6 14:29:19 2010
 * 
 * @brief  专门处理充值
 * 
 */


#ifndef _MDBILLTICK_H
#define _MDBILLTICK_H

#include "Type.h"
#include "Timer.h"
#include "Thread.h"

class MODI_BillTick: public MODI_Thread 
{
public:
    MODI_BillTick();
	
    virtual ~MODI_BillTick();

    void Final();

    virtual void Run();

	static MODI_BillTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_BillTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	static MODI_RTime m_stRTime;
	static MODI_TTime m_stTTime;
	
private:
	
	static MODI_BillTick * m_pInstance;
	
};

#endif
