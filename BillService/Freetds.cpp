/**
 * @file   Freetds.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Oct 24 17:38:31 2011
 * 
 * @brief  对freetds简单封装
 * 
 * 
 */

#include "Freetds.h"

static std::string g_strURL;

int err_handler(DBPROCESS * dbproc, int severity, int dberr, int oserr, char * dberrstr, char * oserrstr)
{
	if ((dbproc == NULL) || (DBDEAD(dbproc)))
	{
		MODI_Freetds::GetInstance().Init(g_strURL.c_str());
	}
	else
	{
		Global::logger->fatal("[err_handler] DB-Library error: %s", dberrstr);
	}
	return(INT_CANCEL);
}

MODI_Freetds * MODI_Freetds::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true
 *
 */
bool MODI_Freetds::Init(const char * tds_url)
{
	g_strURL.assign(tds_url);

	if(! m_stURLInfo.GetURLInfo(tds_url))
	{
		Global::logger->fatal("[system_init] unable get tds url");
		return false;
	}
	
	if(dbinit() == FAIL)
	{
		Global::logger->fatal("[system_init] call freetdb dbinit failed");
		return false;
	}

	if((logininfo = dblogin()) == NULL)
	{
		Global::logger->fatal("[system_init] call dblogin failed <name=%s,passwd=%s>",
							  m_stURLInfo.GetLoginName().c_str(), m_stURLInfo.GetPassWord().c_str());
		return false;
	}
	
	DBSETLUSER(logininfo, m_stURLInfo.GetLoginName().c_str());
	DBSETLPWD(logininfo, m_stURLInfo.GetPassWord().c_str());

	dberrhandle(err_handler);

	/// Set network timeout for DB open
	dbsetlogintime(60 /*seconds*/);

	if((dbproc = dbopen(logininfo, m_stURLInfo.GetLoginIP().c_str())) == NULL)
	{
		Global::logger->fatal("[system_init] call dbopen failed <server=%s>", m_stURLInfo.GetLoginIP().c_str());
		return false;
	}

	/// Set network timeout for SQL query
	dbsettime(10 /*seconds*/);

// 	if(m_stURLInfo.GetDBName() != "" && (dbuse(dbproc, m_stURLInfo.GetDBName().c_str())) == FAIL)
// 	{
// 		Global::logger->fatal("[system_init] dbuser failed <dbname=%s>", m_stURLInfo.GetDBName().c_str());
// 		return false;
// 	}

	return true;
}


/** 
 * @brief 购买返回
 * 
 * @param MODI_OrderInPara 输入参数
 * 
 * @return 返回结果
 *
 */
bool MODI_Freetds::ReqOrder(const MODI_OrderInPara & in_para, MODI_OrderOutPara  & out_para)
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );

	std::ostringstream os;
	/// INA
	if(Global::g_byGameShop == 1)
	{
		/// 定义参数
		os << "DECLARE @orderTypeId AS TINYINT "
		   <<"DECLARE @gameServiceId AS INT "
		   <<"DECLARE @publisherUserId AS NVARCHAR(50) "
		   <<"DECLARE @toPublisherUserId AS NVARCHAR(50) "
		   <<"DECLARE @cpId AS INT "
		   <<"DECLARE @productCode AS NVARCHAR(20) "
		   <<"DECLARE @gameTransactionId AS NVARCHAR(50) "
		   <<"DECLARE @gameTransactionDate AS DATETIME "
		   <<"DECLARE @transactionId AS INT "
		   <<"DECLARE @productId AS INT "
		   <<"DECLARE @return AS INT "
		   <<"DECLARE @returnMessage AS NVARCHAR(100) "
			
			/// 构造参数
		   <<" SET @orderTypeId = \'" << in_para.orderTypeId << "\'"
		   <<" SET @gameServiceId = \'" << in_para.gameServiceId << "\'"
		   <<" SET @publisherUserId = \'" << in_para.publisherUserId << "\'"
		   <<" SET @toPublisherUserId = \'" << in_para.toPublisherUserId << "\'"
		   <<" SET @cpId =  \'" << in_para.cpId << "\'"
		   <<" SET @productCode = \'" << in_para.productCode << "\'"
		   <<" SET @gameTransactionId = \'"<< in_para.gameTransactionId << "\'"
		   <<" SET @gameTransactionDate = \'" << in_para.gameTransactionDate << "\'"

			/// 调用存储过程
		   <<" EXECUTE procOrder "
		   <<"@orderTypeId "
		   <<",@gameServiceId "
		   <<",@publisherUserId "
		   <<",@toPublisherUserId "
		   <<",@cpId "
		   <<",@productCode "
		   <<",@gameTransactionId "
		   <<",@gameTransactionDate "
		   <<",@transactionId OUTPUT "
		   <<",@productId OUTPUT "
		   <<",@return OUTPUT "
		   <<",@returnMessage OUTPUT "
			/// 获取输出
		   <<"SELECT 'tid'=@transactionId, 'pid'=@productId, 'rid'=@return, 'msg'=@returnMessage ";
	}
	/// SGP
	else if(Global::g_byGameShop == 2)
	{
		/// 定义参数
		os<<"DECLARE @accountId           AS INT "
		  <<"DECLARE @cashPoint AS INT "
		  <<"DECLARE @accountIP         AS NVARCHAR(50) "
		  <<"DECLARE @gameTransId       AS NVARCHAR(50) "

		  <<"DECLARE @returnOrderId           AS INT "
		  <<"DECLARE @verification                  AS INT "
                                       
		  <<"SET @accountId = \'" << in_para.m_dwAccountId << "\' "
		  <<"SET @cashPoint = \'"  << in_para.m_dwPrice << "\' "
		  <<"SET @accountIP = \'" << in_para.publisherUserId << "\' "
		  <<"SET @gameTransId = \'" << in_para.gameTransactionId << "\' "

		  <<" EXECUTE myportalcenter.dbo.mp_STAccountSaleItemInGame "
		  <<"   @accountId "
		  <<",@accountIP "
		  <<",@cashPoint "
		  <<",@gameTransId "
		  <<",@verification OUTPUT "
		  <<",@returnOrderId OUTPUT "
		  <<"SELECT 'tid'=@returnOrderId, 'rid'=@verification ";
	}
	

#ifdef _DEBUG
	Global::logger->debug("[lgadb] <%s>", os.str().c_str());
#endif
	
	if(dbfcmd(dbproc, "%s ", os.str().c_str()) == FAIL)
	{
		Global::logger->fatal("[make_sql] dbfcmd failed <cmd=%s>", os.str().c_str());
		return false;
	}

	if(dbsqlexec(dbproc) == FAIL)
	{
		Global::logger->fatal("[exec_sql] dbsqlexec failed <cmd=%s>", os.str().c_str());
		return false;
	}
	
	int erc = 0;
	int ncols = 0;
	while((erc = dbresults(dbproc)) != NO_MORE_RESULTS)
	{
		if (erc == FAIL)
		{
			Global::logger->fatal("[get_resutl] db get result failed");
			return false;
		}
		
		ncols = dbnumcols(dbproc);
		if(ncols == 0)
		{
			continue;
		}
		
		MODI_MSSQLRESULT resutl_array[ncols];
		for(int i=0; i<ncols; i++)
		{
			strncpy(resutl_array[i].name, dbcolname(dbproc, i+1), sizeof(resutl_array[i].name));		
			resutl_array[i].type = dbcoltype(dbproc, i+1);
			resutl_array[i].size = dbcollen(dbproc, i+1);
			if (SYBCHAR != resutl_array[i].type)
			{			
				resutl_array[i].size = dbwillconvert(resutl_array[i].type, SYBCHAR);
			}
			
			erc = dbbind(dbproc, i+1, NTBSTRINGBIND,	resutl_array[i].size+1, (BYTE*)resutl_array[i].buffer);
			if (erc == FAIL)
			{
				Global::logger->fatal("dbbind failed");
				return false;
			}

			erc = dbnullbind(dbproc, i+1, &resutl_array[i].status);	
			if (erc == FAIL)
			{
				Global::logger->fatal("dbnullbind failed");
				return 0;
			}
		}

		/// get buffer
		while( dbnextrow(dbproc) != NO_MORE_ROWS);

		for(int i=0; i<ncols; i++)
		{
			
#if _INA_ITEM
			Global::logger->debug("name=%s,buffer=%s,type=%d,size=%d,status=%d",
								  resutl_array[i].name, resutl_array[i].buffer, resutl_array[i].type,
								  resutl_array[i].size, resutl_array[i].status);
#endif
			if( strcmp(resutl_array[i].name,"tid") == 0 && resutl_array[i].status == 0)
			{
				out_para.transactionId = atoi(resutl_array[i].buffer);
			}
			else if(strcmp(resutl_array[i].name, "pid") == 0 && resutl_array[i].status == 0)
			{
				out_para.productId = atoi(resutl_array[i].buffer);
			}
			else if(strcmp(resutl_array[i].name, "rid") == 0 && resutl_array[i].status == 0)
			{
				out_para.returnId = atoi(resutl_array[i].buffer);
			}
			else if(strcmp(resutl_array[i].name, "msg") == 0 && resutl_array[i].status == 0)
			{
				strncpy(out_para.returnMessage, resutl_array[i].buffer, sizeof(out_para.returnMessage));
			}
		}
		
		Global::logger->debug("tid=%d,pid=%d,rid=%d,msg=%s",
								  out_para.transactionId, out_para.productId, out_para.returnId, out_para.returnMessage);
	}

	return true;
}




/** 
 * @brief 查询价格
 *
 */
bool MODI_Freetds::ReqPrice(const MODI_PriceInPara & in_para, MODI_PriceOutPara & out_para)
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );

	std::ostringstream os;
	os<<"DECLARE @productCode AS NVARCHAR(20) "
	  <<"DECLARE @gameServiceId AS INT "
	  <<"DECLARE @productId AS INT "
	  <<"DECLARE @productName AS NVARCHAR(50) "
	  <<"DECLARE @cyberCashName AS NVARCHAR(20) "
	  <<"DECLARE @cyberCashAmount AS INT "
	  <<"SET @gameServiceId = 6 "
	  <<"SET @productCode = \'" << in_para.productCode << "\' "
	  <<" EXECUTE procProductSelectForGame "
	  <<"@productCode "
	  <<",@gameServiceId "
	  <<",@productId OUTPUT "
	  <<",@productName OUTPUT "
	  <<",@cyberCashName OUTPUT "
	  <<",@cyberCashAmount OUTPUT "
	  <<"SELECT 'pid'=@productId, 'pname'=@productName, 'name'=@cyberCashName, 'amount'=@cyberCashAmount";
		
	if(dbfcmd(dbproc, "%s ", os.str().c_str()) == FAIL)
	{
		Global::logger->fatal("[make_sql] dbfcmd failed <cmd=%s>", os.str().c_str());
		return false;
	}

	if(dbsqlexec(dbproc) == FAIL)
	{
		Global::logger->fatal("[exec_sql] dbsqlexec failed <cmd=%s>", os.str().c_str());
		return false;
	}
	
	int erc = 0;
	int ncols = 0;
	while((erc = dbresults(dbproc)) != NO_MORE_RESULTS)
	{
		if (erc == FAIL)
		{
			Global::logger->fatal("[get_price_resutl] db get result failed");
			return false;
		}
		
		ncols = dbnumcols(dbproc);
		if(ncols == 0)
		{
			continue;
		}
		
		MODI_MSSQLRESULT resutl_array[ncols];
		for(int i=0; i<ncols; i++)
		{
			strncpy(resutl_array[i].name, dbcolname(dbproc, i+1), sizeof(resutl_array[i].name));		
			resutl_array[i].type = dbcoltype(dbproc, i+1);
			resutl_array[i].size = dbcollen(dbproc, i+1);
			if (SYBCHAR != resutl_array[i].type)
			{			
				resutl_array[i].size = dbwillconvert(resutl_array[i].type, SYBCHAR);
			}
			
			erc = dbbind(dbproc, i+1, NTBSTRINGBIND,	resutl_array[i].size+1, (BYTE*)resutl_array[i].buffer);
			if (erc == FAIL)
			{
				Global::logger->fatal("dbbind failed");
				return false;
			}

			erc = dbnullbind(dbproc, i+1, &resutl_array[i].status);	
			if (erc == FAIL)
			{
				Global::logger->fatal("dbnullbind failed");
				return 0;
			}
		}

		/// get buffer
		while( dbnextrow(dbproc) != NO_MORE_ROWS);

		for(int i=0; i<ncols; i++)
		{
			
#if _INA_ITEM
			Global::logger->debug("name=%s,buffer=%s,type=%d,size=%d,status=%d",
								  resutl_array[i].name, resutl_array[i].buffer, resutl_array[i].type,
								  resutl_array[i].size, resutl_array[i].status);
#endif
			if( strcmp(resutl_array[i].name,"pid") == 0 && resutl_array[i].status == 0)
			{
				out_para.productId = atoi(resutl_array[i].buffer);
			}
			else if(strcmp(resutl_array[i].name, "pname") == 0 && resutl_array[i].status == 0)
			{
				strncpy(out_para.productName, resutl_array[i].buffer, sizeof(out_para.productName));
			}
			else if(strcmp(resutl_array[i].name, "name") == 0 && resutl_array[i].status == 0)
			{
				strncpy(out_para.cyberCashName, resutl_array[i].buffer, sizeof(out_para.cyberCashName));
			}
			else if(strcmp(resutl_array[i].name, "amount") == 0 && resutl_array[i].status == 0)
			{
				out_para.cyberCashAmount = atoi(resutl_array[i].buffer);
			}
		}
		
		//	Global::logger->debug("pid=%d,pname=%s,name=%s,amount=%d",
		//								  out_para.productId, out_para.productName, out_para.cyberCashName, out_para.cyberCashAmount);
	}

	return true;
}



/** 
 * @brief 查询某个点券
 *
 */
bool MODI_Freetds::ReqBalance(const MODI_BalanceInPara & in_para, MODI_BalanceOutPara & out_para)
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );

	std::ostringstream os;

	if(Global::g_byGameShop == 1)
	{
		os<<"DECLARE @publisherUserId AS NVARCHAR(50) "
		  <<"DECLARE @gameServiceId AS INT "
		  <<"DECLARE @cashBalance AS INT "
		  <<"SET @gameServiceId = \'6\' "
		  <<"SET @publisherUserId = \'" << in_para.publisherUserId << "\' "

		  <<"EXECUTE procUserBalanceSelectForGame "
		  <<"@gameServiceId"
		  <<",@publisherUserId"
		  <<",@cashBalance OUTPUT"
		  <<" SELECT 'balance'=@cashBalance";
	}
	else if(Global::g_byGameShop == 2)
	{
		os<<"DECLARE @accountId AS INT "
		  <<"DECLARE @cashPoint AS INT "
		  <<"SET @accountId = \'" << in_para.m_dwAccountId << "\' "

		  <<"EXECUTE myportalcenter.dbo.mp_STGetAccountCashPoint "
		  <<"@accountId"
		  <<",@cashPoint OUTPUT"
		  <<" SELECT 'balance'=@cashPoint";
	}
	
#ifdef _DEBUG
	Global::logger->debug("[freetds_sql] <%s>", os.str().c_str());
#endif	
	if(dbfcmd(dbproc, "%s ", os.str().c_str()) == FAIL)
	{
		Global::logger->fatal("[make_sql] dbfcmd failed <cmd=%s>", os.str().c_str());
		return false;
	}

	if(dbsqlexec(dbproc) == FAIL)
	{
		Global::logger->fatal("[exec_sql] dbsqlexec failed <cmd=%s>", os.str().c_str());
		return false;
	}
	
	int erc = 0;
	int ncols = 0;
	while((erc = dbresults(dbproc)) != NO_MORE_RESULTS)
	{
		if (erc == FAIL)
		{
			Global::logger->fatal("[get_balance_resutl] db get result failed");
			return false;
		}
		
		ncols = dbnumcols(dbproc);
		if(ncols == 0)
		{
			continue;
		}
		
		MODI_MSSQLRESULT resutl_array[ncols];
		for(int i=0; i<ncols; i++)
		{
			strncpy(resutl_array[i].name, dbcolname(dbproc, i+1), sizeof(resutl_array[i].name));		
			resutl_array[i].type = dbcoltype(dbproc, i+1);
			resutl_array[i].size = dbcollen(dbproc, i+1);
			if (SYBCHAR != resutl_array[i].type)
			{			
				resutl_array[i].size = dbwillconvert(resutl_array[i].type, SYBCHAR);
			}
			
			erc = dbbind(dbproc, i+1, NTBSTRINGBIND,	resutl_array[i].size+1, (BYTE*)resutl_array[i].buffer);
			if (erc == FAIL)
			{
				Global::logger->fatal("dbbind failed");
				return false;
			}

			erc = dbnullbind(dbproc, i+1, &resutl_array[i].status);	
			if (erc == FAIL)
			{
				Global::logger->fatal("dbnullbind failed");
				return 0;
			}
		}

		/// get buffer
		while( dbnextrow(dbproc) != NO_MORE_ROWS);

		for(int i=0; i<ncols; i++)
		{
			
#if _INA_ITEM
			Global::logger->debug("name=%s,buffer=%s,type=%d,size=%d,status=%d",
								  resutl_array[i].name, resutl_array[i].buffer, resutl_array[i].type,
								  resutl_array[i].size, resutl_array[i].status);
#endif
			if( strcmp(resutl_array[i].name,"balance") == 0 && resutl_array[i].status == 0)
			{
				out_para.cashBalance = atoi(resutl_array[i].buffer);
			}
		}
		
		Global::logger->debug("balance=%u", out_para.cashBalance);
	}

	return true;
}


/** 
 * @brief 释放资源
 * 
 */
void MODI_Freetds::Final()
{
	dbclose(dbproc);
	dbexit();
}



