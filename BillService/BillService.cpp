/**
 * @file   BillService.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:46:36 2010
 * @version $Id $
 * @brief  充值服务器
 * 
 */

#include "BillService.h"
#include "BillTask.h"
#include "ZoneTask.h"
#include "FunctionTime.h"

MODI_BillService * MODI_BillService::m_pInstance = NULL;
std::vector<WORD> MODI_BillService::m_stPortVec;
std::vector<std::string> MODI_BillService::m_stIPVec;

/** 
 * @brief 创建新任务
 * 
 * @param sock 任务相关的task
 * @param addr 任务相关的地址
 * @param port 任务相关的端口
 * 
 * @return 成功true 失败false
 */		
bool MODI_BillService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	/// 增加一个充值http连接
	if(port == m_wdBillPort)
	{
		if(sock == -1 || addr == NULL)
			return false;
		std::string pay_ip = inet_ntoa(addr->sin_addr);
		if(!(pay_ip == Global::g_strPayIP || pay_ip == "192.168.2.208" || pay_ip == "202.107.244.122" || pay_ip == "119.188.10.237"))
		{
			Global::logger->fatal("[pay_ip_error] <payip=%s,setip=%s>", pay_ip.c_str(), Global::g_strPayIP.c_str());
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}
		
		MODI_BillTask * p_task = new MODI_BillTask(sock, addr);
		if(p_task == NULL)
		{
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}
		if(m_stBillTaskSched.AddNormalSched(p_task))
		{
			return true;
		}
		delete p_task;
		TEMP_FAILURE_RETRY(close(sock));
		return false;
	}
	/// 增加一个游戏服务器连接
	else if(port == m_wdZonePort)
	{
		if(sock == -1 || addr == NULL)
			return false;
		if(MODI_ZoneTask::GetInstancePtr())
		{
			WORD n_port = ntohs(addr->sin_port);                    
			std::string str_ip = inet_ntoa(addr->sin_addr);
			Global::logger->fatal("only one zone task,not add a task(%s:%d)", str_ip.c_str(), n_port);
			if(sock != -1)
				TEMP_FAILURE_RETRY(close(sock));
			return false;
		}
		
		MODI_ZoneTask * p_task = new MODI_ZoneTask(sock, addr);
		if(p_task == NULL)
		{
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}
		
		if(m_stZoneTaskSched.AddNormalSched(p_task))
			return true;
		delete p_task;
		TEMP_FAILURE_RETRY(close(sock));
		return false;
	}
	else
	{
		if(sock != -1)
			TEMP_FAILURE_RETRY(close(sock));
	}
	return false;
}


/** 
 * @brief 初始化
 * 
 * @return 成功true 失败false
 */
bool MODI_BillService::Init()
{
	if(! MODI_MNetService::Init())
		return false;
	m_stBillTaskSched.Init();
	m_stZoneTaskSched.Init();
	return true;
}

/** 
 * @brief 释放资源
 * 
 */
void MODI_BillService::Final()
{
	MODI_MNetService::Final();
}
