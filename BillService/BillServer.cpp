/**
 * @file   BillServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:40:29 2010
 * @version $Id $
 * @brief  程序入口
 * 
 */

#include "Global.h"
#include "Share.h"
#include "ServerConfig.h"
#include "BillService.h"
#include "BillTick.h"
#include "PayManager.h"
#include "DBClient.h"
#include "BillTask.h"
#include "Freetds.h"
#include "GlobalDB.h"
#include "ZoneTask.h"
#include "BinFileMgr.h"

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 *
 */
bool Init(int argc, char * argv[])
{
	/// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	/// 初始化日志对象
	Global::logger = new MODI_Logger("bill");
	Global::net_logger = Global::logger;
	
	/// 加载配置文件
	if(!MODI_ServerConfig::GetInstance().Load(Global::Value["config_path"].c_str()))
	{
		Global::logger->fatal("[load_config] billservice load config fialed");
		return false;
	}

	const MODI_SvrBillConfig  * pBillConfig = (const MODI_SvrBillConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_BILLSERVICE ));
	if( !pBillConfig )
	{
		Global::logger->debug("not get config billservice");
		return false;
	}
	
	/// 增加一个日志文件到指定目录
	std::string net_log_path_bill = pBillConfig->m_strLogPath;
	std::string::size_type c_size = net_log_path_bill.find_last_of("/", net_log_path_bill.size());
	if(c_size == std::string::npos)
	{
		return false;
	}
	
	MODI_ZoneTask::m_strConsumeFile = net_log_path_bill.substr(0, c_size);
    Global::logger->AddLocalFileLog(pBillConfig->m_strLogPath.c_str());


	MODI_BillService::m_stPortVec.clear();
	MODI_BillService::m_stPortVec.push_back(pBillConfig->m_dwBillPort);
	MODI_BillService::m_stPortVec.push_back(pBillConfig->m_dwZonePort);

	MODI_BillService::m_stIPVec.clear();
	MODI_BillService::m_stIPVec.push_back( pBillConfig->m_strBillIP.c_str() );
	MODI_BillService::m_stIPVec.push_back( pBillConfig->m_strZoneInIP.c_str() );

	// 连接游戏数据库
	if(! MODI_DBManager::GetInstance().Init(Global::g_stURLMap["gamedb"]))
	{
		Global::net_logger->debug("not connect gamedb");
		return false;
	}

	/// get pay ip from db
	MODI_RecordContainer record_select;
	std::ostringstream where;
	where << "id=\'" << 1 << "\'";
	MODI_Record select_field;
	select_field.Put("ip");

	MODI_DBClient * p_ip_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_ip_dbclient)
	{
		Global::logger->fatal("--->>not get db client");
		return false;
	}

	MODI_TableStruct * p_ip_table = MODI_DBManager::GetInstance().GetTable("payip");
	if(! p_ip_table)
	{
		Global::logger->fatal("[%s] Unable get payip table struct", SYS_INIT);
		return false;
	}

	if((p_ip_dbclient->ExecSelect(record_select, p_ip_table, where.str().c_str(), &select_field)) != 1)
	{
		Global::logger->fatal("not get ip");
		return false;
	}

	MODI_Record * p_select_record = record_select.GetRecord(0);
	if(p_select_record == NULL)
	{
		Global::logger->fatal("not get ip1");
		return false;
	}
	
	MODI_VarType v_ip = p_select_record->GetValue("ip");
	if(v_ip.Empty())
	{
		Global::logger->fatal("not get ip2");
		return false;
	}
	
	Global::g_strPayIP = (const char *)v_ip;
	Global::logger->debug("[check_payip] <ip=%s>", Global::g_strPayIP.c_str());
	
	MODI_DBManager::GetInstance().PutHandle(p_ip_dbclient);

	/// 充值表
	MODI_BillTask::m_pPayTable = MODI_DBManager::GetInstance().GetTable("platformpay");
	if(! MODI_BillTask::m_pPayTable)
	{
		Global::logger->fatal("[%s] Unable get PLATFORMPAY table struct", SYS_INIT);
		return false;
	}
	
	/// 游戏数据库中的characters表
	MODI_BillTask::m_pCharTable = MODI_DBManager::GetInstance().GetTable("characters");
	if(! MODI_BillTask::m_pCharTable)
	{
		Global::logger->fatal("[%s] Unable get CHARACTERS table struct", SYS_INIT);
		return false;
	}


// 	std::string _url;
// 	if(Global::g_byGameShop == 1)
// 	{
// 		_url = "modidb:://inGame@1433&modi@!qlffld!&hrx_test";
// 	}
// 	else if(Global::g_byGameShop == 2)
// 	{
// 		_url = "modidb:://sgp@1433&modimssqlserveruser@dk2S2jFa205kd12dUWhb&hrx_test";
// 	}

	if(Global::g_byGameShop > 0)
	{
		if(! MODI_Freetds::GetInstance().Init(pBillConfig->m_strURL.c_str()))
		{
			return false;
		}
	
		MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
		if(! p_db_client)
		{
			Global::logger->fatal("--->>not get db client");
			return false;
		}
		/// get max(buy_serial)
		MODI_RecordContainer select_container;
		MODI_Record select_record;
		std::string select_max = "MAX(`consumeid`)";
		select_record.Put(select_max);
		int rec_num = p_db_client->ExecSelect(select_container, GlobalDB::m_pINAConsume, NULL, &select_record);
		if(rec_num == 1)
		{
			MODI_Record * get_record = select_container.GetRecord(0);
			const MODI_VarType & v_max_itemid = get_record->GetValue(select_max);
			if(! v_max_itemid.Empty())
			{
				MODI_ZoneTask::m_qdBuySerial = (unsigned long long)v_max_itemid;
			}
			else
			{
				MODI_ZoneTask::m_qdBuySerial = 0;
			}
		}
		else
		{
			MODI_ASSERT(0);
			return false;
		}
		MODI_DBManager::GetInstance().PutHandle(p_db_client);
	}

	/// 临时
	Global::logger->RemoveConsoleLog();

#if 0
	Global::logger->debug("[check_item] check price begin, will take a long time");
	/// 加载资源表，核对INA的
	if(Global::g_byGameShop == 1)
	{
		const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
		if( !pResInfo )
		{
			return false;
		}

		int iNoLoadMask = enBinFT_Music | enBinFT_Level | enBinFT_Item | enBinFT_Suit | enBinFT_Avatar | enBinFT_GoodsPackage;

		// 初始化bin mgr
		if (!MODI_BinFileMgr::GetInstancePtr()->Initial( pResInfo->strBinDir.c_str() , iNoLoadMask ) )
		{
			Global::logger->fatal("[system_init] load bin file failed");
			return false;
		}

		/// 核对价格好物品信息
		defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
		for( size_t n = 0; n < shopBin.GetSize(); n++ )
		{
			const GameTable::MODI_Shop * pElement = shopBin.GetByIndex(n);
			if( !pElement )
			{
				return false;
			}
			if( pElement->get_ConfigID() == INVAILD_CONFIGID )
			{
				return false;
			}

			enMoneyType bys = (enMoneyType)(pElement->get_BuyStrategy());
			/// 金币
			if(kGameMoney == bys)
			{
				continue;
			}
			else if(kRMBMoney == bys)
			{
				DWORD item_id = pElement->get_SellItemID();
				if(item_id > 50000)
				{
					/// 只要永久的价格
					DWORD price = pElement->get_BS_2_Time4();
					MODI_PriceInPara in_para;
					MODI_PriceOutPara out_para;
					std::ostringstream item_name;
					item_name << pElement->get_ConfigID() << "_3";
					strncpy(in_para.productCode, item_name.str().c_str(), sizeof(in_para.productCode));
					MODI_Freetds::GetInstance().ReqPrice(in_para, out_para);
					if(out_para.cyberCashAmount != price)
					{
						Global::logger->fatal("[check_shop] item price failed <itemid=%s,price=%u,get_amount=%u>",
											  in_para.productCode, price, out_para.cyberCashAmount);
						return false;
					}
				}
				else
				{
					/// 7天，30天，永久的
					DWORD price_1 = pElement->get_BS_2_Time2();
					MODI_PriceInPara in_para;
					MODI_PriceOutPara out_para;
					std::ostringstream item_name;
					item_name << pElement->get_ConfigID() << "_1";
					strncpy(in_para.productCode, item_name.str().c_str(), sizeof(in_para.productCode));
					MODI_Freetds::GetInstance().ReqPrice(in_para, out_para);
					if(out_para.cyberCashAmount != price_1)
					{
						Global::logger->fatal("[check_shop] item price failed <itemid=%s,price=%u,get_amount=%u>",
											  in_para.productCode, price_1, out_para.cyberCashAmount);
						return false;
					}
					
					DWORD price_2 = pElement->get_BS_2_Time3();
					item_name.str("");
					item_name << pElement->get_ConfigID() << "_2";
					memset(in_para.productCode, 0, sizeof(in_para.productCode));
					out_para.cyberCashAmount = 0;
					strncpy(in_para.productCode, item_name.str().c_str(), sizeof(in_para.productCode));
					MODI_Freetds::GetInstance().ReqPrice(in_para, out_para);
					if(out_para.cyberCashAmount != price_2)
					{
						Global::logger->fatal("[check_shop] item price failed <itemid=%s,price=%u,get_amount=%u>",
											  in_para.productCode, price_2, out_para.cyberCashAmount);
						return false;
					}
					
					DWORD price_3 = pElement->get_BS_2_Time4();
					memset(in_para.productCode, 0, sizeof(in_para.productCode));
					out_para.cyberCashAmount = 0;
					item_name.str("");
					item_name << pElement->get_ConfigID() << "_3";
					strncpy(in_para.productCode, item_name.str().c_str(), sizeof(in_para.productCode));
					MODI_Freetds::GetInstance().ReqPrice(in_para, out_para);
					if(out_para.cyberCashAmount != price_3)
					{
						Global::logger->fatal("[check_shop] item price failed <itemid=%s,price=%u,get_amount=%u>",
											  in_para.productCode, price_3, out_para.cyberCashAmount);
						return false;
					}
				}
				
			}
			
		}
	}
	Global::logger->debug("[check_item] check price end");
#endif	
	return true;
}


/** 
 * @brief 结束
 * 
 */
void Final()
{
	MODI_DBManager::GetInstance().DelInstance();
	MODI_PayManager::GetInstance().DelInstance();
	MODI_BillTick::GetInstance().DelInstance();
	MODI_BillService::GetInstance().DelInstance();
	if(Global::logger)
	{
 		delete Global::logger;
	}
 	Global::logger = NULL;
}



int main(int argc, char * argv[])
{
	if(! Init(argc, argv))
	{
		Global::logger->fatal("[system_init] billservice initializtion faield");
		return 0;
	}

	if(! MODI_PayManager::GetInstance().Init())
	{
		Global::logger->fatal("Initialzation pay manager fialed");
		return false;
	}

	if(! MODI_BillTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization bill client");
		return false;
	}

	MODI_BillService::GetInstance().Main();

	Global::logger->info("[system_run] <<<<<<------billservice terminate------>>>>>>");
	Final();
	return 0;
}

