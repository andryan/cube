/**
 * @file   Freetds.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Oct 24 17:25:44 2011
 * 
 * @brief  freetds 头文件
 * 
 * 
 */

#ifndef _MD_FREETDS_H_
#define _MD_FREETDS_H_

#include "RecurisveMutex.h"
#include "DBStruct.h"
#include "Global.h"
#include "AssertEx.h"
#include </usr/local/include/sybfront.h>
#include </usr/local/include/sybdb.h>

/// mssql结果
struct MODI_MSSQLRESULT
{
	MODI_MSSQLRESULT()
	{
		memset(name, 0, sizeof(name));
		memset(buffer, 0, sizeof(buffer));
		type = 0;
		size = 0;
		status = 0;
	};
	
	char name[33];
	char buffer[100];
	int type;
	int size;
	int status;
};


/// 交易输入参数
struct MODI_OrderInPara
{
	MODI_OrderInPara()
	{
		orderTypeId = 1;
		gameServiceId = 6;
		cpId = 1;
		m_dwAccountId = 0;
		m_dwPrice = 0;
		memset(publisherUserId , 0, sizeof(publisherUserId));
		memset(toPublisherUserId,  0, sizeof(toPublisherUserId));
		memset(productCode, 0, sizeof(productCode));
		memset(gameTransactionId, 0, sizeof(gameTransactionId));
		memset(gameTransactionDate, 0, sizeof(gameTransactionDate));
	}
	
	WORD orderTypeId;
	DWORD gameServiceId;
	DWORD cpId;

	/// src accname
	char publisherUserId[50];
	/// dest accname
	char toPublisherUserId[50];
	DWORD m_dwAccountId;
	DWORD m_dwPrice;
	/// item id
	char productCode[20];
	/// buy_serial
	char gameTransactionId[50];
	/// buy_date
   	char gameTransactionDate[20];
};

/// 交易输出参数
struct MODI_OrderOutPara
{
	MODI_OrderOutPara()
	{
		transactionId = 0;
		productId = 0;
		returnId = 0;
		memset(returnMessage, 0, sizeof(returnMessage));
	}
	DWORD transactionId;
	DWORD productId;
	int returnId;
	char returnMessage[100];
};


/// 询问价格输入参数
struct MODI_PriceInPara
{
	MODI_PriceInPara()
	{
		gameServiceId = 6;
		memset(productCode, 0, sizeof(productCode));
	}

	/// 道具标识
	char productCode[20];
	/// 服务器id
	int gameServiceId;
};


/// 询问价格输出参数
struct MODI_PriceOutPara
{
	MODI_PriceOutPara()
	{
		productId = 0;
		memset(productName, 0, sizeof(productName));
		memset(cyberCashName, 0, sizeof(cyberCashName));
		cyberCashAmount = 0;
	}

	DWORD productId;
	char productName[50];
	char cyberCashName[20];
	DWORD cyberCashAmount;
};



/// 查询用户钱信息输入信息
struct MODI_BalanceInPara
{
	MODI_BalanceInPara()
	{
		memset(publisherUserId, 0, sizeof(publisherUserId));
		m_dwAccountId = 0;
	}
	
	char publisherUserId[33];
	DWORD m_dwAccountId;
};

/// 查询用户钱信息输出
struct MODI_BalanceOutPara
{
	MODI_BalanceOutPara()
	{
		cashBalance = 0;
	}
	DWORD cashBalance;
};


/**
 * @brief  封装一下freetds
 * 
 */
class MODI_Freetds
{
 public:
	static MODI_Freetds & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_Freetds;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	MODI_Freetds()
	{
		
	}

	~MODI_Freetds()
	{
		Final();
	}
	


	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true
	 *
	 */
	bool Init(const char * tds_url);


	/** 
	 * @brief 购买扣钱返回
	 * 
	 * @param MODI_OrderInPara 输入参数
	 * 
	 * @return 结果
	 *
	 */
	bool ReqOrder(const MODI_OrderInPara & in_para, MODI_OrderOutPara & out_para);


	/** 
	 * @brief 查询某个道具价格，服务器启动时核对一下
	 * 
	 * @param MODI_PriceInPara 输入参数
	 * 
	 * @return 结果
	 *
	 */
	bool ReqPrice(const MODI_PriceInPara & in_para, MODI_PriceOutPara & out_para);


	/** 
	 * @brief 查询某个人的点券,登陆和充值时刷新
	 * 
	 * @param MODI_BalanceInPara 输入参数
	 * 
	 * @return 结果
	 *
	 */
	bool ReqBalance(const MODI_BalanceInPara & in_para, MODI_BalanceOutPara & out_para);

	
 private:

	/** 
	 * @brief 结束
	 * 
	 */
	void Final();
	
	static MODI_Freetds * m_pInstance;

	/// 数据库连接
	DBPROCESS * dbproc;

	/// 登陆信息
	LOGINREC * logininfo;
	
	/// 连接的url
	MODI_URLInfo m_stURLInfo;

	MODI_RecurisveMutex m_mutexSqls;
};


#endif
