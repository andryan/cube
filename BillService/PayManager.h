/**
 * @file   PayManager.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Wed Oct 13 13:55:29 2010
 * @version $Id:$
 * @brief  交易管理
 * 
 */

#ifndef _MD_PAYMANAGER_H
#define _MD_PAYMANAGER_H

#include "Global.h"
#include "Thread.h"


/// 冲值记录保存的时间
const QWORD PAY_KEEP_TIME = 24 * 3600 * 1000;

/**
 * @brief 交易数据
 * 
 */
class MODI_PayData
{
 public:
	MODI_PayData()
	{
		m_stRTime.Delay(PAY_KEEP_TIME);
		//memset(m_cstrLocalSerial, 0, sizeof(m_cstrLocalSerial));
	}
	MODI_RTime m_stRTime;
	//char m_cstrLocalSerial[33];
};


/**
 * @brief 交易管理
 * 
 */
class MODI_PayManager: public MODI_Thread
{
 public:
	typedef __gnu_cxx::hash_map<std::string, MODI_PayData, str_hash> defPayDataMap;
	typedef defPayDataMap::iterator defPayDataMapIter;
	typedef defPayDataMap::value_type defPayDataMapValue;

	MODI_PayManager(): MODI_Thread("PayManager")
	{
		
	}

	~MODI_PayManager()
	{
		Final();
	}
	
	static MODI_PayManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_PayManager;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}

	bool Init();

	void Run();

	/** 
	 * @brief 增加一个支付数据
	 * 
	 * @param pay_serial 支付序号
	 * @param pay_data 支付数据
	 * 
	 * @return 成功ture,失败false
	 */
	bool AddPayData(const char * pay_serial, MODI_PayData pay_data);

	/** 
	 * @brief 删除一个支付数据
	 * 
	 * @param pay_serial 支付数据
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool RemovePayData(const char * pay_serial);

	/** 
	 * @brief 是不是支付数据
	 * 
	 * @param pay_serial 支付数据
	 * @param pay_data 获取的数据
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool IsPayData(const char * pay_serial);
	
 private:
	void Final()
	{
		TTerminate();
		Join();
	}
	static MODI_PayManager * m_pInstance;
	defPayDataMap m_PayDataMap;
	MODI_RWLock m_stLock;
};

#endif
