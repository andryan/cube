/**
 * @file   YYLoginTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep 22 11:18:28 2011
 * 
 * @brief  yy客户端登陆连接对象
 * 
 * 
 */


#include "yauthLib.h"
#include "Global.h"
#include "YYLoginTask.h"
#include "YYUserManager.h"
#include "AccSyncClient.h"
#include "protocol/c2yyls_cmd.h"


MODI_YYLoginTask::MODI_YYLoginTask(const int sock, const struct sockaddr_in * addr):
	MODI_ServiceTask(sock, addr)
{
	/// 如果不是调试版本则增加ping和接收命令频率检查
#ifndef _DEBUG	
	SetEnableCheck();
	EnableCheckFlux();
#endif
	//	m_pSocket->SetEncryptType(MODI_Encrypt::ENCRYPT_TYPE_RC5);
}

	
MODI_YYLoginTask::~MODI_YYLoginTask()
{

}
 
/** 
 * @brief 回收
 * 
 * 
 * @return 返回1回收，返回0继续等待
 *
 */
int MODI_YYLoginTask::RecycleConn()
{
	return 1;
}



/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 接收到的命令
 * @param cmd_size 命令大小
 * 
 *
 */ 
bool MODI_YYLoginTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
#ifdef _DEBUG
	Global::logger->debug("[recv_cmd] recv cmd <cmd=%u,para=%u>", pt_null_cmd->byCmd, pt_null_cmd->byParam);
#endif
	
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
	{
		Terminate();
		return false;
	}

	if(pt_null_cmd->byCmd == 1 && pt_null_cmd->byParam == 1)
	{
		if(cmd_size != sizeof(MODI_C2YYLS_Request_Login))
		{
			Global::logger->warn("[cmd_size_error] request login cmd size error <cmd_size=%u,get_size=%u>", sizeof(MODI_C2YYLS_Request_Login), cmd_size);
			MODI_ASSERT(0);
			Terminate();
			return false;
		}
		std::string lpkey = YAuth_genRequest(m_pSocket->GetSock());
		if(lpkey.size() == 0 || lpkey.size() > 1000)
		{
			Global::logger->warn("[gen_auth] gen auth failed");
			MODI_ASSERT(0);
			Terminate();
			return false;
		}
		
#ifdef _DEBUG		
		Global::logger->debug("[gen_key] <%s>", lpkey.c_str());
#endif
		char buf[1024];
		memset(buf, 0, sizeof(buf));
		MODI_YYLS2C_Return_LoginKey * p_send_cmd = (MODI_YYLS2C_Return_LoginKey *)buf;
		AutoConstruct(p_send_cmd);
		p_send_cmd->m_dwKeyLen = lpkey.size();
		memcpy(p_send_cmd->m_pKey, lpkey.c_str(), lpkey.size());
		SendCmd(p_send_cmd, p_send_cmd->m_dwKeyLen + sizeof(MODI_YYLS2C_Return_LoginKey));
		return true;
	}
	else if(pt_null_cmd->byCmd == 1 && pt_null_cmd->byParam == 3)
	{
		if((unsigned int)cmd_size <= sizeof(MODI_C2YYLS_Return_LoginInfo))
		{
			Global::logger->warn("[cmd_size_error] return login info cmd size error <cmd_size=%u,get_size=%u>", sizeof(MODI_C2YYLS_Return_LoginInfo), cmd_size);
			MODI_ASSERT(0);
			Terminate();
			return false;
		}

		MODI_C2YYLS_Return_LoginInfo * p_recv_cmd = (MODI_C2YYLS_Return_LoginInfo *)pt_null_cmd;
		if((unsigned int)cmd_size != p_recv_cmd->m_dwInfoLen + sizeof(MODI_C2YYLS_Return_LoginInfo))
		{
			Global::logger->warn("[cmd_size_error] return login info cmd2 size error <cmd_size=%u,get_size=%u>",
								 sizeof(MODI_C2YYLS_Return_LoginInfo)+p_recv_cmd->m_dwInfoLen, cmd_size);
			MODI_ASSERT(0);
			Terminate();
			return false;
		}
		
		char info_buf[1024];
		memset(info_buf, 0, sizeof(info_buf));
		memcpy(info_buf, p_recv_cmd->m_pInfo, p_recv_cmd->m_dwInfoLen);
		int ret = YAuth_verifyToken(info_buf, m_pSocket->GetSock());

		std::string uid;
		std::string cardnum;

		char buff[1024];
		char outbuf[1024];
		memset(buff, 0, 1024);
		memset(outbuf, 0, 1024);
		YAuth_getAttribute(info_buf, "UserID", buff, 1023); ///取得YY的uid
		uid = buff;
			
		memset(buff, 0, 1024);
		memset(outbuf, 0, 1024);
		YAuth_getAttribute(info_buf, "IDCardNumber", buff, 1024);//取证件
		YAuth_fromHexToStr(buff, outbuf, 1024);
		cardnum = outbuf;

		Global::logger->debug("[recv_info] <uid=%s, cardnum=%s>", uid.c_str(), cardnum.c_str());

		/// send info service
		//#if 0		
		//if(! MODI_YYUserManager::GetInstance().IsYYUser(uid.c_str(), cardnum.c_str()))
		{
			Global::logger->debug("%s,%d", Global::g_strAccSyncIP.c_str(), Global::g_wdAccSyncPort);
			MODI_AccSyncClient send_client(uid.c_str(), cardnum.c_str());
			if(send_client.Init())
				send_client.SendSyncCmd();
		}
		//#endif		
		MODI_YYLS2C_Return_LoginResult send_cmd;
		send_cmd.m_iResult = ret;
		send_cmd.m_dwUserId = atoi(uid.c_str());
		SendCmd(&send_cmd, sizeof(send_cmd));
		return true;
	}
	
	return true;
}
