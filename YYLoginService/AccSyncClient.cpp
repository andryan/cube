/**
 * @file   AccSyncClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Sep 26 11:09:28 2011
 * 
 * @brief  同步yy到infoservice
 * 
 * 
 */

#include "Global.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "AccSyncClient.h"
#include "md5.h"
#include "XMLParser.h"
#include "base64.h"

/** 
 * @brief 初始化
 *
 * @return 成功true,失败false
 *
 */
bool MODI_AccSyncClient::Init()
{
	if(! MODI_PClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 发送校验命令
 * 
 */
void MODI_AccSyncClient::SendSyncCmd()
{
	///sync_account.jsp?account=ZjIwMDY0MTI0OQ==&idcard=142723199305220263&time_u=1317009436288&sn=fa8f64b59746bc36f4a2b62eb27fa61a&otype=null&oid=null HTTP/1.1
	
	std::ostringstream send_cmd;
	m_strEncodeAccName = base64_encode(m_strAccName.c_str(), m_strAccName.size());
	
	send_cmd << "GET /sync_account.jsp?account="
			 << m_strEncodeAccName
			 <<"&idcard=" << m_strCardNum
			 <<"&time_u=1317009436288&sn="
			 <<MakeMd5() << "&otype=YY&oid="
			 <<m_strAccName << " Http/1.1\r\n";
#ifdef _HRX_DEBUG
	Global::logger->debug("-->>send a command =%s", send_cmd.str().c_str());
#endif
	
	SendCmd(send_cmd.str().c_str(), send_cmd.str().size());

	Terminate();
}


const char *  MODI_AccSyncClient::MakeMd5()
{
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;
	//sn=md5(base64_encode(account)+KEY+idcard+time_u)
	str_md5<< m_strEncodeAccName  << "qwerads" << m_strCardNum << "1317009436288";
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("md5_src_str=%s", str_md5.str().c_str());
#endif
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	return recv_check_str.c_str();
}
