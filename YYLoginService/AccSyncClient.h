/**
 * @file   AccSyncClient.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Sep 26 11:01:10 2011
 * 
 * @brief  把yy同步infoservice
 * 
 * 
 */


#ifndef _MD_ACCSYNCCLIENT_H
#define _MD_ACCSYNCCLIENT_H

#include "PClientTask.h"


/**
 * @brief 登录校验连接
 * 
 */
class MODI_AccSyncClient: public MODI_PClientTask
{
 public:
	
	MODI_AccSyncClient(const char * acc_name, const char * card_num): MODI_PClientTask(Global::g_strAccSyncIP.c_str(), Global::g_wdAccSyncPort)
		, m_strAccName(acc_name), m_strCardNum(card_num)
		//MODI_PClientTask("192.168.2.208", 80)
	{
		
	}

	bool CmdParse(const Cmd::stNullCmd*, int)
	{
		return true;
	}

	bool SendCmd(const char * cmd, const unsigned int cmd_size)
	{
		if(m_pSocket)
		{
			m_pSocket->SendCmdNoPacket(cmd, cmd_size,false);
			return true;
		}
		return false;
	}

	bool Init();

	void SendSyncCmd();
	
	int RecycleConn()
	{
		return 1;
	}
	
 private:
	const char *  MakeMd5();
	std::string m_strAccName;
	std::string m_strEncodeAccName;
	std::string m_strCardNum;
};

#endif
