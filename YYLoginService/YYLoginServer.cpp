/**
 * @file   YYLoginServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Sep 21 18:36:34 2011
 * 
 * @brief  YY验证服务器
 * 
 * 
 */

#include "yauthLib.h"
#include "Global.h"
#include "Share.h"
#include "ServerConfig.h"
#include "YYLoginService.h"


/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool Init(int argc, char * argv[])
{
	
	/// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	/// 初始化日志对象
	Global::logger = new MODI_Logger("yylogin");
	Global::net_logger = Global::logger;

	if(!YAuth_init())
	{
		Global::logger->fatal("[system_init] unable init yauth");
		return 0;
	}
	
	/// 加载配置文件
	if(!MODI_ServerConfig::GetInstance().Load(Global::Value["config_path"].c_str()))
	{
		Global::logger->fatal("[load_config] yyloginservice load config fialed");
		return false;
	}

	const MODI_SvrYYLoginConfig  * pYYLoginConfig = (const MODI_SvrYYLoginConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_YYLOGINSERVICE ));
	if( !pYYLoginConfig )
	{
		Global::logger->debug("not get config yyloginservice");
		return false;
	}
	
	/// 增加一个日志文件到指定目录
    Global::logger->AddLocalFileLog(pYYLoginConfig->m_strLogPath.c_str());
	Global::logger->info("[system_init] <<<<<<------congratulation yyloginservice initialization successful------>>>>>>");
	return true;
}


/** 
 * @brief 结束，释放资源
 * 
 */
void Final()
{
 	if(Global::logger)
	{
 		delete Global::logger;
	}
 	Global::logger = NULL;
}


int main(int argc, char * argv[])
{
	if(! Init(argc, argv))
	{
		Global::logger->fatal("[system_init] yyloginservice initializtion faield");
		return 0;
	}

	/// 临时
	Global::logger->RemoveConsoleLog();

	MODI_YYLoginService::GetInstance().Main();
	Global::logger->info("[system_run] <<<<<<------yyloginservice terminate------>>>>>>");
	
	Final();
}

