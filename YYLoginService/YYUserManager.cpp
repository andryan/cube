/**
 * @file   YYUserManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Sep 26 11:21:15 2011
 * 
 * @brief  yy 用户缓冲
 * 
 * 
 */



#include "YYUserManager.h"

MODI_YYUserManager * MODI_YYUserManager::m_pInstance = NULL;

/** 
 * @brief 增加一个任务
 * 
 */
void MODI_YYUserManager::AddUser(const char * acc_name, const char * card_num)
{
	//m_stRWLock.wrlock();
	m_stTaskMap.insert(defYYUserMapValue(acc_name, card_num));
	//	m_stRWLock.unlock();
}


bool MODI_YYUserManager::IsYYUser(const char * acc_name, const char * card_num)
{
	m_stRWLock.wrlock();
	defYYUserMapIter iter = m_stTaskMap.find(acc_name);
	if(iter != m_stTaskMap.end())
	{
		if(iter->second == card_num)
		{
			m_stRWLock.unlock();
			return true;
		}
		m_stRWLock.unlock();
		return false;
	}
	AddUser(acc_name, card_num);
	m_stRWLock.unlock();
	return false;
}

