/**
 * @file   YYLoginTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep 22 11:17:11 2011
 * 
 * @brief  yy客户端登陆连接对象 
 * 
 * 
 */


#ifndef _MDYYLOGINTASK_H_
#define _MDYYLOGINTASK_H_

#include "ServiceTask.h"
#include "CommandQueue.h"


/**
 * @brief 网关连接类
 *
 */
class MODI_YYLoginTask : public MODI_ServiceTask//,public MODI_CmdParse
{
 public:

	MODI_YYLoginTask(const int sock, const struct sockaddr_in * addr);

	~MODI_YYLoginTask();


	/** 
	 * @brief 队列消息处理
	 * 
	 * @param pt_null_cmd 接收到的消息
	 * @param cmd_size 消息大小
	 * 
	 * @return 成功true,处理失败false
	 */
	/*	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);*/


	/** 
	 * @brief 消息处理
	 * 
	 * @param pt_null_cmd 要处理的消息
	 * @param cmd_size 消息大小
	 * 
	 * @return 成功true,失败false
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);


	/** 
	 * @brief回收连接
	 * 
	 * @return 能回收返回1,等待返回0
	 *
	 */
	int RecycleConn();
};

#endif
