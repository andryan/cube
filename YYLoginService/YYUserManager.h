/**
 * @file   YYUserManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Sep 26 11:15:58 2011
 * 
 * @brief  yy user manager
 * 
 * 
 */



#ifndef _MDYYUSERMANAGER_H
#define _MDYYUSERMANAGER_H

#include "Global.h"
#include "RWLock.h"


/**
 * @brief 区连接管理
 *
 */
class MODI_YYUserManager
{
 public:
	typedef __gnu_cxx::hash_map<std::string, std::string, str_hash> defYYUserMap;
	typedef __gnu_cxx::hash_map<std::string, std::string, str_hash>::iterator defYYUserMapIter;
	typedef __gnu_cxx::hash_map<std::string, std::string, str_hash>::value_type defYYUserMapValue;
	
	static MODI_YYUserManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_YYUserManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}


	bool IsYYUser(const char * acc_name, const char * card_num);
	
 private:
	void AddUser(const char * acc_name, const char * card_num);
	
	static MODI_YYUserManager * m_pInstance;

	defYYUserMap  m_stTaskMap;

	MODI_RWLock m_stRWLock;
};

#endif
