/**
 * @file   YYLoginService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep 22 11:33:06 2011
 * 
 * @brief  yylogin 登陆服务器
 * 
 * 
 */


#ifndef _MDYYLOGINSERVICE_H
#define _MDYYLOGINSERVICE_H

#include "ServiceTaskSched.h"
#include "NetService.h"


/**
 * @brief YYLogin服务器 
 *
 */
class MODI_YYLoginService: public MODI_NetService 
{
 public:

	/** 
	 * @brief 初始化
	 * 
	 * 
	 */
	bool Init();

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();

	/** 
	 * @brief 创建一个新的网关连接
	 * 
	 * @param sock 
	 * @param addr 
	 * 
	 * @return 
	 */
	bool CreateTask(const int sock, const struct sockaddr_in * addr);

	static MODI_YYLoginService & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_YYLoginService();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	MODI_YYLoginService(): MODI_NetService("YYLoginSevice"), m_stTaskSched(6, 1){}

	~MODI_YYLoginService(){}
	
 private:
	/// 连接轮询
	MODI_ServiceTaskSched m_stTaskSched;

	/// 实体
	static MODI_YYLoginService * m_pInstance;
};

#endif
