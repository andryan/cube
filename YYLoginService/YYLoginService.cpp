/**
 * @file   YYLoginService.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep 22 11:35:44 2011
 * 
 * @brief  yylogin service
 * 
 * 
 */


#include "Global.h"
#include "ServerConfig.h"
#include "YYLoginTask.h"
#include "YYLoginService.h"

MODI_YYLoginService * MODI_YYLoginService::m_pInstance = NULL;

/**
 * @brief 初始化
 *
 * @param 成功true,失败false
 *
 */
bool MODI_YYLoginService::Init()
{
	const MODI_SvrYYLoginConfig  * pYYLoginConfig = (const MODI_SvrYYLoginConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_YYLOGINSERVICE ));
	if( !pYYLoginConfig )
	{
		Global::logger->debug("not get config yyloginservice");
		return false;
	}

	Global::logger->fatal("[system_init] get yyloginservice <port=%d,ip=%s>", pYYLoginConfig->m_wdYYLoginPort, pYYLoginConfig->m_strYYLoginIP.c_str());
	
	// 网关帮定能够上外网的网卡
	if (!MODI_NetService::Init( pYYLoginConfig->m_wdYYLoginPort, pYYLoginConfig->m_strYYLoginIP.c_str() ) )
	{
		Global::logger->fatal("[system_init] faild init yyloginservice <port=%d,ip=%s>", pYYLoginConfig->m_wdYYLoginPort, pYYLoginConfig->m_strYYLoginIP.c_str());
		return false;
	}

	m_stTaskSched.Init();
	
	return true;
}


/**
 * @brief 新建一个连接 
 * @param sock socket描述符
 * @param addr 连接的地址
 * @return 成功true,失败false
 *
 */
bool MODI_YYLoginService::CreateTask(const int sock, const struct sockaddr_in * addr)
{
	if ((sock == -1) || (addr = NULL))
	{
		Global::net_logger->fatal("[create_task] yyloginservice create a new task failed<sock==-1||addr==null>");
		return false;
	}

	MODI_YYLoginTask *  p_task = new MODI_YYLoginTask(sock, addr);
	if(! m_stTaskSched.AddNormalSched(p_task))
	{
		TEMP_FAILURE_RETRY(close(sock));
		delete p_task;
		return false;
	}
	
	return true;
}


/**
 * @brief 释放资源
 *
 */
void MODI_YYLoginService::Final()
{
	m_stTaskSched.Final();
}

