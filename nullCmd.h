/**
 * @file Type.h
 * @date 2009-12-04 CST
 * @version $Id $
 * @author hurixin hrixin@modi.com
 * @brief 一些基本的声明
 *
 */

#ifndef MODI_NULLCMD_DEFINED_H_
#define MODI_NULLCMD_DEFINED_H_ 

#ifdef _MSC_VER
#pragma once 
#endif

namespace Cmd
{
#pragma pack(push,1)

	/// 空指令
	const unsigned char NULL_CMD = 0xfe;

	/// 空参数
	const unsigned char NULL_PARA = 0xfe;	   

	/**
	 * @brief 空指令定义
	 *
	 */
	struct stNullCmd
	{
		stNullCmd()
		{
			byCmd = NULL_CMD;
			byParam = NULL_PARA;
		}
		
		/// 指令类型
		unsigned char byCmd;
		/// 指令参数
		unsigned char byParam;				
	};
#pragma pack(pop)
};

template<typename buf_type>
inline void AutoConstruct(buf_type * ptr)
{
	new (static_cast<void *>(ptr)) buf_type();
}

#define GET_PACKAGE_SIZE( type , per_size , count ) \
	 (sizeof(type) + (per_size) * (count))

using Cmd::stNullCmd;

#ifdef _MSC_VER
#pragma warning(disable : 4200) // 关闭这个警告，让chat t[0]可以使用
#endif


#endif

