/**
 * @file   YYBillTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 12:19:53 2011
 * 
 * @brief  yy充值服务器
 * 
 * 
 */


#include <iostream>
#include "md5.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "FunctionTime.h"
#include "YYBillTask.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "base64.h"
#include "ZoneClientSched.h"
#include "YYBillTaskMgr.h"
#include "sessionid_creator.h"
#include "ServerConfig.h"

MODI_TableStruct * MODI_YYBillTask::m_pPayTable = NULL;
MODI_TableStruct * MODI_YYBillTask::m_pCharTable = NULL;

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_YYBillTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	return true;
}


/** 
 * @brief 发送命令
 * 
 * @param send_cmd 要发送的命令 
 * @param cmd_size 发送命令的大小不能超过64K
 * 
 * @return 成功true 失败false
 */
bool MODI_YYBillTask::SendCmd(const char * send_cmd, const unsigned int cmd_size)
{
	if((! m_pSocket) || (cmd_size > RET_BUF_LENGTH))
	{
		Global::net_logger->fatal("[sock_data] send cmd to large");
		return false;
	}
	m_pSocket->SendCmdNoPacket(send_cmd, cmd_size);
	return true;
}


/**
 * @brief 接收到数据处理
 *
 * @return  继续等待true 成功关闭连接false
 *
 */
bool MODI_YYBillTask::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > 1000)
		{
			Global::net_logger->fatal("%s:%d recv command more than 1000", GetIP(), GetPort());
			ResetRecvBuf();
			return false;
		}

		const std::string m_strRecvContent = m_stRecvBuf;
		std::string::size_type method_pos = m_strRecvContent.find_first_of(' ', 0);
		std::string method;
		if(method_pos == std::string::npos)
		{
			if(m_strRecvContent.size() > 5)
			{
				Global::logger->fatal("%s:%d get method error", GetIP(), GetPort());
				ResetRecvBuf();
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			method = m_strRecvContent.substr(0, method_pos);
		}
		Global::logger->debug("getcommand=%s",m_strRecvContent.c_str());

		if(method == "GET")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n", 0, 2);
			if(end_pos == std::string::npos)
			{
#ifdef _HRX_DEBUG				
				Global::net_logger->fatal("get command not get crlf");
#endif				
				return true;
			}
			
			std::string::size_type source_pos_end = m_strRecvContent.find_first_of(' ', method_pos+1);
			if(source_pos_end == std::string::npos)
			{
				Global::logger->fatal("not get source pos end");
				ResetRecvBuf();
				return false;
		}

			std::string get_source = m_strRecvContent.substr(method_pos+2, (source_pos_end - (method_pos+2)));
			
#ifdef _HRX_CUBE			
			Global::logger->debug("[recv_data] recv a command <content=%s,size=%d>", get_source.c_str(), get_source.size());
#endif

			if(! ParsePay(get_source.c_str()))
			{
				Global::logger->fatal("pay failed");
				return false;
			}
			// wait zone client answer
			
			//ResetRecvBuf();
			return true;
		}
		
		else if(method == "POST")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n\r\n", 0, 4);
			if(end_pos == std::string::npos)
			{
				Global::logger->debug("not get content");
			}
			std::string content = m_strRecvContent.substr(end_pos + 4, (m_strRecvContent.size() - (end_pos+4)));
			Global::logger->debug("-->>>>content=%s", content.c_str());
			if(! ParsePay(content.c_str()))
			{
				Global::logger->fatal("pay failed");
				return false;
			}
			//ResetRecvBuf();
			//return false;
			return true;
		}
		else
		{
			Global::logger->fatal("method failed");
		}
		return false;
	}
	return true;
}


/** 
 * @brief 冲值处理
 * 
 * @param cmd_line 要处理的命令
 */
bool MODI_YYBillTask::ParsePay(const char * cmd_line)
{
	std::string sync_cmd = cmd_line;
	std::string::size_type get_pos = sync_cmd.find_first_of('?', 0);
	if(get_pos == std::string::npos)
	{
		Global::logger->fatal("get ? failed");
		return false; 
	}
	
	std::string str_html = sync_cmd.substr(0, get_pos);
	if(str_html != "pay.jsp")
	{
		Global::logger->fatal("get pay.jsp html failed");
		return false;
	}

	std::string sync_content = sync_cmd.substr(get_pos+1, sync_cmd.size() - (get_pos+1));
	
	if( ! GetPayContent(sync_content.c_str()))
	{
		RetPayResult(-10);
		return false;
	}

	if(! CheckMD5())
	{
		RetPayResult(-11);
 		return false;
	}

	if(! DealPay())
	{
		return false;
	}

	return true;
	
}


/** 
 * @brief 分析冲值内容
 * 
 * @param pay_content 冲值内容
 */
bool MODI_YYBillTask::GetPayContent(const char * pay_content)
{
	MODI_SplitString deal_result;
	deal_result.Init(pay_content, "&");
	
	m_strOrderid = deal_result["orderid"];
	m_strEncodePlayer = deal_result["id"];
	m_strCurrency = deal_result["rmb"];
	//m_strGameCurrency = deal_result["game_currency"];
	m_strGameMoney = deal_result["coin"];
	//m_strRatio = deal_result["ratio"];
	std::string m_strAction = deal_result["type"];
	m_strTime = deal_result["time"];
	m_strSN = deal_result["verify"];
	m_strServerName = deal_result["server"];
	m_strGameName = deal_result["game"];
	m_strAccid = deal_result["id"];

	if(m_strOrderid == "" || m_strOrderid.size() > 30 ||
	   m_strEncodePlayer == "" ||
	   m_strSN == "" || m_strSN.size() > 32 || m_strTime == "" || m_strAction == "" ||
	   m_strServerName == "" || m_strGameName == "")
	   {
		   Global::logger->fatal("[pay_content] get pay content failed<orderid=%s,m_strEncodePlayer=%s,rmb=%s,coin=%s,m_strSN=%s,m_strTime=%s,m_strAction=%s>",
								 m_strOrderid.c_str(),m_strEncodePlayer.c_str(),m_strCurrency.c_str(), m_strGameMoney.c_str(),
								 m_strSN.c_str(), m_strTime.c_str(), m_strAction.c_str());
		   return false;
	   }


	//	m_strURLDecodePlayer = PublicFun::StrDecode(m_strEncodePlayer.c_str());
	//	m_byAction = atoi(m_strAction.c_str());
// 	if(m_byAction == 0 || m_byAction > 2)
// 	{
// 		Global::logger->fatal("[pay_content] get pay contend failed <action=%d>", m_byAction);
// 		return false;
// 	}
	
// 	unsigned char buf[m_strEncodePlayer.size() + 1];
// 	memset(buf, 0, sizeof(buf));
// 	base64_decode(buf, m_strURLDecodePlayer.c_str());
// 	m_strDecodePlayer = (const char *)buf;

#ifdef _HRX_DEBUG	
	Global::logger->debug("order_id=%s,player=%s,rmb=%s,coin=%s,time=%s,sn=%s,type=%s, uid=%s",
						  m_strOrderid.c_str(), m_strEncodePlayer.c_str(),
						  m_strCurrency.c_str(), 
						  m_strGameMoney.c_str(),
						  m_strTime.c_str(), m_strSN.c_str(),m_strAction.c_str(), m_strAccid.c_str());
#endif

	//	m_dwMoney = atoi(m_strGameCurrency.c_str()) + atoi(m_strGameMoney.c_str());
	return true;
}


/** 
 * @brief 签名校验
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_YYBillTask::CheckMD5()
{
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;

	//	std::string md5_player = PublicFun::StrDecode(m_strEncodePlayer.c_str());
	
	//Verify=MD5(id+name + orderid + rmb + coin + game+ server + 密钥)
	str_md5<<m_strAccid<<m_strOrderid<<m_strCurrency<<m_strGameMoney<<m_strGameName<<m_strServerName+"B86E6CEC7B999FC8A3D28805EB1CBB17VUOW8COI";
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("md5_src_str=%s", str_md5.str().c_str());
#endif
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	
	if(recv_check_str != m_strSN)
	{
		Global::logger->fatal("[check_md5] %s:%d md5 check failed(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
		return false;
	}
	Global::logger->fatal("[check_md5] %s:%d md5 check successful(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
	return true;
}


/** 
 * @brief 处理冲值
 * 
 * 
 * @return 成功true,失败false
 */
bool MODI_YYBillTask::DealPay()
{
	MODI_ZoneClientInPara in_para;
	in_para.m_stSessionID = m_SessionID;
	const MODI_SvrYYBillConfig  * pYYBillConfig = (const MODI_SvrYYBillConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_YYBILLSERVICE ));
	if( !pYYBillConfig )
	{
		Global::logger->debug("not get config yybillservice");
		return false;
	}
	
	std::map<std::string, std::string>::const_iterator iter = pYYBillConfig->m_ZoneAddrMap.find(m_strServerName);
	if(iter == pYYBillConfig->m_ZoneAddrMap.end())
	{
		RetPayResult(-10);
		return false;
	}

	std::string::size_type pos = iter->second.find_first_of(':', 0);
	if(pos == std::string::npos)
	{
		RetPayResult(-20);
		return false;
	}

	std::string ip = iter->second.substr(0, pos);
	std::string port = iter->second.substr(pos+1, iter->second.size() - (pos+1));

	strncpy(in_para.m_cstrIP, ip.c_str(), sizeof(in_para.m_cstrIP) - 1);
	in_para.m_wdPort = atoi(port.c_str());
	
	MODI_ZoneClientSched::GetInstance().ReqBill(in_para);
	return true;
}


/** 
 * @brief 返回冲值结果
 * 
 */
void MODI_YYBillTask::RetPayResult(int ret_code)
{
	std::ostringstream os;
	os<< "HTTP/1.1 200 OK\r\n"
	 	<<"Content-Type: text/html; charset=utf-8\r\n\r\n"
	  << ret_code <<"\r\n";
	
	SendCmd(os.str().c_str(), os.str().size());
	TerminateFinal();
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_YYBillTask::RecycleConn()
{
	if(IsTerminateFinal())
		return 1;
	return 0;
}



/** 
 * @brief 转换成md5
 * 
 * @param in_put 输入
 * 
 * @return 输出
 */
const char * MODI_YYBillTask::ToMD5(const char * in_put)
{
	if(!in_put)
	{
		std::string ret_str;
		return ret_str.c_str();
	}
	
	std::string in_str = in_put;
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(in_str.c_str()) , in_str.size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	return recv_check_str.c_str();
}



/** 
 * @brief 添加到管理中
 * 
 */
void MODI_YYBillTask::AddTaskToManager()
{
	if(MODI_SessionIDCreator::GetInstancePtr())
	{
		MODI_SessionIDCreator::GetInstancePtr()->Create( m_SessionID , GetIPNum() , GetPort());
	}
	else
	{
		MODI_ASSERT(0);
	}

	MODI_YYBillTaskMgr::GetInstance().AddBillTask(this);
}

/** 
 * @brief 从管理中删除
 * 
 */
void MODI_YYBillTask::RemoveTaskFromManager()
{
	MODI_YYBillTaskMgr::GetInstance().RemoveBillTask(this);
}
