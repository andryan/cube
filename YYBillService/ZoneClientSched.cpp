/**
 * @file   ZoneClientSched.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 14:49:52 2011
 * 
 * @brief  yy充值中转
 * 
 * 
 */


#include "ZoneClientSched.h"
#include "ZoneClientTick.h"

MODI_ZoneClientSched * MODI_ZoneClientSched::m_pInstance = NULL;


/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_ZoneClientSched::Init()
{
	if(! m_stClientSched.Init())
	{
		Global::logger->fatal("Unable initialization zone client sched");
		return false;
	}

	if(! MODI_ZoneClientTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization zone start");
		return false;
	}
	
	return true;
}

/** 
 * @brief 增加一个校验
 * @param p_client 校验
 *
 */
void MODI_ZoneClientSched::AddZoneClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.push_back(p_client);
	m_stRWLock.unlock();
}

/** 
 * @brief 移出一个校验
 * @param p_client 校验
 *
 */
void MODI_ZoneClientSched::RemoveZoneClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.remove(p_client);
	m_stRWLock.unlock();
}


/** 
 * @brief 账号校验
 * 
 * @param in_para 需要校验的参数
 * 
 */
bool MODI_ZoneClientSched::ReqBill(const MODI_ZoneClientInPara & in_para)
{
	/// 不阻塞调用线程,放到tick里面去做
	Put((Cmd::stNullCmd *) &in_para, sizeof(MODI_ZoneClientInPara));
	return true;
}

void MODI_ZoneClientSched::UpDate()
{

	if(m_ClientList.empty())
	{
		return;
	}

	m_stRWLock.rdlock();
	std::list<MODI_PClientTask * >::iterator next, iter;
	for(iter=m_ClientList.begin(), next=iter, next++; iter!=m_ClientList.end(); iter=next, next++)
	{
		MODI_ZoneClient * p_client = (MODI_ZoneClient *)(*iter);
		p_client->Get(1);
	}
	m_stRWLock.unlock();
}


bool MODI_ZoneClientSched::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ZoneClientInPara * in_para = (MODI_ZoneClientInPara *)pt_null_cmd;
	MODI_ZoneClient * p_client = new MODI_ZoneClient(*in_para);
	if(!p_client)
	{
		Global::logger->fatal("Unable new a zone client");
		return false;
	}
	if(p_client->Init())
	{
		m_stClientSched.AddNormalSched(p_client);
		p_client->SendBillCmd();
		return true;
	}
	delete p_client;
	return false;
}
