/**
 * @file   ZoneClientSched.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 14:44:21 2011
 * 
 * @brief  yy充值中转下
 * 
 * 
 */


#ifndef _MD_ZONECLIENTSCHED_H_
#define _MD_ZONECLIENTSCHED_H_

#include "ZoneClient.h"
#include "PClientTask.h"
#include "PClientTaskSched.h"
#include "CommandQueue.h"

/**
 * @brief 校验入口
 * 
 */
class MODI_ZoneClientSched: public MODI_CmdParse
{
 public:
	MODI_ZoneClientSched()
	{
		Resize(1024);
	}
	
	static MODI_ZoneClientSched & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_ZoneClientSched;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}


	bool Init();

	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	
	/** 
	 * @brief 请求充值
	 * 
	 * 
	 */
	bool ReqBill(const MODI_ZoneClientInPara & in_para);
	

	/** 
	 * @brief 更新
	 * 
	 */
	void UpDate();
	

	/** 
	 * @brief 增加一个校验
	 * @param p_client 校验
	 *
	 */
	void AddZoneClient(MODI_PClientTask * p_client);

	/** 
	 * @brief 移出一个校验
	 * 
	 * @param p_client 
	 */
	void RemoveZoneClient(MODI_PClientTask * p_client);

	
	
 private:
	/// 队列
	std::list< MODI_PClientTask * > m_ClientList;
	MODI_RWLock m_stRWLock;
	
	static MODI_ZoneClientSched * m_pInstance;
	MODI_PClientTaskSched m_stClientSched;
};

#endif
