/**
 * @file   ZoneClientTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 17:46:08 2011
 * 
 * @brief  YY充值中转
 * 
 * 
 */



#include "ZoneClientTick.h"
#include "Global.h"
#include "ZoneClientSched.h"

MODI_ZoneClientTick * MODI_ZoneClientTick::m_pInstance = NULL;

MODI_ZoneClientTick::MODI_ZoneClientTick() : MODI_Thread("zoneclienttick"), m_stCmdFluxTime(24 * 3600 * 1000)
{
	
}

MODI_ZoneClientTick::~MODI_ZoneClientTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_ZoneClientTick::Run()
{
    while(!IsTTerminate())
    {
		m_stRTime.GetNow();
    	::usleep(10000);
		MODI_ZoneClientSched::GetInstance().Get(1000);
		MODI_ZoneClientSched::GetInstance().UpDate();
    }
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_ZoneClientTick::Final()
{
	
}
