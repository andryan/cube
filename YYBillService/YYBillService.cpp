/**
 * @file   YYBillService.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 12:17:14 2011
 * 
 * @brief  yy充值服务器
 * 
 * 
 */



#include "Global.h"
#include "ServerConfig.h"
#include "YYBillTask.h"
#include "YYBillService.h"
#include "ZoneClientSched.h"

MODI_YYBillService * MODI_YYBillService::m_pInstance = NULL;

/**
 * @brief 初始化
 *
 * @param 成功true,失败false
 *
 */
bool MODI_YYBillService::Init()
{
	const MODI_SvrYYBillConfig  * pYYBillConfig = (const MODI_SvrYYBillConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_YYBILLSERVICE ));
	if( !pYYBillConfig )
	{
		Global::logger->debug("not get config yybillservice");
		return false;
	}

	Global::logger->fatal("[system_init] get yybillservice <port=%u,ip=%s>", pYYBillConfig->m_wdYYBillPort, pYYBillConfig->m_strYYBillIP.c_str());

	if (!MODI_NetService::Init( pYYBillConfig->m_wdYYBillPort, pYYBillConfig->m_strYYBillIP.c_str() ) )
	{
		Global::logger->fatal("[system_init] faild init yybillservice <port=%d,ip=%s>", pYYBillConfig->m_wdYYBillPort, pYYBillConfig->m_strYYBillIP.c_str());
		return false;
	}

	m_stTaskSched.Init();

	if(! MODI_ZoneClientSched::GetInstance().Init())
	{
		Global::logger->fatal("[system_init] failed init zone client ");
		return false;
	}
	
	return true;
}


/**
 * @brief 新建一个连接 
 * @param sock socket描述符
 * @param addr 连接的地址
 * @return 成功true,失败false
 *
 */
bool MODI_YYBillService::CreateTask(const int sock, const struct sockaddr_in * addr)
{
	if ((sock == -1) || (addr == NULL))
	{
		Global::net_logger->fatal("[create_task] yybillservice create a new task failed<sock==-1||addr==null>");
		return false;
	}

	std::string pay_ip = inet_ntoa(addr->sin_addr);
	if(!(pay_ip == "120.132.133.56" ||
		 pay_ip == "120.132.133.57" ||
		 pay_ip == "120.132.133.58" ||
		 pay_ip == "120.132.133.59" ||
		 pay_ip == "120.132.133.60" ||
		 pay_ip == "192.168.2.208" ||
		 pay_ip == "61.152.171.91" ||
		 pay_ip == "120.132.133.55" ||
		 pay_ip == "121.9.221.137"))
	{
		Global::logger->fatal("[pay_ip_error] <payip=%s,setip=%s>", pay_ip.c_str(), Global::g_strPayIP.c_str());
		TEMP_FAILURE_RETRY(close(sock));
		return false;
	}

	MODI_YYBillTask *  p_task = new MODI_YYBillTask(sock, addr);
	if(! m_stTaskSched.AddNormalSched(p_task))
	{
		TEMP_FAILURE_RETRY(close(sock));
		delete p_task;
		return false;
	}
	
	return true;
}


/**
 * @brief 释放资源
 *
 */
void MODI_YYBillService::Final()
{
	m_stTaskSched.Final();
}

