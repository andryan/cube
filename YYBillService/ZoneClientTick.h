/**
 * @file   ZoneClientTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 17:34:10 2011
 * 
 * @brief  YY充值中转
 * 
 * 
 */



#ifndef _MDZONECLIENTTICK_H
#define _MDZONECLIENTTICK_H

#include "Timer.h"
#include "Thread.h"

class MODI_ZoneClientTick: public MODI_Thread 
{
public:
    MODI_ZoneClientTick();
	
    virtual ~MODI_ZoneClientTick();

    void Final();

    virtual void Run();

	static MODI_ZoneClientTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_ZoneClientTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
private:
	
	static MODI_ZoneClientTick * m_pInstance;
	MODI_RTime m_stRTime;
	MODI_Timer m_stCmdFluxTime;
};

#endif
