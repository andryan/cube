/**
 * @file   YYBillTaskMgr.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Sep 28 15:27:27 2011
 * 
 * @brief  yy 充值连接管理
 * 
 * 
 */

#include "YYBillTaskMgr.h"

MODI_YYBillTaskMgr * MODI_YYBillTaskMgr::m_pInstance = NULL;

/** 
 * @brief 增加一个连接
 * 
 */
void MODI_YYBillTaskMgr::AddBillTask(MODI_YYBillTask * p_task)
{
	m_stLock.wrlock();
	m_stTaskMap.insert(defTaskMapValue(p_task->GetSessionID(), p_task));
	m_stLock.unlock();
}



/**
 * @brief 删除一个连接
 * 
 */
void MODI_YYBillTaskMgr::RemoveBillTask(MODI_YYBillTask * p_task)
{
	m_stLock.wrlock();
	defTaskMapIter iter = m_stTaskMap.find(p_task->GetSessionID());
	if(iter != m_stTaskMap.end())
	{
		m_stTaskMap.erase(iter);
	}
	m_stLock.unlock();
}




/** 
 * @brief 查找一个连接
 * 
 */
MODI_YYBillTask *  MODI_YYBillTaskMgr::FindBillTask(const MODI_SessionID & session_id)
{
	MODI_YYBillTask * ret_task = NULL;
	m_stLock.wrlock();
	defTaskMapIter iter = m_stTaskMap.find(session_id);
	if(iter != m_stTaskMap.end())
	{
		ret_task = iter->second;
	}
	m_stLock.unlock();

	return ret_task;
}


