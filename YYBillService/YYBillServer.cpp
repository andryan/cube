/**
 * @file   YYBillServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 12:08:32 2011
 * 
 * @brief  yy充值入口
 * 
 * 
 */


#include "Global.h"
#include "Share.h"
#include "ServerConfig.h"
#include "YYBillService.h"
#include "DBClient.h"
#include "YYBillTask.h"


/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 *
 */
bool Init(int argc, char * argv[])
{
	/// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	/// 初始化日志对象
	Global::logger = new MODI_Logger("yybill");
	Global::net_logger = Global::logger;
	
	/// 加载配置文件
	if(!MODI_ServerConfig::GetInstance().Load(Global::Value["config_path"].c_str()))
	{
		Global::logger->fatal("[load_config] yybillservice load config fialed");
		return false;
	}

	const MODI_SvrYYBillConfig  * pBillConfig = (const MODI_SvrYYBillConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_YYBILLSERVICE ));
	if( !pBillConfig )
	{
		Global::logger->debug("not get config yybillservice");
		return false;
	}
	
	/// 增加一个日志文件到指定目录
	std::string net_log_path_bill = pBillConfig->m_strLogPath;
    Global::logger->AddLocalFileLog(pBillConfig->m_strLogPath.c_str());
	
	return true;
}


/** 
 * @brief 结束
 * 
 */
void Final()
{
	MODI_YYBillService::GetInstance().DelInstance();
	if(Global::logger)
	{
 		delete Global::logger;
	}
 	Global::logger = NULL;
}



int main(int argc, char * argv[])
{
	if(! Init(argc, argv))
	{
		Global::logger->fatal("[system_init] yybillservice initializtion faield");
		return 0;
	}

	/// 临时
	Global::logger->RemoveConsoleLog();

	MODI_YYBillService::GetInstance().Main();

	Global::logger->info("[system_run] <<<<<<------yybillservice terminate------>>>>>>");
	Final();
	return 0;
}

