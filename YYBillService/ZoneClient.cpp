/**
 * @file   ZoneClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 15:37:16 2011
 * 
 * @brief  yy充值中转
 * 
 * 
 */


#include "Global.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "ZoneClient.h"
#include "ZoneClientSched.h"
#include "md5.h"
#include "XMLParser.h"
#include "base64.h"
#include "YYBillTaskMgr.h"

/** 
 * @brief 初始化
 *
 * @return 成功true,失败false
 *
 */
bool MODI_ZoneClient::Init()
{
	if(! MODI_PClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 发送校验命令
 * 
 */
void MODI_ZoneClient::SendBillCmd()
{
	MODI_YYBillTask * p_task = MODI_YYBillTaskMgr::GetInstance().FindBillTask(m_stSessionID);
	if(!p_task)
	{
		MODI_ASSERT(0);
		Global::logger->fatal("[find_yybill_task] not find yybill task <%s>", m_stSessionID.ToString());
		return;
	}
	
	std::ostringstream send_cmd;
	std::string player = base64_encode(p_task->m_strEncodePlayer.c_str(),p_task->m_strEncodePlayer.size());
	std::ostringstream os;
	os<<player
	  <<p_task->m_strOrderid
	  <<"12345600"
	  <<p_task->m_strGameMoney
	  <<"0"
	  <<p_task->m_strTime;

	Global::logger->debug("md5_str=%s", os.str().c_str());
	std::string auth_str = MakeMd5(os.str().c_str());
	
	std::ostringstream send_content;
	send_content<< "player=" <<player << "&order_id=" << p_task->m_strOrderid <<"&currency=0&game_currency=0&game_money=" << p_task->m_strGameMoney
				<<"&ratio=0&order_time_u=" << p_task->m_strTime << "&action=3&sn=" <<auth_str;
	
	send_cmd << "POST /login HTTP/1.1\r\n"
			 <<"Content-Type: application/x-www-form-urlencoded\r\n"
			 <<"Content-Length: "<< send_content.str().size() <<"\r\n"
			 <<"Connection: Keep-Alive\r\n\r\n"
			 <<send_content.str();

#ifdef _HRX_DEBUG
	Global::logger->debug("-->>send a command =%s", send_cmd.str().c_str());
#endif
	
	SendCmd(send_cmd.str().c_str(), send_cmd.str().size());
}


/**
 * @brief 校验
 * 
 */
bool MODI_ZoneClient::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > 1000)
		{
			Global::logger->fatal("recv command more than 1000");
			ResetRecvBuf();
			return false;
		}
		
		const std::string m_strRecvContent = m_stRecvBuf;
		
#ifdef _HRX_DEBUG		
		Global::logger->debug(m_strRecvContent.c_str());
#endif

		std::string::size_type end_pos = m_strRecvContent.find_first_of("\r\n", 0);
		if(end_pos == std::string::npos)
		{
			Global::logger->debug("not get crlf");
			return true;
		}
		std::string str_answer;
		str_answer= m_strRecvContent.substr(0, end_pos);
		std::string::size_type answer_end_pos = str_answer.find_last_of(' ', str_answer.size());
		if(answer_end_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get answer");
#endif
			BillFailed();
			return false;
		}

		std::string ret_result = str_answer.substr(answer_end_pos+1, (str_answer.size() - answer_end_pos + 1));
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("ret_result=%s.", ret_result.c_str());
#endif

		if(ret_result != "OK")
		{
#ifndef _HRX_DEBUG			
			Global::logger->debug("answer not ok");
#endif
			BillFailed();
			return false;
		}

		std::string::size_type begin_content_pos = m_strRecvContent.find("\r\n\r\n", 0);
		if(begin_content_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get begin crlfcrlf");
#endif
			BillFailed();
			return false;
		}

		if(m_strRecvContent.size() < (begin_content_pos + 6))
		{
			BillFailed();
			return false;
		}
		std::string final_str_result = m_strRecvContent.substr(begin_content_pos+4, (m_strRecvContent.size() - (begin_content_pos+4)));
#ifdef _HRX_DEBUG
		Global::logger->debug("final_str_result=%s.", final_str_result.c_str());
#endif
		
		std::string final_result = final_str_result.substr(0,2);
		MODI_RetBillResult send_cmd;
		if(final_result == "1&")
		{
			send_cmd.m_iResult = 1;
		}
		else if(final_result == "-1")
		{
			send_cmd.m_iResult = -1;
		}
		else if(final_result == "-2")
		{
			send_cmd.m_iResult = -2;
		}
		else if(final_result == "-3")
		{
			send_cmd.m_iResult = -3;
		}
		else if(final_result == "-4")
		{
			send_cmd.m_iResult = -4;
		}
		else if(final_result == "-5")
		{
			send_cmd.m_iResult = -5;
		}
		Put(&send_cmd, sizeof(send_cmd));
		return true;
	}
	return true;
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 
 * @param cmd_size 
 * 
 * @return 
 */
bool MODI_ZoneClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if(!pt_null_cmd)
	{
		return false;
	}
	
	if(pt_null_cmd->byCmd == ACCOUNT_VERIFY_CMD && pt_null_cmd->byParam == ACCOUNT_VERIFY_PARA)
	{
		MODI_RetBillResult * recv_cmd = (MODI_RetBillResult *)pt_null_cmd;
		/// 命令处理
		RetBillResult(recv_cmd->m_iResult);
		Terminate();
		TerminateFinal();
		return true;
	}
	
	return false;
}


/** 
 * @brief 校验返回
 * 
 * @param out_para 返回结果
 * 
 */
void MODI_ZoneClient::RetBillResult(int result)
{
	MODI_YYBillTask * p_task = MODI_YYBillTaskMgr::GetInstance().FindBillTask(m_stSessionID);
	if(!p_task)
	{
		MODI_ASSERT(0);
		return;
	}

	if(result == 1)
	{
		p_task->RetPayResult(1);
	}
	else if(result == -2)
	{
		p_task->RetPayResult(-19);
	}
	else if(result == -4)
	{
		p_task->RetPayResult(-18);
	}
	else
	{
		p_task->RetPayResult(-20);
	}
	
	p_task->Terminate();
	
}


/** 
 * @brief 添加到管理中
 * 
 */
void MODI_ZoneClient::AddTaskToManager()
{
	MODI_ZoneClientSched::GetInstance().AddZoneClient(this);
}

/** 
 * @brief 从管理中删除
 * 
 */
void MODI_ZoneClient::RemoveTaskFromManager()
{
	MODI_ZoneClientSched::GetInstance().RemoveZoneClient(this);
}

/** 
 * @brief 超时
 * 
 */
void MODI_ZoneClient::TimeOut()
{
	MODI_RetBillResult send_cmd;
	send_cmd.m_iResult = -5;
	Put(&send_cmd, sizeof(send_cmd));
	//ResetRecvBuf();
	//TerminateFinal();
}


void MODI_ZoneClient::BillFailed()
{
	MODI_RetBillResult send_cmd;
	send_cmd.m_iResult = -5;
	Put(&send_cmd, sizeof(send_cmd));
	//ResetRecvBuf();	
}


const char * MODI_ZoneClient::MakeMd5(const char * inpara)
{
	unsigned char md5_out[17];
    memset(md5_out, 0 , sizeof(md5_out));
    char final_out[33];
    memset(final_out, 0, sizeof(final_out));

    MD5Context check_str;
    MD5Init( &check_str );
    MD5Update( &check_str , (const unsigned char*)(inpara) , strlen(inpara));
    MD5Final(md5_out , &check_str );
    Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
    transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
    return recv_check_str.c_str();
}
