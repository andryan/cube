/**
 * @file   YYBillService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep 22 11:33:06 2011
 * 
 * @brief  yy充值服务器
 * 
 * 
 */


#ifndef _MDYYBILLSERVICE_H
#define _MDYYBILLSERVICE_H

#include "ServiceTaskSched.h"
#include "NetService.h"
#include "sessionid_creator.h"

/**
 * @brief YYBill服务器 
 *
 */
class MODI_YYBillService: public MODI_NetService 
{
 public:

	/** 
	 * @brief 初始化
	 * 
	 * 
	 */
	bool Init();

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();

	/** 
	 * @brief 创建一个新的网关连接
	 * 
	 * @param sock 
	 * @param addr 
	 * 
	 * @return 
	 */
	bool CreateTask(const int sock, const struct sockaddr_in * addr);

	static MODI_YYBillService & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_YYBillService();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	MODI_YYBillService(): MODI_NetService("YYBillSevice"), m_stTaskSched(6, 1){}

	~MODI_YYBillService(){}
	
 private:
	/// 连接轮询
	MODI_ServiceTaskSched m_stTaskSched;

	/// 实体
	static MODI_YYBillService * m_pInstance;

	MODI_SessionIDCreator  m_SessionIDCreator;
};

#endif
