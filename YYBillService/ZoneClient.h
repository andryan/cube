/**
 * @file   ZoneClient.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Oct  9 15:33:18 2011
 * 
 * @brief  yy充值中转
 * 
 * 
 */



#ifndef _MD_ZONECLIENT_H
#define _MD_ZONECLIENT_H

#include "PClientTask.h"
#include "CommandQueue.h"

/// 校验返回命令
const BYTE ACCOUNT_VERIFY_CMD = 1;
const BYTE ACCOUNT_VERIFY_PARA = 1;
struct MODI_RetBillResult: public Cmd::stNullCmd
{
	MODI_RetBillResult()
	{
		byCmd = ACCOUNT_VERIFY_CMD;
		byParam = ACCOUNT_VERIFY_PARA;
		m_iResult = 0;
	}
	int m_iResult;
}__attribute__((packed));

/// 中转传递的参数
struct MODI_ZoneClientInPara: public Cmd::stNullCmd
{
	MODI_ZoneClientInPara()
	{
		memset(m_cstrIP, 0, sizeof(m_cstrIP));
		m_wdPort = 0;
	}
	
	MODI_SessionID m_stSessionID;
	char m_cstrIP[32];
	WORD m_wdPort;
};


/**
 * @brief 登录校验连接
 * 
 */
class MODI_ZoneClient: public MODI_PClientTask, public MODI_CmdParse
{
 public:
	
	MODI_ZoneClient(const MODI_ZoneClientInPara & in_para):
		MODI_PClientTask(in_para.m_cstrIP, in_para.m_wdPort)
	{
		ResetRecvBuf();
		m_stSessionID = in_para.m_stSessionID;
	}

	bool CmdParse(const Cmd::stNullCmd*, int)
	{
		return true;
	}

	bool RecvDataNoPoll();
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	bool SendCmd(const char * cmd, const unsigned int cmd_size)
	{
		if(m_pSocket)
		{
			m_pSocket->SendCmdNoPacket(cmd, cmd_size,false);
			return true;
		}
		return false;
	}

	bool Init();

	void SendBillCmd();

	int RecycleConn()
	{
		if(IsTerminateFinal())
			return 1;
		return 0;
	}

   	void AddTaskToManager();
	void RemoveTaskFromManager();
	void TimeOut();
	void BillFailed();
	const char * MakeMd5(const char * inpara);
	void RetBillResult(int result);
 private:
	void ResetRecvBuf()
	{
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}
	
	/// 接收缓冲
	char m_stRecvBuf[1024];

	/// 接收的个数
	unsigned int m_dwRecvCount;

	MODI_SessionID m_stSessionID;
};

#endif
