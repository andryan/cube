/**
 * @file   YYBillTaskMgr.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Sep 28 14:53:42 2011
 * 
 * @brief  yy充值连接管理
 * 
 * 
 */

#ifndef _MD_YYBILLTASKMGR_H_
#define _MD_YYBILLTASKMGR_H_

#include "YYBillTask.h"

class MODI_YYBillTaskMgr
{
 public:
	typedef std::map<MODI_SessionID, MODI_YYBillTask * > defTaskMap;
	typedef std::map<MODI_SessionID, MODI_YYBillTask * >::iterator defTaskMapIter;
	typedef std::map<MODI_SessionID, MODI_YYBillTask * >::value_type defTaskMapValue;
	
	MODI_YYBillTaskMgr(){}
	~MODI_YYBillTaskMgr(){}

	static MODI_YYBillTaskMgr & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_YYBillTaskMgr;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
			m_pInstance = NULL;
		}
	}

	void AddBillTask(MODI_YYBillTask * p_task);
	void RemoveBillTask(MODI_YYBillTask * p_task);
	MODI_YYBillTask *  FindBillTask(const MODI_SessionID & session_id);

 private:
	static MODI_YYBillTaskMgr * m_pInstance;
	defTaskMap m_stTaskMap;
	MODI_RWLock m_stLock;
};
#endif
