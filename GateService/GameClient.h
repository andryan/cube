
#ifndef _MD_GAMECLIENT_H
#define _MD_GAMECLIENT_H

#include "Global.h"
#include "ClientTask.h"
#include "CommandQueue.h"
#include "protocol/c2gs.h"
#include "gw2gs.h"

/**
 * @brief GameService连接
 * 
 */
class MODI_GameClient: public MODI_ClientTask, public MODI_CmdParse
{
public:
	MODI_GameClient(const char * name, const char * server_ip,const WORD & port): MODI_ClientTask(name, server_ip, port), m_strClientName(name)
	{
		Resize(GWTOGS_CMDSIZE);
	}

	static MODI_GameClient & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_GameClient("gameclient", Global::Value["gameservice_ip"].c_str(), atoi(Global::Value["gameservice_port"].c_str()));
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	/** 
	 * @brief 初始化
	 * 
	 * 
	 */
	bool Init();

	
	/** 
	 * @brief 发送命令
	 * 
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	
	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	
	/** 
	 * @brief 队列内的命令
	 * 
	 * @param pt_null_cmd 获取的命令
	 * @param dwCmdLen 命令长度
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);


	/** 
	 * @brief 增加旁观
	 * 
	 */
	void AddTransMissionHandler( MODI_GS2GW_AddTransMission * p_notify );


	/** 
	 * @brief 删除旁观
	 * 
	 */
	void DelTransMissionHandler( MODI_GS2GW_DelTransMission * p_notify );
	
 protected:
	virtual void Final();
	
 private:
	/// 实例
	static MODI_GameClient * m_pInstance;

	/// 释放资源
	virtual ~MODI_GameClient()
	{
		ClientFinal();
	}
	
	void ClientFinal();

	/// 线程名字
	std::string m_strClientName;
};

#endif
