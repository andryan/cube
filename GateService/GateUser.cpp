/**
 * @file   GateUser.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 18:48:22 2011
 * 
 * @brief  网关用户连接
 * 
 */

#include "Global.h"
#include "GateUser.h"
#include "protocol/c2gs_musiccmd.h"
#include "GateUserManager.h"

MODI_GateUser::MODI_GateUser(const MODI_SessionID & id ,MODI_GateTask * p_task): m_pGateTask(p_task),
		m_SessionID( id ),m_bBekick(false)
{
}


MODI_GateUser::~MODI_GateUser()
{
	m_pGateTask = NULL;
}


/** 
 * @brief 发送命令
 * 
 *
 */
bool MODI_GateUser::SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size, bool is_zip)
{
	if(m_pGateTask)
	{
		return m_pGateTask->SendCmd(pt_null_cmd, cmd_size, is_zip);
	}
	return false;
}


/** 
 * @brief 添加一个观战者
 * 
 */
void MODI_GateUser::AddSpector( const MODI_SessionID & id )
{
	if( id.IsInvaild() )
	{
		return ;
	}

	m_stRWLock.wrlock();
	m_setSpectors.insert( id );
	m_stRWLock.unlock();
	
#ifdef _HRX_DEBUG
	std::string strSelfIP = this->GetTask()->GetIP();
	Global::logger->debug("[%s] ip<%s> add a spector with sessionid = <%s> ." , LOGACT_DEBUG , strSelfIP.c_str() , id.ToString());
#endif		
}


/** 
 * @brief 删除一个观战者
 * 
 */
void MODI_GateUser::DelSpector( const MODI_SessionID & id )
{
	if( id.IsInvaild() )
	{
		return ;
	}

	std::string strSelfIP = this->GetTask()->GetIP();

	m_stRWLock.wrlock();
	m_setSpectors.erase( id );
	m_stRWLock.unlock();

#ifdef _HRX_DEBUG	
		Global::logger->debug("[%s] ip<%s> del a spector with sessionid = <%s> ." , LOGACT_DEBUG ,strSelfIP.c_str(),  id.ToString());
#endif
}


/** 
 * @brief 删除所有的听歌
 * 
 */
void MODI_GateUser::ClearSpector()
{
	m_setSpectors.clear();
}

/** 
 * @brief 把音乐数据转发给其他旁听者
 * 
 */
void MODI_GateUser::SendSpectors( const void * p , int cmd_size )
{
	m_stRWLock.rdlock();
	SET_SPECTORS_ITER itor = m_setSpectors.begin();
	while( itor != m_setSpectors.end() )
	{
		SET_SPECTORS_ITER itorNext = itor;
		itorNext ++;

		MODI_GateUser * pSpector = MODI_GateUserManager::GetInstance().GetGateUser(*itor);

		if( pSpector )
		{
			pSpector->SendCmd( (const Cmd::stNullCmd *)(p) , cmd_size , false);
		}
		itor = itorNext;
	}
	m_stRWLock.unlock();
}


/** 
 * @brief 设置用户的账号id
 * 
 */
void MODI_GateUser::SetAccountID(const defAccountID & nAccountID )
{
	m_nAccountID = nAccountID;
}


/** 
 * @brief 获取账号id
 * 
 * 
 */
defAccountID MODI_GateUser::GetAccountID() const
{
	return m_nAccountID;
}


const char * MODI_GateUser::GetAddress() 
{
	const char * szTemp = "unknow";
	if( m_pGateTask )
	{
		return m_pGateTask->GetIP();
	}
	return szTemp;
}


/** 
 * @brief 增加到用户管理
 * 
 */
void MODI_GateUser::AddUserToManager()
{
	MODI_GateUserManager::GetInstance().AddUser(this);
}


/** 
 * @brief 从管理中删除
 * 
 */
void MODI_GateUser::RemoveFromManager()
{
	MODI_GateUserManager::GetInstance().RemoveUser(this);
}
