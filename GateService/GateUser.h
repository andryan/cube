/** 
 * @file GateUser.h
 * @brief 
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-20
 */


#ifndef _MDGATEUSER_H
#define _MDGATEUSER_H

#include "Global.h"
#include "GateTask.h"
#include "protocol/gamedefine.h"

/**
 * @brief 网关用户
 *
 */
class MODI_GateUser: public MODI_DisableCopy
{
public:
	typedef std::set<MODI_SessionID> SET_SPECTORS;
	typedef SET_SPECTORS::iterator SET_SPECTORS_ITER;
	typedef SET_SPECTORS::const_iterator SET_SPECTORS_C_ITER;
	
	MODI_GateUser(const MODI_SessionID & id ,MODI_GateTask * p_task);

	~MODI_GateUser();


	/** 
	 * @brief 发送命令
	 * 
	 */
	bool SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size, bool is_zip = true);


	/** 
	 * @brief 获取连接
	 * 
	 */
	MODI_GateTask * GetTask()
	{
		return m_pGateTask;
	}
	

	/** 
	 * @brief 获取标识
	 * 
	 * 
	 */
	const MODI_SessionID & 	GetSessionID()
	{
		return m_SessionID;
	}

	/** 
	 * @brief 设置 是否 参战玩家
	 * 
	 */
	void SetIsPlayer( bool bSet )
	{
		m_bPlayer = bSet;
	}

	/** 
	 * @brief 是否参战玩家
	 * 
	 * 
	 */
	bool IsPlayer() const
	{
		return m_bPlayer;
	}

	/** 
	 * @brief 添加一个观战者
	 * 
	 */
	void AddSpector(const MODI_SessionID & id);


	/** 
	 * @brief 删除一个观战者
	 * 
	 */
	void DelSpector(const MODI_SessionID & id);
	
	/** 
	 * @brief 清空所有观战者
	 * 
	 */
	void ClearSpector();

	/** 
	 * @brief 广播给自己的听众
	 * 
	 */
	void SendSpectors(const void * p , int cmd_size );


	/** 
	 * @brief 用户账号id
	 * 
	 */
	void SetAccountID( const defAccountID & nAccountID );

	
	/** 
	 * @brief 获取账号id
	 * 
	 */
	defAccountID GetAccountID() const;


	/** 
	 * @brief 是否是gameservice 断开
	 * 
	 */
	bool IsBeKick() const 
	{
		return m_bBekick;
	}

	
	/** 
	 * @brief 是gameservice主动断开
	 * 
	 */
	void SetBekick( bool b )
	{
		m_bBekick = b;
	}

	
	const char * GetAddress() ;

	/** 
	 * @brief 增加到用户管理
	 * 
	 */
	void AddUserToManager();

	
	/** 
	 * @brief 从管理中删除
	 * 
	 */
	void RemoveFromManager();

 private:
	/// 对应的连接
	MODI_GateTask * m_pGateTask;

	///	会话ID
	MODI_SessionID m_SessionID;

	/// 账号id
	defAccountID m_nAccountID;

	/// 听众（观战)
	SET_SPECTORS m_setSpectors;

	/// 增加一个读写锁
	MODI_RWLock m_stRWLock;

	/// 是否参战者（唱歌的人）
	bool m_bPlayer;

	/// 是否是gameservic主动断开
	bool m_bBekick;
};

#endif
