/**
 * @file   GateUserManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 17:34:05 2011
 * 
 * @brief  网关用户管理
 * 
 * 
 */


#include "GateUserManager.h"

MODI_GateUserManager * MODI_GateUserManager::m_pInstance = NULL;

/** 
 * @brief 增加一个任务
 * 
 */
void MODI_GateUserManager::AddUser(MODI_GateUser * p_user)
{
	if(p_user)
	{
		m_stRWLock.wrlock();
		m_stTaskMap.insert(defGateUserMapValue(p_user->GetSessionID(), p_user));
		m_stRWLock.unlock();
	}
}

/** 
 * @brief 删除一个连接
 * 
 */
void MODI_GateUserManager::RemoveUser(MODI_GateUser * p_user)
{
	if(p_user)
	{
		m_stRWLock.wrlock();
		defGateUserMapIter iter = m_stTaskMap.find(p_user->GetSessionID());
		if(iter != m_stTaskMap.end())
		{
			m_stTaskMap.erase(iter);
		}
		m_stRWLock.unlock();
	}
}


/** 
 * @brief 获取一个gateuser
 * 
 * @param session_id user的session_id
 * 
 */
MODI_GateUser * MODI_GateUserManager::GetGateUser(const MODI_SessionID & session_id)
{
	m_stRWLock.rdlock();
	defGateUserMapIter iter = m_stTaskMap.find(session_id);
	if(iter != m_stTaskMap.end())
	{
		m_stRWLock.unlock();
		return iter->second;
	}
	m_stRWLock.unlock();
	return NULL;
}


/** 
 * @brief 执行所有用户的命令
 * 
 */
void MODI_GateUserManager::DealQueueCmd()
{
	m_stRWLock.rdlock();
	if(m_stTaskMap.size() == 0)
	{
		m_stRWLock.unlock();
		return;
	}
	
	defGateUserMapIter iter = m_stTaskMap.begin();
	for(; iter != m_stTaskMap.end(); iter++)
	{
		iter->second->GetTask()->Get(1000);
	}
	m_stRWLock.unlock();
}


/** 
 * @brief 频道广播
 * 
 */
void MODI_GateUserManager::Broadcast(const stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	m_stRWLock.rdlock();
	if(m_stTaskMap.size() == 0)
	{
		m_stRWLock.unlock();
		return;
	}
	
	defGateUserMapIter iter = m_stTaskMap.begin();
	for(; iter != m_stTaskMap.end(); iter++)
	{
		if(iter->second)
		{
			(iter->second)->SendCmd(pt_null_cmd, cmd_size);
		}
	}
	m_stRWLock.unlock();
}

