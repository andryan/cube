/**
 * @file   GameClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Mar  9 17:10:04 2011
 * 
 * @brief  GameService连接
 * 
 * 
 */

#include "AssertEx.h"
#include "GameClient.h"
#include "GateTask.h"
#include "GateUserManager.h"
#include "GateService.h"

MODI_GameClient * MODI_GameClient::m_pInstance = NULL;
 
/** 
 * @brief 初始化
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_GameClient::Init()
{
	if(! MODI_ClientTask::Init())
	{
		return false;
	}
	
	if(! Start())
	{
		return false;
	}
	
	return true;
}


/** 
 * @brief 释放
 * 
 */
void MODI_GameClient::ClientFinal()
{
	TTerminate();
	Join();
}


/** 
 * @brief 发送命令
 * 
 * @param pt_null_cmd 要发送的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_GameClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
/*	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}*/
	if(pt_null_cmd == NULL)
	{
		Global::logger->info("[ERROR_GC] Pointer NULL.");
		return false;
	}
	if(cmd_size == 0)
	{
		Global::logger->info("[ERROR_GC] Command size 0.");
		return false;
	}

	return MODI_ClientTask::SendCmd(pt_null_cmd, cmd_size);
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 
 */
bool MODI_GameClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd))))
    {
        return false;
    }
	
	//#ifdef _HRX_DEBUG
	//	Global::logger->debug("[recv_cmd] gameclient recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
	//#endif
	
	/// 直接转发到client
	if (pt_null_cmd->byCmd == MCMD_SVR_REDIRECTIONAL)
    {
        MODI_GS2GW_Redirectional * p_recv_cmd = (MODI_GS2GW_Redirectional *)(pt_null_cmd);
		MODI_GateUser * p_gate_user = MODI_GateUserManager::GetInstance().GetGateUser(p_recv_cmd->m_sessionID);
        if(p_gate_user)
        {
			/// 数据临时加密
			//           	const Cmd::stNullCmd * pTemp = (const Cmd::stNullCmd *)(&(p_recv_cmd->m_pData[0]));
// 			char * szContent =  &(p_recv_cmd->m_pData[0]);
// 			szContent += sizeof(Cmd::stNullCmd);
// 			xor_encrypt(szContent , p_recv_cmd->m_nSize - 2 ,  g_szPackageKey , 0);

            p_gate_user->SendCmd((Cmd::stNullCmd *)(&p_recv_cmd->m_pData[0]), p_recv_cmd->m_nSize);
            return true;
        }
    }

	else if(pt_null_cmd->byCmd == MCMD_GS2GW_BROADCAST)
	{
		MODI_GS2GW_Broadcast_Cmd * p_recv_cmd = (MODI_GS2GW_Broadcast_Cmd *)pt_null_cmd;
		if(p_recv_cmd->m_wdSessionSize > 0)
		{
			char * p_session_id = p_recv_cmd->m_pData + p_recv_cmd->m_wdCmdSize;
			MODI_SessionID * p_startadd = (MODI_SessionID *)p_session_id;
			
			for(WORD i=0; i<p_recv_cmd->m_wdSessionSize; i++)
			{
				MODI_GateUser * p_gate_user = MODI_GateUserManager::GetInstance().GetGateUser(*p_startadd);
				if(p_gate_user)
				{
					p_gate_user->SendCmd((Cmd::stNullCmd *)(p_recv_cmd->m_pData), p_recv_cmd->m_wdCmdSize);
#ifdef _DEBUG
					Global::logger->debug("[broad_cast] broad cast <accid=%u>", p_gate_user->GetAccountID());
#endif					
				}
				p_startadd++;
			}
		}
	}

	else
	{
		Put( pt_null_cmd , cmd_size );
	}
    return true;
}

/** 
 * @brief 队列内的命令
 * 
 * @param pt_null_cmd 获取的命令
 * @param dwCmdLen 命令长度
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_GameClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	if(pt_null_cmd == NULL || dwCmdLen < 2)
	{
		return false;
	}

	if(pt_null_cmd->byCmd == MCMD_GS2GW_MUSICTRANS) 
	{
		switch(pt_null_cmd->byParam)
		{
			/// 增加旁听记录
			case SCMD_GS2GW_ADDTRANSMISSION:
			{
				AddTransMissionHandler( (MODI_GS2GW_AddTransMission *)(pt_null_cmd));
			}
			break;
			
			/// 删除旁听记录
			case SCMD_GS2GW_DELTRANSMISSION:
			{
				DelTransMissionHandler( (MODI_GS2GW_DelTransMission *)(pt_null_cmd));
			}
			break;
			
			default:
				break;
		}
		return false;
	}

	/// 删除某个用户
	else if(pt_null_cmd->byCmd  == MCMD_GW2GS_SESSION)
	{
		MODI_GW2GS_DelSession * p_recv_cmd = (MODI_GW2GS_DelSession *)(pt_null_cmd);
		MODI_GateUser * p_gate_user = MODI_GateUserManager::GetInstance().GetGateUser(p_recv_cmd->m_sessionID);
		if(p_gate_user)
		{
			p_gate_user->SetBekick(true);
			p_gate_user->GetTask()->Terminate();
			return true;
		}
	}
	
	return true;	
}


/** 
 * @brief 删除某个用户的旁观
 * 
 */
void MODI_GameClient::DelTransMissionHandler( MODI_GS2GW_DelTransMission * p_notify )
{
	MODI_GateUser * p_srcUser =	MODI_GateUserManager::GetInstance().GetGateUser( p_notify->m_SrcSession);
	if( p_srcUser )
	{
		// count == 0 ,视作清空旁观命令
		if( !p_notify->m_nCount )
		{
			p_srcUser->ClearSpector();
			return ;
		}

		for( unsigned char n = 0; n < p_notify->m_nCount; n++ )
		{
			p_srcUser->DelSpector( p_notify->m_pDestSession[n]);
		}
	}
}


/** 
 * @brief 增加某个玩家的旁观
 * 
 */
void MODI_GameClient::AddTransMissionHandler( MODI_GS2GW_AddTransMission * p_notify)
{
	MODI_GateUser * p_srcUser =	MODI_GateUserManager::GetInstance().GetGateUser( p_notify->m_SrcSession );
	if( p_srcUser )
	{
		for( unsigned char n = 0; n < p_notify->m_nCount; n++ )
		{
			p_srcUser->AddSpector( p_notify->m_pDestSession[n] );
		}
	}
}


/**
 * @brief 结束
 *
 */
void MODI_GameClient::Final()
{
	MODI_GateService::GetInstance().Terminate();
}
