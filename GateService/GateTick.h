/**
 * @file   GateTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Mar  9 15:41:33 2011
 * 
 * @brief  网关逻辑线程
 * 
 * 
 */

#ifndef _MDGATETICK_H
#define _MDGATETICK_H

#include "Type.h"
#include "Timer.h"
#include "Thread.h"

class MODI_GateTick: public MODI_Thread 
{
public:
    MODI_GateTick();
	
    virtual ~MODI_GateTick();

	/** 
	 * @brief 初始化
	 * 
	 */
	bool Init();

	
	/** 
	 * @brief 释放资源
	 * 
	 */
    void Final();

	/** 
	 * @brief 主循环
	 * 
	 */
    virtual void Run();

	
	static MODI_GateTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_GateTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
private:
	/// 实例
	static MODI_GateTick * m_pInstance;

	void TickFinal()
	{
		TTerminate();
		Join();
	}
};

#endif

