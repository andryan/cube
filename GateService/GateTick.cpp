/**
 * @file   GateTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Mar  9 15:41:57 2011
 * 
 * @brief  网关逻辑线程
 * 
 * 
 */


#include "Global.h"
#include "GateTick.h"
#include "GameClient.h"
#include "GateUserManager.h"

MODI_GateTick * MODI_GateTick::m_pInstance = NULL;


MODI_GateTick::MODI_GateTick() : MODI_Thread("gatetick")
{
	
}

MODI_GateTick::~MODI_GateTick()
{
	TickFinal();
}

/** 
 * @brief 初始化
 * 
 * 
 */
bool MODI_GateTick::Init()
{
	return Start();
}


/**
 * @brief 主循环
 *
 */
void MODI_GateTick::Run()
{
	while(! IsTTerminate())
	{
		::usleep(10000);
		MODI_GameClient::GetInstance().Get(10000);
		MODI_GateUserManager::GetInstance().DealQueueCmd();
	}
	
    Final();
}


/**
 * @brief 释放资源
 *
 */
void MODI_GateTick::Final()
{
	
}


