/**
 * @file   GateServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar 15 16:39:17 2011
 * 
 * @brief  网关服务器
 * 
 * 
 */

#include "Global.h"
#include "Share.h"
#include "ServerConfig.h"
#include "GateService.h"
#include "GateTick.h"
#include "GameClient.h"

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool Init(int argc, char * argv[])
{
	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	/// 初始化日志对象
	Global::logger = new MODI_Logger("gate");
	Global::net_logger = Global::logger;
	
	/// 加载配置文件
	if(!MODI_ServerConfig::GetInstance().Load(Global::Value["config_path"].c_str()))
	{
		Global::logger->fatal("[load_config] gateservice load config fialed");
		return false;
	}

	const MODI_SvrGSConfig * pGSInfo = (const MODI_SvrGSConfig *)(MODI_ServerConfig::GetInstance().GetConfig(SVRCFG_CHANNEL));
	if( !pGSInfo )
	{
		Global::logger->fatal("[load_config] gateservice load config self failed");
		return false;
	}
	
	const MODI_SvrGSConfig::MODI_Info * pSelfChannelInfo = pGSInfo->GetInfoByID( GetChannelIDFromArgs() );
	if( !pSelfChannelInfo )
	{
		Global::logger->fatal("[load_config] gateservice load config self channel failed");
		return false;
	}
	
	Global::logger->AddLocalFileLog( pSelfChannelInfo->strGatewayLogPath.c_str());
	Global::Value["gameservice_ip"] = pSelfChannelInfo->strIP;
	std::ostringstream os ;
	os<< pSelfChannelInfo->nPort;
	Global::Value["gameservice_port"] = os.str();
	Global::logger->info("[system_init] <<<<<<------congratulation gateservice initialization successful------>>>>>>");
	return true;
}


/** 
 * @brief 结束，释放资源
 * 
 */
void Final()
{
	MODI_GateTick::GetInstance().DelInstance();
	MODI_GameClient::GetInstance().DelInstance();
 	if(Global::logger)
	{
 		delete Global::logger;
	}
 	Global::logger = NULL;
}


int main(int argc, char * argv[])
{
	if(! Init(argc, argv))
	{
		Global::logger->fatal("[system_init] gateservice initializtion faield");
		return 0;
	}

	/// 临时
	Global::logger->RemoveConsoleLog();

	/// game service connect
	if(! MODI_GameClient::GetInstance().Init())
	{
		Global::logger->fatal("[system_init] gameclient init failed <gameip=%s,gameport=%d>",
							  Global::Value["gameservice_ip"].c_str(), Global::Value["gameservice_port"].c_str());
		return 0;
	}

	/// gate tick thread
	if(! MODI_GateTick::GetInstance().Init())
	{
		Global::logger->fatal("[system_init] gate tick init failed");
		return 0;
	}

	MODI_GateService::GetInstance().Main();
	Global::logger->info("[system_run] <<<<<<------gateservice terminate------>>>>>>");
	
	Final();
}

