/**
 * @file   GateUserManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 17:32:29 2011
 * 
 * @brief  网关用户管理
 * 
 * 
 */


#ifndef _MDGATEUSERMANAGER_H
#define _MDGATEUSERMANAGER_H

#include "Global.h"
#include "RWLock.h"
#include "GateTask.h"
#include "GateUser.h"
#include "session_id.h"

/**
 * @brief 回调基类
 *
 */
struct MODI_GateUserCallBack
{
	virtual ~MODI_GateUserCallBack(){};
	virtual bool Done(MODI_GateUser * p_callback_task) = 0;
};

/**
 * @brief 区连接管理
 *
 */
class MODI_GateUserManager
{
 public:
	typedef std::map<MODI_SessionID, MODI_GateUser * > defGateUserMap;
	typedef std::map<MODI_SessionID, MODI_GateUser * >::iterator defGateUserMapIter;
	typedef std::map<MODI_SessionID, MODI_GateUser * >::value_type defGateUserMapValue;
	
	static MODI_GateUserManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_GateUserManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	void AddUser(MODI_GateUser * p_user);

	void RemoveUser(MODI_GateUser * p_user);

	MODI_GateUser * GetGateUser(const MODI_SessionID & session_id);

	const DWORD Size() 
	{
		m_stRWLock.rdlock();
		DWORD num = m_stTaskMap.size();
		m_stRWLock.unlock();
		return num;
	}

	void ExecAllTask(MODI_GateUserCallBack & callback)
	{
		if(m_stTaskMap.size() == 0)
			return;
		m_stRWLock.rdlock();
		defGateUserMapIter iter = m_stTaskMap.begin();
		for(; iter != m_stTaskMap.end(); iter++)
		{
			callback.Done(iter->second);
		}
		m_stRWLock.unlock();
	}

	/** 
	 * @brief 广播
	 * 
	 * @param pt_null_cmd 广播的命令
	 *
	 */
	void Broadcast(const stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	/** 
	 * @brief 执行所有用户的命令
	 * 
	 */
	void DealQueueCmd();
	
 private:
	
	static MODI_GateUserManager * m_pInstance;

	defGateUserMap  m_stTaskMap;

	MODI_RWLock m_stRWLock;
};

#endif
