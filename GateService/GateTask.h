/**
 * @file   GateTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 16:27:33 2011
 * 
 * @brief  网关连接
 * 
 * 
 */

#ifndef _MDGATETASK_H
#define _MDGATETASK_H

#include "ServiceTask.h"
#include "CommandQueue.h"

class MODI_GateUser;

/**
 * @brief 网关连接类
 *
 */
class MODI_GateTask : public MODI_ServiceTask, public MODI_CmdParse
{
 public:

	MODI_GateTask(const int sock, const struct sockaddr_in * addr);

	~MODI_GateTask();


	/** 
	 * @brief 队列消息处理
	 * 
	 * @param pt_null_cmd 接收到的消息
	 * @param cmd_size 消息大小
	 * 
	 * @return 成功true,处理失败false
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);


	/** 
	 * @brief 消息处理
	 * 
	 * @param pt_null_cmd 要处理的消息
	 * @param cmd_size 消息大小
	 * 
	 * @return 成功true,失败false
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);


	/** 
	 * @brief回收连接
	 * 
	 * @return 能回收返回1,等待返回0
	 *
	 */
	int RecycleConn();


	/** 
	 * @brief 获取一个对于的网关用户
	 * 
	 *
	 */
	MODI_GateUser * GetGateUser()
	{
		return m_pGateUser;
	}


	/** 
	 * @brief 设置对应的gateuser
	 * 
	 * @param p_user 
	 */
	void SetGateUser(MODI_GateUser * p_user)
	{
		m_pGateUser = p_user;
	}

	/** 
	 * @brief 增加到管理,通知gameservice增加一个用户
	 * 
	 */
	void AddTaskToManager();

	
	/** 
	 * @brief 删除一个连接,包括对应的用户
	 * 
	 */
	void RemoveFromManager();

 private:
	/// 用户
	MODI_GateUser * m_pGateUser;

	/// 回收状态
	enum enRecycleState
	{
		enRecycleBegin,
		enRecycleDelay,
		enRecycleEnd,
	};

	/// 回收状态
	enRecycleState m_enRecycleState;
};

#endif
