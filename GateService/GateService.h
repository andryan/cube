/**
 * @file   GateService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 16:24:04 2011
 * 
 * @brief  网关服务器
 * 
 */

#ifndef _MDGATESERVICE_H
#define _MDGATESERVICE_H

#include "sessionid_creator.h"
#include "ServiceTaskSched.h"
#include "NetService.h"


/**
 * @brief 网关服务器 
 *
 */
class MODI_GateService: public MODI_NetService 
{
 public:

	/** 
	 * @brief 初始化
	 * 
	 * 
	 */
	bool Init();

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();

	/** 
	 * @brief 创建一个新的网关连接
	 * 
	 * @param sock 
	 * @param addr 
	 * 
	 * @return 
	 */
	bool CreateTask(const int sock, const struct sockaddr_in * addr);

	static MODI_GateService & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_GateService();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	MODI_GateService(): MODI_NetService("GateSevice"), m_stTaskSched(6, 1){}

	~MODI_GateService(){}
	
 private:
	/// 连接轮询
	MODI_ServiceTaskSched m_stTaskSched;

	/// 实体
	static MODI_GateService * m_pInstance;

	MODI_SessionIDCreator  m_SessionIDCreator;
};

#endif
