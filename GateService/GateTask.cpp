/**
 * @file   GateTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar  8 16:51:54 2011
 * 
 * @brief  网关连接
 * 
 */

#include "Global.h"
#include "GateTask.h"
#include "GateUser.h"
#include "GameClient.h"
#include "sessionid_creator.h"
#include "protocol/c2gs.h"
#include "gw2gs.h"


/** 
 * @brief 增加到管理,通知gameservice增加一个用户
 * 
 */
void MODI_GateTask::AddTaskToManager()
{
	/// 用户增加到管理器
	MODI_SessionID sessionID;
	if(MODI_SessionIDCreator::GetInstancePtr())
	{
		MODI_SessionIDCreator::GetInstancePtr()->Create( sessionID , GetIPNum() , GetPort());
	}
	else
	{
		MODI_ASSERT(0);
	}
	
	MODI_GateUser * p_user = new MODI_GateUser(sessionID, this);
	SetGateUser(p_user);
	p_user->AddUserToManager();

	/// 通知增加一个用户
	MODI_GW2GS_AddSession notify;
   	notify.m_sessionID = sessionID;
	MODI_GameClient::GetInstance().SendCmd(&notify, sizeof(notify));
}


/** 
 * @brief 删除一个连接,包括对应的用户
 * 
 */
void MODI_GateTask::RemoveFromManager()
{
	if(m_pGateUser)
	{
		///  若不是GameService让其删除该session,则通知GameService
		//	if(m_pGateUser->IsBeKick() == false)
		{
			MODI_GW2GS_DelSession notify;
			notify.m_sessionID = m_pGateUser->GetSessionID();
			MODI_GameClient::GetInstance().SendCmd(&notify, sizeof(notify));
		}

		m_pGateUser->RemoveFromManager();
		delete m_pGateUser;
		m_pGateUser = NULL;
	}
}


MODI_GateTask::MODI_GateTask(const int sock, const struct sockaddr_in * addr):
	MODI_ServiceTask(sock, addr),m_pGateUser(NULL)
{
	/// 如果不是调试版本则增加ping和接收命令频率检查
#ifndef _DEBUG	
	SetEnableCheck();
	EnableCheckFlux();
#endif
	m_pSocket->SetEncryptType(MODI_Encrypt::ENCRYPT_TYPE_RC5);
	m_enRecycleState = enRecycleBegin;
}

	
MODI_GateTask::~MODI_GateTask()
{

}
 
/** 
 * @brief 回收
 * 
 * 
 * @return 返回1回收，返回0继续等待
 *
 */
int MODI_GateTask::RecycleConn()
{
	if(m_enRecycleState == enRecycleBegin)
	{
		m_enRecycleState = enRecycleDelay;
	}
	else if(m_enRecycleState == enRecycleDelay)
	{
		m_enRecycleState = enRecycleEnd;
	}
	else if(m_enRecycleState == enRecycleEnd)
	{
		return 1;
	}
	return 0;
}



/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 接收到的命令
 * @param cmd_size 命令大小
 * 
 *
 */ 
bool MODI_GateTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
	{
		Terminate();
		return false;
	}

	MODI_GateUser * pUser = GetGateUser();
    if (!pUser)
    {
		Global::logger->error("[find_gate_user] not find a gate user <ip=%s>", GetIP());
		Terminate();
        return false;
    }

	/// 音乐数据直接转发
	else if(pt_null_cmd->byCmd == MAINCMD_MUSIC  && pt_null_cmd->byParam == MODI_C2C_Notify_MusicData::ms_SubCmd)
	{
		if(cmd_size < (int)sizeof(MODI_C2C_Notify_MusicData))
		{
			Global::logger->error("[music_data] task send a music data size error <ip=%s,port=%d>", GetIP(), GetPort());
			Terminate();
			return false;
		}
		
		const MODI_C2C_Notify_MusicData * pMusicData = (const MODI_C2C_Notify_MusicData *)(pt_null_cmd);

		const unsigned int per_size = sizeof(char);
		unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2C_Notify_MusicData , per_size ,  pMusicData->m_nSize );
		if( nSafePackageSize != (unsigned int)cmd_size )
		{
			Global::logger->error("[music_data] task send a music data size error <ip=%s,port=%d>", GetIP(), GetPort());
			Terminate();
			return false;
		}	

		pUser->SendSpectors( pt_null_cmd , cmd_size);
	}

	/// 键盘数据直接转发
	else if(pt_null_cmd->byCmd == MAINCMD_MUSIC  && pt_null_cmd->byParam == MODI_C2C_Notify_KeyData::ms_SubCmd)
	{
		if(cmd_size != (int)sizeof(MODI_C2C_Notify_KeyData))
		{
			Global::logger->error("[key_data] task send a key data size error <ip=%s,port=%d>", GetIP(), GetPort());
			Terminate();
			return false;
		}
		pUser->SendSpectors( pt_null_cmd , cmd_size);
	}
	

	/// 其他命令逻辑线程统一处理
	else if(pt_null_cmd->byCmd > C2GS_CMD_BEGIN && pt_null_cmd->byCmd < C2GS_CMD_END)
	{
		if(Put( pt_null_cmd , cmd_size ) == 0)
		{
			Global::logger->fatal("[client_cmd] gatetask put command failed <ip=%s,port=%d> ", GetIP(), GetPort());
			Terminate();
			return false;
		}
	}
	else
	{
		Global::logger->error("[error_cmd] task send a invalid command <ip=%s,port=%u,byCmd=%d,byParam=%d>", GetIP(), GetPort(),
							  pt_null_cmd->byCmd, pt_null_cmd->byParam);
		Terminate();
        return false;
	}
	
	return true;
}

 
/** 
 * @brief 队列消息处理
 * 
 * @param pt_null_cmd 接收到的消息
 * @param cmd_size 消息大小
 * 
 * @return 成功true,处理失败false
 */
bool MODI_GateTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
    {
		Terminate();
        return false;
    }

    MODI_GateUser * pUser = GetGateUser();
    if (!pUser)
    {
        Global::logger->error("[find_gate_user] not find a gate user <ip=%s>", GetIP());
		Terminate();
        return false;
    }

	/// 发送命令到gameservcie
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GW2GS_Redirectional * send_cmd = (MODI_GW2GS_Redirectional *)buf;
	AutoConstruct(send_cmd);
	
	send_cmd->m_sessionID = pUser->GetSessionID();
	send_cmd->m_nSize = cmd_size;
	char * pTemp = &(send_cmd->m_pData[0]);
	memcpy(pTemp, pt_null_cmd, cmd_size);
	int send_size = cmd_size + sizeof(MODI_GW2GS_Redirectional);

	/// 临时加密
	// size_t ndebugsize = sizeof(Cmd::stNullCmd);
// 	MODI_ASSERT( ndebugsize == 2 );
// 	char * pContent = pTemp + ndebugsize;
// 	xor_decrypt(pContent , cmd_size - 2,  g_szPackageKey , 0);
	
	if(!MODI_GameClient::GetInstance().SendCmd(send_cmd, send_size))
	{
		Global::logger->error("[send_game_client] send a command to game client failed <ip=%s,prot=%d>",
				MODI_GameClient::GetInstance().GetServiceIP(),
				MODI_GameClient::GetInstance().GetServicePort());
		Terminate();
		return false;
	}
	
    return true;
}
