/**
 * @file cmdhelp
 * @date 2009/12/24
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief
 *		���ڰ��������ݰ�ṹ�������л����
 */

#ifndef MODI_CMD_HELP_H_
#define MODI_CMD_HELP_H_


#ifdef _MSC_VER
#pragma once 
#endif

#include "nullCmd.h"
#include "TStream/stream.h"


///	û���ֶεĽṹ
#define DECLARATION_PACKAGE_0( name , cmd , sub_cmd )		typedef struct tag##name : public stNullCmd\
{\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd); }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ) {  unsigned int n = s.UnUsed(); \
	if( s.Put( obj.byCmd ) && s.Put( obj.byParam ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if( s.Get( obj.byCmd ) && s.Get( obj.byParam ) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}



//	ֻ��1���ֶεĽṹ
#define DECLARATION_PACKAGE_1( name , cmd , sub_cmd  , v1type , v1 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd); }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if( s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if( s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��2���ֶεĽṹ
#define DECLARATION_PACKAGE_2( name , cmd , sub_cmd  , v1type , v1 , v2type , v2   )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd); }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ) {  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��3���ֶεĽṹ
#define DECLARATION_PACKAGE_3( name , cmd , sub_cmd , v1type , v1 , v2type , v2  ,  v3type , v3  )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd); }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}
///	ֻ��4���ֶεĽṹ
#define DECLARATION_PACKAGE_4( name , cmd , sub_cmd  ,  v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��5���ֶεĽṹ
#define DECLARATION_PACKAGE_5( name , cmd , sub_cmd , v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ))\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5))\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��6���ֶεĽṹ
#define DECLARATION_PACKAGE_6( name , cmd , sub_cmd ,  v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 , v6type , v6 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	v6type  v6;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ) && s.Put( obj.v6 ))\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5) && s.Get(obj.v6) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��7���ֶεĽṹ
#define DECLARATION_PACKAGE_7( name , cmd , sub_cmd  , v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 , v6type , v6 , v7type , v7 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	v6type  v6;\
	v7type  v7;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ) && s.Put( obj.v6 )&& s.Put( obj.v7 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(   s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5) && s.Get(obj.v6)&& s.Get(obj.v7) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��8���ֶεĽṹ
#define DECLARATION_PACKAGE_8( name , cmd , sub_cmd  , v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 , v6type , v6 , v7type , v7 , v8type , v8 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	v6type  v6;\
	v7type  v7;\
	v8type  v8;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ) && s.Put( obj.v6 )&& s.Put( obj.v7 ) && s.Put( obj.v8 ))\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(   s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5) && s.Get(obj.v6)&& s.Get(obj.v7)&& s.Get(obj.v8) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��9���ֶεĽṹ
#define DECLARATION_PACKAGE_9( name , cmd , sub_cmd  , v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 , v6type , v6 , v7type , v7 , v8type , v8 , v9type , v9 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	v6type  v6;\
	v7type  v7;\
	v8type  v8;\
	v9type  v9;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ) && s.Put( obj.v6 )&& s.Put( obj.v7 ) && s.Put( obj.v8 ) && s.Put( obj.v9 ))\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(   s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5) && s.Get(obj.v6)&& s.Get(obj.v7)&& s.Get(obj.v8)&&s.Get(obj.v9) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

///	ֻ��10���ֶεĽṹ
#define DECLARATION_PACKAGE_10( name , cmd , sub_cmd  , v1type , v1 , v2type , v2  ,  v3type , v3  ,  v4type , v4 , v5type , v5 , v6type , v6 , v7type , v7 , v8type , v8  , v9type , v9 , v10type , v10 )		typedef struct tag##name : public stNullCmd\
{\
	v1type	v1;\
	v2type  v2;\
	v3type  v3;\
	v4type  v4;\
	v5type  v5;\
	v6type  v6;\
	v7type  v7;\
	v8type  v8;\
	v9type  v9;\
	v10type v10;\
	tag##name() { byCmd = (cmd); byParam = (sub_cmd);  }\
}MODI_##name;\
inline unsigned int 	MODI_PackCmd( TSTREAM::CStream & s , const MODI_##name & obj ){  unsigned int n = s.UnUsed(); \
	if(  s.Put( obj.byCmd ) && s.Put( obj.byParam ) && s.Put( obj.v1 ) && s.Put( obj.v2 ) && s.Put( obj.v3 ) && s.Put( obj.v4 ) && s.Put( obj.v5 ) && s.Put( obj.v6 )&& s.Put( obj.v7 ) && s.Put( obj.v8 ) && s.Put( obj.v9 ) && s.Put( obj.v10 ) )\
	{ n = n - s.UnUsed(); }\
	else n = 0;\
	return n;\
}\
inline unsigned int 	MODI_UnpackCmd( TSTREAM::CStream & s ,  MODI_##name & obj ) { unsigned int n = s.UnRead();\
	if(  s.Get( obj.byCmd ) && s.Get( obj.byParam ) && s.Get( obj.v1 ) && s.Get( obj.v2 ) && s.Get(obj.v3) && s.Get(obj.v4) && s.Get(obj.v5) && s.Get(obj.v6)&& s.Get(obj.v7)&& s.Get(obj.v8)&&s.Get(obj.v9)&& s.Get(obj.v10) )\
	{ n = n - s.UnRead(); }\
	else n = 0;\
	return n;\
}

#endif
