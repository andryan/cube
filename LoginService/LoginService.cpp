#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "GameClientTask.h"
#include "LoginService.h"
#include "Global.h"
#include "ServerConfig.h"
#include "LoginUser.h"
#include "AssertEx.h"
#include "sessionid_creator.h"
#include "Channellist.h"

MODI_LoginService::MODI_LoginService(std::vector<WORD > &  vec, const std::vector<std::string>  & ip_vec, const char * name , const int count ):
MODI_MNetService(vec, ip_vec , name, count)
{

}

/// 创建新的连接
bool MODI_LoginService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	if(sock == -1 || addr == NULL)
	{
		return false;
	}
	
	if( this->m_PortVec.size() !=  LSSVR_PORTIDX_COUNT )
	{
		return false;
	}
	
	if(port == this->m_PortVec[LSSVR_PORTIDX_4_CLIENTS] )
	{
		Global::logger->info("[login_ip] add a login <ip=%s,port=%d>", inet_ntoa(addr->sin_addr), ntohs(addr->sin_port));


		// 有客户端连接上
		MODI_LoginUser * pUser = new MODI_LoginUser();
		MODI_GameClientTask * pUserTask = new MODI_GameClientTask( pUser , sock , addr );

		MODI_SessionID id;
		MODI_SessionIDCreator * pCreator = MODI_SessionIDCreator::GetInstancePtr();
		pCreator->Create( id  , pUserTask->GetIPNum() , pUserTask->GetPort() );
		pUser->SetSessionID( id );

		MODI_GameClientTask::ms_ProcessBase.OnAcceptConnection( pUserTask );

		//m_pTaskPools[LSSVR_PORTIDX_4_CLIENTS]->AddTask( pUserTask );
		m_stTaskSched[LSSVR_PORTIDX_4_CLIENTS].AddNormalSched(pUserTask);
		return true;
	}
//	else if(port == this->m_PortVec[LSSVR_PORTIDX_4_MGR] )
//	{
//		Global::logger->debug("add a ManagerServer task(%s,%d)", inet_ntoa(addr->sin_addr), ntohs(addr->sin_port));
//		return true;
//	}

	Global::logger->debug("[%s] login server createtask faild!" , SVR_TEST );

	TEMP_FAILURE_RETRY(::close(sock));
	MODI_ASSERT(0);

	return false;
}

void MODI_LoginService::Final()
{

}

bool MODI_LoginService::Init()
{
	if( MODI_MNetService::Init() )
	{
		for( int i = 0 ; i <LSSVR_PORTIDX_COUNT; i++ )
		{
			//MODI_ServiceTaskPoll * p = new MODI_ServiceTaskPoll();
			//m_pTaskPools[i] = p;
			//p->Start();
			m_stTaskSched[i].Init();
		}
			
		return true;
	}

	return false;
}

void MODI_LoginService::ReloadConfig()
{
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(! plist)
	{
		Global::net_logger->fatal("[reload_config] reload config failed <%s>", GetName());
		return;
	}

	if(!plist->Init("./Config/config.xml"))
	{
		Global::net_logger->fatal("[reload_config] reload config failed <%s>", GetName());
		return;
	}

	Global::net_logger->info("[reload_config] reload config successful <%s>", GetName());
}
