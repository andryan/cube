/** 
 * @file GameClientTask.h
 * @brief 游戏客户端的TASK类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-08
 */

#ifndef GAMECLIENT_TASK_H_
#define GAMECLIENT_TASK_H_

#include "ServiceTask.h"
#include "RecurisveMutex.h"
#include "CommandQueue.h"
#include <set>
#include "SvrPackageProcessBase.h"

class MODI_LoginUser;

class  MODI_GameClientTask : public MODI_IServerClientTask
{
	public:

		explicit MODI_GameClientTask( MODI_LoginUser * p ,const int sock, const struct sockaddr_in * addr);
		virtual ~MODI_GameClientTask();

	public:

		virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

		virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

		virtual int RecycleConn();

		MODI_LoginUser * GetUser()
		{
			return m_pLoginUser;
		}

		/// 设置用户
		void SetUser(MODI_LoginUser * p_user)
		{
			m_pLoginUser = p_user;
		}

		// 连接上的回调,可以安全的在逻辑线程调用
		virtual void OnConnection();

		// 断开的回调，可以安全的在逻辑线程调用
		virtual void OnDisconnection();

	public:


		/** 
		 * @brief 处理所有客户端的数据包
		 */
		static void 	ProcessAllPackage();

		static MODI_SvrPackageProcessBase 	ms_ProcessBase;

	 private:

		MODI_LoginUser * m_pLoginUser;

};


#endif
