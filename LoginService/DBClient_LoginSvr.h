/** 
 * @file DBClient_LoginSvr.h
 * @brief loginserver上的连接DB的客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-05
 */
#ifndef DBCLIENT_LOGINSVR_H_ 
#define DBCLIENT_LOGINSVR_H_ 

#include "ClientTask.h"
#include "SingleObject.h"
#include "CommandQueue.h"
#include "RecurisveMutex.h"

/** 
 * @brief 连接ACCOUNT DB 的客户端模型
 */
class MODI_DBClient_LoginSvr : public MODI_ClientTask  , 
	public CSingleObject<MODI_DBClient_LoginSvr> ,
	public MODI_CmdParse
{
public:

	MODI_DBClient_LoginSvr(const char * name, const char * server_ip,const WORD & port);

	~MODI_DBClient_LoginSvr();

	/// 发送命令
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

	/// 初始化
	virtual	bool Init();

 protected:

	virtual void Final();

};


#endif
