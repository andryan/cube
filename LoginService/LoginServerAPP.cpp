#include "LoginServerAPP.h"
#include "BinFileMgr.h"

MODI_LoginServerAPP::MODI_LoginServerAPP( const char * szAppName ):MODI_IAppFrame( szAppName ),
	m_pAccountDBClient(0),
	m_pManangerClient(0)
{


}


MODI_LoginServerAPP::~MODI_LoginServerAPP()
{


}

int MODI_LoginServerAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	// 获取相关配置信息
	const MODI_SvrLSConfig * pLSInfo = (const MODI_SvrLSConfig * )(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_LOGINSERVER ) );
	if( !pLSInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrADBConfig * pAccountDBInfo = (const MODI_SvrADBConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ACCOUNTSERVER ) );
	if( !pAccountDBInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrMSConfig * pManangerSvrInfo = (const MODI_SvrMSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_MANGGERSERVER ) );
	if( !pManangerSvrInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

    Global::logger->AddLocalFileLog(pLSInfo->strLogPath);
	std::string net_log_path = pLSInfo->strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();

	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(!plist)
		return MODI_IAppFrame::enConfigInvaild;
	
	if(! plist->Init("./Config/config.xml"))
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
	if( !pResInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	Global::logger->info("[%s]  Loading Bin Files ...", SYS_INIT);
	int iNoLoadMask = enBinFT_Music | enBinFT_Level | enBinFT_Item | enBinFT_Shop | enBinFT_Suit | enBinFT_Avatar | enBinFT_GoodsPackage;

    // 初始化bin mgr
    if (!MODI_BinFileMgr::GetInstancePtr()->Initial( pResInfo->strBinDir.c_str() , iNoLoadMask ) )
    {
        Global::logger->fatal("[%s] Fail to initialize BIN file manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
    }

	if( !m_DefaultCreator.Init() )
	{
        Global::logger->fatal("[%s] Fail to initialize Default Avatar Creator faild.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	if(	!m_Packagehandler_c2ls.Initialization()  || !m_Packagehandler_ls2s.Initialization() )
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	
	if( !m_DisableStrTable.Init( "./Data/DisableStringTable.txt" ) )
	{
        Global::logger->fatal("[%s] Fail to initialize Disable string table faild.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	// 初始化登入服务器的网络服务对象
	std::vector<WORD > port_vec;
	std::vector<std::string> ip_vec;
	port_vec.push_back(pLSInfo->nPort);
	ip_vec.push_back(pLSInfo->strIP.c_str());
	m_pNetService = new MODI_LoginService(port_vec, ip_vec);
	if( !m_pNetService )
		return MODI_IAppFrame::enNoMemory;

	// 创建连接ACCOUNT DB SERVER 的客户端对象
	unsigned short nAccountDBPort = pAccountDBInfo->nPort;
	m_pAccountDBClient = new MODI_DBClient_LoginSvr("AccountDB_Client" , pAccountDBInfo->strIP.c_str() , nAccountDBPort );
	Global::logger->debug("[%s] login server try connect to account db , address<%s:%u>" , 
			SVR_TEST , 
			pAccountDBInfo->strIP.c_str() ,
			nAccountDBPort );
	if( !m_pAccountDBClient )
		return MODI_IAppFrame::enNoMemory;

	// 创建连接 MANANGER SERVER 的客户端对象
	unsigned short nManangerPort = pManangerSvrInfo->nPort;
	m_pManangerClient = new MODI_ManangerClient("ManangerClient" , pManangerSvrInfo->strIP.c_str() , nManangerPort );
	Global::logger->debug("[%s] login server try connect to ManangerServer  , address<%s:%u>" , 
			SVR_TEST , 
			pManangerSvrInfo->strIP.c_str() ,
			nManangerPort );
	if( !m_pManangerClient )
		return MODI_IAppFrame::enNoMemory;

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_LoginSvrLogic();
	if( !m_pLogicThread )
		return MODI_IAppFrame::enNoMemory;

	return MODI_IAppFrame::enOK;
}


int MODI_LoginServerAPP::Run()
{

	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;

	if( m_pAccountDBClient )
	{
		Global::logger->info("[%s]try connect to account db server." , SYS_INIT );
		if( !m_pAccountDBClient->Init() )
		{
			Global::logger->info("[%s]connect to account db server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pAccountDBClient->Start();
			Global::logger->info("[%s]connect to account db server successful. " , SYS_INIT );
		}
	}
	else 
	{
		Global::logger->info("[%s]connect to account db server faild. not exist account db client. " , SYS_INIT );
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	if( m_pManangerClient )
	{
		Global::logger->info("[%s]try connect to mananger server." , SYS_INIT );
		if( !m_pManangerClient->Init() )
		{
			Global::logger->info("[%s]connect to mananger server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pManangerClient->Start();
			Global::logger->info("[%s]connect to mananger server successful. " , SYS_INIT );
		}
	}
	else 
	{
		Global::logger->info("[%s]connect to mananger  server faild. not exist mananger_server client. " , SYS_INIT );
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	sleep(2);
	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );

	MODI_S2MS_Request_ServerReg msg;
	msg.m_iServerType = SVR_TYPE_LS;
	m_pManangerClient->SendCmd( &msg , sizeof(msg) );

	if( m_pLogicThread )
	{
		// 启动逻辑线程
		m_pLogicThread->Start();
	}


	if( m_pNetService )
	{
		// start server for gameclients ..
		m_pNetService->Main();
	}

	Global::logger->debug("[%s] login server NetService shutdown!" , SVR_TEST );
	return MODI_IAppFrame::enOK;
}


int MODI_LoginServerAPP::Shutdown()
{
	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

	if( m_pAccountDBClient )
	{
		m_pAccountDBClient->TTerminate();
		m_pAccountDBClient->Join();
	}

	if( m_pManangerClient )
	{
		m_pManangerClient->TTerminate();
		m_pManangerClient->Join();
	}

	if( m_pNetService )
	{
		m_pLogicThread->TTerminate();
	}

	sleep(3);

	delete m_pLogicThread;

	delete m_pNetService;

	delete m_pAccountDBClient;

	delete m_pManangerClient;
	
	m_Channellist.Clear( true );

	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}


