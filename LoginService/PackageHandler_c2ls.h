/** 
 * @file PackageHandler_c2ls.h
 * @brief LS和游戏客户端的消息包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-09
 */
#ifndef PACKAGE_HANDLER_H_C2LS_
#define PACKAGE_HANDLER_H_C2LS_

#include "IPackageHandler.h"
#include "SingleObject.h"

class MODI_LoginUser;

/**
 * @brief 游LS和游戏客户端的消息包处理
 *
 */
class MODI_PackageHandler_c2ls : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_c2ls>
{
public:

    MODI_PackageHandler_c2ls();
    virtual ~MODI_PackageHandler_c2ls();

public:

	/// 登入请求
	static int RequestLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 处理创建角色的请求
	static int RequestCreateRole_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 选择频道的处理
	static int RequestSelChannel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

private:

    virtual bool FillPackageHandlerTable();
};

#endif

