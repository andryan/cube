/** 
 * @file LoginUser.h
 * @brief 登入服务器上的客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-06
 */

#ifndef LOGIN_USER_H_
#define LOGIN_USER_H_


#include "Type.h"
#include "protocol/gamedefine.h"
#include "Timer.h"

class MODI_GameClientTask;

/** 
 * @brief 登入服务器上的客户端
 * @note  不允许外部主动detelte MODI_LoginUser * 对象，由网络层删除TASK对象，从而触发 MODI_LoginUser 对象的释放
 */
class 	MODI_LoginUser : public MODI_DisableCopy
{
	friend class MODI_GameClientTask;

	enum
	{
		enWaittingClose, // 等待关闭状态
		enWaittingSendAP, // 等待客户端发送帐号密码的状态
		enWaittingWaitDBAuth, // 等待DB验证账号的状态
		enWaittingRoleData, // 等待角色DB发送过来的角色信息
		enWaittingCreateRole, // 处于等待创建角色的状态
		enWaittingCreateRoleResult, // 处于创建角色结果返回的状态
		enWaittingSelectGameChannnel, // 处于等待选择游戏频道的状态
	};

	public:

		MODI_LoginUser();
		~MODI_LoginUser();

		bool SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

		void 	SetAccount( const char * szAccount ) 
		{
			m_strAccount = szAccount;
		}

		const char * GetAccount() const
		{
			return m_strAccount.c_str();
		}

		void 	SetAccountID( defAccountID id )
		{
			m_nAccountID = id;
		}

		defAccountID 	GetAccountID() const
		{
			return m_nAccountID;
		}

		void 	SetLoginKey( const MODI_LoginKey & key ) 
		{
			m_Key = key;
		}

		const MODI_LoginKey & GetLoginKey() const
		{
			return m_Key;
		}

		unsigned long GetCreateTime() const
		{
			return m_nCreateTime;
		}

		unsigned int GetSyncChannellistCount() const
		{
			return m_nSyncChannellistCount;
		}

		void 	SetSyncChannellistCount( unsigned int n )
		{
			m_nSyncChannellistCount = n;
		}

		void SwitchStatusToWaitDBAuth();

		void SwitchStatusToWaitClose();

		void SwitchStatusToWaitRoleData();

		void SwitchStatusToWaitCreateRole();

		void SwitchStatusToWaitCreateRoleReuslt();

		void SwitchStatusToWaitSelectChannnel();

		bool IsInWaitSendAPStatus() const;

		bool IsInWaitDBAuthStatus() const;

		bool IsInWaitCloseStatus() const;

		bool IsInWaitRoleDataStatus() const;

		bool IsInWaitCreateRoleStatus() const;

		bool IsInWaitCreateRoleStatusResult() const;

		bool IsInWaitSelectChannel() const;

		bool IsWaitCloseTimeout();

		void Disconnection();

		bool IsHasCreateRoleStatus() const;

		MODI_GameClientTask * GetTask() 
		{
			return m_pTask;
		}

		const MODI_SessionID & 	GetSessionID() const
		{
			return m_SessionID;
		}

		void 	SetSessionID( const MODI_SessionID & id )
		{
			m_SessionID = id;
		}

		const char * GetMacAddr() const
		{
			return m_strMacAddr.c_str();
		}

		void 	SetMacAddr( const char * szMacAddr )
		{
			m_strMacAddr = szMacAddr;
		}


	private:

		void 	SetTask( MODI_GameClientTask * pTask );

	private:

		bool 			m_bHasCreateRoleStatus;
		unsigned long 	m_nCreateTime;
		std::string 	m_strAccount;
		defAccountID 	m_nAccountID;
		int 			m_iStatus;
		MODI_GameClientTask * m_pTask;
		MODI_LoginKey  	m_Key;
		unsigned int 	m_nSyncChannellistCount;
		MODI_SessionID  m_SessionID;
		MODI_Timer 		m_timerWaitClose;
		std::string 	m_strMacAddr;
};


#endif
