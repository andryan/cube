#include "LoginUserMgr.h"
#include "Global.h"

static	MODI_LoginUserMgr gs_LoginUserMgrInst;

bool StringLess::operator() ( const std::string & left , const std::string & right ) const
{
	if( left.size() < right.size() )
		return true;
	else if( left.size() > right.size() )
		return false;


	return strcmp( left.c_str() ,right.c_str() ) < 0;
}

MODI_LoginUserMgr::MODI_LoginUserMgr()
{

}

MODI_LoginUserMgr::~MODI_LoginUserMgr()
{

}

bool MODI_LoginUserMgr::AddUser( const MODI_SessionID & nUserID , MODI_LoginUser * pUser )
{
	MAP_USERS_INSERT_RESULT result = m_mapUsers.insert( std::make_pair( nUserID , pUser ) );
	size_t nSize = m_mapUsers.size();
	Global::logger->debug("[%s] mgr size = %u ." , SVR_TEST  ,nSize );
	DISABLE_UNUSED_WARNING( nSize );
	return result.second;
}

bool MODI_LoginUserMgr::DelUser( const MODI_SessionID & nUserID )
{
	MODI_LoginUserMgr::MAP_USERS_ITER itor = m_mapUsers.find( nUserID );
	if( itor != m_mapUsers.end() )
	{
		size_t nSize = m_mapUsers.size();
		Global::logger->debug("[%s] mgr size = %u ." , SVR_TEST  ,nSize );
		DISABLE_UNUSED_WARNING( nSize );
		m_mapUsers.erase( itor );
		return true;
	}
	return false;
}


MODI_LoginUser * 	MODI_LoginUserMgr::Find( const MODI_SessionID & nUserID )
{
	size_t nSize = m_mapUsers.size();
	Global::logger->debug("[%s] mgr size = %u ." , SVR_TEST , nSize );
	DISABLE_UNUSED_WARNING( nSize );
	MODI_LoginUserMgr::MAP_USERS_ITER itor = m_mapUsers.find( nUserID );
	if( itor != m_mapUsers.end() )
	{
		return itor->second;
	}
	else 
	{
		Global::logger->debug("[%s] can't find in mgr by <%s> ." , SVR_TEST , nUserID.ToString() );
		itor = m_mapUsers.begin();
		while( itor != m_mapUsers.end() )
		{
			const MODI_SessionID & temp = itor->first;
			Global::logger->debug("[%s] session in mgr <%s> " , SVR_TEST , temp.ToString() );
			itor++;
		}
	}

	return 0;
}
		

void MODI_LoginUserMgr::DisconnectionTimeOutUsers( unsigned long nCurTime )
{
	static unsigned long s_nTimeOut = 60 * 1000 * 3;
	static unsigned long s_nCreateRoleAddTime = 60 * 1000 * 7; 

	MODI_LoginUserMgr::MAP_USERS_ITER itor = m_mapUsers.begin();
	while( itor != m_mapUsers.end() )
	{
		MODI_LoginUserMgr::MAP_USERS_ITER itNext = itor;
		itNext++;

		MODI_LoginUser * pUser = itor->second;

		if( pUser->IsInWaitCloseStatus() && pUser->IsWaitCloseTimeout() )
		{
			Global::logger->info("[%s] client<session=%s,accid=%u,acc=%s> waitclose timeout , disconnection it." , 
					LOGIN_OPT ,
					pUser->GetSessionID().ToString(),
					pUser->GetAccountID(),
					pUser->GetAccount());
			// 断开
			pUser->Disconnection();
			m_mapUsers.erase( itor );
		}
		else 
		{
			unsigned long nTimeOut = s_nTimeOut;

			// 如果有创建角色步骤，多加点时间
			if( pUser->IsHasCreateRoleStatus() )
				nTimeOut += s_nCreateRoleAddTime;

			// 从登入到现在，如果超过超时时间。则断开
			if( nCurTime > pUser->GetCreateTime() + nTimeOut )
			{
				Global::logger->info("[%s] client<session=%s,accid=%u,acc=%s> timeout , disconnection it." , 
						LOGIN_OPT ,
						pUser->GetSessionID().ToString(),
						pUser->GetAccountID(),
						pUser->GetAccount());
				// 断开
				pUser->Disconnection();
				m_mapUsers.erase( itor );
			}
		}

		itor = itNext;
	}
}
