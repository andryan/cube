#include "GameClientTask.h"
#include "gw2gs.h"
#include "protocol/c2gs.h"
#include "Share.h"
#include "LoginUser.h"
#include "LoginUserMgr.h"
#include "LoginLogicThread.h"
#include "PackageHandler_c2ls.h"


MODI_SvrPackageProcessBase 	 MODI_GameClientTask::ms_ProcessBase;

void MODI_GameClientTask::OnConnection()
{
}

// 断开的回调，可以安全的在逻辑线程调用
void MODI_GameClientTask::OnDisconnection()
{
	MODI_ASSERT( GetUser() );
	if( m_pLoginUser )
	{
		MODI_LoginUserMgr * pMgr = MODI_LoginUserMgr::GetInstancePtr();
		pMgr->DelUser( m_pLoginUser->GetSessionID() );
	}
}

void 	MODI_GameClientTask::ProcessAllPackage()
{
	ms_ProcessBase.ProcessAllPackage();
}

MODI_GameClientTask::MODI_GameClientTask(MODI_LoginUser * p ,const int sock, const struct sockaddr_in * addr):
	MODI_IServerClientTask( sock , addr ),
	m_pLoginUser(p)
{
	m_pLoginUser->SetTask( this );
	//m_pSocket->SetEncryptType(MODI_Encrypt::ENCRYPT_TYPE_RC5);
}


MODI_GameClientTask::~MODI_GameClientTask()
{
	delete m_pLoginUser;
}

bool MODI_GameClientTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
		return false;

	this->Put( pt_null_cmd , cmd_size );
	return true;
}


bool MODI_GameClientTask::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	if( m_pLoginUser )
	{
            ///	游戏命令的处理
		int iRet = MODI_PackageHandler_c2ls::GetInstancePtr()->DoHandlePackage(
						ptNullCmd->byCmd, ptNullCmd->byParam, m_pLoginUser , ptNullCmd,
						dwCmdLen);

		if( iRet == enPHandler_Ban )
		{

		}
		else  if ( iRet == enPHandler_Kick )
		{
				m_pLoginUser->Disconnection();
		}
		else if( iRet == enPHandler_Warning )
		{

		}

		return true;
	}

	return true;
}

int MODI_GameClientTask::RecycleConn()
{
	if( this->IsCanDel() )
		return 1;

	return 0;
}

