#include "PackageHandler_c2ls.h"
#include "protocol/c2gs.h"
#include "GameClientTask.h"
#include "AssertEx.h"
#include "LoginUserMgr.h"
#include "DBClient_LoginSvr.h"
#include "ManangerClient.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "Channellist.h"
#include "Global.h"
#include "PackageHandler_ls2adb.h"
#include "GameHelpFun.h"
#include "DefaultAvatarCreator.h"
#include "BinFileMgr.h"
#include "LoginServerAPP.h"
#include "Base/HelpFuns.h"
#include "Base/DisableStrTable.h"

MODI_PackageHandler_c2ls::MODI_PackageHandler_c2ls()
{

}

MODI_PackageHandler_c2ls::~MODI_PackageHandler_c2ls()
{

}

bool MODI_PackageHandler_c2ls::FillPackageHandlerTable()
{
	this->AddPackageHandler(MAINCMD_LOGIN , MODI_C2LS_Request_Login::ms_SubCmd , 
			&MODI_PackageHandler_c2ls::RequestLogin_Handler);
	this->AddPackageHandler(MAINCMD_ROLE , MODI_C2GS_Request_CreateRole::ms_SubCmd ,
			&MODI_PackageHandler_c2ls::RequestCreateRole_Handler);
	this->AddPackageHandler(MAINCMD_LOGIN, MODI_C2LS_Request_SelChannel::ms_SubCmd ,
			&MODI_PackageHandler_c2ls::RequestSelChannel_Handler );

	return true;
}


/*-----------------------------------------------------------------------------
 * 处理客户端的登入请求 
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_c2ls::RequestLogin_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_LoginUser * pUser = (MODI_LoginUser *)(pObj);
	if( !pUser )
	{
		return enPHandler_Kick;
	}

	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2LS_Request_Login , 0 , 0 );
	if( nSafePackageSize != cmd_size )
	{
		// MODI_C2LS_Request_Login handler for <= 1.8.0
		if( GET_PACKAGE_SIZE( MODI_C2LS_Request_Login_Pre180 , 0 , 0 ) == cmd_size )
		{
			MODI_C2LS_Request_Login_Pre180 * pReq = (MODI_C2LS_Request_Login_Pre180 *)(pt_null_cmd);

			if( !is_valid_version( pReq->version.major , pReq->version.minor , pReq->version.patch ) )
			{
				Global::logger->info("[%s] loginuser<sessionid=%s, acc=%s> send LoginRequest Package, invaild version<major=%u,minor=%u,patch=%u> ." ,
						LOGIN_OPT ,
						pUser->GetSessionID().ToString() ,
						pReq->m_szAccountName ,
						pReq->version.major ,
						pReq->version.minor ,
						pReq->version.patch );

				MODI_GS2C_Respone_OptResult 	result;
				result.m_nResult = SvrResult_Login_InvalidVersion;
				pUser->SendCmd( &result , sizeof(result) );
				pUser->SwitchStatusToWaitClose();
				return enPHandler_Warning;
			}
		}

		Global::logger->warn("[%s] client <accid=%u> send an invaild size package<%s>." ,
				ERROR_PACKAGE ,
				pUser->GetAccountID(),
				"MODI_C2LS_Request_Login " );

		return enPHandler_Kick;
	}

	// 是否处于等待发送帐号密码状态
	if( !pUser->IsInWaitSendAPStatus() )
	{
		Global::logger->debug("[%s] loginuser<session=%s>  not in WaitSendAPStatus , but send Request Login Package." ,
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() );
		// 状态不对，断开链接
		return enPHandler_Kick;
	}

	// account db 是否还连接着
	MODI_DBClient_LoginSvr * pADB =  MODI_DBClient_LoginSvr::GetInstancePtr();
	if( pADB->IsTTerminate() )
	{
		Global::logger->info("[%s] loginuser<session=%s> request login faild. connection with account db server was Terminate ." ,
			   LOGIN_OPT , pUser->GetSessionID().ToString() );

		return enPHandler_Kick;
	}

	MODI_C2LS_Request_Login * pReq = (MODI_C2LS_Request_Login *)(pt_null_cmd);
	MODI_LoginUserMgr * pMgr = MODI_LoginUserMgr::GetInstancePtr();

	if( !is_valid_version( pReq->version.major , pReq->version.minor , pReq->version.patch ) )
	{
		Global::logger->info("[%s] loginuser<sessionid=%s, acc=%s> send LoginRequest Package, invaild version<major=%u,minor=%u,patch=%u> ." ,
			   	LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pReq->m_szAccountName ,
				pReq->version.major , 
				pReq->version.minor , 
				pReq->version.patch );

		MODI_GS2C_Respone_OptResult 	result;
		result.m_nResult = SvrResult_Login_InvalidVersion;
		pUser->SendCmd( &result , sizeof(result) );
		pUser->SwitchStatusToWaitClose();
		return enPHandler_Warning;
	}
	

	// 验证帐号,密码有效性
	if( !MODI_GameHelpFun::AccountSafeCheck( pReq->m_szAccountName , MAX_ACCOUNT_LEN + 1 ) )
	{
		pReq->m_szAccountName[sizeof(pReq->m_szAccountName) - 1] = '\0';
		Global::logger->debug("[%s] loginuser<sessionid=%s, acc=%s> send Invaild LoginRequest Package, invaild acc string." ,
			   	LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pReq->m_szAccountName );

		// 返回客户端无效帐号或者密码
		MODI_GS2C_Respone_OptResult 	result;
		result.m_nResult = SvrResult_Login_InvaildAP;
		pUser->SendCmd( &result , sizeof(result) );

		// 帐号长度太短
		pUser->SwitchStatusToWaitClose();
		return enPHandler_Warning;
	}

	size_t nAccLen = strlen(pReq->m_szAccountName);
	unsigned int nPwdLen = 0;
	if( nAccLen < MIN_ACCOUNT_LEN || nAccLen > MAX_ACCOUNT_LEN ||
		!MODI_GameHelpFun::StrSafeLength( pReq->m_szAccountPwd , ACCOUNT_PWD_LEN + 1 , nPwdLen ) )
	{
		pReq->m_szAccountName[sizeof(pReq->m_szAccountName) - 1] = '\0';
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild LoginRequest Package.account or pwd size error." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pReq->m_szAccountName );
		// 返回客户端无效帐号或者密码
		MODI_GS2C_Respone_OptResult 	result;
		result.m_nResult = SvrResult_Login_InvaildAP;
		pUser->SendCmd( &result , sizeof(result) );

		// 帐号长度太短
		pUser->SwitchStatusToWaitClose();
		return enPHandler_Warning;
	}


//	char szTemp[64];
//	memset( szTemp , 0 , sizeof(szTemp) );
//	if( !Binary2String( pReq->m_szAccountPwd , sizeof(pReq->m_szAccountPwd), szTemp ) )
//	{
//		pReq->m_szAccountName[sizeof(pReq->m_szAccountName) - 1] = '\0';
//		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild LoginRequest Package.bin to string falid." , 
//				LOGIN_OPT ,
//				pUser->GetSessionID().ToString() , 
//				pReq->m_szAccountName );
//		pUser->SwitchStatusToWaitClose();
//		return enPHandler_Warning;
//	}

	char szMacAddr[MAC_ADDR_LEN * 2 + 1];
	unsigned int i;

	memset( szMacAddr , 0 , sizeof(szMacAddr) );

	for (i = 0; i < MAC_ADDR_LEN; i++)
	{
		SNPRINTF( &szMacAddr[i * 2] , 3 , "%02x" , pReq->m_pMacAddr[i] & 0xFF );
	}

	szMacAddr[sizeof(szMacAddr) - 1] = '\0';

	pUser->SetAccount( pReq->m_szAccountName );
	pUser->SetMacAddr( szMacAddr );

	MODI_S2ADB_Request_LoginIn msg;
	msg.m_SessionID = pUser->GetSessionID();
	safe_strncpy( msg.m_szAccountName , pReq->m_szAccountName , sizeof(msg.m_szAccountName) );
	safe_strncpy( msg.m_szAccountPwd , pReq->m_szAccountPwd , sizeof(msg.m_szAccountPwd) );

	// 向DB请求
	MODI_DBClient_LoginSvr::GetInstanceRef().SendCmd( &msg , sizeof(msg) );
	
	// 切换状态－> 等待DB验证
	pUser->SwitchStatusToWaitDBAuth();

	Global::logger->debug("[%s] loginuser <%s,account=%s,mac=%s> send a request logincmd" ,
			LOGIN_OPT,
			pUser->GetSessionID().ToString() , 
			pUser->GetAccount() ,
			pUser->GetMacAddr() );

	bool bRet = pMgr->AddUser( pUser->GetSessionID() , pUser );
	MODI_ASSERT( bRet );
	bRet = 0;
	return enPHandler_OK;
}


/*-----------------------------------------------------------------------------
 *  客户端请求创建角色的处理
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_c2ls::RequestCreateRole_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_LoginUser * pUser = (MODI_LoginUser *)(pObj);
	if( !pUser )
	{
		return enPHandler_Kick;
	}

	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2GS_Request_CreateRole , 0 , 0 );
	if( nSafePackageSize != cmd_size )
	{
		Global::logger->warn("[%s] client <accid=%u> send an invaild size package<%s>." , 
				ERROR_PACKAGE ,
				pUser->GetAccountID(),
				"MODI_C2GS_Request_CreateRole " );

		return enPHandler_Kick;
	}	

	// 该客户端是否处于等待请求创建角色的状态
	if( !pUser->IsInWaitCreateRoleStatus() )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> not in WaitCreateRole Status , but send CreateRole Package." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pUser->GetAccount() );
		return enPHandler_Kick;
	}

	// mananger server 是否还连接着
	MODI_ManangerClient * pMc = MODI_ManangerClient::GetInstancePtr();
	if( pMc->IsTTerminate() )
	{
		Global::logger->info("[%s] loginuser<session=%s, account=%s> request create role faild. connection with mananger server was Terminate ." ,
			   LOGIN_OPT , 
			   pUser->GetSessionID().ToString() , 
			   pUser->GetAccount() ); 
		return enPHandler_Kick;
	}

	const MODI_C2GS_Request_CreateRole * pReq = (const MODI_C2GS_Request_CreateRole *)(pt_null_cmd);
	
	// 所选的config id 是否有效
	MODI_BinFileMgr * pBinMgr = MODI_BinFileMgr::GetInstancePtr();
	if( pBinMgr->IsValidConfigInTables( pReq->m_createInfo.m_AvatarIdx , enBinFT_DefaultAvatar ) == false )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. \
				invaild default avatar index, buy configid<%u> ." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() ,
				pUser->GetAccount() ,
				pReq->m_createInfo.m_AvatarIdx );
		return enPHandler_Kick;
	}


	unsigned int  nNameLen = 0;
	// 验证数据包
	if( !MODI_GameHelpFun::CharacterNameSafeCheck( pReq->m_createInfo.m_szRoleName , ROLE_NAME_MAX_LEN + 1 , nNameLen ) )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. RoleName too large." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() ,
				pUser->GetAccount() );
		return enPHandler_Kick;
	}

	if( ( nNameLen >= ROLE_NAME_MIN_LEN + 1  && nNameLen <= ROLE_NAME_MAX_LEN + 1 ) == false )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. RoleName too small." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() ,
				pUser->GetAccount() );
		return enPHandler_Kick;
	}

	if( strncmp( pReq->m_createInfo.m_szRoleName , SYS_MAIL_SENDER , strlen(SYS_MAIL_SENDER) ) == 0)
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. RoleName disable." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() ,
				pUser->GetAccount() );
		return enPHandler_Kick;
	}

	MODI_DisableStrTable * p_dst = MODI_DisableStrTable::GetInstancePtr();
	if( !p_dst )
	{
		// 失败
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Role_CRFFaildUnknowError;
		pUser->SendCmd( &result , sizeof(result) );
		Global::logger->warn("[%s] loginuser<session=%s, account=%s> create role faild.can't find DisableStringTable." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString(),
				pUser->GetAccount() );

		return enPHandler_OK;
	}

	if( p_dst->CheckDirtyWord( pReq->m_createInfo.m_szRoleName ) )
	{
		// 失败
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Role_CRFNameHaveDisableString;
		pUser->SendCmd( &result , sizeof(result) );

		Global::logger->info("[%s] loginuser<session=%s, account=%s> create role faild. name has disable string<name=%s> ." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString(),
				pUser->GetAccount() ,
				pReq->m_createInfo.m_szRoleName );

		return enPHandler_OK;
	}

	DWORD nSigLen = 0;
	if( !MODI_GameHelpFun::StrSafeLength( pReq->m_createInfo.m_Sign , sizeof(pReq->m_createInfo.m_Sign) , nSigLen ) )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. Sign too large." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString(),
				pUser->GetAccount() );
		return enPHandler_Kick;
	}
	
	if( (pReq->m_createInfo.m_byXuexing > enXuexingBegin && pReq->m_createInfo.m_byXuexing < enXuexingEnd ) == false )
	{
		Global::logger->debug("[%s] loginuser<session=%s, account=%s> send Invaild CreateRole Package. boold type is invaild<%u>." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pUser->GetAccount() ,
			   	pReq->m_createInfo.m_byXuexing );
		return enPHandler_Kick;
	}

	MODI_S2RDB_Request_CreateChar msg;
	msg.m_nSessionID = pUser->GetSessionID();
	msg.m_nAccountID = pUser->GetAccountID();
	strncpy(msg.m_cstrAccName, pUser->GetAccount(), sizeof(msg.m_cstrAccName) - 1);
	msg.m_createInfo = pReq->m_createInfo;

	// 让ROLE DB创建角色
	MODI_ManangerClient::GetInstancePtr()->SendCmd( &msg , sizeof(msg) );

	// 等待DB的角色创建结果
	pUser->SwitchStatusToWaitCreateRoleReuslt();

	Global::logger->debug("[%s] loginuser<session=%s, account=%s> send create role Package..." , 
			LOGIN_OPT ,
			pUser->GetSessionID().ToString() , 
			pUser->GetAccount() );

	return enPHandler_OK;
}

int MODI_PackageHandler_c2ls::RequestSelChannel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_LoginUser * pUser = (MODI_LoginUser *)(pObj);
	if( !pUser )
		return enPHandler_Kick;

	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2LS_Request_SelChannel , 0 , 0 );
	if( nSafePackageSize != cmd_size )
	{
		Global::logger->warn("[%s] client <accid=%u> send an invaild size package<%s>." , 
				ERROR_PACKAGE ,
				pUser->GetAccountID(),
				"MODI_C2LS_Request_SelChannel " );

		return enPHandler_Kick;
	}	


	if( !pUser->IsInWaitSelectChannel() )
		return enPHandler_Kick;

	const MODI_C2LS_Request_SelChannel 	* pReq = (const MODI_C2LS_Request_SelChannel *)(pt_null_cmd);
	// 频道是否存在
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if( !plist )
		return enPHandler_Kick;

	// mananger server 是否还连接着
	MODI_ManangerClient * pMc = MODI_ManangerClient::GetInstancePtr();
	if( !pMc || pMc->IsTTerminate() )
	{
		Global::logger->info("[%s] loginuser<sessionid=%s, account=%s> request select channel faild. connection with mananger server was Terminate ." ,
			   LOGIN_OPT , 
			   pUser->GetSessionID().ToString() , 
			   pUser->GetAccount() ); 
		return enPHandler_Kick;
	}

	const MODI_ServerStatus * pStatus = plist->Find( pReq->m_nChannelID );
	if( !pStatus )
	{
		Global::logger->info("[%s] loginuser<session=%s , account=%s> send Invaild Select Channel<%u> Package. \
				channel is't exist  ." , 
				LOGIN_OPT ,
				pUser->GetSessionID().ToString() , 
				pUser->GetAccount() , 
				pReq->m_nChannelID );

		MODI_PackageHandler_ls2s::SendChannelList( pUser , 
				MODI_LS2C_Notify_Channellist::kChannelNotExist );
		pUser->SetSyncChannellistCount( pUser->GetSyncChannellistCount() + 1 );
		return enPHandler_OK;
	}
	else 
	{
		// 频道是否满
		if( pStatus->m_byLoadStatus >= 100 )
		{
			// 频道满
			MODI_PackageHandler_ls2s::SendChannelList( pUser  , 
					MODI_LS2C_Notify_Channellist::kChannelFull );
			pUser->SetSyncChannellistCount( pUser->GetSyncChannellistCount() + 1 );
			return enPHandler_OK;
		}
	}

	MODI_LS2C_Notify_GatewayInfo msg;
	if(! pReq->m_szNetType)
		return enPHandler_OK;
	
	MODI_ChannelAddr get_addr;
	if(! plist->GetChannelAddr(pReq->m_nChannelID, pReq->m_szNetType, get_addr))
	{
		Global::logger->fatal("[get_channel_info] get channel <%d,%s> failed", pReq->m_nChannelID, pReq->m_szNetType);
		MODI_PackageHandler_ls2s::SendChannelList( pUser , 
				MODI_LS2C_Notify_Channellist::kChannelNotExist );
		pUser->SetSyncChannellistCount( pUser->GetSyncChannellistCount() + 1 );
		return enPHandler_OK;
	}
	
	msg.m_nIP = PublicFun::StrIP2NumIP(get_addr.m_strChannelIP.c_str());
	msg.m_nPort = get_addr.m_wdChannelPort;
	//msg.m_nIP = pStatus->m_nGatewayIP;
	//msg.m_nPort = pStatus->m_nGatewayPort;
	
	msg.m_Key = pUser->GetLoginKey();
	msg.m_nAccountID = pUser->GetAccountID();

	pUser->SendCmd( &msg , sizeof(msg) );

	pUser->SwitchStatusToWaitClose();

	Global::logger->debug("[%s] loginuser<session=%s, account=%s,key=%d,%d,%d,%d> send select channel<%u> Package..." , 
			LOGIN_OPT ,
			pUser->GetSessionID().ToString() , 
			pUser->GetAccount() , 
			msg.m_Key.m_szContnet[0],
			msg.m_Key.m_szContnet[1],
			msg.m_Key.m_szContnet[2],
			msg.m_Key.m_szContnet[3],
			pReq->m_nChannelID );


	return enPHandler_OK;
}
