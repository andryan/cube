#include "PackageHandler_ls2adb.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "s2zs_cmd.h"
#include "LoginUserMgr.h"
#include "DBClient_LoginSvr.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "ManangerClient.h"
#include "Channellist.h"


MODI_PackageHandler_ls2s::MODI_PackageHandler_ls2s()
{

}

MODI_PackageHandler_ls2s::~MODI_PackageHandler_ls2s()
{

}

bool MODI_PackageHandler_ls2s::FillPackageHandlerTable()
{
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_Charlist::ms_SubCmd , 
			&MODI_PackageHandler_ls2s::Charslist_Handler );

	// 创建角色的返回通知
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_CreateCharResult::ms_SubCmd ,
			&MODI_PackageHandler_ls2s::CreateChar_Handler );


	/*-----------------------------------------------------------------------------
	 *  mananger server
	 *-----------------------------------------------------------------------------*/
	// 频道列表的通知
	this->AddPackageHandler( MAINCMD_LOGIN , MODI_LS2C_Notify_Channellist::ms_SubCmd ,
			&MODI_PackageHandler_ls2s::Channellist_Handler );

	// pp验证结果 
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_LoginAuthResult::ms_SubCmd ,
			&MODI_PackageHandler_ls2s::DirectionToManangerServer_Handle );

	/*-----------------------------------------------------------------------------
	 *  account db server
	 *-----------------------------------------------------------------------------*/
	// 登入结果的通知
    this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_LoginResult::ms_SubCmd, 
			&MODI_PackageHandler_ls2s::LoginResult_Handler);

	// 转发 登入网关的验证请求通知
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_LoginAuth::ms_SubCmd ,
			&MODI_PackageHandler_ls2s::DirectionToAccountDB_Handle );

	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_ReMakePP::ms_SubCmd , 
			&MODI_PackageHandler_ls2s::DirectionToAccountDB_Handle );

	this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_ReMakePPResult::ms_SubCmd , 
			&MODI_PackageHandler_ls2s::DirectionToManangerServer_Handle );

	return true;
}

int MODI_PackageHandler_ls2s::DirectionToAccountDB_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_DBClient_LoginSvr * pLoginServer = MODI_DBClient_LoginSvr::GetInstancePtr();
	if( pLoginServer )
	{
		pLoginServer->SendCmd( pt_null_cmd , cmd_size );
	}
	else 
	{
		Global::logger->warn("[%s] can't direction package to account server . disconnection with account server." , 
				ERROR_CON );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_ls2s::DirectionToManangerServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ManangerClient * pManangerClient = MODI_ManangerClient::GetInstancePtr();
	if( pManangerClient )
	{
		pManangerClient->SendCmd( pt_null_cmd , cmd_size );
	}
	else 
	{
		Global::logger->warn("[%s] can't direction package to mananger server . disconnection with mananger server." , ERROR_CON );
	}

	return enPHandler_OK;
}

/*-----------------------------------------------------------------------------
 *  处理ADB对登入请求验证的结果
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_ls2s::LoginResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{

	if( pObj != (void *)(MODI_DBClient_LoginSvr::GetInstancePtr()) || !pObj )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_DBClient_LoginSvr * pClient = MODI_DBClient_LoginSvr::GetInstancePtr();
	DISABLE_UNUSED_WARNING( pClient );

	const MODI_ADB2S_Notify_LoginResult * pReq = (const MODI_ADB2S_Notify_LoginResult *)(pt_null_cmd);

	Global::logger->info("[%s] Recv loginuser<session<%s>'s LoginResult",
			LOGIN_OPT ,
			pReq->m_SessionID.ToString() );

	if( !pReq )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	MODI_LoginUserMgr * pMgr = MODI_LoginUserMgr::GetInstancePtr();
	MODI_LoginUser * pUser = pMgr->Find( pReq->m_SessionID );
	if( !pUser )
	{
		Global::logger->debug("[%s] loginuser<sessionid=%s> Notify LoginResult , but can't find it ." , LOGIN_OPT ,
				pReq->m_SessionID.ToString()  );
		return enPHandler_OK;
	}

	// 失败，返回结果给客户端
	if( pReq->m_nResult != SvrResult_Login_OK )
	{
		// 登入失败
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = pReq->m_nResult;
		pUser->SendCmd( &result , sizeof(result) );
		pUser->SwitchStatusToWaitClose();
		return enPHandler_OK;
	}

	/********** 帐号密码正确的处理 *****************************************/

	const MODI_ADB2S_Notify_LoginResult::MODI_Result  * pDeailRes = (const MODI_ADB2S_Notify_LoginResult::MODI_Result *)(pReq->m_pData);

	// 更新该客户端相关信息
	pUser->SetAccount(pDeailRes->m_cstrAccName);
	pUser->SetAccountID( pDeailRes->m_nAccountID );
	pUser->SetLoginKey( pDeailRes->m_Key );

	// 处于等待角色数据状态
	pUser->SwitchStatusToWaitRoleData();
	
	Global::logger->info("[login_ok] loginuser ok send to zoneservice <accid=%u,%s,mac=%s>",
			pUser->GetAccountID(),
			pUser->GetSessionID().ToString(),
			pUser->GetMacAddr() );

	// 向MGRSVR请求角色数据
	MODI_S2ZS_Request_ClientLogin 	msgcl;
	msgcl.m_accid = pUser->GetAccountID();
	msgcl.m_sessionID = pUser->GetSessionID();
	MODI_ManangerClient::GetInstancePtr()->SendCmd( &msgcl , sizeof(msgcl) );
	
	return enPHandler_OK;
}

/*-----------------------------------------------------------------------------
 *  处理RDB返回的角色列表
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_ls2s::Charslist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_Charlist * pNotifyChars = (const MODI_ZS2S_Notify_Charlist *)(pt_null_cmd);

	Global::logger->info("[%s] Recv loginuser<accid=%u,session<%s>'s characters<count=%u> list." , 
			LOGIN_OPT ,
			pNotifyChars->m_accid,
			pNotifyChars->m_sessionID.ToString() ,
			pNotifyChars->m_nRoleCount );

	// find client 
	MODI_LoginUser * pUser = MODI_LoginUserMgr::GetInstancePtr()->Find( pNotifyChars->m_sessionID );
	if( !pUser )
	{
		Global::logger->info("[%s] Can't find Session<%s>'." , 
			LOGIN_OPT ,
			pNotifyChars->m_sessionID.ToString() );
		return enPHandler_OK;
	}

	if( !pNotifyChars->m_bySuccessful )
	{
		Global::logger->warn("[%s] notify charlist faild. kick client<%s>." , 
				LOGIN_OPT ,
				pUser->GetAccount() );
		return enPHandler_Kick;
	}

	// 发送角色列表
	SendCharslist( pUser , (MODI_CharInfo *)(pNotifyChars->m_pChars) ,  pNotifyChars->m_nRoleCount );

	if( pNotifyChars->m_nRoleCount > 0 )
	{
		// 发送频道列表
		SendChannelList( pUser , MODI_LS2C_Notify_Channellist::kLogin );

		// 有角色,将基本角色数据发送给客户端，等待客户端选择频道
		pUser->SwitchStatusToWaitSelectChannnel();
	}
	else 
	{
		// 没有角色等待客户端创建角色
		pUser->SwitchStatusToWaitCreateRole();
	}

	return enPHandler_OK;
}


/*-----------------------------------------------------------------------------
 *  对RDB创建角色结果的处理
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_ls2s::CreateChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_CreateCharResult * pCreateResult = (const MODI_RDB2S_Notify_CreateCharResult *)(pt_null_cmd);

	Global::logger->info("[%s] Recv loginuser<accid=%u,session=%s>'s create character result<successful=%u>." , 
			LOGIN_OPT ,
			pCreateResult->m_nAccountID,
			pCreateResult->m_nSessionID.ToString(),
			pCreateResult->m_bySuccessful );

	MODI_LoginUser * pUser = MODI_LoginUserMgr::GetInstancePtr()->Find( pCreateResult->m_nSessionID );
	if( !pUser )
		return enPHandler_OK;

	if( pCreateResult->m_bySuccessful == SvrResult_Role_CreateRoleSuccessful )
	{
		Global::logger->debug("[%s] loginuser<%s> createrole successful." , LOGIN_OPT ,
				pUser->GetAccount() );
		// 创建成功

		// 发送角色数据
		SendCharslist( pUser ,  &pCreateResult->m_roleInfo , 1 );

		// 发送频道列表
		SendChannelList( pUser  , MODI_LS2C_Notify_Channellist::kLogin );

		// 客户端处于选择频道状态
		pUser->SwitchStatusToWaitSelectChannnel();
	}
	else 
	{
		if( pCreateResult->m_bySuccessful == SvrResult_Role_CRFNameAlreadyExist )
		{
			Global::logger->debug("[%s] loginuser<%s> createrole faild. already exist rolename . " , LOGIN_OPT ,
				pUser->GetAccount()  );
			pUser->SwitchStatusToWaitCreateRole();
		}
		else 
		{
			Global::logger->debug("[%s] loginuser<%s> createrole faild." , LOGIN_OPT ,
				pUser->GetAccount() );
		}
		// 失败
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = pCreateResult->m_bySuccessful;
		pUser->SendCmd( &result , sizeof(result) );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_ls2s::SendCharslist( MODI_LoginUser * pUser , const MODI_CharInfo * pCharInfo , unsigned int nCharsNum )
{
	// 发送角色列表给客户端
	if( pUser )
	{
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_LS2C_Notify_Charlist * p_notify = (MODI_LS2C_Notify_Charlist *)(szPackageBuf);
		AutoConstruct( p_notify );
		p_notify->m_nRoleCount = nCharsNum;
		memcpy( p_notify->m_pChars , pCharInfo , sizeof(MODI_CharInfo) * p_notify->m_nRoleCount );
		int iSendSize = sizeof(MODI_LS2C_Notify_Charlist) + sizeof(MODI_CharInfo) * p_notify->m_nRoleCount;
		bool bRet = pUser->SendCmd( p_notify , iSendSize );
		Global::logger->debug("[%s] Send charslist to Client , accountName<%s> ." , LOGIN_OPT , 
				pUser->GetAccount() );
		return bRet ? 0 : false;
	}
	return 1;
}

int MODI_PackageHandler_ls2s::SendChannelList( MODI_LoginUser * pUser , int iReason )
{
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();

	if( plist )
	{
		// 频道列表发送给客户端
		int iChannelNum = 0;
		MODI_ServiceTask * pTask = (MODI_ServiceTask *)(pUser->GetTask());
	   	if( !plist->SendChannellistToGameClient( pTask , iChannelNum , iReason , pUser->GetAccountID()) )
		{
			Global::logger->warn("[%s] send channellist faild. disconnection with client." , 
					SVR_TEST );
			pUser->Disconnection();
			return 0;
		}

		Global::logger->debug("[%s] send channellist <channelcount = %u> to client, accountname<%s> ." , 
				LOGIN_OPT , 
				iChannelNum ,
				pUser->GetAccount());
	}

	return 0;
}

int MODI_PackageHandler_ls2s::Channellist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{

	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if( plist )
	{
		plist->Clear( true );

		const MODI_LS2C_Notify_Channellist * pReq =  (const MODI_LS2C_Notify_Channellist *)(pt_null_cmd);
		const MODI_ServerStatus * pStatus = (const MODI_ServerStatus *)(pReq->m_pData);
		for( unsigned int n = 0; n < pReq->m_nCount; n++ )
		{
			MODI_ServerStatus * p = new MODI_ServerStatus();
			*p = pStatus[n];
			bool bRet = plist->Add( p->m_nServerID , p );
			MODI_ASSERT( bRet );
			bRet = 0;
		}
		Global::logger->debug("[game_channel] recv channellist <count=%u>" , pReq->m_nCount);
	}

	return 0;
}
