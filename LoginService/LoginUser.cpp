#include "LoginUser.h"
#include "LoginUserMgr.h"
#include "GameClientTask.h"




MODI_LoginUser::MODI_LoginUser():m_nAccountID(INVAILD_ACCOUNT_ID),
	m_iStatus(MODI_LoginUser::enWaittingSendAP),
	m_pTask(0),m_timerWaitClose(0)
{
	m_nSyncChannellistCount = 0;
	m_nCreateTime = GetTickCount( true );
	SetAccount("Not Auth User");
}

MODI_LoginUser::~MODI_LoginUser()
{

}

bool MODI_LoginUser::SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if(m_pTask)
	{
		return m_pTask->SendCmd(pt_null_cmd, cmd_size);
	}
	return false;
}

void MODI_LoginUser::SwitchStatusToWaitDBAuth()
{
	m_iStatus = MODI_LoginUser::enWaittingWaitDBAuth;
}

void MODI_LoginUser::SwitchStatusToWaitClose()
{
	m_iStatus = MODI_LoginUser::enWaittingClose;
	m_timerWaitClose.Reload( 1 * 1000 , Global::m_stLogicRTime );
}

void MODI_LoginUser::SwitchStatusToWaitRoleData()
{
	m_iStatus = MODI_LoginUser::enWaittingRoleData;
}

void MODI_LoginUser::SwitchStatusToWaitCreateRole()
{
	m_bHasCreateRoleStatus = true;
	m_iStatus = MODI_LoginUser::enWaittingCreateRole;
}

void MODI_LoginUser::SwitchStatusToWaitCreateRoleReuslt()
{
	m_iStatus = MODI_LoginUser::enWaittingCreateRoleResult;
}

void MODI_LoginUser::SwitchStatusToWaitSelectChannnel()
{
	m_iStatus = MODI_LoginUser::enWaittingSelectGameChannnel;
}

bool MODI_LoginUser::IsInWaitSendAPStatus() const
{
	return m_iStatus == MODI_LoginUser::enWaittingSendAP;
}

bool MODI_LoginUser::IsInWaitDBAuthStatus() const
{
	return m_iStatus == MODI_LoginUser::enWaittingWaitDBAuth;
}

bool MODI_LoginUser::IsInWaitCloseStatus() const
{
	return m_iStatus == MODI_LoginUser::enWaittingClose;
}

bool MODI_LoginUser::IsInWaitRoleDataStatus() const
{
	return m_iStatus == MODI_LoginUser::enWaittingRoleData;
}

bool MODI_LoginUser::IsInWaitCreateRoleStatus() const
{
	return m_iStatus == MODI_LoginUser::enWaittingCreateRole;
}

bool MODI_LoginUser::IsInWaitSelectChannel() const
{
	return m_iStatus == MODI_LoginUser::enWaittingSelectGameChannnel;
}

bool MODI_LoginUser::IsWaitCloseTimeout()
{
	if( m_timerWaitClose( Global::m_stLogicRTime ) )
		return true;
	return false;
}

bool MODI_LoginUser::IsHasCreateRoleStatus() const
{
	return m_bHasCreateRoleStatus;
}

void MODI_LoginUser::Disconnection()
{
	if( m_pTask )
	{
		m_pTask->Terminate();
	}
}

void MODI_LoginUser::SetTask( MODI_GameClientTask * pTask )
{
	m_pTask = pTask;
}
