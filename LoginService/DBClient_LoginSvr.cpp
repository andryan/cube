#include "DBClient_LoginSvr.h"
#include "LoginService.h"
#include "PackageHandler_ls2adb.h"



MODI_DBClient_LoginSvr::MODI_DBClient_LoginSvr(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(SVR_CMDSIZE);
}

MODI_DBClient_LoginSvr::~MODI_DBClient_LoginSvr()
{

}



/**
 * @brief 初始化
 *
 */
bool MODI_DBClient_LoginSvr::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

/**
 * @brief 结束,临时方案
 *
 */
void MODI_DBClient_LoginSvr::Final()
{
	MODI_ClientTask::Final();

	Global::logger->info("[%s] Disconnection with Account Server. LoginServer will Terminate. " , 
			ERROR_CON );

	// 终止服务器
	if( MODI_LoginService::GetInstancePtr() )
	{
		MODI_LoginService::GetInstancePtr()->Terminate();
	}
}

bool MODI_DBClient_LoginSvr::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }

    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_DBClient_LoginSvr::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

	return false;
}

bool MODI_DBClient_LoginSvr::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	// 数据包处理
	if( MODI_DBClient_LoginSvr::GetInstancePtr() )
	{
            ///	游戏命令的处理
		int iRet = MODI_PackageHandler_ls2s::GetInstancePtr()->DoHandlePackage(
						ptNullCmd->byCmd, ptNullCmd->byParam, MODI_DBClient_LoginSvr::GetInstancePtr() , ptNullCmd,
						dwCmdLen);

		if( iRet == enPHandler_Ban )
		{

		}
		else  if ( iRet == enPHandler_Kick )
		{

		}
		else if( iRet == enPHandler_Warning )
		{

		}

		return true;
	}

	return true;
}

