/**
 * @file   LoginService.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Sat Feb 27 17:07:35 2010
 * @version Ver0.1
 * @brief  多监听的服务器
 * 
 */


#ifndef _MDLOGINSERVICE_H
#define _MDLOGINSERVICE_H

#include <vector>
#include <iostream>
#include "MNetService.h"
//#include "ServiceTaskPoll.h"
#include "ServiceTaskSched.h"
#include "SingleObject.h"

enum
{
	LSSVR_PORTIDX_4_CLIENTS = 0,
//	LSSVR_PORTIDX_4_MGR = 1,
	LSSVR_PORTIDX_COUNT,
};

/**
 * @brief 多监听的服务器
 * 
 */
class MODI_LoginService: public MODI_MNetService , public CSingleObject<MODI_LoginService>
{
 public:
	MODI_LoginService(std::vector<WORD > &  vec, const std::vector<std::string>  & ip_vec, const char * name = "LoginService", const int count = LSSVR_PORTIDX_COUNT);

	bool CreateTask(const int & sock, const struct sockaddr_in * addr , const WORD & port);
		
	virtual bool Init();

	virtual void Final();

	void ReloadConfig();

 private:

	//MODI_ServiceTaskPoll * m_pTaskPools[LSSVR_PORTIDX_COUNT];
	MODI_ServiceTaskSched m_stTaskSched[LSSVR_PORTIDX_COUNT];
};

#endif
