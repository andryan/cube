/** 
 * @file LoginLogicThread.h
 * @brief 登入服务器的逻辑主循环
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-10
 */

#ifndef LOGINSVR_LOGIC_THREAD_H_
#define LOGINSVR_LOGIC_THREAD_H_

#include "Type.h"
#include "Timer.h"
#include "Thread.h"
#include "SingleObject.h"
#include "RecurisveMutex.h"
#include "Global.h"


class MODI_LoginSvrLogic: public MODI_Thread , public CSingleObject<MODI_LoginSvrLogic>
{

public:

    /// 主循环
    virtual void Run();

	void Final();

    /// 构造
    MODI_LoginSvrLogic();

    /// 析构
    virtual ~MODI_LoginSvrLogic();


private:

	MODI_Timer 		m_timerTimeoutCheck;
};

#endif
