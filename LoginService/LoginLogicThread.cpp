#include "LoginLogicThread.h"
#include "DBClient_LoginSvr.h"
#include "GameClientTask.h"
#include "AssertEx.h"
#include "LoginUserMgr.h"
#include "ManangerClient.h"



MODI_LoginSvrLogic::MODI_LoginSvrLogic():m_timerTimeoutCheck( 10 * 1000 , 
		Global::m_stLogicRTime )
{

}


MODI_LoginSvrLogic::~MODI_LoginSvrLogic()
{

}

/**
 * @brief 主循环
 *
 */
void MODI_LoginSvrLogic::Run()
{
    while (!IsTTerminate())
    {
		// 断开一些超时的客户端
		Global::m_stLogicRTime.GetNow();

		if( m_timerTimeoutCheck( Global::m_stLogicRTime ) )
		{
			MODI_LoginUserMgr * pTemp = MODI_LoginUserMgr::GetInstancePtr();
			pTemp->DisconnectionTimeOutUsers( GetTickCount( true ) );
		}

		// 处理来自DB的消息
		if( MODI_DBClient_LoginSvr::GetInstancePtr() )
		{
			unsigned int nMaxGetCmdDB = 1000;
			MODI_DBClient_LoginSvr::GetInstancePtr()->Get(nMaxGetCmdDB);
		}

		if( MODI_ManangerClient::GetInstancePtr() )
		{
			unsigned int nMaxGetCmdMS = 1000;
			MODI_ManangerClient::GetInstancePtr()->Get(nMaxGetCmdMS);
		}

		// 处理来自客户端的消息
		MODI_GameClientTask::ProcessAllPackage();
		
        ::usleep(50);
    }

    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_LoginSvrLogic::Final()
{

}
