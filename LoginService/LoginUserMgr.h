/** 
 * @file LoginUserMgr.h
 * @brief ls上的游戏客户端管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-09
 */

#ifndef LOGIN_USER_MGR_H_
#define LOGIN_USER_MGR_H_

#include "LoginUser.h"
#include "SingleObject.h"
#include <map>

struct StringLess
{
	bool operator() ( const std::string & left , const std::string & right ) const;
};

class 	MODI_LoginUserMgr : public CSingleObject<MODI_LoginUserMgr>
{
	public:

		MODI_LoginUserMgr();
		~MODI_LoginUserMgr();

		
		bool AddUser( const MODI_SessionID & nUserID , MODI_LoginUser * pUser );
		bool DelUser( const MODI_SessionID & nUserID );
		MODI_LoginUser * 	Find( const MODI_SessionID & nUserID );
		
		// 断开所有超时的用户
		void DisconnectionTimeOutUsers( unsigned long nCurTime );

	private:

		typedef std::map<MODI_SessionID ,MODI_LoginUser *> 	MAP_USERS;
		typedef MAP_USERS::iterator 						MAP_USERS_ITER;
		typedef MAP_USERS::const_iterator 					MAP_USERS_C_ITER;
		typedef std::pair<MAP_USERS_ITER,bool> 				MAP_USERS_INSERT_RESULT;

		MAP_USERS 		m_mapUsers;
};


#define 	g_pLoginUserMgr 	((MODI_LoginUserMgr *)(MODI_LoginUserMgr::GetInstancePtr()))

#endif
