/** 
 * @file LoginServer.cpp
 * @brief 登入服务器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-11
 */
#include "LoginServerAPP.h"

int main(int argc, char * argv[])
{	
	int iRet = 0;
	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	MODI_LoginServerAPP app("Login Server");

	iRet = app.Init();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	Global::logger->debug("[%s] login server shutdown!" , SVR_TEST );

	app.Shutdown();

	::sleep(1);

	return iRet;
}
