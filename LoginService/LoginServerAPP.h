#ifndef MODI_LOGINSERVER_APPFRAME_H_
#define MODI_LOGINSERVER_APPFRAME_H_


#include "Base/IAppFrame.h"
#include "Base/AssertEx.h"
#include "LoginService.h"
#include "DBClient_LoginSvr.h"
#include "PackageHandler_c2ls.h"
#include "PackageHandler_ls2adb.h"
#include "LoginLogicThread.h"
#include "ManangerClient.h"
#include "Base/s2ms_cmd.h"
#include "Base/Channellist.h"
#include "Base/DefaultAvatarCreator.h"
#include "Base/DisableStrTable.h"

class 	MODI_LoginServerAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_LoginServerAPP( const char * szAppName );

		virtual ~MODI_LoginServerAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

//		// 默认的avatar 形象创建
		MODI_DefaultAvatarCreator 	& 	GetDefaultAvatarCreator() 
		{
			return m_DefaultCreator; 
		}

	private:

		MODI_SvrChannellist  		m_Channellist; // 当前区的频道列表
		MODI_PackageHandler_c2ls 	m_Packagehandler_c2ls; //  数据包派发器
		MODI_PackageHandler_ls2s    m_Packagehandler_ls2s; //  数据包派发器
		MODI_DBClient_LoginSvr  * 	m_pAccountDBClient; // 连接 account db 服务器的客户端
		MODI_ManangerClient * 		m_pManangerClient; // 连接   mananger server 的客户端
		MODI_DefaultAvatarCreator 	m_DefaultCreator; // 默认的avatar形象创建
		MODI_DisableStrTable 		m_DisableStrTable;
};

#endif
