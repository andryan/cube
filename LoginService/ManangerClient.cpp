#include "ManangerClient.h"
#include "LoginService.h"
#include "PackageHandler_ls2adb.h"



MODI_ManangerClient::MODI_ManangerClient(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(SVR_CMDSIZE);
}

MODI_ManangerClient::~MODI_ManangerClient()
{

}

bool MODI_ManangerClient::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

void MODI_ManangerClient::Final()
{
	MODI_ClientTask::Final();

	Global::logger->info("[%s] Disconnection with Manager Server. LoginServer will Terminate. " , 
			ERROR_CON );

	// 终止服务器
	if( MODI_LoginService::GetInstancePtr() )
	{
		MODI_LoginService::GetInstancePtr()->Terminate();
	}
}

bool MODI_ManangerClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }

    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_ManangerClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

	return false;
}

bool MODI_ManangerClient::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	// 数据包处理
	if( MODI_ManangerClient::GetInstancePtr() )
	{
            ///	游戏命令的处理
		int iRet = MODI_PackageHandler_ls2s::GetInstancePtr()->DoHandlePackage(
						ptNullCmd->byCmd, ptNullCmd->byParam, MODI_ManangerClient::GetInstancePtr() , ptNullCmd,
						dwCmdLen);

		if( iRet == enPHandler_Ban )
		{

		}
		else  if ( iRet == enPHandler_Kick )
		{

		}
		else if( iRet == enPHandler_Warning )
		{

		}

		return true;
	}

	return true;
}

