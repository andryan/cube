/** 
 * @file PackageHandler_ls2adb.h
 * @brief LS和ADB的消息包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-09
 */
#ifndef PACKAGE_HANDLER_H_LS2ADB_
#define PACKAGE_HANDLER_H_LS2ADB_

#include "protocol/gamedefine.h"
#include "IPackageHandler.h"
#include "SingleObject.h"


class 	MODI_LoginUser;

/**
 * @brief 游LS和ADB的消息包处理
 *
 */
class MODI_PackageHandler_ls2s : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_ls2s>
{
public:

    MODI_PackageHandler_ls2s();
    ~MODI_PackageHandler_ls2s();

public:

	/// 处理ACCOUNT DB对登入请求的处理结果
	static int LoginResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 处理ROLE DB 对角色列表查询请求的返回结果
	static int Charslist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 处理ROLE DB 创建角色的结果
	static int CreateChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 处理MANANGER的频道列表
	static int Channellist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );


	static int DirectionToAccountDB_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int DirectionToManangerServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );


	static int SendCharslist( MODI_LoginUser * pUser , const MODI_CharInfo * pCharInfo , unsigned int nCharsNum );

	static int SendChannelList( MODI_LoginUser * pUser , int iReason );

private:

    virtual bool FillPackageHandlerTable();

};

#endif

