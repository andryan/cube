/**
 * @file   Scene.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Dec 14 12:05:39 2011
 * 
 * @brief  游戏里面的场景
 * 
 * 
 */

#include "Scene.h"
#include "MultiGameRoom.h"

void MODI_Scene::SetSceneRoom(MODI_MultiGameRoom * p_room)
{
	m_pGameRoom = p_room;
}

MODI_MultiGameRoom * MODI_Scene::GetSceneRoom()
{
	return m_pGameRoom;
}


/** 
 * @brief 加载场景文件信息
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Scene::LoadSceneFile(const char * file_name)
{
	MODI_SceneHeadInfo head_info;
    FILE* fp = fopen(file_name,"rb");
    if(!fp)
	{
		Global::logger->fatal("[scene_init] load file failed <filename=%s>", file_name);
		return false;
	}
	
    fread(&head_info,sizeof(head_info),1,fp);
	
    m_dwWidth = head_info.m_dwWidth;
    m_dwHeight = head_info.m_dwHeight;
	
    m_ScenePosVec.resize(m_dwWidth * m_dwHeight);
    fread(&m_ScenePosVec[0], m_ScenePosVec.size() * sizeof(MODI_ScenePos),1,fp);
    fclose(fp);

	std::vector<MODI_ScenePos >::iterator iter = m_ScenePosVec.begin();
	for(DWORD i=0; iter != m_ScenePosVec.end(); iter++,i++)
	{
		MODI_ScenePos & pos = *iter;
		if(pos.m_byAttr == enGridAttrBorn)
		{
			m_SceneBornVec.push_back(i);
		}
	}

	if(m_SceneBornVec.size() == 0)
	{
		Global::logger->fatal("[load_scene] load born failed");
		MODI_ASSERT(0);
		return false;
	}
	
    return true;
}


/** 
 * @brief 加载屏的关系
 * 
 * @return 成功true
 *
 */
bool MODI_Scene::LoadNineScreen()
{
	m_dwSceneWidth = (m_dwWidth+SCREEN_WIDTH -1)/SCREEN_WIDTH;
    m_dwSceneHeight = (m_dwHeight+SCREEN_HEIGHT -1)/SCREEN_HEIGHT;
    m_dwSceneMax = m_dwSceneWidth * m_dwSceneHeight;

	for(int i = 0; i < (int)enSceneAttrMax; i++)
	{
		for(DWORD j=0; j < m_dwSceneMax; j ++)
		{
			/// i是类型,j是屏
			m_SceneAttrIndex[i][j];
		}
	}

	/// 建立所有屏和周围屏的索引关系
	const int adjust[9][2] = { {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, 0} };
	for(DWORD j=0; j < m_dwSceneMax; j++)
	{
		int scene_x = j % m_dwSceneWidth;
		int scene_y = j / m_dwSceneWidth;
		std::vector<DWORD > nine_scene_vec;
		for(int i = 0; i < 9; i++)
		{
			int x = scene_x + adjust[i][0];
			int y = scene_y + adjust[i][1];
			if (x >= 0 && y >= 0 && x < (int)m_dwSceneWidth && y < (int)m_dwSceneHeight)
			{
				nine_scene_vec.push_back(y * m_dwSceneWidth + x);
			}
		}
		m_NineSceneIndex.insert(defNineSceneIndexValue(j,nine_scene_vec));
	}

	return true;
}

/** 
 * @brief 加载某种场景
 * 
 * @param file_name 场景文件名
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Scene::Init(const char * file_name, const WORD scene_id)
{
	if(scene_id == 0)
	{
		return false;
	}

	m_wdSceneId = scene_id;
	
	if(! LoadSceneFile(file_name))
	{
		return false;
	}

	/// 加载屏的关系
	if(! LoadNineScreen())
	{
		return false;
	}

	return true;
}



/** 
 * @brief 是否能够移动
 * 
 * @param pos 移动位置 y * m_dwWidth + x
 * 
 * @return 能true
 *
 */
bool MODI_Scene::IsCanMove(const int x, const int y)
{
	unsigned int pos = y * m_dwWidth + x;
	
	if(pos>0 && pos<m_dwWidth * m_dwHeight)
	{
 		if(m_ScenePosVec[pos - 1].m_byAttr < 128)
 		{
 			return true;
 		}
		return true;
	}
	return false;
}



/** 
 * @brief 尝试移动
 * 
 * @param src_state 移动前初始状态
 * @param cmp_state 目标比较状态
 * @param dest_state 移动结果
 * 
 * @return 有移动改变ture,无移动改变false
 *
 */
bool MODI_Scene::CheckMove(MODI_MoveState & src_state, const MODI_MoveState & cmp_state)
{
	if(cmp_state.m_Pos == src_state.m_Pos)
	{
		return false;
	}

	src_state.m_Pos.x = cmp_state.m_Pos.x;
	src_state.m_Pos.y = cmp_state.m_Pos.y;

	//	Global::logger->debug("-->>>Move <x=%f,y=%f>",
	//						  src_state.m_Pos.x, src_state.m_Pos.y);
	return true;
}

/** 
 * @brief 50ms更新一次，主要move
 * 
 */
void MODI_Scene::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
// 	if(m_pGameRoom == NULL)
// 	{
// 		return;
// 	}

// 	if(m_pGameRoom->GetRoomStatus() < ROOM_STATUS_START)
// 	{
// 		return;
// 	}
	
	if(m_AllSceneAttr[enSceneAttrPlayer].size() == 0)
	{
		return;
	}
	
	defSceneAttrSet::iterator iter = m_AllSceneAttr[enSceneAttrPlayer].begin();
	for(; iter != m_AllSceneAttr[enSceneAttrPlayer].end();iter++)
	{
		MODI_MoveableObject * p_attr = *iter;
		if(! p_attr)
		{
			MODI_ASSERT(0);
			continue;
		}

		if(! CheckMove(p_attr->m_stSrcState, p_attr->m_stDestState))
		{
			continue;
		}

		//		Global::logger->debug("-->>>Move2 <x=%f,y=%f>",
		//				  p_attr->m_stSrcState.m_Pos.x, p_attr->m_stSrcState.m_Pos.y);

		/// 判断是否换屏
		UpdateAttr(p_attr);
	}	

	/// 同步
	if(m_stSyncClientPos(cur_rtime))
	{
		defSceneAttrSet::iterator iter = m_AllSceneAttr[enSceneAttrPlayer].begin();
		for(; iter != m_AllSceneAttr[enSceneAttrPlayer].end();iter++)
		{
			MODI_MoveableObject * p_attr = *iter;
			if(! p_attr)
			{
				MODI_ASSERT(0);
				continue;
			}
		
			//SyncAttr(p_attr);
		}
	}
}


void MODI_Scene::Reset()
{
	m_blUsed = false;
	m_pGameRoom = NULL;
	m_AllSceneAttr[enSceneAttrPlayer].clear();
	m_SceneAttrIndex[enSceneAttrPlayer].clear();
}


/** 
 * @brief 物品更新
 * 
 * @param p_attr 需要更新的物品
 * 
 * @return 成功ture
 *
 */
bool MODI_Scene::UpdateAttr(MODI_MoveableObject * p_attr)
{
	if(p_attr == NULL)
	{
		MODI_ASSERT(0);
		return false;
	}

	/// 已经在里面了
	if(p_attr->IsInScene())
	{
		return true;
		DWORD get_screen = GetScreenNum((DWORD)p_attr->m_stSrcState.m_Pos.x, (DWORD)p_attr->m_stSrcState.m_Pos.y);
		/// 换屏了
		if(get_screen != p_attr->GetScreen())
		{
#ifdef _DEBUG
			Global::logger->debug("[scene_info] move <oldscreen=%u,newscreen=%u,x=%f,y=%f>", p_attr->GetScreen(), get_screen,
								  p_attr->m_stSrcState.m_Pos.x, p_attr->m_stSrcState.m_Pos.y);
#endif
			/// 加入屏
			m_SceneAttrIndex[p_attr->GetType()][p_attr->GetScreen()].erase(p_attr);
			m_SceneAttrIndex[p_attr->GetType()][get_screen].insert(p_attr);
			p_attr->SetScreen(get_screen);
		
			/// 广播
			//DelMeToNine(p_attr->GetScreen(), p_attr);
			//SendMeToNine(get_screen, p_attr);
			//SendNineToMe(get_screen, p_attr);
			
			p_attr->SetScreen(get_screen);
		}
	}
	
	/// 第一次进入
	else
	{
		/// 随机一个坐标
		MODI_MoveState src, dest;
		DWORD i = PublicFun::GetRandNum(0, m_SceneBornVec.size() - 1);
		DWORD rand_pos = m_SceneBornVec[i];

		src.m_Pos.x = (rand_pos%m_dwWidth) * GRID_DIMENSION + GRID_DIMENSION/2;
		src.m_Pos.y = (rand_pos/m_dwWidth) * GRID_DIMENSION + GRID_DIMENSION/2;

		//src.m_Pos.x = m_dwWidth / 2;
		//src.m_Pos.y = m_dwHeight / 2;

		Global::logger->debug("[rand_pos] client rand pos <pos=%u,rand_pos=%u,x=%f,y=%f>",
							  i, rand_pos, src.m_Pos.x,src.m_Pos.y);
		src.m_fDir.x = 1;
		src.m_fDir.y = 0;
		src.m_fCharDir = src.m_fDir;
		p_attr->SetSrcState(src);
		p_attr->SetDestState(src);

		DWORD get_screen = GetScreenNum((DWORD)src.m_Pos.x, (DWORD)src.m_Pos.y);
		
#ifdef _DEBUG
		Global::logger->debug("[scene_info] first enter scene <screen=%u>", get_screen);
#endif		
		
		/// 加入屏
		m_AllSceneAttr[p_attr->GetType()].insert(p_attr);
		m_SceneAttrIndex[p_attr->GetType()][get_screen].insert(p_attr);
		p_attr->SetScreen(get_screen);
		p_attr->SetInScene(true);
		
		/// 广播
		SendMeToNine(get_screen, p_attr);
		SendNineToMe(get_screen, p_attr);

		
	}

	return true;
}


/** 
 * @brief 物品更新
 * 
 * @param p_attr 需要更新的物品
 * 
 * @return 成功ture
 *
 */
bool MODI_Scene::SyncAttr(MODI_MoveableObject * p_attr)
{
	if(p_attr == NULL)
	{
		MODI_ASSERT(0);
		return false;
	}

	/// 已经在里面了
	if(p_attr->IsInScene())
	{
		SyncMeToNine(p_attr->GetScreen(), p_attr);
	}
	return true;
}


void MODI_Scene::SyncMeToNine(const DWORD screen, MODI_MoveableObject * p_attr)
{
	MODI_GS2C_Adjuest_MoveState_Cmd send_cmd;
	send_cmd.m_stGuid = p_attr->GetGUID();
	send_cmd.m_stMoveState = p_attr->GetMoveState();
	std::vector<DWORD> & nine_screen_ref = m_NineSceneIndex[screen];
	if(nine_screen_ref.size() == 0)
	{
		Global::logger->debug("[sync_me_to_nine] load nine screen failed <screen=%u>", screen);
		MODI_ASSERT(0);
		return;
	}
#ifdef _DEBUG	
	else
	{
		std::vector<DWORD>::iterator test_screen_iter = nine_screen_ref.begin();
		for(; test_screen_iter != nine_screen_ref.end(); test_screen_iter++)
		{
			Global::logger->debug("[nine_screen] <screen=%u,nine_screen=%u>",
								  screen, *test_screen_iter);
		}
	}
#endif	

	std::vector<DWORD>::iterator screen_iter = nine_screen_ref.begin();
	for(; screen_iter != nine_screen_ref.end(); screen_iter++)
	{
		defSceneAttrSet & ref_set = m_SceneAttrIndex[p_attr->GetType()][*screen_iter];
		if(ref_set.size() == 0)
		{
#ifdef _DEBUG			
			Global::logger->debug("[sync_me_to_nine] not have any attr <screen=%u,nine_screen=%u>",
								  screen, *screen_iter);
#endif			
			continue;
		}
	
		defSceneAttrSet::iterator iter = ref_set.begin();
		for(; iter != ref_set.end(); iter++)
		{
			MODI_BaseObject * p_obj = *iter;
			if(p_obj->IsClientAvatar())
			{
				MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_obj;
				p_client->SendPackage(&send_cmd, sizeof(send_cmd));
#ifdef _DEBUG			
				Global::logger->debug("[sync_me_to_nine] sync me to nine <screen=%u,nine_screen=%u,attr=%s>",
									  screen, *screen_iter, p_client->GetAccName());
									  
									  
#endif									  
			}
			
		}
	}
}


void MODI_Scene::DelMeToNine(const DWORD screen, MODI_MoveableObject * p_attr)
{
	MODI_GS2C_Del_SceneUser_Cmd send_cmd;
	send_cmd.m_stGuid = p_attr->GetGUID();
	
	std::vector<DWORD> & nine_screen_ref = m_NineSceneIndex[screen];
	if(nine_screen_ref.size() == 0)
	{
		Global::logger->debug("[del_me_to_nine] load nine screen failed <screen=%u>", screen);
		MODI_ASSERT(0);
		return;
	}
#ifdef _DEBUG	
	else
	{
		std::vector<DWORD>::iterator test_screen_iter = nine_screen_ref.begin();
		for(; test_screen_iter != nine_screen_ref.end(); test_screen_iter++)
		{
			Global::logger->debug("[nine_screen] <screen=%u,nine_screen=%u>",
								  screen, *test_screen_iter);
		}
	}
#endif	

	std::vector<DWORD>::iterator screen_iter = nine_screen_ref.begin();
	for(; screen_iter != nine_screen_ref.end(); screen_iter++)
	{
		defSceneAttrSet & ref_set = m_SceneAttrIndex[p_attr->GetType()][*screen_iter];
		if(ref_set.size() == 0)
		{
#ifdef _DEBUG			
			Global::logger->debug("[del_me_to_nine] not have any attr <screen=%u,nine_screen=%u>",
								  screen, *screen_iter);
#endif			
			continue;
		}
	
		defSceneAttrSet::iterator iter = ref_set.begin();
		for(; iter != ref_set.end(); iter++)
		{
			MODI_BaseObject * p_obj = *iter;
			if(p_obj->IsClientAvatar())
			{
				MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_obj;
				p_client->SendPackage(&send_cmd, sizeof(send_cmd));
#ifdef _DEBUG			
				Global::logger->debug("[del_me_to_nine] del me to nine <screen=%u,nine_screen=%u,attr=%s>",
									  screen, *screen_iter, p_client->GetAccName());
									  
									  
#endif									  
			}
			
		}
	}
}


#if 0
void MODI_Scene::SendNineToMe(const DWORD screen, MODI_MoveableObject * p_attr)
{
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Update_SceneUser_Cmd * p_send_cmd = (MODI_GS2C_Update_SceneUser_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	std::vector<DWORD> & nine_screen_ref = m_NineSceneIndex[screen];
	if(nine_screen_ref.size() == 0)
	{
		Global::logger->debug("[send_nine_to_me] load nine screen failed <screen=%u>", screen);
		MODI_ASSERT(0);
		return;
	}

#ifdef _DEBUG	
	else
	{
		std::vector<DWORD>::iterator test_screen_iter = nine_screen_ref.begin();
		for(; test_screen_iter != nine_screen_ref.end(); test_screen_iter++)
		{
			Global::logger->debug("[nine_screen] <screen=%u,nine_screen=%u>",
								  screen, *test_screen_iter);
		}
	}
#endif
	
	MODI_SceneUserInfo * p_startaddr = p_send_cmd->m_pData;
	WORD i = 0;
		
	std::vector<DWORD>::iterator screen_iter = nine_screen_ref.begin();
	for(; screen_iter != nine_screen_ref.end(); screen_iter++)
	{
		defSceneAttrSet & ref_set = m_SceneAttrIndex[p_attr->GetType()][*screen_iter];
		if(ref_set.size() == 0)
		{
#ifdef _DEBUG			
			Global::logger->debug("[send_nine_to_me] not have any attr <screen=%u,nine_screen=%u>",
								  screen, *screen_iter);
#endif			
			continue;
		}
	
		defSceneAttrSet::iterator iter = ref_set.begin();
		for(; iter != ref_set.end(); iter++, i++)
		{
			MODI_MoveableObject* p_obj = *iter;
			if(p_obj)
			{
				p_startaddr->m_stGuid = p_obj->GetGUID();
				p_startaddr->m_stMoveState = p_obj->GetMoveState();
#ifdef _DEBUG
				Global::logger->debug("[send_nine_to_me] client state is <guid=%llu, x=%f,y=%f>",
									  (QWORD)p_obj->GetGUID(), p_startaddr->m_stMoveState.m_Pos.x, p_startaddr->m_stMoveState.m_Pos.y);
#endif				
			}
			else
			{
				MODI_ASSERT(0);
			}
			p_startaddr++;
		}
#ifdef _DEBUG			
		Global::logger->debug("[send_nine_to_me] have any attr <screen=%u,nine_screen=%u,attr=%u>",
							  screen, *screen_iter, i);
#endif			
	}
	
	p_send_cmd->m_wdSize = i;
	MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_attr;
	p_client->SendPackage(p_send_cmd, sizeof(MODI_GS2C_Update_SceneUser_Cmd) + sizeof(MODI_SceneUserInfo) * i);
#ifdef _DEBUG
	Global::logger->debug("[send_nine_to_me] send a nine to cmd");
#endif	
}
#endif


void MODI_Scene::SendNineToMe(const DWORD screen, MODI_MoveableObject * p_attr)
{
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Update_SceneUser_Cmd * p_send_cmd = (MODI_GS2C_Update_SceneUser_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	MODI_SceneUserInfo * p_startaddr = p_send_cmd->m_pData;
	WORD i = 0;

	if(m_AllSceneAttr[enSceneAttrPlayer].size() == 0)
	{
		return;
	}
	
	defSceneAttrSet::iterator iter = m_AllSceneAttr[enSceneAttrPlayer].begin();
	for(; iter != m_AllSceneAttr[enSceneAttrPlayer].end();iter++,i++)
	{
		MODI_MoveableObject* p_obj = *iter;
		if(p_obj)
		{
			p_startaddr->m_stGuid = p_obj->GetGUID();
			p_startaddr->m_stMoveState = p_obj->GetMoveState();
			Global::logger->debug("-->>>send nine to me  add a user <%llu,x=%f,y=%f>", (QWORD)p_obj->GetGUID(),
								  p_startaddr->m_stMoveState.m_Pos.x, p_startaddr->m_stMoveState.m_Pos.y);
		}
		else
		{
			MODI_ASSERT(0);
		}
		p_startaddr++;
	}
	
	p_send_cmd->m_wdSize = i;
	MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_attr;
	p_client->SendPackage(p_send_cmd, sizeof(MODI_GS2C_Update_SceneUser_Cmd) + sizeof(MODI_SceneUserInfo) * i);
	Global::logger->debug("send nine to me count <%u>", i);
}


#if 0
void MODI_Scene::SendMeToNine(const DWORD screen, MODI_MoveableObject * p_attr)
{
	MODI_GS2C_Add_SceneUser_Cmd send_cmd;
	send_cmd.m_stAddUser.m_stGuid = p_attr->GetGUID();
	send_cmd.m_stAddUser.m_stMoveState = p_attr->GetMoveState();
	
	std::vector<DWORD> & nine_screen_ref = m_NineSceneIndex[screen];
	if(nine_screen_ref.size() == 0)
	{
		Global::logger->debug("[get_nine_screen] send me to nine load nine screen failed <screen=%u>", screen);
		MODI_ASSERT(0);
		return;
	}
#ifdef _DEBUG	
	else
	{
		std::vector<DWORD>::iterator test_screen_iter = nine_screen_ref.begin();
		for(; test_screen_iter != nine_screen_ref.end(); test_screen_iter++)
		{
			Global::logger->debug("[nine_screen] <screen=%u,nine_screen=%u>",
								  screen, *test_screen_iter);
		}
	}
#endif	

	std::vector<DWORD>::iterator screen_iter = nine_screen_ref.begin();
	for(; screen_iter != nine_screen_ref.end(); screen_iter++)
	{
		defSceneAttrSet & ref_set = m_SceneAttrIndex[p_attr->GetType()][*screen_iter];
		if(ref_set.size() == 0)
		{
#ifdef _DEBUG			
			Global::logger->debug("[send_me_to_nine] not have any attr <screen=%u,nine_screen=%u>",
								  screen, *screen_iter);
#endif			
			continue;
		}
	
		defSceneAttrSet::iterator iter = ref_set.begin();
		for(; iter != ref_set.end(); iter++)
		{
			MODI_BaseObject * p_obj = *iter;
			if(p_obj->IsClientAvatar())
			{
				MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_obj;
				p_client->SendPackage(&send_cmd, sizeof(send_cmd));
#ifdef _DEBUG			
				Global::logger->debug("[send_me_to_nine] send_me_to_nine <screen=%u,nine_screen=%u,attr=%s>",
									  screen, *screen_iter, p_client->GetAccName());
									  
									  
#endif									  
			}
			
		}
	}
}

#endif


void MODI_Scene::SendMeToNine(const DWORD screen, MODI_MoveableObject * p_attr)
{
	MODI_GS2C_Add_SceneUser_Cmd send_cmd;
	send_cmd.m_stAddUser.m_stGuid = p_attr->GetGUID();
	send_cmd.m_stAddUser.m_stMoveState = p_attr->GetMoveState();
	Global::logger->debug("-->>send me to nine <guid=%llu,x=%f,y=%f>",
						  (QWORD)p_attr->GetGUID(), send_cmd.m_stAddUser.m_stMoveState.m_Pos.x,send_cmd.m_stAddUser.m_stMoveState.m_Pos.y);
	Broadcast(&send_cmd, sizeof(send_cmd));
}


DWORD MODI_Scene::GetScreenNum(const DWORD x, const DWORD y)
{
	DWORD screen = m_dwSceneWidth * (y/(GRID_DIMENSION * SCREEN_HEIGHT)) + x/(GRID_DIMENSION * SCREEN_WIDTH);
	return screen;
}

/** 
 * @brief 删除某个物品
 * 
 * @param p_attr 场景里面的物品
 * 
 * @return 成功ture
 *
 */
bool MODI_Scene::RemoveAttr(MODI_MoveableObject * p_attr)
{
	if(! p_attr)
	{
		return false;
	}
	
	DelMeToNine(p_attr->GetScreen(), p_attr);

	p_attr->SetInScene(false);
	m_AllSceneAttr[p_attr->GetType()].erase(p_attr);
	m_SceneAttrIndex[p_attr->GetType()][p_attr->GetScreen()].erase(p_attr);
	return false;
}


void MODI_Scene::SyncAllPos()
{
	if(m_AllSceneAttr[enSceneAttrPlayer].size() == 0)
	{
		return;
	}
	
	defSceneAttrSet::iterator iter = m_AllSceneAttr[enSceneAttrPlayer].begin();
	for(; iter != m_AllSceneAttr[enSceneAttrPlayer].end();iter++)
	{
		MODI_MoveableObject * p_attr = *iter;
		if(! p_attr)
		{
			continue;
		}

		Global::logger->debug("-->>>>>sync all pos <guid=%llu>", (QWORD)p_attr->GetGUID());
		SendMeToNine(p_attr->GetScreen(), p_attr);
		SendNineToMe(p_attr->GetScreen(), p_attr);
	}	
}


void MODI_Scene::AllAttrStop()
{
	defSceneAttrSet & attr_set_ref = m_AllSceneAttr[enSceneAttrPlayer];
	if(attr_set_ref.size() == 0)
	{
		return;
	}
	
	defSceneAttrSet::iterator iter = attr_set_ref.begin();
	for(; iter != attr_set_ref.end(); iter++)
	{
		MODI_MoveableObject * p_obj = *iter;
		if(p_obj)
		{
			p_obj->StopMove();
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
}


void MODI_Scene::SendCmdToNine(MODI_MoveableObject * p_attr, const stNullCmd * pt_null_cmd, const DWORD cmd_size)
{
	WORD screen = p_attr->GetScreen();
	std::vector<DWORD> & nine_screen_ref = m_NineSceneIndex[screen];
	if(nine_screen_ref.size() == 0)
	{
		Global::logger->debug("[get_nine_screen] send me to nine load nine screen failed <screen=%u>", screen);
		MODI_ASSERT(0);
		return;
	}


	std::vector<DWORD>::iterator screen_iter = nine_screen_ref.begin();
	for(; screen_iter != nine_screen_ref.end(); screen_iter++)
	{
		defSceneAttrSet & ref_set = m_SceneAttrIndex[p_attr->GetType()][*screen_iter];
		if(ref_set.size() == 0)
		{
			continue;
		}
	
		defSceneAttrSet::iterator iter = ref_set.begin();
		for(; iter != ref_set.end(); iter++)
		{
			MODI_BaseObject * p_obj = *iter;
			if(p_obj->IsClientAvatar())
			{
				MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_obj;
				p_client->SendPackage(pt_null_cmd, cmd_size);
			}
			
		}
	}	
}


void MODI_Scene::Broadcast(const stNullCmd * pt_null_cmd, const DWORD cmd_size)
{
	if(m_AllSceneAttr[enSceneAttrPlayer].size() == 0)
	{
		return;
	}
	
	defSceneAttrSet::iterator iter = m_AllSceneAttr[enSceneAttrPlayer].begin();
	for(; iter != m_AllSceneAttr[enSceneAttrPlayer].end();iter++)
	{
		MODI_MoveableObject * p_attr = *iter;
		if(! p_attr)
		{
			continue;
		}
		MODI_ClientAvatar * p_client = (MODI_ClientAvatar *)p_attr;
		Global::logger->debug("broad a cmd to user <guid=%llu>", (QWORD)p_attr->GetGUID());
		p_client->SendPackage(pt_null_cmd, cmd_size);
	}		
}
