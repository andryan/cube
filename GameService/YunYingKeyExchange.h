/** 
 * @file YunYingKeyExchange.h
 * @brief 运营活动用的CDKEY兑换功能
 * @author Tang Teng
 * @version v0.1
 * @date 2010-11-29
 */
#ifndef MODI_YUNYING_KEY_EXCHANGE_H_
#define MODI_YUNYING_KEY_EXCHANGE_H_

#include "SingleObject.h"
#include "ClientAvatar.h"

#define	NO_LIMIT_KEY	0

#define	CDKEY_DATE_LEN	4
#define	CDKEY_CLASS_LEN	3

#define	CDKEY_DATE_OFF	7
#define	CDKEY_CLASS_OFF	11

class 	MODI_YunYingKeyExchange : public CSingleObject<MODI_YunYingKeyExchange>
{
	public:
		MODI_YunYingKeyExchange();
		virtual ~MODI_YunYingKeyExchange();


	public:

		enYunYingKeyExchangeResult 	Exchange( MODI_ClientAvatar * pClient , const char * szKey , const char * szContent , 
				unsigned int nContentLen , MODI_GoodsPackageArray & goods );

		//static bool 	StrConvToGoodsPackage( MODI_GoodsPackageArray & goods , const char * szContent , unsigned int nContentLen );
};




#endif
