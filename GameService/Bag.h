/**
 * @file   Bag.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 16:08:09 2011
 * 
 * @brief  包裹对象
 * 
 */


#ifndef MODI_GAME_BAG_H
#define MODI_GAME_BAG_H


#include "AssertEx.h"
#include "Global.h"
#include "BagPosInfo.h"
#include "ItemOperator.h"
class MODI_ClientAvatar;


/** 
 * @brief 游戏中的背包
 *
 */
class MODI_Bag: public MODI_DisableCopy
{
 public:
	/// 道具id对应道具信息
	typedef std::map<QWORD, MODI_GameItemInfo * > defItemInfoMap;
	typedef std::map<QWORD, MODI_GameItemInfo * >::iterator defItemInfoIter;
	typedef std::map<QWORD, MODI_GameItemInfo * >::value_type defItemInfoValue;

	/// config对应的包裹位置信息
	typedef std::multimap<DWORD, MODI_BagPosInfo  * > defPosInfoMap;
	typedef std::multimap<DWORD, MODI_BagPosInfo * >::iterator defPosInfoIter;
	typedef std::multimap<DWORD, MODI_BagPosInfo * >::value_type defPosInfoValue;
	typedef std::pair<defPosInfoIter, defPosInfoIter > defPosInfoRange;
	
	MODI_Bag(MODI_ClientAvatar * pOwner, const MODI_PackageType & type);
		
	~MODI_Bag();

	/// 备份
	void Backup();

	/// 还原
	void ReBackup();


	/** 
	 * @brief 获取包裹信息
	 * 
	 * @param pos 位置信息
	 * 
	 * @return 包裹信息
	 *
	 */
	MODI_BagPosInfo * GetPosInfo(const WORD & pos)
	{
		MODI_BagPosInfo * p_info = NULL;
		if(IsValidPos(pos))
		{
			p_info = &m_stBagPosInfo[pos];
		}
		else
		{
			Global::logger->error("[get_pos_info] pos invalid <pos=%u,bagtype=%d>",pos,GetBagType());
			MODI_ASSERT(0);
		}
		return p_info;
	}


	/** 
	 * @brief 更新包裹信息
	 * 
	 * @param config_id id
	 * @param p_info 位置
	 */
	void UpdatePosInfo(const DWORD config_id, MODI_BagPosInfo * p_info);

	
	/** 
	 * @brief 更新
	 * 
	 */
	void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);


	/** 
	 * @brief 添加一个物品
	 * 
	 * @param create_info 此物品信息
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool AddItemToBag(const MODI_CreateItemInfo & create_info, QWORD & get_item_id);


	/** 
	 * @brief 添加一个物品,但不立刻写入到数据库中
	 * 
	 * @param create_info 此物品信息
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool AddItemToBag(const MODI_CreateItemInfo & create_info, QWORD & get_item_id, MODI_ByteBuffer & save_sql);


	/** 
	 * @brief 从包裹里面删除某件道具
	 * 
	 * @param item_id 道具id
	 * 
	 * @return 成功true
	 *
	 */
	bool DelItemFromBag(const QWORD & item_id, const enDelItemReason del_reason);


	/** 
	 * @brief 增加一个道具,且要分配位置信息
	 * 
	 * @param p_item 此道具
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool AddItemUpdate(MODI_GameItemInfo * p_item);


	/** 
	 * @brief 增加一个道具,不需要分配位置信息
	 * 
	 * @param p_item 此道具
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool AddItemNoUpdate(MODI_GameItemInfo * p_item);


	/** 
	 * @brief 删除道具,除了更新位置信息，还要删除道具
	 * 
	 * @param p_item 道具
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool DelItemUpdate(MODI_GameItemInfo * p_item);


	/** 
	 * @brief 删除道具,只是更新包裹位置，主要是换衣服
	 * 
	 * @param p_item 道具
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool DelItemNoUpdate(MODI_GameItemInfo * p_item);


	/** 
	 * @brief 是否拥有道具
	 * 
	 * @param item_id 道具id
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool IsHaveItem(const QWORD & item_id);


	/** 
	 * @brief 根据位置获取物品
	 * 
	 * @param pos 位置信息
	 * 
	 * @return 物品
	 *
	 */
	MODI_GameItemInfo * GetItemByPos(const WORD & pos);


	/** 
	 * @brief 根据道具id获取某个道具
	 * 
	 * @param item_id 道具id
	 * 
	 * @return null则是失败
	 *
	 */
	MODI_GameItemInfo * GetItemByItemId(const QWORD & item_id);
	

	/** 
	 * @brief 根据配置id获取某个道具
	 * 
	 * @param config_id 物品属性id
	 * 
	 * @return null 失败
	 */
	MODI_GameItemInfo *  GetItemByConfigId(const DWORD & config_id);


	/** 
	 * @brief 获取某个道具
	 * 
	 * @param type 道具类型(喇叭)
	 *
	 * @param sub_class 子类型(频道喇叭)
	 * 
	 * @return null失败
	 *
	 */
	MODI_GameItemInfo * GetItemBySubClass(const MODI_ItemType & type, const WORD & sub_class);
	
	
	/** 
	 * @brief 获得背包中某种类（大／小)型物品的数量
	 * 
	 * @param item_type 物品类型
	 * 
	 * @return 	返回数量
	 *
	 */
	WORD GetCountByItemType(const MODI_ItemType & item_type);

	
	/** 
	 * @brief 获得背包中某种物品的数量
	 * 
	 * @param config_id 物品配置ID
	 * 
	 * @return 返回数量
	 *
	 */
	WORD GetCountByItemConfigId(const WORD & config_id);
	

	/** 
	 * @brief 从背包中获取一个空的位置
	 * 
	 * @return 	返回背包中的一个空位置
	 *
	 */
	MODI_BagPosInfo * GetEmptyPos();


	/** 
	 * @brief 位置下标是否正确
	 * 
	 * @param nPos 位置
	 * 
	 * @return 位置
	 *
	 */
	bool IsValidPos(const WORD & nPos)
	{
		if( nPos != INVAILD_POS_INBAG && nPos >= 1 && nPos <= this->GetSize() )
		{
			return true;
		}
		return false;
	}
	
	
	/** 
	 * @brief 背包是否已满
	 * 
	 * @return 	如果背包满，返回true
	 *
	 */
	bool IsFull() const;

	
	/** 
	 * @brief 获得背包中空位置的总数
	 * 
	 * @return 	返回空位置的总数
	 *
	 */
	const WORD GetEmptyPosCount() const;
	
	
	/** 
	 * @brief 获得背包的大小
	 * 
	 * @return 	返回背包的空间大小
	 *
	 */
	WORD GetSize() const
	{
		return m_wdBagSize;
	}

	
	/** 
	 * @brief 获得背包类型
	 * 
	 * @return 	返回背包类型
	 */
	MODI_PackageType GetBagType() const
	{
		return m_bagType; 
	}


	/** 
	 * @brief 获取包裹属于谁
	 * 
	 */
	MODI_ClientAvatar * GetOwner()
	{
		return m_pOwner;
	}


	/** 
	 * @brief 发送道具列表
	 * 
	 * @param p_item_info 道具信息
	 * 
	 * @return 道具个数
	 *
	 */
	const WORD NotifyItemList();

 private:
	/// 清空
	void Reset();
	
	/// 包裹大小
	MODI_BagPosInfo m_stBagPosInfo[MAX_BAG_AVATAR_COUNT + MAX_BAG_PLAYER_COUNT];

	/// 道具操作
	defItemInfoMap m_stItemInfoMap;

	/// 道具备份操作
	defItemInfoMap m_stItemInfoMapBackup;

	/// 包裹操作 
	defPosInfoMap m_stPosInfoMap;
	
	/// 背包大小
	WORD m_wdBagSize;
		
	/// 背包类型
	MODI_PackageType m_bagType; 

	/// 拥有者
	MODI_ClientAvatar * m_pOwner;
};

#endif
