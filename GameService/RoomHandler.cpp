#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "BinFileMgr.h"
#include "protocol/svrresult.h"
#include "Share.h"

void SendResultForDebug( MODI_ClientAvatar * pClient ,  unsigned short n )
{
#ifdef _DEBUG
	MODI_GS2C_Respone_OptResult notify;
	notify.m_nResult = n;
	pClient->SendPackage( &notify , sizeof(notify) );
#endif
}

///    准备请求
int MODI_PackageHandler_c2gs::Ready_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_Ready , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent ready request. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug(pClient , SvrResult_Room_NotInRoomButSendReady);
        return enPHandler_Kick;
    }

    if (pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is room <%u> master, but sent ready request.", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug(pClient , SvrResult_Room_IsMasterButSendReady );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent ready request.", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug(pClient , SvrResult_Room_IsStartButSendReady);
        return enPHandler_Warning;
    }

 //   MODI_C2GS_Request_Ready & req = *((MODI_C2GS_Request_Ready *)(pt_null_cmd));
    ///    是否参战玩家
    if (!room->IsPlayerClient(pClient))
    {
        Global::logger->warn("[%s] player <%s> is spectator, but sent ready request.", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug(pClient, SvrResult_Room_IsNotPlayerButSendReady );
        return enPHandler_Warning;
    }

    ///    改变为准备状态
    room->ChangeReady(pClient, true);
    return enPHandler_OK;
}

///    取消准备
int MODI_PackageHandler_c2gs::UnReady_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_UnReady , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent cancel ready request. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendUnReady );
        return enPHandler_Kick;
    }

    if (pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is room<%u> master, but sent cancel ready request.", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsMasterButSendUnReady );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent cancel ready request.", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendUnReady );
        return enPHandler_Kick;
    }

   // MODI_C2GS_Request_UnReady & req = *((MODI_C2GS_Request_UnReady *)(pt_null_cmd));
    if (!room->IsPlayerClient(pClient))
    {
        Global::logger->warn("[%s] player <%s> is spectator, but sent cancel ready request.", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsNotPlayerButSendUnReady );
        return enPHandler_Warning;
    }

    ///    改变为未准备状态
    room->ChangeReady(pClient, false);
    return enPHandler_OK;
}

///    房主开始游戏请求
int MODI_PackageHandler_c2gs::Start_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_Start , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent start game request.", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendStart );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent start game request. ", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendStart  );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent start game request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendStart );
        return enPHandler_Warning;
    }

  //  MODI_C2GS_Request_Start & req = *((MODI_C2GS_Request_Start *)(pt_null_cmd));
    //    尝试开始游戏
    if (!room->Start())
    {

        Global::logger->warn("[%s] room <%u> fail to start game, somebody is not ready.", GS_ROOMOPT, pClient->GetRoomID());
        // 返回结果
        MODI_GS2C_Respone_OptResult result;
        result.m_nResult = SvrResult_Room_CanNotStart;
        pClient->SendPackage(&result,sizeof(result));
    }

    return enPHandler_OK;
}

///    房主踢人请求
int MODI_PackageHandler_c2gs::KickPlayer_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_KickPlayer , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent kick player request. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendKick );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent kick player request. ", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendKick );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent kick player request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendKick );
        return enPHandler_Kick;
    }

    MODI_C2GS_Request_KickPlayer & req = *((MODI_C2GS_Request_KickPlayer *)(pt_null_cmd));
	bool bPlayer = false;
	defRoomPosSolt nPos;
	RoleHelpFuns::DecodePosSolt( req.m_kickPos , bPlayer , nPos );
	if( bPlayer && nPos < room->GetMaxPlayerInitialSetting() )
	{
		room->KickSb(pClient , req.m_kickPos);
	}
	else if( nPos < room->GetMaxSpectorInitalSetting() )
	{
		room->KickSb(pClient , req.m_kickPos);
	}

    return enPHandler_OK;
}

///    房主开位置请求
int MODI_PackageHandler_c2gs::OnPostion_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_OnPos , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent open pos request. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendOnPos );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent open pos request. ", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendOnPos );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent open pos request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendOnPos );

        return enPHandler_Kick;
    }

    MODI_C2GS_Request_OnPos & req = *((MODI_C2GS_Request_OnPos *)(pt_null_cmd));

    // 开位置
    if (!room->OnPostion(pClient , true, req.m_targetPos))
    {
        Global::logger->warn("[%s] player <%s> fail to open pos.", GS_ROOMOPT, pClient->GetRoleName());
        return enPHandler_OK;
    }

    return enPHandler_OK;
}

///    房主关位置请求
int MODI_PackageHandler_c2gs::OffPostion_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_OffPos , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent close pos request. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendOffPos );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master but sent close pos request.", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendOffPos );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent close pos request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendOffPos );
        return enPHandler_Kick;
    }

    MODI_C2GS_Request_OffPos & req = *((MODI_C2GS_Request_OffPos *)(pt_null_cmd));

    if (!room->OffPostion(pClient , true, req.m_targetPos, false ))
    {
        Global::logger->warn("[%s] player <%s> fail to close pos.", GS_ROOMOPT, pClient->GetRoleName());
        return enPHandler_OK;
    }

    return enPHandler_OK;
}

///   房主请求更换音乐
int MODI_PackageHandler_c2gs::ChangeMusic_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ChangeMusic , cmd_size , 0 , 0, pClient );


    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent change music request.", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendChangeMusic );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room <%u> master, but sent change music request. ", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendChangeMusic );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent change music request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendChangeMusic );
        return enPHandler_Kick;
    }

	MODI_C2GS_Request_ChangeMusic & req = *((MODI_C2GS_Request_ChangeMusic *)(pt_null_cmd));
	if( req.m_musicID != RANDOM_MUSIC_ID )
	{
		const GameTable::MODI_Musiclist * pMusicInfo =
				MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get(req.m_musicID);
		if (!pMusicInfo)
		{
			Global::logger->fatal("[%s] fail to change music : invalid music ID<%u> . player <%s> , room <%u> ", 
					GS_ROOMOPT ,
					req.m_musicID,
					pClient->GetRoleName() , 
					pClient->GetRoomID() );
			return enPHandler_Kick;
		}
	}

    room->ChangeMusicID(req.m_musicID , req.m_flag ,req.m_songType , req.m_mt );

    // 其他相关判断
    return enPHandler_OK;
}

///   房主请求更换场景
int MODI_PackageHandler_c2gs::ChangeMap_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ChangeMap , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent change scene request.", 
				GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_NotInRoomButSendChangeMap );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent change scene request. ", 
				GS_ROOMOPT,
                pClient->GetRoleName() , 
				pClient->GetRoomID() );
		SendResultForDebug( pClient , SvrResult_Room_IsNotMasterButSendChangeMap );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent change scene request.  ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
		SendResultForDebug( pClient , SvrResult_Room_IsStartButSendChangeMap );
        return enPHandler_Kick;
    }

	MODI_C2GS_Request_ChangeMap & req = *((MODI_C2GS_Request_ChangeMap *)(pt_null_cmd));
	if(req.m_MapID != 0)
	{
		const GameTable::MODI_Scenelist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetSceneBinFile().Get( req.m_MapID );
		if( !pItem )
			return enPHandler_Warning;

		bool bCheckOk = true;
		// 房间类型是否和所选场景匹配
		if( room->GetRoomType() == ROOM_TYPE_NORMAL && !pItem->get_Small() )
		{
			bCheckOk = false;
		}
		else if( room->GetRoomType() == ROOM_TYPE_BIG && !pItem->get_Large() )
		{
			bCheckOk = false;
		}

		if( pItem->get_Type() != kGameScene )
		{
			bCheckOk = false;
		}

		if( bCheckOk )
		{
			room->ChangeMapID(req.m_MapID);
		}
		else 
		{
			Global::logger->warn("[%s] room <%u> client<charid=%u,name=%s> change scene request faild, param vaild.  ", 
				GS_ROOMOPT ,
				pClient->GetRoomID(),
				pClient->GetCharID(),
				pClient->GetRoleName());
		}

   		return enPHandler_OK;
	}
	else// 随机
	{
		room->ChangeMapID(RANDOM_MAP_ID);
		return enPHandler_OK;
	}
}

int MODI_PackageHandler_c2gs::ChangeGameMode_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ChangeGameMode , cmd_size , 0 , 0, pClient );

    if (!pClient->IsInMulitRoom())
    {
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent change scene request.  ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return enPHandler_Kick;
    }

	const MODI_C2GS_Request_ChangeGameMode * pReq = (const MODI_C2GS_Request_ChangeGameMode *)(pt_null_cmd);
	room->ChangeGameMode( pReq->m_newSubType );
    return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::ChangePostion_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_ChangePostion ,cmd_size , 0 ,0,pClient );

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent change pos request. ", GS_ROOMOPT,
                pClient->GetRoleName());
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent change pos request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
        return enPHandler_Kick;
    }

   // MODI_C2GS_Request_ChangePostion & req = *((MODI_C2GS_Request_ChangePostion *)(pt_null_cmd));

	room->ExchangePostion( pClient );
    return enPHandler_OK;
}

///    房主更改房间信息
int MODI_PackageHandler_c2gs::ChangeRoomInfo_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd,
			const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_ChangeRoomInfo ,cmd_size , 0 ,0,pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent update room info request. ", GS_ROOMOPT,
                pClient->GetRoleName());

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
		pClient->SendPackage( &res , sizeof(res) );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent update room info request.", GS_ROOMOPT,
                pClient->GetRoleName() , pClient->GetRoomID() );

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
		pClient->SendPackage( &res , sizeof(res) );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent update room's information request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
		pClient->SendPackage( &res , sizeof(res) );

        return enPHandler_Kick;
    }

	MODI_C2GS_Request_ChangeRoomInfo * pReq = (MODI_C2GS_Request_ChangeRoomInfo *)(pt_null_cmd);
	size_t nRoomNameLen = strlen( pReq->m_szRoomName );
    if ( nRoomNameLen && nRoomNameLen > ROOM_NAME_MAX_LEN ) 
    {
		Global::logger->warn("[%s] player <%s> fail to update room info: room name exceed limit.", 
				GS_ROOMOPT , 
				pClient->GetRoleName() );

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
		pClient->SendPackage( &res , sizeof(res) );

		return enPHandler_Warning;
	}

	if( pReq->m_bHasPwd )
	{
		size_t nRoomPwdLen = strlen( pReq->m_szRoomPwd );
		if( !nRoomPwdLen || nRoomPwdLen > ROOM_PWD_MAX_LEN )
		{
			Global::logger->warn("[%s] player <%s> fail to update room info: password exceed limit.", 
					GS_ROOMOPT , 
					pClient->GetRoleName() );

			MODI_GS2C_Respone_OptResult res;
			res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
			pClient->SendPackage( &res , sizeof(res) );
			return enPHandler_Warning;
		}
	}

	if( (pReq->m_ucNewOffSpectator >= -1 && pReq->m_ucNewOffSpectator <= 1) == false )
	{
		Global::logger->warn("[%s] player <%s> fail to update room info: spectating failure. " , 
				GS_ROOMOPT , 
				pClient->GetRoleName() );

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_Warning;
	}	

	if( nRoomNameLen )
	{
		room->SetRoomName( pReq->m_szRoomName );
	}

	if( pReq->m_bHasPwd )
	{
		room->SetPassword( 0 );
		room->SetPassword( pReq->m_szRoomPwd );
	}
	else 
	{
		if( room->IsPasswordRoom() )
		{
			room->SetPassword( 0 );
		}
	}

	if( pReq->m_ucNewOffSpectator )
	{
		bool bAllowSpectator = pReq->m_ucNewOffSpectator == -1 ? false : true;
		// 如果设置为不允许观战，房主必须不在观战位置
		if( bAllowSpectator == false )
		{
			if( pClient->IsPlayer() )
			{
				room->SetAllowSpectator(  bAllowSpectator );
			}
			else 
			{
				MODI_GS2C_Respone_OptResult res;
				res.m_nResult = SvrResult_ModifyRoomInfo_Faild;
				pClient->SendPackage( &res , sizeof(res) );

				Global::logger->debug("[%s] player <%s> fail to update room info , self in spectator postion . but set off spectator. " , 
						GS_ROOMOPT , 
						pClient->GetRoleName() );

				return enPHandler_Warning;
			}
		}
		else 
		{
			room->SetAllowSpectator( bAllowSpectator );
		}
	}
//	if( pReq->m_subType != room->GetRoomSubType() && 
//		pReq->m_subType > ROOM_STYPE_BEGIN && pReq->m_subType < ROOM_STYPE_END )
//	{
//		defRoomType t;
//		t = RoomHelpFuns::MakeRoomType( room->IsPasswordRoom() , 
//				room->GetRoomType(),
//				pReq->m_subType );
//		room->SetRoomType( t );
//	}
   	
	MODI_GS2C_Notify_RoomInfoChanged notify;
	memcpy( notify.m_szRoomName , pReq->m_szRoomName , sizeof(notify.m_szRoomName) );
	notify.m_bHasPwd = pReq->m_bHasPwd;
	notify.m_ucNewOffSpectator = pReq->m_ucNewOffSpectator;
	room->Broadcast( &notify , sizeof(notify) );
	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::TransferMaster_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_TransferMaster ,cmd_size , 0 ,0,pClient );

	const MODI_C2GS_Request_TransferMaster * req = (const MODI_C2GS_Request_TransferMaster *)(pt_null_cmd);
	if( req->new_master.IsInvalid() )
		return enPHandler_Kick;

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent transfer master request. ", 
				GS_ROOMOPT,
                pClient->GetRoleName());

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Room_TransferMasterFaild;
		pClient->SendPackage( &res , sizeof(res) );
        return enPHandler_Kick;
    }

    if (!pClient->IsRoomMaster())
    {
        Global::logger->warn("[%s] player <%s> is not room<%u> master, but sent transfer master request.", 
				GS_ROOMOPT,
                pClient->GetRoleName() , 
				pClient->GetRoomID() );

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Room_TransferMasterFaild;
		pClient->SendPackage( &res , sizeof(res) );
        return enPHandler_Kick;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Room_TransferMasterFaild;
		pClient->SendPackage( &res , sizeof(res) );

		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent transfer master request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());

		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Room_TransferMasterFaild;
		pClient->SendPackage( &res , sizeof(res) );
        return enPHandler_Kick;
    }

	if( room->GetMaster() != pClient->GetGUID() )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_ClientAvatar * new_master = room->FindClientByGUID( req->new_master );
	if( new_master )
	{
		if( !AVATAR_CAN( new_master,PREVILEGE_ROOMOWNER))
		{
#ifdef _XXP_DEBUG
			Global::logger->debug("[room_change_owner] the target has no previllege to be the room maser");
#endif
			MODI_GS2C_Respone_OptResult res;
			res.m_nResult = SvrResult_Room_TransferMasterFaild;
			pClient->SendPackage( &res , sizeof(res) );
			return enPHandler_OK;
		}
		/// 先不限制
#ifdef _ML_VERSION
#else 		
		if(room->GetRoomType() == ROOM_TYPE_VIP)
		{
			if(! (new_master->IsVip()))
			{
				MODI_GS2C_Respone_OptResult res;
				res.m_nResult = SvrResult_Room_TransferMasterFaild;
				pClient->SendPackage( &res , sizeof(res) );
				return enPHandler_OK;
			}
		}
#endif		
		room->TransferMaster( new_master );
	}
	else 
	{
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Room_TransferMasterFaild;
		pClient->SendPackage( &res , sizeof(res) );
	
		Global::logger->info("[%s] room <%u> 's master<%s> try transfer room master , but target not in room. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
	}

	return enPHandler_OK;
}


/** 
 * @brief 组队情况改变
 * 
 */
int MODI_PackageHandler_c2gs::ChangeTeam_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												  const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ChangeTeam , cmd_size , 0 , 0, pClient );

	const MODI_C2GS_Request_ChangeTeam * p_recv_cmd = (const MODI_C2GS_Request_ChangeTeam *)pt_null_cmd;

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> is not in room, but sent ready change team. ", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug(pClient , SvrResult_Room_NotInRoomButSendReady);
        return enPHandler_Kick;
    }


    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if( !room )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

    if (room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> is in match game status ,but  player <%s> sent ready changeteam.", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
		SendResultForDebug(pClient , SvrResult_Room_IsStartButSendReady);
        return enPHandler_Warning;
    }

    ///    是否参战玩家
    if (!room->IsPlayerClient(pClient))
    {
        Global::logger->warn("[%s] player <%s> is spectator, but sent ready change team.", GS_ROOMOPT,
                pClient->GetRoleName());
		SendResultForDebug(pClient, SvrResult_Room_IsNotPlayerButSendReady );
        return enPHandler_Warning;
    }

    ///    改变为准备状态
    room->ChangeTeam(pClient, p_recv_cmd->m_byChangeTeam);
	
    return enPHandler_OK;
}
