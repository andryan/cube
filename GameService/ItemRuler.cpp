/**
 * @file   ItemRuler.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jul  5 18:26:54 2011
 * 
 * @brief  物品操作规则
 * 
 */

#include "AssertEx.h"
#include "ItemRuler.h"
#include "BinFileMgr.h"


/** 
 * @brief 是否可以重叠
 * 
 * @param config_id 道具id
 * 
 * @return 可以重叠true
 *
 */
bool MODI_ItemRuler::IsCanLap(const DWORD & config_id)
{
	/// 获得物品类型
	MODI_ItemType nType = GetItemType(config_id);
	if(nType == enItemType_Unknow || nType <= CAN_NOT_LAP_ITEMTYPE )
	{
		return false;
	}

	if( nType <= ITEM_BIG_CLASS )
	{
		/// 只有道具能重叠
		const  GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get(config_id);
		if( pItem )
		{
			return true;
		}
	}
	
	return false;
}


/** 
 * @brief 是否可以出售
 * 
 * @param config_id 物品属性id
 * 
 * @return 可以true
 *
 */
bool MODI_ItemRuler::IsCanInShop(const DWORD & config_id)
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar )
		{
			return pAvatar->get_CanInShop();
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			return pItem->get_CanInShop();
		}
		return false;
	}

	return false;
}



/** 
 * @brief 等级是否符合要求
 * 
 * @param config_id 物品id
 * @param byLevel 等级
 * 
 * @return 可以true
 *
 */
bool MODI_ItemRuler::GetReqLevel(const DWORD config_id , BYTE & byLevel)
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar )
		{
			byLevel = pAvatar->get_LevelReq();
			return true;
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			byLevel = pItem->get_LevelReq();
			return true;
		}
		return false;
	}

	return false;
}


/** 
 * @brief 性别是否符合要求
 * 
 * @param config_id 道具id
 * @param bySex 性别
 * 
 * @return 符合true
 *
 */
bool MODI_ItemRuler::GetReqSex(const DWORD config_id , BYTE & bySex)
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar && pAvatar->get_SexReq() <= enSexAll )
		{
			bySex = pAvatar->get_SexReq();
			return true;
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem && pItem->get_SexReq() <= enSexAll )
		{
			bySex = pItem->get_SexReq();
			return true;
		}
		return false;
	}

	return false;
}


/** 
 * @brief 最大的重叠数
 * 
 * @param config_id 物品id
 * @param dwMax 最大重叠数
 * 
 * @return 成功true
 *
 */
bool MODI_ItemRuler::GetMaxLap(const WORD config_id , WORD & dwMax )
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar )
		{
			dwMax = 1;
			return true;
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			dwMax = pItem->get_MaxOverLap();
			return true;
		}
		return false;
	}

	return false;
}


/** 
 * @brief 获取能最大的拥有数
 * 
 * @param config_id 物品id
 * @param dwMax 最大数
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_ItemRuler::GetMaxOwn(const DWORD config_id , WORD & dwMax )
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar )
		{
			dwMax = pAvatar->get_MaxOwnNum();
			return true;
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			dwMax = pItem->get_MaxOwnNum();
			return true;
		}
		return false;
	}

	return false;
}


/** 
 * @brief 使用环境
 * 
 * @param config_id 物品id
 * @param state 游戏状态
 * 
 * @return 成功ture
 *
 */
bool MODI_ItemRuler::GetCanUseInGameState(const DWORD config_id , enItemCanUseInGameState & state)
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		return true;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			state = (enItemCanUseInGameState)pItem->get_CanUseInGamestate();
			return true;
		}
		return false;
	}
	return false;
}


/** 
 * @brief 使用目标检查
 * 
 * @param config_id 物品id
 * @param type 使用目标
 * 
 * @return 成功true
 *
 */
bool MODI_ItemRuler::GetCanUseTarget(const WORD & config_id , enItemCanUseTargetType & type )
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		return true;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			type = (enItemCanUseTargetType)pItem->get_TargetType();
			return true;
		}
		return false;
	}
	return false;
}


/** 
 * @brief 此类型物品最大的拥有数
 * 
 * @param config_id 物品id
 * @param nMaxOwn 大小
 * 
 * @return 成功ture,失败false
 *
 */
bool MODI_ItemRuler::GetSubClassMaxOwn(const WORD & config_id , WORD & nMaxOwn)
{
	MODI_ItemType nType = GetItemType( config_id );
	if( nType == enItemType_Unknow )
		return false;

	if( nType <= AVATARTYPE_BIG_CLASS )
	{
		const GameTable::MODI_Avatar * pAvatar = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get( config_id );
		if( pAvatar )
		{
			nMaxOwn = GetItemSubClassMaxOwn( nType );
			return true;
		}
		return false;
	}
	else if( nType <= ITEM_BIG_CLASS )
	{
		const GameTable::MODI_Itemlist * pItem = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get( config_id );
		if( pItem )
		{
			nMaxOwn = GetItemSubClassMaxOwn( nType );
			return true;
		}
		return false;
	}

	return false;
}
