/** 
 * @file ImpactList.h
 * @brief 效果列表
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-07
 */

#ifndef MODI_IMPACT_LIST_H_
#define MODI_IMPACT_LIST_H_


#include "gamestructdef.h"
#include "Timer.h"
#include "ImpactObj.h"

class 	MODI_ClientAvatar;


class 	MODI_ImpactList
{
	public:

		explicit MODI_ImpactList( MODI_ClientAvatar * pOwner );
		~MODI_ImpactList();

		bool  	Register( MODI_ImpactObj & impactObj );
		bool 	UnRegister( const MODI_GUID & guid );
		bool 	Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);

		bool 	IsFull();
		bool 	IsEmpty();

		void 	ClearAll_OutGameFadeOut();

		void 	SendImpactsTo( MODI_ClientAvatar * pClient );

	private:

		bool 	Active( MODI_ImpactObj & impactObj );
		bool 	InActivate( MODI_ImpactObj & impactObj );
		bool 	AddNew( MODI_ImpactObj & impactObj );
		bool 	RelpaceImpact( MODI_ImpactObj & impactObj );
		void 	SyncPackage( void * p , size_t nSize );
		void  	BuildPackage( void * pmsg , MODI_ImpactObj & impactObj , int iFlag );

	public:

		MODI_ClientAvatar * m_pOwner;
		std::list<MODI_ImpactObj *> 	m_list;
};

#endif
