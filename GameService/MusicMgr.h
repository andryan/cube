#ifndef MODI_MUSIC_MGR_H__
#define	MODI_MUSIC_MGR_H__

#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "Timer.h"
#include "MusicHeaderData.h"
#include <vector>

using namespace std;

class	MODI_MusicMgr
{
public:
	MODI_MusicMgr();
	~MODI_MusicMgr();

	 defMusicID 	GetRandomHideID() ;
	 defMusicID  	GetRandomAom();
private:

	static MODI_MusicMgr *m_pMgr;
	bool		m_bHideInit;
	bool 		m_bAomInit;
	vector<int >	m_vHideMusic;
	vector<int >	m_vAomMusic;
public:
	static MODI_MusicMgr *GetInstancePtr() 
	{

		if( !m_pMgr )
		{
			m_pMgr = new MODI_MusicMgr();
		}

		return m_pMgr;
	}
};

#endif
