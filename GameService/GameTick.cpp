#include "GameTick.h"
#include "Global.h"
#include "GameTask.h"
#include "GameChannelMgr.h"
#include "GameService.h"
#include "GameChannel.h"
#include "FunctionTime.h"
#include "GUIDCreator.h"
#include "ZoneClient.h"
#include "GameServerAPP.h"
#include "s2rdb_cmd.h"
#include "Base/FPSControl.h"
#include "ScriptManager.h"
#include "TaskManager.h"
#include "GamePackt.h"
#include "GameSingleMusicMgr.h"
#include "TmpSystemBroad.h"
#include "SceneManager.h"

MODI_TTime MODI_GameTick::m_stTTime;
MODI_RTime  MODI_GameTick::m_stRTime;

/// 构造
MODI_GameTick::MODI_GameTick() :
	MODI_Thread("gametick"),
	m_timerLastItemSerial(1 * 1000),
	m_timerRubbishCheck( 30 * 1000 )
{

}

/// 析构
MODI_GameTick::~MODI_GameTick()
{

}


/**
 * @brief 主循环
 *
 */
void MODI_GameTick::Run()
{
	unsigned long one_second_delay = 0;
	unsigned long one_minute_delay = 0;
	Global::m_stLogicRTime.GetNow();
	unsigned long nBegin = Global::m_stLogicRTime.GetMSec();

	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	ptm->Init();
	Global::g_nExcepLogout =0;
	Global::g_nNormalLogout=0;

	MODI_Timer	timeperminute(60*1000);
	//MODI_FPSControl fps;
	//fps.Initial( 50 );

    while (!IsTTerminate())
    {
		::usleep(20);
		m_stRTime.GetNow();
		Global::m_stRTime = m_stRTime;
		Global::m_stLogicRTime = m_stRTime;
		m_stTTime = m_stRTime;
		ptm->GetANSITime();
		nBegin = m_stRTime.GetMSec();

		MODI_FunctionTime functiontime(50 * 1000, "gameservice");//100ms

		if( timeperminute(Global::m_stLogicRTime))
		{
			///打印日志

			Global::logger->info("[LogoutTotal] <ExceptLogout=%u,NormalLogout=%u,Total=%u>",Global::g_nExcepLogout,Global::g_nNormalLogout,Global::g_nExcepLogout+Global::g_nNormalLogout);

			///重置统计数据
			if( Global::g_nExcepLogout >= 10)
			{
				Global::logger->info("[LogoutWarn] <ExceptLogout=%u>",Global::g_nExcepLogout);
			}
			Global::g_nNormalLogout=0;
			Global::g_nExcepLogout=0;
		}

		if(nBegin > one_second_delay)
		{
			MODI_TempSystemBroad::GetInstance()->Update();
			MODI_GameMusicMgr::GetInstance().Update();

			/// 系统玩家执行脚本
			static MODI_ClientAvatar * p_system_avatar = new MODI_ClientAvatar(INVAILD_GUID, NULL);
			MODI_TriggerTime system_script(2);
			MODI_ScriptManager<MODI_TriggerTime>::GetInstance().Execute(p_system_avatar, system_script);
			
			if(Global::g_byReloadScript)
			{
				/// 重新加载脚本
				Global::logger->info("[reload_script] gameservice reload script begin");
				
				///  脚本加载
				if(!MODI_TaskManager::GetInstance().ReloadScript())
				{
					Global::logger->info("[reload_script] gameservice reload script failed");
				}
				Global::g_byReloadScript = 0;
			}
			if( Global::g_byReloadPackage)
			{
				///  重新加载礼包
				Global::logger->info("[reload_package] gameservice reload package begin");

				///	礼包加载
				if( !MODI_DynamicPackt::GetInstance()->ReloadPackt())
				{
					Global::logger->info("[reload_package] gameservice reload package failed");
				}	
				Global::g_byReloadPackage = 0;
			}
			
			one_second_delay = nBegin + 1000;
			struct ExecuteTimeScript: public MODI_ChannelClientCallBack
			{
				bool Done(MODI_ClientAvatar * p_client)
				{
					//p_client->m_stVarManager.UpDate();
					MODI_TriggerTime script(1);
					MODI_ScriptManager<MODI_TriggerTime>::GetInstance().Execute(p_client, script);
					return true;
				}
			};

			ExecuteTimeScript time_script;
			MODI_GameChannel::GetInstancePtr()->AllClientExcuteCallback(time_script);
		}
		
		if(nBegin > one_minute_delay)
		{
			one_minute_delay = nBegin + 60000;//1分钟
			
#ifdef _ROOM_INFO			
			Global::logger->debug("[room_size] this lobby room <size= %d>",MODI_GameLobby::GetInstancePtr()->GetRoomSize());
#endif
			
#ifdef _CMD_FLUX			
			MODI_GameTask::m_stSendCmdFlux.ShowInfo(true);
			MODI_GameTask::m_stRecvCmdFlux.ShowInfo(true);
#endif
			MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
			MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	
			const unsigned int player_num = pChannel->Size();
			const unsigned int lobby_player_num = pLobby->GetLobbyPlayerNum();
			const BYTE room_num = pLobby->GetRoomSize();
			Global::logger->debug("[channel_info] this channel have <rooms=%d,players=%u,lobbyplayers=%u,roomsplayers=%u>",
						  room_num,
						  player_num,
						  lobby_player_num, (player_num-lobby_player_num));
		}

		if( m_timerRubbishCheck( Global::m_stLogicRTime ) )
		{
			MODI_ClientSessionMgr * pSessionMgr = MODI_ClientSessionMgr::GetInstancePtr();
			pSessionMgr->DisconnectionRubbishClients( Global::m_stLogicRTime.GetMSec() );
		}

		MODI_SceneManager::GetInstance().Update(m_stRTime, m_stTTime);

		// 处理zs
		if( MODI_ZoneServerClient::GetInstancePtr() )
		{
			MODI_ZoneServerClient::GetInstancePtr()->ProcessPackages();
		}

		MODI_GameTask::ProcessAllPackage();
		MODI_GameChannel::GetInstancePtr()->Update(m_stRTime, m_stTTime);

		/// 0点
		if(m_stZeroEqu(m_stTTime))
		{
			/// 清除enDayClearType变量
			Global::logger->debug("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
			std::ostringstream os;
			os<< "update game_user_var set value=0 where type=" << MODI_VarState::enDayClearType << ";";
			MODI_GameServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());

			os.str("");
			os<< "update game_user_var set value=value+1 where type=" << MODI_VarState::enDayIncType << ";";
			MODI_GameServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());
			
			static MODI_ClientAvatar * p_system_avatar = new MODI_ClientAvatar(INVAILD_GUID, NULL);
			MODI_TriggerEnter system_script(12);
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(p_system_avatar, system_script);
			
				
		}
    }

    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_GameTick::Final()
{
	MODI_GameService * pService = MODI_GameService::GetInstancePtr();
	if( pService )
	{
		pService->Terminate();
	}
}
