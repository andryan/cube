
#ifndef _SINGLE_MUSIC_INFO_H__
#define _SINGLE_MUSIC_INFO_H__

#include "AssertEx.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "Timer.h"

#define	FLUSH_TIME	1*60*1000

/**
 * @brief 单曲音乐信息管理
 *
 */

class	MODI_GameMusicMgr
{
public:
	typedef std::map<WORD, SingleMusicInfo *> defSingleMap;
	typedef std::map<WORD, SingleMusicInfo *>::value_type  defSingleMapValue;
	typedef std::map<WORD, SingleMusicInfo * >::iterator  defSingleMapIter;

	static  MODI_GameMusicMgr  & GetInstance()
	{
		if( ! m_pInstance)
		{
			m_pInstance = new MODI_GameMusicMgr();
		}
		return * m_pInstance;
	}

	bool 	Init( size_t size, struct SingleMusicInfo *p, enGameMode  mode_t);

	bool 	OnRecvMusicInfo( SingleMusicInfo * p_music, enGameMode  mode);

	void	SyncToClient(MODI_ClientAvatar * pclient, enGameMode mode);

	WORD	GenPackage( SingleMusicInfo * array, enGameMode mode_t);

	void	Update();
	
protected:
	void	Cleanup(enGameMode mode);
	
	SingleMusicInfo  * GetMusic( WORD id, enGameMode mode_t);

	bool	InsertMusic(SingleMusicInfo *p_music , enGameMode mode_t);

private:
	MODI_GameMusicMgr():m_tTimer(FLUSH_TIME)
	{
	}
	~MODI_GameMusicMgr()
	{
	}

	MODI_Timer	m_tTimer;

	static MODI_GameMusicMgr  * m_pInstance;
	
	defSingleMap	m_mMusicMap;
	defSingleMap 	m_mKeyMusicMap;

};


#endif

