/**
 * @file   Action.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 17:07:07 2011
 * 
 * @brief  动作实现
 * 
 * 
 */

#include "ZoneClient.h"
#include "s2zs_cmd.h"
#include "Action.h"
#include "ClientAvatar.h"
#include "SplitString.h"
#include "GameUserVar.h"
#include "ItemOperator.h"
#include "protocol/c2gs_syscmd.h"
#include "HelpFuns.h"
#include "GameLobby.h"
#include "GameConstant.h"
#include "GameServerAPP.h"
#include "GameChannel.h"
#include "protocol/c2gs_chatcmd.h"
#include "protocol/c2gs_rolecmd.h"
#include "GlobalVar.h"
#include "Global.h"
#include "TmpSystemBroad.h"
#include "YunYingKeyExchange.h"


/*
static int GetMonDays(int month, int year)
{
	int 	a[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	bool	leap;	

	leap=( year % 400 ==0 || year % 4== 0 && year % 100 != 0 );
	return ( a[month-1] + (leap && (month== 2 ) ));
}
*/
/** 
 * @brief 变量更新
 * 
 * @return 
 */
bool MODI_VarAction::Init(const char * action_line)
{
#ifdef _DEBUG
	Global::logger->debug("[action condition debug] ------conline= %s",action_line);
#endif
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strName = split["name"];
	m_strAttribute = split["attribute"];
	m_strOp = split["op"];
	m_wdValue = atoi(split["value"]);
	m_strVarToOp = split["varname"];
	return true;
}


/** 
 * @brief 变量更新
 * 
 */
bool MODI_VarAction::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_GameUserVar * p_var = p_client->m_stGameUserVarMgr.GetVar(m_strName.c_str());

	if(! p_var)
	{
		p_var = new MODI_GameUserVar(m_strName.c_str());
		if(p_var->Init(p_client, m_strAttribute.c_str()))
		{
			if(! p_client->m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
				//#ifdef _VAR_DEBUG
				//				Global::logger->debug("[do_action] do action when new a var <name=%s,type=%d,value=%u,time=%ul>",
				//									  p_var->GetName(), p_var->GetType(), p_var->GetValue(), p_var->GetTime());
				//#endif				
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	if(!p_var)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	WORD new_value = 0;
	if(m_strOp == "add")
	{
		new_value = m_wdValue + p_var->GetValue();
	}
	else if(m_strOp == "sub")
	{
		if(p_var->GetValue() > m_wdValue)
		{
			new_value =  p_var->GetValue() - m_wdValue;
		}
		else
		{
			Global::logger->error("[var_action] var action sub failed <name=%s>", p_var->GetName());
			MODI_ASSERT(0);
			return false;
		}
	}
	else if(m_strOp == "set")
	{
		new_value = m_wdValue;
	}
	else if( m_strOp == "inc" )
	{
		new_value = p_var->GetValue() +1;	
	}
	else if( m_strOp == "varset")
	{
		if( m_strVarToOp == "")
		{
#ifdef _DEBUG
			Global::logger->debug("[var_operate] var operator but can't find the variable's name");
#endif
			return false;
		}
		MODI_GameUserVar * _p_var2= NULL;
		GETVAR(m_strVarToOp.c_str(),m_strAttribute.c_str(),p_client,_p_var2);
		MODI_ASSERT(_p_var2);
		p_var->SetValue(_p_var2->GetValue());
#ifdef _DEBUG
		Global::logger->debug("[var_operate]  new var name = %s,value = %d,var %s 's value be set to %d",_p_var2->GetName(),_p_var2->GetValue(),p_var->GetName(),p_var->GetValue());
#endif
		return true;
	}
	else
	{
		return false;
	}
	
// #ifdef _VAR_DEBUG
// 	Global::logger->debug("[do_action] do action <name=%s,op=%s,oldvalue=%u,opvalue=%u,newvalue=%u>",
// 						  p_var->GetName(), m_strOp.c_str(),p_var->GetValue(), m_wdValue, new_value);
// #endif	
	p_var->SetValue(new_value);
	return true;
}


/** 
 * @brief 送东西的mail
 * 
 *
 */
bool MODI_SendItemMail_A::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	/*	MODI_SplitString split;
	split.InitXML(action_line);
	m_strCaption = split["caption"];
	m_strContent = split["content"];
	m_dwItemid = atoi(split["id"]);
	m_wdNum = atoi(split["num"]);
	string tmp = split["time"];
	if( tmp.c_str() == '\0')
	 	m_wtime=3;	
	else
	m_wtime = atoi( split["time"]);*/

	MODI_XMLParser xml;
	if(!xml.InitStr(action_line))
	{
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("senditemmail");
	if(!p_root)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "caption", m_strCaption))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "content", m_strContent))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "attach", m_strAttach))
	{
		//MODI_ASSERT(0);
		return false;
	}


	
	return true;
}


/** 
 * @brief 进入频道动作实现
 * 
 * @param p_client 该角色进入频道
 * 
 */
bool MODI_SendItemMail_A::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}
	
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_S2ZS_Request_SendGiveItemMail msg;
		msg.m_shopmail = false;
		strncpy( msg.m_szSenderName  , SYS_MAIL_SENDER ,  sizeof(msg.m_szSenderName) -1 );
		strncpy( msg.m_szReceverName , p_client->GetRoleName() , sizeof(msg.m_szReceverName) - 1);
		strncpy( msg.m_szContent ,  m_strContent.c_str(), sizeof(msg.m_szContent) - 1);
		strncpy( msg.m_szCaption ,  m_strCaption.c_str(), sizeof(msg.m_szCaption) -1);

		if(m_strAttach.size())
		{
			/// 获取附件
			MODI_GoodsPackageArray goods;
			if(! PublicFun::StrConvToGoodsPackage(goods, m_strAttach.c_str(), m_strAttach.size()))
			{
				MODI_ASSERT(0);
				return false;
			}

			if(goods.m_count > MAX_MAIL_ATTACH_NUM)
			{
				MODI_ASSERT(0);
				return false;
			}

			msg.m_nCount = goods.m_count;
			for(BYTE i=0; i<goods.m_count; i++)
			{
				MODI_S2ZS_Request_SendGiveItemMail::tagGoods * tmp = &msg.m_Goods[i];
				tmp->m_nCount = goods.m_array[i].nCount;
				tmp->m_nItemConfigID = goods.m_array[i].nGoodID;
				tmp->m_nLimitTime = goods.m_array[i].time;
				
			}
		}
		
		pZone->SendCmd( &msg , sizeof(msg));

#ifdef _HRX_DEBUG
		Global::logger->debug("[send_item_mail] mail<title=%s,content=%s,attach=%s>",
							  m_strCaption.c_str(), m_strContent.c_str(), m_strAttach.c_str());
#endif		
		return true;
	}
	return false;
}



/** 
 * @brief 系统通知
 * 
 *
 */
bool MODI_System_Notify_A::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	/*	MODI_SplitString split;
	split.InitXML(action_line);
	m_strContent = split["content"];
	m_byType = atoi(split["type"]);
	return true;*/

	MODI_XMLParser xml;
	if(!xml.InitStr(action_line))
	{
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("system_notify");
	if(!p_root)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "content", m_strContent))
	{
		MODI_ASSERT(0);
		return false;
	}
	return true;
}


bool MODI_System_Notify_A::do_it(MODI_ClientAvatar * p_client)
{
	if(p_client)
	{
		p_client->SendSystemChat(m_strContent.c_str(), m_strContent.size());
	}
	return true;
}



/** 
 * @brief 直接增加物品
 * 
 *
 */
bool MODI_AddItemDirect_A::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_wdItemID = atoi(split["id"]);
	m_wdNum = atoi(split["num"]);
	m_wdLimit = atoi(split["limit"]);
	return true;
}


bool MODI_AddItemDirect_A::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_Bag * p_player_bag = &(p_client->GetPlayerBag());
	if(!p_player_bag)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_CreateItemInfo create_info;
	create_info.m_dwConfigId = m_wdItemID;
	create_info.m_byLimit = m_wdLimit;
	create_info.m_enCreateReason = enAddReason_Task;
	create_info.m_byServerId = pApp->GetServerID();

	for(WORD deal_count=0; deal_count<m_wdNum; deal_count++)
	{
		QWORD new_item_id = 0;
		if(!p_player_bag->AddItemToBag(create_info, new_item_id))
		{
			Global::logger->fatal("[add_item_action] add item direct failed <id=%u,num=%u,limit=%u>", m_wdItemID, m_wdNum, m_wdLimit);
			return false;		
		}
	}
	
	return true;
}


/** 
 * @brief 视频通过验证
 * 
 *
 */
bool MODI_VideoPass_A::Init(const char * action_line)
{
	return true;
}


bool MODI_VideoPass_A::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}

	MODI_MultiGameRoom * p_room = p_client->GetMulitRoom();
	MODI_GS2C_Notify_VideoPass send_cmd;
	strncpy(send_cmd.m_cstrName, p_client->GetRoleName(), sizeof(send_cmd.m_cstrName));
	/// 获取变量值
	MODI_GameUserVar  * get_var = NULL;
	std::string var_name = "video_passed";
	get_var = p_client->m_stGameUserVarMgr.GetVar(var_name.c_str());
	if(get_var == NULL)
	{
		MODI_ASSERT(0);
		return false;
	}

	WORD get_value = get_var->GetValue();
	if(get_value > 0)
	{
		send_cmd.m_wdValue = get_value;
		if(p_room)
		{
			p_room->Broadcast(&send_cmd, sizeof(send_cmd));
#ifdef 	_DEBUG
			Global::logger->debug("-->>>>>video pass <name=%s,value=%d>", p_client->GetRoleName(), get_value);
#endif
		}
		else
		{
			p_client->SendPackage( &send_cmd,sizeof(send_cmd));	
		}
		return true;
	}
	return false;
}



/** 
 * @brief 修改系统变量
 * 
 *
 */
bool MODI_SetCV_A::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(action_line);
   	name = split["name"];
	value = split["value"];
	return true;
}


bool MODI_SetCV_A::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}

	using namespace MODI_GameConstant;
	const char * szValueName = name.c_str();
	const char * szNewValue = value.c_str();
	enConstantValueType vt;
	
	if( get_ValueType_ByName( szValueName , vt ) == false )
	{
		return false;
	}

	if( vt == kCVT_DWORD )
	{
		DWORD n = 0;
		if( safe_strtoul(  szNewValue , &n ) )
		{
			set_DWORDValue_ByName( szValueName ,  n );
		}
	}
	else if( vt == kCVT_INT )
	{
		long i = 0;
		if( safe_strtol(  szNewValue , &i ) )
		{
			set_IntValue_ByName( szValueName ,  i );
		}
	}
	else if( vt == kCVT_FLOAT )
	{
		float f = atof( szNewValue );
		set_FloatValue_ByName( szValueName , f );
	}
	else 
	{
		MODI_ASSERT(0);
	}

	Global::logger->debug("set cv successful");
	return true;
}


bool MODI_SysInfo_A::Init(const char * action_line)
{

	if(!action_line)
	{
		return false;
	}
	
	/*	MODI_SplitString split;
	split.InitXML(action_line);

   	info = split["info"];
	title = split["title"];
	m_byType = atoi(split["type"]);	*/

	MODI_XMLParser xml;
	if(!xml.InitStr(action_line))
	{
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("sysinfo");
	if(!p_root)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "title", title))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "info", info))
	{
		MODI_ASSERT(0);
		return false;
	}
	
	return true;
}

bool MODI_SysInfo_A::do_it(MODI_ClientAvatar * p_client)
{
	std::string	sys_info;
	sys_info += title;
	sys_info += p_client->GetRoleName();
	sys_info += info;


	MODI_GameChannel  * m_pChannel = MODI_GameChannel::GetInstancePtr();		
	m_pChannel->BroadcastSystemChat(sys_info.c_str(),sys_info.size());
/*	
	char buf[sizeof(MODI_GS2C_SystemChat_Cmd) + chat_size+1];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_SystemChat_Cmd * send_cmd = (MODI_GS2C_SystemChat_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_eType = enSystemNotice_Default;
	send_cmd->m_wdSize = sys_info.size()+1;
	strncpy(send_cmd->m_stContent, chat_content, send_cmd->m_wdSize);
	return SendPackage(send_cmd, sizeof(buf));
	*/
/*	
	char buf[Skt::MAX_PACKETSIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_SystemChat_Cmd * send_cmd = (MODI_GS2C_SystemChat_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_wdSize = sys_info.length()+1;
	strncpy(send_cmd->m_stContent,sys_info.c_str(),sys_info.length()+1);
	int size = sizeof(MODI_GS2C_SystemChat_Cmd)+send_cmd->m_wdSize+1;
	m_pChannel->Broadcast(send_cmd,size);
	*/
	return true;	
}

bool	MODI_TestLogger::Init(const char * action_line)
{

	if(!action_line)
	{
		return false;
	}
	
	/*MODI_SplitString split;
	split.InitXML(action_line);

	m_sAction = split["action"];
	m_sInfo = split["info"];*/

	MODI_XMLParser xml;
	if(!xml.InitStr(action_line))
	{
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("log");
	if(!p_root)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "action", m_sAction))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "info", m_sInfo))
	{
		MODI_ASSERT(0);
		return false;
	}
	
	return true;
}


bool	MODI_TestLogger::do_it(MODI_ClientAvatar * p_client)
{
	Global::logger->debug("[%s],<%s> %s ",m_sAction.c_str(),p_client->GetRoleName(),m_sInfo.c_str());	
	return true;
}

bool 	MODI_GVarAction::Init(const char * action_line)
{

#ifdef _DEBUG
	Global::logger->debug("[action global.... condition debug] ------conline= %s",action_line);
#endif
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strName = split["name"];
	m_strOp = split["op"];
	m_wdValue = atoi(split["value"]);
	return true;

}

bool 	MODI_GVarAction::do_it(MODI_ClientAvatar * p_client)
{
	WORD _gvalue = MODI_GlobalVarMgr::GetInstance()->GetVar(m_strName);
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(m_strOp == "add")
	{
		_gvalue += m_wdValue;
	}
	else if(m_strOp == "sub")
	{
		if(_gvalue > m_wdValue)
		{
			_gvalue-= m_wdValue;
		}
		else
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	else if(m_strOp == "set")
	{
		_gvalue = m_wdValue;
	}
	else if( m_strOp == "inc" )
	{
		_gvalue++;
	}
	MODI_GlobalVarMgr::GetInstance()->SetVar(m_strName,_gvalue);	
	return true;

}

bool	MODI_DisablePre::Init(const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_dwPre = atoi(split["condition"]);
	return true;
}
bool	MODI_AddItemTime::Init(const char *action_line)
{
	MODI_SplitString split;
	split.InitXML(action_line);
	m_wConfigid = atoi(split["config"]);
	m_wTime = atoi(split["time"]);
	return true;
}

bool	MODI_DisablePre::do_it( MODI_ClientAvatar  * p_client)
{
		
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
#ifdef	_XXP_DEBUG
	Global::logger->debug("[disable_pre] client has previllege %x,disable %x",p_client->m_dwPrevilege,m_dwPre);
#endif
	if( AVATAR_CAN( p_client,m_dwPre))
	{
		DISABLE_PRE( p_client,m_dwPre);
	}
#ifdef	_XXP_DEBUG
	Global::logger->debug("[disable_pre] after disable  has previllege %x,disable %x",p_client->m_dwPrevilege,m_dwPre);
#endif
	return true;
}
bool	MODI_GrantPre::Init( const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_dwPre = atoi(split["condition"]);
	return true;
}
bool	MODI_ExpPenalty::Init( const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_wExpPerc= atoi(split["condition"]);
	return true;

}
bool	MODI_ExpPenalty::do_it( MODI_ClientAvatar * p_client)
{
	if( !p_client)
		return false;
	if( m_wExpPerc > 100)
		return false;
	p_client->SetExppenalty(m_wExpPerc);
	return true;
}


bool	MODI_MoneyPenalty::Init( const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_wMonPerc= atoi(split["condition"]);
	return true;
}

bool	MODI_MoneyPenalty::do_it(MODI_ClientAvatar * p_client)
{
	if( !p_client)
		return false;
	if( m_wMonPerc > 100)
		return false;
	p_client->SetMoneypenalty(m_wMonPerc);
	return true;
}

bool	MODI_SendSysMail::Init( const char * action_line)
{

	if(!action_line)
	{
		return false;
	}

	MODI_XMLParser xml;
	if(!xml.InitStr(action_line))
	{
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("sysmail");
	if(!p_root)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "sender", m_stSenderName))
	{
		MODI_ASSERT(0);
		return false;
	}

// 	if(!xml.GetNodeStr(p_root,  "recever", m_stReceverName))
// 	{
// 		MODI_ASSERT(0);
// 		return false;
// 	}


	if(!xml.GetNodeStr(p_root,  "content", m_stContent))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(!xml.GetNodeStr(p_root,  "caption", m_stCaption))
	{
		MODI_ASSERT(0);
		return false;
	}
	
	return true;

	/*
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_stSenderName = split["sender"];
	m_stReceverName = split["recever"];
	m_stContent = split["content"];
	m_stCaption = split["caption"];
	return true;*/

}

bool	MODI_SendSysMail::do_it(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}
	
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_S2ZS_Request_SendSysMail msg;	
		strncpy( msg.m_szSenderName,m_stSenderName.c_str(),sizeof( msg.m_szSenderName)-1);
		strncpy( msg.m_szReceverName,p_client->GetRoleName(),sizeof(msg.m_szReceverName)-1);
		strncpy( msg.m_szContent, m_stContent.c_str(),sizeof( msg.m_szContent)-1);
		strncpy(msg.m_szCaption , m_stCaption.c_str(),sizeof(msg.m_szCaption) -1);
#ifdef _XXP_DEBUG
		Global::logger->debug("[send_sys_mail] <sender=%s,rec=%s,content=%s,caption=%s>",msg.m_szSenderName,msg.m_szReceverName,msg.m_szContent,msg.m_szContent);
#endif

		pZone->SendCmd( &msg,sizeof(msg));
	}
	return true;
}

bool	MODI_ChangeMaster::do_it(MODI_ClientAvatar * p_client)
{
	
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	MODI_MultiGameRoom * room = pLobby->GetMultiRoom(p_client->GetRoomID());

	if( !room )
	{
		MODI_ASSERT(0);
		return false;
	}
	MODI_NormalGameRoom * room_normal =(MODI_NormalGameRoom *)room;
	room_normal->ReFixOwner();
	if( p_client->IsRoomMaster())
	{
		room_normal->KillAllExcept(NULL);
	}
	return true;

}


bool	MODI_AddItemTime::do_it(MODI_ClientAvatar * p_client)
{
	///仅仅对第一个发现的configid进行时间的延长，如果没有的话，那么就不
	//处理
	Global::logger->debug("[add_item_time] <accname=%s,configid=%u,is_vip=%d>",
						  p_client->GetAccName(), m_wConfigid, p_client->IsVip());
	
	MODI_GameItemInfo * info = p_client->GetPlayerBag().GetItemByConfigId(m_wConfigid);	
	std::ostringstream   os;
	std::ostringstream  where;
	if( !info)
	{
		Global::logger->debug("[add_item_time] can't find the corret item of the user <accname=%s,configid=%u,time=%u>",p_client->GetAccName(),m_wConfigid,m_wTime);
		return false;
	}
	
	info->m_stItemInfo.m_qdExpireTime += m_wTime;
	MODI_Record  record_update;
	os<<"from_unixtime("<<info->m_stItemInfo.m_qdExpireTime<<" )";
	record_update.Put( "expire_time",os.str());
	where<<"itemid=" <<info->GetItemId();
	record_update.Put("where",where.str());
	MODI_ByteBuffer result(1024);
	if( !MODI_DBClient::MakeUpdateSql(result,GlobalDB::m_pItemInfoTbl,&record_update))
	{
		Global::logger->fatal("[update_item} update item time failed <itemid=%u,time=%u,owenr=%s>",info->GetItemId(),m_wTime,p_client->GetRoleName());
		return false;
	}
	MODI_GameServerAPP::ExecSqlToDB((const char * )result.ReadBuf(),result.ReadSize());
	Global::logger->debug("[add_item_time] send to dbserver <accname=%s,configid=%u,is_vip=%d>",
						  p_client->GetAccName(), m_wConfigid, p_client->IsVip());
	return true;
}

bool	MODI_GrantPre::do_it( MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
#ifdef	_XXP_DEBUG
	Global::logger->debug("[grant_pre] client has previllege %x,grant %x",p_client->m_dwPrevilege,m_dwPre);
#endif
	if(! AVATAR_CAN( p_client,m_dwPre))
	{
		GRANT_PRE( p_client,m_dwPre);
	}
#ifdef	_XXP_DEBUG
	Global::logger->debug("[grant_pre] after grant  has previllege %x,grant %x",p_client->m_dwPrevilege,m_dwPre);
#endif
	return true;
}



bool	MODI_ActiveVip::Init( const char * action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);
	m_dwTime = atoi(split["time"]);

	return true;
}

bool	MODI_ActiveVip::do_it( MODI_ClientAvatar * p_client)
{
		
	///首先为对应的物品添加时间
	//  添加时间的工作交由配置脚本的人来添加
	if(! p_client->IsVip())
	{
		p_client->SetVip(1);
	//	AddVipTime( m_addTime);
		///修改一些会员的数据
		///通知Zone VIP时间更改了
		MODI_S2ZS_Request_ChangeVip  req;			
		req.m_dwCharid = p_client->GetCharID();
		req.m_bVip = 1;	
		MODI_ZoneServerClient  * pZone = MODI_ZoneServerClient::GetInstancePtr();
		if( !pZone )
		{
			Global::logger->debug("[impactT_vip] can't get the zone server client ");
			return false;
		}
		pZone->SendCmd( &req,sizeof(MODI_S2ZS_Request_ChangeVip));

		///  通知GAME VIP时间修改	
		MODI_GS2C_Notify_VipTime   notify;
		notify.m_eTime = enVipTimeType(m_dwTime);
		Global::logger->debug("[vip_vip] vip--------------notify time=%d ",m_dwTime);

		p_client->SendPackage( &notify,sizeof(MODI_GS2C_Notify_VipTime));
	}

	/// 已经是VIP了，那么说明是再登陆的时候激活
	

	return true;
}
	
bool	MODI_ExpireVip::Init( const char * action_line)
{

	return true;
}

bool	MODI_ExpireVip::do_it( MODI_ClientAvatar * p_client)
{

	if( !p_client->IsVip())
	{
#ifdef _XXP_DEBUG
		Global::logger->debug("Request to expire vip ,but the owner isn't the vip ");
		MODI_ASSERT(0);
#endif
	}
	p_client->SetVip(0);
	
	MODI_S2ZS_Request_ChangeVip  req;			
	req.m_bVip = 0;	
	req.m_dwCharid = p_client->GetCharID();
	MODI_ZoneServerClient  * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZone )
	{
		Global::logger->debug("[impactT_vip] can't get the zone server client ");
		return false;
	}
	pZone->SendCmd( &req,sizeof(MODI_S2ZS_Request_ChangeVip));

	///  通知GAME VIP时间修改	
	
	MODI_GS2C_Notify_VipExpire   notify;
	p_client->SendPackage( &notify,sizeof(MODI_GS2C_Notify_VipTime));


	/// 为该玩家写死一个变量来存放七天后删除好友的变量
	
	MODI_GameUserVar   * p_var = NULL;
	GETVAR("vip_expired","type=normal",p_client,p_var);
	MODI_TTime  	now;
	now= Global::m_stRTime;	

	WORD	save=0;
	
	int mon=now.GetMon();
	int day=now.GetMDay(); 
	save  |= (mon & 0xff);
	save <<= 8;
	save  |= (day & 0xff);
	p_var->SetValue( save);

	p_var->SaveToDB();

	return true;
}

bool	MODI_DelFriendandIdol::Init( const char * action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);

	m_nFriend =	atoi(split["friend"]);
	m_nIdole = atoi(split["idole"]);
	m_nBlack = atoi(split["black"]);
	return true;
}

bool	MODI_DelFriendandIdol::do_it( MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		MODI_ASSERT(0);
		return false;
	}	

	MODI_S2ZS_Request_DelFrindandIdole  del;
	del.m_dwCharid = p_client->GetCharID();
	del.m_nBlack = m_nBlack;
	del.m_nFriend = m_nFriend;
	del.m_nIdole = m_nIdole;
	MODI_ZoneServerClient  * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZone )
	{
		Global::logger->debug("[impactT_vip] can't get the zone server client ");
		return false;
	}
	pZone->SendCmd( &del,sizeof(MODI_S2ZS_Request_DelFrindandIdole));
	return true;
}


bool	MODI_SetTimeLimit::Init( const char *action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);
	m_wConfigid = atoi(split["config"]);
	m_wTime = atoi(split["time"]);
	return true;
}


bool	MODI_SetTimeLimit::do_it( MODI_ClientAvatar * p_client)
{

	///仅仅对第一个发现的configid进行时间的延长，如果没有的话，那么就不
	//处理
	MODI_GameItemInfo * info = p_client->GetPlayerBag().GetItemByConfigId(m_wConfigid);	
	std::ostringstream   os;
	std::ostringstream  where;
	MODI_RTime  	now;
	now= Global::m_stRTime;	
	if( !info)
	{
		Global::logger->debug("[add_item_time] can't find the corret item of the user <user=%s,configid=%u,time=%u>",p_client->GetRoleName(),m_wConfigid,m_wTime);
		return false;
	}	

	info->m_stItemInfo.m_qdExpireTime = now.GetSec()+m_wTime;

	MODI_Record  record_update;
	os<<"from_unixtime("<<info->m_stItemInfo.m_qdExpireTime<<" )";
	record_update.Put( "expire_time",os.str());
	where<<"itemid=" <<info->GetItemId();

	record_update.Put("where",where.str());
	MODI_ByteBuffer result(1024);
	if( !MODI_DBClient::MakeUpdateSql(result,GlobalDB::m_pItemInfoTbl,&record_update))
	{
		Global::logger->fatal("[update_item} update item time failed <itemid=%u,time=%u,owenr=%s>",info->GetItemId(),m_wTime,p_client->GetRoleName());
		return false;
	}

	MODI_GameServerAPP::ExecSqlToDB((const char * )result.ReadBuf(),result.ReadSize());
	return true;
}

bool	MODI_NotifyVipTime::Init( const char * action_line)
{
	MODI_SplitString split;
	split.InitXML(action_line);
	m_wConfigid = atoi(split["config"]);
	return true;
}

bool	MODI_NotifyVipTime::do_it( MODI_ClientAvatar * p_client)
{

	MODI_GameItemInfo * info = p_client->GetPlayerBag().GetItemByConfigId(m_wConfigid);	
	std::ostringstream   os;
	DWORD	secs = 0;
	MODI_RTime  	now;
	now= Global::m_stRTime;	
	if( !info)
	{
		return false;
	}	
		
	if( info->m_stItemInfo.m_qdExpireTime > now.GetSec())
	{
		secs = info->m_stItemInfo.m_qdExpireTime - now.GetSec();
		secs/= 3600;
		secs/= 24;

		os<<"Membership VIP kamu akan habis dalam"<<secs<<"hari";
	}
	else
	{
		os<<"Masa berlaku membership VIP kamu sudah habis.";
	}

	if(p_client)
	{
		p_client->SendSystemChat(os.str().c_str(), os.str().size());
	}
	return true;
	
}




bool	MODI_NotifyVipAddTime::Init( const char * action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);
	m_bTime = atoi(split["time"]);
	return true;
}	

bool	MODI_NotifyVipAddTime::do_it( MODI_ClientAvatar * p_client)
{

	MODI_GS2C_Notify_VipTime   notify;
	notify.m_eTime = enVipTimeType(m_bTime);

	p_client->SendPackage( &notify,sizeof(MODI_GS2C_Notify_VipTime));
	return true;
}

bool	MODI_NotifySystemBroad::Init( const char * action_line)
{
	MODI_SplitString split;
	split.InitXML(action_line);

	m_stContent = split["content"];
	return true;
}

bool	MODI_NotifySystemBroad::do_it( MODI_ClientAvatar * p_client)
{
	if( !p_client)
		return false;
	if( m_stContent == "")
	{
		return false;
	}
	MODI_ClientAvatar  *pObj = p_client->GetTarget();
	std::string 	out;
	out+=p_client->GetRoleName();
	out+="对";
	out+=pObj->GetRoleName();
	if( pObj == NULL)
	{
		return false;
	}
	WORD	i= rand()%3;
	switch (i)
	{
		case 0:
			out+="献上一朵求爱玫瑰，许下了一生一世的诺言，";
			out+=pObj->GetRoleName();
			out+="我爱你！";
			break;
		case 1:
			out+="献上一朵求爱玫瑰，让我们一起融化在这美妙的一刻，";
			out+=pObj->GetRoleName();
			out+="我的眼里只有你！";
			break;
		case 2:
			out+="献上一朵求爱玫瑰，在天愿作比翼鸟，在地愿为连理枝，";
			out+=pObj->GetRoleName();
			out+="可以一直陪着我么？";
			break;
		default:
			break;

	}
	
	
	MODI_TempSystemBroad::GetInstance()->Push(out);
	return true;
}

bool	MODI_ModifyTargetParm::Init( const char * action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);

	m_nScore = atoi(split["score"]);
	m_nRenqi = atoi(split["renqi"]);
	m_nConfigid = atoi(split["config"]);
	m_nContinue = atoi(split["continue"]);
	m_nElapsed = atoi(split["elapse"]);
	return true;
}


bool	MODI_ModifyTargetParm::do_it( MODI_ClientAvatar * p_client)
{
	if( ! p_client)
	{
		return false;
	}

	if( p_client->GetTarget() == NULL)
	{
		return false;
	}
	


	MODI_ClientAvatar  * pObj = p_client->GetTarget();	
/*	MODI_GS2C_Notify_Impact  notify;
	MODI_GUIDCreator 	*pCreator = MODI_GUIDCreator::GetInstancePtr();
	MODI_GUID	guid = pCreator->CreateImpactGUID();
	notify.m_ImpactGUID = guid;
	notify.m_ReceiverGUID = pObj->GetGUID();
	notify.m_SenderGUID = p_client->GetGUID();
	notify.m_nImpactConfigID = m_nConfigid;
	notify.m_byFlag = MODI_GS2C_Notify_Impact::kEnable;
	notify.m_Type = kImpactContinue;
	notify.m_ContinueTime = m_nContinue;
	notify.m_nElapsed = m_nElapsed;
	pObj->SendOrBroadcast( &notify,sizeof(MODI_GS2C_Notify_Impact));
	*/


/*	MODI_GS2C_Notify_ImpactModfiyData  imd;
	imd.m_Impactguid  = guid;
	imd.m_ModfiyRenqi = m_nRenqi;
	imd.m_ModfiyScore = m_nScore;
	imd.m_nConfig = m_nConfigid;
	pObj->SendOrBroadcast( &imd,sizeof( MODI_GS2C_Notify_ImpactModfiyData));
	*/

	MODI_S2ZS_Modify_Renqi  modify;
	modify.m_dwCharid = pObj->GetCharID();
	modify.m_nRenqi = m_nRenqi;
		
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone)
	{
		pZone->SendCmd( &modify,sizeof(MODI_S2ZS_Modify_Renqi));
	}

	return true;
}


bool	MODI_SetPayMoney::Init( const char * action_line)
{

	MODI_SplitString split;
	split.InitXML(action_line);
	m_dwValue = atoi(split["value"]);
	return true;
}	

bool	MODI_SetPayMoney::do_it( MODI_ClientAvatar * p_client)
{
	p_client->SetPayMoney(m_dwValue);
	return true;
}
