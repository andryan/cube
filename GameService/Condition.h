/**
 * @file   Condition.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:00:30 2011
 * 
 * @brief  脚本执行条件
 * 
 * 
 */

#ifndef _MD_CONDITION_H
#define _MD_CONDITION_H

#include "Global.h"
#include "ClientAvatar.h"

/**
 * @brief 条件
 * 
 */
class MODI_Condition
{
 public:
	virtual ~MODI_Condition(){}
	virtual bool Init(const char * con_line) = 0;
	virtual bool is_valid(MODI_ClientAvatar * p_client) = 0;
};


/** 
 * @brief 用户变量
 * 
 */
class MODI_VarCondition: public MODI_Condition
{
 public:
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
 private:
	std::string m_strName;
	std::string m_strAttribute;
	WORD m_wdCondition;
	std::string m_strOp;
	std::string m_stVarToOp;
};


/*
 * @brief 在线时间统计
 *
 */
class MODI_OnlineCondition_C : public MODI_Condition
{

public:
	MODI_OnlineCondition_C()
	{
		m_op="";
		timemin=0;
	}
	bool	Init( const char * con_line);
	bool	is_valid( MODI_ClientAvatar * p_client);
private:
	WORD	timemin;
	std::string	m_op;
		
};


/*
 * @brief 唱了多少首歌
 *
 */
class MODI_SongCountCondition : public MODI_Condition
{

public:
	MODI_SongCountCondition()
	{
		m_op="";
		m_dwCondition = 0;
	}
	bool	Init( const char * con_line);
	bool	is_valid( MODI_ClientAvatar * p_client);
private:
	DWORD	m_dwCondition;
	std::string	m_op;
		
};


/**
 * @brief 开始时间
 * 
 */
class MODI_BeginTime_C: public MODI_Condition
{
 public:
	MODI_BeginTime_C()
	{
		m_wdYear = 0;
		m_byMon = 0;
		m_byDay = 0;
		m_byHour = 0;
		m_byMin = 0;
	}
	
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	WORD m_wdYear;
	BYTE m_byMon;
	BYTE m_byDay;
	BYTE m_byHour;
	BYTE m_byMin;
};


/**
 * @brief 结束时间
 * 
 */
class MODI_EndTime_C: public MODI_Condition
{
 public:
	MODI_EndTime_C()
	{
		m_wdYear = 0;
		m_byMon = 0;
		m_byDay = 0;
		m_byHour = 0;
		m_byMin = 0;
	}
	
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	WORD m_wdYear;
	BYTE m_byMon;
	BYTE m_byDay;
	BYTE m_byHour;
	BYTE m_byMin;
};


/**
 * @brief 结束时间
 * 
 */
class MODI_CheckItemNum_C: public MODI_Condition
{
 public:
	MODI_CheckItemNum_C()
	{
		m_wdItemID = 0;
		m_wdNum = 0;
	}
	
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	/// 道具id
	WORD m_wdItemID;
	/// 比较类型
	std::string m_strOp;
	/// 比较数量 
	WORD m_wdNum;
};


class MODI_CheckSexType_C: public MODI_Condition
{
 public:
	MODI_CheckSexType_C()
	{
		m_wSex = 3;
	}
	
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	/// 道具id
	WORD	m_wSex;
	/// 比较类型
	std::string m_strOp;
	/// 比较数量 
	WORD m_wdNum;
};


class MODI_CheckLevel_C: public MODI_Condition
{
 public:
	MODI_CheckLevel_C()
	{
		m_wdLevel = 0;
	}
	
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	WORD	m_wdLevel;
	std::string m_strOp;
};

class MODI_DayTimeBegin : public MODI_Condition
{
public:
	MODI_DayTimeBegin()
	{
	}
	bool	Init(const char * con_line);
	bool	is_valid(MODI_ClientAvatar * p_client);
private:
	BYTE	m_nHour;
	BYTE	m_nMin;	
	std::string  m_sOp;

};

class MODI_DayTimeEnd : public MODI_Condition
{
public:
	MODI_DayTimeEnd()
	{
	}
	bool	Init(const char * con_line);
	bool	is_valid(MODI_ClientAvatar * p_client);
private:

	BYTE	m_nHour;
	BYTE	m_nMin;	
	std::string  m_sOp;
};

class MODI_WeekOp :public  MODI_Condition
{
public:
	MODI_WeekOp()
	{
		m_wSign=0;
	}
	bool	Init(const char * con_line);
	bool	is_valid(MODI_ClientAvatar * p_client);
private:
	std::string m_sOp;
	std::string m_sDays;
	WORD	m_wSign;
};

class MODI_Rands : public  MODI_Condition
{
public:
	MODI_Rands()
	{
		m_nRands = 0;
	}
	bool	Init(const char * con_line);
	bool	is_valid(MODI_ClientAvatar * p_client);

private:
	DWORD	m_nRands;

};


/**
 * @brief 获取注册时间
 * 
 */
class MODI_CheckRegisterTime: public MODI_Condition
{
 public:
	bool Init(const char * action_line);
	bool is_valid(MODI_ClientAvatar * p_client);
 private:
	time_t m_qdBeginTime;
	time_t m_qdEndTime;
};



/**
 * @brief 获取最后登录时间
 * 
 */
class MODI_CheckLastLoginTime: public MODI_Condition
{
 public:
	bool Init(const char * action_line);
	bool is_valid(MODI_ClientAvatar * p_client);
 private:
	std::string m_op;
	time_t m_qdBeginTime;
	time_t m_qdEndTime;
};


/**
 * @brief 获取注册时间
 * 
 */

class MODI_GVarCondition : public MODI_Condition
{
public:
	bool Init(const char * action_line);
	bool is_valid(MODI_ClientAvatar *p_client);
private:
	std::string m_strName;
	WORD m_wdCondition;
	std::string m_strOp;

};

/**
 * @brief 称号检测
 * 
 */
class	MODI_ChenghaoCondition : public MODI_Condition
{
public:
	bool	Init(const char * action_line);
	bool	is_valid(MODI_ClientAvatar  * p_client);

private:
	std::string	m_strOp;
	WORD	m_wdCondition;

};
/**
 * @brief  判断是否是过期的会员
 *
 */
class	MODI_IsExpireVip : public MODI_Condition
{
public:
	bool	Init(const char * action_line);
	bool	is_valid( MODI_ClientAvatar * p_client);
private:
};

/**
 * @brief 是否是房主
 *
 */
class	MODI_IsRoomOwner : public MODI_Condition
{
public:
	bool	Init( const char * action_line)
	{
		return true;
	}
	bool	is_valid(MODI_ClientAvatar  * p_client);


private:


};
/**
 * @brief 判断是否是会员
 *
 */
class	MODI_IsVip  : public MODI_Condition
{
public:

	bool	Init( const char *action_line);
	bool	is_valid(MODI_ClientAvatar * p_client);

private:	
	std::string	m_sOpt;

};


/**
 * @brief 是否到了，删除好友的时间了
 * 
 */

class 	MODI_IsTimeToDeleteFriend : public MODI_Condition
{
public:
	bool	Init( const char * action_line);
	bool	is_valid( MODI_ClientAvatar  * p_client);

private:
	WORD	m_nDays;
	WORD	m_nAllDays;
};


/**
 * @brief 是否在游戏中
 *
 */

class	MODI_IsInRoomPlaying  : public  MODI_Condition
{
public:
	bool	Init( const char * action_line);
	bool	is_valid( MODI_ClientAvatar   * p_client);


private:

	std::string	m_strOp;

};

/**
 * @brief  是否是旁观者
 *
 */
class	MODI_IsSpector : public MODI_Condition
{
public:
	bool	Init( const char * action_line);
	bool	is_valid(MODI_ClientAvatar  * p_client);


private:
	std::string	m_strOp;

};


/**
 * @brief   房间模式
 *
 */

class	MODI_RoomMode  :public MODI_Condition
{
public:
	bool	Init( const char * action_line);
	bool	is_valid( MODI_ClientAvatar * p_client);
private:
	////1   normal   3 key  5 team
	std::string	m_strOp;
	WORD	m_nMode;

};

class MODI_CheckPayMoney: public MODI_Condition
{
 public:
	bool Init(const char * action_line);
	bool is_valid(MODI_ClientAvatar * p_client);
	
 private:
	std::string m_strOp;
	DWORD m_dwCondition;
};

#endif





