/**
 * @file ClientSessionMgr.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 客户端会话管理器
 *
 */

#ifndef CLIENT_SESSION_MGR_GS_H_
#define CLIENT_SESSION_MGR_GS_H_

#include <map>
#include "ClientSession.h"
#include "SingleObject.h"

/**
 * @brief 客户端会话管理器
 *
 */
class MODI_ClientSessionMgr : public CSingleObject<MODI_ClientSessionMgr>
{
public:

    MODI_ClientSessionMgr();
    ~MODI_ClientSessionMgr();

public:

    ///	添加一个客户端会话
    bool Add(MODI_ClientSession * p);

    ///	删除一个客户端会话
    bool Del(const MODI_SessionID & id );
    bool Del(MODI_ClientSession * p );

    ///	判断某会话是否存在
    bool IsIn(const MODI_SessionID & id);

    ///	根据会话ID查找一个客户端会话
    MODI_ClientSession * Find(const MODI_SessionID & id);


    ///	客户端会话个数
    size_t Size() const;

	void DisconnectionRubbishClients( unsigned long nTimeOut );

private:

    typedef std::map<MODI_SessionID, MODI_ClientSession *> MAP_SESSION;
    typedef MAP_SESSION::iterator MAP_SESSION_ITER;
    typedef std::pair<MAP_SESSION_ITER, bool> MAP_SESSION_INSERT_RESULT;

    MAP_SESSION m_mapSessions;

};

#endif
