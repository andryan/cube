#include "GameServerAPP.h"
#include "Base/HelpFuns.h"


#define CHECK_ITEM_TYPE(lower,upper,type) \
	for( int i = lower; i <= upper; i ++ ) \
	{\
		MODI_ASSERT( GetItemType( i ) == type );\
	}

void DebugCheck()
{
#ifdef _DEBUG
	CHECK_ITEM_TYPE(1,2000,enItemType_Head);
	CHECK_ITEM_TYPE(2001,4000,enItemType_Hair);
	CHECK_ITEM_TYPE(4001,6000,enItemType_Upper);
	CHECK_ITEM_TYPE(6001,8000,enItemType_Lower);
	CHECK_ITEM_TYPE(8001,10000,enItemType_Hand);
	CHECK_ITEM_TYPE(10001,12000,enItemType_Foot);
	CHECK_ITEM_TYPE(12001,14000,enItemType_Headdress);
	CHECK_ITEM_TYPE(14001,16000,enItemType_Glass);
	CHECK_ITEM_TYPE(16001,18000,enItemType_Earring);
	CHECK_ITEM_TYPE(18001,20000,enItemType_Mic);
	CHECK_ITEM_TYPE(20001,22000,enItemType_Mousedress);
	CHECK_ITEM_TYPE(22001,24000,enItemType_Necklace);
	CHECK_ITEM_TYPE(24001,26000,enItemType_Watch);
	CHECK_ITEM_TYPE(26001,28000,enItemType_Ring);
	CHECK_ITEM_TYPE(28001,30000,enItemType_Back);
	CHECK_ITEM_TYPE(30001,32000,enItemType_Pet);
	CHECK_ITEM_TYPE(32001,34000,enItemType_Tail);
	CHECK_ITEM_TYPE(34001,36000,enItemType_Suit);

	CHECK_ITEM_TYPE(50001,51000,enItemType_Item_Laba);
	CHECK_ITEM_TYPE(51001,52000,enItemType_Item_Jingyanka);
	CHECK_ITEM_TYPE(52001,53000,enItemType_Item_Bianxingka);
	CHECK_ITEM_TYPE(53001,54000,enItemType_Item_Xiaoguoka);
#endif
}

int main(int argc, char * argv[])
{
#if 0	
	for( size_t n = 0; n < 10000000; n++ )
	{

		BYTE s_maxp , maxp;
		BYTE s_cp , cp;
		BYTE s_maxs , maxs;
		BYTE s_sp , sp;
		BYTE s_boy , boy;
		bool s_off , off;


		s_maxp = rand()%MAX_PLAYSLOT_INROOM + 1;
		s_cp = rand()%s_maxp + 1;
		s_maxs = rand()%MAX_SPECTATORSLOT_INROOM + 1;
		s_sp = rand()%s_maxs + 1;
		s_boy = rand()%s_sp + 1;
		s_off = rand()%2;

		defRoomStateMask res = RoomHelpFuns::EncodeRoomState( s_maxp ,  s_cp , s_maxs , s_sp , s_boy , s_off );
		RoomHelpFuns::DecodeRoomState( res ,  maxp , cp , maxs , sp , boy , off );
		MODI_ASSERT( maxp == s_maxp );
		MODI_ASSERT( cp == s_cp );
		MODI_ASSERT( maxs == s_maxs );
		MODI_ASSERT( sp == s_sp );
		MODI_ASSERT( boy == s_boy );
		MODI_ASSERT( off == s_off );
	}
#endif	

	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	MODI_GameServerAPP app("Game Server");

	int iRet = app.Init();
	
	DebugCheck();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	app.Shutdown();

	::sleep(1);
	return iRet;
}
