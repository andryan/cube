#include "MusicHeaderData.h"
#include "protocol/gamedefine.h"


MODI_MusicHeaderData::MODI_MusicHeaderData():m_pDataBuf(0),m_bOwner(false),m_nSize(0),m_bHasData(false)
{
	memset( m_szData , 0 , sizeof(m_szData) );
}
	
MODI_MusicHeaderData::~MODI_MusicHeaderData()
{
	Reset();
}

bool 	MODI_MusicHeaderData::SetData( const char * pData , unsigned int nSize )
{
	Reset();

	if( nSize >  MAX_MUSICFILE_HEADER )
	{
		return false;
	}

	// 数据很大的话，我们再使用动态分配，一般的话内存都是来自数组
	if( nSize > sizeof(m_szData) )
	{
		Reset();
		m_pDataBuf = new char [nSize];
		m_bOwner = true;
	}
	if(pData)
	{		
		memcpy( m_pDataBuf ,  pData , nSize );
	}
	m_nSize = nSize;
	m_bHasData = true;
	return true;
}

void 	MODI_MusicHeaderData::Reset()
{
	if( m_bOwner )
	{
		delete [] m_pDataBuf;
		m_pDataBuf = 0;
		m_nSize = 0;
	}

	m_bOwner = false;
	m_pDataBuf = &m_szData[0];
	m_nSize = sizeof(m_szData);
	m_bHasData = false;
}


