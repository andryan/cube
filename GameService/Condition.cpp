/**
 * @file   Condition.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 16:59:06 2011
 * 
 * @brief  条件实现
 * 
 * 
 */

#include "Global.h"
#include "SplitString.h"
#include "Condition.h"
#include "GameUserVar.h"
#include "GameTick.h"
#include "Share.h"
#include "GlobalVar.h"
#include "GameLobby.h"
#include "GameChannel.h"

/** 
 * @brief 条件变量初始化
 * 
 * @param con_line 内容
 * 
 * @return 成功true
 *
 */
static int GetMonDays(int month, int year)
{
	int 	a[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	bool	leap;	

	leap=( year % 400 ==0 || year % 4== 0 && year % 100 != 0 );
	return ( a[month-1] + (leap && (month== 2 ) ));
}
bool MODI_VarCondition::Init(const char * con_line)
{
	if(! con_line)
		return false;
	
#ifdef _DEBUG
	Global::logger->debug("[var condition debug] ------conline= %s",con_line);
#endif
	MODI_SplitString split;
	split.InitXML(con_line);
	m_strName = split["name"];
	m_strAttribute = split["attribute"];
	m_strOp = split["op"];
	m_wdCondition = atoi(split["condition"]);
	m_stVarToOp = split["varname"];
	return true;
}


/** 
 * @brief 条件变量判断 
 * 
 * @param p_client 该用户的变量
 * 
 * @return 有效true,无效false
 *
 */
bool MODI_VarCondition::is_valid(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		Global::logger->fatal("[check_condition] condition isvalid <name=%s,op=%s>", m_strName.c_str(), m_strOp.c_str());
		MODI_ASSERT(0);
		return false;
	}

	MODI_GameUserVar * p_var = p_client->m_stGameUserVarMgr.GetVar(m_strName.c_str());
	
	if(! p_var)
	{
		p_var = new MODI_GameUserVar(m_strName.c_str());
		if(p_var->Init(p_client, m_strAttribute.c_str()))
		{
			if(! p_client->m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	if(!p_var)
	{
		MODI_ASSERT(0);
		return false;
	}
#ifdef _VAR_DEBUG
	//	Global::logger->debug("[check_condition] check condition <name=%s,op=%s,condition=%u,value=%u>", p_var->GetName(),
	//				  m_strOp.c_str(), m_wdCondition, p_var->GetValue());
#endif	
	if(m_strOp == "great")
	{
		return   p_var->GetValue() > m_wdCondition;
	}
	else if(m_strOp == "less")
	{
		return p_var->GetValue() < m_wdCondition;
	}
	else if(m_strOp == "equ")
	{
		return m_wdCondition == p_var->GetValue();
	}
	else if(m_strOp == "diff")
	{
		return m_wdCondition != p_var->GetValue();
	}
	else if( m_strOp == "varequ")
	{
		MODI_GameUserVar * _p_var2 = NULL;
		GETVAR(m_stVarToOp.c_str(),m_strAttribute.c_str(),p_client,_p_var2);		
		MODI_ASSERT(_p_var2);
		return ( _p_var2->GetValue() == p_var->GetValue()); 
	}
	else if( m_strOp == "vargreat")
	{
		
		MODI_GameUserVar * _p_var2 = NULL;
		GETVAR(m_stVarToOp.c_str(),m_strAttribute.c_str(),p_client,_p_var2);		
		MODI_ASSERT(_p_var2);
#ifdef _DEBUG
		Global::logger->debug("_var_great   var1 %s,value %d ,value2 %s value %d ,sex= %d",p_var->GetName(),p_var->GetValue(),_p_var2->GetName(),_p_var2->GetValue(),p_client->GetSex());
#endif
		return ( _p_var2->GetValue() < p_var->GetValue()); 
	}
	else if( m_strOp == "varless")
	{

		MODI_GameUserVar * _p_var2 = NULL;
		GETVAR(m_stVarToOp.c_str(),m_strAttribute.c_str(),p_client,_p_var2);		
		MODI_ASSERT(_p_var2);
		return ( _p_var2->GetValue() > p_var->GetValue()); 
	}
	else
	{
		return false;
	}
}


/** 
 * @brief 开始时间
 * 
 * 
 */
bool MODI_BeginTime_C::Init(const char * con_line)
{
	if(!con_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(con_line);
	m_wdYear = atoi(split["year"]);
	m_byMon = atoi(split["mon"]);
	m_byDay = atoi(split["day"]);
	m_byHour = atoi(split["hour"]);
	m_byMin = atoi(split["min"]);
	return true;
}


/** 
 * @brief 开始时间
 * 
 * 
 */
bool MODI_BeginTime_C::is_valid(MODI_ClientAvatar * p_client)
{
	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	BYTE cur_mon = MODI_GameTick::m_stTTime.GetMon();
	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();
	BYTE cur_hour = MODI_GameTick::m_stTTime.GetHour();
	BYTE cur_min = MODI_GameTick::m_stTTime.GetMin();
	
// #ifdef _VAR_DEBUG
// 	Global::logger->debug("[check_begin_time] cur_year=%u,cur_mon=%d,cur_day=%d,cur_hour=%d,cur_min=%d,by=%u,bm=%d,ed=%d,eh=%d,em=%d",
// 						  cur_year,cur_mon,cur_day,cur_hour, cur_min,
// 						  m_wdYear, m_byMon,m_byDay, m_byHour, m_byMin);
// #endif

	if(cur_year > m_wdYear)
	{
		return true;
	}
	else if(cur_year == m_wdYear)
	{
		if(cur_mon > m_byMon)
		{
			return true;
		}
		else if(m_byMon == cur_mon)
		{
			if(cur_day > m_byDay)
			{
				return true;
			}
			else if(m_byDay == cur_day)
			{
				if(cur_hour > m_byHour)
				{
					return true;
				}
				else if(m_byHour == cur_hour)
				{
					if(cur_min >= m_byMin)
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{	
		return false;
	}
	return false;
}



/** 
 * @brief 结束时间
 * 
 * 
 */
bool MODI_EndTime_C::Init(const char * con_line)
{
	if(!con_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(con_line);
	m_wdYear = atoi(split["year"]);
	m_byMon = atoi(split["mon"]);
	m_byDay = atoi(split["day"]);
	m_byHour = atoi(split["hour"]);
	m_byMin = atoi(split["min"]);
	return true;
}



/** 
 * @brief 结束时间
 * 
 * 
 */
bool MODI_EndTime_C::is_valid(MODI_ClientAvatar * p_client)
{
	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	BYTE cur_mon = MODI_GameTick::m_stTTime.GetMon();
	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();
	BYTE cur_hour = MODI_GameTick::m_stTTime.GetHour();
	BYTE cur_min = MODI_GameTick::m_stTTime.GetMin();
	
// #ifdef _HRX_DEBUG
// 	Global::logger->debug("[check_end_time] cur_year=%u,cur_mon=%d,cur_day=%d,cur_hour=%d,cur_min=%d,by=%u,bm=%d,ed=%d,eh=%d,em=%d",
// 						  cur_year,cur_mon,cur_day,cur_hour, cur_min,
// 						  m_wdYear, m_byMon,m_byDay, m_byHour, m_byMin);
// #endif

	if(cur_year < m_wdYear)
	{
		return true;
	}
	else if(cur_year == m_wdYear)
	{
		if(cur_mon < m_byMon)
		{
			return true;
		}
		else if(m_byMon == cur_mon)
		{
			if(cur_day < m_byDay)
			{
				return true;
			}
			else if(m_byDay == cur_day)
			{
				if(cur_hour < m_byHour)
				{
					return true;
				}
				else if(m_byHour == cur_hour)
				{
					if(cur_min < m_byMin)
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		return false;
	}
	else
	{
		return false;
	}
	return false;
}


/** 
 * @brief 结束时间
 * 
 * 
 */
bool MODI_CheckItemNum_C::Init(const char * con_line)
{
	if(!con_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(con_line);
	m_wdItemID = atoi(split["id"]);
	m_wdNum = atoi(split["num"]);
	m_strOp = split["op"];
	return true;
}

bool	MODI_CheckSexType_C::Init( const char *con_line)
{
	if( !con_line)
	{

		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_strOp = split["op"];
	m_wSex = atoi( split["type"]);
	return true;
}


bool	MODI_OnlineCondition_C::Init( const char * con_line)
{
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_op = split["op"];
	timemin = atoi( split["condition"]);
	return true;

}

bool	MODI_OnlineCondition_C::is_valid(MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	WORD	minue= p_client->GetFullData().m_roleExtraData.m_nTodayOnlineTime;
	if( m_op == "equ")
	{
		return ( minue == timemin);
	}
	else if ( m_op == "great")
	{
		return (  minue > timemin);
	}
	else if( m_op == "less")
	{
		return ( timemin > minue);
	}
	else
	{
		return false;
	}
	return false;
}


bool	MODI_SongCountCondition::Init( const char * con_line)
{
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_op = split["op"];
	m_dwCondition = atoi( split["condition"]);
	return true;

}

bool	MODI_SongCountCondition::is_valid(MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	WORD	value = p_client->GetRoleDetailInfo().m_Chang + p_client->GetRoleDetailInfo().m_Heng + p_client->GetRoleDetailInfo().m_KeyBoard;
	if( m_op == "equ")
	{
		return ( value == m_dwCondition);
	}
	else if ( m_op == "great")
	{
		return (value > m_dwCondition);
	}
	else if( m_op == "less")
	{
		return ( value < m_dwCondition);
	}
	else
	{
		return false;
	}
	return false;
}

bool	MODI_CheckSexType_C::is_valid(MODI_ClientAvatar * p_client)
{

	if(!p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	else if(m_strOp == "equ")
	{
		return m_wSex == p_client->GetSex();
	}
	else if(m_strOp == "not")
	{
		return m_wSex !=  p_client->GetSex();
	}
	return false;
}
/** 
 * @brief 结束时间
 * 
 * 
 */
bool MODI_CheckItemNum_C::is_valid(MODI_ClientAvatar * p_client)
{
	if(!p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	WORD item_num = p_client->GetPlayerBag().GetCountByItemConfigId(m_wdItemID);
	
	if(m_strOp == "great")
	{
		return item_num > m_wdNum;
	}
	else if(m_strOp == "equ")
	{
		return item_num == m_wdNum;
	}
	else if(m_strOp == "less")
	{
		return item_num < m_wdNum;
	}
	return false;
}

bool	MODI_DayTimeBegin::Init(const char * con_line)
{
	
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_nHour = atoi(split["hour"]);
	m_nMin = atoi(split["min"]);
	m_sOp = split["op"];
	return true;
}
bool	MODI_DayTimeBegin::is_valid(MODI_ClientAvatar * p_client)
{

	BYTE cur_hour = MODI_GameTick::m_stTTime.GetHour();
	BYTE cur_min = MODI_GameTick::m_stTTime.GetMin();

	return ( (cur_hour>m_nHour) || ((cur_hour== m_nHour) && (cur_min > m_nMin)));
}

bool	MODI_DayTimeEnd::Init(const char * con_line)
{
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_nHour = atoi(split["hour"]);
	m_nMin = atoi(split["min"]);
	m_sOp = split["op"];
	return true;
}
bool 	MODI_DayTimeEnd::is_valid(MODI_ClientAvatar * p_client)
{
	BYTE cur_hour = MODI_GameTick::m_stTTime.GetHour();
	BYTE cur_min = MODI_GameTick::m_stTTime.GetMin();

	return !( (cur_hour>m_nHour) || ((cur_hour== m_nHour) && (cur_min > m_nMin)));

}

bool	MODI_WeekOp::Init(const char * con_line)
{
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_sOp = split["op"];
	m_sDays = split["days"];
	int i=0;
	WORD	mask =0x01;
	for(i=0; i< 7; ++i)
	{
		if( m_sDays[i] != '0')
		{
			m_wSign |= mask;
		}
		mask <<=1;
	}
	return true;
}
bool	MODI_WeekOp::is_valid(MODI_ClientAvatar * p_client)
{
	BYTE cur_week = MODI_GameTick::m_stTTime.GetWDay(); 	
	WORD	mask =0x01;

	if( m_sOp == "or")
	{
		mask <<= cur_week;	
		return (mask & m_wSign);
	}	
	else
	{
		return false;
	}

}

bool	MODI_Rands::Init(const char * con_line)
{
	if( !con_line)
	{
		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	
	m_nRands = atoi(split["rands"]);
	return true;
}

bool	MODI_Rands::is_valid(MODI_ClientAvatar * p_client)
{


	DWORD	_m_nrands = PublicFun::GetRandNum(1,1000000) % 1000000;
	if( m_nRands > _m_nrands)
	{
		return true;
	}
	return false;
}




/** 
 * @brief 检查注册时间
 * 
 *
 */
bool MODI_CheckRegisterTime::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(action_line);
	std::string m_strBeginTime = split["begin"];
	std::string m_strEndTime = split["end"];
	m_qdBeginTime = MODI_TimeFun::DayToSec(m_strBeginTime.c_str());
	m_qdEndTime = MODI_TimeFun::DayToSec(m_strEndTime.c_str());
	return true;
}


bool MODI_CheckRegisterTime::is_valid(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

#ifdef _DEBUG
	Global::logger->debug("[check_register_time] registertime=%llu, begintime=%llu, endtime=%llu",
						  p_client->GetFullData().m_roleExtraData.m_nRegisterTime, m_qdBeginTime, m_qdEndTime);
#endif	

	if(p_client->GetFullData().m_roleExtraData.m_nRegisterTime > m_qdBeginTime && p_client->GetFullData().m_roleExtraData.m_nRegisterTime < m_qdEndTime)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 检查最后登录时间
 * 
 *
 */
bool MODI_CheckLastLoginTime::Init(const char * action_line)
{
	if(!action_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(action_line);
	std::string m_strBeginTime = split["begin"];
	std::string m_strEndTime = split["end"];
	m_op = split["op"];
	m_qdBeginTime = MODI_TimeFun::DayToSec(m_strBeginTime.c_str());
	m_qdEndTime = MODI_TimeFun::DayToSec(m_strEndTime.c_str());
	return true;
}


bool MODI_CheckLastLoginTime::is_valid(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

#ifdef _DEBUG
	Global::logger->debug("[check_lastlogin_time] logintime=%llu, begintime=%llu, endtime=%llu,op=%s",
						  p_client->GetFullData().m_roleExtraData.m_nLastLoginTime, m_qdBeginTime, m_qdEndTime, m_op.c_str());
#endif	

	if(p_client->GetFullData().m_roleExtraData.m_nLastLoginTime > m_qdBeginTime && p_client->GetFullData().m_roleExtraData.m_nLastLoginTime < m_qdEndTime)
	{
		if(m_op == "equ")
		{
			return true;
		}
		else if(m_op == "not")
		{
			return false;
		}
	}
	if(m_op == "equ")
	{
		return false;
	}
	else if(m_op == "not")
	{
		return true;
	}
	
	return false;
}

bool MODI_GVarCondition::Init( const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strName = split["name"];

	m_strOp = split["op"];
	m_wdCondition = atoi(split["condition"]);
	return true;
}


bool MODI_GVarCondition::is_valid(MODI_ClientAvatar *p_client)
{
	WORD	_gvalue;

	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		Global::logger->fatal("[check_condition] condition isvalid <name=%s,op=%s>", m_strName.c_str(), m_strOp.c_str());
		MODI_ASSERT(0);
		return false;
	}
	MODI_GlobalVarMgr * mgr = MODI_GlobalVarMgr::GetInstance();
	_gvalue = mgr->GetVar(m_strName);


	if(m_strOp == "great")
	{
		return   _gvalue > m_wdCondition;
	}
	else if(m_strOp == "less")
	{
		return _gvalue < m_wdCondition;
	}
	else if(m_strOp == "equ")
	{
		return m_wdCondition == _gvalue;
	}
	else if(m_strOp == "diff")
	{
		return m_wdCondition != _gvalue;
	}
	return false;
}

bool	MODI_ChenghaoCondition::Init(const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strOp = split["op"];
	m_wdCondition = atoi( split["condition"]);
	return true;
}

bool	MODI_ChenghaoCondition::is_valid(MODI_ClientAvatar * p_client)
{

	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if( m_strOp == "" )
	{
		Global::logger->fatal("[check_condition] condition isvalid no op and no condition for the Chenghao");
		MODI_ASSERT(0);
		return false;
	}
	
	return ( p_client->GetFullData().m_roleInfo.m_normalInfo.m_wdChenghao == m_wdCondition);

}

bool	MODI_IsRoomOwner::is_valid( MODI_ClientAvatar  * p_client)
{
	if( !p_client->IsInMulitRoom())	
	{
		return  false;
	}
	return ( p_client->IsRoomMaster());

}

bool	MODI_IsExpireVip::Init( const char * action_line)
{
	return true;
}
bool	MODI_IsExpireVip::is_valid(MODI_ClientAvatar * p_client)
{
	
	MODI_GameUserVar   *p_var = NULL;
	GETVAR("vip_expired","type=normal",p_client,p_var);

	if( p_var == NULL)
		return false;	
	if( p_var->GetValue() == 0)
		return false;
	

	return true;
}

bool	MODI_IsVip::Init( const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);

	/// op 是is 或者是no
	m_sOpt = split["op"];
	return true;
}

bool	MODI_IsVip::is_valid( MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	bool is_vip = p_client->IsVip();
	
	if( m_sOpt == "is")
	{
		if(is_vip == 1)
		{
			Global::logger->debug("[is_vip] <accname=%s,op=%s,is_vip=%d,reutrn=true>",
						  p_client->GetAccName(), m_sOpt.c_str(), is_vip);
			return true;
		}
		else
		{
			Global::logger->debug("[is_vip] <accname=%s,op=%s,is_vip=%d,return=false>",
						  p_client->GetAccName(), m_sOpt.c_str(), is_vip);
			return false;
		}
	}

	if(is_vip == 1)
	{
		Global::logger->debug("[is_vip] <accname=%s,op=%s,is_vip=%d,reutrn=false>",
					  p_client->GetAccName(), m_sOpt.c_str(), is_vip);
		return false;
	}
	else
	{
		Global::logger->debug("[is_vip] <accname=%s,op=%s,is_vip=%d,return=true>",
					  p_client->GetAccName(), m_sOpt.c_str(), is_vip);
		return true;
	}
}

bool	MODI_IsTimeToDeleteFriend::Init( const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);

	m_nDays = atoi(split["days"]);
	m_nAllDays = atoi(split["alldays"]);

	return true;
}

bool	MODI_IsTimeToDeleteFriend::is_valid( MODI_ClientAvatar * p_client)
{
	MODI_GameUserVar   *p_var = NULL;
	GETVAR("vip_expired","type=normal",p_client,p_var);
	MODI_TTime 	now;

	MODI_ASSERT(p_var);		

	if( p_var->GetValue() == 0)
	{
		return false;
	}
	
	now= Global::m_stRTime;	
	int year = now.GetYear();
	int mon=now.GetMon();
	int day=now.GetMDay(); 
	int expiremon= (p_var->GetValue() >> 8)	&0xff;
	int expireday = (p_var->GetValue() ) & 0xff;
	int expired = 0;


	if( mon-1 > expiremon)
	{
		return true;		
	}		
	else if( mon > expiremon)
	{
		int tmpday =	GetMonDays(expiremon,year);
		expired = tmpday - expireday + day;	

	}
	else
	{
		expired = day - expireday;	
	}
	expired -= m_nAllDays;

	Global::logger->debug("[time_to_del] time to del friend  year=%d,mon=%u,day=%u,expiremon=%u,expireday=%u,expired=%d,value=%d,alldays=%d",year,mon,day,expiremon,expireday,expired,p_var->GetValue(),m_nAllDays);
	return (expired >= 0);

}

bool	MODI_IsInRoomPlaying::Init( const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strOp =  split["op"];

	return true;
}

bool	MODI_IsInRoomPlaying::is_valid(MODI_ClientAvatar  * p_client)
{
	if( !p_client)
		return false;

	if( m_strOp == "is")
	{
	}
	else
	{
	}
	return true;
}

bool	MODI_IsSpector::Init( const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strOp =  split["op"];
	return true;

}

bool	MODI_IsSpector::is_valid(MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		return false;
	}
	if( m_strOp == "is" )
	{
	}
	else
	{
	}
	return true;
}


bool	MODI_RoomMode::Init(const char * action_line)
{

	if(! action_line)
		return false;
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strOp =  split["op"];

	m_nMode = atoi(split["mode"]);
	return true;
}


bool	MODI_RoomMode::is_valid(MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		return false;
	}
	
	if ( !p_client->IsInMulitRoom())
	{
		return false;
	}	

	MODI_GameLobby   *pLobby = MODI_GameLobby::GetInstancePtr();
	
	MODI_MultiGameRoom * pMultiRoom = pLobby->GetMultiRoom( p_client->GetRoomID() );
	if(! pMultiRoom )
	{
		return false;
	}
	return (pMultiRoom->GetSubType() == m_nMode);
}


bool	MODI_CheckLevel_C::Init( const char *con_line)
{
	if( !con_line)
	{

		return false;
	}
	MODI_SplitString split;
	split.InitXML(con_line);
	m_strOp = split["op"];
	m_wdLevel = atoi( split["condition"]);
	return true;
}

bool	MODI_CheckLevel_C::is_valid(MODI_ClientAvatar * p_client)
{

	if(!p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	else if(m_strOp == "equ")
	{
		return m_wdLevel == p_client->GetLevel();
	}
	else if(m_strOp == "great")
	{
		return m_wdLevel <  p_client->GetLevel();
	}
	else if(m_strOp == "less")
	{
		return m_wdLevel >  p_client->GetLevel();
	}
	return false;
}


bool MODI_CheckPayMoney::Init( const char *con_line)
{
	if( !con_line)
	{
		return false;
	}
	
	MODI_SplitString split;
	split.InitXML(con_line);
	m_strOp = split["op"];
	m_dwCondition = atoi( split["condition"]);
	return true;
}

 
bool MODI_CheckPayMoney::is_valid(MODI_ClientAvatar * p_client)
{
	if( !p_client)
	{
		return false;
	}

	if(m_strOp == "less")
	{
		return p_client->GetPayMoney() < m_dwCondition;
	}
	else if(m_strOp == "equ")
	{
		return p_client->GetPayMoney() == m_dwCondition;
	}
	else if(m_strOp == "great")
	{
		return p_client->GetPayMoney() > m_dwCondition;
	}
	return false;
}

 

 
