/** 
 * @file ImpactCore.h
 * @brief 效果的核心处理模块
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-07
 */
#ifndef IMPACT_CORE_H_H_
#define IMPACT_CORE_H_H_

class 	MODI_ImpactTemplate;
class 	MODI_ImpactObj;
class 	MODI_ClientAvatar;

#include "BinFileMgr.h"

class 	MODI_ImpactCore
{
	private:
		MODI_ImpactCore();
		~MODI_ImpactCore();


	public:

		/** 
		 * @brief 检查效果系统相关的数据/配置文件
		 * 
		 * 
		 * @return 	成功返回true
		 */
		static bool 	CheckConfigFile();

		static bool 	RegisterImpact( MODI_ClientAvatar * pCaster , MODI_ClientAvatar * pTarget , WORD nConfigID , void * pUserdata , DWORD nContinuance );


		/** 
		 * @brief 获得指定效果ID的配置信息
		 * 
		 * @param nConfigID 	效果ID
		 * 
		 * @return 	存在则返回配置信息
		 */

		static const GameTable::MODI_Impactlist * GetConfigData( WORD nConfigID );

		/** 
		 * @brief 获得某个效果对象关联的效果模板
		 * 
		 * @param impactObj 	效果对象
		 * 
		 * @return 	返回效果模板
		 */
		static MODI_ImpactTemplate * GetImpactTempalte( WORD nConfigID , int * pResult );

		/** 
		 * @brief 获得某个效果对象关联的效果模板
		 * 
		 * @param impactObj 	效果对象
		 * 
		 * @return 	返回效果模板
		 */
		static MODI_ImpactTemplate * GetImpactTempalte( const MODI_ImpactObj & impactObj , int * pResult );

		/** 
		 * @brief 初始化效果对象
		 * 
		 * @param impactObj 	需要初始化的效果对象
		 * @param impactData 	效果配置数据
		 * 
		 * @return 	成功返回 kImpactH_Successful
		 */
		static int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		/** 
		 * @brief 激活某个效果到指定对象上
		 * 
		 * @param impactObj 	需要激活的效果
		 * @param pObj 			效果依附的对象
		 * 
		 * @return 	成功返回 kImpactH_Successful
		 */
		static int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		/** 
		 * @brief 更新某个效果对象
		 * 
		 * @param impactObj 	效果对象
		 * @param pObj 			效果依附的对象
		 * @param nTime 		时间
		 * 
		 * @return 	成功返回 kImpactH_Successful
		 */
		static int Update( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj , unsigned long nTime );

		/** 
		 * @brief 取消激活某个效果
		 * 
		 * @param impactObj 	效果对象
		 * @param pObj 			效果依附的对象
		 *  
		 * @return 	成功返回 kImpactH_Successful
		 */
		static int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		/** 
		 * @brief 清理某个效果
		 * 
		 * @param impactObj 	效果对象
		 * @param pObj 			效果依附的对象
		 * 
		 * @return 	成功返回 kImpactH_Successful
		 */
		static int CleanUp( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

	private:

		static void Log( MODI_ClientAvatar * pCaster , MODI_ClientAvatar * pTarget , WORD nConfigID , int iRes , const char * szActionName );
};

#endif
