/** 
 * @file GameServerAPP.h
 * @brief 游戏服务器程序框架
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MODI_GAMESERVER_APPFRAME_H_
#define MODI_GAMESERVER_APPFRAME_H_


#include <string>
#include <vector>
#include "IAppFrame.h"
#include "AssertEx.h"
#include "GameService.h"
#include "ZoneClient.h"
#include "GameTick.h"
#include "GameChannelMgr.h"
#include "BinFileMgr.h"
#include "MdmMgr.h"
#include "ProgressBar.h"
#include "protocol/c2gs.h"
#include "PackageHandler_c2gs.h"
#include "PackageHandler_gs2zs.h"
#include "GUIDCreator.h"
#include "Channellist.h"
#include "ClientSessionMgr.h"
#include "GMCommand.h"
#include "GameChannel.h"
#include "DefaultAvatarCreator.h"
#include "ZoneClient.h"
#include "ImpactTemplateMgr.h"
#include "YunYingKeyExchange.h"
#include "Base/DisableStrTable.h"
#include "DBStruct.h"
#include "DBClient.h"



class 	MODI_GameServerAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_GameServerAPP( const char * szAppName );

		virtual ~MODI_GameServerAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

		const char * GetServerName() const
		{
			return m_strServerName.c_str();
		}


		/** 
		 * @brief 直接保存数据库
		 * 
		 * @param sql 要执行的sql
		 * @param sql_len sql的长度
		 *
		 */
		static void ExecSqlToDB(const char * sql, const WORD sql_len);
		
		WORD 		GetNextTransCount() { m_nTransCount += 1; return m_nTransCount; }

		static MODI_TableStruct * m_pCharactersTbl;

		static QWORD m_qdItemId;

	private:

		bool InitChannels();

	private:

		MODI_SvrChannellist  		m_Channellist; // 当前区的频道列表
		MODI_PackageHandler_c2gs 	m_PackageHandler_c2gs;	// 数据包处理器
//		MODI_PackageHandler_gs2ms 	m_PackageHandler_gs2ms; // 数据包处理器
		MODI_PackageHandler_gs2zs 	m_PackageHandler_gs2zs; // 数据包处理器
//		MODI_RDBClient_GS 	* 		m_pRDBSvrClient; // role db sever 的客户端
		MODI_ZoneServerClient * 	m_pZoneServerClient; // zoneserver 的客户端
		MODI_GUIDCreator 	* 		m_pGUIDCreator; // guid 生成器
		MODI_ClientSessionMgr * 	m_pSessionMgr; // session 管理器
		MODI_GMCmdHandler_gs 		m_gmCmdHandler; // gm 命令处理器
		MODI_GameChannel  			m_gameChannel; // 游戏频道对象
//		MODI_DefaultAvatarCreator 	m_DefaultCreator; // 默认avatar形象创建
		std::string 				m_strServerName; // 服务器频道名
		MODI_ImpactTemplateMgr 		m_ImpactTMgr;
		MODI_YunYingKeyExchange 	m_YYKeyExchange;
		WORD 						m_nTransCount;
		MODI_DisableStrTable 		m_DisableStrTable;
};

#endif
