#include "RandomBox.h"
#include "AssertEx.h"
//#include "GlobalConfig.h"
#include "protocol/c2gs_itemcmd.h"
#include "ScriptManager.h"
#include "protocol/c2gs_itemcmd.h"
#include "GameChannel.h"

#define	MAX_RANDOM_CLASS_NUM	10
using namespace GameTable;

MODI_RandomBoxMgr  *MODI_RandomBoxMgr::m_pRandomBoxMgr= NULL;


MODI_RandomBoxMgr::MODI_RandomBoxMgr()
{
}

MODI_RandomBoxMgr * MODI_RandomBoxMgr::GetInstance()
{
	if( !m_pRandomBoxMgr)
	{
		m_pRandomBoxMgr = new MODI_RandomBoxMgr();
	}
	return m_pRandomBoxMgr;	
}

bool	MODI_RandomBoxMgr::Init()
{
	size_t i=0;
	defAllResourceBinFile  & AllBin = MODI_BinFileMgr::GetInstancePtr()->GetAllResourceBinFile();
	defAvatarBinFile & avatarbin = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile();
	
	size_t size = AllBin.GetSize();

	WORD insert_num = 0;
	DWORD get_configid = 0;

	/// 加载物品类型
	for( i=0; i< size; ++i)
	{
		const MODI_AllResource  * res = AllBin.GetByIndex(i);
		if(!res)
		{
			continue;
		}
		WORD get_class = res->get_Distribution();
		if( get_class != 0)
		{
			get_configid = res->get_ConfigID();
			if(get_configid > 49999)
			{
				insert_num = get_class * 1000;
				defListItor  itor_man = m_ItemMgr.find(insert_num+1);
				defListItor itor_feman = m_ItemMgr.find(insert_num);
				if( itor_man == m_ItemMgr.end())
				{
					std::vector<WORD>  m_vec;
			       	m_vec.push_back(get_configid);
					m_ItemMgr.insert( defListValue(insert_num+1,m_vec));
				}
				else
				{
					itor_man->second.push_back(get_configid);
				}

				if( itor_feman == m_ItemMgr.end())
				{
					std::vector<WORD>  m_vec;
			       	m_vec.push_back(get_configid);
					m_ItemMgr.insert( defListValue(insert_num,m_vec));
				}
				else
				{
					itor_feman->second.push_back(get_configid);
				}
			}
			else
			{
				const GameTable::MODI_Avatar  * p_avatar = avatarbin.Get(get_configid);
				if(p_avatar)
				{
					BYTE req_sex = p_avatar->get_SexReq();
					insert_num = get_class * 1000;
					defListItor itor_feman = m_ItemMgr.find(insert_num);
					/// feman
					if(req_sex == 0)
					{
						if( itor_feman == m_ItemMgr.end())
						{
							std::vector<WORD>  m_vec;
							m_vec.push_back(get_configid);
							m_ItemMgr.insert( defListValue(insert_num,m_vec));
						}
						else
						{
							itor_feman->second.push_back(get_configid);
						}
					}
					else
					{
						defListItor  itor_man = m_ItemMgr.find(insert_num+1);
						if( itor_man == m_ItemMgr.end())
						{
							std::vector<WORD>  m_vec;
							m_vec.push_back(get_configid);
							m_ItemMgr.insert( defListValue(insert_num+1,m_vec));
						}
						else
						{
							itor_man->second.push_back(get_configid);
						}
					}
				}
			}
		}
	}

#ifdef _DEBUG1
	defListItor iter = m_ItemMgr.begin();
	for(;iter != m_ItemMgr.end(); iter++)
	{
		std::vector<WORD> & vec_ref  = iter->second;
		std::vector<WORD>::iterator iter_vec = vec_ref.begin();
		for(; iter_vec!= vec_ref.end(); iter_vec++)
		{
			Global::logger->debug("[load_rand_item] <class=%u,sex=%d,configid=%u>", (iter->first)/1000, (iter->first)%1000, *iter_vec);
		}
	}
#endif

	/// 确保每个类有物品
	bool check_ok = true;
	for(DWORD i=1; i<35; i++)
	{
		DWORD class_num_man = i * 1000 + 1;
		DWORD class_num_feman = i * 1000;

		defListItor iter_man = m_ItemMgr.find(class_num_man);
		defListItor iter_feman = m_ItemMgr.find(class_num_feman);

		if(iter_feman == m_ItemMgr.end() || iter_man == m_ItemMgr.end())
		{
			Global::logger->debug("[check_load_rand_item_1] class not have any item <class=%u>", i);
			check_ok = false;
		}

		std::vector<WORD> & vec_ref_man  = iter_man->second;
		std::vector<WORD> & vec_ref_feman  = iter_feman->second;

		if(vec_ref_man.size() == 0 || vec_ref_feman.size() == 0)
		{
			Global::logger->debug("[check_load_rand_item_2] class not have any item <class=%u>", i);
			check_ok = false;
		}
	}
	
	if(check_ok == false)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	/// 加载概率
	defRandomdrawBinFile & randombin = MODI_BinFileMgr::GetInstancePtr()->GetRandomdrawBinFile();
	size = randombin.GetSize();
	for( unsigned int i=1; i < size+1; i++)
	{
		const MODI_Randomdraw  * res = randombin.Get(i);
		if(!res)
		{
			continue;
		}
		
		MODI_RandomInfo info;
		info.m_wClass = res->get_Interval();
		info.m_dwFrom = res->get_Starting();
		info.m_dwEnd = res->get_Distribution();
		info.m_wdNum = res->get_Maximize();
		info.m_RandNum.push_back(res->get_Arg1());
		info.m_RandNum.push_back(res->get_Arg2());
		info.m_RandNum.push_back(res->get_Arg3());
		info.m_RandNum.push_back(res->get_Arg4());
		info.m_RandNum.push_back(res->get_Arg5());
		info.m_RandNum.push_back(res->get_Arg6());
		info.m_RandNum.push_back(res->get_Arg7());
		info.m_RandNum.push_back(res->get_Arg8());

		WORD key_num = res->get_Number() * 1000 + res->get_Interval();
		m_RandMap[key_num] = info;
	}
	
	defRandMapItor rand_iter = m_RandMap.begin();
	bool ret_code_num = true;
	for(; rand_iter != m_RandMap.end(); rand_iter++)
	{
		MODI_RandomInfo & info = rand_iter->second;
		
#ifdef _DEBUG1		
		Global::logger->debug("[load_rand] <num=%u,class=%u,from=%u,end=%u,num=%u>",rand_iter->first/1000, info.m_wClass,
							  info.m_dwFrom,info.m_dwEnd,info.m_wdNum);
#endif
		bool ret_code = false;
		for(WORD i = 0; i<info.m_wdNum; i++)
		{
			
#ifdef _DEBUG1			
			Global::logger->debug("[load_rand] <%u=%u>", i, info.m_RandNum[i]);
#endif
			
			if(info.m_RandNum[i] == 1000000)
			{
				ret_code = true;
			}
		}
		
		if(ret_code == false && info.m_wdNum !=0)
		{
			Global::logger->debug("[load_rand_error] <num=%d,class=%u>",info.m_wdNum, info.m_wClass);
			ret_code_num = false;
		}
	}
	
	if(ret_code_num == false)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	return true;
}

WORD 	MODI_RandomBoxMgr::GetRandItemNum()
{
	DWORD	_m_nrands = PublicFun::GetRandNum(1,1000000) %1000000;	
	if( _m_nrands  <= 299999 )
	{
		return 1;
	}
	else if( _m_nrands <= 799999 && _m_nrands>299999)
	{
		return 2;
	}
	else if( _m_nrands <= 1000000 && _m_nrands >799999)
	{
		return 3;
	}
	else if( _m_nrands <= 0)
	{
		return 4;
	}

	return 0;	
}


const WORD	MODI_RandomBoxMgr::GetRandClass(const BYTE get_rand_num_type)
{
	DWORD	_m_nrands = PublicFun::GetRandNum(1,1000000) %1000000;
	WORD	_m_wClass=0;

	defRandMapItor iter = m_RandMap.begin();
	for(; iter != m_RandMap.end(); iter++)
	{
		WORD key_value = iter->first;
		if((key_value > get_rand_num_type * 1000) && (key_value < (get_rand_num_type+1) * 1000))
		{
			if(_m_nrands > (iter->second.m_dwFrom) && _m_nrands <= (iter->second.m_dwEnd))
			{
				_m_wClass = key_value;
				break;
			}
		}
	}
	
	return _m_wClass;
}



/** 
 * @brief 开宝盒随机获得道具
 * 
 * @param info 道具信息
 * @param get_rand_num_type 用那张随机表
 * 
 * @return 
 */
bool	MODI_RandomBoxMgr::GetRandItem(BagItemInfos  & info, const BYTE get_rand_num_type, const BYTE client_sex, bool & is_notify)
{
	/// 随机获取一个大类型
	WORD class_info = GetRandClass(get_rand_num_type);
	
	defRandMapItor iter_class = m_RandMap.find(class_info);
	if( iter_class == m_RandMap.end())
	{
		MODI_ASSERT(0);
		return false;
	}

	WORD xy_class = iter_class->second.m_wClass;
	/// 稀有类广播
	if(xy_class == 1 || xy_class == 2  || xy_class == 3 || xy_class == 4 ||
	   xy_class == 5 || xy_class == 6 || xy_class == 7 || xy_class == 8 ||
	   xy_class == 9 || xy_class == 10 || xy_class == 11 || xy_class == 17 ||
	   xy_class == 21 || xy_class == 22 || xy_class == 26 || xy_class == 27 || xy_class == 28||
	   xy_class == 31 || xy_class == 32 || xy_class == 33 )
	   {
		   is_notify |= true;
	   }
	
	DWORD temp_config = (iter_class->second.m_wClass) * 1000 + client_sex;
	defListItor iter = m_ItemMgr.find(temp_config);
	if(iter == m_ItemMgr.end())
	{
		MODI_ASSERT(0);
		return false;
	}

	DWORD temp = iter->second.size();
	DWORD  _m_nrands = PublicFun::GetRandNum(0,temp-1);

	/// get configid
	info.m_wConfigId = iter->second[_m_nrands];
	if(info.m_wConfigId == 0)
	{
		MODI_ASSERT(0);
		return false;
	}
	/// get num or get time
	if(info.m_wConfigId > 49999)
	{
		BYTE num = GetItemAttr(class_info);
		if(num == 0)
		{
			return false;
		}
		info.m_bNum = num;
		info.m_bTime = 3;
	}
	else
	{
		BYTE num = GetItemAttr(class_info);
		if(num == 0)
		{
			return false;
		}
		info.m_bTime = num;
		info.m_bNum = 1;
	}
	
	return true;
}


BYTE MODI_RandomBoxMgr::GetItemAttr(const WORD class_index)
{
	DWORD	_m_nrands = PublicFun::GetRandNum(1,1000000) %1000000;
	defRandMapItor iter_class = m_RandMap.find(class_index);
	if( iter_class == m_RandMap.end())
	{
		MODI_ASSERT(0);
		return false;
	}
	WORD loop = iter_class->second.m_wdNum;
	std::vector<DWORD> & vec_ref = iter_class->second.m_RandNum;
	
	for(WORD i=0; i<loop; i++)
	{
		if( _m_nrands < vec_ref[i])
		{
			return (i+1);
		}	
	}
	MODI_ASSERT(0);
	return 0;
}


/** 
 * @brief 打开宝盒
 * 
 * @param pObj 谁开
 * @param pt_null_cmd 开宝盒命令
 * @param cmd_size 命令大小
 *
 */
void	MODI_RandomBoxMgr::OpenItemBox( void *pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	bool is_notify = false;
	MODI_ClientAvatar * p_client = static_cast<MODI_ClientAvatar *>(pObj);
	if(! p_client)
	{
		MODI_ASSERT(0);
		return ;
	}
	
	MODI_Bag * p_bag = &(p_client->GetPlayerBag());
	if(! p_bag)
	{
		MODI_ASSERT(0);
		return ;
	}

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return;
	}

	MODI_C2GS_Request_OpenItemBox  *open  = (MODI_C2GS_Request_OpenItemBox *)pt_null_cmd;
	MODI_GS2C_Notify_BoxItemList 	boxlist;

	BYTE num = GetRandItemNum();
	if(num == 0)
	{
		MODI_ASSERT(0);
		return;
	}
	
	BYTE	i=0;
	boxlist.m_bNum = num;
	for( i=0; i<num ; ++i)
	{
		BagItemInfos  item;
		if(num == 1)
		{
			GetRandItem(item, 1, p_client->GetSex(), is_notify);
		}
		else if((num == 2) && (i == 0))
		{
			GetRandItem(item, 2,p_client->GetSex(), is_notify);
		}
		else if((num == 2) && (i == 1))
		{
			GetRandItem(item, 3,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 0))
		{
			GetRandItem(item, 4,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 1))
		{
			GetRandItem(item, 5,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 2))
		{
			GetRandItem(item, 6,p_client->GetSex(), is_notify);
		}
		
		if(item.m_wConfigId == 0)
		{
			MODI_ASSERT(0);
			return;
		}
		
		boxlist.m_sInfo[i] = item;
	}

	if( p_bag->GetEmptyPosCount() < 5)
	{
		Global::logger->info("[good_package] client not enough add item <name=%s>", p_client->GetRoleName());
		return;
	}
	
	bool	ok=false;
	for( i =0; i< num; ++i)
	{
		MODI_CreateItemInfo  create_info;
		create_info.m_dwConfigId = boxlist.m_sInfo[i].m_wConfigId;
		create_info.m_byLimit = boxlist.m_sInfo[i].m_bTime;
		create_info.m_byServerId = pApp->GetServerID();
		create_info.m_enCreateReason = enAddReason_ItemXYBox;
		BYTE	count=0;
		for( count =0; count < boxlist.m_sInfo[i].m_bNum; ++count)
		{
			QWORD	new_item_id=0;
			if( !p_bag->AddItemToBag(create_info,new_item_id))
			{
				MODI_ASSERT(0);
				break;
			}	
			ok=true;

			if(boxlist.m_sInfo[i].m_wConfigId >= 51003 && boxlist.m_sInfo[i].m_wConfigId <= 51012)
			{
				MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
			}

			if( boxlist.m_sInfo[i].m_wConfigId == 56003 || boxlist.m_sInfo[i].m_wConfigId == 56004)
			{
				if( p_client->IsVip())
				{
						
					MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
				}
			}

			MODI_TriggerBuyItem   script(boxlist.m_sInfo[i].m_wConfigId);
			MODI_BuyItemManager::GetInstance().Execute(p_client,script);
		}
	}	
	
	if( ok)
	{
		MODI_GameItemInfo  *p_item = p_client->GetPlayerBag().GetItemByItemId(open->m_qwBagId);	
		if( p_item)
		{
			p_item->DelMeFromBag(enDelReason_Used);
		}

		for( i=0; i< BOX_NEED_YINFU_NUM; ++i)
		{
			MODI_GameItemInfo *p_item_yinfu = p_client->GetPlayerBag().GetItemByItemId(open->m_qwYinFu[i]);	
			if( p_item_yinfu)
			{
				p_item_yinfu->DelMeFromBag(enDelReason_Used);
			}
		}
		p_client->SendPackage( &boxlist,sizeof(MODI_GS2C_Notify_BoxItemList));

		std::ostringstream os;
		if(is_notify)
		{
			os.str("");
			os<< "Selamat " << p_client->GetRoleName() << " Kamu beruntung, menemukan sebuah treasure box";
			/// 广播稀有类
			for(BYTE i=0; i<boxlist.m_bNum; i++)
			{
				const GameTable::MODI_Avatar * pAvater = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get(boxlist.m_sInfo[i].m_wConfigId);
				const GameTable::MODI_Itemlist * pElement = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get(boxlist.m_sInfo[i].m_wConfigId);
				if(pElement)
				{
					os<< " "<<pElement->get_Name() << "(" << (WORD)boxlist.m_sInfo[i].m_bNum <<")"; 
				}
				if(pAvater)
				{
					os<< " " << pAvater->get_Name() << "(" ;
					if(boxlist.m_sInfo[i].m_bTime == 1)
					{
						os<<"7Hari)";
					}
					else if(boxlist.m_sInfo[i].m_bTime == 2)
					{
						os<<"30Hari)";
					}
					else if(boxlist.m_sInfo[i].m_bTime == 3)
					{
						os<<"Permanen)";
					}
				}
			}
			char buf[Skt::MAX_USERDATASIZE];
			memset(buf, 0, sizeof(buf));
			MODI_S2ZS_Broadcast_SystemChat  * p_send_cmd = (MODI_S2ZS_Broadcast_SystemChat *)buf;
			AutoConstruct(p_send_cmd);
			p_send_cmd->m_wdSize = os.str().size();
			memcpy(p_send_cmd->m_pContent, os.str().c_str(), p_send_cmd->m_wdSize);
		
			MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
			if( pZone)
			{
				pZone->SendCmd(p_send_cmd, sizeof(MODI_S2ZS_Broadcast_SystemChat) + p_send_cmd->m_wdSize);
			}
		}
		/// 打印日志
		os.str("");
		for(BYTE i=0; i<boxlist.m_bNum; i++)
		{
			os<<boxlist.m_sInfo[i].m_wConfigId<<","<<(WORD)boxlist.m_sInfo[i].m_bNum<<","<<(WORD)boxlist.m_sInfo[i].m_bTime<<";";
		}
		Global::logger->debug("[open_xy_box] open a xy box <accid=%u,item=%s>", p_client->GetAccountID(), os.str().c_str());
	}	
}


bool	MODI_RandomBoxMgr::OpenYangWBox(MODI_ClientAvatar * p_client)
{
	bool is_notify = false;
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_Bag * p_bag = &(p_client->GetPlayerBag());
	if(! p_bag)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_GS2C_Notify_BoxItemList 	boxlist;

	BYTE num = GetRandItemNum();
	if(num == 0)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	BYTE	i=0;
	boxlist.m_bNum = num;
	for( i=0; i<num ; ++i)
	{
		BagItemInfos  item;
		if(num == 1)
		{
			GetRandItem(item, 1, p_client->GetSex(), is_notify);
		}
		else if((num == 2) && (i == 0))
		{
			GetRandItem(item, 2,p_client->GetSex(), is_notify);
		}
		else if((num == 2) && (i == 1))
		{
			GetRandItem(item, 3,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 0))
		{
			GetRandItem(item, 4,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 1))
		{
			GetRandItem(item, 5,p_client->GetSex(), is_notify);
		}
		else if((num == 3) && (i == 2))
		{
			GetRandItem(item, 6,p_client->GetSex(), is_notify);
		}
		
		if(item.m_wConfigId == 0)
		{
			MODI_ASSERT(0);
			return false;
		}
		
		boxlist.m_sInfo[i] = item;
	}

	if( p_bag->GetEmptyPosCount() < 5)
	{
		Global::logger->info("[good_package] client not enough add item <name=%s>", p_client->GetRoleName());
		return false;
	}
	
	bool	ok=false;
	for( i =0; i< num; ++i)
	{
		MODI_CreateItemInfo  create_info;
		create_info.m_dwConfigId = boxlist.m_sInfo[i].m_wConfigId;
		create_info.m_byLimit = boxlist.m_sInfo[i].m_bTime;
		create_info.m_byServerId = pApp->GetServerID();
		create_info.m_enCreateReason = enAddReason_ItemYWBox;
		BYTE	count=0;
		for( count =0; count < boxlist.m_sInfo[i].m_bNum; ++count)
		{
			QWORD	new_item_id=0;
			if( !p_bag->AddItemToBag(create_info,new_item_id))
			{
				MODI_ASSERT(0);
				break;
			}	
			ok=true;
			
			if(boxlist.m_sInfo[i].m_wConfigId >= 51003 && boxlist.m_sInfo[i].m_wConfigId <= 51012)
			{
				MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
			}
			

			if( boxlist.m_sInfo[i].m_wConfigId == 56003 || boxlist.m_sInfo[i].m_wConfigId == 56004)
			{
				if( p_client->IsVip())
				{
						
					MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
				}
			}

			MODI_TriggerBuyItem   script(boxlist.m_sInfo[i].m_wConfigId);
			MODI_BuyItemManager::GetInstance().Execute(p_client,script);
		}
	}

	if(ok)
	{
		p_client->SendPackage( &boxlist,sizeof(MODI_GS2C_Notify_BoxItemList));
		std::ostringstream os;
		if(is_notify)
		{
			os.str("");
			os<< "Selamat " << p_client->GetRoleName() << " Kamu beruntung, menemukan sebuah treasure box";
			/// 广播稀有类
			for(BYTE i=0; i<boxlist.m_bNum; i++)
			{
				const GameTable::MODI_Avatar * pAvater = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile().Get(boxlist.m_sInfo[i].m_wConfigId);
				const GameTable::MODI_Itemlist * pElement = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get(boxlist.m_sInfo[i].m_wConfigId);
				if(pElement)
				{
					os<< " "<<pElement->get_Name() << "(" << (WORD)boxlist.m_sInfo[i].m_bNum <<")"; 
				}
				if(pAvater)
				{
					os<< " " << pAvater->get_Name() << "(" ;
					if(boxlist.m_sInfo[i].m_bTime == 1)
					{
						os<<"7Hari)";
					}
					else if(boxlist.m_sInfo[i].m_bTime == 2)
					{
						os<<"30Hari)";
					}
					else if(boxlist.m_sInfo[i].m_bTime == 3)
					{
						os<<"Permanen)";
					}
				}
			}
			
			char buf[Skt::MAX_USERDATASIZE];
			memset(buf, 0, sizeof(buf));
			MODI_S2ZS_Broadcast_SystemChat  * p_send_cmd = (MODI_S2ZS_Broadcast_SystemChat *)buf;
			AutoConstruct(p_send_cmd);
			p_send_cmd->m_wdSize = os.str().size();
			memcpy(p_send_cmd->m_pContent, os.str().c_str(), p_send_cmd->m_wdSize);
		
			MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
			if( pZone)
			{
				pZone->SendCmd(p_send_cmd, sizeof(MODI_S2ZS_Broadcast_SystemChat) + p_send_cmd->m_wdSize);
			}
		}

		/// 打印日志
		os.str("");
		for(BYTE i=0; i<boxlist.m_bNum; i++)
		{
			os<<boxlist.m_sInfo[i].m_wConfigId<<","<<(WORD)boxlist.m_sInfo[i].m_bNum<<","<<(WORD)boxlist.m_sInfo[i].m_bTime<<";";
		}
		Global::logger->debug("[open_yw_box] open a yw box <accid=%u,item=%s>", p_client->GetAccountID(), os.str().c_str());
	}
	return ok;
}







