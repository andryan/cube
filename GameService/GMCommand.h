/** 
 * @file GMCommand.h
 * @brief 游戏GM命令
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-28
 */

#ifndef GM_MONMAND_H_
#define GM_MONMAND_H_


#include "IGMCommandHandler.h"
#include "SingleObject.h"


class MODI_ClientAvatar;

class MODI_GMCmdHandler_gs : public MODI_IGMCommandHandler , public CSingleObject<MODI_GMCmdHandler_gs> 
{
	public:

	MODI_GMCmdHandler_gs();
	virtual ~MODI_GMCmdHandler_gs();

	private:

    virtual bool FillCmdHandlerTable();

	MODI_ClientAvatar * 	m_pClient;
	public:


	void 	SetClient( MODI_ClientAvatar * pClient );

	MODI_ClientAvatar * 	GetClient();
	/** 
	 * @brief 显示GM命令帮助
	 * 
	 * @param nArgc   	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int ShowHelp( int nArgc ,const char ** szArgs );

	/** 
	 * @brief 显示服务器信息
	 * 
	 * @param nArgc 	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int ShowChannelInfo( int nArgc ,const char ** szArgs);

	/** 
	 * @brief 显示服务器信息
	 * 
	 * @param nArgc 	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * @return 	返回 enGMCmdHandler_SendToZone 
	 */
	static int ShowZoneInfo( int nArgc ,const char ** szArgs);

	/** 
	 * @brief 显示某个角色的信息
	 * 
	 * @param nArgc 	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * format: -clientinfo PLAYER_NAME 
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int ShowClientInfo( int nArgc ,const char ** szArgs );

	/** 
	 * @brief 给某个对象增加某种物品
	 * 
	 * @param nArgc 	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * format: -additem PLAYER_NAME , ITEM_CONFIGID , ITEM_COUNT <default 1> , ITEM_REMAINTIME <default forever>
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int AddItem( int nArgc ,const char ** szArgs );


	/** 
	 * @brief 将某个客户端身上的某个物品删除
	 * 
	 * @param nArgc 参数个数
	 * @param szArgs 内容
	 * 
	 * format: -delitem PLAYER_NAME ,  ITEM_POS_INBAG , BAG_TYPE(1=avatar_bag,2=player_bag <default 2>) ,
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int DelItem( int nArgc ,const char ** szArgs );


	/** 
	 * @brief 使用某个物品
	 * 
	 * @param nArgc  参数个数
	 * @param szArgs  内容
	 * 
	 * format : -useitem ITEM_POS_IN_PLAYERBAG  PLAYER_NAME(default : self)
	 * @return 	
	 */
	static int UseItem( int nArgc ,const char ** szArgs );

	/** 
	 * @brief 获得某个房间的信息
	 * 
	 * @param nArgc 
	 * @param szArgs
	 * 
	 * @return 	
	 */
	static int RoomInfo( int nArgc ,const char ** szArgs );

	/** 
	 * @brief 获得某个房间的信息
	 * 
	 * @param nArgc 
	 * @param szArgs
	 * 
	 * @return 	
	 */
	static int SetChannelClientNum( int nArgc ,const char ** szArgs );

	static int SetConstantValue( int nArgc , const char ** szArgs );

	/** 
	 * @brief 设置角色属性
	 * 
	 * 
	 * @return 
	 */
	static int SetAttr(int nArgc, const char ** szArgs);
	/** 
	 * @brief  增加粉丝数量
	 * 
	 * 
	 * @return 
	 */
	static int AddFensi(int nArgc, const char **szArgs);
};



#endif
