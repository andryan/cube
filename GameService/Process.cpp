/**
 * @file   Process.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 13:53:07 2011
 * 
 * @brief  脚本执行体
 * 
 * 
 */

#include "Process.h"
#include "ClientAvatar.h"

#include "Process.h"
#include "ClientAvatar.h"

/** 
 * @brief 执行体解析
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Process::Init()
{
	return true;
}


/** 
 * @brief 执行,中途错误也执行 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Process::Execute(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}
	
	if(IsCanExecute(p_client))
	{
		defActionVecIter iter = m_stActionVec.begin();
		for(; iter != m_stActionVec.end(); iter++)
		{
			if(*iter)
			{
				(*iter)->do_it(p_client);
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
		return true;
	}
	return false;
}


/** 
 * @brief 是否可以执行
 * 
 */
bool MODI_Process::IsCanExecute(MODI_ClientAvatar * p_client)
{
	if(! p_client)
	{
		return false;
	}
	
	bool ret_code = true;
	defConditionVecIter iter = m_stConditionVec.begin();
	for(; iter != m_stConditionVec.end(); iter++)
	{
		if(*iter)
		{
			if(! (*iter)->is_valid(p_client))
			{
				ret_code = false;
				break;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}

	return ret_code;
}

/** 
 * @brief 释放资源
 * 
 */
void MODI_Process::Final()
{
	defConditionVecIter c_iter = m_stConditionVec.begin();
	for(; c_iter != m_stConditionVec.end(); c_iter++)
	{
		if(*c_iter)
		{
			delete (*c_iter);
			(*c_iter) = NULL;
		}
	}

	defActionVecIter a_iter = m_stActionVec.begin();
	for(; a_iter != m_stActionVec.end(); a_iter++)
	{
		if(*a_iter)
		{
			delete (*a_iter);
			(*a_iter) = NULL;
		}
	}
}


