/** 
 * @file ZoneClient.h
 * @brief zone server 的客户端连接
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-17
 */
#ifndef ZONECLIENT_GAMESERVER_H_
#define ZONECLIENT_GAMESERVER_H_

#include "ClientTask.h"
#include "SingleObject.h"
#include "CommandQueue.h"
#include "RecurisveMutex.h"

class MODI_ZoneServerClient : public MODI_ClientTask  , 
	public CSingleObject<MODI_ZoneServerClient> ,
	public MODI_CmdParse
{
public:

	MODI_ZoneServerClient(const char * name, const char * server_ip,const WORD & port);

	~MODI_ZoneServerClient();

	/// 发送命令
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	virtual bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen); 

	/// 初始化
	virtual	bool Init();

	void ProcessPackages();

 protected:

	virtual void Final();

};


#endif
