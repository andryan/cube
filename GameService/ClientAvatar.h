/**
 * @file ClientAvatar.h 
 * @date 2009-12-28 CST 
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 客户端对象
 *
 */

#ifndef CLIENT_AVATAR_H_
#define CLIENT_AVATAR_H_

#include "GameUserVar.h"
#include "MoveableObj.h"
#include "ClientSession.h"
#include "protocol/gamedefine.h"
#include "protocol/gamedefine2.h"
#include "Timer.h"
#include "MusicHeaderData.h"
#include "Bag.h"
#include "ImpactList.h"
#include "Base/BitArray.h"
#include "protocol/c2gs_family.h"

#define	 PREVILEGE_CREATEROOM	0x01	//创建房间的权限
#define  PREVILEGE_ROOMOWNER	0x02	//作为房主的权限

#define	 PREVILEGE_DEFAULT	0xffffffff	//默认情况下的权利	

#define	 AVATAR_CAN( avatar, pri) \
	(  (avatar->m_dwPrevilege & pri))
#define	DISABLE_PRE( avatar, pri) \
	{\
		avatar->m_dwPrevilege &= ~pri;\
	}

#define GRANT_PRE( avatar, pri) \
	{\
		avatar->m_dwPrevilege |= pri;\
	}


class MODI_ImpactList;
class MODI_GameChannel;
class MODI_MultiGameRoom;
class MODI_GameShop;

#define SYNC_PLAYERLIST_TIMER  30 * 1000
#define CHANGE_SONGER_TIMER 5 * 1000
const BYTE SAVE_GROUP_NUM = 20;

/**
 * @brief 客户端控制的角色对象
 *
 */
class MODI_ClientAvatar: public MODI_MoveableObject 
{

	friend class MODI_GameLobby;

public:

    MODI_ClientAvatar(MODI_GUID id, MODI_ClientSession * p);
    virtual ~MODI_ClientAvatar();

	virtual void OnEnterChannel( );

	virtual void OnLeaveChannel(int iLeaveReason);

	/** 
	 * @brief 进入房间后的回调
	 * 
	 * @param room 房间对象
	 */
	virtual void OnEnterRoom( MODI_GameRoom & room , defRoomPosSolt nPos );

	/** 
	 * @brief  离开房间后的回调
	 * 
	 * @param room 房间对象
	 */
	virtual void OnLeaveRoom( MODI_GameRoom & room );

	/** 
	 * @brief 开始游戏时的回调
	 */
	virtual void OnStartGame();

	virtual void OnEndGame();

	virtual void OnEnterShop( MODI_GameShop & Shop );

	virtual void OnLeaveShop( MODI_GameShop & Shop );

	bool IsPlayExpression() const;

	void OnPlayExpression( WORD nPackage , BYTE byIndex );

	void OnStopExpression();

	void SyncPlayExpression( MODI_ClientAvatar * pClient );

	/** 
	 * @brief 当听歌对象改变的时候调用
	 * 
	 * @param room 房间
	 * @param pNewSonger 新的听歌对象
	 */
	virtual void OnChangeSinger( MODI_MultiGameRoom * room , 
			MODI_ClientAvatar * pOldSonger ,
			MODI_ClientAvatar * pNewSonger );


public:

	/** 
	 * @brief 从数据库载入对象的数据
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool LoadFromDB();

	void	SetChengHao( WORD m_wdChenghao)
	{
		m_fullData.m_roleInfo.m_normalInfo.m_wdChenghao = m_wdChenghao;
	}

	WORD	GetChengHao( )
	{
		return m_fullData.m_roleInfo.m_normalInfo.m_wdChenghao ;
	}

	/** 
	 * @brief 保存对象数据到数据库
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool SaveToDB(); 

	/** 
	 * @brief 从数据中删除该对象的数据
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool DeleteFromDB();

	/** 
	 * @brief 从数据库载入对象数据的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnLoadFromDB( const void * pData ); 

	/** 
	 * @brief 对象数据保存到数据库后的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnSaveToDB( const void * pData ); 

	/** 
	 * @brief 对象数据从数据库删除后的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnDeleteFromDB( const void * pData ); 

	/** 
	 * @brief 游戏角色对象的更新
	 * 
	 * @param nTime
	 */
    virtual void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);

	void 	SyncDirtyAttr();

	void 	ForceSyncAttr() { m_bForceSyncAttr = true; }

	MODI_CHARID 	GetCharID() const;

	virtual bool IsClientAvatar() { return true; }


	void 	SetEffectRelease(bool bSet);
	bool 	GetEffectRelease() const;

	void 	SetEffectTime(float f);
	float 	GetEffectTime() const;

	void 	SetEffectState( int iSet );
	void 	ClearEffectState( int iClear );

	bool 	IsInShowTime() const;
	bool 	IsInFerverTime() const;
	bool 	IsNoEffect() const;	

	enInviteHandlerMode 	GetInviteHandlerMode() const
	{
		return m_TempData.m_AboutGamePlay.m_enInviteMode;
	}

	void 		SetInviteHandlerMode( enInviteHandlerMode e )
	{
		m_TempData.m_AboutGamePlay.m_enInviteMode = e;
	}

	void 	SetInviteRoomID( defRoomID n )
	{
		m_TempData.m_AboutGamePlay.m_InviteRoomID = n;
	}

	defRoomID 	GetInviteRoomID() const
	{
		return 	m_TempData.m_AboutGamePlay.m_InviteRoomID; 
	}

	const MODI_CharactarFullData & GetFullData() const
	{
		return m_fullData;
	}

	void SetGMLevel(const BYTE gm_level)
	{
		m_fullData.m_roleExtraData.m_iGMLevel = gm_level;
	}
	

	/** 
	 * @brief 是否拒绝某个玩家的邀请
	 * 
	 * @param pSrcClient 邀请发起的玩家
	 * 
	 * @return 	拒绝返回true
	 */
	bool 		IsRejectInvitePlay( const MODI_ClientAvatar * pSrcClient );

	void 	SaveToDBAndToZone();

	defAccountID 	GetAccountID() const { return m_accid; }
	void 			SetAccountID( defAccountID accid ) { m_accid = accid; }

	/** 
	 * @brief 返回accname
	 * 
	 */
	const char * GetAccName();


	/** 
	 * @brief 根据avatar背包，更新人物形象
	 */
	void 	UpdateAvatarByAvatarBag();

	void 	SetClientAvatarData( const MODI_ClientAvatarData & set , bool bUpdate );
	const  	MODI_ClientAvatarData & GetClientAvatarData() const;

	void 	SetSingleGame( bool b ) { m_TempData.m_AboutGamePlay.m_bIsSingleGame = b; }
	bool 	IsInSingleGame() const { return m_TempData.m_AboutGamePlay.m_bIsSingleGame; }

	void 	FirstEnterGame();

public:
	/// 权限控制
	DWORD	 m_dwPrevilege;

    /*
     *  发送消息
     */
    bool SendPackage(const void * pt_null_cmd, int cmd_size, bool is_zip = false)
    {
        return m_pSession->SendPackage(pt_null_cmd, cmd_size, is_zip);
    }

	bool 	SendSystemChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type = enSystemNotice_Default);

	void SendOrBroadcast( void * p , size_t nSize );

	void SendMdmToClient( defMusicID musicid );

    ///    获得所在房间ID
    defRoomID GetRoomID()
    {
        return GetRoleBaseInfo().m_roomID;
    }

    ///    是否在多人房间中
    bool IsInMulitRoom();

	MODI_MultiGameRoom * GetMulitRoom();

    ///    是否房主
    bool IsRoomMaster();

	///    是否有音乐头
	bool IsHasMusicHeaderData() const;

    ///    获得角色名字
    const char * GetRoleName() const
    {
        return this->GetRoleBaseInfo().m_cstrRoleName;
    }

	const char * GetRoleLinkName() 
	{
		std::string name = GetRoleName();
		std::string link_name = "{@" + name + "}";
		return link_name.c_str();
	}

	/// 获得切换歌曲的定时器
	MODI_Timer & GetChangeSongerTimer()
	{
		return m_timerChangeSonger;
	}

	MODI_Timer & GetRareShopTimer()
	{
		return m_timerRareShop;
	}

    /// 临时方案
    MODI_ClientSession * GetSession()
	{
		return m_pSession;
	}

	/// 临时方案
	const MODI_SessionID & GetSessionID()
	{
		return 	GetSession()->GetSessionID();
	}

	///    Duplicate packet flag
	QWORD GetFlag() const
	{
		return m_nFlag;
	}

	void SetFlag(QWORD flag)
	{
		m_nFlag = flag;
	}

	///  是否参战玩家
	bool IsPlayer() const;

	///  获得位置号
	defRoomPosSolt GetPostionInRoom() const;

	/** 
	 * @brief 保存音乐文件头信息
	 * 
	 * @param pData 数据流
	 * @param nSize 数据大小
	 */
	void SetMusicDataHeader(const char * pData , unsigned int nSize );

	///  是否连击中
	bool IsComboing() const { return m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount >= 2; }

	/// 当前GOOD以上的次数
	unsigned int GetGoodPlusCount() const { return m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount; }

	/// 设置GOOD以上的次数
	void SetGoodPlusCount( unsigned int n ) { m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount = n; }

	void ResetMaxGoodPlusCount() { m_TempData.m_AboutGamePlay.m_nMaxGoodPlusCount = 0; }

	/// 更新最大连击数
	void UpdateMaxGoodPlusCount();

	unsigned int GetMaxGoodPlusCount() const { return m_TempData.m_AboutGamePlay.m_nMaxGoodPlusCount; }

	/// 获得唱对的秒数
	float GetRightSec() const { return m_TempData.m_AboutGamePlay.m_fRightSec; }

	/// 增加唱对的秒数
	void  AddRightSec( float fAdd ) { m_TempData.m_AboutGamePlay.m_fRightSec += fAdd; }

	/// 设置唱对的秒数
	void  SetRightSec( float fSet ) { m_TempData.m_AboutGamePlay.m_fRightSec = fSet; }

	void  SetRoomSlot( defRoomPosSolt n )
	{
		m_fullData.m_roleInfo.m_normalInfo.m_roomSolt = n;
	}

	defRoomPosSolt GetRoomSlot() const
	{
		return m_fullData.m_roleInfo.m_normalInfo.m_roomSolt;
	}

	void  SetSingerGUID( const MODI_GUID & guid )
	{
		m_fullData.m_roleInfo.m_normalInfo.m_SingerID = guid;
	}

	const MODI_GUID & GetSingerGUID() const
	{
		return m_fullData.m_roleInfo.m_normalInfo.m_SingerID;
	}

	BYTE GetLevel() const
	{
		return GetRoleBaseInfo().m_ucLevel;
	}

	BYTE GetSex() const
	{
		return GetRoleBaseInfo().m_ucSex;
	}

	void SetExp( DWORD nSet );

	void AdjustExp( long lValue );
	void AddRenqi( DWORD nSet );
	DWORD GetExp()
	{
		return m_fullData.m_roleInfo.m_normalInfo.m_nExp;
	}

	float 	GetExpModifyFactor() const { return m_fExpFactor; }
	void 	SetExpModifyFactor( float f ) { m_fExpFactor = f;  }

	void	SetChenMiPro(float f)
	{
		m_fChenmiPro = f;
	}

	float GetChenmiPro() const 
	{
		return m_fChenmiPro;
	}

	float 	GetMoneyFactor() const { return m_fMoneyFactor; }
	void 	SetMoneyFactor( float fFactor ) { m_fMoneyFactor = fFactor; }


	int  	TryLevelUp( bool bSync = true );

	DWORD 	GetQQNum() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nQQ;
	}
	
	BYTE 	GetAge() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_byAge;
	}

	DWORD 	GetCity() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nCity;
	}

	DWORD 	GetClan() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nClan;
	}

	DWORD 	GetGroup() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nCity; 
	}

	BYTE 	GetBlood() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_byBlood;
	}

	DWORD 	GetBirthDay() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nBirthDay;
	}

	DWORD 	GetVoteCount() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nVoteCount;
	}

	void 	SetQQNum( DWORD n );
	void 	SetAge( BYTE n );
	void 	SetCity( DWORD n );
	void 	SetClan( DWORD n );
	void 	SetGroup( DWORD n );
	void 	SetBlood( BYTE n );
	void 	SetBirthDay( DWORD n );
	void 	SetVoteCount( DWORD n );
	void 	SetPersonalSign( const char * szSign , size_t nLen );

	DWORD 	GetMoney() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nMoney;
	}

	void 	IncMoney( DWORD n );
	void 	DecMoney( DWORD n );

	DWORD 	GetRMBMoney() const
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_nRMBMoney;
	}

	void 	IncLogicCount();
	DWORD 	GetLogicCount() const;

	void 	IncRMBMoney( DWORD n );
	void 	DecRMBMoney( DWORD n );
	void 	SetRMBMoney( DWORD n );
	void 	SetMoney( DWORD n );

	void 	IncConsumeRMB( DWORD n );

	void 	IncVoteCount( DWORD n );
	void 	IncWin();
	DWORD 	GetWin() const
	{
		return GetRoleDetailInfo().m_nWin;
	}

	// 键盘模式的数据记录
	void 	IncKeyCount();

	float 	GetHighestKeyPrecision() const;
	void 	SetHighestKeyPrecision(float pre);
	WORD 	GetHighestKeyPrecisionMusicid() const;
	void 	SetHighestKeyPrecisionMusicid( WORD music );

	DWORD 	GetHighestKeySocre() const;
	void 	SetHighestKeyScore( DWORD score );
	WORD 	GetHighestKeyScoreMusicid() const;
	void 	SetHighestKeyScoreMusicid( WORD music );

	void 	IncLoss();
	DWORD 	GetLoss() const
	{
		return GetRoleDetailInfo().m_nLoss;
	}

	void 	IncTie();
	DWORD 	GetTie() const
	{
		return GetRoleDetailInfo().m_nTie;
	}

	void 	IncPefect( DWORD n );
	DWORD 	GetPefect() const
	{
		return GetRoleDetailInfo().m_nPefect;
	}

	void 	IncCool( DWORD n );
	DWORD 	GetCool() const
	{
		return GetRoleDetailInfo().m_nCool;
	}

	void 	IncGood( DWORD n );
	DWORD 	GetGood() const
	{
		return GetRoleDetailInfo().m_nGood;
	}

	void 	IncBad( DWORD n );
	DWORD 	GetBad() const
	{
		return GetRoleDetailInfo().m_nBad;
	}

	void 	IncMiss( DWORD n );
	DWORD 	GetMiss() const
	{
		return GetRoleDetailInfo().m_nMiss;
	}

	void 	IncCombo( DWORD n );
	DWORD 	GetCombo() const
	{
		return GetRoleDetailInfo().m_nCombo;
	}

	void 	IncExact( float f );
	float 	GetExact() const
	{
		return GetRoleDetailInfo().m_fExact;
	}

	const char * 	GetPersonalSign() const 
	{
		return m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign;
	}
	
	void 	IncChangCount( DWORD n );
	void 	IncHengCount( DWORD n );
	void 	IncKeyboardCount( DWORD n );
	void 	IncToupiaoCount( DWORD n );

	DWORD 	GetHighestSocre() const;
	void 	SetHighestScore( DWORD score );
	WORD 	GetHighestScoreMusicid() const;
	void 	SetHighestScoreMusicid( WORD music );

	float 	GetHighestPrecision() const;
	void 	SetHighestPrecision(float pre);
	WORD 	GetHighestPrecisionMusicid() const;
	void 	SetHighestPrecisionMusicid( WORD music );

	void 	SetGameState( kGameState ns );
	kGameState GetGameState() const;

	void 	SetObjStateFlag( enObjStateMask mask );
	void 	ClearObjStateFlag( enObjStateMask mask );
	bool 	IsObjStateFlag( enObjStateMask mask ) const;

	/** 
	 * @brief 将自己的音乐头发送给某个客户端
	 * 
	 * @param pClient 要发送的客户端
	 */
	void SendMusicHeader(MODI_ClientAvatar * pClient );

	MODI_Bag 	& 	GetAvatarBag()
	{
		return	m_bagAvatar;
	}

	MODI_Bag 	& 	GetPlayerBag()
	{
		return m_bagPlayer;
	}

	//	void SendAddItemPackage( BYTE nPos , const MODI_GameItem  * item );

	///	获得客户端角色属性，只读版本
    const MODI_RoleInfo & GetRoleInfo()
    {
        return m_fullData.m_roleInfo;
    }

    /// 获得客户端角色的基本属性，只读版本
    const MODI_RoleInfo_Base & GetRoleBaseInfo() const
    {
        return m_fullData.m_roleInfo.m_baseInfo;
    }

    /// 获得客户端角色的普通属性，只读版本
    const MODI_RoleInfo_Normal & GetRoleNormalInfo() const
    {
        return m_fullData.m_roleInfo.m_normalInfo;
    }

	/// 获得客户端角色详细属性，只读版本
    const MODI_RoleInfo_Detail & GetRoleDetailInfo() const
    {
        return m_fullData.m_roleInfo.m_detailInfo;
    }

	MODI_ImpactList & GetImpactlist() 
	{
		return m_impactList;
	}

	DWORD 	GetItemCountBySubClass( MODI_ItemType type );

	bool 	IsReadedHelp() const;
	void 	SetReadHelp();

	bool 	IsInShop() const { return IsInRareShop() || IsInNormalShop(); }
	bool 	IsInRareShop() const { return m_inShopType == kRareShop; }
	bool 	IsInNormalShop() const { return m_inShopType == kNormalShop; }
	void 	SetShopType( enShopType sp ) { m_inShopType = sp; } 
	MODI_GameShop * GetShop();

	void 	ResetTodayOnlineDay(DWORD nSet);


	bool IsCreateFamilyTimeout();
	void SetCreateFamilyTime();
	void ClearCreateFamilyTime();
	bool IsRequestFamilyTimeout();
	void SetRequestFamilyTime();
	void ClearRequestFamilyTime();

		
	/// 变量管理
	MODI_GameUserVarMgr m_stGameUserVarMgr;
	
	/// 家族信息
	MODI_FamilyMemBaseInfo m_stCFamilyInfo;

	/** 
	 * @brief 是否有家族
	 * 
	 */
	bool IsHaveFamily();

	/** 
	 * @brief 是不是族长
	 * 
	 */
	bool IsFamilyMaster();

	/** 
	 * @brief 是不是福族长
	 * 
	 */
	bool IsFamilySlaver();

	/** 
	 * @brief 是不是申请成员
	 * 
	 */
	bool IsFamilyRequest();


	/** 
	 * @brief 是不是普通成员
	 * 
	 */
	bool IsFamilyNormal();

	/** 
	 * @brief 获取家族名字
	 * 
	 */
	const char * GetFamilyName();


	/** 
	 * @brief 获取家族id
	 * 
	 */
	const DWORD GetFamilyID();

	/** 
	 * @brief 是否要发送家族名片
	 * 
	 */
	void SetSendFCard(BYTE state);
	const BYTE GetSendFCard();

	/** 
	 * @brief 进入家族界面
	 * 
	 */
	void LoginFamily()
	{
		
#ifdef _FAMILY_DEBUG		
		Global::logger->debug("[login_family] <%s>", GetRoleName());
#endif
		
		m_blIsInFamily = true;
	}
	
	/** 
	 * @brief 退出家族界面
	 * 
	 */
	void LogoutFamily()
	{
#ifdef _FAMILY_DEBUG		
		Global::logger->debug("[logout_family] <%s>", GetRoleName());
#endif
		m_blIsInFamily = false;
	}

	/** 
	 * @brief 获得按键的速度
	 * 
	 */
	float  	GetKeySped()
	{
		return m_fSped;
	}
	void	SetSped(float sped)
	{
		m_fSped = sped;
	}
	/** 
	 * @brief 获得按键的声音模式
	 * 
	 */
	enKeyDownVolType GetKeyVolType()
	{
		return m_eVolType;
	}	
	void		SetKeyVolType(enKeyDownVolType type)
	{
		m_eVolType = type;
	}

	/** 
	 * @brief 是否在家族界面里面
	 * 
	 */
	bool IsInFamily()
	{
		return m_blIsInFamily;
	}

	/** 
	 * @brief 返回家族结果
	 * 
	 * @param result 返回的结果
	 *
	 */
	void RetFamilyResult(enFamilyOptResult result)
	{
		MODI_GS2C_Return_FamilyOpt_Result send_cmd;
		send_cmd.m_enResult = result;
		SendPackage(&send_cmd, sizeof(send_cmd));
	}
	

	void	RefreshBagInfoToClient();

	void	OnEnterZone();

	bool	IsLogoutNormal(){ return m_bLogoutNormal;};
	void	SetLogoutNormal( bool is) { m_bLogoutNormal=is;};



	/** 
	 * @brief 加载道具
	 * 
	 * @param p_item_info 道具信息
	 * @param item_size 道具个数
	 *
	 */
	void LoadItem(const MODI_DBItemInfo * p_item_info , const WORD & item_size, bool is_sync = false);

	/** 
	 * @brief 通知道具列表，登陆和进入商场时用
	 * 
	 */
	void NotifyItemList();

	/**
	 * @brief 获得经验处罚值
	 *
	 */
	BYTE	GetExppenalty()
	{
		return m_bExppenalty;
	}
	void	SetExppenalty( WORD  num)
	{
		BYTE	_tmp= num &0xff;
		if( _tmp <= 100)
		{
			m_bExppenalty = _tmp;
		}
	}

	/**
	 * @brief 获得金币的处罚
	 *
	 */
	BYTE	GetMoneypenalty()
	{
		return m_bMoneypenalty;
	}
	void	SetMoneypenalty( WORD num)
	{
		BYTE	_tmp= num &0xff;
		if( _tmp <= 100)
		{
			m_bMoneypenalty= _tmp;
		}
	}
public:
	void	SetKeyPrecise( float  pre)
	{
		m_fullData.m_roleInfo.m_detailInfo.key_highest_precision= pre;
	}
	void	SetKeyScore( DWORD	score)
	{
		m_fullData.m_roleInfo.m_detailInfo.key_highest_socre = score;
	}
	void	SetPreciseKeyMusic( WORD id)
	{
		m_fullData.m_roleInfo.m_detailInfo.key_highest_precision_musicid = id;
	}

	void	 SetScoreKeyMusic( WORD id)
	{
		m_fullData.m_roleInfo.m_detailInfo.key_highest_score_musicid = id;
	}

private:
	bool m_blIsInFamily;
	
	bool m_blSendFamilyToMe;

	BYTE  m_bySendFamilyData;

	bool	m_bLogoutNormal;

	///	经验处罚
	BYTE	m_bExppenalty;	

	///	金币处罚
	BYTE	m_bMoneypenalty;

    ///	获得客户端角色属性，可修改版本
    MODI_RoleInfo & GetRoleInfo_Modfiy()
    {
        return m_fullData.m_roleInfo;
    }

    /// 获得客户端角色的基本属性，可修改版本
    MODI_RoleInfo_Base & GetRoleBaseInfo_Modfiy()
    {
        return m_fullData.m_roleInfo.m_baseInfo;
    }

    /// 获得客户端角色的普通属性，可修改版本
    MODI_RoleInfo_Normal & GetRoleNormalInfo_Modfiy()
    {
        return m_fullData.m_roleInfo.m_normalInfo;

    }
public:
    bool   IsVip()
    {
	    return  (m_fullData.m_roleInfo.m_baseInfo.m_bVip !=0 );
    }

    void	SetVip( BYTE	n)
    {
	    m_fullData.m_roleInfo.m_baseInfo.m_bVip = n;
    }

    MODI_RoleInfo_Detail & GetRoleDetailInfo_Modfiy() 
    {
        return m_fullData.m_roleInfo.m_detailInfo;
    }


	void 	UpdateFangChenmi();
	
private:

	void 	testcode_AddItem( BYTE bySex );
public:
	MODI_ClientAvatar  * GetTarget()
	{
		return m_pTarget;
	}

	void	SetTarget( MODI_ClientAvatar * p_client)
	{
		m_pTarget = p_client;
	}

	void	ResetTarget()
	{
		m_pTarget  = NULL;
	}

	/// 活动的某些临时数据
	const DWORD  GetPayMoney()
	{
		return m_dwPayMoney;
	}

	void SetPayMoney(const DWORD pay_money)
	{
		m_dwPayMoney = pay_money;
	}

private:
	

	///	键盘模式的速度
	float 		m_fSped;

	///	键盘模式的声音模式
	enKeyDownVolType	m_eVolType;

	/// game session
    MODI_ClientSession * m_pSession;

	/// 角色信息
    MODI_CharactarFullData m_fullData;

	// 角色临时数据
	MODI_CharacterTempData 	m_TempData;

	/// 客户端的音乐头
	MODI_MusicHeaderData 	m_MusicHeader;

	/// 切换歌曲的频率
	MODI_Timer 		m_timerChangeSonger;

	/// 装备包裹
	MODI_Bag m_bagAvatar;

	/// 玩家包裹
	MODI_Bag m_bagPlayer;

	MODI_ImpactList  	m_impactList;

	/// 数据库定时保存
	MODI_Timer 			m_timerDBSave;

	defAccountID 		m_accid;

	// 经验缩放比率
	float 				m_fExpFactor;	

	// 游戏币
	float 				m_fMoneyFactor;

	/// 沉迷系数
	float m_fChenmiPro;

	MODI_ObjUpdateMask 	m_UpdateMask;

	MODI_Flag 			m_stateFlag;

	MODI_Timer 			m_timerSyncDirtyAttr;

	DWORD 				m_LogicCount;

	MODI_Timer 			m_timerPlayExpression;

	WORD 				m_nExpressPackageID;

	BYTE 				m_byExpressIndex;

	MODI_Timer 			m_timerBagUpdate;

	MODI_Timer 			m_timerRareShop;

	MODI_Timer 			m_timerUPOnlineTime;

	MODI_Timer			m_timerPerSecond;

	enShopType 			m_inShopType;

	bool 				m_bForceSyncAttr;

	MODI_BitArray 		normal_flags;

	MODI_ClientAvatar   	*m_pTarget;
	
	/// 所有的数据加载完成
	bool m_blAllDataLoad;

	/// 活动的某些临时数据
	DWORD m_dwPayMoney;

	/// Duplicate packet flag
	QWORD m_nFlag;
};

typedef std::map<MODI_GUID, MODI_ClientAvatar *> MAP_CLIENTAVATARS;
typedef MAP_CLIENTAVATARS::iterator MAP_CLIENTAVATARS_ITER;
typedef MAP_CLIENTAVATARS::const_iterator MAP_CLIENTAVATARS_C_ITER;
typedef std::pair<MAP_CLIENTAVATARS_ITER, bool> MAP_CLIENTAVATARS_INSERT_RESULT;


#endif
