/**
 * @file   ItemOperator.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jun 16 10:15:38 2011
 * 
 * @brief  物品操作类
 * 
 * 
 */


#ifndef MD_ITEMOPERATOR_H_
#define MD_ITEMOPERATOR_H_

#include "protocol/c2gs_shop.h"
#include "protocol/c2gs_itemcmd.h"
#include "Global.h"

class MODI_Bag;
class MODI_BagPosInfo;
class MODI_GameItemInfo;
class MODI_ClientAvatar;



/**
 * @brief 物品操作类
 * 
 */
class MODI_ItemOperator
{
 public:

	/**
	 * @brief 操作结果
	 * 
	 */
	enum enItemOperatorResult
	{
		enOK = 0,
		enInvalidParam,
		enBagPosNotEnough,
		enBagFull,
		enItemNotExist,
		enTargetNotExist,
		enLevelFaild,
		enSexFaild,
		enGameStateFaild,
		enTargetFaild,
		enConfigError,
		/// 子类物品上限
		enSubClassItemMax, 
		enUnknowError,
	};

		
	/** 
	 * @brief 包裹是否够
	 * 
	 * @param p_bag 此包裹
	 * @param config_id 配置id
	 * @param item_count 道具数量
	 * 
	 * @return 够true
	 *
	 */
	static bool CheckBagSpace(MODI_Bag * p_bag, const DWORD & config_id, const WORD & item_count);


	/** 
	 * @brief 是否可以增加道具
	 * 
	 * @param p_client 该玩家
	 * @param config_id 配置id
	 * @param item_count 道具数量
	 * 
	 * @return 可以true
	 *
	 */
	static bool CheckCanAddToClient(MODI_ClientAvatar * p_client, const DWORD & config_id, const WORD & item_count);

	/**
	 * @brief	是否可以增加一个物品，如果不能的话，那么就直接使用它
	 *
	 */
	static	bool	ShouldAddToBag( MODI_ClientAvatar *p_client,const DWORD	& config_id);

	/** 
	 * @brief 是否等级符合
	 * 
	 */
	static enUseItemResult CheckItemLevelReq( MODI_ClientAvatar * pCaster ,WORD nConfigID );


	/** 
	 * @brief 性别是否符合
	 * 
	 */
	static enUseItemResult CheckItemSexReq(  MODI_ClientAvatar * pCaster , WORD nConfigID );
	 

	/** 
	 * @brief 游戏状态是否正确
	 * 
	 */
	static enUseItemResult CheckItemGameStateReq(  MODI_ClientAvatar * pCaster , WORD nConfigID );


	/** 
	 * @brief 目标是否正确
	 * 
	 */
	static  enUseItemResult CheckItemTargetReq(MODI_ClientAvatar * pCaster, 	const MODI_GUID & target, WORD nConfigID, MODI_ClientAvatar ** pTargetClient);



	/** 
	 * @brief 是否能够拥有此道具
	 * 
	 * @param p_bag 包裹
	 * @param config_id 物品配置id
	 * @param item_count 物品数量
	 * 
	 * @return 能够拥有
	 *
	 */
	static  enItemOperatorResult IsCanHaveItem(MODI_Bag * p_bag, const DWORD & config_id, const WORD & item_count);

		
	/** 
	 * @brief 使用道具
	 * 
	 * @param p_src_client 源玩家
	 * @param item_id 道具id
	 * @param target_id 目标玩家
	 * 
	 * @return 使用状态
	 *
	 */
	static int UseItem(MODI_ClientAvatar * p_src_client, const QWORD & item_id,  const MODI_GUID & target_id);


	/** 
	 * @brief 拥有道具效果
	 * 
	 * @param p_client 拥有者
	 * @param p_item 道具信息
	 * 
	 */
	static bool HaveItemImpact(MODI_ClientAvatar * p_client, MODI_GameItemInfo * p_item);


	/** 
	 * @brief Avatar的时效计算
	 * 
	 * @param pElement Avatar
	 * @param stt 时效
	 * @param dwTime 时效时间
	 * 
	 * @return 成功true
	 *
	 */
	static bool GetAvatarLimit( const DWORD & config_id, enShopGoodTimeType stt , DWORD & dwTime);
};

#endif
