#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "GameTick.h"
#include "s2ms_cmd.h"
#include <map>
#include "GamePackt.h"
#include "ScriptManager.h"
#include "GameSingleMusicMgr.h"

BYTE MODI_GameChannel::group_num = 0;


MODI_GameChannel::MODI_GameChannel():m_timerPlayerlist( 30 * 1000 ), m_stSaveTime(30 * 1000)
{
    m_nClients = 0;
    m_pLobby = 0;
    m_nMaxClient = 0;
}

MODI_GameChannel::~MODI_GameChannel()
{
    Destory();
}

bool MODI_GameChannel::Create(const std::string & strChannelName, unsigned int nMaxClient)
{
    m_pLobby = new MODI_GameLobby();
    if (!m_pLobby)
        return false;

    if (!m_pLobby->Create())
    {
        return false;
    }

    SetName(strChannelName);
    m_nMaxClient = nMaxClient;
    return true;
}

///	销毁频道
void MODI_GameChannel::Destory()
{
    if (m_pLobby)
    {
        delete m_pLobby;
        m_pLobby = 0;
    }
}

bool MODI_GameChannel::EnterChannel(MODI_ClientAvatar * p)
{
	if( !m_pLobby )
	{
		MODI_ASSERT(0);
		return false;
	}
	
    bool bRet = false;
	bRet = m_mapClients.Add( p->GetGUID() , p->GetRoleName() , p->GetAccountID() , p );
    if ( bRet )
    {
        //    进入频道后，默认进入大厅
        bRet = m_pLobby->EnterLobby(p);
		if( !bRet )
		{
			MODI_ASSERT(0);
		}
		else 
		{
			m_nClients += 1;
			p->OnEnterChannel();

		}
		///	当玩家进入频道后，发送给他一些礼包的信息
		MODI_DynamicPackt::GetInstance()->OnPlayerLogin(p);
		///   进入频道后，执行进入频道脚本
		MODI_TriggerEnter   script(10);
		MODI_EnterScriptManager::GetInstance().Execute( p, script);
		MODI_GameMusicMgr::GetInstance().SyncToClient(p,enGameMode_Key);
		MODI_GameMusicMgr::GetInstance().SyncToClient(p,enGameMode_Sing);
    }
	else 
	{
		Global::logger->fatal("[add_client_failed] %u add to m_mapClient failed", p->GetAccountID());
#ifdef _DEBUG		
		MODI_ClientAvatar * pdebug = (MODI_ClientAvatar *)m_mapClients.FindByGUID( p->GetGUID() );
		MODI_ASSERT( pdebug );
		MODI_ClientAvatar * pdebug2 = (MODI_ClientAvatar *)m_mapClients.FindByName( pdebug->GetRoleName() );
		MODI_ASSERT( pdebug2 );
		MODI_ClientAvatar * pdebug3 = (MODI_ClientAvatar *)m_mapClients.FindByAccid( pdebug->GetAccountID() );
		MODI_ASSERT( pdebug3 );
		MODI_ASSERT( pdebug == pdebug2 && pdebug2 == pdebug3 );

		Global::logger->debug("[%s] debug-> name=%s,guid=%s,accid=%u,session<%s> status=%s" ,SVR_TEST ,
				pdebug->GetRoleName() ,
				pdebug->GetGUID().ToString(),
				pdebug->GetAccountID() ,
				pdebug->GetSessionID().ToString(),
				pdebug->GetSession()->GetStatusStringFormat() );
#endif
	}

    return bRet;
}

void MODI_GameChannel::LeaveChannel(MODI_ClientAvatar * p , int iReason)
{
	MODI_ClientAvatar * pFind = FindClientByID( p->GetGUID() );
	if( !pFind )
		return ;

	if( pFind != p )
	{
		MODI_ASSERT(0);
		return ;
	}

	bool bRet = m_mapClients.Del( p->GetGUID() , p->GetRoleName() , p->GetAccountID() );
	if( !bRet )
	{
		MODI_ASSERT(0);
		return ;
	}

    // 查看玩家是否在房间中
    defRoomID roomID = p->GetRoomID();
    if (roomID != INVAILD_ROOM_ID && roomID != SINGLE_ROOM_ID)
    {
        // 从房间中离开
        MODI_MultiGameRoom * room = m_pLobby->GetMultiRoom(roomID);
        if( room )
        {
        	room->LeaveRoom(p, iReason);
        }
		else
		{
			Global::logger->fatal("[add_hrx] leave room when room isnot exit");
		}
    }

    m_pLobby->LeaveLobby(p->GetGUID());

	p->OnLeaveChannel( iReason );

	MODI_ASSERT( m_nClients > 0 );
	if( m_nClients > 0 )
		m_nClients -= 1;
}

void MODI_GameChannel::SyncPlayerlist( const char * szPackageBuf , int iSendSize , unsigned long nTime)
{
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		//pTemp->Update();

		if( pTemp->IsInSingleGame()  )
		{
			pTemp->SendPackage( szPackageBuf , iSendSize , true);
		}
		else
		{
			MODI_MultiGameRoom * room = m_pLobby->GetMultiRoom( pTemp->GetRoomID() );
			if( room )
			{
				if( room->IsStart() == false )
				{
					pTemp->SendPackage( szPackageBuf , iSendSize , true);
				}
			}
			else 
			{
				pTemp->SendPackage( szPackageBuf , iSendSize , true);
			}
		}
		itor = inext;
	}
}

void MODI_GameChannel::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();

    if (m_pLobby)
    {
        m_pLobby->Update(cur_rtime, cur_ttime);
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		int iSendSize = 0;

		bool bSyncPl = false;
		if( m_timerPlayerlist( pTick->GetTimer() ) )
		{
			iSendSize = m_pLobby->CreateSyncPlaylistPackage( szPackageBuf , Skt::MAX_USERDATASIZE );
			bSyncPl = true;
		}

		if( bSyncPl == true )
		{
			SyncPlayerlist( szPackageBuf , iSendSize, 1);
		}
		//		else 
		{
			const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
			MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
			while (itor != map.end())
			{
				MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
				inext++;
				MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
				if( pTemp )
					pTemp->Update(cur_rtime, cur_ttime);
				itor = inext;
			}
		}

		if( m_stSaveTime( pTick->GetTimer() ) )
		{
			const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
			MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
			while (itor != map.end())
			{
				MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
				inext++;
				MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
				if( pTemp )
				{
					DWORD m_dwAccountID = pTemp->GetAccountID();
					if((m_dwAccountID % SAVE_GROUP_NUM) == group_num)
					{
						pTemp->SaveToDBAndToZone();
#ifdef _DEBUG						
						Global::logger->debug("[save_to_db] <group=%u,accid=%u>", group_num, m_dwAccountID);
#endif						
					}
				}
				itor = inext;
			}
			
			group_num++;
			if(group_num == SAVE_GROUP_NUM)
			{
				group_num = 0;
			}
		}
    }
}


/** 
 * @brief 全体广播
 * 
 * @param pt_null_cmd 广播命令
 * @param cmd_size 命令大小
 * 
 */
bool MODI_GameChannel::Broadcast(const Cmd::stNullCmd * pt_null_cmd, int cmd_size)
{
#if 0	
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			pTemp->SendPackage( pt_null_cmd , cmd_size );
		}
		itor = inext;
	}
#endif

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			(*p_write_id) = pTemp->GetSession()->GetSessionID();
			p_write_id++;
			p_send_cmd->m_wdSessionSize++;
		}
		itor = inext;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
}

bool	MODI_GameChannel::Broadcast_mask( const Cmd::stNullCmd * pt_null_cmd , int cmd_size, BYTE mask , BYTE div)
{

	Global::logger->debug("[broadcast_music_list] receive music list from the server begin!!!");


	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			if( pTemp->GetAccountID() % div != mask )
			{
				itor = inext;
				continue;
			}
			(*p_write_id) = pTemp->GetSession()->GetSessionID();
			p_write_id++;
			p_send_cmd->m_wdSessionSize++;
		}
		itor = inext;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	Global::logger->debug("[broadcast_music_list] receive music list from the server end!!!");
	return true;

}	


bool MODI_GameChannel::BroadcastInFamily(const Cmd::stNullCmd * pt_null_cmd, int cmd_size)
{
#if 0	
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			if(pTemp->IsInFamily()) 
			{
				pTemp->SendPackage( pt_null_cmd , cmd_size );
			}
		}
		itor = inext;
	}
#endif
	
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			if(pTemp->IsInFamily())
			{
				(*p_write_id) = pTemp->GetSession()->GetSessionID();
				p_write_id++;
				p_send_cmd->m_wdSessionSize++;
			}
		}
		itor = inext;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
}


bool MODI_GameChannel::Broadcast_Except(const void * pt_null_cmd, int cmd_size, MODI_ClientAvatar * pExcept)
{
#if 0	
	const MODI_NKeyMap::TMAPS_KEY1 & maps = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = maps.begin();
	while (itor != maps.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if (pTemp && pTemp != pExcept)
		{
			pTemp->SendPackage(pt_null_cmd, cmd_size);
		}

		itor = inext;
	}

	return true;
#endif

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			if(pTemp != pExcept)
			{
				(*p_write_id) = pTemp->GetSession()->GetSessionID();
				p_write_id++;
				p_send_cmd->m_wdSessionSize++;
			}
		}
		itor = inext;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
}


/** 
 * @brief 除商场外的频道广播
 * 
 */
bool MODI_GameChannel::BroadcastNoInShop(const Cmd::stNullCmd * pt_null_cmd, int cmd_size)
{
#if 0	
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp ->IsInShop())
		{
			continue;
		}
		
		pTemp->SendPackage( pt_null_cmd , cmd_size );
		itor = inext;
	}

	return true;
#endif

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			if(! (pTemp ->IsInShop()))
			{
				(*p_write_id) = pTemp->GetSession()->GetSessionID();
				p_write_id++;
				p_send_cmd->m_wdSessionSize++;
			}
		}
		itor = inext;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
	
}



void MODI_GameChannel::AllClientExcuteCallback( MODI_ChannelClientCallBack & callback )
{
   	const MODI_NKeyMap::TMAPS_KEY1 & maps = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = maps.begin();
	while (itor != maps.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if (pTemp)
		{
			callback.Done(pTemp);
		}
		itor = inext;
	}
}
	
MODI_ClientAvatar * MODI_GameChannel::FindClientByID( const MODI_GUID & guid )
{
	if( guid.IsInvalid() )
		return 0;

	MODI_ClientAvatar * pRet = (MODI_ClientAvatar *)m_mapClients.FindByGUID( guid );
	return pRet;
}

MODI_ClientAvatar * MODI_GameChannel::FindClientByName( const char * szName )
{
	if( !szName || !szName[0] )
		return 0;

	MODI_ClientAvatar * pRet = (MODI_ClientAvatar *)m_mapClients.FindByName( szName );
	return pRet;
}

MODI_ClientAvatar * MODI_GameChannel::FindByAccid(const defAccountID & accid )
{
	if( accid == INVAILD_ACCOUNT_ID )
		return NULL;
	MODI_ClientAvatar * pRet = (MODI_ClientAvatar *)m_mapClients.FindByAccid( accid );
	return pRet;
}



/** 
 * @brief 把所有的人的家族名片信息组包
 * 
 * @param szBuf 开始地址
 * @param nBufSize 大小
 * 
 */
int MODI_GameChannel::CreateCFCardPackage( char * szBuf , int nBufSize )
{
    memset( szBuf, 0, nBufSize );
	MODI_GS2C_Notify_FamilyCard * p_send_cmd = (MODI_GS2C_Notify_FamilyCard *)szBuf;
	AutoConstruct(p_send_cmd);
	p_send_cmd->m_wdSize = 0;
	MODI_FamilyCard * p_family_card = &(p_send_cmd->m_stCard[0]);

	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if(! pTemp )
		{
			continue;
		}

		p_family_card->m_stGuid = MAKE_GUID(pTemp->GetCharID() , 0 );
		p_family_card->m_stPosition = (enFamilyPositionType)(pTemp->m_stCFamilyInfo.m_enPosition);
		p_family_card->m_wLevel = pTemp->m_stCFamilyInfo.m_wLevel;
		strncpy(p_family_card->m_cstrFamilyName,pTemp->GetFamilyName(), sizeof(p_family_card->m_cstrFamilyName) - 1);
		p_send_cmd->m_wdSize++;
#ifdef _FAMILY_DEBUG
		Global::logger->debug("[send a family card to client] faimly card <name=%s,position=%d,familyname=%s,serial=%u>",
							  pTemp->GetRoleName(),
							  (BYTE)p_family_card->m_stPosition, p_family_card->m_cstrFamilyName, p_send_cmd->m_wdSize);
#endif
		p_family_card++;
		
		itor = inext;
	}
	
    int iSendSize = p_send_cmd->m_wdSize * sizeof(MODI_FamilyCard)
            + sizeof(MODI_GS2C_Notify_FamilyCard);
	
	return iSendSize;
}

bool	MODI_GameChannel::BroadcastSystemChat(const char * m_cstr,WORD size)
{

	const MODI_NKeyMap::TMAPS_KEY1 & map = m_mapClients.GetGUIDMapContent();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = map.begin();
	while (itor != map.end())
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_ClientAvatar * pTemp = (MODI_ClientAvatar *)(itor->second);
		if( pTemp )
		{
			pTemp->SendSystemChat(m_cstr,size);
		}
		itor = inext;
	}


	return true;
}

bool	MODI_GameChannel::BroadCast_Range(const Cmd::stNullCmd *pt_null_cmd, int cmd_size, MODI_CHARID * mem, WORD	n)
{

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;

	WORD i=0;
	for( i=0; i< n; ++i)
	{
		MODI_ClientAvatar * pava = FindClientByID(MAKE_GUID(mem[i],0));

		if( pava)
		{
			*p_write_id = pava->GetSession()->GetSessionID();
			p_write_id ++;
			p_send_cmd->m_wdSessionSize++;
		}		

	}


	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;


}



