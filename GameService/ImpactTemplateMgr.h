/** 
 * @file ImpactTemplateMgr.h
 * @brief 效果模板管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-06-29
 */

#ifndef IMPACT_TEMPLATE_MGR_H_
#define IMPACT_TEMPLATE_MGR_H_

#include "BaseOS.h"
#include "Base/SingleObject.h"
#include "ImpactTemplateID.h"

class 	MODI_ImpactTemplate;


class 	MODI_ImpactTemplateMgr : public CSingleObject<MODI_ImpactTemplateMgr>
{
	public:

	MODI_ImpactTemplateMgr();
	~MODI_ImpactTemplateMgr();

	public:

	void 		Initial();

	MODI_ImpactTemplate 	* 	GetImpactHandle( WORD nTemplateID ) const;
	const char * GetImpactName( WORD nTemplateID ) const;

	void 		CleanUp();

	private:

	void 		RegisterImpactTemplate(WORD nID , MODI_ImpactTemplate * pTemplate , const char * szTemplateName );

	private:

	MODI_ImpactTemplate * 	m_Template[kImpactTemplateCount];
	std::string 			m_TemplateName[kImpactTemplateCount];
};

#endif
