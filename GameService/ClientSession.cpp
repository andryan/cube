#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "Share.h"
#include "ClientAvatar.h"
#include "protocol/c2gs.h"
#include "s2adb_cmd.h"
#include "IPackageHandler.h"
#include "PackageHandler_c2gs.h"
#include "ZoneClient.h"
#include "GameChannel.h"

#define WAITCLOSE_TIMEOUT 30 * 1000
#define WAITAUTH_TIMEOUT 30 * 1000

MODI_ClientSession::MODI_ClientSession(const MODI_SessionID & id, MODI_ServiceTask * p):
	m_timerWaitClose( WAITCLOSE_TIMEOUT , Global::m_stLogicRTime ),
	m_timerAuth( WAITAUTH_TIMEOUT  , Global::m_stLogicRTime )
{
    m_SessionID = id;
    m_pClient = 0;
    m_pNetIO = p;
	m_iStatus = enInit;
	m_bFangChenmi = false;

    if ( m_SessionID.IsInvaild() )
    {
        throw "Invaild Session id. <MODI_ClientSession::MODI_ClientSession>";
    }
}

MODI_ClientSession::~MODI_ClientSession()
{
	if( m_pClient )
	{
		delete m_pClient;
	}

	MODI_ClientSessionMgr::GetInstancePtr()->Del( this->GetSessionID() );
}

bool MODI_ClientSession::IsVaildSessionID()
{
    if ( m_SessionID.IsInvaild() )
		return false;
	return true;
}


bool MODI_ClientSession::SendPackageNoBuffer(const void * pt_null_cmd, int cmd_size, bool is_zip)
{
    bool bRet = false;

    if (MODI_GameTask::ms_pInstancePtr)
    {
        char szPackageBuf[Skt::MAX_USERDATASIZE];
        MODI_GS2GW_Redirectional * p_redirectionalPackage =
                (MODI_GS2GW_Redirectional *) (szPackageBuf);
        AutoConstruct( p_redirectionalPackage );

        p_redirectionalPackage->m_sessionID = this->GetSessionID();
        p_redirectionalPackage->m_nSize = cmd_size;
        memcpy(p_redirectionalPackage->m_pData, (char *) pt_null_cmd, cmd_size);
        int iSendSize = cmd_size + sizeof(MODI_GS2GW_Redirectional);

        bRet = MODI_GameTask::ms_pInstancePtr->SendCmdNoBuffer((const Cmd::stNullCmd *)p_redirectionalPackage, iSendSize, is_zip);
		
#ifdef _CMD_FLUX		
		MODI_GameTask::m_stSendCmdFlux.Put((Cmd::stNullCmd *)pt_null_cmd, cmd_size);
#endif		
		
    }
    return bRet;
}


bool MODI_ClientSession::SendPackage(const void * pt_null_cmd, int cmd_size, bool is_zip)
{
    bool bRet = false;

    if (MODI_GameTask::ms_pInstancePtr)
    {
        char szPackageBuf[Skt::MAX_USERDATASIZE];
        MODI_GS2GW_Redirectional * p_redirectionalPackage =
                (MODI_GS2GW_Redirectional *) (szPackageBuf);
        AutoConstruct( p_redirectionalPackage );

        p_redirectionalPackage->m_sessionID = this->GetSessionID();
        p_redirectionalPackage->m_nSize = cmd_size;
        memcpy(p_redirectionalPackage->m_pData, (char *) pt_null_cmd, cmd_size);
        int iSendSize = cmd_size + sizeof(MODI_GS2GW_Redirectional);

        bRet = MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_redirectionalPackage, iSendSize, is_zip);
		
#ifdef _CMD_FLUX		
		MODI_GameTask::m_stSendCmdFlux.Put((Cmd::stNullCmd *)pt_null_cmd, cmd_size);
#endif		
		
    }
    return bRet;
}


void 	MODI_ClientSession::SwitchWaitAuthStatus() 
{
	m_iStatus = enWaitAuth;
}

bool 	MODI_ClientSession::IsInWaitAuthStatus() const
{
	return m_iStatus == enWaitAuth;
}

void 	MODI_ClientSession::SwitchWaitAuthResultStatus()
{
	m_iStatus = enWaitAuthResult;
}

bool 	MODI_ClientSession::IsInWaitAuthResultStatus() const
{
	return m_iStatus == enWaitAuthResult;
}

void 	MODI_ClientSession::SwitchAuthedWaitMusicHeaderStatus()
{
	m_iStatus = enAuthedWaitMusicHeader;
}

bool 	MODI_ClientSession::IsInAuthedWaitMusicHeaderStatus() const
{
	return	m_iStatus == enAuthedWaitMusicHeader;
}

void 	MODI_ClientSession::SwitchLoginedStatus()
{
	m_iStatus = enLogined;
}

bool 	MODI_ClientSession::IsInLoginedStatus() const
{
	return 	m_iStatus == enLogined;
}

void 	MODI_ClientSession::SwitchRechangeChannelingStatus()
{
	m_iStatus = enRechangeChanneling;
}

bool 	MODI_ClientSession::IsInRechangeChannelingStatus() const
{
	return m_iStatus == enRechangeChanneling;
}

void 	MODI_ClientSession::SwitchChannelRechangedStatus()
{
	m_iStatus = enChannelRechanged;
}

void 	MODI_ClientSession::SwitchAntherLoginStatus()
{
	m_iStatus = enWaitAntherLogin;
	m_timerWaitClose.Reload( WAITCLOSE_TIMEOUT , Global::m_stLogicRTime );
}

void 	MODI_ClientSession::SwitchWaitBekickStatus() 
{
	m_iStatus = enWaitBekick;

	Global::logger->debug("[%s] session<%s> switch to waitbekick status." ,
			SVR_TEST ,
			this->GetSessionID().ToString() );
}

void 	MODI_ClientSession::SwitchWaitBekickSaveStatus() 
{
	m_iStatus = enWaitBekickSave;
}

bool 	MODI_ClientSession::IsInWaitBekickStatus() const
{
	return 	m_iStatus == enWaitBekick;
}

bool 	MODI_ClientSession::IsInWaitBekickSaveStatus() const
{
	return m_iStatus == enWaitBekickSave;
}

bool 	MODI_ClientSession::IsInChannelRechangedStatus() const
{
	return m_iStatus == enChannelRechanged;
}

bool 	MODI_ClientSession::IsInAntherLoginStatus() const
{
	return m_iStatus == enWaitAntherLogin;
}

bool 	MODI_ClientSession::IsWaitCloseTimeout()
{
	if( m_timerWaitClose( Global::m_stLogicRTime ) )
		return true;
	return false;
}

bool 	MODI_ClientSession::IsAuthTimeOut()
{
	if( m_timerAuth( Global::m_stLogicRTime ) )
		return true;
	return false;
}

const char * MODI_ClientSession::GetStatusStringFormat()
{
	static const std::string strStatus[] = {
		"Init", 			// 初始化状态
		"WaitAuth", 		// 等待客户端请求验证
		"WaitAuthResult", 	// 等待对客户端的验证结果 
		"AuthedWaitMusicHeader", 			// 已经验证通过 , 等待客户端发送音乐头
		"Logined", 							// 客户端已经发送音乐头,算作登入成功
		"RechangeChanneling", // 正在进行重新选择频道操作
		"ChannelRechanged",   // 已经重新选择了频道
		"WaitAntherLogin",
		"WaitBekick", // 等待被踢
		"WaitBekickSave"
	};

	return strStatus[m_iStatus].c_str();
}
		
int 	MODI_SessionStatusHandler::OnHandler_LoginAuth( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size )
{
	if( (pt_null_cmd->byCmd == MAINCMD_LOGIN &&
		pt_null_cmd->byParam == MODI_C2LS_Request_Auth::ms_SubCmd) == false )
		return enPHandler_Kick;

	// 检查状态
	if( !pSession->IsInWaitAuthStatus() )
	{
		Global::logger->warn("[%s] auth faild session<%s> ,  not in wait auth status." , 
				LOGIN_AUTH , pSession->GetSessionID().ToString() );
		return enPHandler_Kick;
	}

	// 检查包的大小
	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2LS_Request_Auth, 0 , 0 );
	if( nSafePackageSize != cmd_size )
	{
		Global::logger->warn("[%s] auth faild session<%s> , request package size is invaild." , 
				LOGIN_AUTH , pSession->GetSessionID().ToString() );
		return enPHandler_Kick;
	}	

	// 检查包的内容
	const MODI_C2LS_Request_Auth * pReq = (const MODI_C2LS_Request_Auth *)(pt_null_cmd);
	if( pReq->m_Passport.m_nAccountID == INVAILD_ACCOUNT_ID ||
			pReq->m_Passport.m_nCharID == INVAILD_CHARID  )
	{
		Global::logger->warn("[%s] auth faild <session = %s> , accountid<%u> or charid<%u> is invaild." , 
				LOGIN_AUTH ,
				pSession->GetSessionID().ToString() ,
				pReq->m_Passport.m_nAccountID , 
				pReq->m_Passport.m_nCharID );

		return enPHandler_Kick;
	}

	// 是否已经存在该帐号的玩家，存在不让进入
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pAlready = pChannel->FindByAccid( pReq->m_Passport.m_nAccountID );
	if( pAlready )
	{
		MODI_ASSERT(pAlready->GetAccountID() == pReq->m_Passport.m_nAccountID );

		Global::logger->warn("[%s] auth faild <session = %s> , is already exist accid<%u>'s session<%s> ,client<name=%s,charid=%u>" , 
			LOGIN_AUTH ,
			pSession->GetSessionID().ToString() ,
			pReq->m_Passport.m_nAccountID , 
			pAlready->GetSession()->GetSessionID().ToString() ,
			pAlready->GetRoleName(),
			pAlready->GetCharID() );
		return enPHandler_Kick;
	}

	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( pZoneServer )
	{
		// 请求ADB验证
		MODI_S2ADB_Request_LoginAuth 	msg;
		msg.m_nSessionID = pSession->GetSessionID();
		msg.m_Passport = pReq->m_Passport;
		msg.m_nServerID = GetChannelIDFromArgs();
		if( pZoneServer->SendCmd( &msg , sizeof(msg) ) )
		{
			pSession->SwitchWaitAuthResultStatus();

			Global::logger->info("[%s] send auth . SessionID<%s> , serverid<%u> , accountid<%u> , charid<%u> " , 
					LOGIN_AUTH ,
					msg.m_nSessionID.ToString() , 
					msg.m_nServerID , 
					msg.m_Passport.m_nAccountID , 
					msg.m_Passport.m_nCharID );

			return enPHandler_OK;
		}

		Global::logger->warn("[%s] auth faild <session = %s> . send to gameserver was faild." , 
				LOGIN_AUTH ,
				pSession->GetSessionID().ToString() );
	}

	return enPHandler_Kick;
}

int 	MODI_SessionStatusHandler::OnHandler_SelectChannel( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size )
{
	if( ( pt_null_cmd->byCmd == MAINCMD_LOGIN && 
		pt_null_cmd->byParam == MODI_C2LS_Request_SelChannel::ms_SubCmd ) == false )
	{
		MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
		Global::logger->warn("[%s] client<%s> is in rechange channel status. but send logic cmd." ,
				LOGIN_OPT  ,
				pClient->GetRoleName() );
		return enPHandler_Kick;
	}

	
	MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
	///	游戏命令的处理
	int iRet = MODI_PackageHandler_c2gs::GetInstancePtr()->DoHandlePackage(
			pt_null_cmd->byCmd, pt_null_cmd->byParam, pClient, pt_null_cmd,
			cmd_size);

	return iRet;
}

int 	MODI_SessionStatusHandler::OnHandler_MusicHeader( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
	if( !pClient  )
	{
		MODI_ASSERT(0);
		return enPHandler_Kick;
	}

	if( ( pt_null_cmd->byCmd == MAINCMD_MUSIC &&
		pt_null_cmd->byParam == MODI_C2GS_Request_MusicHeaderData::ms_SubCmd ) == false )
	{
		Global::logger->warn("[%s] client<%s> is in wait musicheader  status. but send other cmd.<cmd=%d,para=%d>" ,
				LOGIN_OPT  ,
							 pClient->GetRoleName(), pt_null_cmd->byCmd, pt_null_cmd->byParam );
		return enPHandler_Kick;
	}

	int iRet = MODI_PackageHandler_c2gs::GetInstancePtr()->DoHandlePackage(
			pt_null_cmd->byCmd, pt_null_cmd->byParam, pClient, pt_null_cmd,
			cmd_size);

	return iRet;
}
