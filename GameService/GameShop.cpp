/**
 * @file   GameShop.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jun 24 16:25:04 2011
 * 
 * @brief  购买函数
 * 
 * 
 */

#include "ScriptManager.h"
#include "GameShop.h"
#include "AssertEx.h"
#include "ItemOperator.h"
#include "ClientAvatar.h"
#include "ZoneClient.h"
#include "Base/s2zs_cmd.h"
#include "Base/s2rdb_cmd.h"
#include "Base/IAppFrame.h"
#include "GameChannel.h"
#include "protocol/c2gs_shop.h"
#include "ItemRuler.h"
#include "Base/TimeManager.h"
#include "GameServerAPP.h"
#include "Base/TransCommand.h"
#include "Base/HelpFuns.h"


/** 
 * @brief 购买价格计算
 * 
 */
DWORD MODI_GameShop::GetPrice_Money(const GameTable::MODI_Shop * pElement , enShopGoodTimeType st)
{
	if( !pElement )
		return 0;

	if( st == kShopGoodT_7Day )
	{
		return pElement->get_BS_1_Time2();
	}
	else if( st == kShopGoodT_30Day )
	{
		return pElement->get_BS_1_Time3();
	}
	else if( st == kShopGoodT_Forver )
	{
		return pElement->get_BS_1_Time4();
	}
	return 0;
}


/** 
 * @brief 购买价格计算
 *
 */
DWORD MODI_GameShop::GetPrice_RMBMoney(const GameTable::MODI_Shop * pElement , enShopGoodTimeType st)
{
	if( !pElement )
	{
		MODI_ASSERT(0);
		return 0;
	}

	if( st == kShopGoodT_7Day )
	{
		return pElement->get_BS_2_Time2();
	}
	else if( st == kShopGoodT_30Day )
	{
		return pElement->get_BS_2_Time3();
	}
	else if( st == kShopGoodT_Forver )
	{
		return pElement->get_BS_2_Time4();
	}
	return 0;
}


/** 
 * @brief 产生交易号
 * 
 * @param p_client 此用户交易的
 * 
 */
const QWORD MODI_GameShop::GetBuySerial(MODI_ClientAvatar * p_client)
{
	std::ostringstream buy_serial;
	BYTE server_id = 0;
	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)(MODI_GameServerAPP::GetInstancePtr());
	if(pApp)
	{
		server_id = pApp->GetServerID();
	}

	if(server_id > 9)
	{
		buy_serial << Global::m_stLogicRTime.GetSec() << (WORD)server_id << p_client->GetAccountID();
	}
	else
	{
		buy_serial << Global::m_stLogicRTime.GetSec() << "0" << (WORD)server_id << p_client->GetAccountID();
	}
	
	QWORD ret_id = strtoull(buy_serial.str().c_str(), NULL, 10);	
	return ret_id;
}


/** 
 * @brief 检查购买物品是否合法
 * 
 * @param p_goods_info 物品信息
 * @param buy_count 物品个数
 * @param shop_type 商场类型
 * 
 * @return 成功true
 *
 */
bool MODI_GameShop::CheckBuyGoods(const MODI_ShopGood * p_goods_info, const BYTE & buy_count,  enShopType shop_type)
{
	if(!p_goods_info || buy_count == 0)
	{
		Global::logger->error("[check_buy_goods] check buy goods failed");
		MODI_ASSERT(0);
		return false;
	}

	defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
	for(BYTE n=0; n< buy_count; n++)
	{
		const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
		if( !pElement )
		{
			Global::logger->error("[check_buy_goods] not find shopgoods <shopid=%u>", p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}

		/// 商场类型是否正确
		if((enShopType)pElement->get_WhereSell() != shop_type)
		{
			Global::logger->error("[check_buy_goods] shop type failed <type=%d,gettype=%d>", shop_type, pElement->get_WhereSell());
			MODI_ASSERT(0);
			return false;
		}
		
		if((p_goods_info[n].m_TimeStrategy >  kShopGoodT_Begin  && p_goods_info[n].m_TimeStrategy < kShopGoodT_End) == false)
		{
			Global::logger->error("[check_buy_goods] goos limit failed <limit=%u>", p_goods_info[n].m_TimeStrategy);
			MODI_ASSERT(0);
			return false;
		}
	}

	return true;
}


/** 
 * @brief 获取购买物品的钱
 *
 * @param p_client 购买人
 * @param p_goods_info 物品信息
 * @param buy_count 购买数量
 * @param buy_money_rmb 人民币
 * @param buy_money 金币
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_GameShop::CheckBuyMoney(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_goods_info, const BYTE & buy_count)
{
	if(!p_client || !p_goods_info || buy_count == 0)
	{
		Global::logger->error("[get_buy_money] goods info failed");
		MODI_ASSERT(0);
		return false;
	}

	DWORD buy_money_rmb = 0;
	DWORD buy_money = 0;

	defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
	for(BYTE n=0; n<buy_count; n++)
	{
		const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
		if( !pElement )
		{
			Global::logger->error("[get_buy_money] not get goods <goodsid=%u>", p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}

		enMoneyType bys = (enMoneyType)(pElement->get_BuyStrategy());
		/// 金币
		if(kGameMoney == bys)
		{
			DWORD get_money = GetPrice_Money(pElement , p_goods_info[n].m_TimeStrategy);
			if(get_money == 0)
			{
				Global::logger->error("[check_buy_money] not get price <goodid=%u>", p_goods_info[n].m_pConfigID);
				buy_money = 0;
				buy_money_rmb = 0;
				MODI_ASSERT(0);
				return false;
			}
			buy_money += get_money;
		}
		else if(kRMBMoney == bys)
		{
			DWORD get_money = GetPrice_RMBMoney(pElement , p_goods_info[n].m_TimeStrategy);
			if(get_money == 0)
			{
				Global::logger->error("[check_buy_money] not get price <goodid=%u>", p_goods_info[n].m_pConfigID);
				buy_money = 0;
				buy_money_rmb = 0;
				MODI_ASSERT(0);
				return false;
			}
			buy_money_rmb += get_money;
		}
		else 
		{
			Global::logger->error("[get_buy_money] not get money type <goodi=%u>", p_goods_info[n].m_pConfigID);
			buy_money = 0;
			buy_money_rmb = 0;
			MODI_ASSERT(0);
			return false;
		}
	}

	if(buy_money > p_client->GetMoney())
	{
		return false;
	}
	if(buy_money_rmb > p_client->GetRMBMoney())
	{
		return false;
	}

	return true;
}

/** 
 * @brief 购买性别判断
 * 
 * @param p_client 购买人
 * @param p_goods_info 购买物品
 * @param buy_item 数量
 * 
 * @return 成功true
 *
 */
bool MODI_GameShop::CheckBuySex(const BYTE client_sex, const MODI_ShopGood * p_goods_info, const BYTE & buy_count)
{
	defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
	for(BYTE n=0; n<buy_count; n++)
	{
		const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
		if(! pElement)
		{
			Global::logger->error("[check_buy_sex] not find goods <shopid=%u>", p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}

		BYTE byReqSex = enSexAll;
		if(!MODI_ItemRuler::GetReqSex(pElement->get_SellItemID(), byReqSex))
		{
			Global::logger->error("[check_buy_sex] not get sex in this goods <shopid=%u>", p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}

		if(byReqSex != enSexAll)
		{
			if(byReqSex != client_sex)
			{
				return false;
			}
		}
	}
	return true;
}


/** 
 * @brief 包裹空间检查
 * 
 * @param p_client 客户端
 * @param p_goods_info 物品信息
 * @param buy_item 物品个数
 * 
 * @return 成功ture
 *
 */
bool MODI_GameShop::CheckBuySpace(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_goods_info, const BYTE & buy_item)
{
	return true;
}


/** 
 * @brief 购买
 * 
 * @param p_client 购买人
 * @param p_goods_info 购买信息
 * @param buy_count 购买数量
 * 
 * @return 成功true
 *
 */
bool MODI_GameShop::OnBuyItem(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_in_goods_info, const BYTE & buy_count, enShopTransactionType buy_type,
							  QWORD buy_serial)
{
	MODI_ShopGood shopgood[buy_count];
	if(buy_type == kTransaction_Web)
	{
		const MODI_WebItemInfo  * p_startaddr = (const MODI_WebItemInfo *)p_in_goods_info;
		for(WORD i=0; i<buy_count; i++)
		{
			shopgood[i].m_pConfigID = p_startaddr->m_pConfigID;
			shopgood[i].m_TimeStrategy = p_startaddr->m_TimeStrategy;
			p_startaddr++;
		}
	}
	else
	{
		const MODI_ShopGood  * p_startaddr = (const MODI_ShopGood *)p_in_goods_info;
		for(WORD i=0; i<buy_count; i++)
		{
			shopgood[i].m_pConfigID = p_startaddr->m_pConfigID;
			shopgood[i].m_TimeStrategy = p_startaddr->m_TimeStrategy;
			p_startaddr++;
		}
	}

	const MODI_ShopGood * p_goods_info = &shopgood[0];
	
	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_Bag * p_player_bag = &(p_client->GetPlayerBag());
	if(!p_player_bag)
	{
		MODI_ASSERT(0);
		return false;
	}

	/// 获取交易号
	QWORD buy_serial_id = 0;
	if(buy_type != kTransaction_Web)
	{
		buy_serial_id = GetBuySerial(p_client);
		if(buy_serial_id == 0)
		{
			Global::logger->fatal("[get_buy_serial] get buy serial faild <name=%s>", p_client->GetRoleName());
			MODI_ASSERT(0);
			return false;
		}
	}

	defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
	defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();

	bool is_continue = false;
	/// SGP/INA购买东西,刚接收到命令,发送到bill去扣钱
	if((Global::g_byGameShop > 0 && buy_serial == 0) && buy_type != kTransaction_Web)
	{
		char buf[Skt::MAX_USERDATASIZE];
		memset(buf, 0, sizeof(buf));
		MODI_INAItemConsume * p_send_cmd = (MODI_INAItemConsume *)buf;
		AutoConstruct(p_send_cmd);
		MODI_INAItemPrice * p_startaddr = p_send_cmd->pData;
		p_send_cmd->m_wdSize = 0;
		p_send_cmd->m_byServerId = pApp->GetServerID();
		p_send_cmd->m_dwAccountId = p_client->GetAccountID();
		p_send_cmd->m_qdBuySerial = buy_serial_id;
		
		for(BYTE n=0; n<buy_count; n++)
		{
			const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
			if( !pElement )
			{
				Global::logger->fatal("[ina_buy_item] not find element <shopid=%u>", p_goods_info[n].m_pConfigID);
				MODI_ASSERT(0);
				return false;
			}

			BYTE item_limit = kShopGoodT_Forver;
			WORD item_count = 1;
			const GameTable::MODI_Itemlist * p_is_item =  itemBin.Get(pElement->get_SellItemID());
			if(p_is_item)
			{
				/// 道具都是永久的
				item_count = pElement->get_Count();
			}
			else
			{
				/// avatar 商场没有重叠
				item_limit = p_goods_info[n].m_TimeStrategy;
			}
			
			//// 是否能够拥有此商品
			for(WORD deal_count=0; deal_count<item_count; deal_count++)
			{
				if(MODI_ItemOperator::IsCanHaveItem(p_player_bag, pElement->get_SellItemID(), 1) != MODI_ItemOperator::enOK)
				{
					Global::logger->error("[assert_on_ina_buy_item] not can have item <name=%s,configid=%u>", p_client->GetRoleName(),
									  pElement->get_SellItemID());
					MODI_ASSERT(0);
					return false;
				}
			}

			enMoneyType bys = (enMoneyType)(pElement->get_BuyStrategy());
			DWORD get_price = 0;
			/// 金币
			if(kGameMoney == bys)
			{
				is_continue = true;
				continue;
			}
			else if(kRMBMoney == bys)
			{
				get_price = GetPrice_RMBMoney(pElement , p_goods_info[n].m_TimeStrategy);
				p_startaddr->m_dwPrice = get_price;
				std::ostringstream os;
				os<< p_goods_info[n].m_pConfigID << "_" << p_goods_info[n].m_TimeStrategy;
				//os<< p_goods_info[n].m_pConfigID;
				strncpy(p_startaddr->m_cstrItemId, os.str().c_str(), sizeof(p_startaddr->m_cstrItemId));
				p_startaddr->m_pGoods = p_goods_info[n];
				p_startaddr->m_enBuyType = buy_type;
				p_startaddr++;
				p_send_cmd->m_wdSize++;
			}
		}

		/// 发送tobill
		if(p_send_cmd->m_wdSize > 0)
		{
			MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
			if(! pZone )
			{
				Global::logger->info("[buy_ina_item] can't get zone service client");
				MODI_ASSERT(0);
				return false;
			}
			pZone->SendCmd(p_send_cmd, sizeof(MODI_INAItemConsume) + sizeof(MODI_INAItemPrice) * p_send_cmd->m_wdSize);
		}
	}

	/// SGP/INA购买道具全部是人民币，不要往下走了
	if((Global::g_byGameShop > 0 && is_continue == false && buy_serial == 0) && (buy_type != kTransaction_Web))
	{
		return false;
	}
	
	/// SGP/INA已经扣钱成功了
	if((Global::g_byGameShop > 0 && buy_serial != 0) && buy_type != kTransaction_Web)
	{
		buy_serial_id = buy_serial;
	}
	
	static DWORD buy_begin = 0;
	static DWORD buy_end = 0;
	buy_begin++;
	Global::logger->debug("[buy_item_begin] bug item begin <name=%s,begin_serial=%u,buy_serial=%llu>",
						  p_client->GetRoleName(),buy_begin, buy_serial_id);
	
	for(BYTE n=0; n<buy_count; n++)
	{
		if(buy_type == kTransaction_Web)
		{
			const MODI_WebItemInfo * p_web_goods_info = (const MODI_WebItemInfo *)p_in_goods_info;
			const MODI_WebItemInfo * p_info = &p_web_goods_info[n];
			buy_serial_id = p_info->m_qdBuySerial;
		}
		
		const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
		if( !pElement )
		{
			Global::logger->fatal("[buy_item] not find element <shopid=%u>", p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}

		//DWORD item_limit = enOneHour;
		BYTE item_limit = kShopGoodT_Forver;
		WORD item_count = 1;
		const GameTable::MODI_Itemlist * p_is_item =  itemBin.Get(pElement->get_SellItemID());
		if(p_is_item)
		{
			/// 道具都是永久的
			item_count = pElement->get_Count();
			// item_limit = enForever;
// 			if(p_is_item->get_ConfigID() == 51001)
// 			{
// 				item_limit = 3*enOneHour;
// 			}
		}
		else
		{
			/// avatar 商场没有重叠
			//GetAvatarLimit(pElement, p_goods_info[n].m_TimeStrategy, item_limit);
			item_limit = p_goods_info[n].m_TimeStrategy;
		}
			
		//// 是否能够拥有此商品
		for(WORD deal_count=0; deal_count<item_count; deal_count++)
		{
			if(MODI_ItemOperator::IsCanHaveItem(p_player_bag, pElement->get_SellItemID(), 1) != MODI_ItemOperator::enOK)
			{
				Global::logger->error("[assert_on_buy_item] not can have item <name=%s,configid=%u>", p_client->GetRoleName(),
									  pElement->get_SellItemID());
				MODI_ASSERT(0);
				return false;
			}
		}

		enMoneyType bys = (enMoneyType)(pElement->get_BuyStrategy());
		DWORD get_price = 0;
		/// 金币
		if(kGameMoney == bys)
		{
			get_price = GetPrice_Money(pElement , p_goods_info[n].m_TimeStrategy);
		}
		else if(kRMBMoney == bys)
		{
			get_price = GetPrice_RMBMoney(pElement , p_goods_info[n].m_TimeStrategy);
			/// 是INA且是第一就直接继续
			if((Global::g_byGameShop > 0 && buy_serial == 0) && buy_type != kTransaction_Web)
			{
				continue;
			}
		}
		
		if(get_price == 0)
		{
			Global::logger->error("[assert_on_buy_item] not get price <name=%s,goodid=%u>", p_client->GetRoleName(), p_goods_info[n].m_pConfigID);
			MODI_ASSERT(0);
			return false;
		}
		
		Global::logger->debug("[buy_item_second] bug item second <name=%s,buy_serial=%llu,get_price=%u,money_type=%d>",
							  p_client->GetRoleName(),buy_serial_id,get_price, bys);
		
// 		if(bys == kGameMoney)
// 		{
// 			p_client->DecMoney(get_price);
// 		}
// 		else if(bys == kRMBMoney)
// 		{
// 			p_client->DecRMBMoney(get_price);
// 		}

		/// 增加道具的理由
		MODI_CreateItemInfo create_info;
		create_info.m_dwConfigId = pElement->get_SellItemID();
		create_info.m_byLimit = item_limit;
		if(bys == kGameMoney)
		{
			create_info.m_enCreateReason = enAddReason_Money;
		}
		else if(bys == kRMBMoney)
		{
			create_info.m_enCreateReason = enAddReason_MoneyRmb;
		}
		
		create_info.m_byServerId = pApp->GetServerID();

		for(WORD deal_count=0; deal_count<item_count; deal_count++)
		{
			MODI_S2RDB_BuyItem_Cmd send_savetodb_cmd;
			if(buy_type == kTransaction_Web)
			{
				const MODI_WebItemInfo * p_web_goods_info = (const MODI_WebItemInfo *)p_in_goods_info;
				const MODI_WebItemInfo * p_info = &p_web_goods_info[n];
				if(p_info->m_dwMoney != get_price)
				{
					Global::logger->warn("[buy_web_item] money error <recordserial=%llu,buyserial=%llu, get_p=%u,money=%u>",
										 p_info->m_qdRecordSerial, p_info->m_qdBuySerial, get_price, p_info->m_dwMoney);
					MODI_ASSERT(0);
					continue;
				}
				send_savetodb_cmd.m_qdRecordSerial = p_info->m_qdRecordSerial;
				create_info.m_enCreateReason = enAddReason_Web;
			}
			send_savetodb_cmd.m_qdBuySerial = buy_serial_id;
			
			/// 给东西
			QWORD new_item_id = 0;
			MODI_ByteBuffer item_save_sql(1024);

			///  如果只能拥有一件这个物品的话，那么就把多余的使用掉
			
			if(! p_player_bag->AddItemToBag(create_info, new_item_id, item_save_sql))
			{
				Global::logger->debug("[assert_buy_item] add item faild  <name=%s,accid=%u,buy_serial=%llu,configid=%u>",
									  p_client->GetRoleName(), p_client->GetAccountID(),buy_serial_id, create_info.m_dwConfigId);
				MODI_ASSERT(0);
				return false;
			}

			memcpy(send_savetodb_cmd.m_strSaveItem, (const char *)item_save_sql.ReadBuf(), item_save_sql.ReadSize());
			send_savetodb_cmd.m_wdSaveItemSize = item_save_sql.ReadSize();
			
			/// 记录到交易库中
			MODI_Record record_insert;
			record_insert.Put("itemid", new_item_id);
			record_insert.Put("accid", p_client->GetAccountID());
			record_insert.Put("buy_serial",buy_serial_id);
			record_insert.Put("buy_action",create_info.m_enCreateReason);
			if(kGameMoney == bys)
			{
				record_insert.Put("money_rmb", 0);
				record_insert.Put("money", (DWORD)get_price/item_count);
			}
			else if(kRMBMoney == bys)
			{
				record_insert.Put("money_rmb", (DWORD)get_price/item_count);
				record_insert.Put("money", 0);
			}
	
			MODI_RecordContainer record_container;
			record_container.Put(&record_insert);
			MODI_ByteBuffer buy_records_sql(1024);
			if(! MODI_DBClient::MakeInsertSql(buy_records_sql, GlobalDB::m_pBuyrecordTbl, &record_container))
			{
				Global::logger->debug("[save_buy_record_to_db] make save buy_recod sql failed <itemid=%llu,accid=%u,buy_serial=%llu,buy_action=%d,price=%u>",
									  new_item_id, p_client->GetAccountID(), buy_serial_id, create_info.m_enCreateReason, get_price);
				MODI_ASSERT(0);
				return false;
			}
			memcpy(send_savetodb_cmd.m_strBuyRecord, (const char *)buy_records_sql.ReadBuf(), buy_records_sql.ReadSize());
			send_savetodb_cmd.m_wdRecordSize = buy_records_sql.ReadSize();

			send_savetodb_cmd.m_byMoneyType = (BYTE)bys;
			send_savetodb_cmd.m_dwMoney = (DWORD)get_price/item_count;
			send_savetodb_cmd.m_dwAccountID = p_client->GetAccountID();

			MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
			if(! pZone )
			{
				Global::logger->info("[save_buy_item] can't get zone service client <name=%s,itemid=%llu>", p_client->GetRoleName(), new_item_id);
				MODI_ASSERT(0);
				return false;
			}
				

			pZone->SendCmd(&send_savetodb_cmd, sizeof(send_savetodb_cmd));
	
			if( kGameMoney == bys )
			{
				Global::logger->info("[on_buy_successful] client buy good<rolename=%s,itemid=%llu,config=%u,name=%s,limit=%d,money=%u,moneyrmb=%u,type=%d>",
									 p_client->GetRoleName(),
									 new_item_id,
									 pElement->get_SellItemID(),
									 pElement->get_Name(),
									 p_goods_info[n].m_TimeStrategy,
									 get_price,0,0);
			}
			else if( kRMBMoney == bys )
			{
				Global::logger->info("[on_buy_successful] client buy good<rolename=%s,itemid=%llu,config=%u,name=%s,limit=%d,money=%u,moneyrmb=%u,type=%d>",
									 p_client->GetRoleName(),
									 new_item_id,
									 pElement->get_SellItemID(),
									 pElement->get_Name(),
									 p_goods_info[n].m_TimeStrategy,
									 0,
									 get_price,1);
			}

			if( pElement->get_SellItemID() == 56001 || pElement->get_SellItemID() == 56002)
			{
				Global::logger->debug("[buy_vip_item] buy vip item <accname=%s,isvip=%u,itemid=%llu>",
									  p_client->GetAccName(), p_client->IsVip(), new_item_id);
				if( p_client->IsVip())
				{
					Global::logger->debug("[buy_vip_item] use vip item <accname=%s,isvip=%u,itemid=%llu>",
									  p_client->GetAccName(), p_client->IsVip(), new_item_id);
					MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
				}
				else
				{
					Global::logger->debug("[buy_vip_item] first buy vip item <accname=%s,isvip=%u,itemid=%llu>",
									  p_client->GetAccName(), p_client->IsVip(), new_item_id);
				}
			}	
			/// 购买并使用
			else if(new_item_id > 0 && buy_type == kTransaction_BuyAndUse)
			{
				MODI_ItemOperator::UseItem(p_client, new_item_id, p_client->GetGUID());
			}
			MODI_TriggerBuyItem  script(pElement->get_SellItemID());
			MODI_BuyItemManager::GetInstance().Execute(p_client,script);
#ifdef _FB_LOGGER
			Global::fb_logger->debug("\t%u\tBUYS AN ITEM\t%u\t%s\t%s\t%d",Global::m_stRTime.GetSec(),new_item_id,pElement->get_Name(),p_client->GetAccName(),p_client->GetCharID());
#endif
			/// 此处暂时写使用道具
			// 保持36001或则36002的唯一性
		}
	}
	
	buy_end++;
	Global::logger->debug("[buy_item_end] bug item end <name=%s,end_serial=%u,buy_serial=%llu>",
							  p_client->GetRoleName(),buy_end, buy_serial_id);

	return true;
}


/** 
 * @brief 购买物品
 * 
 * @param p_client 购买人  
 * @param p_goods_info 购买的物品信息 
 * @param item_count 物品个数
 * 
 * @return 状态
 *
 */
enShopTransactionResult MODI_GameShop::BuyItem( MODI_ClientAvatar * p_client, const MODI_ShopGood * p_in_goods_info, BYTE buy_count, enShopTransactionType buy_type)
{
	MODI_ShopGood shopgood[buy_count];
	if(buy_type == kTransaction_Web)
	{
		const MODI_WebItemInfo  * p_startaddr = (const MODI_WebItemInfo *)p_in_goods_info;
		for(WORD i=0; i<buy_count; i++)
		{
			shopgood[i].m_pConfigID = p_startaddr->m_pConfigID;
			shopgood[i].m_TimeStrategy = p_startaddr->m_TimeStrategy;
			p_startaddr++;
		}
	}
	else
	{
		const MODI_ShopGood  * p_startaddr = (const MODI_ShopGood *)p_in_goods_info;
		for(WORD i=0; i<buy_count; i++)
		{
			shopgood[i].m_pConfigID = p_startaddr->m_pConfigID;
			shopgood[i].m_TimeStrategy = p_startaddr->m_TimeStrategy;
			p_startaddr++;
		}
	}

	const MODI_ShopGood * p_goods_info = &shopgood[0];
	
	if(p_client == NULL || p_goods_info == NULL || buy_count == 0)
	{
		MODI_ASSERT(0);
		return kTransaction_InvalidArgs;
	}
	
	/// 检查物品信息是否正确
	if(! CheckBuyGoods(p_goods_info, buy_count, GetShopType()))
	{
		Global::logger->error("[check_buy_goods] check faild <name=%s>", p_client->GetRoleName());
		MODI_ASSERT(0);
		return kTransaction_InvalidArgs;
	}

	/// 货币是否充值
	if((! CheckBuyMoney(p_client, p_goods_info, buy_count)) && (buy_type != kTransaction_Web))
	{
		Global::logger->warn("[check_buy_money] not enough money <name=%s>", p_client->GetRoleName());
		return kTransaction_NotEnoughRMBMoney;
	}

	/// 性别判断
	if(! CheckBuySex(p_client->GetSex(), p_goods_info, buy_count))
	{
		Global::logger->error("[check_buy_sex] check sex failed <name=%s,sex=%d>", p_client->GetRoleName(), p_client->GetSex());
		MODI_ASSERT(0);
		return kTransaction_InvalidSex;
	}

	/// 包裹空间检查
	if(! CheckBuySpace(p_client, p_goods_info, buy_count))
	{
		Global::logger->error("[check_buy_space] check space failed <name=%s>", p_client->GetRoleName());
		MODI_ASSERT(0);
		return kTransaction_BuyAndUseFaild_BagFull;
	}

	/// 开始交易
	if(! OnBuyItem(p_client, p_in_goods_info, buy_count, buy_type, 0))
	{
		//MODI_ASSERT(0);
		return kTransaction_UnknowError;
	}

	return kTransaction_Successful;
}


/** 
 * @brief 进入神秘商场
 * 
 */
void MODI_GameRareShop::OnEnter( MODI_ClientAvatar * pClient )
{
	if( !pClient )
		return ;

	pClient->SetShopType( kRareShop );
	pClient->OnEnterShop( *this );

	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_S2ZS_Request_SbEnterRare msg;
		safe_strncpy( msg.m_szName , pClient->GetRoleName() , sizeof(msg.m_szName) );
		pZone->SendCmd( &msg , sizeof(msg) );
	}
}



/** 
 * @brief 离开神秘
 * 
 */
void  MODI_GameRareShop::OnLeave( MODI_ClientAvatar * pClient )
{
	if( !pClient )
	{
		return ;
	}

	pClient->OnLeaveShop( *this );
	pClient->SetShopType( kNullShop );
}


/** 
 * @brief 进入普通商场
 * 
 */
void MODI_GameNormalShop::OnEnter(MODI_ClientAvatar * pClient)
{
	if(!pClient)
	{
		return;
	}

	pClient->SetShopType( kNormalShop );
	pClient->OnEnterShop( *this );
}


/** 
 * @brief 离开商场
 * 
 */
void MODI_GameNormalShop::OnLeave( MODI_ClientAvatar * pClient)
{
	if( !pClient )
	{
		MODI_ASSERT(0);
		return;
	}

	pClient->OnLeaveShop( *this );
	pClient->SetShopType( kNullShop );
}



/** 
 * @brief 赠送物品给别人
 * 
 * @param p_client 赠送人
 * @param p_recv_cmd 赠送参数
 * @param cmd_size 参数大小
 * 
 * @return 结果
 *
 */
enShopTransactionResult MODI_GameShop::SendGiveItemMail(MODI_ClientAvatar * p_client, MODI_C2GS_Request_Transaction * p_recv_cmd, const unsigned int cmd_size)
{
	
	if(p_client == NULL || p_recv_cmd == NULL || cmd_size == 0)
	{
		MODI_ASSERT(0);
		return kTransaction_InvalidArgs;
	}
	
	/// 检查物品信息是否正确
	if(! CheckBuyGoods(p_recv_cmd->m_pGoods, p_recv_cmd->m_nCount, GetShopType()))
	{
		Global::logger->error("[check_buy_goods] check faild <name=%s>", p_client->GetRoleName());
		MODI_ASSERT(0);
		return kTransaction_InvalidArgs;
	}

	/// 货币是否充值
	if(! CheckBuyMoney(p_client, p_recv_cmd->m_pGoods, p_recv_cmd->m_nCount))
	{
		Global::logger->warn("[check_buy_money] not enough money <name=%s>", p_client->GetRoleName());
		return kTransaction_NotEnoughRMBMoney;
	}

	MODI_QueryHasChar_Shop_ExtraData extradata;
    extradata.count = p_recv_cmd->m_nCount;
    memcpy( &extradata.goods , p_recv_cmd->m_pGoods , p_recv_cmd->m_nCount * sizeof(MODI_ShopGood));
    safe_strncpy( extradata.content , p_recv_cmd->m_pContent, sizeof(extradata.content));
    extradata.money = 0;
    extradata.RMBMoeny = 0;

    /// 询问zone目标对象是否存在
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
    if(!pZone )
	{
		MODI_ASSERT(0);
		return kTransaction_UnknowError;
	}

    char szPackageBuf[Skt::MAX_USERDATASIZE];
    memset(szPackageBuf, 0, sizeof(szPackageBuf));
    MODI_S2ZS_Request_QueryHasChar_Shop * pQuery = (MODI_S2ZS_Request_QueryHasChar_Shop *)(szPackageBuf);
    AutoConstruct( pQuery );
    MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
    pQuery->m_nServerID = pApp->GetServerID();
    pQuery->m_nSize = sizeof(extradata);
	memcpy( pQuery->m_pExtraData , (const char *)&extradata, sizeof(extradata));
    safe_strncpy(pQuery->m_szRoleName , p_recv_cmd->m_szTargetName, sizeof(pQuery->m_szRoleName));
    pQuery->m_type = kQueryChar_Shop_Give;
    pQuery->m_queryClient = p_client->GetCharID();
	
    int iSendSize = sizeof(MODI_S2ZS_Request_QueryHasChar_Shop) + sizeof(char) * pQuery->m_nSize;
    if(!pZone->SendCmd( pQuery , iSendSize ))
	{
        return kTransaction_UnknowError;
	}
	
    return kTransaction_Successful;
}


/** 
 * @brief 赠送物品给存在的角色
 * 
 * @param p_recv_cmd 赠送信息
 *
 */
void MODI_GameShop::GiveItemToOther(const MODI_ZS2S_Notify_QueryHasCharResult_Shop * p_recv_cmd, QWORD buy_serial)
{
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
    MODI_GUID clientguid = MAKE_GUID(p_recv_cmd->m_queryClient , enObjType_Player);
    MODI_ClientAvatar * p_client = pChannel->FindClientByID(clientguid);
    if(!p_client)
	{
		MODI_ASSERT(0);
        return;
	}

    MODI_GS2C_Notify_TransactionResult res;
	res.m_actionType = kTransaction_ToGive;
	res.m_result = kTransaction_UnknowError;
    safe_strncpy( res.m_szTargetName , p_recv_cmd->m_szRoleName, sizeof(res.m_szTargetName));
    if(p_recv_cmd->m_bHashChar)
	{
		MODI_ASSERT( sizeof(MODI_QueryHasChar_Shop_ExtraData) == p_recv_cmd->m_nSize);
		MODI_QueryHasChar_Shop_ExtraData extradata;
		memcpy(&extradata , p_recv_cmd->m_pExtraData, p_recv_cmd->m_nSize);

		MODI_ShopGood * p_goods_info = &(extradata.goods[0]);
		const BYTE buy_count = extradata.count;
		
		/// 货币是否充值
		if(! CheckBuyMoney(p_client, p_goods_info, buy_count))
		{
			Global::logger->warn("[check_send_item_money] not enough money <name=%s>", p_client->GetRoleName());
			res.m_result = kTransaction_NotEnoughMoney;
            p_client->SendPackage( &res , sizeof(res));
		}

		/// 性别判断
		if(! CheckBuySex(p_recv_cmd->m_sex, p_goods_info, buy_count))
		{
			Global::logger->error("[check_send_item_sex] check sex failed <name=%s,sex=%d>", p_client->GetRoleName(), p_client->GetSex());
			res.m_result = kTransaction_InvalidSex;
			p_client->SendPackage( &res , sizeof(res) );
			return; 
		}

		/// 开始扣钱发邮件
		
		/// 获取交易号
		QWORD buy_serial_id = GetBuySerial(p_client);
		if(buy_serial_id == 0)
		{
			Global::logger->fatal("[get_give_item_serial] get buy serial faild <name=%s>", p_client->GetRoleName());
			MODI_ASSERT(0);
			p_client->SendPackage(&res , sizeof(res));
		}

		/// ina 第二次进入赠送
		if(buy_serial !=0 && Global::g_byGameShop > 0)
		{
			buy_serial_id = buy_serial;
		}
		

		defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
    	//defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();
		const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[0].m_pConfigID);
		if( !pElement )
		{
			Global::logger->fatal("[send_item] not find element <shopid=%u>", p_goods_info[0].m_pConfigID);
			MODI_ASSERT(0);
			p_client->SendPackage(&res , sizeof(res));
		}

		enMoneyType bys = (enMoneyType)(pElement->get_BuyStrategy());
		DWORD get_price = 0;
		/// 金币
		if(kGameMoney == bys)
		{
			get_price = GetPrice_Money(pElement , p_goods_info[0].m_TimeStrategy);
		}
		else if(kRMBMoney == bys)
		{
			get_price = GetPrice_RMBMoney(pElement , p_goods_info[0].m_TimeStrategy);
		}
		
		if(get_price == 0)
		{
			Global::logger->error("[on_buy_item] not get price <goodid=%u>", p_goods_info[0].m_pConfigID);
			MODI_ASSERT(0);
			p_client->SendPackage(&res , sizeof(res));
		}
		
		Global::logger->debug("[give_item_first] give item first <name=%s,recever=%s,buy_serial=%llu,get_price=%u,money_type=%d>",
							  p_client->GetRoleName(),p_recv_cmd->m_szRoleName, buy_serial_id,get_price, bys);
		if(bys == kGameMoney)
		{
			p_client->DecMoney(get_price);
		}
		else if(bys == kRMBMoney)
		{
			/// INA赠送
			if(buy_serial == 0 && Global::g_byGameShop > 0)
			{
				/// 先去扣钱再来
				char buf[Skt::MAX_USERDATASIZE];
				memset(buf, 0, sizeof(buf));

				MODI_INAGiveItem * p_send_cmd = (MODI_INAGiveItem *)buf;
				AutoConstruct(p_send_cmd);

				MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)(MODI_GameServerAPP::GetInstancePtr());
				if(!pApp)
				{
					return;
				}
				
				/// 基本信息
				p_send_cmd->m_byServerId = pApp->GetServerID();
				p_send_cmd->m_dwAccountId = p_client->GetAccountID();
				p_send_cmd->m_qdBuySerial = buy_serial_id;

				/// 扣钱信息
				if(buy_count == 1)
				{					
					MODI_INAItemPrice * p_startaddr = &(p_send_cmd->pData);
					p_startaddr->m_dwPrice = get_price;
					std::ostringstream os;
					os<< p_goods_info[0].m_pConfigID << "_" << p_goods_info[0].m_TimeStrategy;
					strncpy(p_startaddr->m_cstrItemId, os.str().c_str(), sizeof(p_startaddr->m_cstrItemId));
					p_startaddr->m_pGoods = p_goods_info[0];
				}
				else
				{
					Global::logger->error("[give_inaitem] once send once");
					MODI_ASSERT(0);
				}

				/// 赠送信息
				strncpy(p_send_cmd->m_cstrGiftRoleName, p_recv_cmd->m_szRoleName, sizeof(p_send_cmd->m_cstrGiftRoleName));
				DWORD recv_size = sizeof(MODI_ZS2S_Notify_QueryHasCharResult_Shop) + p_recv_cmd->m_nSize;
				memcpy(p_send_cmd->m_pGiveData, p_recv_cmd, recv_size);
				p_send_cmd->m_wdSize = recv_size;

				/// send cmd to bill
				MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
				if(! pZone )
				{
					Global::logger->info("[buy_ina_item] can't get zone service client");
					MODI_ASSERT(0);
					return;
				}
				pZone->SendCmd(p_send_cmd, sizeof(MODI_INAItemConsume) + sizeof(MODI_INAItemPrice) * p_send_cmd->m_wdSize);
				
				return;
			}
			
			p_client->DecRMBMoney(get_price);
		}

		Global::logger->debug("[give_item_second] give item second <name=%s,recever=%s,buy_serial=%llu,get_price=%u,money_type=%d>",
							  p_client->GetRoleName(),p_recv_cmd->m_szRoleName, buy_serial_id,get_price, bys);

		/// 保存到赠送记录数据库中(临时方便查询)，本来这里应该是直接生成道具给对方的，邮件附件领取时激活(包括分配包裹位置等)
		/// 赠送到交易库中
		MODI_Record record_insert;
		record_insert.Put("buy_serial",buy_serial_id);
		record_insert.Put("accid", p_client->GetAccountID());
		if(kGameMoney == bys)
		{
			record_insert.Put("money_rmb", 0);
			record_insert.Put("money", get_price);
		}
		else if(kRMBMoney == bys)
		{
			record_insert.Put("money_rmb", get_price);
			record_insert.Put("money", 0);
		}
	
		MODI_RecordContainer record_container;
		record_container.Put(&record_insert);
		MODI_ByteBuffer result(1024);
		if(! MODI_DBClient::MakeInsertSql(result, GlobalDB::m_pGiverecordTbl, &record_container))
		{
			Global::logger->debug("[save_give_record_to_db] make save give_recod sql failed <accid=%u,buy_serial=%llu,type=%d,price=%u>",
								  p_client->GetAccountID(), buy_serial_id, bys,get_price);
			MODI_ASSERT(0);
		}
		else
		{
			MODI_GameServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
		}
			

		/// 送邮件
		QWORD nTransid = buy_serial_id;
		SendGiveMail(p_client->GetCharID() ,  p_client->GetRoleName(), p_recv_cmd->m_szRoleName, p_goods_info, buy_count,
					 extradata.content , nTransid);
		res.m_result = kTransaction_Successful;
		p_client->SendPackage( &res , sizeof(res) );

		Global::logger->debug("[give_item_third] give itme third <sender=%s,recvname=%s,transid=%llu>", p_client->GetRoleName(),
							  p_recv_cmd->m_szRoleName, buy_serial_id);
#ifdef _FB_LOGGER
		Global::fb_logger->debug("\t%u\tGIVES ITEM\t%u\t%s\t%d\t%s\t%s",Global::m_stRTime.GetSec(),p_goods_info->m_pConfigID,pElement->get_Name(),p_client->GetCharID(),p_client->GetAccName(),p_recv_cmd->m_szRoleName);
#endif
	}
	else
	{
		res.m_result = kTransaction_ReceverNotExist;
		p_client->SendPackage( &res , sizeof(res) );
	}
}



/** 
 * @brief 发送赠送mail
 * 
 * @return 成功true
 *
 */
bool MODI_GameShop::SendGiveMail(MODI_CHARID senderid, const char * szSender, const char * szReceverName, const MODI_ShopGood * pGoods,
								 BYTE nCount, const char * szContent , QWORD transid)
{
    return ReqZoneSendGiveItemMail( senderid , szSender , szReceverName ,
									pGoods , nCount ,
									kTransaction_ToGive ,
									szContent , transid );
}


/** 
 * @brief 发送带附件的邮件
 * 
 */
bool MODI_GameShop::ReqZoneSendGiveItemMail(MODI_CHARID senderid, const char * szSender, const char * szRecever, const MODI_ShopGood * pGoods,
											BYTE nCount, enShopTransactionType type, const char * szContent, QWORD transid)
{
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
    if( !pZone )
	{
        return false;
	}

    MODI_S2ZS_Request_SendGiveItemMail msg;
    msg.m_sender_charid = senderid;
    msg.m_nCount = nCount;
    msg.m_type = type;
    msg.m_TransID = transid;

    defShoplistBinFile & shopBin = MODI_BinFileMgr::GetInstancePtr()->GetShopBinFile();
    defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();
    defAvatarBinFile & avatarBin = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile();
    DISABLE_UNUSED_WARNING(avatarBin);
    for( BYTE n = 0; n < nCount; n++ )
	{
		const GameTable::MODI_Shop * pElement = shopBin.Get( pGoods[n].m_pConfigID );
		if( !pElement )
			return false;

		MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = msg.m_Goods[n];
		WORD nItemCount = 1;
		const GameTable::MODI_Itemlist * pItem =  itemBin.Get( pElement->get_SellItemID() );
		if( pItem )
		{
			nItemCount = pElement->get_Count();
			tmp.m_nLimitTime = kShopGoodT_Forver;
		}
		else
		{
			//MODI_ItemOperator::GetAvatarLimit(pGoods[n].m_pConfigID, pGoods[n].m_TimeStrategy , tmp.m_nLimitTime) ;
			tmp.m_nLimitTime = pGoods[n].m_TimeStrategy;
		}

		tmp.m_nCount = nItemCount;
		tmp.m_nItemConfigID = pElement->get_SellItemID();
	}

    safe_strncpy( msg.m_szSenderName  , szSender ,  sizeof(msg.m_szSenderName) );
    safe_strncpy( msg.m_szReceverName , szRecever , sizeof(msg.m_szReceverName) );
    if( szContent )
	{
		safe_strncpy( msg.m_szContent ,  szContent , sizeof(msg.m_szContent) );
	}

    if( !pZone->SendCmd( &msg , sizeof(msg) ) )
	{
        return false;
	}
	
    return true;
}
