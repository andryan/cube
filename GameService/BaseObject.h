/** 
 * @file BaseObject.h
 * @brief 游戏中基础对象
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MODI_BASEOBJECT_H_
#define MODI_BASEOBJECT_H_

#include "protocol/gamedefine.h"
#include "G3D/Vector3.h"

class MODI_GameRoom;

/**
 * @brief 游戏中的基础对象
 *
 */
class MODI_BaseObject
{
public:
    MODI_BaseObject(MODI_GUID id);
    virtual ~MODI_BaseObject();

	/** 
	 * @brief 获得对象的GUID
	 * 
	 * @return 	返回对象的GUID 
	 */
    MODI_GUID GetGUID() const { return m_GUID;}  

	virtual bool IsClientAvatar() { return true; }

public:

	/** 
	 * @brief 对象更新
	 * 
	 * @param nTime 更新的时间
	 */
    virtual void Update(DWORD nTime) {}

	/** 
	 * @brief 对象进入房间的回调
	 * 
	 * @param room  房间对象
	 */
	virtual void OnEnterRoom( MODI_GameRoom & room ) { }

	/** 
	 * @brief 对象离开房间的回调
	 * 
	 * @param room 房间对象
	 */
	virtual void OnLeaveRoom( MODI_GameRoom & room ) { }

	/** 
	 * @brief 从数据库载入对象的数据
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool LoadFromDB() { return true; }

	/** 
	 * @brief 保存对象数据到数据库
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool SaveToDB() { return true; }

	/** 
	 * @brief 从数据中删除该对象的数据
	 * 
	 * @note  返回TRUE，并不代表操作真正执行成功。数据操作的过程都是异步的
	 * @return 成功返回TRUE	
	 */
	virtual bool DeleteFromDB() { return true; }

	/** 
	 * @brief 从数据库载入对象数据的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnLoadFromDB( const void * pData ) { return true; }

	/** 
	 * @brief 对象数据保存到数据库后的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnSaveToDB( const void * pData ) { return true; }

	/** 
	 * @brief 对象数据从数据库删除后的回调
	 * 
	 * @note  数据库的异步操作真正完成时回调该方法
	 * @return 	成功返回TRUE
	 */
	virtual bool OnDeleteFromDB( const void * pData ) { return true; }

	void SetType(enSceneAttr type)
	{
		m_stType = type;
	}

	enSceneAttr GetType()
	{
		return m_stType;
	}
	
protected:

	/// 对象的GUID
    MODI_GUID m_GUID;
	/// 类型
	enSceneAttr m_stType;
};

#endif
