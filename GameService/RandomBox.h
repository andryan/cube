#ifndef _RANDOM_BOX_H__
#define _RANDOM_BOX_H__

#include "Global.h"
#include "BinFileMgr.h"
#include <map>
//#include "GameTableDefine/Random.h"
#include "GameTableDefine/AllResource.h"
#include <vector>
#include "protocol/c2gs_itemcmd.h"
#include "protocol/gamedefine.h"
#include "Share.h"
#include "GameServerAPP.h"
#include "ClientAvatar.h"

/// 随机信息
struct MODI_RandomInfo
{
	WORD	m_wClass;
	DWORD	m_dwFrom;
	DWORD	m_dwEnd;
	WORD 	m_wdNum;
	std::vector<DWORD> m_RandNum;
	MODI_RandomInfo()
	{
		m_wClass = 0;
		m_dwFrom = 0;
		m_dwEnd = 0;
		m_wdNum = 0;
	}
};

class	MODI_RandomBoxMgr
{
	typedef	std::map<WORD, std::vector<WORD> > defListMgr;
	typedef std::map<WORD, std::vector<WORD> >::iterator defListItor;
	typedef	std::map<WORD, std::vector<WORD> >::value_type defListValue;

	typedef std::map<WORD, MODI_RandomInfo>    defRandMap;
	typedef std::map<WORD, MODI_RandomInfo>::value_type  defRandMapValue;
	typedef	std::map<WORD, MODI_RandomInfo>::iterator	defRandMapItor;
public:
	static MODI_RandomBoxMgr * GetInstance() ;
	bool	Init();
	WORD	GetRandItemNum();
	bool	GetRandItem( BagItemInfos  & info, const BYTE get_rand_num_type, const BYTE client_sex, bool & is_notify);
	void	OpenItemBox( void *pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	bool	OpenYangWBox(MODI_ClientAvatar * p_client);


private:
	MODI_RandomBoxMgr();
	~MODI_RandomBoxMgr()
	{
	}

	BYTE	GetItemAttr(const WORD class_info);
	const WORD	GetRandClass(const BYTE get_rand_num_type);
	static	MODI_RandomBoxMgr   *m_pRandomBoxMgr;

	defListMgr	m_ItemMgr;
	defRandMap 	m_RandMap;

};

#endif

