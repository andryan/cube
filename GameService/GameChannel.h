/**
 * @file GameChannel.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 游戏中的频道对象
 *
 */

#ifndef MODI_GAME_CHANNEL_H_
#define MODI_GAME_CHANNEL_H_

#include <string>
#include <map>
#include "GameLobby.h"
#include "SingleObject.h"
#include "TNKeyMap.h"
#include "GameShop.h"

class MODI_GameLobby;

class  MODI_ChannelClientCallBack
{
	public:

	MODI_ChannelClientCallBack() {}
	virtual ~MODI_ChannelClientCallBack(){}
	virtual bool Done(MODI_ClientAvatar * p_client) = 0;
};


/**
 * @brief 游戏中的频道对象
 *
 */
class MODI_GameChannel : public CSingleObject<MODI_GameChannel>
{
public:

    MODI_GameChannel();
    virtual ~MODI_GameChannel();

public:

    //	属性访问相关操作
    const std::string & GetName()
    {
        return m_strChannleName;
    }
    void SetName(const char * szName)
    {
        m_strChannleName = szName;
    }
    void SetName(const std::string & strName)
    {
        m_strChannleName = strName;
    }

    MODI_GameLobby * GetLobby()
    {
        return m_pLobby;
    }


	/** 
	 * @brief  广播整个频道
	 * 
	 * @param pt_null_cmd 数据包
	 * @param cmd_size 数据包大小
	 * 
	 * @return 成功返回TRUE
	 */
    bool Broadcast(const Cmd::stNullCmd * pt_null_cmd, int cmd_size);
    	/**
	 * @brief  
	 *	广播以为MASK倍数ACCID的玩家
	 */
    bool  Broadcast_mask(const Cmd::stNullCmd * pt_null_cmd, int cmd_size, BYTE mask=0, BYTE div=1);

    /**
     * @brief 指定范围的人们广播
     */
    bool	BroadCast_Range( const Cmd::stNullCmd *pt_null_cmd,int cmd_size,MODI_CHARID * mem,WORD n);

	/** 
	 * @brief 把所有人的家族信息发给自己
	 * 
	 */
	int CreateCFCardPackage( char * szBuf , int nBufSize );

	bool	BroadcastSystemChat(const char * m_cstr,WORD size);
	/** 
	 * @brief  广播在家族里面的人
	 * 
	 * @return 
	 */
	bool BroadcastInFamily(const Cmd::stNullCmd * pt_null_cmd, int cmd_size);

	/** 
	 * @brief 广播整个频道
	 * 
	 * @param pt_null_cmd 数据包
	 * @param cmd_size 数据包大小
	 * @param pExcept  该对象不会被广播
	 * 
	 * @return 成功返回TRUE	
	 */
    bool Broadcast_Except(const void * pt_null_cmd, int cmd_size, MODI_ClientAvatar * pExcept);

	
	/** 
	 * @brief  广播整个频道除商场外
	 * 
	 * @param pt_null_cmd 数据包
	 * @param cmd_size 数据包大小
	 * 
	 * @return 成功返回TRUE
	 */
    bool BroadcastNoInShop(const Cmd::stNullCmd * pt_null_cmd, int cmd_size);
	

	void SyncPlayerlist( const char * szPackageBuf , int iSendSize , unsigned long nTime);

	void AllClientExcuteCallback( MODI_ChannelClientCallBack & callback );

public:

	/** 
	 * @brief 创建频道
	 * 
	 * @param strChannelName 频道的名字
	 * @param nMaxClient 	频道人数上线
	 * 
	 * @return 	
	 */
    bool Create(const std::string & strChannelName, unsigned int nMaxClient);

	/** 
	 * @brief 销毁频道
	 */
    void Destory();

	/** 
	 * @brief  添加一个对象到频道中
	 * 
	 * @param p 进入频道的对象
	 * 
	 * @return 成功返回TRUE
	 */
    bool EnterChannel(MODI_ClientAvatar * p);

	/** 
	 * @brief 从频道中删除一个客户端
	 * 
	 * @param p 离开频道的对象
	 * @param iReason 离开原因
	 */
    void LeaveChannel(MODI_ClientAvatar * p, int iReason);

    ///    
	/** 
	 * @brief 频道更新
	 * 
	 */
    void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);

	/**
	 * @brief 频道人数
	 * 
	 */
	const unsigned int Size() const
	{
		return m_nClients;
	}
	
	bool 	IsFull() const
	{
		return m_nClients >= m_nMaxClient;
	}

	MODI_ClientAvatar * FindClientByID( const MODI_GUID & guid );

	MODI_ClientAvatar * FindClientByName( const char * szName );

	MODI_ClientAvatar * FindByAccid(const defAccountID & accid );

	MODI_GameNormalShop & 	GetNormalShop() { return m_normalShop; }
	MODI_GameRareShop & 	GetRareShop() { return m_rareShop; }

	static BYTE group_num;

private:

	void 	SyncLoadStatus();

private:

    MODI_GameLobby * m_pLobby;
    std::string m_strChannleName;
    unsigned int m_nClients;
    unsigned int m_nMaxClient;
	MODI_Timer 	m_timerPlayerlist;
	MODI_Timer m_stSaveTime;
	MODI_NKeyMap 	m_mapClients;
	MODI_GameNormalShop 	m_normalShop;
	MODI_GameRareShop 		m_rareShop;

};

#endif
