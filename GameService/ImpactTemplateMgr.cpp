#include "ImpactTemplateMgr.h"
#include "ImpactT_ModifyExp.h"
#include "ImpactT_ModifyMoney.h"
#include "ImpactT_ModifyRenqi.h"
#include "ImpactT_ChangeAvatar.h"
//#include "ImpactT_Feizi.h"
//#include "ImpactT_ChannelChat.h"
#include "ImpactT_Animation.h"
//#include "ImpactT_FensiChat.h"
//#include "ImpactT_WorldChat.h"
#include "ImpactT_AddGoodPackage.h"
#include "ImpactT_RecordMusic.h"



MODI_ImpactTemplateMgr::MODI_ImpactTemplateMgr()
{
	for( WORD n = 0; n < kImpactTemplateCount; n++ )
	{
		m_Template[n] = 0;
	}
}

MODI_ImpactTemplateMgr::~MODI_ImpactTemplateMgr()
{
	CleanUp();
}

void 		MODI_ImpactTemplateMgr::Initial()
{
	RegisterImpactTemplate( kImpactTemplateIDBegin, new MODI_ImpactT_Null() , "null" );
	RegisterImpactTemplate( kIT_ModfiyExpByFactor , new MODI_ImpactT_ModifyExpByFactor() , "ModifyExpByFactor" );
	RegisterImpactTemplate( kIT_ModfiyExpByConst , new MODI_ImpactT_ModifyExpByConst() , "ModifyExpByConst" );
	RegisterImpactTemplate( kIT_ModifyMoneyByConst , new MODI_ImpactT_ModifyMoney() , "ModifyMoney" );
	RegisterImpactTemplate( kIT_ChangeAvatar , new MODI_ImpactT_ChangeAvatar() , "ChangeAvatar" );
	RegisterImpactTemplate( kIT_Feizi , new MODI_ImpactT_Null() , "Feizi" );
	RegisterImpactTemplate( kIT_ChannelChat , new MODI_ImpactT_Null() , "ChannelChat" );
	RegisterImpactTemplate( kIT_PlayAnimation , new MODI_ImpactT_Animation() , "PlayAnimation" );
	RegisterImpactTemplate( kIT_WorldChat , new MODI_ImpactT_Null() , "WorldChat" );
	RegisterImpactTemplate( kIT_FensiChat , new MODI_ImpactT_Null() , "FensiChat" );
	RegisterImpactTemplate( kIT_AddItem , new MODI_ImpactT_AddItem() , "AddItem" );
	RegisterImpactTemplate( kIT_RecordMusic , new MODI_ImpactT_RecordMusic() , "RecordMusic" );
	RegisterImpactTemplate( kIT_ModifyRenqi, new MODI_ImpactT_ModifyRenqi(), "ModiRenqi");
}


MODI_ImpactTemplate 	* 	MODI_ImpactTemplateMgr::GetImpactHandle( WORD nTemplateID ) const
{
	if( nTemplateID > kImpactTemplateIDBegin && nTemplateID < kImpactTemplateIDEnd )
	{
		return m_Template[nTemplateID - 1];
	}
	return 0;
}

const char * MODI_ImpactTemplateMgr::GetImpactName( WORD nTemplateID ) const
{
	if( nTemplateID > kImpactTemplateIDBegin && nTemplateID < kImpactTemplateIDEnd )
	{
		return m_TemplateName[nTemplateID - 1].c_str();
	}
	static const std::string s_strUnknow = "unknow impact";

	return s_strUnknow.c_str();
}

void 		MODI_ImpactTemplateMgr::CleanUp()
{
	for( WORD n = 0; n < kImpactTemplateCount; n++ )
	{
		delete m_Template[n];
	}
}


void 		MODI_ImpactTemplateMgr::RegisterImpactTemplate(WORD nID , MODI_ImpactTemplate * pTemplate, const char * szTemplateName )
{
	if( nID > kImpactTemplateIDBegin && nID < kImpactTemplateIDEnd )
	{
		MODI_ASSERT( m_Template[nID - 1] == 0 );
		m_Template[nID - 1] = pTemplate;
		m_TemplateName[nID - 1] = szTemplateName;
	}
// 	else 
// 	{
// 		MODI_ASSERT(0);
// 	}
}



