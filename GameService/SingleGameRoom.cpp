#include "SingleGameRoom.h"
#include "protocol/c2gs_roomcmd.h"
#include "GameLobby.h"
#include "GameChannel.h"


MODI_SingleGameRoom::MODI_SingleGameRoom( defRoomID id ):Parent(id)
{

}

MODI_SingleGameRoom::~MODI_SingleGameRoom()
{

}

bool MODI_SingleGameRoom::OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator )
{
	// 随机音乐ID 
	GetNormalInfo_Modfiy().m_nMusicId = GetRandomMusicID();
	GetNormalInfo_Modfiy().m_flag = GetRandomMusicFlag( GetMusicID() );
	GetNormalInfo_Modfiy().m_songType = GetRandomSongType( GetMusicID() );

	// 随机地图 
	if( mapid == RANDOM_MAP_ID )
	{
		mapid = GetRandomMapID();
	}

	GetNormalInfo_Modfiy().m_nMapId = mapid;

	GetNormalInfo_Modfiy().m_timeType = kMusicTime_All;

	SetMaster( pClient->GetGUID() );

	return true;
}

bool MODI_SingleGameRoom::EnterRoom(MODI_ClientAvatar * pClient)
{
	pClient->SetSingleGame( true );
	//  成功创建房间
	MODI_C2GS_Response_CreateRoom res;
	res.m_roomInfo = GetRoomInfo();
	res.m_selfPos = RoleHelpFuns::EncodePosSolt( true , 0 );
	pClient->SendPackage(&res,sizeof(res));
	return true;
}

bool MODI_SingleGameRoom::LeaveRoom(MODI_ClientAvatar * pClient, int iReason)
{
	pClient->SetSingleGame( false );
	MODI_GS2C_Notify_LeaveRoom notify;
	notify.m_objID = pClient->GetGUID();
	notify.m_byLeaveReason = iReason;
	pClient->SendPackage( &notify , sizeof(notify) );


//	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
//	if( pLobby )
//	{
//		pLobby->EnterLobby( pClient );
//	}

	return true;
}

void MODI_SingleGameRoom::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{

}

bool MODI_SingleGameRoom::OnDestroy()
{

	return true;
}

const char * MODI_SingleGameRoom::GetMasterName() const
{
	// 单人房间全局只有一个对象，所有客户端共享该对象。所以该方法暂时无法实现
	static const char * unknow_master_name = "unkonw";
//	MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
//	if( !channel )
//	{
//		return unknow_master_name; 
//	}
//
//	MODI_CHARID charid = GUID_LOPART( GetMaster() );
//	MODI_ClientAvatar * client = channel->FindByAccid( charid );
//	if( client )
//	{
//		return client->GetRoleName();
//	}
	return unknow_master_name; 
}
