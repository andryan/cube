#include "ClientAndZSDirection.h"
#include "ZoneClient.h"
#include "s2zs_cmd.h"


bool 	MODI_ClientAndZSDirection::ClientToZoneServer( MODI_ClientAvatar * pClient ,  
		const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZone )
		return false;

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2ZS_Redirectional * red = (MODI_GS2ZS_Redirectional *)(szPackageBuf);
	AutoConstruct( red );
	red->m_guid = MAKE_GUID( pClient->GetCharID() , 0 );
	red->m_sessionID = pClient->GetSessionID();
	memcpy( red->m_pData  , (char *)pt_null_cmd  , cmd_size );
	red->m_nSize = cmd_size;

	int iSendSize = sizeof(MODI_GS2ZS_Redirectional) + red->m_nSize;
	pZone->SendCmd( red, iSendSize );
	return false;
}


bool 	MODI_ClientAndZSDirection::ZoneServerToClient(const MODI_SessionID & sessionID , 
			const MODI_GUID & guid , 
			const char * pData , 
			size_t nSize )
{
	//  find client
	MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find( sessionID );
	if( !pSession )
		return false;
	MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
	if( !pClient )
		return false;
	MODI_CHARID charid = GUID_LOPART( guid );
	if( pClient->GetCharID() != charid )
	{
		MODI_ASSERT(0);
		return false;
	}
	pSession->SendPackage( pData , nSize );
	return true;
}

