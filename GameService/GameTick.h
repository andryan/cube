/**
 * @file GameTick.h
 * @date 2009-12-30 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 逻辑处理的主循环
 *
 */

#ifndef _MDGAMETICK_H
#define _MDGAMETICK_H

#include "Type.h"
#include "Timer.h"
#include "Thread.h"
#include "SingleObject.h"
#include "RecurisveMutex.h"

class MODI_GameTick: public MODI_Thread , public CSingleObject<MODI_GameTick>
{
public:

    /// 构造
    MODI_GameTick();

    /// 析构
    virtual ~MODI_GameTick();

    /// 释放资源
    void Final();

    virtual void Run();

	const MODI_RTime & 	GetTimer()
	{
		return m_stRTime;
	}

	static MODI_TTime m_stTTime;
	static MODI_RTime  m_stRTime;
private:

	MODI_Timer 	m_timerLastItemSerial;
	MODI_Timer 	m_timerRubbishCheck;
	MODI_TimerEqu m_stZeroEqu;
};

#endif
