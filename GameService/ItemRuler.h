/**
 * @file   ItemRuler.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jul  5 18:25:24 2011
 * 
 * @brief  物品操作规则
 * 
 */

#ifndef MD_ITEMRULER_H_
#define MD_ITEMRULER_H_

#include "BaseOS.h"
#include "protocol/gamedefine.h"


/** 
 * @brief 物品规则相关
 *
 */
class MODI_ItemRuler
{
 public:

	/** 
	 * @brief 是否可以重叠
	 * 
	 * @param config_id 道具id
	 * 
	 * @return 可以重叠true
	 *
	 */
	static bool IsCanLap(const DWORD & config_id);

	
	/** 
	 * @brief 最大的重叠数
	 * 
	 * @param config_id 物品id
	 * @param dwMax 最大重叠数
	 * 
	 * @return 成功true
	 *
	 */
	static bool GetMaxLap(const WORD config_id , WORD & dwMax );


	/** 
	 * @brief 是否可以出售
	 * 
	 * @param config_id 物品属性id
	 * 
	 * @return 可以true
	 *
	 */
	static bool IsCanInShop(const DWORD & config_id);



	/** 
	 * @brief 性别是否符合要求
	 * 
	 * @param config_id 道具id
	 * @param bySex 性别
	 * 
	 * @return 符合true
	 *
	 */
	static bool GetReqSex(const DWORD config_id , BYTE & bySex);



	/** 
	 * @brief 获取能最大的拥有数
	 * 
	 * @param config_id 物品id
	 * @param dwMax 最大数
	 * 
	 * @return 成功true,失败false
	 *
	 */
	static bool GetMaxOwn(const DWORD config_id , WORD & dwMax );


	/** 
	 * @brief 此类型物品最大的拥有数
	 * 
	 * @param config_id 物品id
	 * @param nMaxOwn 大小
	 * 
	 * @return 成功ture,失败false
	 *
	 */
	static bool GetSubClassMaxOwn(const WORD & config_id , WORD & nMaxOwn);

	/** 
	 * @brief 等级是否符合要求
	 * 
	 * @param config_id 物品id
	 * @param byLevel 等级
	 * 
	 * @return 可以true
	 *
	 */
	static bool GetReqLevel(const DWORD config_id , BYTE & byLevel);



	/** 
	 * @brief 使用环境
	 * 
	 * @param config_id 物品id
	 * @param state 游戏状态
	 * 
	 * @return 成功ture
	 *
	 */
	static bool GetCanUseInGameState(const DWORD config_id , enItemCanUseInGameState & state);



	/** 
	 * @brief 使用目标检查
	 * 
	 * @param config_id 物品id
	 * @param type 使用目标
	 * 
	 * @return 成功true
	 *
	 */
	static bool GetCanUseTarget(const WORD & config_id , enItemCanUseTargetType & type );

};


#endif
