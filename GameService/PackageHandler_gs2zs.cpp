#include "PackageHandler_gs2zs.h"
#include "s2adb_cmd.h" 
#include "s2rdb_cmd.h" 
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "GameTask.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h" 
#include "ClientAvatar.h"
#include "GUIDCreator.h"
#include "Channellist.h"
#include "s2zs_cmd.h"
#include "ZoneClient.h"
#include "GameChannel.h"
#include "ItemOperator.h"
#include "Base/TransCommand.h"
#include "YunYingKeyExchange.h"
#include "protocol/c2gs_family.h"
#include "GameServerAPP.h"
#include "ItemOperator.h"
#include "GlobalVar.h"
#include "ScriptManager.h"
#include "GameSingleMusicMgr.h"

MODI_PackageHandler_gs2zs::MODI_PackageHandler_gs2zs()
{

}

MODI_PackageHandler_gs2zs::~MODI_PackageHandler_gs2zs()
{

}

bool MODI_PackageHandler_gs2zs::FillPackageHandlerTable()
{
	this->AddPackageHandler( MAINCMD_S2ADB, MODI_ADB2S_Notify_LoginAuthResult::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OnHandler_LoginAuthResult );
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_EnterChannelRet::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OnHandler_EnterChannelRet );
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_KickSb::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OnHandler_KickSb);
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_ItemSerial::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::LoadLastItemSerial_Handle );
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_ReMakePPResult::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::RemakePP_Handle );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Request_AddMoeny::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::AddMoeny_Handler );
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Request_AddItem::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::AddItem_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_QueryHasCharResult_Shop::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::QueryHasCharResult_Handler );

	this->AddPackageHandler( MODI_RetConsumeMoneyCmd::m_bySCmd , MODI_RetConsumeMoneyCmd::m_bySParam ,
			&MODI_PackageHandler_gs2zs::OnBillTransResult_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_ResetOnlineTime::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OnResetTodayOnlineTime_Handler );

	this->AddPackageHandler( MAINCMD_S2RDB,  MODI_RDB2S_Notify_ExchangeByYunyingKeyRes::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OnExchangeYYKey_Handler );

	/// 改变人民币
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_RefreshMoney::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::RefreshMoney_Handler );

	/// 改变金币
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_OptMoney::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::OptMoney_Handler );

	/// 改变gmlevel
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_Modi_GMLevel::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::ModiGMLevel_Handler );

	/// 充值返回
	this->AddPackageHandler( MODI_GS2C_Notify_PayResult::m_bySCmd , MODI_GS2C_Notify_PayResult::ms_SubCmd ,
							 &MODI_PackageHandler_gs2zs::ReturnPayResult_Handler );

	/// 称号改变
	this->AddPackageHandler( MAINCMD_S2ZS, MODI_ZS2S_Notify_ChangChenghao::ms_SubCmd ,
							 &MODI_PackageHandler_gs2zs::ChangeChenghao_Handler );

	/// 沉迷了
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_ChenMi_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::ChenMi_Handler );

	/// 加载变量
	this->AddPackageHandler( MAINCMD_S2RDB,  MODI_RDB2S_Return_LoadUserVar_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::LoadUserVar_Handler );

	/// 用户包裹
	this->AddPackageHandler( MAINCMD_S2RDB,  MODI_RDB2S_Notify_ItemInfo::ms_SubCmd,
			&MODI_PackageHandler_gs2zs::NotifyItemInfo_Handler);

	/// 网页道具
	this->AddPackageHandler( MAINCMD_S2RDB,  MODI_RDB2S_Ret_WebItem::ms_SubCmd,
			&MODI_PackageHandler_gs2zs::NotifyWebItem_Handler);

	/// 网页赠送道具
	this->AddPackageHandler( MAINCMD_S2RDB,  MODI_RDB2S_Ret_WebPresent::ms_SubCmd,
			&MODI_PackageHandler_gs2zs::NotifyWebPresent_Handler);

	/// 加载家族信息
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_FamilyInfo_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::LoadCFamilyInfo_Handler );


	/// 家族增加一个列表
	this->AddPackageHandler( MAINCMD_FAMILY,  MODI_GS2C_Notify_AddFamilyList::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::BroadcastInFamily_Handler );

	/// 家族减少一个列表
	this->AddPackageHandler( MAINCMD_FAMILY,  MODI_GS2C_Notify_DelFamilyList::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::BroadcastInFamily_Handler );


	/// 家族广播列表
	this->AddPackageHandler( MAINCMD_FAMILY,  MODI_ZS2S_Notify_FamilyInfo_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::BroadcastInFamily_Handler );
	
	/// 收到全局变量列表
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_GlobalVarList::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::ReceiveGlobalVarList_Handler );

	/// 收到全局变量修改信息
	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_VarChanged::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::ReceiveVarChange_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS,  MODI_ZS2S_Notify_SingleMusicList::ms_SubCmd ,
			&MODI_PackageHandler_gs2zs::SingleMusicList_Handler );
	
	/// INAItem扣钱返回
	this->AddPackageHandler( MODI_INAItemReturn::m_bySCmd , MODI_INAItemReturn::m_bySParam ,
			&MODI_PackageHandler_gs2zs::INAItemReturn_Handler );

	/// INA赠送钱返回
	this->AddPackageHandler( MODI_INAGiveReturn::m_bySCmd , MODI_INAGiveReturn::m_bySParam ,
			&MODI_PackageHandler_gs2zs::INAGiveReturn_Handler );

	AddPackageHandler( MAINCMD_S2ZS, MODI_ZS2S_Broadcast_Cmd::ms_SubCmd,
		       	&MODI_PackageHandler_gs2zs::BroadcastCmd_Handler);

	AddPackageHandler(MAINCMD_S2ZS, MODI_ZS2S_Broadcast_CmdRange::ms_SubCmd,
			&MODI_PackageHandler_gs2zs::BroadcastCmdRange_Handler);
	return true;
}

int MODI_PackageHandler_gs2zs::OnHandler_LoginAuthResult( void * pObj ,
			const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  )
{
	// account db验证成功
	const MODI_ADB2S_Notify_LoginAuthResult * pMsg = (const MODI_ADB2S_Notify_LoginAuthResult *)(pt_null_cmd);

	MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find( pMsg->m_nSessionID  );
	if( !pSession )
		return enPHandler_OK;

	MODI_ASSERT(pSession->IsInWaitAuthResultStatus());

	bool bInvaild = false;
	if( !pMsg->m_byResult )
	{
		bInvaild = true;

		Global::logger->warn("[%s] session<%s>'s status<%s> , client<accid=%u , charid=%u> request auth P.P faild.. "  ,
				LOGIN_OPT, 
				pSession->GetSessionID().ToString(),
				pSession->GetStatusStringFormat(),
				pMsg->m_nAccountID,
				pMsg->m_nCharID );
	}

	if( bInvaild == false && pSession->GetClientAvatar() )
	{
		Global::logger->warn("[%s] session<%s>'s status<%s> , client<charid=%u, name=%s> already has roledata . "  ,
				LOGIN_OPT, 
				pSession->GetSessionID().ToString(),
				pSession->GetStatusStringFormat(),
				pSession->GetClientAvatar()->GetCharID() ,
				pSession->GetClientAvatar()->GetRoleName() );
		MODI_ASSERT(0);
		bInvaild = true;
		return enPHandler_OK;
	}

	if( !bInvaild  )
	{
		MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
		if( pZoneServer )
		{
			MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
			MODI_ClientAvatar * pAlready = pChannel->FindByAccid( pMsg->m_nAccountID );
			if( pAlready )
			{
				Global::logger->warn("[%s] session<%s>'s status<%s> , \
						client<accid=%u , charid=%u> request auth P.P successful , \
						but already has antherone,kick this. "  ,
					LOGIN_OPT, 
					pSession->GetSessionID().ToString(),
					pSession->GetStatusStringFormat(),
					pMsg->m_nAccountID,
					pMsg->m_nCharID );
			}
			else 
			{
				MODI_S2ZS_Request_EnterChannel msg;
				msg.m_accid = pMsg->m_nAccountID;
				msg.m_charid = pMsg->m_nCharID;
				msg.m_nSessionID = pSession->GetSessionID();
				if( pZoneServer->SendCmd( &msg , sizeof(msg) ) )
				{
					bInvaild = false;
					pSession->SetFangChenMi( pMsg->m_bFangChengMi );
				}
				else 
				{
					MODI_ASSERT(0);
					bInvaild = true;
				}
			}
		}
	}

	if( bInvaild )
	{
		if( MODI_GameTask::ms_pInstancePtr )
		{
			// 让网关删除该客户端的连接
			MODI_GW2GS_DelSession 	delSession;
			delSession.m_sessionID = pSession->GetSessionID();
			MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 
		}

		pSession->SwitchWaitBekickStatus();
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_gs2zs::OnHandler_EnterChannelRet( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  )
{
	const MODI_ZS2S_Notify_EnterChannelRet * pMsg = (const MODI_ZS2S_Notify_EnterChannelRet *)(pt_null_cmd);
	MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find( pMsg->m_sessionID );
	if( !pSession )
		return enPHandler_OK;

	bool bFaild = false;
	if( pMsg->m_bySuccessful == 0 || 
		pSession->IsInWaitAuthResultStatus() == false ||
		pSession->GetClientAvatar() )
	{
		bFaild = true;

		Global::logger->warn("[%s] client<name=%s,charid=%u,accid=%u> enter channel faild, <successful=%u,status=%s,ptr=%p>" , 
						ROLEDATA_OPT ,
						pMsg->m_fullData.m_roleInfo.m_baseInfo.m_cstrRoleName,
						GUID_LOPART( pMsg->m_fullData.m_roleInfo.m_baseInfo.m_roleID ),
						pMsg->m_accid ,
						pMsg->m_bySuccessful ,
						pSession->GetStatusStringFormat(),
						pSession->GetClientAvatar() );
	}

	if( bFaild == false )
	{
		MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
		MODI_ClientAvatar * pAlreadyOne = pChannel->FindByAccid( pMsg->m_accid );
		if( pAlreadyOne )
		{
			MODI_ASSERT(0);
			bFaild = true;
		}
		else 
		{
			MODI_ClientAvatar * pClient = new MODI_ClientAvatar( pMsg->m_fullData.m_roleInfo.m_baseInfo.m_roleID , pSession );
			pClient->SetAccountID( pMsg->m_accid );

			if( pClient->OnLoadFromDB( &pMsg->m_fullData ) == false )
			{
				Global::logger->warn("[%s] client<name=%s,charid=%u,accid=%u> enter chanel faild , load from db faild." , 
						ROLEDATA_OPT ,
						pMsg->m_fullData.m_roleInfo.m_baseInfo.m_cstrRoleName,
						GUID_LOPART( pMsg->m_fullData.m_roleInfo.m_baseInfo.m_roleID ),
						pMsg->m_accid);
				bFaild = true;
				delete pClient;
				MODI_ASSERT( pSession->GetClientAvatar() == 0 );
				MODI_ASSERT(0);
			}
			else 
			{
				MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
				if( pChannel->EnterChannel( pClient ) )
				{
					// 等待音乐头状态
					pSession->SwitchAuthedWaitMusicHeaderStatus();
				}
				else 
				{
					bFaild = true;
					
					Global::logger->warn("[%s] client<name=%s,charid=%u,accid=%u> enter chanel faild , can't add to channel  ." , 
						ROLEDATA_OPT ,
						pMsg->m_fullData.m_roleInfo.m_baseInfo.m_cstrRoleName,
						GUID_LOPART( pMsg->m_fullData.m_roleInfo.m_baseInfo.m_roleID ),
						pMsg->m_accid);

					delete pClient;
					MODI_ASSERT( pSession->GetClientAvatar() == 0 );
				}
			}
		}
	}

	if( bFaild )
	{
		if( MODI_GameTask::ms_pInstancePtr )
		{
			// 让网关删除该客户端的连接
			MODI_GW2GS_DelSession 	delSession;
			delSession.m_sessionID = pSession->GetSessionID();
			MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 
		}

		pSession->SwitchWaitBekickStatus();
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::OnHandler_KickSb( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  )
{
	const MODI_ZS2S_Notify_KickSb * pMsg = (const MODI_ZS2S_Notify_KickSb *)(pt_null_cmd);
	Global::logger->info( "[%s] recv kick client's notify <charid=%u,reason=%s,loginserver_sessionid=%s,selected_serverid=%u." , 
			KICK_CLIENT ,
			GUID_LOPART(pMsg->m_guid) , 
			GetKickReasonStringFormat( pMsg->m_byReason ),
			pMsg->m_LoginServerSessionID.ToString() ,
			pMsg->m_bySelectSverver );

	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer )
	{
		Global::logger->fatal("[%s] kick faild , can't get zoneserver ptr." , 
				KICK_CLIENT );
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	bool bDelClient = true;
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientSession  * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find( pMsg->m_sessionID );
	MODI_S2ZS_Request_KickSbResult res;
	res.m_byResult = MODI_S2ZS_Request_KickSbResult::enNotFound;
	res.m_byReason = pMsg->m_byReason;
	res.m_guid = pMsg->m_guid;
	res.m_LoginServerSessionID = pMsg->m_LoginServerSessionID;
	res.m_bySelectSverver = pMsg->m_bySelectSverver;

	if( !pSession )
	{
		Global::logger->fatal("[%s] kick faild , can't find client by session<%s>" , 
				KICK_CLIENT ,
				pMsg->m_sessionID.ToString() );
		pZoneServer->SendCmd( &res , sizeof(res) );
		return enPHandler_OK;
	}

	MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
	if( !pClient )
	{
		Global::logger->fatal("[%s] kick faild , session<%s ,status=%s> not have clientavatar." , 
				KICK_CLIENT ,
				pMsg->m_sessionID.ToString() ,
				pSession->GetStatusStringFormat() );
		pZoneServer->SendCmd( &res , sizeof(res) );
		return enPHandler_OK;
	}
	
	int iLeaveReason = enLeaveReason_ExitGame;
	if( pClient->GetGUID() != pMsg->m_guid )
	{
		Global::logger->fatal("[%s] kick faild , session<%s ,status=%s>'s guid<%s> not equal<%s>." , 
				KICK_CLIENT ,
				pMsg->m_sessionID.ToString() ,
				pSession->GetStatusStringFormat() ,
				pClient->GetGUID().ToString() ,
				pMsg->m_guid.ToString() );

		pZoneServer->SendCmd( &res , sizeof(res) );
		// 客户端离开频道
		pChannel->LeaveChannel( pClient , iLeaveReason  );
		delete pClient;
		MODI_ASSERT( pSession->GetClientAvatar() == 0 );
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	if( pMsg->m_byReason == kAntherLogin || 
			pMsg->m_byReason == kAntherLoginImmediatelyKick )
	{
		// 通知客户端，你被顶掉了
		MODI_GS2C_Respone_OptResult notify;
		notify.m_nResult = SvrResult_World_Kick_SbLogin;
		pClient->SendPackage( &notify , sizeof(notify) );

		if( pMsg->m_byReason == kAntherLogin )
		{
			// flush 客户端数据到ZS
			iLeaveReason = enLeaveReason_AnotherLogin;
			pClient->SaveToDBAndToZone();
			res.m_fullData = pClient->GetFullData();
		}
		
		pSession->SwitchAntherLoginStatus();
	}
	else if( pMsg->m_byReason == kRechangeChannel )
	{
		MODI_ASSERT( pSession->IsInRechangeChannelingStatus() );

		iLeaveReason = enLeaveReason_RechangeChannel;
		// 频道切换完成
		pSession->SwitchChannelRechangedStatus();
	}
	else if( pMsg->m_byReason == kAdminKick )
	{
		iLeaveReason = enLeaveReason_ExitGame;

		pSession->SwitchWaitBekickSaveStatus();
		bDelClient = false;
		if( MODI_GameTask::ms_pInstancePtr )
		{
			MODI_GW2GS_DelSession 	delSession;
			delSession.m_sessionID = pSession->GetSessionID();
			MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 
		}
	}

	// 返回结果给zs
	res.m_byResult = MODI_S2ZS_Request_KickSbResult::enFound;
	pZoneServer->SendCmd( &res , sizeof(res) );

	Global::logger->info( "[%s] kick client <rolename=%s,charid=%u> sussccful" ,
			KICK_CLIENT ,
			pClient->GetRoleName(),
			GUID_LOPART(pMsg->m_guid) );

	if( bDelClient )
	{
		// 客户端离开频道
		pChannel->LeaveChannel( pClient , iLeaveReason  );
		delete pClient;
		MODI_ASSERT( pSession->GetClientAvatar() == 0 );

	}

	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::RemakePP_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ADB2S_Notify_ReMakePPResult * pMsg = (const MODI_ADB2S_Notify_ReMakePPResult *)(pt_null_cmd);

	MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find( pMsg->m_nSessionID  );
	if( !pSession )
		return enPHandler_OK;

	MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
	if( !pClient )
		return enPHandler_OK;

	bool bKick = false;
	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer )
		bKick = true;

	MODI_GS2C_Respone_OptResult result;
	result.m_nResult = SvrResult_Login_RechangeChannelFaild;
	if( !pMsg->m_bySuccessful )
	{
		bKick = true;
	}

	if( bKick )
	{
		// kick
		Global::logger->info("[%s] client<rolename=%s,charid=%u> remake P.P faild .zoneserver ptr=%p" , 
			LOGIN_CHANGECHANNEL  , 
			pClient->GetRoleName() ,
			pClient->GetCharID(),
		   	pZoneServer );

		pSession->SwitchWaitBekickSaveStatus();

		if( MODI_GameTask::ms_pInstancePtr )
		{
			// 让网关删除该客户端的连接
			MODI_GW2GS_DelSession 	delSession;
			delSession.m_sessionID = pSession->GetSessionID();
			MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 
		}

		return enPHandler_OK;
	}

	// 可以切换频道
	result.m_nResult = SvrResult_Login_RechangeChannelOk;
	pClient->SendPackage( &result , sizeof(result) );

	pClient->SaveToDBAndToZone();
	// 通知zs，有人请求切换频道
	MODI_S2ZS_Request_RechangeChannel msg2zs;
	msg2zs.m_guid = pClient->GetGUID();
	msg2zs.m_key = pMsg->m_Key;
	msg2zs.m_fullData = pClient->GetFullData();
	pZoneServer->SendCmd( &msg2zs , sizeof(msg2zs) );

	// 等待客户端选择
	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::LoadLastItemSerial_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_GUIDCreator * p = MODI_GUIDCreator::GetInstancePtr();
	p->OnLoadLastItemGUIDFromDB( (void *)pt_null_cmd );
	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::AddMoeny_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer )
		return enPHandler_OK;

	const MODI_ZS2S_Request_AddMoeny * pReq = (const MODI_ZS2S_Request_AddMoeny *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pClient = pChannel->FindClientByID( pReq->m_guid );

	MODI_ZS2S_Notify_AddMoenyRes res;
	res.m_guid = pReq->m_guid;
	res.m_reason = pReq->m_reason;
	memcpy( res.m_szData , pReq->m_szData , sizeof(pReq->m_szData) );
	if( !pClient )
	{
		res.m_result  = kZS2S_CannotFindClient;
		pZoneServer->SendCmd( &res, sizeof(res) );
		return enPHandler_OK;
	}

	// 增加游戏货币
	pClient->IncMoney( pReq->m_nMoeny );
	res.m_result = kZS2S_Successful;
	pZoneServer->SendCmd( &res, sizeof(res) );
	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::AddItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer )
		return enPHandler_OK;

	const MODI_ZS2S_Request_AddItem * pReq = (const MODI_ZS2S_Request_AddItem *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pClient = pChannel->FindClientByID( pReq->m_guid );

	MODI_ZS2S_Notify_AddItemRes res;
	res.m_guid = pReq->m_guid;
	res.m_reason = pReq->m_reason;
	res.m_transid = pReq->m_transid;
	memcpy( res.m_szData , pReq->m_szData , sizeof(pReq->m_szData) );

	if( !pClient )
	{
		res.m_result  = kZS2S_CannotFindClient;
		pZoneServer->SendCmd( &res, sizeof(res) );
		return enPHandler_OK;
	}

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		res.m_result  = kZS2S_CannotFindClient;
		pZoneServer->SendCmd( &res, sizeof(res) );
		return enPHandler_OK;
	}
	
	MODI_Bag * p_player_bag = &(pClient->GetPlayerBag());
	if(!p_player_bag)
	{
		MODI_ASSERT(0);
		res.m_result  = kZS2S_CannotFindClient;
		pZoneServer->SendCmd( &res, sizeof(res) );
		return enPHandler_OK;
	}
	
	MODI_CreateItemInfo create_info;
	create_info.m_dwConfigId = pReq->m_nConfigID;
	create_info.m_byLimit = pReq->m_ExpireTime;
	if(pReq->m_transid == 0)
	{
		create_info.m_enCreateReason = enAddReason_SystemSend;
	}
	else
	{
		create_info.m_enCreateReason = enAddReason_Mail;
	}
	create_info.m_byServerId = pApp->GetServerID();

	/// 背包是否足够
	defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();
	const GameTable::MODI_Itemlist * pItem =  itemBin.Get(create_info.m_dwConfigId);
	if(pItem)
	{
		if(p_player_bag->GetEmptyPosCount() < 1)
		{
			/// 空间不足
			Global::logger->info("[good_mail_good] client not enough add item <name=%s>", pClient->GetRoleName());
			MODI_ASSERT(0);
			res.m_result  = kZS2S_BagFull;
			pZoneServer->SendCmd( &res, sizeof(res) );
			return enPHandler_OK;
		}
	}
	else
	{
		if(p_player_bag->GetEmptyPosCount() < pReq->m_nCount)
		{
			/// 空间不足
			Global::logger->info("[add_mail_good] client not enough add item <name=%s>", pClient->GetRoleName());
			MODI_ASSERT(0);
			res.m_result  = kZS2S_BagFull;
			pZoneServer->SendCmd( &res, sizeof(res) );
			return enPHandler_OK;
		}
	}

	for(WORD deal_count=0; deal_count<pReq->m_nCount; deal_count++)
	{
		QWORD new_item_id = 0;
		if(! p_player_bag->AddItemToBag(create_info, new_item_id))
		{
			MODI_ASSERT(0);
			Global::logger->error("[add_mail_goods] failed <reason=%d,configid=%u,count=%u,deal_count=%u>",
								  (WORD)(create_info.m_enCreateReason), create_info.m_dwConfigId, pReq->m_nCount,
								  deal_count);
			break;
		}
		
		Global::logger->debug("[get_attach_item] successful one <name=%s,accid=%u, new_item_id=%llu>",
							  pClient->GetRoleName(), pClient->GetAccountID(), new_item_id);
		
		/// 为了增加购买记录，钱为0 但其实钱早就扣了
		/// 应该是扣钱时候就把道具生成但不激活，等玩家获取附件再激活
		/// 要实现上述必须改变邮件获取附件的方法
		if(pReq->m_transid != 0)
		{
			/// 记录到交易库中
			std::ostringstream os;
			os<< pReq->m_transid;
			MODI_Record record_insert;
			record_insert.Put("itemid", new_item_id);
			record_insert.Put("accid", pClient->GetAccountID());/// 先这样，后期这条记录是在扣钱的时候就插入到库中了,记录的应该是扣钱人的accid
			record_insert.Put("buy_serial",os.str());
			record_insert.Put("buy_action",create_info.m_enCreateReason);
			record_insert.Put("money_rmb", 0);
			record_insert.Put("money", 0);
	
			MODI_RecordContainer record_container;
			record_container.Put(&record_insert);
			MODI_ByteBuffer result(1024);
			if(! MODI_DBClient::MakeInsertSql(result, GlobalDB::m_pBuyrecordTbl, &record_container))
			{
				Global::logger->debug("[save_give_item_faild] make save buy_recod sql failed <itemid=%llu,buy_serial=%s,buy_action=%d>",
									  new_item_id, os.str().c_str(), create_info.m_enCreateReason);
				MODI_ASSERT(0);
			}
			else
			{
				MODI_GameServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
			}		
		}
		if( create_info.m_dwConfigId == 56001 || create_info.m_dwConfigId == 56002)
		{
			if( pClient->IsVip())
			{
				MODI_ItemOperator::UseItem(pClient, new_item_id, pClient->GetGUID());
			}
		}	
	
		MODI_TriggerBuyItem  script(create_info.m_dwConfigId);
		MODI_BuyItemManager::GetInstance().Execute(pClient,script);
	}

	
	res.m_result = kZS2S_Successful;
	pZoneServer->SendCmd( &res , sizeof(res) );
	return enPHandler_OK;

}

int MODI_PackageHandler_gs2zs::QueryHasCharResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_QueryHasCharResult_Shop * p_recv_cmd = (const MODI_ZS2S_Notify_QueryHasCharResult_Shop *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
    MODI_GUID clientguid = MAKE_GUID(p_recv_cmd->m_queryClient , enObjType_Player);
    MODI_ClientAvatar * p_client = pChannel->FindClientByID(clientguid);
    if(!p_client)
	{
		MODI_ASSERT(0);
        return enPHandler_OK;
	}
	MODI_GameShop * pShop = p_client->GetShop();
	if(!pShop)
	{
		/// 不可能这么快离开
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	pShop->GiveItemToOther(p_recv_cmd, 0);
	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::OnBillTransResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	//const MODI_RetConsumeMoneyCmd * pResult = (const MODI_RetConsumeMoneyCmd *)(pt_null_cmd);
	//MODI_GameShop::OnBillTranscationResult( pResult );
	return enPHandler_OK;
}

int MODI_PackageHandler_gs2zs::OnResetTodayOnlineTime_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	class  MODI_ResetCallBack :public MODI_ChannelClientCallBack
	{
		public:

		MODI_ResetCallBack() {}
		virtual ~MODI_ResetCallBack (){}
		virtual bool Done(MODI_ClientAvatar * p_client) 
		{
			if( p_client )
			{
				p_client->ResetTodayOnlineDay( m_new );
				return true;
			}
			return false;
		}

		DWORD 	m_new;
	};

	const MODI_ZS2S_Notify_ResetOnlineTime * pMsg = (const MODI_ZS2S_Notify_ResetOnlineTime *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_ResetCallBack  callback;
		callback.m_new = pMsg->m_todayOnlineTime;
		pChannel->AllClientExcuteCallback( callback );
	}
	return enPHandler_OK;
}
	
int MODI_PackageHandler_gs2zs::OnExchangeYYKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_ExchangeByYunyingKeyRes * pRes = ( const MODI_RDB2S_Notify_ExchangeByYunyingKeyRes  *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pChannel && pZone )
	{
		MODI_GUID guid = MAKE_GUID( pRes->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			MODI_GS2C_Notify_ExchangeYunYingKeyRes res2c;
			res2c.m_result = pRes->m_result;
			if( pRes->m_result == kYYKeyExchange_Successful )
			{
				MODI_YunYingKeyExchange * pYY = MODI_YunYingKeyExchange::GetInstancePtr();
				if( pYY )
				{
					MODI_GoodsPackageArray gpa;
					enYunYingKeyExchangeResult res = pYY->Exchange( pClient ,  pRes->m_szKey , pRes->m_content.m_szContent  , 
							strlen(pRes->m_content.m_szContent) , gpa );
					if( res == kYYKeyExchange_Successful )
					{
						MODI_S2RDB_Request_SaveYunYingKey syykey;
						syykey.m_charid = pClient->GetCharID();
						memcpy( syykey.m_szKey  , pRes->m_szKey , sizeof(pRes->m_szKey) );
						pZone->SendCmd(&syykey,sizeof(syykey));
					}
					res2c.m_result = res;
				}
			}
			pClient->SendPackage(&res2c,sizeof(res2c));
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::RefreshMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_RefreshMoney * pMsg = (const MODI_ZS2S_Notify_RefreshMoney *)(pt_null_cmd);
	Global::logger->info("[%s] recv refresh money cmd.client<charid=%u,rmb=%u>." , SVR_TEST ,
			pMsg->m_charid , pMsg->m_RMBMoney );

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_GUID guid = MAKE_GUID( pMsg->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			if(pMsg->m_byMoneyType == kRMBMoney)
			{
				pClient->SetRMBMoney( pMsg->m_RMBMoney );
			}
			else if(pMsg->m_byMoneyType == kGameMoney)
			{
				pClient->SetMoney( pMsg->m_RMBMoney );
			}
		}
	}
	return enPHandler_OK;
}


/** 
 * @brief 改变金币
 *
 */
int MODI_PackageHandler_gs2zs::OptMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_OptMoney * pMsg = (const MODI_ZS2S_Notify_OptMoney *)(pt_null_cmd);
	Global::logger->info("[%s] recv refresh money cmd.client<charid=%u,money=%u>." , SVR_TEST ,
			pMsg->m_charid , pMsg->m_dwMoney );

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_GUID guid = MAKE_GUID( pMsg->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			if(pMsg->m_enOpt == MODI_ZS2S_Notify_OptMoney::enOptAddMoney)
			{
				pClient->IncMoney( pMsg->m_dwMoney);
			}
			else if(pMsg->m_enOpt == MODI_ZS2S_Notify_OptMoney::enOptSubMoney)
			{
				pClient->DecMoney( pMsg->m_dwMoney);
			}

			/// 附加操作
			if(pMsg->m_enReason == MODI_ZS2S_Notify_OptMoney::enReasonCFFailed)
			{
				pClient->ClearCreateFamilyTime();
			}
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::ReturnPayResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
														const unsigned int cmd_size )
{
    const MODI_GS2C_Notify_PayResult * pRes = (const MODI_GS2C_Notify_PayResult *)(pt_null_cmd);
    MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
    MODI_GUID guid = MAKE_GUID( pRes->m_client , enObjType_Player );
    MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );

    if( pClient )
		{
			pClient->SendPackage(pRes, sizeof(MODI_GS2C_Notify_PayResult));
		}

    return enPHandler_OK;
}


/// 称号改变
int MODI_PackageHandler_gs2zs::ChangeChenghao_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_ChangChenghao * pMsg = (const MODI_ZS2S_Notify_ChangChenghao *)(pt_null_cmd);
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("[chang_chenghao] recv changechenghao <charid=%u,chenghao=%u>." ,pMsg->m_charid , pMsg->m_wdChenghao);
#endif	

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_GUID guid = MAKE_GUID( pMsg->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			pClient->SetChengHao( pMsg->m_wdChenghao);
			MODI_TriggerChenghao  script(pMsg->m_wdChenghao);
			MODI_ChenghaoScriptManager::GetInstance().Execute(pClient,script);
			MODI_GS2C_Notify_ChangeChenghao send_cmd;
			send_cmd.m_PlayerID = guid;
			send_cmd.m_wdChenghao = pMsg->m_wdChenghao;
			
			if(pClient->IsInMulitRoom())
			{
				MODI_MultiGameRoom * p_room = pClient->GetMulitRoom();
				if(p_room)
				{
					p_room->Broadcast(&send_cmd, sizeof(send_cmd));
#ifdef _HRX_DEBUG
					Global::logger->debug("send changhao to multi room <chenghaoid=%u>", send_cmd.m_wdChenghao);
#endif					
				}
			}
			else
			{
				pClient->SendPackage(&send_cmd, sizeof(send_cmd));
#ifdef _HRX_DEBUG
				Global::logger->debug("send changhao to myself <chenghaoid=%u>",  send_cmd.m_wdChenghao);
#endif					
			}
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::ChenMi_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_ChenMi_Cmd * pMsg = (const MODI_ZS2S_Notify_ChenMi_Cmd *)(pt_null_cmd);
	
	Global::logger->info("[%s] recv chenmi cmd.client<charid=%u,chenmi=%u>." , SVR_TEST ,
			pMsg->m_charid , pMsg->m_enResult );

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_GUID guid = MAKE_GUID( pMsg->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			MODI_GS2C_Notify_FangChengMi fcm;
			/// 1-2 小时
			if(pMsg->m_enResult > enChenMi_None && pMsg->m_enResult < enChenMi_ThreeHour)
			{
				fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enNormal;
				//fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enTired;
				fcm.m_nTodayPlayTime = ((BYTE)(pMsg->m_enResult));
				pClient->SendPackage(&fcm,sizeof(fcm));
			}
			
			/// 3
			else if(pMsg->m_enResult == enChenMi_ThreeHour)
			{
				fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enTired;
				fcm.m_nTodayPlayTime = ((BYTE)(pMsg->m_enResult));
				pClient->SendPackage(&fcm,sizeof(fcm));
			}
			
			/// 4-5
			else if(pMsg->m_enResult >= enChenMi_FourHour && pMsg->m_enResult < enChenMi_MoreThen_FiveHour)
			{
				pClient->SetChenMiPro( 0.5f );
				pClient->SetChenMiPro( 0.5f );
				
				fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enUnHelath;
				fcm.m_nTodayPlayTime = 4;
				pClient->SendPackage(&fcm,sizeof(fcm));
			}
			
			/// >5
			else if(pMsg->m_enResult == enChenMi_MoreThen_FiveHour)
			{
				pClient->SetChenMiPro( 0.0f );
				pClient->SetChenMiPro( 0.0f );
				fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enUnHelath;
				fcm.m_nTodayPlayTime = 5;
				pClient->SendPackage(&fcm,sizeof(fcm));
			}

			/// 刚登陆时发个公告
			else if(pMsg->m_enResult == enChenMi_Begin)
			{
// 				char buf[1024];
// 				memset(buf, 0, sizeof(buf));
//     			MODI_GS2C_Notify_SystemBroast * p_notify = (MODI_GS2C_Notify_SystemBroast *) (buf);
//     			AutoConstruct(p_notify);
// 				std::string notify_content = "[防沉迷提示]您的账号处于被防沉迷状态";
// 				p_notify->m_nSize = notify_content.size();
// 				strncpy(p_notify->m_pContents, notify_content.c_str(), notify_content.size());

				fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enNotify;
				pClient->SendPackage(&fcm,sizeof(fcm));
			}
		}
	}
	return enPHandler_OK;
}


/** 
 * @brief 从数据库加载用户变量
 * 
 */
int MODI_PackageHandler_gs2zs::LoadUserVar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
											   const unsigned int cmd_size )
{
	const MODI_RDB2S_Return_LoadUserVar_Cmd * p_recv_cmd = (const MODI_RDB2S_Return_LoadUserVar_Cmd *)pt_null_cmd;	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();

	if( pChannel )
	{
		MODI_ClientAvatar * pClient = pChannel->FindByAccid(p_recv_cmd->m_dwAccountID);
		if( pClient )
		{
#ifdef _VAR_DEBUG
			Global::logger->debug("[load_user_var] load all user var from db <client=%s,size=%u>", pClient->GetRoleName(),p_recv_cmd->m_wdSize);
#endif			
			pClient->m_stGameUserVarMgr.Init(pClient, (const char * )(p_recv_cmd->m_pData), p_recv_cmd->m_wdSize);
		}
		else
		{
			MODI_ASSERT(0);
		}
		
		/// 先放这里调用
		if(pClient && (p_recv_cmd->m_blIsLoadEnd == 1))
		{
			pClient->FirstEnterGame();
		}

		///  此时就当玩家进入频道完毕
		
	}
	return enPHandler_OK;
}



/** 
 * @brief 从数据库加载用户包裹
 * 
 */
int MODI_PackageHandler_gs2zs::NotifyItemInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_ItemInfo * p_recv_cmd = (const MODI_RDB2S_Notify_ItemInfo *)pt_null_cmd;	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();

	if( pChannel )
	{
		MODI_ClientAvatar * pClient = pChannel->FindByAccid(p_recv_cmd->m_dwAccountID);
		if( pClient )
		{
#ifdef _ITEM_DEBUG
			Global::logger->debug("[notify_item_info] load item info from db <client=%s,size=%u>", pClient->GetRoleName(),p_recv_cmd->m_wdSize);
#endif			
			pClient->LoadItem(p_recv_cmd->m_pData, p_recv_cmd->m_wdSize, bool(p_recv_cmd->m_byIsLoadEnd));
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return enPHandler_OK;
}


/** 
 * @brief 加载某个角色的家族信息
 *
 */
int MODI_PackageHandler_gs2zs::LoadCFamilyInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
											   const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_FamilyInfo_Cmd * p_recv_cmd = (const MODI_ZS2S_Notify_FamilyInfo_Cmd *)pt_null_cmd;	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();

	if( pChannel )
	{
		MODI_ClientAvatar * pClient = pChannel->FindByAccid(p_recv_cmd->m_dwAccountID);
		if( pClient )
		{
			pClient->m_stCFamilyInfo = p_recv_cmd->m_stCFamilyInfo;
#ifdef _FAMILY_DEBUG
			Global::logger->debug("[load_family_data] load character family from db <client=%s,familyid=%u,position=%d,familyname=%s>",
								  pClient->GetRoleName(), pClient->m_stCFamilyInfo.m_dwFamilyID, pClient->m_stCFamilyInfo.m_enPosition,
								  pClient->m_stCFamilyInfo.m_cstrFamilyName);
#endif

			enUpDateCFReason reason = p_recv_cmd->m_enReason;

			if(reason == enCreateFamily_Reason ||
			   reason == enLogin_Reason ||
			   reason == enRequest_Reason ||
			   reason == enLeaveFamily_Reason ||
			   reason == enFireFamily_Reason ||
			   reason == enAccept_Reason)
			{
				/// 只是把家族名片信息给所有的人
				pClient->SetSendFCard(2);
			}

			if(reason == enFireFamily_Reason || reason == enRefuseFamily_Reason)
			{
				pClient->ClearRequestFamilyTime();
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return enPHandler_OK;
}


/// 广播给在家族里面的人
int MODI_PackageHandler_gs2zs::BroadcastInFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
								 const unsigned int cmd_size )
{
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	pChannel->BroadcastInFamily(pt_null_cmd, cmd_size);
	return enPHandler_OK;
}


/// 改变gmlevel
int MODI_PackageHandler_gs2zs::ModiGMLevel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
								 const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_Modi_GMLevel * pMsg = (const MODI_ZS2S_Notify_Modi_GMLevel *)(pt_null_cmd);
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( pChannel )
	{
		MODI_GUID guid = MAKE_GUID( pMsg->m_charid , enObjType_Player );
		MODI_ClientAvatar * pClient = pChannel->FindClientByID( guid );
		if( pClient )
		{
			pClient->SetGMLevel(pMsg->m_byLevel);
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::ReceiveVarChange_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{

	MODI_ZS2S_Notify_VarChanged * cmd = (MODI_ZS2S_Notify_VarChanged * ) pt_null_cmd;
	MODI_GlobalVarMgr * m_pIn = MODI_GlobalVarMgr::GetInstance();
	m_pIn->OnReceiveChange(cmd);

	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::ReceiveGlobalVarList_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ZS2S_Notify_GlobalVarList * cmd = (MODI_ZS2S_Notify_GlobalVarList *) pt_null_cmd;
	
	MODI_GlobalVarMgr * m_pIn = MODI_GlobalVarMgr::GetInstance();
	m_pIn->OnReceiveList(cmd);
	return enPHandler_OK;
}



/** 
 * @brief 加载网页道具
 * 
 */
int MODI_PackageHandler_gs2zs::NotifyWebItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_RDB2S_Ret_WebItem * p_recv_cmd = (const MODI_RDB2S_Ret_WebItem *)pt_null_cmd;	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();

	if( pChannel )
	{
		MODI_ClientAvatar * pClient = pChannel->FindByAccid(p_recv_cmd->m_dwAccountId);
		if( pClient )
		{
#ifdef _WEB_DEBUG
			Global::logger->debug("[notify_web_item] load web item from db <client=%s,size=%u>", pClient->GetRoleName(),p_recv_cmd->m_wdSize);
#endif
			MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
			MODI_GameShop * pShop = (MODI_GameShop *)(&channel->GetNormalShop());
			pShop->BuyItem(pClient, p_recv_cmd->m_pData, p_recv_cmd->m_wdSize, kTransaction_Web);
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return enPHandler_OK;
}


/** 
 * @brief 加载网页赠送道具
 * 
 */
int MODI_PackageHandler_gs2zs::NotifyWebPresent_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_RDB2S_Ret_WebPresent * p_recv_cmd = (const MODI_RDB2S_Ret_WebPresent *)pt_null_cmd;	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();

	if( pChannel )
	{
		MODI_ClientAvatar * pClient = pChannel->FindByAccid(p_recv_cmd->m_dwAccountId);
		if( pClient )
		{
#ifdef _WEB_DEBUG
			Global::logger->debug("[notify_web_item] load web present from db <client=%s,size=%u>", pClient->GetRoleName(),p_recv_cmd->m_wdSize);
#endif
			/// 直接加东西，然后把is_valid=1 or =2
			MODI_WebPresentInfo * p_startaddr = (MODI_WebPresentInfo *)(p_recv_cmd->m_pData);
			WORD item_size = p_recv_cmd->m_wdSize;
			for(WORD i = 0; i<item_size; i++)
			{
				MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
				if(!pApp)
				{
					MODI_ASSERT(0);
					return false;
				}
	
				MODI_Bag * p_player_bag = &(pClient->GetPlayerBag());
				if(!p_player_bag)
				{
					MODI_ASSERT(0);
					return false;
				}
	
				MODI_CreateItemInfo create_info;
				create_info.m_dwConfigId = p_startaddr->m_dwItemId;
				create_info.m_byLimit = p_startaddr->m_byLimit;
				create_info.m_enCreateReason = enAddReason_Task;
				create_info.m_byServerId = pApp->GetServerID();

				for(WORD deal_count=0; deal_count<p_startaddr->m_wdNum; deal_count++)
				{
					QWORD new_item_id = 0;
					if(!p_player_bag->AddItemToBag(create_info, new_item_id))
					{
						Global::logger->fatal("[add_web_present] add web present failed <serial=%llu,id=%u,num=%u,limit=%u>",
											  p_startaddr->m_qdSerial, p_startaddr->m_dwItemId, p_startaddr->m_wdNum, p_startaddr->m_byLimit);
						MODI_Record  record_update;
						record_update.Put( "is_valid",2);
						std::ostringstream where;
						where<<"recordserial=" <<p_startaddr->m_qdSerial;
						record_update.Put("where",where.str());
						MODI_ByteBuffer result(1024);
						if( !MODI_DBClient::MakeUpdateSql(result,GlobalDB::m_pWebPresentTbl,&record_update))
						{
							MODI_ASSERT(0);
						}
						else
						{
							MODI_GameServerAPP::ExecSqlToDB((const char * )result.ReadBuf(),result.ReadSize());
						}
						continue;
					}
					/// is_valid=0;
					Global::logger->debug("[add_web_present] add web present successful <serial=%llu,itemid=%llu>", p_startaddr->m_qdSerial, new_item_id);
					
					MODI_Record  record_update;
					record_update.Put( "is_valid",1);
					std::ostringstream where;
					where<<"recordserial=" <<p_startaddr->m_qdSerial;
					record_update.Put("where",where.str());
					MODI_ByteBuffer result(1024);
					if( !MODI_DBClient::MakeUpdateSql(result,GlobalDB::m_pWebPresentTbl,&record_update))
					{
						MODI_ASSERT(0);
					}
					else
					{
						MODI_GameServerAPP::ExecSqlToDB((const char * )result.ReadBuf(),result.ReadSize());
					}
				}
				p_startaddr++;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_gs2zs::SingleMusicList_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 

			const unsigned int cmd_size )
{

	MODI_ZS2S_Notify_SingleMusicList * list = (MODI_ZS2S_Notify_SingleMusicList *)pt_null_cmd;
	Global::logger->debug("[receve_music_list] receive music list from the server begin!!!");
	MODI_GameMusicMgr::GetInstance().Init( list->m_wNum,list->m_stMusic,list->m_eMode);
	Global::logger->debug("[receve_music_list] receive music list from the server ok!!!");
	return enPHandler_OK;
}

/** 
 * @brief INA第三方扣钱返回了
 * 
 *
 */
int MODI_PackageHandler_gs2zs::INAItemReturn_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_INAItemReturn * p_recv_cmd = (const MODI_INAItemReturn *)pt_null_cmd;
	
	MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
	MODI_GameShop *	pShop = NULL;
	if(channel)
	{
		pShop= (MODI_GameShop *)(&channel->GetNormalShop());
	}
	if(!pShop)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_ClientAvatar * p_client = channel->FindByAccid(p_recv_cmd->m_dwAccountId);
	if(! p_client)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	enShopTransactionType buy_type = p_recv_cmd->pData.m_enBuyType;

	if(p_recv_cmd->m_iResult != 100 || p_recv_cmd->m_qdTransactionId == 0)
	{
		MODI_GS2C_Notify_TransactionResult notify;
		notify.m_actionType = buy_type;
		notify.m_result = kTransaction_UnknowError;
		p_client->SendPackage(&notify, sizeof(notify));
		return enPHandler_OK;
	}

	MODI_GS2C_Notify_TransactionResult notify;
	notify.m_actionType = buy_type;
	notify.m_result = kTransaction_UnknowError;
	if(pShop->OnBuyItem(p_client, &(p_recv_cmd->pData.m_pGoods), 1, buy_type, p_recv_cmd->m_qdBuySerial))
	{
		notify.m_result = kTransaction_Successful;
	}
	p_client->SendPackage(&notify, sizeof(notify));
	return enPHandler_OK;
}


/** 
 * @brief INA第三方赠送扣钱返回了
 * 
 *
 */
int MODI_PackageHandler_gs2zs::INAGiveReturn_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_INAGiveReturn * p_recv_cmd = (const MODI_INAGiveReturn *)pt_null_cmd;
	
	MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
	MODI_GameShop *	pShop = NULL;
	if(channel)
	{
		pShop= (MODI_GameShop *)(&channel->GetNormalShop());
	}
	if(!pShop)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_ClientAvatar * p_client = channel->FindByAccid(p_recv_cmd->m_dwAccountId);
	if(! p_client)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	if(p_recv_cmd->m_iResult != 100 || p_recv_cmd->m_qdTransactionId == 0)
	{
		MODI_GS2C_Notify_TransactionResult notify;
		notify.m_actionType = kTransaction_Buy;
		notify.m_result = kTransaction_UnknowError;
		p_client->SendPackage(&notify, sizeof(notify));
		return enPHandler_OK;
	}
	

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	memcpy(buf, p_recv_cmd->m_pGiveData, p_recv_cmd->m_wdSize);
	
	const MODI_ZS2S_Notify_QueryHasCharResult_Shop * p_give_cmd = (const MODI_ZS2S_Notify_QueryHasCharResult_Shop *)buf;
	pShop->GiveItemToOther(p_give_cmd, p_recv_cmd->m_qdBuySerial);
	return enPHandler_OK;
}


/**
 *全频道广播
 */
int MODI_PackageHandler_gs2zs::BroadcastCmd_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	
	MODI_ZS2S_Broadcast_Cmd   * broad = (MODI_ZS2S_Broadcast_Cmd * ) pt_null_cmd;
#ifdef	_XXP_DEBUG
	Global::logger->debug("[broadcast_cmd]  all to broad  cmd_size=%u,allsize=%u",broad->m_nSize, cmd_size);
#endif

	MODI_GameChannel::GetInstancePtr()->Broadcast( (const Cmd::stNullCmd *)(broad->m_csCmd),(int) (broad->m_nSize));	
	return enPHandler_OK;
}

/**
 *全世界广播给某些人
 */

int MODI_PackageHandler_gs2zs::BroadcastCmdRange_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	MODI_ZS2S_Broadcast_CmdRange * range = (MODI_ZS2S_Broadcast_CmdRange *)pt_null_cmd;
	MODI_CHARID  * charid = (MODI_CHARID *)(range->m_csCmd+range->m_nSize);

#ifdef	_XXP_DEBUG
	Global::logger->debug("[broadcast_range]  memnum=%u,packetsize= %u, cmdsize=%u",range->m_nMemNum,range->m_nSize,cmd_size);
#endif
	MODI_GameChannel::GetInstancePtr()->BroadCast_Range((const Cmd::stNullCmd *)( range->m_csCmd),(int)(range->m_nSize),charid,range->m_nMemNum);
	return enPHandler_OK;

}


















