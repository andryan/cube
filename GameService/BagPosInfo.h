/**
 * @file   BagPosInfo.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 15:24:29 2011
 * 
 * @brief  包裹位置信息
 * 
 * 
 */


#ifndef _MD_BAGPOSINFO_H
#define _MD_BAGPOSINFO_H

#include "Global.h"
#include "GameItemInfo.h"
#include "protocol/gamedefine.h"

class MODI_Bag;

/**
 * @brief 包裹位置信息
 * 
 */
class MODI_BagPosInfo
{
 public:
	MODI_BagPosInfo()
	{
		m_dwConfigId = 0;
		m_wdCount = 0;
		m_enItemType = enItemType_Unknow;
	}

	~MODI_BagPosInfo()
	{
			
	}


	/** 
	 * @brief 初始化
	 * 
	 * @param pos 位置
	 * @param p_bag 哪个包裹
	 *
	 */
	void Init(const WORD & pos, MODI_Bag * p_bag)
	{
		m_stItemIdSet.clear();
		m_dwConfigId = 0;
		m_wdCount = 0;
		m_enItemType = enItemType_Unknow;
		m_wdPos = pos;
		m_pOwnBag = p_bag;
	}


	/** 
	 * @brief 获取包裹位置
	 * 
	 */
	const WORD & GetBagPos()
	{
		return m_wdPos;
	}
	
	/** 
	 * @brief 清理位置信息
	 * 
	 */
	void Clean()
	{
		m_dwConfigId = 0;
		m_wdCount = 0;
		m_enItemType = enItemType_Unknow;
		m_stItemIdSet.clear();
	}
	

	/** 
	 * @brief 获取物品类型
	 * 
	 * @return 物品类型
	 *
	 */
	const MODI_ItemType & GetItemType() const 
	{
		return m_enItemType;
	}

	/** 
	 * @brief 设置物品类型
	 * 
	 * @param item_type 物品类型
	 *
	 */
	void SetItemType(const MODI_ItemType & item_type)
	{
		m_enItemType = item_type;
	}

	/** 
	 * @brief 是否是空位置
	 * 
	 * @return 空true
	 *
	 */
	bool IsEmpty() const
	{
		if(m_wdCount > 0)
		{
			return false;
		}
		return true;
	}

	/** 
	 * @brief 获取该位置上物品的id
	 * 
	 */
	const DWORD & GetConfigId() const 
	{
		return m_dwConfigId;
	}


	/** 
	 * @brief 设置位置上的道具id
	 * 
	 * @param config_id 道具id
	 *
	 */
	void SetConfigId(const DWORD & config_id)
	{
		m_dwConfigId = config_id;
	}

	/** 
	 * @brief 获取物品个数
	 * 
	 * 
	 */
	WORD GetCount() const
	{
		return m_wdCount;
	}

	/** 
	 * @brief 增加物品个数
	 * 
	 * @param item_count 物品个数
	 *
	 */
	void AddCount(const WORD item_count)
	{
		m_wdCount += item_count;
	}

	void DelCount()
	{
		if(m_wdCount > 0)
		{
			m_wdCount--;
		}
	}
	
	/** 
	 * @brief 获取某个道具id
	 * 
	 * 
	 */
	const QWORD  GetItemId()
	{
		if(m_stItemIdSet.size() > 0)
		{
			std::set<QWORD>::iterator iter = m_stItemIdSet.begin();
			return *iter;
		}
		return 0;
	}


	/** 
	 * @brief 获取包裹
	 * 
	 * 
	 */
	MODI_Bag * GetOwnBag();


	/** 
	 * @brief 获取道具
	 */
	MODI_GameItemInfo * GetItemInfo();


	/** 
	 * @brief 增加一个道具
	 * 
	 * @param p_item 道具
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool AddItem(MODI_GameItemInfo * p_item);


	/** 
	 * @brief 删除此道具
	 * 
	 * @param p_item 道具
	 * 
	 * @return 成功ture,失败false
	 *
	 */
	bool DelItem(MODI_GameItemInfo * p_item);

 private:
	/// 是哪个位置
	WORD m_wdPos;
	
	/// 是哪个包裹
	MODI_Bag * m_pOwnBag;
	
	/// 物品属性id
	DWORD m_dwConfigId;

	/// 物品类型
	MODI_ItemType m_enItemType;

	/// 物品数量
	WORD m_wdCount;

	/// 物品id
	std::set<QWORD> m_stItemIdSet;
};

#endif
