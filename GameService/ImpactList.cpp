#include "ImpactList.h"
#include "ImpactCore.h"
#include "AssertEx.h"
#include "protocol/c2gs.h"
#include "ClientAvatar.h"
#include "GameLobby.h"
#include "ImpactTemplate.h"



MODI_ImpactList::MODI_ImpactList( MODI_ClientAvatar * pOwner ):m_pOwner(pOwner)
{
	MODI_ASSERT( m_pOwner );
}

MODI_ImpactList::~MODI_ImpactList()
{

}

bool  	MODI_ImpactList::Register( MODI_ImpactObj & impactObj )
{
	if( !m_pOwner )
	{
		MODI_ASSERT(0);
		return false;
	}

	bool bRet = false;
	if( impactObj.IsContinue() )
	{
		if( IsFull() )
		{
			// 覆盖掉一个效果
			bRet = RelpaceImpact( impactObj );
		}
		else 
		{
			// add new
			bRet = AddNew( impactObj );
		}
	}
	else 
	{
		bRet = Active( impactObj );
	}

	

	return bRet;
}

bool 	MODI_ImpactList::UnRegister( const MODI_GUID & guid )
{
	if( !m_pOwner || guid.IsInvalid() )
	{
		MODI_ASSERT(0);
		return false;
	}

	std::list<MODI_ImpactObj *>::iterator itor = m_list.begin();
	while( itor != m_list.end() )
	{	
		std::list<MODI_ImpactObj *>::iterator inext = itor;
		inext++;

		MODI_ImpactObj * impactObj = *itor;
		if( impactObj && impactObj->GetGUID() == guid )
		{
			InActivate( *impactObj );
			m_list.erase( itor );
			MODI_ImpactObjPool::Delete( impactObj );
			impactObj = 0;
		}

		itor = inext;
	}

	return true;
}

bool 	MODI_ImpactList::Active( MODI_ImpactObj & impactObj )
{
	if( !m_pOwner )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( MODI_ImpactCore::Active( impactObj , m_pOwner ) != kImpactH_Successful )
		return false;

	MODI_GS2C_Notify_Impact msg;
	BuildPackage( &msg , impactObj , MODI_GS2C_Notify_Impact::kEnable );

	SyncPackage( &msg , sizeof(msg) );

	return true;
}

bool 	MODI_ImpactList::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	if( !m_pOwner )
	{
		MODI_ASSERT(0);
		return false;
	}
	
	std::list<MODI_ImpactObj *>::iterator itor = m_list.begin();
	while( itor != m_list.end() )
	{	
		std::list<MODI_ImpactObj *>::iterator inext = itor;
		inext++;

		MODI_ImpactObj * pTemp = *itor;
		if( pTemp->GetGUID().IsInvalid() == false )
		{
			MODI_ImpactCore::Update( *pTemp , m_pOwner , 1);
		}

		itor = inext;
	}

	return true;
}

bool 	MODI_ImpactList::InActivate( MODI_ImpactObj & impactObj )
{
	if( !m_pOwner )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( MODI_ImpactCore::InActivate( impactObj , m_pOwner ) != kImpactH_Successful )
		return false;

	MODI_GS2C_Notify_Impact msg;
	BuildPackage( &msg , impactObj , MODI_GS2C_Notify_Impact::kFadeout );

	SyncPackage( &msg , sizeof(msg) );

	return true;
}

bool 	MODI_ImpactList::IsFull()
{
	if( m_list.size() >= IMPACT_MAX_COUNT )
		return true;
	return false;
}

bool 	MODI_ImpactList::IsEmpty()
{
	if( m_list.empty() )
		return true;
	return false;
}

bool 	MODI_ImpactList::AddNew( MODI_ImpactObj & impactObj )
{
	if( IsFull() )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( Active( impactObj ) )
	{
		m_list.push_back( &impactObj );
		return true;
	}

	return false;
}
		
bool 	MODI_ImpactList::RelpaceImpact( MODI_ImpactObj & impactObj )
{
	if( IsEmpty() )
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_ImpactObj * old = *m_list.begin();
	InActivate( *old );
	m_list.erase( m_list.begin() );
	if( Active( impactObj ) )
	{
		m_list.push_back( &impactObj );
		return true;
	}

	return false;
}
		
void 	MODI_ImpactList::SyncPackage( void * p , size_t nSize )
{
	if( m_pOwner )
	{
		m_pOwner->SendOrBroadcast( p , nSize );
	}
}
		
void  	MODI_ImpactList::BuildPackage( void * pmsg , MODI_ImpactObj & impactObj , int iFlag )
{
	MODI_GS2C_Notify_Impact & msg = *((MODI_GS2C_Notify_Impact *)(pmsg));
	msg.m_ImpactGUID = impactObj.GetGUID();
	msg.m_nElapsed = impactObj.GetContinuanceElapsed();
	msg.m_ContinueTime = impactObj.GetContinuance();
	msg.m_SenderGUID = impactObj.GetCasterGUID();
	msg.m_ReceiverGUID = m_pOwner->GetGUID();
	msg.m_byFlag = MODI_GS2C_Notify_Impact::enFlag(iFlag);
	msg.m_nImpactConfigID = impactObj.GetClientID();
	if( impactObj.IsContinue() )
	{
		msg.m_Type = kImpactContinue;
	}
	else 
	{
		msg.m_Type = kImpactOnce;
	}
}

void 	MODI_ImpactList::ClearAll_OutGameFadeOut()
{
	if( !m_pOwner )
	{
		return ;
	}

	std::list<MODI_ImpactObj *>::iterator itor = m_list.begin();
	while( itor != m_list.end() )
	{	
		std::list<MODI_ImpactObj *>::iterator inext = itor;
		inext++;

		MODI_ImpactObj * impactObj = *itor;
		if( impactObj && impactObj->IsOutGameFadeOut() )
		{
			InActivate( *impactObj );
			m_list.erase( itor );
			MODI_ImpactObjPool::Delete( impactObj );
			impactObj = 0;
		}
		itor = inext;
	}
}
		
void 	MODI_ImpactList::SendImpactsTo( MODI_ClientAvatar * pClient )
{
	std::list<MODI_ImpactObj *>::iterator itor = m_list.begin();
	while( itor != m_list.end() )
	{	
		std::list<MODI_ImpactObj *>::iterator inext = itor;
		inext++;

		MODI_ImpactObj * impactObj = *itor;
		if( impactObj )
		{
			MODI_GS2C_Notify_Impact msg;
			BuildPackage( &msg , *impactObj , MODI_GS2C_Notify_Impact::kContinue );
			pClient->SendPackage( &msg , sizeof(msg) );

			MODI_GS2C_Notify_ImpactModfiyData imd;
			imd.m_Impactguid = impactObj->GetGUID();
			int iRes = 0;
			MODI_ImpactTemplate * pTemplate = MODI_ImpactCore::GetImpactTempalte(  *impactObj , &iRes );
			if( pTemplate )
			{
				pTemplate->SyncModfiyData( *impactObj , pClient );
			}
			else 
			{
				MODI_ASSERT(0);
			}
		}
		itor = inext;
	}
}
