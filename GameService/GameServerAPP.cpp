#include "GameServerAPP.h"
#include "s2ms_cmd.h"
#include "s2zs_cmd.h"
#include "ImpactCore.h"
#include "Base/GameConstant.h"
#include "TaskManager.h"
#include "ScriptManager.h"
#include "MakeManager.h"
#include "GlobalDB.h"
#include "GamePackt.h"
#include "RandomBox.h"
#include "SceneManager.h"

QWORD MODI_GameServerAPP::m_qdItemId = 0;
MODI_TableStruct * MODI_GameServerAPP::m_pCharactersTbl = NULL;

bool MODI_GameServerAPP::InitChannels()
{
	const MODI_SvrGSConfig * pGSInfo = (const MODI_SvrGSConfig *)( MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_CHANNEL) );
	const MODI_SvrGSConfig::MODI_Info * pSelfInfo = pGSInfo->GetInfoByID( GetChannelIDFromArgs() );

	if( pSelfInfo )
	{
//        MODI_GameChannel * p = new MODI_GameChannel();
        if ( m_gameChannel.Create( pSelfInfo->strChannelName.c_str() , 500))
        {
//            MODI_GameChannelMgr::GetInstancePtr()->Add(p);
			return true;
        }
        else
        {
            Global::logger->fatal("[%s] Fail to create game channel.", SYS_INIT); //            delete p; 
		}
    }

	return false;
}


MODI_GameServerAPP::MODI_GameServerAPP( const char * szAppName ):MODI_IAppFrame( szAppName ),
//	m_pRDBSvrClient(0),
	m_pZoneServerClient(0),
	m_pGUIDCreator(0),
	m_pSessionMgr(0)
{
	m_nTransCount = 0;
}


MODI_GameServerAPP::~MODI_GameServerAPP()
{


}

int MODI_GameServerAPP::Init()
{
	const char * pTest = "54001,1,3;55001,1,2;";
	MODI_GoodsPackageArray gpa;
	PublicFun::StrConvToGoodsPackage( gpa , pTest , strlen(pTest) );

	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	srand(time(0));
	
	const MODI_SvrGSConfig * pGSInfo = (const MODI_SvrGSConfig *)( MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_CHANNEL ) );
	if( !pGSInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
	const MODI_SvrGSConfig::MODI_Info * pSelfInfo = pGSInfo->GetInfoByID( GetChannelIDFromArgs() );
	if( !pSelfInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
	//	SNPRINTF( m_szPidFile , sizeof(m_szPidFile) , "GameServer[%u].modipid" ,   GetChannelIDFromArgs() );
	const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
	if( !pResInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
//	const MODI_SvrMSConfig * pMsInfo = (const MODI_SvrMSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_MANGGERSERVER ) );
//	if( !pMsInfo )
//	{
//		return MODI_IAppFrame::enConfigInvaild;
//	}
	const MODI_SvrZSConfig * pZsInfo = (const MODI_SvrZSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ZONESERVER ) );
	if( !pZsInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

    Global::logger->AddLocalFileLog(pSelfInfo->strLogPath);
	std::string net_log_path = pSelfInfo->strLogPath + ".net";
#ifdef _FB_LOGGER
	std::string fb_log_path = pSelfInfo->strLogPath + ".fb";
	Global::fb_logger->AddLocalFileLog(fb_log_path.c_str());
	Global::fb_logger->RemoveConsoleLog();
#endif

 	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();


	if( !m_PackageHandler_c2gs.Initialization() ||
//		!m_PackageHandler_gs2ms.Initialization() || 
		!m_PackageHandler_gs2zs.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}

	if( !m_gmCmdHandler.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}


	m_strServerName = pSelfInfo->strChannelName;
	this->SetWorldID( 1 );
	this->SetServerID( GetChannelIDFromArgs() );

	// world id 暂时写1
	m_pGUIDCreator = new MODI_GUIDCreator( GetWorldID() , GetServerID() ); // guid 生成器
	m_pSessionMgr = new MODI_ClientSessionMgr();
	if( !m_pGUIDCreator || !m_pSessionMgr )
		return MODI_IAppFrame::enNoMemory;

	Global::logger->info("[%s]  Loading Bin Files ...", SYS_INIT);
	Global::logger->info("[server_debug], size of fulldata is %d , sizeof relation is %d ",sizeof(MODI_CharactarFullData),sizeof(MODI_DBRelationInfo));
    // 初始化bin mgr
    if (!MODI_BinFileMgr::GetInstancePtr()->Initial(pResInfo->strBinDir.c_str(),
													enBinFT_Chenghao))
    {
        Global::logger->fatal("[%s] Fail to initialize BIN file manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
    }



    ////	当初始化完成BinFileManager后立刻初始化礼包
	if( !MODI_DynamicPackt::GetInstance()->Init())
	{
        Global::logger->fatal("[%s] Fail to initialize Dynamic Package manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}


	if(!MODI_RandomBoxMgr::GetInstance()->Init())
	{
		Global::logger->fatal("[system_init] init randombox failed");
		return MODI_IAppFrame::enLoadResourceFaild;
	}
		
		
	Global::logger->info("[%s]  Loading Mdm Files ...", SYS_INIT);
	// 初始化mdm mgr
	if( !MODI_MdmMgr::GetInstancePtr()->Initial(pResInfo->strMdmDir.c_str()))
	{
        Global::logger->fatal("[%s]  [%s] Fail to initialize MDM file manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	Global::logger->info("[%s]  Checking Mdm Files ...", SYS_INIT);
	// 检查MDM文件是否完整
	if( !MODI_MdmMgr::GetInstancePtr()->CheckResource( MODI_BinFileMgr::GetInstancePtr() ) )
	{
		Global::logger->fatal("[%s]  [%s] Fail to check MDM file." , SYS_INIT );
		return MODI_IAppFrame::enLoadResourceFaild;
	}

//	if( !m_DefaultCreator.Init() )
//	{
//      Global::logger->fatal("[%s] Fail to initialize Default Avatar Creator faild.", SYS_INIT);
//		return MODI_IAppFrame::enLoadResourceFaild;
//	}

    //	根据配置初始化频道
    if( !InitChannels() )
	{
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	m_ImpactTMgr.Initial();

	if( !MODI_ImpactCore::CheckConfigFile() )
	{
        Global::logger->fatal("[%s] Fail to chheck impactcore configfile .", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	if(! MODI_SceneManager::GetInstance().Init())
	{
		Global::logger->fatal("[load_scene] load scene failed");
		return false;
	}
	
//	// 连接ROLE DB SERVER
//	m_pRDBSvrClient = new MODI_RDBClient_GS( "RoleDBServerClient" , pRdbInfo->strIP.c_str() , pRdbInfo->nPort );
//	if( !m_pRDBSvrClient )
//	{
//		return MODI_IAppFrame::enNoMemory;
//	}
//
	m_pZoneServerClient = new MODI_ZoneServerClient( "ZoneServerClient" , pZsInfo->strIP.c_str() , pZsInfo->nPort );
	if( !m_pZoneServerClient )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_GameTick();
	if( !m_pLogicThread )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	// 创建网络服务对象
	m_pNetService = new MODI_GameService();
	if( !m_pNetService )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_GameServerAPP::Run()
{
	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

	if( m_pZoneServerClient  )
	{
		Global::logger->info("[%s]try connect to zone server." , SYS_INIT );
		if( !m_pZoneServerClient->Init() )
		{
			Global::logger->info("[%s]connect to zone server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pZoneServerClient->Start();
			Global::logger->info("[%s]connect to zone server successful. " , SYS_INIT );
		}
	}
	else 
	{
		Global::logger->info("[%s]connect to zone server faild. not exist zoneserver client. " , SYS_INIT );
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	sleep(2);


	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );


	/// 数据库初始化
	if(! MODI_DBManager::GetInstance().Init(Global::g_stURLMap["gamedb"]))
	{
		Global::net_logger->debug("not connect gamedb <%s>", Global::g_stURLMap["gamedb"].c_str());
		return MODI_IAppFrame::enConfigInvaild;
	}

	m_pCharactersTbl = MODI_DBManager::GetInstance().GetTable("characters");
	if(!m_pCharactersTbl)
	{
		Global::logger->fatal("[system_init] unable get characters table struct");
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	if(!MODI_GlobalDB::Init())
	{
		return false;
	}
	/// 获取最大的itemid
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	if(!p_db_client)
	{
		MODI_ASSERT(0);
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	MODI_RecordContainer select_container;
	MODI_Record select_record;
	std::string select_field = "MAX(`itemid`)";
	select_record.Put(select_field);
	std::ostringstream where;
	where<< "server_id=" << (WORD)GetServerID();

	int rec_num = p_db_client->ExecSelect(select_container, GlobalDB::m_pItemInfoTbl, where.str().c_str(), &select_record);
	if(rec_num == 1)
	{
		MODI_Record * get_record = select_container.GetRecord(0);
		const MODI_VarType & v_max_itemid = get_record->GetValue(select_field);
		if(v_max_itemid.Empty())
		{
			QWORD server_id = GetServerID();
			QWORD base_id = (DWORD() - 1);
			Global::g_qdItemId = 8 * server_id * base_id;
		}
		else
		{
			Global::g_qdItemId = (unsigned long long)v_max_itemid;
		}
		Global::logger->debug("[get_max_itemid] <itemid=%llu>", Global::g_qdItemId);
	}
	else
	{
		MODI_ASSERT(0);
		return MODI_IAppFrame::enConfigInvaild;
	}

	MODI_DBManager::GetInstance().PutHandle(p_db_client);

	
	///  脚本加载
	if (!MODI_TaskManager::GetInstance().Init())
	{
		MODI_ASSERT(0);
		return false;
	}

	if( m_pLogicThread )
	{
		// 启动逻辑线程
		m_pLogicThread->Start();
	}
	else 
	{
		Global::logger->info("[%s] start game server faild. logic thread not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistLogicThread;
	}
	
	
	if( m_pNetService )
	{
		m_pNetService->Main();
	}
	else 
	{
		Global::logger->info("[%s] start game server faild. net service moudle not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistNetService;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_GameServerAPP::Shutdown()
{
	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

//	if( m_pRDBSvrClient )
//	{
//		m_pRDBSvrClient->TTerminate();
//		m_pRDBSvrClient->Join();
//	}

	if( m_pZoneServerClient )
	{
		m_pZoneServerClient->TTerminate();
		m_pZoneServerClient->Join();
	}

	if( m_pNetService )
	{
		m_pNetService->Terminate();
	}

	// 注意删除的先后次序

	m_gameChannel.Destory();

	delete m_pLogicThread;

	delete m_pNetService;

//	delete m_pRDBSvrClient;

	delete m_pGUIDCreator;

	delete m_pSessionMgr;

	m_Channellist.Clear( true );

	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}




/** 
 * @brief 请求执行sql
 * 
 * @param sql sql语句
 * @param sql_len 语句长度
 *
 */
void MODI_GameServerAPP::ExecSqlToDB(const char * sql, const WORD sql_len)
{
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if(! pZone )
	{
		Global::logger->info("[request_exec_sql] can't get zone service client");
		return;
	}

	char cmd_buf[sql_len + sizeof(MODI_S2RDB_Request_ExecSql_Cmd) + 2];
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_S2RDB_Request_ExecSql_Cmd * p_send_cmd = (MODI_S2RDB_Request_ExecSql_Cmd *)cmd_buf;
	AutoConstruct(p_send_cmd);
	memcpy(p_send_cmd->m_Sql, sql, sql_len);
	p_send_cmd->m_wdSize = sql_len;
	pZone->SendCmd(p_send_cmd, p_send_cmd->GetSize());
}
