/**
 * @file   Process.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 12:25:39 2011
 * 
 * @brief  任务执行体
 * 
 * 
 */


#ifndef _MD_PROCESS_H
#define _MD_PROCESS_H

#include "Global.h"
#include "Condition.h"
#include "Action.h"

class MODI_ClientAvatar;

/**
 * @brief 任务执行体
 * 
 */
class MODI_Process
{
 public:
	typedef std::vector<MODI_Condition * > defConditionVec;
	typedef std::vector<MODI_Condition * >::iterator defConditionVecIter;
	typedef std::vector<MODI_Action * > defActionVec;
	typedef std::vector<MODI_Action * >::iterator defActionVecIter;

	MODI_Process(const WORD & process_id): m_wdProcessID(process_id){}

	~MODI_Process()
	{
		Final();
	}

	/** 
	 * @brief 执行体解析
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool Init();

	/** 
	 * @brief 执行
	 * 
	 * @return 成功
	 *
	 */
	bool Execute(MODI_ClientAvatar * p_client);


	/** 
	 * @brief 是否可以执行
	 * 
	 */
	bool IsCanExecute(MODI_ClientAvatar * p_client);


	/** 
	 * @brief 获取过程id
	 * 
	 * 
	 */
	const WORD & GetProcessID()
	{
		return m_wdProcessID;
	}

	void AddCondition(MODI_Condition * p_condition)
	{
		m_stConditionVec.push_back(p_condition);
	}

	void AddAction(MODI_Action * p_action)
	{
		m_stActionVec.push_back(p_action);
	}
	
 private:

	/// 必须释放
	void Final();
	
	/// 过程id
	WORD m_wdProcessID;

	/// 条件集合
	defConditionVec m_stConditionVec;

	/// 过程集合
	defActionVec m_stActionVec;
};

#endif
