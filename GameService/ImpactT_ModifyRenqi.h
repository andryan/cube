/** 
 * @file ImpactT_ModifyMoney.h
 * @brief 使用常量修改游戏币
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-09
 */

#ifndef MODI_IMPACT_RENQI_H_
#define MODI_IMPACT_RENQI_H_

#include "ImpactTemplate.h"

/** 
 * @brief 按系数修改经验
 */
class 	MODI_ImpactT_ModifyRenqi : public MODI_ImpactTemplate
{
	enum
	{
		kArg_ModifyConst = 0,
	};

	public:

		MODI_ImpactT_ModifyRenqi();
		virtual ~MODI_ImpactT_ModifyRenqi();

	public:

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetModifyConst( MODI_ImpactObj & impactObj , long lValue );
		long 	GetModifyConst( MODI_ImpactObj & impactObj ) const;

};

#endif
