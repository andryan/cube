#include "GameTask.h"
#include "gw2gs.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "ClientAvatar.h"
#include "PackageHandler_c2gs.h"
#include "GameChannelMgr.h"
#include "GameChannel.h"
#include "Mutex.h"
#include "protocol/c2gs_cmddef.h"
#include "protocol/c2gs.h"
#include "s2ms_cmd.h"
#include "s2adb_cmd.h"
#include "Share.h"
#include "ServerConfig.h"
#include "AssertEx.h"
#include "GameTick.h"
#include "GameServerAPP.h"
#include "s2zs_cmd.h"
#include "s2rdb_cmd.h"
#include "s2rdb_cmd.h"
#include "Base/HelpFuns.h"



MODI_GameTask * MODI_GameTask::ms_pInstancePtr = 0;
MODI_SvrPackageProcessBase 	MODI_GameTask::ms_ProcessBase;

MODI_CmdFlux MODI_GameTask::m_stRecvCmdFlux("gametask_recv");
MODI_CmdFlux MODI_GameTask::m_stSendCmdFlux("gametask_send");

MODI_GameTask::MODI_GameTask(const int sock, const struct sockaddr_in *addr):MODI_IServerClientTask(sock, addr)
{
	Resize(GWTOGS_CMDSIZE);
	m_stLifeTime.GetNow();
}

MODI_GameTask::~MODI_GameTask()
{

}

void MODI_GameTask::OnAddSession(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    MODI_GW2GS_AddSession & addNotify = *((MODI_GW2GS_AddSession *) (pt_null_cmd));

	//	处理
    MODI_ClientSession * pSession = new MODI_ClientSession(addNotify.m_sessionID, this);

	Global::logger->debug("[%s] add a session(%s)", "add_session", addNotify.m_sessionID.ToString() );

    if (pSession)
    {
        if (MODI_ClientSessionMgr::GetInstancePtr()->Add(pSession))
        {
			// 等待验证
			pSession->SwitchWaitAuthStatus();
        }
		else 
		{
			Global::logger->error("[%s] dup session<%llu> id , can't add to session mgr. " , SVR_TEST , 
					pSession->GetSessionID().ToString() );
			MODI_ASSERT(0);
			delete pSession;
		}
    }
}

void MODI_GameTask::OnDelSession(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    MODI_GW2GS_DelSession & delNotify = *((MODI_GW2GS_DelSession *) (pt_null_cmd));
    MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find(
            delNotify.m_sessionID);

#ifdef _SOCKET_DEBUG
	Global::logger_send->debug("[%s] del a session<%s>", GAME_SESSION , 
			delNotify.m_sessionID.ToString());
#endif			

    if (pSession)
    {
		Global::logger->debug("[%s] find session , and delete session<%s> 's status<%s> . " , 
				GAME_SESSION ,
				delNotify.m_sessionID.ToString(),
				pSession->GetStatusStringFormat());

        MODI_ClientAvatar * pClientAvatar = pSession->GetClientAvatar();
	
		if( pClientAvatar == NULL )
		{
//			MODI_ASSERT( pSession->IsInWaitAuthStatus() ||
//						pSession->IsInWaitAuthResultStatus() );
			delete pSession;
			return ;
		}

		if( pClientAvatar->IsLogoutNormal())
		{
			Global::g_nNormalLogout++;
		}
		else
		{
			Global::g_nExcepLogout++;
			Global::logger->debug("[ Excep Logout] <ip=%s,accid=%u,logintime=%u,name=%s>",pClientAvatar->GetSessionID().GetIPString(),pClientAvatar->GetAccountID(),pClientAvatar->GetFullData().m_roleExtraData.m_nLastLoginTime,pClientAvatar->GetRoleName());
		}
		MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
		DISABLE_UNUSED_WARNING( pChannel );

		bool bSave = false;
		bool bLoginOut = false;
		if( pSession->IsInWaitAuthStatus() ||
			pSession->IsInWaitAuthResultStatus() )
		{
			// 未进入游戏逻辑的状态
			bSave = false; // 未进入游戏逻辑，可以不保存
		}
		if( pSession->IsInAuthedWaitMusicHeaderStatus() )
		{
			bSave = false;
			bLoginOut = true;
		}
		else if( pSession->IsInLoginedStatus() )
		{
		
			// 游戏中退出，需要保存
			bSave = true;
			bLoginOut = true;
		}
		else if( pSession->IsInRechangeChannelingStatus() )
		{
			// 正在切换频道时退出
			bSave = true;
			bLoginOut = true;
		}
		else if( pSession->IsInChannelRechangedStatus() )
		{
			bSave = false;
		}
		else if( pSession->IsInAntherLoginStatus() )
		{
			bSave = false;
			bLoginOut = false;
		}
		else if( pSession->IsInWaitBekickStatus() )
		{
			bSave = false;
			bLoginOut = true;
		}
		else if( pSession->IsInWaitBekickSaveStatus() )
		{
			bSave = true;
			bLoginOut = true;
		}

		if( pClientAvatar )
		{
			pChannel->LeaveChannel( pClientAvatar , enLeaveReason_ExitGame );

			pClientAvatar->SaveToDBAndToZone();

			if( bLoginOut )
			{
				MODI_S2ZS_Request_ClientLoginOut msg2zs;
				msg2zs.m_guid = pClientAvatar->GetGUID();
				msg2zs.m_sessionID = pClientAvatar->GetSessionID();
				msg2zs.m_fullData = pClientAvatar->GetFullData();
				MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
				pZoneServer->SendCmd( &msg2zs , sizeof(msg2zs) );
			}
		}

		delete pSession;
    }
	else 
	{
		// 该情况存在，并且合理
		//MODI_ASSERT(0);
	}
}

/// 处理网关命令
int MODI_GameTask::OnHandlerGatewayCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	int iRet = enPHandler_OK;
	switch (pt_null_cmd->byCmd)
	{
		//	SESSION 相关操作
		case MCMD_GW2GS_SESSION:
		{
			if (pt_null_cmd->byParam == SCMD_GW2GS_ADD)
			{
				OnAddSession(pt_null_cmd, cmd_size);
			}
			else if (pt_null_cmd->byParam == SCMD_GW2GS_DEL)
			{
				OnDelSession(pt_null_cmd, cmd_size);
			}
		}
		break;
		default:
		{
			Global::logger->debug("[接收命令] 接收到非法的Gateway命令");
		}
		break;
	}

	return iRet;
}

/// 处理游戏命令
int MODI_GameTask::OnHandlerGameClientCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
    MODI_GW2GS_Redirectional & redirectionalPackage = *((MODI_GW2GS_Redirectional *) (pt_null_cmd));
	//	find session
	MODI_ClientSession * pSession = MODI_ClientSessionMgr::GetInstancePtr()->Find(
			redirectionalPackage.m_sessionID);

	// check vaild
	if( !pSession )
	{

		return enPHandler_OK;
	}
	if( redirectionalPackage.m_nSize < 2 )
	{

		return enPHandler_OK;
	}

	const Cmd::stNullCmd * pClientCmd = (const Cmd::stNullCmd *) (&redirectionalPackage.m_pData[0]);

	int iRet = enPHandler_Kick;

	if( pSession->IsInLoginedStatus() )
	{
		// 登入状态，进行逻辑处理

		MODI_ClientAvatar * pClient = 0;

		MODI_BEGIN_TRYCATCH
		pClient = pSession->GetClientAvatar();
		if (!pClient)
		{
			MODI_ASSERT(0);
			throw "GameSession没有绑定ClientAvatar对象.";
		}
		MODI_END_TRYCATCH

		if( pClient )
		{
			//	游戏命令的处理
			iRet = MODI_PackageHandler_c2gs::GetInstancePtr()->DoHandlePackage(
							pClientCmd->byCmd, pClientCmd->byParam, pClient, pClientCmd,
							redirectionalPackage.m_nSize);
		}
	}
	else if( pSession->IsInWaitAuthStatus() )
	{
		// 客户端等待验证
		iRet = MODI_SessionStatusHandler::OnHandler_LoginAuth( pSession , 
				pClientCmd , 
				redirectionalPackage.m_nSize );
	}
	else if( pSession->IsInAuthedWaitMusicHeaderStatus() )
	{
		// 验证成功，等待发送音乐头数据状态
		iRet = MODI_SessionStatusHandler::OnHandler_MusicHeader( pSession , 
				pClientCmd , 
				redirectionalPackage.m_nSize );
	}
	else if( pSession->IsInRechangeChannelingStatus() )
	{
		// 正在切换频道状态
		iRet = MODI_SessionStatusHandler::OnHandler_SelectChannel( pSession ,
				pClientCmd , 
				redirectionalPackage.m_nSize );
	}
	else if( pSession->IsInChannelRechangedStatus() )
	{
		// 
		MODI_ClientAvatar * pClient = pSession->GetClientAvatar();
		if( pClient )
		{
			Global::logger->warn("[%s] client<charid=%u,name=%s> is rechanged channel status , but send logic cmd." , 
					SVR_TEST , 
					pClient->GetCharID(),
					pClient->GetRoleName() );
		}
	}

	return iRet;
}


bool MODI_GameTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
#ifdef _CMD_FLUX		
	MODI_GW2GS_Redirectional * rev_cmd = (MODI_GW2GS_Redirectional *)pt_null_cmd;
	Cmd::stNullCmd * null_cmd = (Cmd::stNullCmd *)(rev_cmd->m_pData);
	m_stRecvCmdFlux.Put(null_cmd, rev_cmd->m_nSize);
#endif				
    Put(pt_null_cmd, cmd_size);
    return true;
}

/**
 * @brief 消息处理 
 *
 * @param pt_null_cmd 处理的消息
 * @param cmd_size 消息大小
 *
 * @return 处理成功true, 处理失败false
 *
 */

bool MODI_GameTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if( !pt_null_cmd || cmd_size < sizeof(Cmd::stNullCmd) )
    {
        return false;
    }

    ///	是否网关命令
    bool bGwCmd = true;
    if (pt_null_cmd->byCmd == MCMD_SVR_REDIRECTIONAL)
        bGwCmd = false;

	int iRet = enPHandler_OK;
    if (bGwCmd)
    {
		iRet = OnHandlerGatewayCmd( pt_null_cmd , cmd_size );
    }
    else
    {
		iRet = OnHandlerGameClientCmd( pt_null_cmd , cmd_size );
	}

	if( iRet == enPHandler_Kick )
	{

	}
	else if( iRet == enPHandler_Warning )
	{

	}
	else if( iRet == enPHandler_Ban )
	{

	}

    return true;
}

/// 回收连接
int MODI_GameTask::RecycleConn()
{
	if( this->IsCanDel() )
		return 1;
	return 0;
}

// 连接上的回调,可以安全的在逻辑线程调用
void MODI_GameTask::OnConnection() 
{
	if( ms_pInstancePtr )
	{	
		// GS和网关是1对1模式
		Global::logger->error("[%s] another gateway connectioned , close connection." , SYS_INIT );
		this->Terminate();
		return ;
	}

	ms_pInstancePtr = this;

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)(MODI_GameServerAPP::GetInstancePtr());

	const MODI_SvrGSConfig * pConfig = (const MODI_SvrGSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_CHANNEL ) );
	const MODI_SvrGSConfig::MODI_Info * pSelfInfo  = pConfig->GetInfoByID( GetChannelIDFromArgs() );
	MODI_ASSERT( pSelfInfo );

	// reg self to zoneserver
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_S2ZS_Request_RegSelf msg;
		msg.m_nWorldID = pApp->GetWorldID();
		msg.m_ServerID = pApp->GetServerID();
		safe_strncpy( msg.m_szServerName , pApp->GetServerName() , sizeof(msg.m_szServerName) );
		unsigned int nIP = PublicFun::StrIP2NumIP( pSelfInfo->strGatewayWanIP.c_str() );
		msg.m_nGatewayIP = nIP;
		msg.m_nGatewayPort = pSelfInfo->nGatewayWanPort;
		pZone->SendCmd( &msg , sizeof(msg) );
		
		Global::logger->info("[%s] gameserver address<ip=%s,port=%u> , gateway address<ip=%s,port=%u> , channel<id=%u> . " , 
			SVR_TEST ,
			pSelfInfo->strIP.c_str() , pSelfInfo->nGatewayPort , 
			pSelfInfo->strGatewayIP.c_str() , pSelfInfo->nGatewayPort , 
			pSelfInfo->nChannelID );


		Global::logger->info("[%s] gameserver try load lastitemserial from db." , SVR_TEST);
	}
}

// 断开的回调，可以安全的在逻辑线程调用
void MODI_GameTask::OnDisconnection() 
{
	ms_pInstancePtr = 0;
	Global::logger->info("[%s] Disconnection with Gate Server. GameServer will Terminate. " , 
			ERROR_CON );

	// shut down gameserver 
	MODI_GameService * pService = MODI_GameService::GetInstancePtr();
	pService->Terminate();
}


void 	MODI_GameTask::ProcessAllPackage()
{
	ms_ProcessBase.ProcessAllPackage();
}


