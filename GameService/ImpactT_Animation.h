/** 
 * @file ImpactT_Animation.h
 * @brief 播放动画效果
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-24
 */
#ifndef MODI_IMPACT_ANIMATION_H_
#define MODI_IMPACT_ANIMATION_H_

#include "ImpactTemplate.h"

class 	MODI_ImpactT_Animation : public MODI_ImpactTemplate
{
		enum
		{
			kArg_ModfiyScore = 0,
			kArg_ModfiyRenqi = 1,
		};

	public:
		MODI_ImpactT_Animation();
		virtual ~MODI_ImpactT_Animation();

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData );

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetModfiyScore( MODI_ImpactObj & impactObj , long lValue );
		long 	GetModfiyScore( const MODI_ImpactObj & impactObj ) const;

		void 	SetModfiyRenqi( MODI_ImpactObj & impactObj , long lValue );
		long 	GetModfiyRenqi( const MODI_ImpactObj & impactObj ) const;
	   
};

#endif
