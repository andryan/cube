/**
 * @file   SceneManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Dec 15 15:39:43 2011
 * 
 * @brief  场景管理器
 * 
 * 
 */

#ifndef _MD_SCENEMANAGER_H_
#define _MD_SCENEMANAGER_H_

#include "Global.h"
#include "Scene.h"

class MODI_GameRoom;


/**
 * @brief 场景管理器
 * 
 */
class MODI_SceneManager
{
 public:
	typedef std::vector<MODI_Scene * > defSceneArray;
	static MODI_SceneManager & GetInstance();
	static void DelInstance();

	void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);

	bool Init();

	MODI_Scene * GetScene(MODI_GameRoom * p_room, const WORD scene_id);
	
 private:
	MODI_SceneManager();
	~MODI_SceneManager();
	static MODI_SceneManager * m_pInstance;

	/// 场景管理
	std::map<DWORD, defSceneArray > m_SceneMap;
};

#endif

