#include "ImpactT_AddGoodPackage.h"
#include "gamestructdef.h"
#include "ClientAvatar.h"
#include "Bag.h"
#include "GameServerAPP.h"
#include "protocol/c2gs_rolecmd.h"
#include "protocol/c2gs_itemcmd.h"
#include "ItemOperator.h"

MODI_ImpactT_AddItem::MODI_ImpactT_AddItem()
{

}
		
MODI_ImpactT_AddItem::~MODI_ImpactT_AddItem()
{

}

bool MODI_ImpactT_AddItem::CheckImpactData( GameTable::MODI_Impactlist & impactData )
{
	return true;
}

int MODI_ImpactT_AddItem::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	WORD nAddID = (WORD)impactData.get_Arg1();
	SetAddID( impactObj , nAddID );
	return kImpactH_Successful;
}

int MODI_ImpactT_AddItem::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	MODI_ClientAvatar * p_client = pObj;
	if(! p_client)
	{
		MODI_ASSERT(0);
		return 0;
	}

	MODI_Bag * p_bag = &(p_client->GetPlayerBag());
	if(! p_bag)
	{
		MODI_ASSERT(0);
		return 0;
	}

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	DWORD nConfigID = GetAddID( impactObj );
	if( nConfigID == INVAILD_CONFIGID )
	{
		MODI_ASSERT(0);
		return kImpactH_CannotFindConfig;
	}
	
	MODI_GoodsPackageArray gpa;
	if( !MODI_BinFileMgr::GetInstancePtr()->GetGoodsPackage( nConfigID , &gpa ) )
	{
		MODI_ASSERT(0);
		return kImpactH_ConfigDataFaild;
	}
	
	/// 背包是否足够
	if(p_bag->GetEmptyPosCount() < gpa.m_count)
	{
		/// 空间不足
		Global::logger->info("[good_package] client not enough add item <name=%s>", p_client->GetRoleName());
		return kImpactH_ParamFaild;
	}

	//	MODI_GS2C_Notify_AddItemByBox  msg;
	//	msg.m_count = 0;
	
	/// 增加物品
	for( BYTE n = 0; n < gpa.m_count; n++ )
	{

		MODI_CreateItemInfo create_info;
		create_info.m_dwConfigId = gpa.m_array[n].nGoodID;
		create_info.m_byLimit = gpa.m_array[n].time;
		create_info.m_enCreateReason = enAddReason_GoodPackages;
		create_info.m_byServerId = pApp->GetServerID();
		for(WORD deal_count=0; deal_count<gpa.m_array[n].nCount; deal_count++)
		{
			QWORD new_item_id = 0;
			if(MODI_ItemOperator::CheckItemSexReq(p_client, create_info.m_dwConfigId) != kUseItem_Ok)
			{
				continue;
			}
			
			/// 有男女，加失败也正常的
			p_bag->AddItemToBag(create_info, new_item_id);
			//if(!p_bag->AddItemToBag(create_info, new_item_id))
			// {
// 				Global::logger->fatal("[add_item_from_package] add item direct failed <id=%u,limit=%u>", create_info.m_dwConfigId,
// 									  create_info.m_byLimit);
// 				MODI_ASSERT(0);
// 				return kImpactH_ParamFaild;;		
// 			}
		}
	}
	
	//	p_client->SendPackage( &msg , sizeof(msg) );
	return kImpactH_Successful;
}

int MODI_ImpactT_AddItem::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	return kImpactH_Successful;
}

void 	MODI_ImpactT_AddItem::SetAddID( MODI_ImpactObj & impactObj , WORD nAddID )
{
	impactObj.SetArgDWORD( kArg_AddID , nAddID );
}

WORD 	MODI_ImpactT_AddItem::GetAddID( const MODI_ImpactObj & impactObj ) const
{
	DWORD nAddID = INVAILD_CONFIGID;
	impactObj.GetArgDWORD( kArg_AddID , nAddID );
	return nAddID;
}


