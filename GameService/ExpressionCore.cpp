#include "ExpressionCore.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"



int 	MODI_ExpressionCore::IsCanPlayExpression( MODI_ClientAvatar * pClient )
{
	// 是否装备有表情包
	MODI_Bag & bag = pClient->GetAvatarBag();
	MODI_BagPosInfo * p_pos_info = NULL;
   	p_pos_info = bag.GetPosInfo(enAvatarT_Express);
	if(!p_pos_info)
	{
		return kPExpression_UnEquip;
	}
	
	MODI_GameItemInfo * p_item;
	p_item = p_pos_info->GetItemInfo();
   	if(! p_item)
	{
		// 未装备
		return kPExpression_UnEquip;
	}

	// 状态判断
	if( pClient->IsObjStateFlag( kObjState_ChangeAvatar ) )
	{
		// 变形中
		return kPExpression_Transfroming;
	}

	if( pClient->IsPlayExpression() )
	{
		return kPExpression_Playing;
	}

	if(p_item->GetItemType() != enItemType_Express )
	{
		Global::logger->error("[play_express] client<name=%s,type=%d> request play express.but item invaild" , 
							  pClient->GetRoleName(),p_item->GetItemType() );
		MODI_ASSERT(0);
		return kPExpression_UnKnowError;
	}

	return kPExpression_Successful;
}

int 	MODI_ExpressionCore::PlayExpression( MODI_ClientAvatar * pClient , BYTE byIndex )
{
	MODI_Bag & bag = pClient->GetAvatarBag();
	MODI_BagPosInfo * p_pos_info = NULL;
   	p_pos_info = bag.GetPosInfo(enAvatarT_Express);
	if(!p_pos_info)
	{
		return kPExpression_UnEquip;
	}
	
	MODI_GameItemInfo * p_item;
	p_item = p_pos_info->GetItemInfo();
   	if(! p_item)
	{
		// 未装备
		return kPExpression_UnEquip;
	}

	pClient->OnPlayExpression(p_item->GetConfigId() , byIndex );
	return kPExpression_Successful;
}

