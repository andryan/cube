#ifndef _Tmp_SYSTEM_BROAD_H__
#define _Tmp_SYSTEM_BROAD_H__

#define	MAX_SECS	16
#include <list>
#include <string>
#include <deque>

class	MODI_TempSystemBroad
{

public:
	typedef	std::deque<std::string >    defList;
	typedef	std::deque<std::string >::iterator defItor;


	
	void	Push( std::string  &str);

	void	Update();

	static	MODI_TempSystemBroad  * GetInstance();

private:
	void	Broadcast( std::string  &string);

	static	MODI_TempSystemBroad  *m_pInstance;

	MODI_TempSystemBroad()
	{
		m_tLast = 0;
	}


	~MODI_TempSystemBroad()
	{
	}

	time_t		m_tLast;
	defList  	m_lList;


};



#endif



