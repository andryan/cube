/** 
 * @file ImpactTemplateID.h
 * @brief 效果模块的定义
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */
#ifndef MODI_IMPACT_TEMPLATEID_H_
#define MODI_IMPACT_TEMPLATEID_H_

/*

   不得随意改动此处的常量/枚举的值
   若必须改动，则需要询问相关人员（策划，主管)
*/

enum ImpactTempalteID
{
	kNullImpactTemplateID = 0,
	kImpactTemplateIDBegin = 0,

	kIT_ModfiyExpByFactor = 1, 		// 	 	按系数修改经验
	kIT_ModfiyExpByConst = 2, 		// 		使用常量修改经验
	kIT_ModifyMoneyByConst = 3, 	// 		使用常量修改钱
	kIT_PlayAnimation = 4,  		// 		播放动画
	kIT_ChangeAvatar = 5, 			// 		改变形象
	kIT_Feizi = 6, 					// 		飞字效果
	kIT_ChannelChat = 7, 			// 		频道聊天效果
	kIT_WorldChat = 8, 				// 		服务器喇叭
	kIT_FensiChat = 9, 				// 		粉丝喇叭
	kIT_AddItem = 10, 				// 		添加物品效果
	kIT_RecordMusic = 11, 			// 		录音道具
	KIT_AddVip30 = 12,			// vip 30天的效果
	KIT_AddVip90 = 13,			// vip 90天的效果
	kIT_ModifyRenqi = 14, 	// 		使用常量修改人气

	kImpactTemplateIDEnd,
	kImpactTemplateCount = kImpactTemplateIDEnd - kImpactTemplateIDBegin - 1,
};

#endif
