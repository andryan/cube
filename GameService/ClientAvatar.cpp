#include "Global.h"
#include "ClientAvatar.h"
#include "GameChannel.h"
#include "AssertEx.h"
#include "protocol/c2gs.h"
#include "s2rdb_cmd.h"
#include "GameChannelMgr.h"
#include "GUIDCreator.h"
#include "BinFileMgr.h"
#include "GameServerAPP.h"
#include "ZoneClient.h"
#include "s2zs_cmd.h"
#include "ItemOperator.h"
#include "Base/GameConstant.h"
#include "Base/HelpFuns.h"
#include "ScriptManager.h"


#define DODAY_ONLNIETIME_TIMER 1000 * 60

#define	PER_SECOND_TIMER	1000

MODI_ClientAvatar::MODI_ClientAvatar(MODI_GUID id, MODI_ClientSession * p) :
    MODI_MoveableObject(id), m_timerChangeSonger(CHANGE_SONGER_TIMER),
	m_bagAvatar( this , enBagType_Avatar ),
	m_bagPlayer( this , enBagType_PlayerBag ),
	m_impactList( this ),
	m_timerDBSave( MODI_GameConstant::get_CharacterDBSaveInterval() ),
	m_timerSyncDirtyAttr( MODI_GameConstant::get_CharacterSyncAttrInterval() ),
	m_timerPlayExpression( EXPRESS_PLAY_TIME ),
	m_timerBagUpdate( 1000 ),
	m_timerRareShop( MODI_GameConstant::get_EnterRareShopInterval() ),
	m_timerUPOnlineTime( DODAY_ONLNIETIME_TIMER ),
	m_timerPerSecond( PER_SECOND_TIMER)
{
	MODI_ENTER_FUNCTION

		//MODI_ASSERT( p );
    m_pSession = p;
	if( m_pSession )
	{
		m_pSession->SetClientAvatar( this );
	}
	SetType(enSceneAttrPlayer);
   	m_fullData.m_roleInfo.m_baseInfo.m_roleID = id;
	m_accid = INVAILD_ACCOUNT_ID;
	m_LogicCount = 1;
	m_fExpFactor = 1.0f;
	m_fMoneyFactor = 1.0f;
	m_fChenmiPro = 1.0f;
	m_nExpressPackageID = INVAILD_CONFIGID;
	m_byExpressIndex = 0;;

	///	默认情况下，权限全满，经验金钱获得比率100%
	m_dwPrevilege = PREVILEGE_DEFAULT;
	m_bMoneypenalty = 100;
	m_bExppenalty = 100;

	m_inShopType = kNullShop;
	m_bForceSyncAttr = false;
//	m_bShoppingLock = false;
	
	m_bLogoutNormal = false;
	m_bySendFamilyData = 0;
	m_blSendFamilyToMe = false;
	m_blIsInFamily = false;
	m_fSped = 0.0f;
	m_eVolType = enKeyDownVol_None;
	m_blAllDataLoad = false;
	m_pTarget = NULL;

	m_dwPayMoney = 0;
	m_nFlag = 0;
	
	MODI_LEAVE_FUNCTION
	
}

MODI_ClientAvatar::~MODI_ClientAvatar()
{
	if( GetSession() )
	{
		GetSession()->SetClientAvatar( 0 );
	}
}



/** 
 * @brief 获取accname
 * 
 */
const char * MODI_ClientAvatar::GetAccName()
{
	std::string acc_name;
	acc_name = m_fullData.m_roleExtraData.m_cstrAccName;
	return acc_name.c_str();
}

bool MODI_ClientAvatar::IsInMulitRoom()
{
	if( GetRoomID() != INVAILD_ROOM_ID && GetRoomID() != SINGLE_ROOM_ID )
	{
		return true;
	}
	return false;
}

MODI_CHARID 	MODI_ClientAvatar::GetCharID() const
{
	MODI_CHARID charid = GUID_LOPART( GetGUID() );
	return charid;
}

bool MODI_ClientAvatar::IsRoomMaster()
{
	if( IsInMulitRoom() )
	{
        MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
		MODI_MultiGameRoom * pRoom = pLobby->GetMultiRoom( GetRoomID() );
		if( pRoom )
		{
			return pRoom->IsRoomMaster( this );
		}
	}
    return false;
}

MODI_MultiGameRoom * MODI_ClientAvatar::GetMulitRoom()
{
	MODI_MultiGameRoom * room = 0;
	if( IsInMulitRoom() )
	{
		MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
		room = pLobby->GetMultiRoom( GetRoomID() );
	}
	return room;
}

///  是否参战玩家
bool MODI_ClientAvatar::IsPlayer() const
{
	bool bRet = RoleHelpFuns::IsSpectator( GetRoleNormalInfo().m_roomSolt ) ? false : true;
	return bRet;
}

///  获得位置号
defRoomPosSolt MODI_ClientAvatar::GetPostionInRoom() const
{
	defRoomPosSolt nRet = RoleHelpFuns::GetPosSolt( GetRoleNormalInfo().m_roomSolt );
	return nRet;
}
	
bool MODI_ClientAvatar::LoadFromDB()
{
	return true;
}

bool MODI_ClientAvatar::SaveToDB()
{
	return true;
}
	
bool MODI_ClientAvatar::DeleteFromDB()
{
	return true;
}


/** 
 * @brief 加载道具
 * 
 * @param p_item_info 道具信息
 * @param item_size 道具数量
 *
 */
void MODI_ClientAvatar::LoadItem(const MODI_DBItemInfo * p_item_info, const WORD & item_size, bool is_sync)
{
	if(!p_item_info || (item_size == 0))
	{
		Global::logger->error("[load_item] load item faild <name=%s>",GetRoleName());
		MODI_ASSERT(0);
		return;
	}

	/// 从数据库加载该用户的所有的道具
	MODI_DBItemInfo * p_startaddr = (MODI_DBItemInfo *)(p_item_info);
	bool	item_will_expire = false;
	for(WORD i = 0; i<item_size; i++)
	{
		MODI_GameItemInfo * p_game_item = new MODI_GameItemInfo;
		if( p_game_item->Init(this,p_startaddr) == false)
		{
			item_will_expire  = true;
		}
		
#ifdef _ITEM_DEBUG
			Global::logger->debug("[load_user_item] add a new item from db <itemid=%llu,bag_pos=%u,bag_type=%u,configid=%u,exprit_time=%u>",
								  p_game_item->GetItemId(), p_game_item->GetBagPos(), p_game_item->GetBagType(),p_game_item->GetConfigId(),
								  p_game_item->GetToExpireTime());
#endif		
			p_startaddr++;
	}

	if(is_sync)
	{
		// 发送登入成功
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_Login_OK;
		this->SendPackage( &res , sizeof(res) );
	
		NotifyItemList();
		/// 更新人物进入房间信息
		UpdateAvatarByAvatarBag();

		/// 领取网页购买的道具
		MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
		if( pZone )
		{
			MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)(MODI_GameServerAPP::GetInstancePtr());
			if(pApp)
			{

				MODI_S2RDB_Req_WebItem send_cmd;
				send_cmd.m_dwAccountId = GetAccountID();
				send_cmd.m_byServerId = pApp->GetServerID();
				pZone->SendCmd(&send_cmd, sizeof(send_cmd));
			}
		}
	}
	if( item_will_expire )
	{
		MODI_GS2C_Notify_ItemWillExpire  notify;
		SendPackage(&notify,sizeof( MODI_GS2C_Notify_ItemWillExpire));
	}
}

bool MODI_ClientAvatar::OnLoadFromDB( const void * pData )
{
	bool bRet = false;
	MODI_CharactarFullData * pInfoFromDB = (MODI_CharactarFullData *)(pData);
	// 角色基本的信息
	memcpy( &m_fullData , pInfoFromDB , sizeof(MODI_CharactarFullData) );

//	// 重新进入服务器，重置一些数据
	m_fullData.m_roleInfo.m_baseInfo.m_ucGameState = kInLobby;
	m_fullData.m_roleInfo.m_baseInfo.m_roomID = INVAILD_ROOM_ID;
	m_fullData.m_roleInfo.m_normalInfo.m_SingerID = INVAILD_GUID;
	m_fullData.m_roleInfo.m_normalInfo.m_roomSolt = INVAILD_POSSOLT;
	m_nExpressPackageID = 0;
	m_byExpressIndex = 0;
	m_stateFlag.CleanUp();
	m_UpdateMask.CleanUp();
	m_fullData.m_roleExtraData.m_bFangChenmi = GetSession()->IsFangChenMi();

	normal_flags.CreateFromBuf( m_fullData.m_roleExtraData.m_szNormalFlags , 
			sizeof(m_fullData.m_roleExtraData.m_szNormalFlags ) );

	//  下发角色的所有信息
	MODI_GS2C_Notify_RoleAllInfo notifyRoleAllInfo;
	notifyRoleAllInfo.m_RoleAllInfo = this->GetRoleInfo();
	this->SendPackage( &notifyRoleAllInfo , sizeof(notifyRoleAllInfo) );

	/// 通知数据库获取包裹信息
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)(MODI_GameServerAPP::GetInstancePtr());
		if(pApp)
		{
			MODI_S2RDB_Request_ItemInfo send_cmd;
			send_cmd.m_dwAccountID = GetAccountID();
			send_cmd.m_byServerID = pApp->GetServerID();
			pZone->SendCmd(&send_cmd, sizeof(send_cmd));
		}
	}
	
	
/*  
	// test code
	MODI_GameItem * pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag(), kIT_ChannelChat , true , false );
	if( !pItem )
	{
		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() ,  50002 , 100 , enOneDay );
	}

	pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag() , kIT_Feizi , true , false );
	if( !pItem )
	{
		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() ,  50001 , 100 , enOneDay );
	}

	pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag() , kIT_ModfiyExpByFactor  , false , true );
	if( !pItem )
	{
		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() ,  51001 , 1 , enOneHour * 2 );
	}

//	pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag() , kIT_ModfiyExpByConst , true , true );
//	if( !pItem )
//	{
//		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() , 51002 , 100 , enOneDay );
//	}
//
	pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag() , kIT_ChangeAvatar , true , true );
	if( !pItem )
	{
		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() , 52001 , 100 , enOneDay );
	}
	pItem = MODI_ItemOperator::GetItemByImpactTemplateID( &GetPlayerBag() , kIT_PlayAnimation , true , true );
	if( !pItem )
	{
		MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() , 53001 , 100 , enOneDay );
	}
	// end test code
*/

	// test code

	
	if( normal_flags.IsBitSet( kClientNormalFlags_FirstLogin ) == false )
	{
	// 	MODI_GS2C_Notify_GMCmdResult result;
// 		SNPRINTF( result.m_szResult , 
// 				sizeof(result.m_szResult) , 
// 				"first time login" );
// 		SendPackage( &result , sizeof(result) );
		normal_flags.SetBit( kClientNormalFlags_FirstLogin );
		
		//		// 送个礼包
// 		MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
// 		if( pZone )
// 		{
// 			MODI_S2ZS_Request_SendGiveItemMail msg;
// 			msg.m_nCount = 1;
// 			msg.m_shopmail = false;
// 			safe_strncpy( msg.m_szSenderName  , SYS_MAIL_SENDER ,  sizeof(msg.m_szSenderName) );
// 			safe_strncpy( msg.m_szReceverName , this->GetRoleName() , sizeof(msg.m_szReceverName) );
// 			safe_strncpy( msg.m_szContent ,  "第一次进入游戏送给你的东西。这是个测试" , sizeof(msg.m_szContent) );
// 			MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = msg.m_Goods[0];
// 			tmp.m_nCount = 1;
// 			tmp.m_nItemConfigID = 50001;
// 			tmp.m_nLimitTime = enForever;
// 			if( pZone->SendCmd( &msg , sizeof(msg) ) )
// 			{
// 				m_fullData.m_roleExtraData.m_szNormalFlags[3] = 1;
// 			}
// 		}

	}

	bRet = true;
	return bRet;
}

void 	MODI_ClientAvatar::testcode_AddItem( BYTE bySex )
{
	// 临时代码，60级不判断了。没人到那个等级
	// 恢复等级
//	defLevelBinFile & lvlFile = MODI_BinFileMgr::GetInstancePtr()->GetLevelBinFile();
//	DISABLE_UNUSED_WARNING( lvlFile );
//
//	DWORD nLvl = 1;
//	while( nLvl < MAX_LEVEL )
//	{
//		const GameTable::MODI_Level * pLvlInfo = lvlFile.Get( nLvl );
//		if( pLvlInfo )
//		{
//			const GameTable::MODI_Level * pNextLvlInfo = lvlFile.Get( nLvl + 1 );
//			if( pNextLvlInfo )
//			{
//				if( GetExp() >= pLvlInfo->get_Exp() && GetExp() <= pNextLvlInfo->get_Exp() )
//				{
//					GetRoleBaseInfo_Modfiy().m_ucLevel = nLvl;
//					m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLevel );
//					break;
//				}
//			}
//		}
//		nLvl += 1;
//	}

//	defAvatarBinFile & binFile = MODI_BinFileMgr::GetInstancePtr()->GetAvatarBinFile();
//	for( size_t n =0; n < binFile.GetSize(); n++ )
//	{
//		const GameTable::MODI_Avatar * pAvatar = binFile.GetByIndex( n );
//		if( pAvatar )
//		{
//			if( bySex == enSexAll ||
//					bySex == pAvatar->get_SexReq() )
//			{
//				// 添加物品
//				int iRes = MODI_ItemOperator::CreateItemAddToBag( &GetPlayerBag() , pAvatar->get_ConfigID(), 1 , enForever );
//				if( iRes != MODI_ItemOperator::enOK  )
//				{
//					Global::logger->info("[%s] testcode_addItem faild.configid<%u>,client<charid=%u,name=%s>",
//							SVR_TEST,
//							pAvatar->get_ConfigID(),
//							GetCharID(),
//							GetRoleName() );
//				}
//			}
//		}
//	}
}

bool MODI_ClientAvatar::OnSaveToDB( const void * pData )
{
	bool bRet = true ;

	return bRet;
}

bool MODI_ClientAvatar::OnDeleteFromDB( const void * pData )
{
	bool bRet = true;

	return bRet;
}	

void MODI_ClientAvatar::UpdateMaxGoodPlusCount()
{
	if( m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount < 2 )
		return ;

	if( m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount - 1> m_TempData.m_AboutGamePlay.m_nMaxGoodPlusCount )
	{
		m_TempData.m_AboutGamePlay.m_nMaxGoodPlusCount = m_TempData.m_AboutGamePlay.m_nCurGoodPlusCount - 1; 
	}
}

void MODI_ClientAvatar::OnEnterChannel()
{
	m_nFlag = 0;
}

void MODI_ClientAvatar::OnLeaveChannel( int iLeaveReason )
{
	if( iLeaveReason == enLeaveReason_AnotherLogin ||
		iLeaveReason == enLeaveReason_RechangeChannel )
	{

	}
	else 
	{
//		SaveToDB();
	}
	
}

void MODI_ClientAvatar::OnEnterRoom( MODI_GameRoom & room , defRoomPosSolt nPos )
{
	m_nFlag = 0;
	this->SetInviteRoomID( INVAILD_ROOM_ID );
	if( GetRoleBaseInfo().m_roomID != room.GetRoomID() )
	{
		this->GetRoleBaseInfo_Modfiy().m_roomID = room.GetRoomID();
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRoomID );
	}
	SetRoomSlot( nPos );
}

void MODI_ClientAvatar::OnLeaveRoom( MODI_GameRoom & room )
{
    ///    更新玩家的相关信息
    this->GetRoleBaseInfo_Modfiy().m_roomID = INVAILD_ROOM_ID; 
	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRoomID );

	SetRoomSlot( INVAILD_POSSOLT );
	this->GetRoleNormalInfo_Modfiy().m_SingerID = INVAILD_GUID;
	m_TempData.m_AboutGamePlay.m_effectType = 0;
}


void MODI_ClientAvatar::OnStartGame()
{
	m_nFlag = 0;
	SetGoodPlusCount( 0 );
	ResetMaxGoodPlusCount();
	SetRightSec( 0.0f );
	SetEffectRelease( false );
	SetEffectTime( 0.0f );
	SetEffectState( enEffect_FerverTime | enEffect_ShowTime );
}

void MODI_ClientAvatar::OnEndGame()
{
	// 	清掉所有游戏中才有的效果
	GetImpactlist().ClearAll_OutGameFadeOut();
}

void MODI_ClientAvatar::OnEnterShop(MODI_GameShop & Shop)
{

}

void MODI_ClientAvatar::OnLeaveShop(MODI_GameShop & Shop)
{

}

bool MODI_ClientAvatar::IsPlayExpression() const
{
	if( IsObjStateFlag( kObjState_PlayExpression ) )
		return true;
	return false;
}

void MODI_ClientAvatar::OnPlayExpression( WORD nPackage , BYTE byIndex )
{
	m_nExpressPackageID = nPackage;
	m_byExpressIndex = byIndex;
	SetObjStateFlag( kObjState_PlayExpression );
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_timerPlayExpression.Reload(  EXPRESS_PLAY_TIME , pTick->GetTimer() );

	MODI_GS2C_Notify_PlayExpression msg;
	msg.m_ExpressionPackageID = m_nExpressPackageID;
	msg.m_byEnable = MODI_GS2C_Notify_PlayExpression::kPlay;
	msg.m_guid = this->GetGUID();
	msg.m_nIndex = m_byExpressIndex;
	SendOrBroadcast( &msg, sizeof(msg) );

	Global::logger->debug("[%s] client<charid=%u,name=%s> play expression." , EXPRESSION_MODULE ,
			GetCharID(),
			GetRoleName() );
}

void MODI_ClientAvatar::OnStopExpression()
{
	MODI_GS2C_Notify_PlayExpression msg;
	msg.m_ExpressionPackageID = m_nExpressPackageID;
	msg.m_byEnable = MODI_GS2C_Notify_PlayExpression::eStop;
	msg.m_guid = this->GetGUID();
	msg.m_nIndex = m_byExpressIndex;
	SendOrBroadcast( &msg, sizeof(msg) );

	m_nExpressPackageID = 0;
	m_byExpressIndex = 0;
	ClearObjStateFlag( kObjState_PlayExpression );

	Global::logger->debug("[%s] client<charid=%u,name=%s> stop play expression." , EXPRESSION_MODULE ,
			GetCharID(),
			GetRoleName() );
}

void MODI_ClientAvatar::SyncPlayExpression( MODI_ClientAvatar * pClient )
{
	if( IsPlayExpression() )
	{
		MODI_GS2C_Notify_PlayExpression msg;
		msg.m_ExpressionPackageID = m_nExpressPackageID;
		msg.m_byEnable = MODI_GS2C_Notify_PlayExpression::kPlay;
		msg.m_guid = this->GetGUID();
		msg.m_nIndex = m_byExpressIndex;
		pClient->SendPackage( &msg , sizeof(msg) );
	}
}

void MODI_ClientAvatar::OnChangeSinger( MODI_MultiGameRoom * room , 
			MODI_ClientAvatar * pOldSonger ,
			MODI_ClientAvatar * pNewSonger )
{

	if( room->IsPlaying() )
	{
		// 将正在效果中的玩家同步给他
		if( pNewSonger && pNewSonger->IsInFerverTime() )
		{
			MODI_GS2C_Notify_SyncGPower msg;
			msg.m_fSynctime = pNewSonger->GetEffectTime();
			msg.m_guidClient = pNewSonger->GetGUID();
			msg.m_bRelease = pNewSonger->GetEffectRelease();
			this->SendPackage( &msg , sizeof(msg) );
		}
	}
}

bool MODI_ClientAvatar::IsHasMusicHeaderData() const
{
	if( m_MusicHeader.IsHasData() )
		return true;
	return false;
}


void MODI_ClientAvatar::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	if( this->GetSession()->IsInLoginedStatus() && m_blAllDataLoad)
	{
		MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();

		if( m_timerPerSecond(pTick->GetTimer()))
		{
			MODI_TriggerTime	script(3);
			//MODI_ScriptManager<MODI_TriggerTime>::GetInstance().Execute(this,script);
			MODI_TimeScriptManager::GetInstance().Execute(this,script);
			/// 变量更新
			m_stGameUserVarMgr.UpDate(cur_rtime, cur_ttime);
		}
		if( IsPlayExpression() && m_timerPlayExpression( pTick->GetTimer() ) )
		{
			OnStopExpression();
		}

		if( m_timerBagUpdate( pTick->GetTimer() ) )
		{
			GetAvatarBag().Update(cur_rtime, cur_ttime);
			GetPlayerBag().Update(cur_rtime, cur_ttime);
		}

		GetImpactlist().Update(cur_rtime, cur_ttime);

		if( m_bForceSyncAttr )
		{
			SyncDirtyAttr();
			m_timerSyncDirtyAttr.Reload( MODI_GameConstant::get_CharacterSyncAttrInterval() , 
					pTick->GetTimer() );
			m_bForceSyncAttr = false;
		}
		else if(m_timerSyncDirtyAttr( pTick->GetTimer() )) 
		{
			SyncDirtyAttr();
			m_bForceSyncAttr = false;
		}

// 		// 定时保存到数据库
// 		if (m_timerDBSave( pTick->GetTimer() ) )
// 		{
// 			FlushFullCharacterData();
// 			SaveToDB();
// 		}

		UpdateFangChenmi();

		if(! m_blSendFamilyToMe)
		{
			/// 把别人的家族名片发给自己(临时放在这里)
			MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
			char buf[Skt::MAX_USERDATASIZE];
			WORD send_size = 0;
			send_size = pChannel->CreateCFCardPackage(buf, send_size);
			SendPackage(&buf, send_size);
			
#ifdef _FAMILY_DEBUG
			Global::logger->debug("[send_family_card_to_me] <name=%s,size=%u>", GetRoleName(), send_size);
#endif
			m_blSendFamilyToMe = true;
		}
		
		if(GetSendFCard() == 2)
		{
			SetSendFCard(3);
			//if(GetFamilyID() != 0)
			{
				MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
				
				/// 通知频道家族名片
				char buf[sizeof(MODI_GS2C_Notify_FamilyCard) + sizeof(MODI_FamilyCard)];
				memset(buf, 0, sizeof(buf));
				MODI_GS2C_Notify_FamilyCard * p_send_cmd = (MODI_GS2C_Notify_FamilyCard *)buf;
				AutoConstruct(p_send_cmd);
				
				MODI_FamilyCard * p_family_card = &(p_send_cmd->m_stCard[0]);
				p_family_card->m_stGuid = MAKE_GUID(GetCharID() , 0 );
				p_family_card->m_stPosition = (enFamilyPositionType)m_stCFamilyInfo.m_enPosition;
				p_family_card->m_wLevel = m_stCFamilyInfo.m_wLevel;
				strncpy(p_family_card->m_cstrFamilyName,GetFamilyName(), sizeof(p_family_card->m_cstrFamilyName) - 1);
				p_send_cmd->m_wdSize = 1;
				pChannel->Broadcast(p_send_cmd, sizeof(buf));
				
#ifdef _FAMILY_DEBUG
				Global::logger->debug("[first_login] send family card to all client <name=%s>", GetRoleName());
#endif				
			}
		}
	}
}

void MODI_ClientAvatar::SyncDirtyAttr()
{
	if( m_UpdateMask.IsDirty() == false )
		return ;

	static char szPackageBuf[Skt::MAX_USERDATASIZE];
	static char szPackageBufOther[Skt::MAX_USERDATASIZE];
	static char szPackageBufZS[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof( szPackageBuf ) );
	memset( szPackageBufOther , 0 , sizeof(szPackageBufOther) );
	memset( szPackageBufZS , 0 , sizeof(szPackageBufZS) );

	unsigned int nMaxBufSize = Skt::MAX_USERDATASIZE - sizeof(MODI_GS2C_Notify_UpdateObj);
	unsigned int nMaxBufSizeZS = Skt::MAX_USERDATASIZE - sizeof(MODI_S2ZS_Notify_UpdateObj);

	MODI_GS2C_Notify_UpdateObj * p_notify = (MODI_GS2C_Notify_UpdateObj *)(szPackageBuf);
	AutoConstruct( p_notify );
	p_notify->m_objType = enObjType_Player;
	p_notify->m_guid = GetGUID();
	MODI_UpdateDataWrite w( p_notify->m_pData , nMaxBufSize );

	MODI_GS2C_Notify_UpdateObj * p_notifyother = (MODI_GS2C_Notify_UpdateObj *)(szPackageBufOther);
	AutoConstruct( p_notifyother );
	p_notifyother->m_objType = enObjType_Player;
	p_notifyother->m_guid = GetGUID();
	MODI_UpdateDataWrite wother( p_notifyother->m_pData , nMaxBufSize );

	MODI_S2ZS_Notify_UpdateObj * p_notifyzs = (MODI_S2ZS_Notify_UpdateObj *)(szPackageBufZS);
	AutoConstruct( p_notifyzs );
	p_notifyzs->m_guid = GetGUID();
	MODI_UpdateDataWrite wzs( p_notifyzs->m_pData , nMaxBufSizeZS );

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kRenqi ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRenqi );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRenqi );

		w.Write( &GetRoleDetailInfo().m_SocialRelData.m_nRenqi, sizeof(GetRoleDetailInfo().m_SocialRelData.m_nRenqi) );
		wzs.Write( &GetRoleDetailInfo().m_SocialRelData.m_nRenqi, sizeof(GetRoleDetailInfo().m_SocialRelData.m_nRenqi) );

		Global::logger->debug("[%s] sync->renqi=%u to client<charid=%u,name=%s>" , SVR_TEST ,
				GetRoleDetailInfo().m_SocialRelData.m_nRenqi , 
				this->GetCharID(),
				this->GetRoleName() );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kGameState ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGameState );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGameState );

		w.Write( &GetRoleBaseInfo().m_ucGameState , sizeof(GetRoleBaseInfo().m_ucGameState) );
		wzs.Write( &GetRoleBaseInfo().m_ucGameState , sizeof(GetRoleBaseInfo().m_ucGameState) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kLevel ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLevel );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLevel );
		p_notifyother->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLevel );

		w.Write( &GetRoleBaseInfo().m_ucLevel, sizeof(GetRoleBaseInfo().m_ucLevel) );
		wother.Write( &GetRoleBaseInfo().m_ucLevel, sizeof(GetRoleBaseInfo().m_ucLevel) );
		wzs.Write( &GetRoleBaseInfo().m_ucLevel, sizeof(GetRoleBaseInfo().m_ucLevel) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kAvatar ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAvatar );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAvatar );
		p_notifyother->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAvatar );

		w.Write( &GetRoleNormalInfo().m_avatarData ,  sizeof(GetRoleNormalInfo().m_avatarData) );
		wother.Write( &GetRoleNormalInfo().m_avatarData ,  sizeof(GetRoleNormalInfo().m_avatarData) );
		wzs.Write( &GetRoleNormalInfo().m_avatarData ,  sizeof(GetRoleNormalInfo().m_avatarData) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kExp ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExp );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExp );

		w.Write( &GetRoleNormalInfo().m_nExp , sizeof(GetRoleNormalInfo().m_nExp) );
		wzs.Write( &GetRoleNormalInfo().m_nExp , sizeof(GetRoleNormalInfo().m_nExp) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kAge ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAge );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAge );

		w.Write( &GetRoleDetailInfo().m_byAge , sizeof(GetRoleDetailInfo().m_byAge) );
		wzs.Write( &GetRoleDetailInfo().m_byAge , sizeof(GetRoleDetailInfo().m_byAge) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kCity ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCity );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCity );

		w.Write( &GetRoleDetailInfo().m_nCity , sizeof(GetRoleDetailInfo().m_nCity) );
		wzs.Write( &GetRoleDetailInfo().m_nCity , sizeof(GetRoleDetailInfo().m_nCity) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kClan ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kClan );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kClan );

		w.Write( &GetRoleDetailInfo().m_nClan  , sizeof(GetRoleDetailInfo().m_nClan) );
		wzs.Write( &GetRoleDetailInfo().m_nClan  , sizeof(GetRoleDetailInfo().m_nClan) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kGroup ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGroup );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGroup );

		w.Write( &GetRoleDetailInfo().m_nGroup , sizeof(GetRoleDetailInfo().m_nGroup) );
		wzs.Write( &GetRoleDetailInfo().m_nGroup , sizeof(GetRoleDetailInfo().m_nGroup) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kQQ ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kQQ );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kQQ );

		w.Write( &GetRoleDetailInfo().m_nQQ  , sizeof(GetRoleDetailInfo().m_nQQ) );
		wzs.Write( &GetRoleDetailInfo().m_nQQ  , sizeof(GetRoleDetailInfo().m_nQQ) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kBlood ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBlood );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBlood );

		w.Write( &GetRoleDetailInfo().m_byBlood , sizeof(GetRoleDetailInfo().m_byBlood) );
		wzs.Write( &GetRoleDetailInfo().m_byBlood , sizeof(GetRoleDetailInfo().m_byBlood) );
	}
	
	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kBirthDay ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBirthDay );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBirthDay );

		w.Write( &GetRoleDetailInfo().m_nBirthDay , sizeof(GetRoleDetailInfo().m_nBirthDay) );
		wzs.Write( &GetRoleDetailInfo().m_nBirthDay , sizeof(GetRoleDetailInfo().m_nBirthDay) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kVoteCount ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kVoteCount );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kVoteCount );

		w.Write( &GetRoleDetailInfo().m_nVoteCount , sizeof(GetRoleDetailInfo().m_nVoteCount) );
		wzs.Write( &GetRoleDetailInfo().m_nVoteCount , sizeof(GetRoleDetailInfo().m_nVoteCount) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kPersonalSign ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPersonalSign );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPersonalSign );

		w.Write( GetPersonalSign() , sizeof(GetRoleDetailInfo().m_szPersonalSign) );
		wzs.Write( GetPersonalSign() , sizeof(GetRoleDetailInfo().m_szPersonalSign) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kWin ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kWin );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kWin );

		w.Write( &GetRoleDetailInfo().m_nWin , sizeof(GetRoleDetailInfo().m_nWin) );
		wzs.Write( &GetRoleDetailInfo().m_nWin , sizeof(GetRoleDetailInfo().m_nWin) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kLoss ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLoss );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLoss );

		w.Write( &GetRoleDetailInfo().m_nLoss , sizeof(GetRoleDetailInfo().m_nLoss) );
		wzs.Write( &GetRoleDetailInfo().m_nLoss , sizeof(GetRoleDetailInfo().m_nLoss) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kTie ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kTie );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kTie );

		w.Write( &GetRoleDetailInfo().m_nTie , sizeof(GetRoleDetailInfo().m_nTie) );
		wzs.Write( &GetRoleDetailInfo().m_nTie , sizeof(GetRoleDetailInfo().m_nTie) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kPefect ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPefect );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPefect );

		w.Write( &GetRoleDetailInfo().m_nPefect , sizeof(GetRoleDetailInfo().m_nPefect) );
		wzs.Write( &GetRoleDetailInfo().m_nPefect , sizeof(GetRoleDetailInfo().m_nPefect) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kCool ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCool );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCool );

		w.Write( &GetRoleDetailInfo().m_nCool , sizeof(GetRoleDetailInfo().m_nCool) );
		wzs.Write( &GetRoleDetailInfo().m_nCool , sizeof(GetRoleDetailInfo().m_nCool) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kGood ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGood );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGood );

		w.Write( &GetRoleDetailInfo().m_nGood , sizeof(GetRoleDetailInfo().m_nGood) );
		wzs.Write( &GetRoleDetailInfo().m_nGood , sizeof(GetRoleDetailInfo().m_nGood) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kBad ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBad );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBad );

		w.Write( &GetRoleDetailInfo().m_nBad , sizeof(GetRoleDetailInfo().m_nBad) );
		wzs.Write( &GetRoleDetailInfo().m_nBad , sizeof(GetRoleDetailInfo().m_nBad) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kMiss ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMiss );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMiss );

		w.Write( &GetRoleDetailInfo().m_nMiss , sizeof(GetRoleDetailInfo().m_nMiss) );
		wzs.Write( &GetRoleDetailInfo().m_nMiss , sizeof(GetRoleDetailInfo().m_nMiss) );
	}
	
	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kCombo ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCombo );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCombo );

		w.Write( &GetRoleDetailInfo().m_nCombo , sizeof(GetRoleDetailInfo().m_nCombo) );
		wzs.Write( &GetRoleDetailInfo().m_nCombo , sizeof(GetRoleDetailInfo().m_nCombo) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kExact ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExact );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExact );

		w.Write( &GetRoleDetailInfo().m_fExact , sizeof(GetRoleDetailInfo().m_fExact) );
		wzs.Write( &GetRoleDetailInfo().m_fExact , sizeof(GetRoleDetailInfo().m_fExact) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kMoney ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMoney );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMoney );

		w.Write( &GetRoleDetailInfo().m_nMoney, sizeof(GetRoleDetailInfo().m_nMoney) );
		wzs.Write( &GetRoleDetailInfo().m_nMoney, sizeof(GetRoleDetailInfo().m_nMoney) );

		Global::logger->debug("[%s] sync->money=%u to client<charid=%u,name=%s>" , SVR_TEST ,
				GetRoleDetailInfo().m_nMoney , 
				this->GetCharID(),
				this->GetRoleName() );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kRMBMoney ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRMBMoney );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRMBMoney );

		w.Write( &GetRoleDetailInfo().m_nRMBMoney, sizeof(GetRoleDetailInfo().m_nRMBMoney) );
		wzs.Write( &GetRoleDetailInfo().m_nRMBMoney, sizeof(GetRoleDetailInfo().m_nRMBMoney) );

		Global::logger->debug("[%s] sync->rmb_money=%u to client<charid=%u,name=%s>" , SVR_TEST ,
				GetRoleDetailInfo().m_nRMBMoney,
				this->GetCharID(),
				this->GetRoleName() );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kChangCount ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kChangCount );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kChangCount );

		w.Write( &GetRoleDetailInfo().m_Chang, sizeof(GetRoleDetailInfo().m_Chang) );
		wzs.Write( &GetRoleDetailInfo().m_Chang, sizeof(GetRoleDetailInfo().m_Chang) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kHengCount ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHengCount );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHengCount );

		w.Write( &GetRoleDetailInfo().m_Heng, sizeof(GetRoleDetailInfo().m_Heng) );
		wzs.Write( &GetRoleDetailInfo().m_Heng, sizeof(GetRoleDetailInfo().m_Heng) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kKeyBoardCount ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kKeyBoardCount );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kKeyBoardCount );

		w.Write( &GetRoleDetailInfo().m_KeyBoard, sizeof(GetRoleDetailInfo().m_KeyBoard) );
		wzs.Write( &GetRoleDetailInfo().m_KeyBoard, sizeof(GetRoleDetailInfo().m_KeyBoard) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kToupiaoCount ) )
	{
		p_notify->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kToupiaoCount );
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kToupiaoCount );

		w.Write( &GetRoleDetailInfo().m_Toupiao, sizeof(GetRoleDetailInfo().m_Toupiao) );
		wzs.Write( &GetRoleDetailInfo().m_Toupiao, sizeof(GetRoleDetailInfo().m_Toupiao) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kRoomID ) )
	{
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRoomID );
		wzs.Write( &GetRoleBaseInfo().m_roomID , sizeof(GetRoleBaseInfo().m_roomID) );
	}

	if( m_UpdateMask.isSetBit( MODI_RoleUPMaskDefine::kConsumeRMB ) )
	{
		p_notifyzs->m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kConsumeRMB );
		wzs.Write( &GetFullData().m_roleExtraData.consume_rmb , sizeof(GetFullData().m_roleExtraData.consume_rmb) );
	}

	if( w.GetWriteSize() > 0 )
	{
		p_notify->m_nSize = w.GetWriteSize();
		int iSendSize =  p_notify->m_nSize * sizeof(char) + sizeof(MODI_GS2C_Notify_UpdateObj); 
		SendPackage( p_notify , iSendSize );
	}

	if( wother.GetWriteSize() > 0 )
	{
		MODI_MultiGameRoom * pMultiRoom = GetMulitRoom();
		if( pMultiRoom )
		{
			p_notifyother->m_nSize = wother.GetWriteSize();
			int iSendSize =  p_notifyother->m_nSize * sizeof(char) + sizeof(MODI_GS2C_Notify_UpdateObj); 
			pMultiRoom->Broadcast_Except( p_notifyother , iSendSize , this );
		}
	}

	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( wzs.GetWriteSize() > 0 && pZone )
	{
		p_notifyzs->m_nSize = wzs.GetWriteSize();
		int iSendSize =  p_notifyzs->m_nSize * sizeof(char) + sizeof(MODI_S2ZS_Notify_UpdateObj); 
		pZone->SendCmd( p_notifyzs , iSendSize );
	}

	m_UpdateMask.CleanUp();
	m_bForceSyncAttr = false;
}

//    保存音乐文件头信息
void MODI_ClientAvatar::SetMusicDataHeader( const char * pData , unsigned int nSize )
{
	m_MusicHeader.SetData( pData , nSize );
}
	
//    发送游戏音乐头
void MODI_ClientAvatar::SendMusicHeader(MODI_ClientAvatar * pClient )
{
	if( !m_MusicHeader.IsHasData() )
	{
		Global::logger->error("[%s] Client has not MusicHeaderData .<RoleName = %s , GUID = %llu> ." , GS_ROOMOPT ,
				GetRoleName() , GetGUID().ToUint64() );
		MODI_ASSERT( 0 );
		return ;
	}

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	MODI_GS2C_Notify_MusicHeaderData  * p_MusicHeaderNotify = (MODI_GS2C_Notify_MusicHeaderData *)(szPackageBuf);
	AutoConstruct( p_MusicHeaderNotify );
	p_MusicHeaderNotify->m_nSize =  m_MusicHeader.GetSize();
	p_MusicHeaderNotify->m_nSongerGUID = this->GetGUID();
	const char * pDataTemp = m_MusicHeader.GetDataPtr();
	char * pDestTemp = (char *)(p_MusicHeaderNotify->m_pData);
	memcpy( pDestTemp , pDataTemp , p_MusicHeaderNotify->m_nSize );
	int iSendSize = p_MusicHeaderNotify->m_nSize * sizeof(char) + sizeof(MODI_GS2C_Notify_MusicHeaderData);
	pClient->SendPackage( p_MusicHeaderNotify , iSendSize );	

	Global::logger->debug("[%s] send client <charid=%u,name=%s> music header to target client<charid=%u,name=%s> ." , 
			GS_ROOMOPT ,
			GetCharID(),
			GetRoleName(),
			pClient->GetCharID(),
			pClient->GetRoleName() );
}


#if 0
void MODI_ClientAvatar::SendAddItemPackage( BYTE nPos , const MODI_GameItem  * item )
{
	if( !item || item->GetCount() == 0 || item->GetGUID().IsInvalid() )
	{
		MODI_ASSERT(0);
		return ;
	}

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	MODI_GS2C_Notify_Itemlist  	* addItemMsg  = (MODI_GS2C_Notify_Itemlist *)(szPackageBuf);
	AutoConstruct( addItemMsg );
	addItemMsg->m_byOpt = MODI_GS2C_Notify_Itemlist::enAddItem;
	addItemMsg->m_nItemSize = 1;
	addItemMsg->m_pItemlist[0].m_ItemGUID  = item->GetGUID();
	addItemMsg->m_pItemlist[0].m_nCount = item->GetCount();
	addItemMsg->m_pItemlist[0].m_nBagType = enBagType_PlayerBag;
	addItemMsg->m_pItemlist[0].m_nPosInBag = item->GetPosInBag();
	addItemMsg->m_pItemlist[0].m_nConfigID = item->GetConfigID();
	addItemMsg->m_pItemlist[0].m_nRemainTime = item->GetRemainTime();
	SendPackage( addItemMsg , sizeof(MODI_GS2C_Notify_Itemlist) + sizeof(MODI_ItemInfoInBag) * 1 );
}
#endif
	
bool MODI_ClientAvatar::IsRejectInvitePlay( const MODI_ClientAvatar * pSrcClient )
{
	if( GetInviteHandlerMode() == enInviteH_RejectAll )
		return true;


	return false;
}


void 	MODI_ClientAvatar::SaveToDBAndToZone()
{
	unsigned int copy_size = sizeof(m_fullData.m_roleExtraData.m_szNormalFlags);
	normal_flags.CopyTo( m_fullData.m_roleExtraData.m_szNormalFlags , copy_size );

	// 切换频道或者被顶，保存一下用户变量
	m_stGameUserVarMgr.SaveUserVar();
}

void 	MODI_ClientAvatar::SetEffectState( int iSet )
{
	m_TempData.m_AboutGamePlay.m_effectType |= iSet;
}

void 	MODI_ClientAvatar::ClearEffectState( int iClear )
{
	m_TempData.m_AboutGamePlay.m_effectType &= ~iClear;
}

bool 	MODI_ClientAvatar::IsInShowTime() const
{
	return m_TempData.m_AboutGamePlay.m_effectType & enEffect_ShowTime;
}

bool 	MODI_ClientAvatar::IsInFerverTime() const
{
	return  m_TempData.m_AboutGamePlay.m_effectType & enEffect_FerverTime;
}

bool 	MODI_ClientAvatar::IsNoEffect() const
{
	return m_TempData.m_AboutGamePlay.m_effectType == enEffect_None;
}
	
void 	MODI_ClientAvatar::SetEffectRelease(bool bSet)
{
	m_TempData.m_AboutGamePlay.m_bRelease = bSet;
}

bool 	MODI_ClientAvatar::GetEffectRelease() const
{
	return m_TempData.m_AboutGamePlay.m_bRelease;
}

void 	MODI_ClientAvatar::SetEffectTime(float f)
{
	m_TempData.m_AboutGamePlay.m_ftime = f;
}

float 	MODI_ClientAvatar::GetEffectTime() const
{
	return m_TempData.m_AboutGamePlay.m_ftime;
}

void 	MODI_ClientAvatar::SetClientAvatarData( const MODI_ClientAvatarData & set , bool bUpdate )
{
	if( bUpdate )
	{
		if( memcmp( &set , &GetRoleNormalInfo().m_avatarData ,  sizeof(MODI_ClientAvatarData) ) )
		{
			m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAvatar );
		}
	}

	GetRoleNormalInfo_Modfiy().m_avatarData = set;
}

const  	MODI_ClientAvatarData & MODI_ClientAvatar::GetClientAvatarData() const
{
	return GetRoleNormalInfo().m_avatarData;
}

void 	MODI_ClientAvatar::UpdateAvatarByAvatarBag()
{
	MODI_ClientAvatarData old = GetRoleNormalInfo().m_avatarData;

	// 更新人物形象
	for( int i = 0; i < GetAvatarBag().GetSize(); i++ )
	{
		MODI_GameItemInfo * p = GetAvatarBag().GetItemByPos(i + 1);
		if( p )
		{
			GetRoleNormalInfo_Modfiy().m_avatarData.m_Avatars[i] = p->GetConfigId();
		}
		else 
		{
			GetRoleNormalInfo_Modfiy().m_avatarData.m_Avatars[i] = INVAILD_CONFIGID;
		}
	}

	if( memcmp( &old , &GetRoleNormalInfo().m_avatarData ,  sizeof(MODI_ClientAvatarData) ) )
	{
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAvatar );
	}
}

void MODI_ClientAvatar::SetPersonalSign( const char * szSign , size_t nLen )
{
	if( strcmp( szSign , 
				m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign  ) )
	{
		memset( m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign , 
				0 , 
				sizeof(m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign) );
		safe_strncpy( m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign , 
				szSign , 
				sizeof(m_fullData.m_roleInfo.m_detailInfo.m_szPersonalSign) );
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPersonalSign );
	}
}

void 	MODI_ClientAvatar::IncMoney( DWORD n )
{
	if( n )
	{
		if( GetRoleDetailInfo().m_nMoney + n > MAX_MONEY )
		{
			GetRoleDetailInfo_Modfiy().m_nMoney = MAX_MONEY;
			Global::logger->warn("[%s]client<charid=%u,name=%s>'s money exceptional." ,  
					CLIENT_EXCEPTION ,
					GetCharID(),
					GetRoleName() );
		}
		else 
		{
			GetRoleDetailInfo_Modfiy().m_nMoney += n;
		}

		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMoney );
		ForceSyncAttr();
	}
}

void 	MODI_ClientAvatar::SetMoney( DWORD n )
{
	if( n == GetMoney() )
		return ;

	if( n >= MAX_MONEY )
	{
		GetRoleDetailInfo_Modfiy().m_nMoney = MAX_MONEY;

		Global::logger->error("[%s]client<charid=%u,name=%s>'s money<set=%u> exceptional." ,  
				CLIENT_EXCEPTION ,
				GetCharID(),
				GetRoleName() ,
				n );
	}
	else 
	{
		GetRoleDetailInfo_Modfiy().m_nMoney = n;
	}

	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMoney );
	ForceSyncAttr();
}


void 	MODI_ClientAvatar::DecMoney( DWORD n )
{
	if( n )
	{
		if( n > GetMoney() )
		{
			GetRoleDetailInfo_Modfiy().m_nMoney = 0;
		}
		else 
		{
			GetRoleDetailInfo_Modfiy().m_nMoney -= n;
		}
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMoney );
		ForceSyncAttr();
		
// 		/// 发命令给roledb
// 		MODI_S2RDB_DecMoney_Cmd send_cmd;
// 		send_cmd.m_dwAccountID = GetAccountID();
// 		send_cmd.m_dwMoney = n;
// 		send_cmd.m_byMoneyType = 1;
// 		MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
// 		if(! pZone )
// 		{
// 			Global::logger->info("[dec_money] can't get zone service client <name=%s,decmoney=%u>", GetRoleName(), n);
// 			MODI_ASSERT(0);
// 			return;
// 		}
// 		pZone->SendCmd(&send_cmd, sizeof(send_cmd));
	}
}

void 	MODI_ClientAvatar::IncLogicCount()
{
	m_LogicCount += 1;
}

DWORD 	MODI_ClientAvatar::GetLogicCount() const
{
	return m_LogicCount;
}

void 	MODI_ClientAvatar::IncRMBMoney( DWORD n )
{
	if( n )
	{
		if( GetRoleDetailInfo().m_nRMBMoney + n >= MAX_RMBMONEY )
		{
			GetRoleDetailInfo_Modfiy().m_nRMBMoney  = MAX_RMBMONEY;
	
			Global::logger->warn("[%s]client<charid=%u,name=%s>'s RMBmoney exceptional." ,  
					CLIENT_EXCEPTION ,
					GetCharID(),
					GetRoleName() );
		}
		else 
		{
			GetRoleDetailInfo_Modfiy().m_nRMBMoney += n;
		}


		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRMBMoney );
		ForceSyncAttr();
	}
}

void 	MODI_ClientAvatar::SetRMBMoney( DWORD n )
{
	if( n == GetRMBMoney() )
		return ;

	if( n >= MAX_RMBMONEY )
	{
		GetRoleDetailInfo_Modfiy().m_nRMBMoney = MAX_RMBMONEY;

		Global::logger->warn("[%s]client<charid=%u,name=%s>'s RMBmoney<set=%u> exceptional." ,  
				CLIENT_EXCEPTION ,
				GetCharID(),
				GetRoleName() ,
				n );
	}
	else 
	{
		GetRoleDetailInfo_Modfiy().m_nRMBMoney = n;
	}

	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRMBMoney );
	ForceSyncAttr();
}



void 	MODI_ClientAvatar::DecRMBMoney( DWORD n )
{
// 	if( n )
// 	{
// 		if( n > GetRMBMoney() )
// 		{
// 			GetRoleDetailInfo_Modfiy().m_nRMBMoney = 0;
// 		}
// 		else 
// 		{
// 			GetRoleDetailInfo_Modfiy().m_nRMBMoney -= n;
// 		}
// 		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRMBMoney );
// 		ForceSyncAttr();
// 		/// 直接写数据库
// 	}

	/// 发命令给
	MODI_S2RDB_DecMoney_Cmd send_cmd;
	send_cmd.m_dwAccountID = GetAccountID();
	send_cmd.m_dwMoney = n;
	send_cmd.m_byMoneyType = 2;
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if(! pZone )
	{
		Global::logger->info("[dec_money_rmb] can't get zone service client <name=%s,decmoneyrmb=%u>", GetRoleName(), n);
		MODI_ASSERT(0);
		return;
	}
	pZone->SendCmd(&send_cmd, sizeof(send_cmd));
}

void 	MODI_ClientAvatar::IncConsumeRMB( DWORD n )
{
	if( n )
	{
		m_fullData.m_roleExtraData.consume_rmb += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kConsumeRMB );
	}
}

void 	MODI_ClientAvatar::IncVoteCount( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nVoteCount += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kVoteCount );
	}
}

void 	MODI_ClientAvatar::IncWin()
{
	GetRoleDetailInfo_Modfiy().m_nWin += 1;
	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kWin );
}

void 	MODI_ClientAvatar::IncLoss()
{
	GetRoleDetailInfo_Modfiy().m_nLoss += 1;
	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLoss );
}

void 	MODI_ClientAvatar::IncTie()
{
	GetRoleDetailInfo_Modfiy().m_nTie += 1;
	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kTie );
}

void 	MODI_ClientAvatar::IncPefect( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nPefect += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kPefect );
	}
}

void 	MODI_ClientAvatar::IncCool( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nCool += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCool );
	}
}

void 	MODI_ClientAvatar::IncGood( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nGood += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGood );
	}
}

void 	MODI_ClientAvatar::IncBad( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nBad += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBad );
	}
}

void 	MODI_ClientAvatar::IncMiss( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nMiss += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kMiss );
	}
}

void 	MODI_ClientAvatar::IncCombo( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_nCombo += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCombo );
	}
}

void 	MODI_ClientAvatar::IncExact( float f )
{
	if( f )
	{
		GetRoleDetailInfo_Modfiy().m_fExact += f;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExact );
	}
}

void 	MODI_ClientAvatar::IncChangCount( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_Chang += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kChangCount );
	}

}

void 	MODI_ClientAvatar::IncHengCount( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_Heng += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHengCount );
	}
}

void 	MODI_ClientAvatar::IncKeyboardCount( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_KeyBoard += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kKeyBoardCount );
	}
}

void 	MODI_ClientAvatar::IncToupiaoCount( DWORD n )
{
	if( n )
	{
		GetRoleDetailInfo_Modfiy().m_Toupiao += n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kToupiaoCount );
	}
}

//获得键盘模式最大的准度
float	MODI_ClientAvatar::GetHighestKeyPrecision() const
{
	//

	return 0.0f;
}
void 	MODI_ClientAvatar::SetHighestKeyPrecision(float pre)
{

}
// 键盘模式最大精确度的歌曲
WORD	MODI_ClientAvatar::GetHighestKeyPrecisionMusicid() const
{

	return 0;
}

void 	MODI_ClientAvatar::SetHighestKeyPrecisionMusicid(WORD music)
{

}
//键盘模式最高的分数
DWORD	MODI_ClientAvatar::GetHighestKeySocre() const
{

	return 0;
}
void 	MODI_ClientAvatar::SetHighestKeyScore(DWORD score)
{


}
//键盘模式最大分数的音乐ID
WORD	MODI_ClientAvatar::GetHighestKeyScoreMusicid( ) const
{
	return 0;
}
void 	MODI_ClientAvatar::SetHighestKeyScoreMusicid(WORD music)
{

}



DWORD 	MODI_ClientAvatar::GetHighestSocre() const
{
	return GetRoleDetailInfo().highest_socre;
}

void 	MODI_ClientAvatar::SetHighestScore( DWORD score )
{
	if( GetHighestSocre() != score )
	{
		GetRoleDetailInfo_Modfiy().highest_socre = score;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHighestScore );
	}
}

WORD 	MODI_ClientAvatar::GetHighestScoreMusicid() const
{
	return GetRoleDetailInfo().highest_score_musicid;
}

void 	MODI_ClientAvatar::SetHighestScoreMusicid( WORD music )
{
	if( GetHighestScoreMusicid() != music )
	{
		GetRoleDetailInfo_Modfiy().highest_score_musicid = music;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHighestScoreMusicid );
	}
}

float 	MODI_ClientAvatar::GetHighestPrecision() const
{
	return GetRoleDetailInfo().highest_precision;
}

void 	MODI_ClientAvatar::SetHighestPrecision(float pre)
{
	if( GetHighestPrecision() != pre )
	{
		GetRoleDetailInfo_Modfiy().highest_precision = pre;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHighestPrecision );
	}
}

WORD 	MODI_ClientAvatar::GetHighestPrecisionMusicid() const
{
	return GetRoleDetailInfo().highest_precision_musicid;
}

void 	MODI_ClientAvatar::SetHighestPrecisionMusicid( WORD music )
{
	if( GetHighestPrecisionMusicid() != music )
	{
		GetRoleDetailInfo_Modfiy().highest_precision_musicid = music;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kHighestPrecisionMusicid );
	}
}

void 	MODI_ClientAvatar::SetGameState( kGameState ns )
{
	if( ns != GetRoleBaseInfo().m_ucGameState )
	{
		GetRoleBaseInfo_Modfiy().m_ucGameState = ns;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGameState );
	}
}

kGameState MODI_ClientAvatar::GetGameState() const
{
	return GetRoleBaseInfo().m_ucGameState;
}

void MODI_ClientAvatar::SetExp( DWORD nSet )
{
	if( nSet != GetExp() )
	{
		m_fullData.m_roleInfo.m_normalInfo.m_nExp = nSet;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kExp );
	}
}


void MODI_ClientAvatar::AddRenqi( DWORD nSet )
{
	m_fullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi += nSet;
	m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kRenqi );
	ForceSyncAttr();
}

void 	MODI_ClientAvatar::SetQQNum( DWORD n )
{
	if( GetQQNum() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nQQ = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kQQ );
	}
}

void 	MODI_ClientAvatar::SetAge( BYTE n )
{
	if( GetAge() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_byAge = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kAge );
	}
}

void 	MODI_ClientAvatar::SetCity( DWORD n )
{
	if( GetCity() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nCity = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kCity );
	}
}

void 	MODI_ClientAvatar::SetClan( DWORD n )
{
	if( GetClan() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nClan = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kClan );
	}
}

void 	MODI_ClientAvatar::SetGroup( DWORD n )
{
	if( GetGroup() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nCity = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kGroup );
	}
}

void 	MODI_ClientAvatar::SetBlood( BYTE n )
{
	if( GetBlood() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_byBlood = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBlood );
	}
}

void 	MODI_ClientAvatar::SetBirthDay( DWORD n )
{
	if( GetBirthDay() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nBirthDay = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kBirthDay );
	}
}

void 	MODI_ClientAvatar::SetVoteCount( DWORD n )
{
	if( GetVoteCount() != n )
	{
		m_fullData.m_roleInfo.m_detailInfo.m_nVoteCount = n;
		m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kVoteCount );
	}
}

void MODI_ClientAvatar::AdjustExp( long lValue )
{
	if( lValue == 0 )
		return ;

	if( lValue > 0 )
	{
		SetExp( GetExp() + lValue );
	}
	else 
	{
		lValue = lValue * -1;
		if( lValue > GetExp() )
			SetExp( 0 );
		else 
			SetExp( GetExp() - lValue );
	}
}

int  MODI_ClientAvatar::TryLevelUp( bool bSync )
{
	if( GetLevel() >= MAX_LEVEL )
		return 0;
	int iLevelUp = 0;

	// 尝试升级
	do
	{
		unsigned char byNextLevel = GetLevel() + 1;
		if( byNextLevel <= MAX_LEVEL )
		{
			// 计算下一个等级所需的经验
			unsigned int nNextLevelExp = 0;
			if( !MODI_BinFileMgr::GetInstancePtr()->GetNeedExpByLevel( byNextLevel , nNextLevelExp ) )
			{
				MODI_ASSERT(0);
				break;
			}

			if( GetExp() >= nNextLevelExp )
			{
				GetRoleBaseInfo_Modfiy().m_ucLevel += 1;
				Global::logger->info("[char_level_up] char <charid=%u,charname=%s,srclevel=%u,targetlevel=%u>",
									 GetCharID(), GetRoleName(),byNextLevel - 1, byNextLevel);
				iLevelUp += 1;
				m_UpdateMask.SetBit( MODI_RoleUPMaskDefine::kLevel );
			}
			else 
				break;
		}	
		else 
			break;
	}while(true);

	if( iLevelUp > 0 && bSync )
	{
		MODI_GS2C_Notify_LevelUp lvlup_msg;
		lvlup_msg.m_guid = GetGUID();
		lvlup_msg.m_byLevel = GetLevel();
		SendOrBroadcast( &lvlup_msg , sizeof(lvlup_msg) );
	}

// 	if(MODI_GameConstant::get_LevelupToten())
// 	{
// #ifdef _HRX_DEBUG
// 		Global::logger->debug("[levelup_toten] set levelup to ten falg");
// #endif		
// 		// 送礼包
// 		if( GetLevel() >= 10 && GetFullData().m_roleExtraData.m_szNormalFlags[3] == 0 )
// 		{
// 			// 送个礼包
// 			MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
// 			if( pZone )
// 			{
// 				MODI_S2ZS_Request_SendGiveItemMail msg;
// 				msg.m_nCount = 1;
// 				msg.m_shopmail = false;
// 				safe_strncpy( msg.m_szSenderName  , SYS_MAIL_SENDER ,  ROLE_NAME_MAX_LEN );
// 				safe_strncpy( msg.m_szReceverName , this->GetRoleName() , ROLE_NAME_MAX_LEN );
// 				safe_strncpy( msg.m_szContent ,  "冲等级活动奖励礼包" , MAIL_CONTENTS_MAX_LEN );
// 				MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = msg.m_Goods[0];
// 				tmp.m_nCount = 1;
// 				tmp.m_nItemConfigID = 54005;
// 				tmp.m_nLimitTime = enForever;
// 				if( pZone->SendCmd( &msg , sizeof(msg) ) )
// 				{
// 					m_fullData.m_roleExtraData.m_szNormalFlags[3] = 1;
// 				}
// 			}
// 		}
// 	}
// 	else
// 	{
// #ifdef _HRX_DEBUG
// 		Global::logger->debug("[levelup_toten] clear levelup to ten falg");
// #endif
// 		;
// 	}

	return iLevelUp;
}

void 	MODI_ClientAvatar::SendOrBroadcast( void * p , size_t nSize )
{
	if( IsInMulitRoom() )
	{
		MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
		MODI_MultiGameRoom * pMultiRoom = pLobby->GetMultiRoom( GetRoomID() );
		if( pMultiRoom )
		{
			pMultiRoom->Broadcast( p , nSize );
		}
	}
	else 
	{
		SendPackage( p , nSize );
	}
}

void MODI_ClientAvatar::SendMdmToClient( defMusicID musicid )
{
	if( musicid == INVAILD_CONFIGID )
		return ;

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2C_Notify_MdmFile * p_notify = (MODI_GS2C_Notify_MdmFile *)(szPackageBuf);
	AutoConstruct( p_notify );
	char * pBuf = (char *)(&p_notify->m_pMdmData[0]);
	p_notify->m_nSize = Skt::MAX_USERDATASIZE - sizeof(MODI_GS2C_Notify_MdmFile);
	p_notify->m_musicid = musicid;
	if(	!MODI_MdmMgr::GetInstancePtr()->GetMdmBuffer( musicid , pBuf , p_notify->m_nSize ) )
	{
		Global::logger->warn("[%s] send client<charid=%u,name=%s> mdmfile faild, can't find musicid<%u>'s mdm file.",
			   	SVR_TEST , 
				GetCharID(),
				GetRoleName() ,
				musicid);
		p_notify->m_nSize = 0;
		return ;
	}
	int nSendSize = p_notify->m_nSize * sizeof(char) + sizeof(MODI_GS2C_Notify_MdmFile);
	SendPackage( p_notify, nSendSize );
}

void MODI_ClientAvatar::SetObjStateFlag( enObjStateMask mask )
{
	m_stateFlag.SetBit( mask );
}

void MODI_ClientAvatar::ClearObjStateFlag(enObjStateMask mask)
{
	m_stateFlag.ResetBit( mask );
}

bool MODI_ClientAvatar::IsObjStateFlag(enObjStateMask mask) const
{
	return	m_stateFlag.isSetBit( mask );
}

DWORD MODI_ClientAvatar::GetItemCountBySubClass( MODI_ItemType type )
{
	DWORD nPCount = GetPlayerBag().GetCountByItemType( type );
	DWORD nACount = GetAvatarBag().GetCountByItemType( type );
	return nPCount + nACount;
}

bool MODI_ClientAvatar::IsReadedHelp() const
{
	if( GetRoleDetailInfo().m_byReadHelp )
		return true;
	return false;
}

void MODI_ClientAvatar::SetReadHelp()
{
	if( IsReadedHelp() == false )
	{
		GetRoleDetailInfo_Modfiy().m_byReadHelp  = true;
	}
}

MODI_GameShop * MODI_ClientAvatar::GetShop()
{
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( IsInRareShop() )
	{
		return &pChannel->GetRareShop();
	}
	else if( IsInNormalShop() )
	{
		return &pChannel->GetNormalShop();
	}
  	return 0;
}

void 	MODI_ClientAvatar::ResetTodayOnlineDay(DWORD nSet)
{
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_timerUPOnlineTime.Reload( DODAY_ONLNIETIME_TIMER , pTick->GetTimer() );
	m_fullData.m_roleExtraData.m_nTodayOnlineTime = nSet;
}

void 	MODI_ClientAvatar::UpdateFangChenmi()
{
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	if( m_timerUPOnlineTime( pTick->GetTimer() ) )
	{
		m_fullData.m_roleExtraData.m_nTotalOnlineTime += 1;
		m_fullData.m_roleExtraData.m_nTodayOnlineTime += 1;
// 		/// 2小时发东西
// 		if(m_fullData.m_roleExtraData.m_nTodayOnlineTime > 5)
// 		{
// #ifdef _HRX_DEBUG
// 			Global::logger->debug("[today_online_time] more than 5");
// #endif			
// 			if(GetFullData().m_roleExtraData.m_szNormalFlags[4] == 0 )
// 			{
// #ifdef _HRX_DEBUG
// 			Global::logger->debug("[today_online_time] more than 5 and send a bag");
// #endif							
// 				// 送个礼包
// 				MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
// 				if( pZone )
// 				{
// 					MODI_S2ZS_Request_SendGiveItemMail msg;
// 					msg.m_nCount = 1;
// 					msg.m_shopmail = false;
// 					safe_strncpy( msg.m_szSenderName  , SYS_MAIL_SENDER ,  ROLE_NAME_MAX_LEN );
// 					safe_strncpy( msg.m_szReceverName , this->GetRoleName() , ROLE_NAME_MAX_LEN );
// 					safe_strncpy( msg.m_szContent ,  "冲等级活动奖励礼包" , MAIL_CONTENTS_MAX_LEN );
// 					MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = msg.m_Goods[0];
// 					tmp.m_nCount = 1;
// 					tmp.m_nItemConfigID = 54005;
// 					tmp.m_nLimitTime = enForever;
// 					if( pZone->SendCmd( &msg , sizeof(msg) ) )
// 					{
// 						m_fullData.m_roleExtraData.m_szNormalFlags[4] = 1;
// 					}
// 				}
// 			}
// 		}
// 		else
// 		{
// #ifdef _HRX_DEBUG
// 			Global::logger->debug("[today_online_time] no more than 5");
// #endif						
// 			m_fullData.m_roleExtraData.m_szNormalFlags[4] = 0;
			
// 		}

// #ifdef _DEBUG
// 		const DWORD nTiredTimeBegin = 1;
// 		const DWORD nTiredTimeEnd = 3;
// #else 
// 		const DWORD nTiredTimeBegin = 60 *  3;
// 		const DWORD nTiredTimeEnd = 60 * 5;
// #endif

		// // 是否开始防沉迷
// 		if( m_fullData.m_roleExtraData.m_bFangChenmi )
// 		{
// 			if( m_fullData.m_roleExtraData.m_nTodayOnlineTime >= nTiredTimeBegin &&
// 				m_fullData.m_roleExtraData.m_nTodayOnlineTime <= nTiredTimeEnd )
// 			{
// 				// 疲劳
// 				SetExpModifyFactor( 0.5f );
// 				SetMoneyFactor( 0.5f );


// 				if( m_fullData.m_roleExtraData.m_nTodayOnlineTime % 3 == 0 )
// 				{
// 					MODI_GS2C_Notify_FangChengMi fcm;
// 					fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enTired;
// 					fcm.m_nTodayPlayTime = m_fullData.m_roleExtraData.m_nTodayOnlineTime;
// 					SendPackage(&fcm,sizeof(fcm));
// 				}

// 				Global::logger->debug("[fangchenmi] client<charid=%u,name=%s> enter tired time.doday online <%u> minutes." ,
// 						GetCharID(),
// 						GetRoleName(),
// 						m_fullData.m_roleExtraData.m_nTodayOnlineTime );
// 			}
// 			else if( m_fullData.m_roleExtraData.m_nTodayOnlineTime > nTiredTimeEnd )
// 			{
// 				// 不健康
// 				SetExpModifyFactor( 0.0f );
// 				SetMoneyFactor( 0.0f );
				
// 				if( m_fullData.m_roleExtraData.m_nTodayOnlineTime % 5 == 0 )
// 				{
// 					MODI_GS2C_Notify_FangChengMi fcm;
// 					fcm.m_nTodayPlayTime = m_fullData.m_roleExtraData.m_nTodayOnlineTime;
// 					fcm.m_byState = MODI_GS2C_Notify_FangChengMi::enUnHelath;
// 					SendPackage(&fcm,sizeof(fcm));
// 				}

// 				Global::logger->debug("[fangchenmi] client<charid=%u,name=%s> enter unhealth time.doday online <%u> minutes." ,
// 						GetCharID(),
// 						GetRoleName(),
// 						m_fullData.m_roleExtraData.m_nTodayOnlineTime );
// 			}
// 		}
// 		else 
// 		{
// 			Global::logger->debug("[fangchenmi] client<charid=%u,name=%s> not need fangchenmi",
// 					GetCharID(),
// 					GetRoleName() );
// 		}
	}
}



/** 
 * @brief 系统公告
 * 
 */
bool 	MODI_ClientAvatar::SendSystemChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type)
{
	char buf[sizeof(MODI_GS2C_SystemChat_Cmd) + chat_size+1];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_SystemChat_Cmd * send_cmd = (MODI_GS2C_SystemChat_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_eType = notify_type;
	send_cmd->m_wdSize = chat_size+1;
	strncpy(send_cmd->m_stContent, chat_content, send_cmd->m_wdSize);
	return SendPackage(send_cmd, sizeof(buf));
}


/** 
 * @brief 进入频道执行脚本
 * 
 */
void MODI_ClientAvatar::FirstEnterGame()
{
	/// 脚本执行
#ifdef _HRX_DEBUG	
	Global::logger->debug("[enter_script] first enter game begin <client=%s>", GetRoleName());
#endif	
	MODI_TriggerEnter script(1);
	MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(this, script);
#ifdef _HRX_DEBUG	
	Global::logger->debug("[enter_script] first enter game end <client=%s>", GetRoleName());
#endif

	/// 所有数据加载完成
	m_blAllDataLoad = true;
}


/** 
 * @brief 是否有家族
 * 
 */
bool MODI_ClientAvatar::IsHaveFamily()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是族长
 *
 */
bool MODI_ClientAvatar::IsFamilyMaster()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Lead)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是副族长
 * 
 */
bool MODI_ClientAvatar::IsFamilySlaver()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_ViceLead)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是正式成员
 * 
 */
bool MODI_ClientAvatar::IsFamilyNormal()
{
	if((m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Normal)) ||
	   (m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Lead)) ||
	   (m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_ViceLead)))
	{
		return true;
	}
	return false;
}

/** 
 * @brief 是否是申请成员
 * 
 */
bool MODI_ClientAvatar::IsFamilyRequest()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Request))
	{
		return true;
	}
	return false;
}


/** 
 * @brief 获取家族id
 * 
 */
const DWORD MODI_ClientAvatar::GetFamilyID()
{
	return m_stCFamilyInfo.m_dwFamilyID;
}

/** 
 * @brief 获取家族名字
 * 
 */
const char * MODI_ClientAvatar::GetFamilyName()
{
	return m_stCFamilyInfo.m_cstrFamilyName;
}


/** 
 * @brief 是否发送家族名片
 * 
 */
void MODI_ClientAvatar::SetSendFCard(BYTE state)
{
	m_bySendFamilyData = state;
}


/** 
 * @brief 发送家族名片
 * 
 */
const BYTE MODI_ClientAvatar::GetSendFCard()
{
	return m_bySendFamilyData;
}


/** 
 * @brief 创建家族是否过了24小时
 * 
 */
bool MODI_ClientAvatar::IsCreateFamilyTimeout()
{
	std::string m_strName = create_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(p_var)
	{
		WORD var_value = p_var->GetValue();
		if(var_value > 0)
		{
			return false;
		}
	}
	return true;
}


/** 
 * @brief 曾经创建过家族
 * 
 */
void MODI_ClientAvatar::SetCreateFamilyTime()
{
	std::string m_strName = create_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(! p_var)
	{
		p_var = new MODI_GameUserVar(m_strName.c_str());
#ifndef _DEBUG
		std::string var_attribute = "type=time_time=86400";
#else
		std::string var_attribute = "type=time_time=120";
#endif
		if(p_var->Init(this, var_attribute.c_str()))
		{
			if(! m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				return;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
				p_var->SetValue(1);
				return;
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			return;
			MODI_ASSERT(0);
		}
	}
	else
	{
		p_var->SetValue(1);
	}
}


/** 
 * @brief 清楚创建过家族
 * 
 */
void MODI_ClientAvatar::ClearCreateFamilyTime()
{
	std::string m_strName = create_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(p_var)
	{
		p_var->SetValue(0);
	}
	else
	{
		MODI_ASSERT(0);
	}
}



/** 
 * @brief 申请家族是否过了1小时
 * 
 */
bool MODI_ClientAvatar::IsRequestFamilyTimeout()
{
	std::string m_strName = request_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(p_var)
	{
		WORD var_value = p_var->GetValue();
		if(var_value > 0)
		{
			return false;
		}
	}
	return true;
}


/** 
 * @brief 曾经申请过家族
 * 
 */
void MODI_ClientAvatar::SetRequestFamilyTime()
{
	std::string m_strName = request_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(! p_var)
	{
		p_var = new MODI_GameUserVar(m_strName.c_str());
		std::string var_attribute;
#ifndef _DEBUG
		if(IsVip())
		{
			var_attribute = "type=time_time=86400";
		}
		else
		{
			var_attribute = "type=time_time=1209600";
		}
#else
		if(IsVip())
		{
			var_attribute = "type=time_time=60";
		}
		else
		{
			var_attribute = "type=time_time=180";
		}
#endif		
		if(p_var->Init(this, var_attribute.c_str()))
		{
			if(! m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				return;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
				p_var->SetValue(1);
				return;
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			return;
			MODI_ASSERT(0);
		}
	}
	else
	{
		p_var->SetValue(1);
	}
}


/** 
 * @brief 清除申请过家族
 * 
 */
void MODI_ClientAvatar::ClearRequestFamilyTime()
{
	std::string m_strName = request_family_time_var;
	MODI_GameUserVar * p_var = m_stGameUserVarMgr.GetVar(m_strName.c_str());
	if(p_var)
	{
		p_var->SetValue(0);
	}
}


void MODI_ClientAvatar::RefreshBagInfoToClient()
{
#if 0	
	WORD	n=0;
	int 	isize=0;
	char	buf[Skt::MAX_PACKETSIZE];
		
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_Itemlist * notify_itemlist = (MODI_GS2C_Notify_Itemlist *)(buf);
	AutoConstruct( notify_itemlist);

	for( n=0;  n< GetAvatarBag().GetSize(); ++n)
	{
		MODI_GameItem * item = GetAvatarBag().GetItemByPos(n+1);
		if( item)
		{
			MODI_ItemInfoInBag 	info;	
			info.m_nBagType = item->GetBag()->GetBagType();
			info.m_nPosInBag = item->GetPosInBag();
			info.m_ItemGUID = item->GetGUID();
			info.m_nConfigID = item->GetConfigID();
			info.m_nCount = item->GetCount();
			info.m_nRemainTime = item->GetRemainTime();
			notify_itemlist->m_pItemlist[notify_itemlist->m_nItemSize] = info;
			
			notify_itemlist->m_nItemSize++;
		}
	}	

	for( n=0;  n< GetPlayerBag().GetSize(); ++n)
	{
		MODI_GameItem * item = GetPlayerBag().GetItemByPos(n+1);
		if( item)
		{
			MODI_ItemInfoInBag 	info;	
			info.m_nBagType = item->GetBag()->GetBagType();
			info.m_nPosInBag = item->GetPosInBag();
			info.m_ItemGUID = item->GetGUID();
			info.m_nConfigID = item->GetConfigID();
			info.m_nCount = item->GetCount();
			info.m_nRemainTime = item->GetRemainTime();
			notify_itemlist->m_pItemlist[notify_itemlist->m_nItemSize] = info;
			notify_itemlist->m_nItemSize++;
		}
	}
	isize = sizeof( MODI_GS2C_Notify_Itemlist);
	isize += sizeof(MODI_ItemInfoInBag ) * notify_itemlist->m_nItemSize;
	SendPackage(notify_itemlist,isize);
#endif	
}


/** 
 * @brief 进入个人空间
 * 
 */
void MODI_ClientAvatar::OnEnterZone()
{

	NotifyItemList();
	//RefreshBagInfoToClient();
}


/** 
 * @brief 通知道具列表
 * 
 */
void MODI_ClientAvatar::NotifyItemList()
{
	MODI_Bag * p_avatar_bag = NULL;
	p_avatar_bag = &GetAvatarBag();
	if(!p_avatar_bag)
	{
		MODI_ASSERT(0);
		return;
	}

	MODI_Bag * p_player_bag = NULL;
	p_player_bag = &GetPlayerBag();
	if(!p_player_bag)
	{
		MODI_ASSERT(0);
		return;
	}

	p_avatar_bag->NotifyItemList();
	p_player_bag->NotifyItemList();
}
