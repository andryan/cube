#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameChannelMgr.h"
#include "BinFileMgr.h"
#include "Channellist.h"
#include "s2adb_cmd.h"
#include "GMCommand.h"
#include "ZoneClient.h"
#include "s2zs_cmd.h"
#include "ClientAndZSDirection.h"
#include "protocol/c2gs_pay.h"
#include "protocol/c2gs_rank.h"
#include "Base/HelpFuns.h"
#include "Base/DisableStrTable.h"
#include "protocol/c2gs_Activity.h"
#include "ScriptManager.h"
#include "MakeManager.h"
#include "GameServerAPP.h"
#include "RandomBox.h"

MODI_PackageHandler_c2gs::MODI_PackageHandler_c2gs()
{
}

MODI_PackageHandler_c2gs::~MODI_PackageHandler_c2gs()
{

}

bool MODI_PackageHandler_c2gs::FillPackageHandlerTable()
{
	this->AddPackageHandler(MAINCMD_MUSIC , MODI_C2GS_Request_MusicHeaderData::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::MusicHeader_Handler);

	this->AddPackageHandler(MAINCMD_LOGIN , MODI_C2LS_Request_ChangeServerChannel::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::ChangeGameChannel_Handler);

	this->AddPackageHandler(MAINCMD_LOGIN , MODI_C2LS_Request_SelChannel::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::ReqChangeGameChannel_Handler);

	this->AddPackageHandler(MAINCMD_ROLE , MODI_C2GS_Request_ModifyRoleInfo::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::ModfiyRoleinfo_Handler);

    /*
     * 	聊天相关消息派发注册
     */
    this->AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_RangeChat::ms_SubCmd,
            &MODI_PackageHandler_c2gs::RangeChat_Handler);
    this->AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_GMCmd::ms_SubCmd,
            &MODI_PackageHandler_c2gs::GMCommand_Handler);
    /*
     *     大厅相关消息派发注册
     */
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_CreateRoom::ms_SubCmd,
            &MODI_PackageHandler_c2gs::CreateRoom_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_JoinRoom::ms_SubCmd,
            &MODI_PackageHandler_c2gs::JoinRoom_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_RandomJoinRoom::ms_SubCmd,
            &MODI_PackageHandler_c2gs::RandomJoinRoom_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_LeaveRoom::ms_SubCmd,
			&MODI_PackageHandler_c2gs::LeaveRoom_Handler);

    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_InvitePlay::ms_SubCmd,
			&MODI_PackageHandler_c2gs::InvitePlay_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_InvitePlayRes::ms_SubCmd,
			&MODI_PackageHandler_c2gs::InvitePlayRes_Handler);

    /*
     * 房间中相关操作
     */
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_Ready::ms_SubCmd,
			&MODI_PackageHandler_c2gs::Ready_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_UnReady::ms_SubCmd,
			&MODI_PackageHandler_c2gs::UnReady_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_Start::ms_SubCmd,
            &MODI_PackageHandler_c2gs::Start_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_KickPlayer::ms_SubCmd,
            &MODI_PackageHandler_c2gs::KickPlayer_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_OnPos::ms_SubCmd,
            &MODI_PackageHandler_c2gs::OnPostion_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_OffPos::ms_SubCmd,
            &MODI_PackageHandler_c2gs::OffPostion_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangeMusic::ms_SubCmd,
            &MODI_PackageHandler_c2gs::ChangeMusic_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangeMap::ms_SubCmd,
            &MODI_PackageHandler_c2gs::ChangeMap_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangeGameMode::ms_SubCmd,
            &MODI_PackageHandler_c2gs::ChangeGameMode_Handler);
    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangePostion::ms_SubCmd,
            &MODI_PackageHandler_c2gs::ChangePostion_Handler);
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangeRoomInfo::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ChangeRoomInfo_Handler);

	/*
	 * 游戏中的逻辑相关操作 
	 */

    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_FirstHalfReady::ms_SubCmd,
            &MODI_PackageHandler_c2gs::FirstHalfReady_Handler);

    this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_IamReady::ms_SubCmd,
            &MODI_PackageHandler_c2gs::IamReady_Handler);
	
	this->AddPackageHandler(MAINCMD_ROOM , MODI_C2GS_Request_EffectOver::ms_SubCmd ,
			&MODI_PackageHandler_c2gs::GameEffectOver );

	this->AddPackageHandler(MAINCMD_ROOM , MODI_C2GS_Request_EffectBegin::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::GameEffectBegin );

	this->AddPackageHandler(MAINCMD_ROOM , MODI_C2GS_Request_SyncGPower::ms_SubCmd , 
			&MODI_PackageHandler_c2gs::GameSyncGPower );

	/*
	 * 音乐相关的操作
	 */
	this->AddPackageHandler(MAINCMD_MUSIC , MODI_C2GS_Request_ChangeObSinger::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ChangeObSinger);
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_SentenceScore::ms_SubCmd,
			&MODI_PackageHandler_c2gs::SentenceScore_Handler);
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_OneKeyScore::ms_SubCmd,
			&MODI_PackageHandler_c2gs::OneKeyScore_Handler);
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ListenersOver::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ListernOver );
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_VoteTo::ms_SubCmd,
			&MODI_PackageHandler_c2gs::VoteTo_Handler );
	this->AddPackageHandler(MAINCMD_MUSIC,MODI_C2GS_Request_MicHasData::ms_SubCmd,
			&MODI_PackageHandler_c2gs::MicHasData_Handler );
	this->AddPackageHandler(MAINCMD_ROOM,MODI_C2GS_Request_GetMdm::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ReqMdmFile_Handler );


	/*
	   物品相关操作
  */

	this->AddPackageHandler(MAINCMD_ROLE , MODI_C2GS_Request_SyncAvatar::ms_SubCmd ,
			&MODI_PackageHandler_c2gs::ChangeAvatar );
	this->AddPackageHandler(MAINCMD_ITEM , MODI_C2GS_UseItem_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_c2gs::UseItem_Handler );

	this->AddPackageHandler(MAINCMD_ROLE , MODI_C2GS_Request_PlayExpression::ms_SubCmd,
			&MODI_PackageHandler_c2gs::PlayExpression_Handler );
	/// 

	this->AddPackageHandler(MAINCMD_RELATION , MODI_C2GS_Request_AddRelation::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_RELATION , MODI_C2GS_Request_ModifyRelation::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_RELATION , MODI_C2GS_Request_CanyouMarryMe::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_RELATION , MODI_C2GS_Request_AnserMarry::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_ROLE , MODI_C2GS_Request_RoleDetailInfo::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_PrivateChat_ByName::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_MAIL, MODI_C2GS_Request_SendMail::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_MAIL, MODI_C2GS_Request_SetReadMail::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_MAIL, MODI_C2GS_Request_DelMail::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_MAIL, MODI_C2GS_Request_GetAttachment::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

    this->AddPackageHandler(MAINCMD_ROLE, MODI_C2GS_Request_Movement::ms_SubCmd,
            &MODI_PackageHandler_c2gs::Movement_Handler );

	this->AddPackageHandler(MAINCMD_ROLE, MODI_C2GS_Request_Move_Cmd::ms_SubCmd,
            &MODI_PackageHandler_c2gs::RequestMove_Handler );

    this->AddPackageHandler(MAINCMD_ROLE, MODI_C2GS_Request_ReadHelp::ms_SubCmd,
            &MODI_PackageHandler_c2gs::ReadHelp_Handler );
    

	this->AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_SystemBroast::ms_SubCmd,
            &MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_SHOP, MODI_C2GS_Request_EnterShop::ms_SubCmd,
            &MODI_PackageHandler_c2gs::EnterShop_Handler );
	this->AddPackageHandler(MAINCMD_SHOP , MODI_C2GS_Request_Transaction::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ShopTransaction_Handler );
	this->AddPackageHandler(MAINCMD_SHOP , MODI_C2GS_Request_LeaveShop::ms_SubCmd,
			&MODI_PackageHandler_c2gs::LeaveShop_Handler );

	this->AddPackageHandler(MAINCMD_ITEM , MODI_C2GS_Request_ExchangeYunYingKey::ms_SubCmd,
			&MODI_PackageHandler_c2gs::YYExchangeKey_Handler );

	this->AddPackageHandler(MAINCMD_ITEM , MODI_C2GS_Request_RecordMusic::ms_SubCmd,
			&MODI_PackageHandler_c2gs::RecordMusic_Handler );

	this->AddPackageHandler(MAINCMD_RANK , MODI_C2GS_Request_Rank::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_RANK , MODI_C2GS_Request_SpecificRank::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_RANK , MODI_C2GS_Request_SelfRank::ms_SubCmd,
			&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	this->AddPackageHandler(MAINCMD_ROOM , MODI_C2GS_Request_TransferMaster::ms_SubCmd,
			&MODI_PackageHandler_c2gs::TransferMaster_Handler );
	
	this->AddPackageHandler(MAINCMD_ROOM , MODI_C2GS_Request_ModifyKeyModeParam::ms_SubCmd,
			&MODI_PackageHandler_c2gs::ModifyKeyModeParam_Handler );

	// 如果ADD失败，则让程序异常退出，否则一直返回TRUE

	/// 充值命令
	this->AddPackageHandler(MAINCMD_PAY, MODI_C2GS_Request_PayCmd::ms_SubCmd,
							&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );
							
	this->AddPackageHandler(MAINCMD_SYSTEM, MODI_C2GS_Point_Error_Cmd::ms_SubCmd,
							&MODI_PackageHandler_c2gs::PointError_Handler);

	/// 组队改变了
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Request_ChangeTeam::ms_SubCmd,
							&MODI_PackageHandler_c2gs::ChangeTeam_Handler);

	/// 家族相关命令
	/// 进入家族
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyIn::ms_SubCmd,
							&MODI_PackageHandler_c2gs::LoginFamily_Handler );
	/// 退出家族
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyOut::ms_SubCmd,
							&MODI_PackageHandler_c2gs::LogoutFamily_Handler );
	/// 请求创建家族
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_CreateFamily::ms_SubCmd,
							&MODI_PackageHandler_c2gs::CreateFamily_Handler );
	/// 某人对家族的操作
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyOpt::ms_SubCmd,
							&MODI_PackageHandler_c2gs::FamilyOpt_Handler );
	/// 对申请成员进行操作
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyOptRequest::ms_SubCmd,
							&MODI_PackageHandler_c2gs::RequestMemberOpt_Handler );
	/// 家族职位更改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyModifyPosition::ms_SubCmd,
							&MODI_PackageHandler_c2gs::FModifyPosition_Handler );
	/// 访问家族
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_VisitFamily::ms_SubCmd,
							&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );
	/// 家族名字检测
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyNameCheck::ms_SubCmd,
							&MODI_PackageHandler_c2gs::FamilyNameCheck_Handler);
	/// 家族宣言修改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilySetXuanyan::ms_SubCmd,
							&MODI_PackageHandler_c2gs::ModiFamilyXuanyan_Handler);
	/// 家族公告修改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilySetPublic::ms_SubCmd,
							&MODI_PackageHandler_c2gs::ModiFamilyPublic_Handler);
	/// 家族邀请
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyInvite::ms_SubCmd,
							&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );
	/// 家族邀请回答 
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_InvitionResponse::ms_SubCmd,
							&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );
	
	this->AddPackageHandler(MAINCMD_SYSTEM, MODI_C2GS_Request_Logout_Cmd::ms_SubCmd,
							&MODI_PackageHandler_c2gs::LogOut_Handler);
	this->AddPackageHandler(MAINCMD_ZONE, MODI_C2GS_Request_EnterZone::ms_SubCmd,
							&MODI_PackageHandler_c2gs::EnterZone_Handler);

	/// 增加ispeak标识命令
	this->AddPackageHandler(MAINCMD_ROOM, MODI_C2GS_Enter_ISpeak::ms_SubCmd,
							&MODI_PackageHandler_c2gs::EnterISpeak_Handler);
	this->AddPackageHandler(MAINCMD_ACTIVE, MODI_C2GS_Request_Activities::ms_SubCmd,&MODI_PackageHandler_c2gs::RequestActivities_Handler);

	this->AddPackageHandler(MAINCMD_ACTIVE, MODI_C2GS_Request_Signup::ms_SubCmd,&MODI_PackageHandler_c2gs::Signup_Handler);

	/// INA版本查询balance
	this->AddPackageHandler(MAINCMD_SHOP, MODI_C2GS_Req_Balance::ms_SubCmd,
							&MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle );

	/// 增加一个宝盒
	AddPackageHandler(MAINCMD_ITEM, MODI_C2GS_Request_AddItemBox::ms_SubCmd, & MODI_PackageHandler_c2gs::AddItemBox_Handler);
	
	/// 打开一个随机宝盒
	AddPackageHandler(MAINCMD_ITEM, MODI_C2GS_Request_OpenItemBox::ms_SubCmd, & MODI_PackageHandler_c2gs::OpenItemBox_Handler);

	

	/// 增加yy聊天标识
	this->AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_YYChatState_Cmd::ms_SubCmd,
							&MODI_PackageHandler_c2gs::YYChatState_Handler);
	return true;
}



int MODI_PackageHandler_c2gs::MusicHeader_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);

	if( pClient->IsHasMusicHeaderData() )
	{
		Global::logger->warn("[%s] client send music file header again. <RoleName = %s , GUID = %llu>" ,  
				LOGACT_DEBUG , 
				pClient->GetRoleName() , pClient->GetGUID().ToUint64() );
		return enPHandler_Warning;
	}
	
	MODI_C2GS_Request_MusicHeaderData & req = *((MODI_C2GS_Request_MusicHeaderData *)(pt_null_cmd));
	if( req.m_nMusicDataHeaderSize == 0 )
	{
		Global::logger->warn("[%s] client send null music file header  . <RoleName = %s , GUID = %llu> " , 
				LOGACT_DEBUG ,
			   pClient->GetRoleName() , pClient->GetGUID().ToUint64() );	
		return enPHandler_Warning;
	}
	else if( req.m_nMusicDataHeaderSize > MAX_MUSICFILE_HEADER )
	{
		Global::logger->warn("[%s] client send music file header , but it oversize . <RoleName = %s , GUID = %llu> " , 
				LOGACT_DEBUG ,
			   pClient->GetRoleName() , pClient->GetGUID().ToUint64() );	
	}
	else 
	{
		const char * pData = ( const char *)(&req.m_pMusicDataHeader[0]);
		//pClient->SetMusicDataHeader( pData , req.m_nMusicDataHeaderSize );
		pClient->SetMusicDataHeader( pData , 1);
		Global::logger->debug("[%s] client send music file header successful . <RoleName = %s , GUID = %llu> " , 
				LOGACT_DEBUG , 
				pClient->GetRoleName() , pClient->GetGUID().ToUint64() );
	}

	
	// 处于登入状态
	pClient->GetSession()->SwitchLoginedStatus();

	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::ChangeGameChannel_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2LS_Request_ChangeServerChannel  , cmd_size , 0, 0, pClient );

	// 是否正在房间中
	if( pClient->IsInMulitRoom() )
	{
		Global::logger->info("[%s] client<%s> is in the room<%u>. but send change game channel request." , 
				LOGIN_CHANGECHANNEL ,
			   	pClient->GetRoleName() , 
		   		pClient->GetRoomID());
		return enPHandler_OK;
	}

	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer )
		return enPHandler_OK;

	//  让 ADB 重新生成该帐号登入的PP
	MODI_S2ADB_Request_ReMakePP msg;
	msg.m_nAccountID = pClient->GetAccountID();
	msg.m_nServerID = GetChannelIDFromArgs();
	msg.m_nSessionID = pClient->GetSessionID();
	pZoneServer->SendCmd( &msg , sizeof(msg) );

	// 设置切换频道标志
	// 之后客户端无法进行任何逻辑操作
	pClient->GetSession()->SwitchRechangeChannelingStatus();

	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::ReqChangeGameChannel_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2LS_Request_SelChannel , cmd_size , 0 , 0 , pClient );

	const MODI_C2LS_Request_SelChannel * pReq = (const MODI_C2LS_Request_SelChannel *)(pt_null_cmd);

	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZoneServer || !(pReq->m_szNetType))
		return enPHandler_Kick;

	// 检查状态
	if( !pClient->GetSession()->IsInRechangeChannelingStatus() )
	{
		Global::logger->warn("[%s] client<%s> not in rechange channel status . but send RequestChangeChannel cmd." , 
				SVR_TEST ,
				pClient->GetRoleName() );
		return enPHandler_OK;
	}
	
	MODI_S2ZS_Request_SelectChannel msg2zs;
	msg2zs.m_guid = pClient->GetGUID();
	msg2zs.m_byServer = pReq->m_nChannelID;
	safe_strncpy(msg2zs.m_szNetType, pReq->m_szNetType, sizeof(msg2zs.m_szNetType));
	pZoneServer->SendCmd( &msg2zs , sizeof(msg2zs) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::GMCommand_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_GMCmd , cmd_size , 0 , 0 , pClient );
	
	// 判断是否有GM命令使用权限
	// to do ..
	if( pClient->GetFullData().m_roleExtraData.m_iGMLevel == 0)
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> use gm cmd.but no gm powr. " , SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_OK;
	}

// 	std::string gm_ip = pClient->GetSession()->GetSessionID().GetIPString();
// 	if(!(gm_ip == "61.152.171.91" || gm_ip == "211.144.127.237" || gm_ip == "218.242.148.34"
// 		 || gm_ip == "180.168.112.106" || gm_ip == "220.248.106.210" || gm_ip == "192.168.2.18"))
// 	{
// 		Global::logger->warn("[gmip_check] gm ip failed <name=%s,ip=%s>", pClient->GetRoleName(), gm_ip.c_str());
// 		return enPHandler_OK;
// 	}

	MODI_GMCmdHandler_gs  * p = MODI_GMCmdHandler_gs::GetInstancePtr();
	if( p )
	{
		const MODI_C2GS_Request_GMCmd * pReq = (const MODI_C2GS_Request_GMCmd *)(pt_null_cmd);
		p->SetClient( pClient );
		int iRes = p->DoHandleCommand( pReq->m_szCmd );
		Global::logger->info("[gm_cmd] gm client do cmd <name=%s,cmd=%s>", pClient->GetRoleName(), pReq->m_szCmd);
		if( iRes == enGMCmdHandler_InvalidCmd || iRes == enGMCmdHandler_SendToZone )
		{
			MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd,cmd_size );
		}
		p->SetClient(0);
	}

	return enPHandler_OK;
}


int MODI_PackageHandler_c2gs::ModfiyRoleinfo_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ModifyRoleInfo , cmd_size , 0 , 0 , pClient );

	MODI_GS2C_Respone_OptResult res;
	res.m_nResult = SvrResult_ModifyRoleinfo_Faild;
	const MODI_C2GS_Request_ModifyRoleInfo * pReq = (const MODI_C2GS_Request_ModifyRoleInfo *)(pt_null_cmd);
	size_t nLen =  strlen( pReq->m_Sign );
	if( nLen > MAX_PERSONAL_SIGN )
	{
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_Kick;
	}
	if( (pReq->m_byXuexing > enXuexingBegin && pReq->m_byXuexing < enXuexingEnd ) == false )
	{
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_Kick;
	}

	
	// modify role's detail infomations
	pClient->SetBlood( pReq->m_byXuexing );
	pClient->SetQQNum( pReq->m_qq );
	pClient->SetCity( pReq->m_nCityID );
	pClient->SetBirthDay( pReq->m_nBirthday );
	pClient->SetPersonalSign( pReq->m_Sign , nLen );

	res.m_nResult = SvrResult_ModifyRoleinfo_Ok;
	pClient->SendPackage( &res , sizeof(res) );
	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::DirectionToZoneServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , 
			pt_null_cmd,
			cmd_size );
	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::ReqMdmFile_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_GetMdm , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_GetMdm * pReq = (const MODI_C2GS_Request_GetMdm *)(pt_null_cmd);
	if( pClient->IsInMulitRoom() || pClient->IsInSingleGame() )
	{
//		if( pClient->IsInMulitRoom() )
//		{
//			MODI_MultiGameRoom * pRoom = pClient->GetMulitRoom();
//			if( pRoom && pRoom->IsStarting() )
//			{
//				pClient->SendMdmToClient( pReq->m_musicid );
//			}
//		}
//		else 
		{
			pClient->SendMdmToClient( pReq->m_musicid );
		}
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::ReadHelp_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ReadHelp , cmd_size , 0 , 0 , pClient );
	if( pClient->IsReadedHelp() == false )
	{
		pClient->SetReadHelp();
	}
	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::PointError_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Point_Error_Cmd , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Point_Error_Cmd * recv_cmd = (const MODI_C2GS_Point_Error_Cmd *)pt_null_cmd;
	Global::logger->fatal("[point_error] point error <name=%s,point=%u>", pClient->GetRoleName(), recv_cmd->m_dwPoint);
	return enPHandler_OK;
}
/** 
 * @brief 进入家族界面
 * 
 */
int MODI_PackageHandler_c2gs::LoginFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_FamilyIn, cmd_size , 0 , 0 , pClient );
	
	if(pClient->IsInFamily())
	{
		Global::logger->error("[login_family] login family but in family <name=%s>", pClient->GetRoleName());
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	pClient->LoginFamily();
	
	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;
}


/** 
 * @brief 退出家族界面
 * 
 */
int MODI_PackageHandler_c2gs::LogoutFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_FamilyOut, cmd_size , 0 , 0 , pClient );
	pClient->LogoutFamily();
	return enPHandler_OK;
}


/** 
 * @brief 创建家族
 * 
 */
int MODI_PackageHandler_c2gs::CreateFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_CreateFamily, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_CreateFamily * p_recv_cmd = (const MODI_C2GS_Request_CreateFamily *)pt_null_cmd;
	
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[create_family] create family command <name=%s,xuanyan=%s,public=%s>",
						  p_recv_cmd->m_szName, p_recv_cmd->m_szXuanyan, p_recv_cmd->m_szPublic);
#endif

// 	/// 等级和钱
	if(pClient->GetLevel() < CREATE_FAMILY_REQ_LEVEL)
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Level);
		return enPHandler_OK;
	}

	if( pClient->GetMoney() < CREATE_FAMILY_REQ_MONEY )
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Money);
		return enPHandler_OK;
	}

	/// 是否过了24小时
	if(! pClient->IsCreateFamilyTimeout())
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Time);
		return enPHandler_OK;
	}

	/// 是否在家族
	if(! pClient->IsInFamily())
	{
		Global::logger->error("[create_family] not in family but send create family command <name=%s>", pClient->GetRoleName());
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	/// 是否有家族
	if(pClient->IsHaveFamily())
	{
		if(pClient->IsFamilyRequest())
		{
			pClient->RetFamilyResult(enFamilyRetT_All_Request);
		}
		else
		{
			pClient->RetFamilyResult(enFamilyRetT_All_Have);
		}
		return enPHandler_OK;
	}

	/// 参数是否正确
	MODI_DisableStrTable * p_dst = MODI_DisableStrTable::GetInstancePtr();
	if( !p_dst )
	{
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	/// 名字检查
	if( p_dst->CheckDirtyWord( p_recv_cmd->m_szName))
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Name);
		return enPHandler_OK;
	}
	/// 宣言检查
	if( p_dst->CheckDirtyWord( p_recv_cmd->m_szXuanyan))
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Xuanyan);
		return enPHandler_OK;
	}
	/// 公告检查
	if( p_dst->CheckDirtyWord( p_recv_cmd->m_szPublic))
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Public);
		return enPHandler_OK;
	}

	/// 名字太小
	if(strlen(p_recv_cmd->m_szName) < FAMILY_NAME_LESS)
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_Name);
		return enPHandler_OK;
	}
	
	pClient->DecMoney( CREATE_FAMILY_REQ_MONEY);
	pClient->SetCreateFamilyTime();

#ifdef _FB_LOGGER
	Global::fb_logger->debug("\t%u\tCREATES FAMILY\t%d\t%s\t%s",Global::m_stRTime.GetSec(),pClient->GetCharID(),pClient->GetAccName(),p_recv_cmd->m_szName);
#endif
	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;
}



/** 
 * @brief 个人对家族的操作
 *
 * 申请家族，取消申请，离开家族，解散家族
 *
 */
int MODI_PackageHandler_c2gs::FamilyOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_FamilyOpt, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_FamilyOpt * p_recv_cmd = (const MODI_C2GS_Request_FamilyOpt *)pt_null_cmd;
	enFamilyOptType opt_type = p_recv_cmd->m_enOptType;
 
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[family_opt] family opt command <opt=%d>", opt_type);
#endif

	if(! pClient->IsInFamily())
	{
		Global::logger->debug("[family_opt] client not in family bot send family opt command <%s>", pClient->GetRoleName());
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	/// 申请加入家族
	if(opt_type == enFamilyOpt_Request)
	{
		if(pClient->IsFamilyNormal())
		{
			pClient->RetFamilyResult(enFamilyRetT_All_Have);
			return enPHandler_OK;
		}
		
		if(pClient->GetFamilyID() == p_recv_cmd->m_dwFamilyID)
		{
			pClient->RetFamilyResult(enFamilyRetT_All_Have);
 			return enPHandler_OK;
		}

		if(pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel  < 3)
		{
			std::ostringstream os;
			os<<"Your level less than 3,can't join in the family";
			pClient->SendSystemChat(os.str().c_str(), os.str().size());
			return enPHandler_OK;
		}
		
		/// 时间24未到
		if(! pClient->IsRequestFamilyTimeout())
		{
			pClient->RetFamilyResult(enFamilyRetT_Request_Time);
			return enPHandler_OK;
		}

		pClient->SetRequestFamilyTime();
		
		MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
		return enPHandler_OK;
	}
	
	/// 取消申请
	else if(opt_type == enFamilyOpt_Cancel)
	{
		if(p_recv_cmd->m_dwFamilyID != pClient->GetFamilyID())
		{
			Global::logger->warn("[cancel_family] cancel family but family not equ <cancelid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}

		if(! pClient->IsFamilyRequest())
		{
			Global::logger->warn("[cancel_family] cancel family but position not request <cancelid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		
		MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
		return enPHandler_OK;
	}
	
	/// 离开家族
	else if(opt_type == enFamilyOpt_Leave)
	{
		if(p_recv_cmd->m_dwFamilyID != pClient->GetFamilyID())
		{
			Global::logger->warn("[leave_family] leave family but not in family <freeid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		if(!pClient->IsFamilyNormal())
		{
			Global::logger->warn("[leave_family] leave family but is request <freeid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
#ifdef _FB_LOGGER
		Global::fb_logger->debug("\t%u\tPLAYER LEAVES FAMILY\t%d\t%s\t%d ",Global::m_stRTime.GetSec(),pClient->GetCharID(),pClient->GetAccName(),pClient->GetFamilyID());
#endif	
		MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
		return enPHandler_OK;
		
	}
	/// 解散家族
	else if(opt_type == enFamilyOpt_Free)
	{
		if(p_recv_cmd->m_dwFamilyID == INVAILD_FAMILY_ID)
		{
			Global::logger->warn("[free_family] free family but family id invaild <name=%s>", pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		if(p_recv_cmd->m_dwFamilyID != pClient->GetFamilyID())
		{
			Global::logger->warn("[free_family] free family but not in family <freeid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		if(! pClient->IsFamilyMaster())
		{
			pClient->RetFamilyResult(enFamilyRetT_Free_Auth);
			return enPHandler_OK;
		}
		
		MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
		return enPHandler_OK;
		
	}
	else if(opt_type == enFamilyOpt_Level)
	{
		if(p_recv_cmd->m_dwFamilyID == INVAILD_FAMILY_ID)
		{
			Global::logger->warn("[level_family] level family but family id invaild <name=%s>", pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		if(p_recv_cmd->m_dwFamilyID != pClient->GetFamilyID())
		{
			Global::logger->warn("[level_family] level family but not in family <freeid=%u,familyid=%u,name=%s>",
								 p_recv_cmd->m_dwFamilyID, pClient->GetFamilyID(), pClient->GetRoleName());
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		if(! (pClient->IsFamilyMaster() || pClient->IsFamilySlaver()))
		{
			pClient->RetFamilyResult(enLevelFamily_NoAuth);
			return enPHandler_OK;
		}

		if(Global::g_byGameShop == 2)
		{
			if(pClient->GetRMBMoney() < 1000)
			{
				pClient->RetFamilyResult(enLevelFamily_NoMoney);
				return enPHandler_OK;
			}
		}
		else if(Global::g_byGameShop == 1)
		{
			if(pClient->GetRMBMoney() < 4000)
			{
				pClient->RetFamilyResult(enLevelFamily_NoMoney);
				return enPHandler_OK;
			}
		}
		else
		{
			if(pClient->GetRMBMoney() < nsFamilyLevel::LEVEL_MONEY)
			{
				pClient->RetFamilyResult(enLevelFamily_NoMoney);
				return enPHandler_OK;
			}
		}
		
		MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
		return enPHandler_OK;
	}
	
	/// 打开家族信息共享
	else if(opt_type == enFamilyOpt_Open)
	{
		
	}
	/// 关闭家族信息
	else if(opt_type == enFamilyOpt_Close)
	{
		
	}
	return enPHandler_OK;
}


/** 
 * @brief 对申请成员的批准
 * 
 */
int MODI_PackageHandler_c2gs::RequestMemberOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												 const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_FamilyOptRequest, cmd_size , 0 , 0 , pClient );

	//const MODI_C2GS_Request_FamilyOptRequest * p_recv_cmd = (const MODI_C2GS_Request_FamilyOptRequest *)pt_null_cmd;

	if(! pClient->IsInFamily())
	{
		Global::logger->error("[create_family] not in family but send create family command <name=%s>", pClient->GetRoleName());
		//pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	/// 是族长或者副族长
	if(! (pClient->IsFamilyMaster() || pClient->IsFamilySlaver()))
	{
		pClient->RetFamilyResult(enFamilyRetT_Free_Auth);
		return enPHandler_OK;
	}

	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;	
}


/** 
 * @brief 职位修改
 * 
 */
int MODI_PackageHandler_c2gs::FModifyPosition_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
												 const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_FamilyModifyPosition, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_FamilyModifyPosition * p_recv_cmd = (const MODI_C2GS_Request_FamilyModifyPosition *)pt_null_cmd;

	if(! pClient->IsInFamily())
	{
		Global::logger->error("[create_family] not in family but send create family command");
		//pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	/// 是族长或者副族长
	if(! pClient->IsFamilyMaster())
	{
		pClient->RetFamilyResult(enFamilyRetT_Free_Auth);
		return enPHandler_OK;
	}

	if(p_recv_cmd->m_stGuid == pClient->GetGUID())
	{
		Global::logger->error("[modi_family] modi family position is myself");
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		return enPHandler_OK;
	}

	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;	
}



/** 
 * @brief 家族名字检查
 * 
 */
int MODI_PackageHandler_c2gs::FamilyNameCheck_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_FamilyNameCheck, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_FamilyNameCheck * p_recv_cmd = (const MODI_C2GS_Request_FamilyNameCheck*)pt_null_cmd;

	/// 参数是否正确
	MODI_DisableStrTable * p_dst = MODI_DisableStrTable::GetInstancePtr();
	if( !p_dst )
	{
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	/// 名字检查
	char check_name[FAMILY_NAME_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_szName, sizeof(check_name) - 1);
	if( p_dst->CheckDirtyWord( check_name))
	{
		pClient->RetFamilyResult(enFamilyRetT_CheckN_Failed);
		return enPHandler_OK;
	}

	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;		
}


/** 
 * @brief 修改宣言
 * 
 */
int MODI_PackageHandler_c2gs::ModiFamilyXuanyan_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	if(! pClient->IsFamilyMaster())
	{
		return enPHandler_OK;
	}
	
	CHECK_PACKAGE(MODI_C2GS_Request_FamilySetXuanyan, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_FamilySetXuanyan * p_recv_cmd = (const MODI_C2GS_Request_FamilySetXuanyan *)pt_null_cmd;

	/// 参数是否正确
	MODI_DisableStrTable * p_dst = MODI_DisableStrTable::GetInstancePtr();
	if( !p_dst )
	{
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	/// 名字检查
	char check_name[FAMILY_XUANYAN_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_cstrXuanyan, sizeof(check_name) - 1);
	if( p_dst->CheckDirtyWord( check_name))
	{
		pClient->RetFamilyResult(enFamilyRetT_CheckN_Failed);
		return enPHandler_OK;
	}

	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;		
}


/** 
 * @brief 修改公告
 * 
 */
int MODI_PackageHandler_c2gs::ModiFamilyPublic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	if(! pClient->IsFamilyMaster())
	{
		return enPHandler_OK;
	}
	
	CHECK_PACKAGE(MODI_C2GS_Request_FamilySetPublic, cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_FamilySetPublic * p_recv_cmd = (const MODI_C2GS_Request_FamilySetPublic *)pt_null_cmd;

	/// 参数是否正确
	MODI_DisableStrTable * p_dst = MODI_DisableStrTable::GetInstancePtr();
	if( !p_dst )
	{
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	/// 名字检查
	char check_name[FAMILY_PUBLIC_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_cstrPublic, sizeof(check_name) - 1);
	if( p_dst->CheckDirtyWord( check_name))
	{
		pClient->RetFamilyResult(enFamilyRetT_CheckN_Failed);
		return enPHandler_OK;
	}

	MODI_ClientAndZSDirection::ClientToZoneServer( pClient , pt_null_cmd, cmd_size );
	return enPHandler_OK;		
}


int MODI_PackageHandler_c2gs::LogOut_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{

	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_Logout_Cmd , cmd_size , 0 , 0 , pClient );
	//const MODI_C2GS_Request_Logout_Cmd * recv_cmd = (const MODI_C2GS_Request_Logout_Cmd *)pt_null_cmd;
	Global::logger->info("[LogOut] client logout normally  <sessionid=%u,accid=%u,ip=%s,lastloginip=%s,lastlogintime=%u)" ,
			pClient->GetSessionID().GetSeq(),
			pClient->GetAccountID(),
			pClient->GetSessionID().GetIPString(),
			pClient->GetFullData().m_roleExtraData.m_szLastLoginIP,
			pClient->GetFullData().m_roleExtraData.m_nLastLoginTime
			);
	
	pClient->SetLogoutNormal(true);

	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::EnterZone_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
//	MODI_C2GS_Request_EnterZone *p_recv_cmd=(MODI_C2GS_Request_EnterZone *)pt_null_cmd;
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_EnterZone,cmd_size,0,0,pClient);
	pClient->OnEnterZone();
	return enPHandler_OK;
}



int MODI_PackageHandler_c2gs::FirstHalfReady_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
//	MODI_C2GS_Request_FirstHalfReady *p_recv_cmd=(MODI_C2GS_Request_FirstHalfReady *)pt_null_cmd;
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	MODI_ASSERT(pClient);
	CHECK_PACKAGE(MODI_C2GS_Request_FirstHalfReady,cmd_size,0,0,pClient);

	MODI_GS2C_Notify_LoadSecondHalf second;
	MODI_GS2C_Notify_SbReady sbrd;
//	MODI_GS2C_Notify_SbFirstHalfReady sbrd;
//	MODI_GS2C_Notify_S
	sbrd.m_nClientID = pClient->GetGUID();
	MODI_MultiGameRoom *room=pClient->GetMulitRoom();	
	if( !room)
	{
		return 0;
	}
	room->Broadcast(&sbrd,sizeof(MODI_GS2C_Notify_SbReady));
	pClient->SendPackage(&second,sizeof(MODI_GS2C_Notify_LoadSecondHalf));	
	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::ModifyKeyModeParam_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
		
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	MODI_C2GS_Request_ModifyKeyModeParam *p_recv_cmd=(MODI_C2GS_Request_ModifyKeyModeParam *) pt_null_cmd;
	CHECK_PACKAGE(MODI_C2GS_Request_ModifyKeyModeParam,cmd_size,0,0,pClient);
	if( p_recv_cmd->m_eVolType >=  enKeyDownVol_End || p_recv_cmd->m_eVolType < enKeyDownVol_None)
	{
		Global::logger->debug("[modifykey] client %s request %uto modify key vold typemode  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`",pClient->GetRoleName(),p_recv_cmd->m_eVolType);
		return 0;
	}
	pClient->SetKeyVolType(p_recv_cmd->m_eVolType);
	pClient->SetSped( p_recv_cmd->m_fSped);
	
	MODI_MultiGameRoom *room=pClient->GetMulitRoom();	
	if( !room)
	{
		Global::logger->debug("[modifykey] client %s request to modify key mode without a  room ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`",pClient->GetRoleName());
		return 0;
	}
	room->SyncKeyModeParam(pClient);	

	return enPHandler_OK;
}


/// 增加一个ispeak记录
int MODI_PackageHandler_c2gs::EnterISpeak_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	
	const MODI_C2GS_Enter_ISpeak *p_recv_cmd = (const MODI_C2GS_Enter_ISpeak *) pt_null_cmd;
	CHECK_PACKAGE(MODI_C2GS_Enter_ISpeak, cmd_size, 0, 0, pClient);
	if(pClient->GetGUID() != p_recv_cmd->m_stId.m_stGuid)
	{
		//MODI_ASSERT(0);
		return false;
	}
	
	MODI_MultiGameRoom  * room = pClient->GetMulitRoom();	
	if( !room)
	{
		Global::logger->fatal("[enter_ispeak] client %s request to enter ispeak without a  room",pClient->GetRoleName());
		//MODI_ASSERT(0);
		return enPHandler_OK;
	}

	if(! room->AddISpeakId(p_recv_cmd->m_stId))
	{
		//MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	return enPHandler_OK;
}


/// 增加yy聊天标识
int MODI_PackageHandler_c2gs::YYChatState_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	
	const MODI_C2GS_YYChatState_Cmd *p_recv_cmd = (const MODI_C2GS_YYChatState_Cmd *) pt_null_cmd;
	CHECK_PACKAGE(MODI_C2GS_YYChatState_Cmd, cmd_size, 0, 0, pClient);
	if(pClient->GetGUID() != p_recv_cmd->m_stState.m_stGuid)
	{
		return false;
	}
	
	MODI_MultiGameRoom  * room = pClient->GetMulitRoom();	
	if( !room)
	{
		Global::logger->fatal("[enter_ispeak] client %s request to enter ispeak without a  room",pClient->GetRoleName());
		//MODI_ASSERT(0);
		return enPHandler_OK;
	}

	if(! room->ChangeYYChatState(p_recv_cmd->m_stState))
	{
		//MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	return enPHandler_OK;
}


int  MODI_PackageHandler_c2gs::RequestActivities_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	MODI_TriggerActivity  script(1);
	MODI_ActivityManager::GetInstance().Execute(pClient,script);
	return enPHandler_OK;
}
int  MODI_PackageHandler_c2gs::Signup_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{

//	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
//	MODI_TriggerActivity  script(2);
//	MODI_ActivityManager::GetInstance().Execute(pClient,script);

	return enPHandler_OK;
}


/// 增加一个随机宝盒
int MODI_PackageHandler_c2gs::AddItemBox_Handler(void *pObj, const Cmd::stNullCmd * pt_null_cmd,    const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar * >(pObj);
	MODI_Bag *p_bag  = &(pClient->GetPlayerBag());
	if( !p_bag)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
	if(!pApp)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_CreateItemInfo  create_info;
	create_info.m_dwConfigId = 56006;//MODI_ConfigInfo::GetInstance()->m_RandBoxConfig.m_nluckyconfigid;
	create_info.m_byLimit = 3;
	create_info.m_enCreateReason = enAddReason_SystemSend;
	create_info.m_byServerId = pApp->GetServerID();
	QWORD   new_item_id=0;
	if( !p_bag->AddItemToBag(create_info, new_item_id))
	{
		Global::logger->fatal("[add_item_from_package] add item direct failed <id=%u,limit=%u>", create_info.m_dwConfigId,
								  create_info.m_byLimit);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	return enPHandler_OK;
}


/// 开启宝盒
int MODI_PackageHandler_c2gs::OpenItemBox_Handler(void *pObj, const Cmd::stNullCmd * pt_null_cmd,  const unsigned int cmd_size)
{
    MODI_ClientAvatar * p_client = static_cast<MODI_ClientAvatar *>(pObj);
    MODI_C2GS_Request_OpenItemBox  * p_recv_cmd  = (MODI_C2GS_Request_OpenItemBox *)pt_null_cmd;
	if(p_recv_cmd->m_qwBagId == 0 )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
    MODI_GameItemInfo  * p_item = p_client->GetPlayerBag().GetItemByItemId(p_recv_cmd->m_qwBagId);
	if( !p_item)
	{
		Global::logger->debug("[openitem_box] can't find the correct itembox ");
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	// if(p_item->GetConfigId() != MODI_ConfigInfo::GetInstance()->m_RandBoxConfig.m_nluckyconfigid)
	if(p_item->GetConfigId() != 56006)
	{
		Global::logger->debug("[openitem_box] please give the right itembox id  ");
		return enPHandler_OK;
	}

    for(int i=0; i< BOX_NEED_YINFU_NUM; ++i)
	{
		MODI_GameItemInfo * p_item_yinfu =p_client->GetPlayerBag().GetItemByItemId(p_recv_cmd->m_qwYinFu[i]);
		if( !p_item_yinfu)
		{
			Global::logger->debug("[openitem_box] can't find the corrected yinfu item  ");
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		
		//if( p_item_yinfu->GetConfigId() != MODI_ConfigInfo::GetInstance()->m_RandBoxConfig.m_nyinfuconfigid)
		if( p_item_yinfu->GetConfigId() != 56005)
		{
			Global::logger->debug("[openitem_box] we need four yinfu item to open  ");
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
	}

	MODI_RandomBoxMgr::GetInstance()->OpenItemBox(pObj,pt_null_cmd,cmd_size);
    return enPHandler_OK;
}
