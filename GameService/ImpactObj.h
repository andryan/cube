/** 
 * @file ImpactObj.h
 * @brief 游戏中的效果对象
 * @author Tang Teng
 * @version v0.1
 * @date 2010-06-29
 */

#ifndef MODI_IMPACTOBJ_H_
#define MODI_IMPACTOBJ_H_


#include "gamestructdef.h"
#include "BaseObject.h"


class MODI_ClientAvatar;
class MODI_ImpactTemplate;

// 效果参数对象
struct 	MODI_ImpactArg 
{
	union
	{
		int 	nIntValue;
		DWORD 	nDwordValue;
		float 	fFloatValue;
		long 	lLongValue;
	};

	DWORD 	GetDWORD() const { return nDwordValue; }
	int 	GetInt() const { return nIntValue; }
	float 	GetFloat() const { return fFloatValue; }
	long 	GetLong() const { return lLongValue; }

	void 	SetDWORD( DWORD n ) { nDwordValue = n; }
	void  	SetInt( int n ) { nIntValue = n; }
	void 	SetFloat( float n ) { fFloatValue = n; }
	void 	SetLong( long n ) { lLongValue = n; }
};

// 效果对象
class  	MODI_ImpactObj : public MODI_BaseObject
{
	public:

		MODI_ImpactObj();
		MODI_ImpactObj(MODI_GUID id);
		virtual ~MODI_ImpactObj();

		void 		SetGUID( const MODI_GUID & guid );

		WORD 		GetConfigID() const
		{
			return m_nConfigID;
		}

		void 		SetConfigID(WORD nConfigID)
		{
			m_nConfigID = nConfigID;
		}

		WORD 		GetClientID() const
		{
			return m_nClientID; 
		}

		void 		SetClientID(WORD nID)
		{
			m_nClientID = nID;
		}

		const MODI_GUID & GetCasterGUID() const
		{
			return m_nCasterGUID;
		}

		void 		SetCasterGUID(MODI_GUID guid)
		{
			m_nCasterGUID = guid;
		}

		DWORD 		GetContinuance() const
		{
			return m_nContinuance;
		}

		bool 		IsContinue() const 
		{
			return m_nContinuance != 0;
		}

		void 		SetContinuance(DWORD nTime)
		{
			m_nContinuance = nTime;
		}

		DWORD 		GetContinuanceElapsed() const
		{
			return m_nContinuanceElapsed;
		}

		void 		SetContinuanceElapsed(DWORD nTime)
		{
			m_nContinuanceElapsed = nTime;
		}

		DWORD 		GetIntervalElapsed()const
		{
			return m_nIntervalElapsed;
		}

		void 		SetIntervalElapsed(DWORD nTime)
		{
			m_nIntervalElapsed = nTime;
		}

		void 		SetOutGameFadeOut( bool bSet ) 
		{
			m_bOutGameFadeout = bSet;
		}
			
		bool 		IsOutGameFadeOut() const
		{
			return m_bOutGameFadeout; 
		}
	
		void 		SetArg( DWORD nIdx , const MODI_ImpactArg & arg );
		void 		SetArgInt( DWORD nIdx , int nSet );
		void 		SetArgDWORD( DWORD nIdx , DWORD nSet );
		void 		SetArgFloat( DWORD nIdx , float fSet );
		void 		SetArgLong( DWORD nIdx , long lSet );

		const MODI_ImpactArg * 	GetArg( DWORD nIdx );
		bool 	 	GetArgInt( DWORD nIdx , int & nOut ) const;
		bool 		GetArgDWORD( DWORD nIdx , DWORD & nOut ) const;
		bool 		GetArgFloat( DWORD nIdx , float & fOut ) const;
		bool 		GetArgLong( DWORD nIdx , long & lOut ) const;

		bool 		IsValidArgIndex( DWORD nIdx ) const;

		void * 		GetUserdata() const { return m_pUserdata; }
		void 		SetUserdata( void * p ) { m_pUserdata = p; }

	private:

		WORD 		m_nConfigID; 	// 效果configid
		WORD 		m_nClientID;
		MODI_GUID 	m_nCasterGUID; 	// 效果释放者
		DWORD		m_nContinuance; // 效果持续时间
		DWORD 		m_nContinuanceElapsed; // 效果逝去时间
		DWORD 		m_nIntervalElapsed; // 效果间隔性触发逝去时间
		MODI_ImpactArg  m_Param[IMPACT_MAX_PARAMS]; // 参数
		void 	* 	m_pUserdata;
		bool 		m_bOutGameFadeout; 	
};


class 	MODI_ImpactObjPool
{
	public:

		static MODI_ImpactObj * 	New();
		static void 				Delete( MODI_ImpactObj * p );

};








#endif
