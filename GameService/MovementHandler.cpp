#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"
#include "Scene.h"

int MODI_PackageHandler_c2gs::Movement_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_Movement , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_Movement * pReq = (const MODI_C2GS_Request_Movement *)(pt_null_cmd);
	
	MODI_MultiGameRoom * pRoom = pClient->GetMulitRoom();
	if( !pRoom )
	{
		return enPHandler_OK;
	}

	if( pRoom->IsStart() )
	{
		Global::logger->warn("[%s] room<%u> is start , but client<charid=%u,name=%s> send move package." , 
				SVR_TEST ,
				pRoom->GetRoomID(),
				pClient->GetCharID(),
				pClient->GetRoleName() );

		return enPHandler_OK;
	}

	if( pClient->IsObjStateFlag( kObjState_ChangeAvatar ) )
	{
		return enPHandler_OK;
	}

	pClient->IncLogicCount();

	MODI_GS2C_Notify_Movement pos_msg;
	pos_msg.m_obj = pClient->GetGUID();
	pos_msg.m_Flag = pReq->m_Flag;
	pos_msg.m_fDir = pReq->m_fDir;
	pos_msg.m_fPosX = pReq->m_fPosX;
	pos_msg.m_fPosZ = pReq->m_fPosZ;
	pos_msg.m_fTime = pReq->m_fTime;
	pos_msg.m_nClientLogicCount = pReq->m_nClientLogicCount;
	pos_msg.m_nServerLogicCount = pClient->GetLogicCount();

	pClient->SetPosX( pReq->m_fPosX );
	pClient->SetPosZ( pReq->m_fPosZ );
	pClient->SetDir( pReq->m_fDir );
	pClient->SetMovementFlag( pReq->m_Flag );

	pRoom->Broadcast_Except( &pos_msg ,sizeof(pos_msg) , pClient );
#if 0
	Global::logger->debug("[%s] client<charid=%u,name=%s> sync movement<x=%3f,z=%3f,dir=%3f,clogic_count=%u,slogic_count=%u>." , 
			SVR_TEST ,
			pClient->GetCharID(),
			pClient->GetRoleName(),
			pClient->GetPosX(),
			pClient->GetPosZ(),
			pClient->GetDir(),
			pos_msg.m_nClientLogicCount,
			pos_msg.m_nServerLogicCount);
#endif
	return enPHandler_OK;
}




int MODI_PackageHandler_c2gs::RequestMove_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_Move_Cmd , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_Move_Cmd * pReq = (const MODI_C2GS_Request_Move_Cmd *)(pt_null_cmd);
	
	MODI_MultiGameRoom * pRoom = pClient->GetMulitRoom();
	if( !pRoom )
	{
		return enPHandler_OK;
	}

	if( pRoom->IsStart() )
	{
		Global::logger->warn("[%s] room<%u> is start , but client<charid=%u,name=%s> send move package." , 
				SVR_TEST ,
				pRoom->GetRoomID(),
				pClient->GetCharID(),
				pClient->GetRoleName() );

		return enPHandler_OK;
	}

	if( pClient->IsObjStateFlag( kObjState_ChangeAvatar ) )
	{
		return enPHandler_OK;
	}

#ifdef _DEBUG1
	Global::logger->debug("[recv_move_cmd] <srcx=%f,srcy=%f,fx=%f,fy=%f,playerfx=%f,playerfy=%f>",
						  pReq->m_stSrcState.m_Pos.x, pReq->m_stSrcState.m_Pos.y,pReq->m_stSrcState.m_fDir.x,pReq->m_stSrcState.m_fDir.y,
						  pReq->m_stSrcState.m_fCharDir.x,pReq->m_stSrcState.m_fCharDir.y);
#endif	
	
	///改变自己的属性
	pClient->SetMoveState(pReq);

	/// 广播给9屏玩家开始移动
	pRoom->Broadcast_Except( pt_null_cmd ,cmd_size , pClient );

#if 0	
	MODI_Scene * p_scene = pRoom->GetRoomScene();
	if(p_scene)
	{
		p_scene->SendCmdToNine(pClient, pt_null_cmd, cmd_size);
	}
#endif
	
	return enPHandler_OK;
}
