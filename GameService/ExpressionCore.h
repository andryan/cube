/** 
 * @file ExpressionCore.h
 * @brief 表情模块
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */
#ifndef MODI_EXPRESSION_CORE_H_
#define MODI_EXPRESSION_CORE_H_

#include "BaseOS.h"

class 	MODI_ClientAvatar;

class 	MODI_ExpressionCore
{
	public:


	/** 
	 * @brief 
	 * 
	 * @param pClient
	 * 
	 * @return 	
	 */
	static int 		IsCanPlayExpression( MODI_ClientAvatar * pClient );

	/** 
	 * @brief 播放表情
	 * 
	 * @param pClient 	要播放表情的客户端对象
	 * @param byIndex 	表情索引
	 * 
	 * @return 	
	 */
	static 	int 	PlayExpression( MODI_ClientAvatar * pClient , BYTE byIndex );

};

#endif
