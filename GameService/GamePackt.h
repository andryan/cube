#include "BinFileMgr.h"
#include "ClientAvatar.h"
#include "GameChannel.h"
#include "protocol/gamedefine.h"
#include <list>
#include "Base/Define.h"
using namespace std;


class  MODI_DynamicPackt 
{
public:
	typedef	list<PacketInfo>	PACKET_LIST;
	typedef	list<PacketInfo>::iterator	PACKET_ITOR;	

	static MODI_DynamicPackt * GetInstance();

	///	当系统初始化的时候
	bool	Init();
	void	ReGenPacket();
	void 	BroadCastForChange();
	void	LoginNotify(MODI_ClientAvatar * avatar);
	bool 	ReloadPackt();
	///	当玩家登陆后，需要发送所有的包给他
	void	OnPlayerLogin(MODI_ClientAvatar * m_pclient);

private:
	MODI_DynamicPackt();
	~MODI_DynamicPackt();
	static MODI_DynamicPackt * m_pInstance;	
	char	m_szPacket[Skt::MAX_USERDATASIZE];
	PACKET_LIST	m_lList;
};
