/**
 * @file   MakeManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Mar 22 11:24:58 2011
 * 
 * @brief  条件动作制造
 * 
 */


#ifndef _MD_MAKEMANAGER_H
#define _MD_MAKEMANAGER_H

#include "Global.h"
#include "AssertEx.h"
#include "Condition.h"
#include "Action.h"


/** 
 * @brief 制造模板
 * 
 * 
 */
template <class A>
class MODI_Make
{
public:
	virtual A *  Make() = 0;
	virtual ~MODI_Make(){}
};


/** 
 * @brief 条件制造
 * 
 * 
 * @return 
 */
template <class A >
class MODI_ConditionMake: public MODI_Make<MODI_Condition>
{
 public:
	A * Make()
	{
		return new A();
	}
};


/** 
 * @brief 动作制造
 * 
 * 
 */
template <class A>
class MODI_ActionMake: public MODI_Make<MODI_Action>
{
 public:
	A * Make()
	{
		return new A();
	}
};


/** 
 * @brief 制造管理
 * 
 * 
 */
template <typename m_name, typename m_make>
class MODI_MakeManager
{
public:
	typedef MODI_MakeManager<m_name, m_make> self_t;

	static self_t & GetInstance()
	{
		if (!m_pInstance)
		{
			m_pInstance = new self_t;
		}
		
		return *m_pInstance;
	}


	/** 
	 * @brief 获取一个制造者
	 * 
	 * @param _name 制造者的名字
	 * 
	 * @return 制造厂
	 *
	 */
	m_make * GetMake(const m_name & _name) const
	{		
		const_list_iterator iter = m_stList.find(_name);
		if(iter != m_stList.end())
		{
			return iter->second;
		}
		return NULL;		
	}


	/** 
	 * @brief 条件注册
	 * 
	 */
	void RegCondition();



	/** 
	 * @brief 动作注册
	 * 
	 */
	void RegAction();

	/** 
	 * @brief 注册成员
	 * 
	 */
	bool RegMake(const m_name & _name, m_make * p_make)	
	{
		const_list_iterator iter = m_stList.find(_name);
		if(iter != m_stList.end())
		{
			MODI_ASSERT(0);
			return false;
		}
		m_stList[_name] = p_make;
		return true;
	}
	
private:
	
	MODI_MakeManager(){}
	~MODI_MakeManager(){}
	
	static self_t * m_pInstance;
	
	typedef std::map<m_name, m_make* > LIST;
	typedef typename LIST::iterator list_iterator;
	typedef typename LIST::const_iterator const_list_iterator;
	LIST m_stList;
};


template<typename m_name, typename m_make>
MODI_MakeManager<m_name, m_make> * MODI_MakeManager<m_name, m_make>::m_pInstance = NULL;

/// 条件制造管理
typedef MODI_MakeManager<std::string, MODI_Make<MODI_Action> > MODI_MakeActionManager;

/// 动作制造管理
typedef MODI_MakeManager<std::string, MODI_Make<MODI_Condition> > MODI_MakeConditionManager;

#endif


