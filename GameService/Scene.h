/**
 * @file   Scene.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Dec 14 12:02:16 2011
 * 
 * @brief  游戏中的场景
 * 
 * 
 */


#ifndef _MD_SCENE_H_
#define _MD_SCENE_H_

#define SCREEN_WIDTH 10
#define SCREEN_HEIGHT 10

#include "Global.h"
#include "ClientAvatar.h"

class MODI_MultiGameRoom;


/// 场景上某个点
struct MODI_ScenePos
{
	/// 属性值
	BYTE m_byAttr;
};


/**
 * @brief 地图上面的场景
 * 
 */
class MODI_Scene
{
 public:
	MODI_Scene(): m_stSyncClientPos(10 * 1000)
	{
		m_pGameRoom = NULL;
		m_wdSceneId = 0;
		m_blUsed = 0;
	}
	
	~MODI_Scene(){}
	/// 某屏里面的物品索引
	typedef std::set<MODI_MoveableObject * > defSceneAttrSet;
	typedef std::map<DWORD, defSceneAttrSet > defSceneAttrIndex;
	
	/// 九屏索引
	typedef std::map<DWORD, std::vector<DWORD> > defNineSceneIndex;
	typedef std::map<DWORD, std::vector<DWORD> >::iterator defNineSceneIndexIter;
	typedef std::map<DWORD, std::vector<DWORD> >::value_type defNineSceneIndexValue;

	void Reset();

	MODI_MultiGameRoom * GetSceneRoom();
	void SetSceneRoom(MODI_MultiGameRoom * p_room);
	
	/** 
	 * @brief 初始化场景
	 * 
	 * @param file_name 场景文件名
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool Init(const char * file_name, const WORD scene_id);


	/** 
	 * @brief 物品更新
	 * 
	 * @param p_attr 需要更新的物品
	 * 
	 * @return 成功ture
	 *
	 */
	bool UpdateAttr(MODI_MoveableObject * p_attr);

	bool SyncAttr(MODI_MoveableObject * p_attr);

	void SyncMeToNine(const DWORD screen, MODI_MoveableObject * p_attr);


	/** 
	 * @brief 删除某个物品
	 * 
	 * @param p_attr 场景里面的物品
	 * 
	 * @return 成功ture
	 *
	 */
	bool RemoveAttr(MODI_MoveableObject * p_attr);


	/** 
	 * @brief 是否能够移动
	 * 
	 * @param pos 移动位置
	 * 
	 * @return 能true
	 *
	 */
	bool IsCanMove(const int x, const int y);


	/** 
	 * @brief 尝试移动
	 * 
	 * @param src_state 移动前初始状态
	 * @param cmp_state 目标比较状态
	 * @param dest_state 移动结果
	 * 
	 * @return 有移动改变ture,无移动改变false
	 *
	 */
	bool CheckMove(MODI_MoveState & src_state, const MODI_MoveState & cmp_state);

	void AllAttrStop();


	void DelMeToNine(const DWORD screen, MODI_MoveableObject * p_attr);

	void SendNineToMe(const DWORD screen, MODI_MoveableObject * p_attr);

	void SendMeToNine(const DWORD screen, MODI_MoveableObject * p_attr);

	DWORD GetScreenNum(const DWORD x, const DWORD y);

	void SyncAllPos();

	void SendCmdToNine(MODI_MoveableObject * p_attr, const stNullCmd * pt_null_cmd, const DWORD cmd_size);
	
	
	void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);

	void Broadcast(const stNullCmd * pt_null_cmd, const DWORD cmd_size);
	
	DWORD GetSceneId()
	{
		return m_wdSceneId;
	}

	bool IsUsed()
	{
		return m_blUsed;
	}

	void SetUsed(bool is_used)
	{
		m_blUsed = is_used;
	}

	const WORD GetType()
	{
		return m_wdType;
	}

	void SetType(const WORD scene_type)
	{
		m_wdType = scene_type;
	}

	
 private:
	/** 
	 * @brief 加载场景文件信息
	 *
	 * @param file_name 场景信息文件名
	 *
	 * @return 成功true,失败false
	 *
	 */
	bool LoadSceneFile(const char * file_name);


	/** 
	 * @brief 九屏索引
	 * 
	 * @return 成功
	 *
	 */
	bool LoadNineScreen();
	
	
	/// 地图文件名
	std::string m_strMapFileName;
	
	/// 地图文件点信息
	std::vector<MODI_ScenePos > m_ScenePosVec;

	std::vector<DWORD > m_SceneBornVec;

	/// 地图宽高
	DWORD m_dwWidth;
	DWORD m_dwHeight;
	
	/// 屏幕的宽和高
	DWORD m_dwSceneWidth;
	DWORD m_dwSceneHeight;
	DWORD m_dwSceneMax;

	
	///  所有物品索引
	defSceneAttrSet m_AllSceneAttr[enSceneAttrMax];

	/// 屏为key的物品索引
	defSceneAttrIndex m_SceneAttrIndex[enSceneAttrMax];
	
	/// 9屏索引
	defNineSceneIndex m_NineSceneIndex;

	MODI_MultiGameRoom * m_pGameRoom;

	MODI_Timer 	m_stSyncClientPos;

	WORD m_wdSceneId;
	WORD m_wdType;
	
	bool m_blUsed;
	
};

#endif
