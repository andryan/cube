#include "GMCommand.h"
#include "AssertEx.h"
#include "protocol/c2gs_chatcmd.h"
#include "BinFileMgr.h"
#include "HelpFuns.h"
#include "ClientAvatar.h"
#include "GUIDCreator.h"
#include "GameChannel.h"
#include "ItemOperator.h"
#include "Base/GameConstant.h"



MODI_GMCmdHandler_gs::MODI_GMCmdHandler_gs():m_pClient(0)
{

}

MODI_GMCmdHandler_gs::~MODI_GMCmdHandler_gs()
{


}

void 	MODI_GMCmdHandler_gs::SetClient( MODI_ClientAvatar * pClient )
{
	m_pClient = pClient;
}

MODI_ClientAvatar * 	MODI_GMCmdHandler_gs::GetClient()
{
	return m_pClient;
}

bool MODI_GMCmdHandler_gs::FillCmdHandlerTable()
{
	static const MODI_GMCommand Commands[]=
	{
		{"-help", &(MODI_GMCmdHandler_gs::ShowHelp) ,"display help message" , "usage: -help" },

		{"-channelinfo", &(MODI_GMCmdHandler_gs::ShowChannelInfo) ,
			"display game server infomations " , "usage: -channelinfo" },

		{"-zoneinfo", &(MODI_GMCmdHandler_gs::ShowZoneInfo) ,
			"display zone infomations " , "usage: -zoneinfo" },

//	 * format: -clientinfo PLAYER_NAME 
		{"-clientinfo", &(MODI_GMCmdHandler_gs::ShowClientInfo) ,
			"display a special client's infomations " , "usage: -clientinfo PLAYER_NAME " },

//	 * format: -additem PLAYER_NAME , ITEM_CONFIGID , ITEM_COUNT <default 1> , ITEM_REMAINTIME <default forever>
		{"-additem", &(MODI_GMCmdHandler_gs::AddItem) ,
			"add a item to a special client" ,
		   	"usage: -additem PLAYER_NAME , ITEM_CONFIGID , ITEM_COUNT <default 1> , ITEM_REMAINTIME <default forever>" },

//	 * format: -delitem PLAYER_NAME ,  ITEM_POS_INBAG , BAG_TYPE(1=avatar_bag,2=player_bag <default 2>) ,
		{"-delitem", &(MODI_GMCmdHandler_gs::DelItem) ,
			"del a item from a special client's bag" ,
		   	"usage: -additem PLAYER_NAME , BAG_TYPE(1=avatar_bag,2=player_bag <default 2>), ITEM_POS_INBAG" },
		{"-useitem", &(MODI_GMCmdHandler_gs::UseItem) ,
			"use a item from client's player bag" ,
		   	"usage: -additem ITEM_POS_INBAG , TARGET_NAME" },
		{"-roominfo", &(MODI_GMCmdHandler_gs::RoomInfo) ,
			"get room information form room's id." ,
		 	"usage: -roominfo ROOM_ID " },
		{"-setccnum", &(MODI_GMCmdHandler_gs::SetChannelClientNum) ,
			"set channel client num" ,
		 "usage: -setccnum maxnum " },
		{"-setcv",&(MODI_GMCmdHandler_gs::SetConstantValue) ,
			"set game constant vlaue.",
			"usage: -setcv VALUENAME NEWVALUES" },
		{"-setattr",&(MODI_GMCmdHandler_gs::SetAttr) ,
			"set avatar attr.",
			"usage: -setattr VALUENAME NEWVALUES" },
		{"-addfensi",&(MODI_GMCmdHandler_gs::AddFensi),
			"add fensigirl count.",
			"usage: -addfensi  USERNAME "},
		{"-addblack",&(MODI_GMCmdHandler_gs::AddFensi),
			"add add black .",
			"usage: -addblack  USERNAME "},
		{"-decblack",&(MODI_GMCmdHandler_gs::AddFensi),
			"dec black .",
			"usage: -decblack  USERNAME "},
	};
	int iCount = sizeof(Commands) / sizeof(MODI_GMCommand);

	for( int i = 0; i < iCount; i++ )
	{
		this->AddCmdHandler( Commands[i].cmd , Commands[i] );
	}

	return true;
}

// 显示帮助明
int MODI_GMCmdHandler_gs::ShowHelp( int nArgc ,const char ** szArgs )
{
	if( !nArgc )
	   	return enGMCmdHandler_InvalidParam;

	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	/// > 2 才能看
	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}

	MODI_GS2C_Notify_GMCmdResult result;
	MAP_HANDLER_ITER itor = MODI_GMCmdHandler_gs::GetInstancePtr()->m_mapHandlers.begin();
	while( itor != MODI_GMCmdHandler_gs::GetInstancePtr()->m_mapHandlers.end() )
	{
		const MODI_GMCommand & cmd = itor->second;
		SNPRINTF( result.m_szResult ,
				sizeof(result.m_szResult) , 
				"Command: %s , Des: %s , Usage: %s ." ,
				cmd.cmd , cmd.description , cmd.usage );
		pClient->SendPackage( &result , sizeof(result) );

		itor++;
	}
	
	return enGMCmdHandler_SendToZone; 
}

int MODI_GMCmdHandler_gs::ShowChannelInfo( int nArgc ,const char ** szArgs)
{

	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );
	
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	
	const unsigned int player_num = pChannel->Size();
	const unsigned int lobby_player_num = pLobby->GetLobbyPlayerNum();
	const BYTE room_num = pLobby->GetRoomSize();
	
	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult , sizeof(result.m_szResult) ,
			  "this channel have rooms(%d), players(%u), in lobby players(%u), in rooms players(%u)",
			  room_num,
			  player_num,
			  lobby_player_num, (player_num-lobby_player_num));
	pClient->SendPackage( &result , sizeof(result) );
	Global::logger->debug("[channel_info] this channel have rooms(%d), players(%u), in lobby players(%u), in rooms players(%u)",
						  room_num,
						  player_num,
						  lobby_player_num, (player_num-lobby_player_num));
	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_gs::ShowZoneInfo( int nArgc ,const char ** szArgs)
{
	return enGMCmdHandler_SendToZone;
}


int MODI_GMCmdHandler_gs::ShowClientInfo( int nArgc ,const char ** szArgs )
{
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_GS2C_Notify_GMCmdResult result;
	const char * szRoleName = szArgs[1];
	MODI_ClientAvatar * pDestClient = pChannel->FindClientByName( szRoleName );
	if( !pDestClient )
	{
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't get player's info , player<%s> is't online " ,
				szRoleName );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;
	}

	const MODI_ClientAvatarData & av = pDestClient->GetRoleNormalInfo().m_avatarData;
	bool bInPlayerPos = RoleHelpFuns::IsSpectator(pDestClient->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家
 	defRoomPosSolt nPosInRoom = RoleHelpFuns::GetPosSolt(pDestClient->GetRoleNormalInfo().m_roomSolt); // 位置索引
	SNPRINTF( result.m_szResult , sizeof(result.m_szResult) ,
			"name=%s,sex=%u,level=%u,exp=%u,roomid=%u,isplayer=%d,pos_inroom=%u,age=%u,blood=%u,city=%u,qq=%u.\
			<head_id=%u,hair_id=%u,upper_id=%u,lower_id=%u,hand_id=%u,foot_id=%u,headdress=%u,\
			glass=%u,earring=%u,mic=%u,mousedress_id=%u,necklave_id=%u,watch_id=%u,ring_id=%u,back_id=%u,\
			pet_id=%u,tail_id=%u>" ,
			pDestClient->GetRoleBaseInfo().m_cstrRoleName ,
			pDestClient->GetRoleBaseInfo().m_ucSex,
			pDestClient->GetRoleBaseInfo().m_ucLevel,
			pDestClient->GetRoleNormalInfo().m_nExp,
			pDestClient->GetRoomID() , 
			(int)(bInPlayerPos),
			nPosInRoom,
			pDestClient->GetRoleDetailInfo().m_byAge,
			pDestClient->GetRoleDetailInfo().m_byBlood,
			pDestClient->GetRoleDetailInfo().m_nCity,
			pDestClient->GetRoleDetailInfo().m_nQQ ,
			av.m_Avatars[enAvatarT_Head - 1] ,
			av.m_Avatars[enAvatarT_Hair - 1] ,
			av.m_Avatars[enAvatarT_Upper - 1] ,
			av.m_Avatars[enAvatarT_Lower - 1] ,
			av.m_Avatars[enAvatarT_Hand - 1] ,
			av.m_Avatars[enAvatarT_Foot - 1] ,
			av.m_Avatars[enAvatarT_Headdress - 1] ,
			av.m_Avatars[enAvatarT_Glass - 1] ,
			av.m_Avatars[enAvatarT_Earring - 1] ,
			av.m_Avatars[enAvatarT_Mic - 1] ,
			av.m_Avatars[enAvatarT_Mousedress - 1] ,
			av.m_Avatars[enAvatarT_Necklace - 1] ,
			av.m_Avatars[enAvatarT_Watch - 1] ,
			av.m_Avatars[enAvatarT_Ring - 1] ,
			av.m_Avatars[enAvatarT_Back - 1] ,
			av.m_Avatars[enAvatarT_Pet - 1] ,
			av.m_Avatars[enAvatarT_Tail - 1] 
			);
	pClient->SendPackage( &result , sizeof(result) );

	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_gs::AddItem( int nArgc ,const char ** szArgs )
{
#if 0	
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
//	  format: -additem PLAYER_NAME , ITEM_CONFIGID , ITEM_COUNT <default 1> , ITEM_REMAINTIME <default forever>
	if( nArgc < 3 )
		return enGMCmdHandler_InvalidParam;

	const char * szRoleName = szArgs[1];
	MODI_GameChannel * pGameChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pDestClient = pGameChannel->FindClientByName( szRoleName );
	if( !pDestClient )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't create item , player<%s> is't online " ,
				szRoleName );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;
	}

	// 物品类型
	DWORD nConfigID = 0;
	if( !safe_strtoul( szArgs[2] , &nConfigID ) )
		return enGMCmdHandler_InvalidParam;

	DWORD nCount = 1;
	DWORD nTime = enForever;
	if( nArgc >= 4 )
	{
		safe_strtoul( szArgs[3] , &nCount ); 

		if( nArgc >= 5 )
		{
			safe_strtoul( szArgs[4] , &nTime ); 
		}
	}

	int iRet = MODI_ItemOperator::CreateItemAddToBag( 
			&(pDestClient->GetPlayerBag()) ,
			nConfigID ,
			nCount ,
			nTime );

	if( iRet == MODI_ItemOperator::enInvalidParam )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't create item , invalid create param." );

		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_InvalidParam;
	}
	else if( iRet == MODI_ItemOperator::enBagPosNotEnough )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't create item , bag is full.<%u> " ,
				nConfigID );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_InvalidParam;
	}
	else if( iRet == MODI_ItemOperator::enSubClassItemMax )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't create item , sub class is max.<%u> " ,
				nConfigID );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_InvalidParam;
	}
	MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"add item successful , player<%s>" ,
				szRoleName );
		pClient->SendPackage( &result , sizeof(result) );
#endif
	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_gs::DelItem( int nArgc ,const char ** szArgs )
{
#if 0	
//	 * format: -delitem PLAYER_NAME ,  ITEM_POS_INBAG , BAG_TYPE(1=avatar_bag,2=player_bag <default 2>) ,

	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 3 )
		return enGMCmdHandler_InvalidParam;

	MODI_GameChannel * pGameChannel = MODI_GameChannel::GetInstancePtr();
	const char * szRoleName = szArgs[1];
	MODI_ClientAvatar * pDestClient = pGameChannel->FindClientByName( szRoleName );
	if( !pDestClient )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"can't del item , player<%s> is't online " ,
				szRoleName );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;
	}

	DWORD nPos = INVAILD_POS_INBAG;
	if( !safe_strtoul( szArgs[2] , &nPos ) )
		return enGMCmdHandler_OK;

	MODI_Bag * bag = &(pDestClient->GetAvatarBag());
	if( nArgc >= 4 )
	{
		DWORD nPagType = 0;
		if( safe_strtoul( szArgs[3] , &nPagType ) && nPagType == enBagType_PlayerBag )
		{
			bag = &(pDestClient->GetPlayerBag());
		}
	}

	MODI_GameItem * pItem = bag->GetItemByPos( nPos );
	if( pItem )
	{
		pItem->DeleteFromDB();
	}
#endif
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_gs::UseItem( int nArgc ,const char ** szArgs )
{
#if 0	
//	 * format: -useitem ITEM_POS_INBAG  TARGET_NAME
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	DWORD nPos = INVAILD_POS_INBAG;
	if( !safe_strtoul( szArgs[1] , &nPos ) )
		return enGMCmdHandler_InvalidParam;
	if( nPos == INVAILD_POS_INBAG )
		return enGMCmdHandler_InvalidParam;

	MODI_GUID targetguid = pClient->GetGUID();
	if( nArgc >= 3 )
	{
		MODI_GameChannel * pGameChannel = MODI_GameChannel::GetInstancePtr();
		const char * szTargetName = szArgs[2];
		MODI_ClientAvatar * pDestClient = pGameChannel->FindClientByName( szTargetName );
		if( !pDestClient )
		{
			MODI_GS2C_Notify_GMCmdResult result;
			SNPRINTF( result.m_szResult , 
					sizeof(result.m_szResult) , 
					"can't use item , target player<%s> is't online " ,
					szTargetName );
			pClient->SendPackage( &result , sizeof(result) );
			return enGMCmdHandler_OK;
		}
		targetguid = pDestClient->GetGUID();
	}

	int iRet = MODI_ItemOperator::UseItem( pClient , nPos , targetguid , 0 , true );
	if( iRet != MODI_ItemOperator::enOK )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				" use item faild. " );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;
	}
#endif
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_gs::RoomInfo( int nArgc ,const char ** szArgs )
{
//	 * format: -roominfo ROOMID
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if( nArgc < 1 )
		return enGMCmdHandler_InvalidParam;

	DWORD nRoomID = INVAILD_ROOM_ID;
	if( !safe_strtoul( szArgs[1] , &nRoomID ) )
		return enGMCmdHandler_InvalidParam;
	if( nRoomID == INVAILD_ROOM_ID )
		return enGMCmdHandler_InvalidParam;

	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	MODI_MultiGameRoom * room = pLobby->GetMultiRoom( nRoomID );
	if( !room )
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) , 
				"room <id=%u> is't exist. " ,
				nRoomID );
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;
	}

	BYTE byPlayerNum = room->GetCurPlayersNum();
	WORD nSpectatorNum = room->GetCurSpectatorsNum();
	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult , 
			sizeof(result.m_szResult) , 
			"room <id=%u> has players: %u , spectators : %u , master : %s " ,
			nRoomID,
			byPlayerNum , 
			nSpectatorNum ,
			room->GetMasterName() );
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_gs::SetChannelClientNum( int nArgc ,const char ** szArgs)
{
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );
	
	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	return enGMCmdHandler_SendToZone;
}

int MODI_GMCmdHandler_gs::SetConstantValue( int nArgc , const char ** szArgs )
{
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	using namespace MODI_GameConstant;
	const char * szValueName = szArgs[1];
	const char * szNewValue = szArgs[2];
	enConstantValueType vt;
	
	if( get_ValueType_ByName( szValueName , vt ) == false )
	{
		Global::logger->info("[%s] gm<charid=%u,name=%s> try SetConstantValue by <valuename=%s,newvalue=%s>" , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				szValueName,
				szNewValue );

		return enGMCmdHandler_InvalidParam;
	}

	if( vt == kCVT_DWORD )
	{
		DWORD n = 0;
		if( safe_strtoul(  szNewValue , &n ) )
		{
			set_DWORDValue_ByName( szValueName ,  n );
		}
	}
	else if( vt == kCVT_INT )
	{
		long i = 0;
		if( safe_strtol(  szNewValue , &i ) )
		{
			set_IntValue_ByName( szValueName ,  i );
		}
	}
	else if( vt == kCVT_FLOAT )
	{
		float f = atof( szNewValue );
		set_FloatValue_ByName( szValueName , f );
	}
	else 
	{
		MODI_ASSERT(0);
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_success] deal gm command successful.");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_SendToZone;
}

int MODI_GMCmdHandler_gs::SetAttr( int nArgc , const char ** szArgs )
{
	MODI_ClientAvatar * pClient = MODI_GMCmdHandler_gs::GetInstancePtr()->GetClient();
	if(! pClient)
	{
		MODI_ASSERT( 0 );
		return enGMCmdHandler_InvalidParam;
	}

	if(! (pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1))
	{
		Global::logger->debug("gmlevel < 2 <role=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}

	if( nArgc < 3 )
		return enGMCmdHandler_InvalidParam;

	std::string attr_name = szArgs[1];

	if(attr_name == "test")
	{
		return enGMCmdHandler_OK;
	}
	else
	{
		return enGMCmdHandler_SendToZone;
	}
	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_gs::AddFensi(int nArgc, const char **szArgs)
{

	return enGMCmdHandler_SendToZone;

}

