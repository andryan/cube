#include "GUIDCreator.h"
#include "AssertEx.h"
#include "Global.h"
#include "IAppFrame.h"
#include "s2rdb_cmd.h"
#include "ZoneClient.h"
#include "gamestructdef.h"


MODI_GUIDCreator::MODI_GUIDCreator( BYTE nWorldID , defServerChannelID nSvrID ):	
	m_nWorldID( nWorldID ),
	m_nChannelID( nSvrID ),
	m_nItemIDLayer(1),
	m_nLastSaveDBItemID(1),
	m_nImpactIDLayer(1),
	m_nLastImpactGUID(1)
{
	MODI_ENTER_FUNCTION
	if( m_nChannelID == INVAILD_SVRCHANNELID ||
		m_nWorldID == 0 )
		throw "invaild world/server id on instance MODI_GUIDCreator object";
	MODI_LEAVE_FUNCTION
}


MODI_GUID 	MODI_GUIDCreator::CreatePlayerGUID( MODI_CHARID nCharID )
{
	MODI_GUID 	guid = INVAILD_GUID;

	MODI_ENTER_FUNCTION

	if( nCharID == INVAILD_CHARID )
	{
		MODI_ASSERT( 0 );
	}
	else 
	{
		guid = MAKE_GUID( nCharID , enObjType_Player  );
	}

	MODI_LEAVE_FUNCTION

	return guid;
}


MODI_GUID 	MODI_GUIDCreator::CreateItemGUID()
{
	MODI_GUID guid = INVAILD_GUID;
	MODI_ENTER_FUNCTION
	
	if( m_nLastItemID == 0xFFFFFFFF )
	{
		Global::logger->error("[%s] can't create item's guid. item low guid is to overflow." ,  ERROR_CGUID );
		MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
		pApp->Terminate();
//		m_nItemIDLayer += 1;
//		m_nLastItemID = 1;
		// 考虑增加id_layer ..
//		throw "can't create item's guid. item low guid is to overflow.";
	}
	else 
	{
		if( EncodeItemGUID( guid , GetWorldID() , GetServerID() , m_nLastItemID ) )
		{
			m_nLastItemID += 1;
		}
		else 
		{
			MODI_ASSERT(0);
		}
	}

	MODI_LEAVE_FUNCTION
	return guid;
}
		
MODI_GUID 	MODI_GUIDCreator::CreateImpactGUID()
{
	MODI_GUID guid = INVAILD_GUID;
	MODI_ENTER_FUNCTION
	
	if( m_nLastImpactGUID == 0xFFFFFFFF )
	{
		if( m_nImpactIDLayer >= 255 )
		{
			Global::logger->error("[%s] can't create impact's guid. impact low guid is to overflow." ,  ERROR_CGUID );
			MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
			pApp->Terminate();
		}
		else 
		{
			m_nImpactIDLayer += 1;
			m_nLastImpactGUID = 1;
		}
	}
	else 
	{
		guid = MAKE_GUID( m_nLastImpactGUID++ , enObjType_Impact );
	}

	MODI_LEAVE_FUNCTION
	return guid;
}

void 		MODI_GUIDCreator::OnLoadLastItemGUIDFromDB( void * p )
{
	const MODI_RDB2S_Notify_ItemSerial * pMsg = (const MODI_RDB2S_Notify_ItemSerial *)(p);
	if( pMsg )
	{
		if( pMsg->m_bySuccessful )
		{
			m_nLastSaveDBItemID = pMsg->m_nItemSerial;
			SetLastItemGUID( pMsg->m_nItemSerial );
			Global::logger->info("[%s] load last item serial<%u> from db successufl ." ,  
					GAMEDB_OPT ,
					m_nLastSaveDBItemID );
		}
		else 
		{
			Global::logger->error("[%s] load last item serial from db faild , shutdown server ." ,  
					GAMEDB_OPT );
			MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
			pApp->Terminate();
		}
	}
}

void MODI_GUIDCreator::SaveLastItemSerialToDB( bool bForce )
{
	if( m_nLastSaveDBItemID == m_nLastItemID )
		return ;

	MODI_ZoneServerClient * pZoneServer = MODI_ZoneServerClient::GetInstancePtr();
	if( pZoneServer )
	{
		MODI_S2RDB_Request_SaveSerial msg;
		msg.m_byServerID = GetServerID();
		msg.m_byWorldID = GetWorldID();
		msg.m_byForce = bForce;
		msg.m_nItemSerial = m_nLastItemID;
		m_nLastSaveDBItemID = m_nLastItemID;
		pZoneServer->SendCmd( &msg ,sizeof(msg) );
		Global::logger->info("[%s] Save GameItemSerial<%u> To DB." , 
					SVR_TEST ,
					m_nLastItemID );
	}
}
