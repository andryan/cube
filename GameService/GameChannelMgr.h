///**
// * @file GameChannelMgr.h
// * @date 2009-12-31 CST
// * @version $Id $
// * @author tangteng tangteng@modi.com
// * @brief 游戏中的频道管理器
// *
// */
//
//#ifndef MODI_GAMECHANNEL_MGR_H_
//#define MODI_GAMECHANNEL_MGR_H_
//
//#include "GameChannel.h"
//#include "SingleObject.h"
//
///**
// * @brief 频道管理器
// *
// */
//class MODI_GameChannelMgr : public CSingleObject<MODI_GameChannelMgr>
//{
//public:
//
//    MODI_GameChannelMgr();
//    virtual ~MODI_GameChannelMgr();
//
//
//public:
//
//    ///	添加一个频道
//    bool Add(MODI_GameChannel * p);
//
//    ///	删除一个频道
//    bool Del(MODI_GameChannel * p);
//
//    ///	根据频道名查找一个频道
//    MODI_GameChannel * Get(const std::string & strChannel);
//
//    //	春节前版本临时方案
//    MODI_GameChannel * Get_TempVersion()
//    {
//        if (m_listChannels.size())
//            return *(m_listChannels.begin());
//        return 0;
//    }
//
//    void Update(unsigned long nTime);
//
//public:
//
//    typedef std::list<MODI_GameChannel *> LIST_CHANNELS;
//    typedef LIST_CHANNELS::iterator LIST_CHANNELS_ITER;
//    typedef LIST_CHANNELS::const_iterator LIST_CHANNELS_C_ITER;
//
//    LIST_CHANNELS m_listChannels;
//};
//
//#endif
