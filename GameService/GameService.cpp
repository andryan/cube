#include "GameTask.h"
#include "GameService.h"
#include "ServerConfig.h"
#include "RecurisveMutex.h"
#include "GameTick.h"
#include "BinFileMgr.h"


/**
 * @brief 初始化
 *
 */
bool MODI_GameService::Init()
{
    const MODI_SvrGSConfig * pGSInfo = (const MODI_SvrGSConfig *)( MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_CHANNEL ) );
	if( !pGSInfo )
		return false;

	const MODI_SvrGSConfig::MODI_Info * pSelfInfo = (const MODI_SvrGSConfig::MODI_Info *)(pGSInfo->GetInfoByID( GetChannelIDFromArgs() ) );
	if( !pSelfInfo )
		return false;

	// gameserver 帮定只能上内网的网卡
    if (!MODI_NetService::Init( pSelfInfo->nPort , pSelfInfo->strIP.c_str() ) )
    {
        Global::logger->fatal("[%s] Fail to initialize service %s.", SYS_INIT, GetName());
        return false;
    }

   //  m_pTaskPoll = new MODI_ServiceTaskPoll();

//     if (!m_pTaskPoll)
//     {
//         Global::logger->fatal("[%s] Service %s fail to establish a connection with TaskPoll.", SYS_INIT, GetName());
//         return false;
//     }

//     m_pTaskPoll->Start();
	m_pTaskSched.Init();

    return true;
}

/**
 * @brief 创建新的连接
 * @param sock 创建新的连接的socket描述符
 * @param addr 新的连接的地址
 *
 * @return 成功true,失败false
 *
 */
bool MODI_GameService::CreateTask(const int sock, const struct sockaddr_in * addr)
{
    if ((sock == -1) || (addr == NULL))
    {
        return false;
    }

    MODI_GameTask * p_task = new MODI_GameTask(sock, addr);

    if (p_task)
    {
		// 加入到队列中
		MODI_GameTask::ms_ProcessBase.OnAcceptConnection( p_task );
        //m_pTaskPoll->AddTask(p_task);
		m_pTaskSched.AddNormalSched(p_task);
#ifdef _CUBE_DEBUG
        Global::logger->debug("[%s] 新增加一个连接%s,port=%d . ", SOCK_DATA, p_task->GetIP(),p_task->GetPort());
#endif
        return true;
    }
    else
    {
        TEMP_FAILURE_RETRY(::close(sock));
        return false;
    }
}


/** 
 * @brief 重新加载礼包文件
 * 
 */
void MODI_GameService::DealSignalUsr2()
{
	Global::g_byReloadPackage = 1;
}


/** 
 * @brief 重新加载脚本
 * 
 */
void MODI_GameService::ReloadConfig()
{
	Global::g_byReloadScript = 1;
}
