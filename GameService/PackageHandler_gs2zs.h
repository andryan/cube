/** 
 * @file PackageHandler_gs2zs.h
 * @brief 游戏服务器和zoneserver的数据包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */
#ifndef PACKAGE_HANDLER_H_GS2ZS_
#define PACKAGE_HANDLER_H_GS2ZS_

#include "protocol/gamedefine.h"
#include "IPackageHandler.h"
#include "SingleObject.h"



class MODI_PackageHandler_gs2zs : public MODI_IPackageHandler , 
	public CSingleObject<MODI_PackageHandler_gs2zs>
{
public:

    MODI_PackageHandler_gs2zs();
    ~MODI_PackageHandler_gs2zs();

	static int OnHandler_LoginAuthResult( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  );

	static int OnHandler_EnterChannelRet( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  );

	static int OnHandler_KickSb( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size  );

	static int RemakePP_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadLastItemSerial_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int AddMoeny_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int AddItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int QueryHasCharResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int OnBillTransResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int OnResetTodayOnlineTime_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int OnExchangeYYKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int RefreshMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 改变金币
	static int OptMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 充值返回
    static int ReturnPayResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,const unsigned int cmd_size );

	/// 称号改变
	static int ChangeChenghao_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size );

	/// 沉迷了
	static int ChenMi_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载变量
	static int LoadUserVar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载用户包裹
	static int NotifyItemInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载网页道具
	static int NotifyWebItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载网页赠送道具
	static int NotifyWebPresent_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载家族信息
	static int LoadCFamilyInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 家族里面的人
	static int BroadcastInFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 改变gmlevel
	static int ModiGMLevel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 收到全局变量列表
	static int ReceiveGlobalVarList_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	/// 收到全局变量修改信息
	static int ReceiveVarChange_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	static int SingleMusicList_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	/// INAitemreturn
	static int INAItemReturn_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	
	static int INAGiveReturn_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	static int BroadcastCmd_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	static int BroadcastCmdRange_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
private:

    virtual bool FillPackageHandlerTable();

};

#endif

