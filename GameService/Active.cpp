#include "Active.h"
#include "GameTick.h"
#include "MakeManager.h"
#include "ScriptManager.h"
#include "protocol/c2gs_Activity.h"

#define VAR_TIME_TYPE	"type=normal_del=21990901"

static int GetMonDays(int month, int year)
{
	int 	a[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	bool	leap;	

	leap=( year % 400 ==0 || year % 4== 0 && year % 100 != 0 );
	return ( a[month-1] + (leap && (month== 2 ) ));
}

static BYTE	GetDays(DWORD  condition,int days)
{
	DWORD	j=0x01;
	BYTE	n=0;
	int i=0;
	condition &= 0xfffffffe;
	condition >>= 1;
	for( i=0; i< days; ++i)
	{
		if( condition & j)
			n++;		
		condition >>= 1;
	}
	return n;
}

bool MODI_ActiveSignup::Init(const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_ntimes = atoi(split["alltime"]);	
	return  true;
}
bool MODI_ActiveSignup::do_it(MODI_ClientAvatar * p_client)
{
	MODI_GS2C_Notify_SignupResult result;

	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	int cur_mon = MODI_GameTick::m_stTTime.GetMon();
	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();

	std::ostringstream os;
	os.str("");
	os<<"Sigup_";
	os<<cur_year<<"_";
	os<<cur_mon;

	if( cur_day > 15)
		os<<"_";

	MODI_GameUserVar * p_var = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if(! p_var)
	{
		p_var = new MODI_GameUserVar(os.str().c_str());
		if(p_var->Init(p_client, VAR_TIME_TYPE))
		{
			if(! p_client->m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	///	签到从手动签到，改为脚本自动签到，因此，不再有时间限制
/*	if( p_client->GetFullData().m_roleExtraData.m_nTotalOnlineTime * 60 < m_ntimes)
	{
		result.m_eRetResult = enSignUpResult_Time;
		p_client->SendPackage(&result,sizeof(MODI_GS2C_Notify_SignupResult));		
		/// 时间未到
		Global::logger->debug("[active_warn]  %s time not enough   time is %d,requires %d ",p_client->GetRoleName(),p_client->GetFullData().m_roleExtraData.m_nTotalOnlineTime*60,m_ntimes);
		return false;	
	}
	*/
	unsigned short i=0x01;
	if( cur_day > 15)
	{
		i <<= (cur_day-16);
	}
	else
	{
		i <<= cur_day;
	}
	if(  p_var->GetValue() & i)
	{
	//	result.m_eRetResult = enSignUpResult_Had;
	//	p_client->SendPackage(&result,sizeof(MODI_GS2C_Notify_SignupResult));		
		/// 今天已经签到
		Global::logger->debug("[active_warn] %s has signed before ",p_client->GetRoleName());
		return false;
	}
	WORD value = p_var->GetValue();
	value |= i;
	p_var->SetValue(value);
//	p_var->SaveToDB();
	result.m_eRetResult = enSignUpResult_Suc;
	p_client->SendPackage(&result,sizeof(MODI_GS2C_Notify_SignupResult));		
	///	检查添加获奖信息
	//
	

	//执行成功后的脚本
	//
	MODI_TriggerActivity script(3);
	MODI_ActivityManager::GetInstance().Execute(p_client,script);
	return true;
}

bool MODI_ActiveSignDone::Init(const char * action_line)
{
	return true;
}
bool MODI_ActiveSignDone::do_it(MODI_ClientAvatar * p_client)
{

	return true;
}


bool MODI_ActiveSigupDesc::Init(const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_ntimes = atoi(split["alltime"]);	
	m_nSec1 =  atoi(split["sec1"]);
	m_nSec2 = atoi(split["sec2"]);
	m_nSec3 = atoi(split["sec3"]);
	m_nSec4 = atoi(split["sec4"]);

	m_nPack1 = atoi(split["pac1"]);
	m_nPack2 = atoi(split["pac2"]);
	m_nPack3 = atoi(split["pac3"]);
	m_nPack4 = atoi(split["pac4"]);
	m_nPack5 = atoi(split["pac5"]);
	return true;
}

bool MODI_ActiveSigupDesc::do_it(MODI_ClientAvatar * p_client)
{
	///	发送给客户端该活动的玩家所有的信息
	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	int cur_mon = MODI_GameTick::m_stTTime.GetMon();
	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();
	BYTE cur_week = MODI_GameTick::m_stTTime.GetWDay();

	std::ostringstream os;
	os.str("");
	os<<"Sigup_";
	os<<cur_year<<"_";
	os<<cur_mon;

	MODI_GameUserVar * p_var = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if(! p_var)
	{
		p_var = new MODI_GameUserVar(os.str().c_str());
		if(p_var->Init(p_client, VAR_TIME_TYPE))
		{
			if(! p_client->m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	os<<"_";
	MODI_GameUserVar * p_var2 = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if( !p_var2)
	{
		p_var2 = new MODI_GameUserVar(os.str().c_str());	
		if( p_var2->Init(p_client,VAR_TIME_TYPE))
		{

			if(! p_client->m_stGameUserVarMgr.AddVar(p_var2))
			{
				delete p_var2;
				p_var2 = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var2->SaveToDB();
			}
		}
		else
		{
			delete p_var2;
			p_var2 = NULL;
			MODI_ASSERT(0);

		}
		
	}
	os.str("");
	os<<"Sigup_result";
	os<<cur_year<<"_";
	os<<cur_mon;
	MODI_GameUserVar * p_result_var = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if( !p_result_var)
	{
		p_result_var = new MODI_GameUserVar(os.str().c_str());
		if( p_result_var ->Init(p_client,VAR_TIME_TYPE))
		{
			if( !p_client->m_stGameUserVarMgr.AddVar(p_result_var))
			{
				delete p_result_var;
				p_result_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_result_var->SaveToDB();
			}
		}
		else
		{
			delete p_result_var;
			p_result_var = NULL;
			MODI_ASSERT(0);
		}
	}
	os<<"_";
	
	MODI_GameUserVar * p_result_var2 = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if( !p_result_var2)
	{
		p_result_var2 = new MODI_GameUserVar(os.str().c_str());
		if( p_result_var2->Init(p_client,VAR_TIME_TYPE))
		{
			if( !p_client->m_stGameUserVarMgr.AddVar(p_result_var2))
			{
				delete p_result_var2;
				p_result_var2 = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_result_var2->SaveToDB();
			}
		}
		else
		{
			delete p_result_var2;
			p_result_var2 = NULL;
			MODI_ASSERT(0);
		}
	}


	///	制作签到情况	
	DWORD	condition=0;
	DWORD	tmp=0;
	condition |= p_var->GetValue();
	condition &= 0xffff;
	tmp |= p_var2->GetValue();
	tmp &= 0xffff;
	tmp <<= 16;
	condition |= tmp;	

	///	制作获得奖励情况
	tmp=0;
	DWORD	result=0;
	result |= p_result_var->GetValue();
	result &= 0xffff;
	tmp |= p_result_var2->GetValue();
	tmp &= 0xffff;
	tmp <<= 16;
	result |= tmp;

	int	days = GetMonDays(cur_mon,cur_year);
	BYTE m_nSigned = GetDays(condition,days);
	MODI_GS2C_Notify_Signup  notify;
	if( m_nSigned < m_nSec1)
	{
		notify.m_nSecDays = m_nSec1-m_nSigned ;
	}
	else if( m_nSigned < m_nSec2)
	{
		notify.m_nSecDays = m_nSec2-m_nSigned;
	}
	else if( m_nSigned < m_nSec3)
	{
		notify.m_nSecDays = m_nSec3 - m_nSigned;
	}
	else if( m_nSigned < m_nSec4)
	{
		notify.m_nSecDays = m_nSec4 - m_nSigned;
	}
	else
	{
		notify.m_nSecDays = days - m_nSigned;
	}

	///	发送给客户端该活动玩家的信息
	notify.m_dwSignCondition = condition;
	notify.m_dwSignResult = result;
	notify.m_nDay = cur_day;
	notify.m_nMonth = cur_mon;
	notify.m_nYear = cur_year;
	notify.m_nRequire = m_ntimes;
	notify.m_nTime = p_client->GetFullData().m_roleExtraData.m_nTodayOnlineTime;
	notify.m_tWeek =  cur_week;
	notify.m_nPackets = 5;
	notify.m_aPacketCon[0].m_nConfigId = m_nPack1; 
	notify.m_aPacketCon[1].m_nConfigId = m_nPack2;	
	notify.m_aPacketCon[2].m_nConfigId = m_nPack3; 
	notify.m_aPacketCon[3].m_nConfigId = m_nPack4;	
	notify.m_aPacketCon[4].m_nConfigId = m_nPack5;


	p_client->SendPackage(&notify,sizeof(MODI_GS2C_Notify_Signup)+5*sizeof(PacketInfos));
		
	return true;
}



bool	MODI_ActivePackage::Init( const char * con_line)
{
	return true;
}

bool	MODI_ActivePackage::do_it(MODI_ClientAvatar * p_client)
{

	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	int cur_mon = MODI_GameTick::m_stTTime.GetMon();
	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();
	//BYTE cur_week = MODI_GameTick::m_stTTime.GetWDay();

	std::ostringstream os;
	os.str("");
	os<<"Sigup_result";
	os<<cur_year<<"_";
	os<<cur_mon;

	if( cur_day > 15)
	{
		os << "_";
	}
	MODI_GameUserVar * p_result_var = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if( !p_result_var)
	{
		Global::logger->debug("[active_debug] name=%s ERROR can't find ---result value=  %s",p_client->GetRoleName(),os.str().c_str());
		return false;
	}

	WORD	res = p_result_var->GetValue();
	unsigned  short	mask = 0x01;
	if( cur_day > 15)
	{
		mask <<= (cur_day-16);
	}
	else
	{
		mask <<= cur_day;
	}

	res |= mask;
	p_result_var->SetValue(res);

	// 刷新包获得情况


	return true;
}




















