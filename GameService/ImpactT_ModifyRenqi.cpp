#include "ImpactT_ModifyRenqi.h"
#include "ImpactObj.h"
#include "ClientAvatar.h"
#include "AssertEx.h"
#include "protocol/c2gs_chatcmd.h"


MODI_ImpactT_ModifyRenqi::MODI_ImpactT_ModifyRenqi()
{

}

MODI_ImpactT_ModifyRenqi::~MODI_ImpactT_ModifyRenqi()
{

}


void 	MODI_ImpactT_ModifyRenqi::SetModifyConst( MODI_ImpactObj & impactObj , long lValue )
{
	impactObj.SetArgLong( kArg_ModifyConst  , lValue );
}

long 	MODI_ImpactT_ModifyRenqi::GetModifyConst( MODI_ImpactObj & impactObj ) const
{
	long lValue = 0;
	impactObj.GetArgLong( kArg_ModifyConst , lValue );
	return lValue;
}

int MODI_ImpactT_ModifyRenqi::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	long lValue = (long)impactData.get_Arg1();
	SetModifyConst( impactObj , lValue );
	return kImpactH_Successful;
}

int MODI_ImpactT_ModifyRenqi::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	long lValue = GetModifyConst( impactObj );

	pObj->AddRenqi(lValue);

	Global::logger->info("[%s] client<charid=%u,name=%s> modify money<%d> ." ,
			IMPACT_MODULE ,
			pObj->GetCharID(),
			pObj->GetRoleName() ,
			lValue );

	return kImpactH_Successful;
}

int MODI_ImpactT_ModifyRenqi::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	return kImpactH_Successful;
}

