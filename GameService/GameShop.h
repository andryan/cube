/**
 * @file   GameShop.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jun 24 15:33:47 2011
 * 
 * @brief  道具购买
 * 
 */

#ifndef MODI_GAMESHOP_H_
#define MODI_GAMESHOP_H_


#include "gamestructdef.h"
#include "Base/s2zs_cmd.h"
#include "Base/BinFileMgr.h"
#include "protocol/c2gs_shop.h"

class MODI_ClientAvatar;


/** 
 * @brief 购买函数
 * 
 */
class MODI_GameShop
{
 public:

	virtual ~MODI_GameShop(){}

	/** 
	 * @brief 进入商场
	 * 
	 */
	virtual void OnEnter( MODI_ClientAvatar * pClient ) = 0;


	/** 
	 * @brief 离开商场
	 * 
	 */
	virtual void OnLeave( MODI_ClientAvatar * pClient ) = 0;
	

	/**
	 * @brief 商场的类型
	 *
	 * @return
	 */
	virtual enShopType GetShopType()
	{
		return kNullShop;
	}
	
	/** 
	 * @brief 商场某道具的价格
	 * 
	 * @param pElement 道具
	 * @param st 时效
	 * 
	 * @return 价格
	 *
	 */
	DWORD GetPrice_Money( const GameTable::MODI_Shop * pElement , enShopGoodTimeType st);


	/** 
	 * @brief 商场某道具的点券价格
	 * 
	 * @param pElement 道具
	 * @param st 时效
	 * 
	 * @return 价格
	 *
	 */
	DWORD GetPrice_RMBMoney( const GameTable::MODI_Shop * pElement , enShopGoodTimeType st);
	

	/** 
	 * @brief 产生交易号
	 * 
	 * @param p_client 此用户交易的
	 * 
	 */
	const QWORD GetBuySerial(MODI_ClientAvatar * p_client);
	
	
	/** 
	 * @brief 检查购买物品是否合法
	 * 
	 * @param p_goods_info 物品信息
	 * @param buy_count 物品个数
	 * @param shop_type 商场类型
	 * 
	 * @return 成功true
	 *
	 */
	bool CheckBuyGoods(const MODI_ShopGood * p_goods_info, const BYTE & buy_count,  enShopType shop_type);


	/** 
	 * @brief 获取购买物品的钱
	 *
	 * @param p_client 购买人
	 * @param p_goods_info 物品信息
	 * @param buy_count 购买数量
	 * @param buy_money_rmb 人民币
	 * @param buy_money 金币
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CheckBuyMoney(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_goods_info, const BYTE & buy_count);


	/** 
	 * @brief 购买性别判断
	 * 
	 * @param p_client 购买人
	 * @param p_goods_info 购买物品
	 * @param buy_item 数量
	 * 
	 * @return 成功true
	 *
	 */
	bool CheckBuySex(const BYTE check_sex, const MODI_ShopGood * p_goods_info, const BYTE & buy_item);


	/** 
	 * @brief 包裹空间检查
	 * 
	 * @param p_client 客户端
	 * @param p_goods_info 物品信息
	 * @param buy_item 物品个数
	 * 
	 * @return 成功ture
	 *
	 */
	bool CheckBuySpace(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_goods_info, const BYTE & buy_item);


	/** 
	 * @brief 购买
	 * 
	 * @param p_client 购买人
	 * @param p_goods_info 购买信息
	 * @param buy_count 购买数量
	 * 
	 * @return 成功true
	 *
	 */
	bool OnBuyItem(MODI_ClientAvatar * p_client, const MODI_ShopGood * p_in_goods_info, const BYTE & buy_count, enShopTransactionType buy_type, QWORD buy_serial);

	
	/** 
	 * @brief 购买物品
	 * 
	 * @param p_client 购买人  
	 * @param p_goods_info 购买的物品信息 
	 * @param item_count 物品个数
	 * 
	 * @return 状态
	 *
	 */
	enShopTransactionResult BuyItem( MODI_ClientAvatar * p_client, const MODI_ShopGood * p_in_goods_info, BYTE buy_count, enShopTransactionType bug_type);


	/** 
	 * @brief 赠送物品给存在的角色
	 * 
	 * @param p_recv_cmd 赠送信息
	 *
	 */
	void GiveItemToOther(const MODI_ZS2S_Notify_QueryHasCharResult_Shop * p_recv_cmd, QWORD buy_serial);


	/** 
	 * @brief 赠送物品给别人
	 * 
	 * @param p_client 赠送人
	 * @param p_recv_cmd 赠送参数
	 * @param cmd_size 参数大小
	 * 
	 * @return 结果
	 *
	 */
	enShopTransactionResult SendGiveItemMail(MODI_ClientAvatar * p_client, MODI_C2GS_Request_Transaction * p_recv_cmd, const unsigned int cmd_size);


	/** 
	 * @brief 发送赠送mail
	 * 
 	 * @return 成功true
 	 *
 	 */
	bool SendGiveMail(MODI_CHARID senderid, const char * szSender, const char * szReceverName, const MODI_ShopGood * pGoods,
								 BYTE nCount, const char * szContent , QWORD transid);


	/** 
 	 * @brief 发送带附件的邮件
 	 * 
 	 */
	bool ReqZoneSendGiveItemMail(MODI_CHARID senderid, const char * szSender, const char * szRecever, const MODI_ShopGood * pGoods,
											BYTE nCount, enShopTransactionType type, const char * szContent, QWORD transid);
};
 
/** 
 * @brief 神秘商场
 * 
 * 
 */
class MODI_GameRareShop: public MODI_GameShop
{
 public:
	virtual ~MODI_GameRareShop(){}
	
	virtual void 	OnEnter( MODI_ClientAvatar * pClient );
	
	virtual void 	OnLeave( MODI_ClientAvatar * pClient );
	
	virtual enShopType GetShopType()
	{
		return kRareShop;
	}
};


/** 
 * @brief 普通商场
 * 
 */
class MODI_GameNormalShop : public MODI_GameShop
{
 public:
	virtual ~MODI_GameNormalShop(){}
	
	virtual void 	OnEnter( MODI_ClientAvatar * pClient );
	
	virtual void 	OnLeave( MODI_ClientAvatar * pClient );
	
	virtual enShopType GetShopType()
	{
		return kNormalShop;
	}
};


#endif
