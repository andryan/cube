/**
 * @file   BagPosInfo.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 15:43:43 2011
 * 
 * @brief  包裹位置信息
 * 
 * 
 */


#include "AssertEx.h"
#include "BagPosInfo.h"
#include "Bag.h"
#include "ItemRuler.h"


/** 
 * @brief 获取包裹
 * 
 */
MODI_Bag * MODI_BagPosInfo::GetOwnBag()
{
	return m_pOwnBag;
}

/** 
 * @brief 增加一个道具
 * 
 * @param p_item 道具
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BagPosInfo::AddItem(MODI_GameItemInfo * p_item)
{
	if(!p_item)
	{
	   	Global::logger->error("[add_item_in_pos] item in null");
		MODI_ASSERT(0);
		return false;
	}

	WORD max_lap = 0;
	if(! MODI_ItemRuler::GetMaxLap(p_item->GetConfigId(), max_lap))
	{
		Global::logger->error("[add_item_in_pos] unable get max lap <configid=%u>", p_item->GetConfigId());
		MODI_ASSERT(0);
		return false;
	}

	
	if(GetCount() > max_lap)
	{
		Global::logger->error("[add_item_in_pos] get count faild <count=%u,max_lap=%u>", GetCount(), max_lap);
		MODI_ASSERT(0);
		return false;
	}
	else if(GetCount() == max_lap)
	{
		Global::logger->error("[add_item_in_pos] max_lap <count=%u,max_lap=%u>", GetCount(), max_lap);
		return false;
	}
	else
	{
		MODI_Bag * p_bag = GetOwnBag();
		if(! p_bag)
		{
			Global::logger->error("[add_item_in_pos] get bag null");
			MODI_ASSERT(0);
			return false;
		}
			
		if(GetCount() == 0)
		{
			m_dwConfigId = p_item->GetConfigId();
			m_enItemType = ::GetItemType(m_dwConfigId);
			p_bag->UpdatePosInfo(p_item->GetConfigId(), this);
		}

		if(m_dwConfigId != p_item->GetConfigId())
		{
			Global::logger->error("[add_item_in_pos] configid not equ <oldid=%u,newid=%u>", m_dwConfigId, p_item->GetConfigId());
			MODI_ASSERT(0);
			return false;
		}
		
		AddCount(1);
		m_stItemIdSet.insert(p_item->GetItemId());
		p_item->SetClient(p_bag->GetOwner());
		p_item->SetBag(p_bag);
		p_item->SetBagType(p_bag->GetBagType());
		p_item->SetBagPos(GetBagPos());
		return true;
	}
	return false;
}


/** 
 * @brief 删除此道具
 * 
 * @param p_item 道具
 * 
 * @return 成功ture,失败false
 *
 */
bool MODI_BagPosInfo::DelItem(MODI_GameItemInfo * p_item)
{
	std::set<QWORD>::iterator iter = m_stItemIdSet.find(p_item->GetItemId());
	if(iter == m_stItemIdSet.end())
	{
		Global::logger->error("[del_item_in_pos] not find itemid <itemid=%llu>",p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	if(GetCount() < 1)
	{
		Global::logger->error("[del_item_in_pos] count zero <count=%u>",GetCount());
		MODI_ASSERT(0);
		return false;
	}

	DelCount();
	m_stItemIdSet.erase(p_item->GetItemId());

	if(GetCount() == 0)
	{
		Clean();
	}

	return true;
}


/** 
 * @brief 获取道具
 *
 */
MODI_GameItemInfo * MODI_BagPosInfo::GetItemInfo()
{
	MODI_GameItemInfo * p_item = NULL;
	QWORD item_id = GetItemId();
	if(item_id != 0)
	{
		p_item = m_pOwnBag->GetItemByItemId(item_id);
	}
	return p_item;
}
