#include "ZoneClient.h"
#include "GameService.h"
#include "PackageHandler_gs2zs.h"
#include "s2zs_cmd.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "ClientAvatar.h"
#include "ClientAndZSDirection.h"


MODI_ZoneServerClient::MODI_ZoneServerClient(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(GSTOZS_CMDSIZE);
}

MODI_ZoneServerClient::~MODI_ZoneServerClient()
{

}



/**
 * @brief 初始化
 *
 */
bool MODI_ZoneServerClient::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

void MODI_ZoneServerClient::Final()
{
	MODI_ClientTask::Final();

	Global::logger->info("[%s] Disconnection with Zone Server. GameServer will Terminate. " , 
			ERROR_CON );

	// 和 zone server 断开连接，则结束服务器
	MODI_GameService * pService = MODI_GameService::GetInstancePtr();
	if( pService )
	{
		pService->Terminate();
	}
}

bool MODI_ZoneServerClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }

    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_ZoneServerClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
/*	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}*/
	if(pt_null_cmd == NULL)
	{
        	Global::logger->info("[ERROR_ZC] Pointer NULL.");
		return false;
	}
	if(cmd_size == 0)
	{
        	Global::logger->info("[ERROR_ZC] Command size 0.");
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

       	Global::logger->info("[ERROR_ZC] Socket failure.");
	return false;
}

bool MODI_ZoneServerClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	// 数据包处理
	if( MODI_ZoneServerClient::GetInstancePtr() )
	{
		if( pt_null_cmd->byCmd == MAINCMD_S2ZS  && pt_null_cmd->byParam == MODI_ZS2GS_Redirectional::ms_SubCmd )
		{
			MODI_ZS2GS_Redirectional & red = *((MODI_ZS2GS_Redirectional *) (pt_null_cmd));

			MODI_ClientAndZSDirection::ZoneServerToClient( red.m_sessionID ,
					red.m_guid, red.m_pData , red.m_nSize );
			return true;
		}
		else 
		{
	
			int iRet = MODI_PackageHandler_gs2zs::GetInstancePtr()->DoHandlePackage(
						pt_null_cmd->byCmd, pt_null_cmd->byParam,  MODI_ZoneServerClient::GetInstancePtr(), pt_null_cmd,
						dwCmdLen);
			if( iRet == enPHandler_Ban )
			{

			}
			else  if ( iRet == enPHandler_Kick )
			{

			}
			else if( iRet == enPHandler_Warning )
			{

			}
		}
	}

	return true;
}

void MODI_ZoneServerClient::ProcessPackages()
{
	const unsigned int  nMaxGetCmd = 10000;
	MODI_CmdParse::Get(nMaxGetCmd );
}
