#include "MultiGameRoom.h"
#include "protocol/c2gs.h"
#include "GameChannel.h"
#include "GameTick.h"
#include "Share.h"
#include "BinFileMgr.h"
#include <math.h>
#include "protocol/gamedefine.h"
#include "MdmMgr.h"
#include "LoggerInfo.h"
#include "LoggerAction.h"
#include "AssertEx.h"
#include "BaseOS.h"




MODI_MultiGameRoom::MODI_MultiGameRoom( defRoomID roomid ):Parent(roomid),
	m_fTotalNodeLength(0.0f)
{
	m_byTeamState[0] = 0;
	m_byTeamState[1] = 0;
	m_byTeamState[2] = 0;
	m_byTeamState[3] = 0;
	m_byTeamState[4] = 0;
	m_byTeamState[5] = 0;
}
    
MODI_MultiGameRoom::~MODI_MultiGameRoom()
{

}

bool MODI_MultiGameRoom::OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator )
{
	if( nPlayMax > MAX_PLAYSLOT_INROOM )
		nPlayMax = MAX_PLAYSLOT_INROOM;
	if( nSpectorMax > MAX_SPECTATORSLOT_INROOM )
		nSpectorMax = MAX_SPECTATORSLOT_INROOM;
	m_nPlayerMaxSize = nPlayMax;
	m_nSpectatorMaxSize = nSpectorMax;

	for( DWORD n = 0; n < m_nPlayerMaxSize; n++ )
	{
		m_Players[n].Reset();
	}
	for( DWORD n = 0; n < m_nSpectatorMaxSize; n++ )
	{
		m_Spectators[n].Reset();
	}
	SetAllowSpectator( bAllowSpectator );
	GetRoomInfo_Modfiy().m_bPlaying = false;

	ClearAllTeam();
	return true;
}

bool MODI_MultiGameRoom::EnterRoom(MODI_ClientAvatar * pClient)
{
	bool bIsIn = IsIn(pClient);
	if( bIsIn )
	{
		MODI_ASSERT(0);
		return false;
	}
	
	BroadcastYYChatState();

	return true;
}

bool MODI_MultiGameRoom::LeaveRoom(MODI_ClientAvatar * pClient, int iReason)
{
    bool bFind = RemoveClient(pClient);
    if (!bFind)
	{
		MODI_ASSERT(0);
		return false;
	}
	return true;
}

void MODI_MultiGameRoom::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{

}

void MODI_MultiGameRoom::OnChangeMap( defMapID oldmap , defMapID newmap )
{
	MODI_GS2C_Notify_MapChanged msg;
	if( newmap == RANDOM_MAP_ID )
	{
		GetNormalInfo_Modfiy().m_bRandomMap = true;
		msg.m_bRamdom = true;
		msg.m_MapID = RANDOM_MAP_ID;
	}
	else 
	{
		GetNormalInfo_Modfiy().m_bRandomMap = false;
//		GetNormalInfo_Modfiy().m_nMapId = newmap;
		msg.m_bRamdom = false;
		msg.m_MapID = newmap;
	}
	Broadcast( &msg , sizeof(msg) );
}

void MODI_MultiGameRoom::OnChangeMusic( defMusicID oldmusic , defMusicID newmusic , enMusicFlag flag , enMusicSType st , enMusicTime mt )
{
	MODI_GS2C_Notify_MusicChanged msg;
	if( newmusic == RANDOM_MUSIC_ID )
	{
		GetNormalInfo_Modfiy().m_bRandomMusic = true;
//		GetNormalInfo_Modfiy().m_flag = flag;
//		GetNormalInfo_Modfiy().m_songType = st;
//		GetNormalInfo_Modfiy().m_nMusicId = newmusic;

		msg.m_bRandom = IsRandomMusic();
		msg.m_flag = GetMusicFlag();
		msg.m_songType = GetSongType();
		msg.m_musicID = RANDOM_MUSIC_ID;
		msg.m_mt = mt;
	}
	else 
	{
		GetNormalInfo_Modfiy().m_bRandomMusic = false;
//		GetNormalInfo_Modfiy().m_flag = flag;
//		GetNormalInfo_Modfiy().m_songType = st;
//		GetNormalInfo_Modfiy().m_nMusicId = newmusic;

		msg.m_musicID = newmusic;
		msg.m_flag = flag;
		msg.m_songType = st;
		msg.m_bRandom = false;
		msg.m_mt = mt;
	}
	Broadcast( &msg , sizeof(msg) );
}

void MODI_MultiGameRoom::OnChangeGameMode( ROOM_SUB_TYPE oldMode , ROOM_SUB_TYPE newMode )
{
	defRoomType t;
	t = RoomHelpFuns::MakeRoomType( IsPasswordRoom() , 
			GetRoomType(),
			newMode );
	SetRoomType(t );
	m_enRoomSubType = newMode;
	MODI_GS2C_Notify_GameModeChanged msg;
	msg.m_newSubType = newMode;
	Broadcast( &msg , sizeof(msg) );
	
	if( newMode == ROOM_STYPE_KEYBOARD_TEAM || newMode == ROOM_STYPE_KEYBOARD ||  newMode == ROOM_STYPE_KEYBOARD_PROP)
	{
		if( GetMusicFlag() == kMusicFlag_Banzou)
		{
			GetNormalInfo_Modfiy().m_flag = kMusicFlag_Yuanchang;	
			MODI_GS2C_Notify_MusicChanged change;
			change.m_bRandom = IsRandomMusic();
			change.m_flag = GetMusicFlag();
			change.m_mt =GetMusicTimeType();
			change.m_musicID = GetMusicID();
			change.m_songType = GetSongType();
			Broadcast( &change,sizeof( MODI_GS2C_Notify_MusicChanged));
		}
	}

}

bool MODI_MultiGameRoom::IsIn(MODI_ClientAvatar * pClient)
{
	if (!pClient)
        return false;

    unsigned int n = 0;
    for (n = 0; n < m_nPlayerMaxSize; n++)
    {
        if (m_Players[n].m_pClient == pClient)
        {
            return true;
        }
    }

    for (n = 0; n < m_nSpectatorMaxSize; n++)
    {
        if (m_Spectators[n].m_pClient == pClient)
        {
            return true;
        }
    }
    return false;
}

bool MODI_MultiGameRoom::OnDestroy()
{

	return true;
}

//   广播整个房间
void MODI_MultiGameRoom::Broadcast(const void * pt_null_cmd, int cmd_size)
{
    this->Broadcast_Players(pt_null_cmd, cmd_size);
    this->Broadcast_Spectators(pt_null_cmd, cmd_size);
}

//    广播整个房间
void MODI_MultiGameRoom::Broadcast_Except(const void * pt_null_cmd, int cmd_size, MODI_ClientAvatar * pExcept)
{
    this->Broadcast_Players_Except(pt_null_cmd, cmd_size, pExcept);
    this->Broadcast_Spectators_Except(pt_null_cmd, cmd_size, pExcept);
}

//    广播给所有参战者
void MODI_MultiGameRoom::Broadcast_Players(const void * pt_null_cmd, int cmd_size)
{
    unsigned int n = 0;
    for (n = 0; n < m_nPlayerMaxSize; n++)
    {
        if (m_Players[n].m_pClient)
        {
            m_Players[n].m_pClient ->SendPackage(pt_null_cmd, cmd_size);
        }
    }
}

//    广播给所有观战者
void MODI_MultiGameRoom::Broadcast_Spectators(const void * pt_null_cmd, int cmd_size)
{
    unsigned int n = 0;
    for (n = 0; n < m_nSpectatorMaxSize; n++)
    {
        if (m_Spectators[n].m_pClient)
        {
            m_Spectators[n].m_pClient ->SendPackage(pt_null_cmd, cmd_size);
        }
    }
}

void MODI_MultiGameRoom::Broadcast_Spectators(MODI_ClientAvatar * pClient , const void * pt_null_cmd, int cmd_size)
{
	if( pClient->GetGUID().IsInvalid() )
		return ;

    unsigned int n = 0;
    for (n = 0; n < m_nSpectatorMaxSize; n++)
    {
        MODI_ClientAvatar * pTemp = m_Spectators[n].m_pClient;
		if( pTemp && pTemp->GetRoleNormalInfo().m_SingerID == pClient->GetGUID() )
        {
            pTemp->SendPackage(pt_null_cmd, cmd_size);
        }
    }
}

//    广播给所有参战者
void MODI_MultiGameRoom::Broadcast_Players_Except(const void * pt_null_cmd, int cmd_size,
        MODI_ClientAvatar * pExcept)
{
    unsigned int n = 0;
    for (n = 0; n < m_nPlayerMaxSize; n++)
    {
        if (m_Players[n].m_pClient && m_Players[n].m_pClient != pExcept)
        {
            m_Players[n].m_pClient ->SendPackage(pt_null_cmd, cmd_size);
        }
    }
}

//    广播给所有观战者
void MODI_MultiGameRoom::Broadcast_Spectators_Except(const void * pt_null_cmd, int cmd_size,
        MODI_ClientAvatar * pExcept)
{
    unsigned int n = 0;
    for (n = 0; n < m_nSpectatorMaxSize; n++)
    {
        if (m_Spectators[n].m_pClient && m_Spectators[n].m_pClient != pExcept)
        {
            m_Spectators[n].m_pClient ->SendPackage(pt_null_cmd, cmd_size);
        }
    }
}

void MODI_MultiGameRoom::SendRoleInfoToClients(MODI_ClientAvatar * pClient)
{
	MODI_GS2C_Notify_CreateClient notify;
	notify.m_fPosX = pClient->GetPosX();
	notify.m_fPosZ = pClient->GetPosZ();
	notify.m_fDir = pClient->GetDir();
	notify.m_baseInfo = pClient->GetRoleBaseInfo();
	notify.m_normalInfo = pClient->GetRoleNormalInfo();
	notify.m_eVolType = pClient->GetKeyVolType();
	notify.m_fSped = pClient->GetKeySped();
    Broadcast_Except( &notify,sizeof(notify) , pClient );
}

void MODI_MultiGameRoom::SendRolesInfoToClient(MODI_ClientAvatar * pClient)
{
	MODI_GS2C_Notify_CreateClient notify;
	unsigned int nClient = 0;
	for (nClient = 0; nClient < m_nPlayerMaxSize; nClient++)
	{
		MODI_ClientAvatar * pTemp = m_Players[nClient].m_pClient;
		if (pTemp && pTemp != pClient)
		{
			notify.m_fPosX = pTemp->GetPosX();
			notify.m_fPosZ = pTemp->GetPosZ();
			notify.m_fDir = pTemp->GetDir();
			notify.m_baseInfo = pTemp->GetRoleBaseInfo();
			notify.m_normalInfo = pTemp->GetRoleNormalInfo();
			notify.m_eVolType = pTemp->GetKeyVolType();
			notify.m_fSped = pTemp->GetKeySped();
			pClient->SendPackage(&notify, sizeof(notify));

		}
	}

	for (nClient = 0; nClient < m_nSpectatorMaxSize; nClient++)
	{
		MODI_ClientAvatar * pTemp = m_Spectators[nClient].m_pClient;
		if (pTemp && pTemp != pClient)
		{
			notify.m_fPosX = pTemp->GetPosX();
			notify.m_fPosZ = pTemp->GetPosZ();
			notify.m_fDir = pTemp->GetDir();
			notify.m_baseInfo = pTemp->GetRoleBaseInfo();
			notify.m_normalInfo = pTemp->GetRoleNormalInfo();
			notify.m_eVolType = pTemp->GetKeyVolType();
			notify.m_fSped = pTemp->GetKeySped();
			pClient->SendPackage(&notify, sizeof(notify));
		}
	}
}

//    获得最先进入的参战玩家
MODI_MultiGameRoom::MODI_ClientAvatarInRoom * MODI_MultiGameRoom::GetEarliestEnterInPlayers()
{
    MODI_ClientAvatarInRoom * pRet = 0;
    unsigned int nMin = 0xFFFFFFFF;
    for (unsigned int n = 0; n < m_nPlayerMaxSize; n++)
    {
		if (m_Players[n].m_pClient &&   m_Players[n].m_nEnterCounter < nMin)
		{
			pRet = &m_Players[n];
			nMin = m_Players[n].m_nEnterCounter;
		}
	}
	return pRet;
}

//    获得最先进入的观战玩家
MODI_MultiGameRoom::MODI_ClientAvatarInRoom * MODI_MultiGameRoom::GetEarliestEnterInSpectators()
{
	MODI_ClientAvatarInRoom * pRet = 0;
	unsigned int nMin = 0xFFFFFFFF;
	for (unsigned int n = 0; n < m_nSpectatorMaxSize; n++)
	{
		if (m_Spectators[n].m_pClient &&  m_Spectators[n].m_nEnterCounter < nMin)
		{
			pRet = &m_Spectators[n];
			nMin = m_Spectators[n].m_nEnterCounter;
		}
	}
	return pRet;
}

//    从位置槽中移出一个客户端
bool MODI_MultiGameRoom::RemoveClient(MODI_ClientAvatar * pRemove)
{
	unsigned int n;
	for (n = 0; n < m_nPlayerMaxSize; n++)
	{
		if (m_Players[n].m_pClient == pRemove)
		{
			m_Players[n].Reset();
			return true;
		}
	}

	for (n = 0; n < m_nSpectatorMaxSize; n++)
	{
		if (m_Spectators[n].m_pClient == pRemove)
		{
			m_Spectators[n].Reset();
			return true;
		}
	}
	return false;
}

bool MODI_MultiGameRoom::IsStart() const
{
    return m_iStatus >= ROOM_STATUS_START;
}

bool MODI_MultiGameRoom::IsPlaying() const
{
    return m_iStatus >= ROOM_STATUS_PLAYING;
}

//    房间是否正在开始中(客户端都在读取资源准备就绪的阶段)
bool MODI_MultiGameRoom::IsStarting() const
{
	return m_iStatus == ROOM_STATUS_START;
}

bool MODI_MultiGameRoom::IsVoting() const
{
	return m_iStatus == ROOM_STATUS_VOTE;
}

MODI_GUID MODI_MultiGameRoom::GetSingerForSpector()
{
	// 为观众分配一个唱歌者，目前的分配策略为随机
	MODI_GUID nTemp[MAX_PLAYSLOT_INROOM];
	int iCount = 0;
	for( DWORD i = 0; i < m_nPlayerMaxSize; i++ )
	{
		if( m_Players[i].m_pClient )
		{
			nTemp[iCount] = m_Players[i].m_pClient->GetGUID();
			iCount += 1;
		}
	}	
	
	if( iCount )
	{
		int iRandom = PublicFun::GetRandNum(1,100) % iCount;
		return nTemp[iRandom];
	}

	return INVAILD_GUID;
}

//    获得一个空的参战位置
defRoomPosSolt MODI_MultiGameRoom::GetFreePlayersPosSolt()
{
    defRoomPosSolt n = INVAILD_POSSOLT;
    for (DWORD i = 0; i < m_nPlayerMaxSize; i++)
    {
        //    位置没关，并且位置上面没人
        if (m_Players[i].m_pClient == 0 && m_Players[i].m_bClosed == false)
        {
            n = (defRoomPosSolt) i;
            break;
        }
    }
    return n;
}

// 获得一个空的观战位置
defRoomPosSolt MODI_MultiGameRoom::GetFreeSpectatorPosSolt()
{
    defRoomPosSolt n = INVAILD_POSSOLT;
    for (DWORD i = 0; i < m_nSpectatorMaxSize; i++)
    {
        if (m_Spectators[i].m_pClient == 0)
        {
            n = (defRoomPosSolt) i;
            break;
        }
    }
    return n;
}

//    获得一个空的位置
bool 	MODI_MultiGameRoom::GetFreePosSolt( bool & bIsPlayer , defRoomPosSolt & nPos )
{
	if( IsPlaying() )
	{
		// 游戏中，只能作为观战者
		nPos = GetFreeSpectatorPosSolt();
	}
	else 
	{
		// 优先参战，观战次之
		bIsPlayer = true;
		nPos = GetFreePlayersPosSolt();
		if (nPos == INVAILD_POSSOLT)
		{
			bIsPlayer = false;
			nPos = GetFreeSpectatorPosSolt();
		}
	}

	if( nPos == INVAILD_POSSOLT )
		return false;
	return true;
}

//    最大参战人数
unsigned char MODI_MultiGameRoom::GetMaxPlayers() const
{
    unsigned char nRet = 0;
    for (DWORD i = 0; i < m_nPlayerMaxSize; i++)
    {
        //    位置没关
        if ( m_Players[i].m_pClient || !m_Players[i].m_bClosed )
        {
            nRet += 1;
        }
    }
    return nRet;
}

// 当前参战玩家个数
unsigned char MODI_MultiGameRoom::GetCurPlayersNum() const
{
    unsigned char nRet = 0;
    for (DWORD i = 0; i < m_nPlayerMaxSize; i++)
    {
        if (m_Players[i].m_pClient)
        {
            nRet += 1;
        }
    }
    return nRet;
}

// 当前观战玩家个数
unsigned char MODI_MultiGameRoom::GetCurSpectatorsNum() const
{
    unsigned char nRet = 0;
    for (DWORD i = 0; i < m_nSpectatorMaxSize; i++)
    {
        if (m_Spectators[i].m_pClient)
        {
            nRet += 1;
        }
    }
    return nRet;
}

// 当前房间中的总人数
unsigned char MODI_MultiGameRoom::GetCurClientsNum() const
{
    return GetCurPlayersNum() + GetCurSpectatorsNum();
}

MODI_ClientAvatar * MODI_MultiGameRoom::GetClientAvatarByPos(bool bPlayer, defRoomPosSolt nPos)
{
    MODI_ClientAvatarInRoom * client = GetClientInRoomByPos(bPlayer, nPos);
	if( client )
	{
		return client->m_pClient;
	}
	return 0;
}

MODI_MultiGameRoom::MODI_ClientAvatarInRoom * MODI_MultiGameRoom::GetClientInRoomByPos(bool bPlayer,
        defRoomPosSolt nPos)
{
    if (bPlayer)
    {
        if( nPos >= m_nPlayerMaxSize )
        {
			return 0;
        }

        return &m_Players[nPos];
    }

    if( nPos >= m_nSpectatorMaxSize )
    {
		return 0;
    }

    return &m_Spectators[nPos];
}

bool MODI_MultiGameRoom::IsPlayerClient(MODI_ClientAvatar * pClient)
{
	if( !pClient )
		return false;
    for (unsigned int n = 0; n < m_nPlayerMaxSize; n++)
    {
        if (m_Players[n].m_pClient == pClient)
            return true;
    }
    return false;
}

//    获得房间中性别符合的客户端个数
int  MODI_MultiGameRoom::GetClientsNumBySex( int iSex ,bool bIncludeSpector ) const
{
    unsigned char ucRet = 0;
    unsigned int n = 0;
    for (n = 0; n < m_nPlayerMaxSize; n++)
    {
        if (m_Players[n].m_pClient && m_Players[n].m_pClient->GetRoleBaseInfo().m_ucSex == iSex)
        {
            ucRet += 1;
        }
    }

	if( bIncludeSpector )
	{
		for (n = 0; n < m_nSpectatorMaxSize; n++ )
		{
			if( m_Spectators[n].m_pClient && m_Spectators[n].m_pClient->GetRoleBaseInfo().m_ucSex == iSex)
			{
				ucRet += 1;
			}
		}
	}

	return ucRet;
}

// 当前男孩个数
unsigned char MODI_MultiGameRoom::GetCurBoysNum( bool bIncludeSpector ) const
{
	return GetClientsNumBySex( enSexBoy , bIncludeSpector );
}

// 当前女孩个数
unsigned char MODI_MultiGameRoom::GetCurGirlNum( bool bIncludeSpector ) const
{
	return GetClientsNumBySex( enSexGril , bIncludeSpector );
}

void MODI_MultiGameRoom::NotifyTransMissionMapToGW()
{
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	for( unsigned int n = 0; n < m_nPlayerMaxSize; n++ )
	{
		if( m_Players[n].m_pClient )
		{
			Global::logger->debug("[%s] Start game. room <%u> player <%s> has following spectators: " , 
					GS_ROOMOPT , 
					GetRoomID() , 
					m_Players[n].m_pClient->GetRoleName() );
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_GS2GW_AddTransMission * p_addTransNotify = (MODI_GS2GW_AddTransMission *)(szPackageBuf); 
			AutoConstruct( p_addTransNotify );
			
			p_addTransNotify->m_nCount = 0;
			p_addTransNotify->m_SrcSession = m_Players[n].m_pClient->GetSessionID();
			for( unsigned int i = 0; i < m_nSpectatorMaxSize; i++ )
			{
				if( m_Spectators[i].m_pClient && 
						m_Spectators[i].m_pClient->GetRoleNormalInfo().m_SingerID == m_Players[n].m_pClient->GetGUID() )
				{
					Global::logger->debug("[%s]---> \t<%s>." , GS_ROOMOPT ,  m_Spectators[i].m_pClient->GetRoleName() );
					p_addTransNotify->m_pDestSession[p_addTransNotify->m_nCount] =  m_Spectators[i].m_pClient->GetSessionID();
					p_addTransNotify->m_nCount += 1;
				}
			}

			if( p_addTransNotify->m_nCount )
			{
				int iSendSize = sizeof(MODI_SessionID) * p_addTransNotify->m_nCount + sizeof(MODI_GS2GW_AddTransMission);
				MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_addTransNotify, iSendSize);
			}
		}
	}
}

/*
// 客户端改变传输映射
void MODI_MultiGameRoom::NotifyChangeTransMissionMapToGW( MODI_SessionID nNewSrc , MODI_SessionID nOldSrc , MODI_SessionID * pDest , unsigned int nCount )
{
	NotifyDelTransMission( nOldSrc , pDest , nCount );
	NotifyAddTransMission( nNewSrc , pDest , nCount );
}
*/

// 让网关增加一条传输映射
void MODI_MultiGameRoom::NotifyAddTransMission( const MODI_SessionID & nAdd , const MODI_SessionID * pDest , unsigned int nCount )
{
	if( nAdd.IsInvaild() )
		return ;

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	int iSendSize = 0;

	// 让网关增加新的转发通道
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2GW_AddTransMission * pAddNotify = (MODI_GS2GW_AddTransMission *)(szPackageBuf);
	AutoConstruct( pAddNotify );
	pAddNotify->m_SrcSession = nAdd;
	pAddNotify->m_nCount = nCount;
	if( pDest )
	{
		memcpy( pAddNotify->m_pDestSession , pDest , sizeof(MODI_SessionID) * nCount );
	}
	iSendSize = pAddNotify->m_nCount * sizeof(MODI_SessionID) + sizeof(MODI_GS2GW_AddTransMission);
	MODI_GameTask::ms_pInstancePtr->SendCmd( (const Cmd::stNullCmd *)(pAddNotify) , iSendSize );
}

// 让网关删除一条传输映射
void MODI_MultiGameRoom::NotifyDelTransMission( const MODI_SessionID & nDel , const MODI_SessionID * pDest , unsigned int nCount )
{
	if( nDel.IsInvaild() )
		return ;

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	int iSendSize = 0;

	// 让网关先删除旧的转发通道
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2GW_DelTransMission * pDelNotify = (MODI_GS2GW_DelTransMission *)(szPackageBuf);
	AutoConstruct( pDelNotify );
	pDelNotify->m_SrcSession = nDel;
	pDelNotify->m_nCount = nCount;
	if( pDest )
	{
		memcpy( pDelNotify->m_pDestSession , pDest , sizeof(MODI_SessionID) * nCount );
	}
	iSendSize = pDelNotify->m_nCount * sizeof(MODI_SessionID) + sizeof(MODI_GS2GW_DelTransMission);
	MODI_GameTask::ms_pInstancePtr->SendCmd( (const Cmd::stNullCmd *)(pDelNotify) , iSendSize );
}

//   获得当前歌曲的歌词句数
unsigned short MODI_MultiGameRoom::GetSentencesCount() const
{
	unsigned short nCount =  MODI_MdmMgr::GetInstancePtr()->GetSencentsSize( GetNormalInfo().m_nMusicId );
	return nCount;
}

//   获得当前歌曲的NOTE总长度
float 	MODI_MultiGameRoom::GetTotalNodeLength() const
{
	return m_fTotalNodeLength;
}

void  MODI_MultiGameRoom::TransferMaster( const MODI_GUID & new_master )
{
	MODI_ClientAvatar * target = FindClientByGUID( new_master );
	if( !target )
		return ;

	SetMaster( target->GetGUID() );

	OnMasterChanged( target );
}

void  MODI_MultiGameRoom::TransferMaster( MODI_ClientAvatar * pNewMaster  )
{
	if( pNewMaster )
	{
		SetMaster( pNewMaster->GetGUID() );
		OnMasterChanged( pNewMaster );
	}
}

MODI_ClientAvatar * MODI_MultiGameRoom::FindClientByGUID( MODI_GUID nGUID ) const
{
	MODI_ClientAvatar * pTemp = FindPlayerByGUID( nGUID );

	if( !pTemp )
	{
		pTemp = FindSpectorByGUID( nGUID );
	}

	return pTemp;
}

MODI_ClientAvatar * MODI_MultiGameRoom::FindPlayerByGUID( MODI_GUID nGUID ) const
{
	if( nGUID == INVAILD_GUID )
		return 0;

	unsigned int n = 0;
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		if( m_Players[n].m_pClient && m_Players[n].m_pClient->GetGUID() == nGUID )
		{
			return m_Players[n].m_pClient;
		}
	}
	return 0;
}

	
MODI_ClientAvatar * MODI_MultiGameRoom::FindSpectorByGUID( MODI_GUID nGUID ) const
{
	if( nGUID == INVAILD_GUID )
		return 0;

	unsigned int n = 0;
	for( n = 0; n < m_nSpectatorMaxSize; n++ )
	{
		if( m_Spectators[n].m_pClient && m_Spectators[n].m_pClient->GetGUID() == nGUID )
		{
			return m_Spectators[n].m_pClient;
		}
	}

	return 0;
}

void MODI_MultiGameRoom::SendMusicHeader( MODI_ClientAvatar  * pClient , int i )
{
	// 要发送的客户端，观战位置上的某个客户端
	MODI_ClientAvatar * pDestClient = GetClientAvatarByPos( false , i );
	if( pDestClient )
	{
		// 发送
		pClient->SendMusicHeader( pDestClient );
	}
	else 
	{
		Global::logger->warn("[%s] can't send musicheader to client.<room id = %u , src postion = %d >." , 
			GS_ROOMOPT , GetRoomID() , i );
		MODI_ASSERT(0);
	}
}

void MODI_MultiGameRoom::SetAllowSpectator( bool bSet )
{
	m_bOffSpectator = bSet ? false : true;

	if( IsAllowSpectator() == false )
	{
		// 不允许观战
		// 踢掉观战位置上的人
		for( DWORD i = 0; i < m_nSpectatorMaxSize; i++ )
		{
			MODI_ClientAvatar * pTarget = GetClientAvatarByPos( false , i );
			if (pTarget)
			{
				LeaveRoom(pTarget, enLeaveReason_Kick);
			}
		}
	}
	
}

// 房间是否已满
bool MODI_MultiGameRoom::IsFull()
{
	if( IsPlaying() )
	{
		// 游戏中的处理
		if( IsAllowSpectator() )
		{
			// 观战位置还有空位，则房间未满
			if( GetFreeSpectatorPosSolt() !=  INVAILD_POSSOLT )
				return false;
			return true;
		}
		// 不能观战，则算是房间已满
		return true;
	}

	// 游戏未开始
	if( IsAllowSpectator() )
	{
		bool bIsPlayer = false;
		defRoomPosSolt nPos = INVAILD_POSSOLT;
		if( GetFreePosSolt( bIsPlayer , nPos ) )
			return false;
	}
	else 
	{
		// 无法观战，则看参战位置是否还有空位
		if( GetFreePlayersPosSolt() != INVAILD_POSSOLT )
			return false;
	}

	return true;
}

void MODI_MultiGameRoom::SyncReadyState()
{
	for( DWORD i = 0; i < m_nPlayerMaxSize; i++ )
	{
		if( m_Players[i].m_pClient )
		{
			// 同步所有玩家的准备状态给所有人
			MODI_GS2C_Notify_SbReadyStateChanged sbReadyNotify;
			sbReadyNotify.m_nClientGUID = m_Players[i].m_pClient->GetGUID();
			sbReadyNotify.m_byReady = m_Players[i].m_bReadyInRoom ? 1 : 0;
			Broadcast( &sbReadyNotify , sizeof(sbReadyNotify) );
		}
	}
}

void MODI_MultiGameRoom::SyncPosionState()
{
	DWORD i = 0;
	for(i=0 ; i < m_nPlayerMaxSize; i++ )
	{
		// 同步所有位置的开关状态给所有人
		MODI_GS2C_Notify_PostionStateChagned notify;
		notify.m_nTargetPos = RoleHelpFuns::EncodePosSolt( true , i );
		notify.m_byClosed = m_Players[i].m_bClosed ? 1 : 0;
		Broadcast( &notify , sizeof(notify) );
	}
}

void MODI_MultiGameRoom::SendVote( const MODI_GUID & src , const MODI_GUID & dest )
{
	MODI_GS2C_Notify_SbVoteTo msg;
	msg.m_voter = src;
	msg.m_player  = dest;
	Broadcast( &msg , sizeof(msg) );
}

void MODI_MultiGameRoom::SendVoteResult( const MODI_GUID & dest , BYTE byCount )
{
	MODI_GS2C_Notify_VoteResult msg;
	msg.player = dest;
	msg.votecount = byCount;
	Broadcast( &msg , sizeof(msg) );
}

void MODI_MultiGameRoom::SyncVoteState( MODI_ClientAvatar * pClient )
{
	// 同步所有票数
	for( DWORD n = 0; n < m_nPlayerMaxSize; n++ )
	{
		if( m_Players[n].m_pClient )
		{
			MODI_GS2C_Notify_VoteResult msg;
			msg.player = m_Players[n].m_pClient->GetGUID();
			msg.votecount = m_Players[n].m_byVoteCount;
			pClient->SendPackage( &msg , sizeof(msg) );
		}
	}
}
	
void MODI_MultiGameRoom::InitPostion( MODI_ClientAvatar * pClient )
{
	if( pClient->IsPlayer() )
	{
		pClient->SetPosX( -300.0f );
		pClient->SetPosZ( 60.0f );
		pClient->SetDir( 0.0f );

		const GameTable::MODI_Scenelist * pScene = MODI_BinFileMgr::GetInstancePtr()->GetSceneBinFile().Get( 1 );
		if( pScene )
		{
			float fRightX = pScene->get_Max_X();
			float fLeftX = pScene->get_Min_X();
			if( fLeftX > fRightX )
			{
				std::swap(fLeftX,fRightX);
			}

			float fTop = pScene->get_Min_Z();
			float fBotton = pScene->get_Max_Z();
			if( fTop > fBotton )
			{
				std::swap(fTop,fBotton);
			}

			int iX = (int)(fRightX - fLeftX);
			int iZ = (int)(fBotton - fTop);
			if( iX && iZ )
			{
				iX = rand()%iX;
				iZ = rand()%iZ;
			}

			pClient->SetPosX( fLeftX + iX );
			pClient->SetPosZ( fTop + iZ );
		}

		pClient->IncLogicCount();

		MODI_GS2C_Notify_AdjustPostion msg;
		msg.m_obj = pClient->GetGUID();
		msg.m_fPosX  = pClient->GetPosX();
		msg.m_fPosZ = pClient->GetPosZ();
		msg.m_fDir = pClient->GetDir();
		msg.m_nServerLogicCount = pClient->GetLogicCount();
		pClient->SendPackage( &msg, sizeof(msg) );


	}
}

const char * MODI_MultiGameRoom::GetMasterName() const
{
	static const char * unknow_master_name = "unknow";
	MODI_ClientAvatar * master = FindClientByGUID( GetMaster() );
	if( master )
	{
		return master->GetRoleName();
	}
	return unknow_master_name ;
}


void MODI_MultiGameRoom::OnMasterChanged( MODI_ClientAvatar * pNewMaster )
{
	bool bNewMasterIsPlayer = RoleHelpFuns::IsSpectator(pNewMaster->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家
	defRoomPosSolt nNewMasterPos = RoleHelpFuns::GetPosSolt(pNewMaster->GetRoleNormalInfo().m_roomSolt); // 位置索引
	MODI_ClientAvatarInRoom * newMasterClient = GetClientInRoomByPos( bNewMasterIsPlayer , nNewMasterPos );
	if(! newMasterClient)
	{
		Global::logger->debug("[change_master] <isplayer=%d,pos=%y>", bNewMasterIsPlayer, nNewMasterPos);
		MODI_ASSERT(0);
	}
	
	if( newMasterClient->m_bReadyInRoom )
	{
		newMasterClient->m_bReadyInRoom = false;
		MODI_GS2C_Notify_SbReadyStateChanged readyChangeNotify;
		readyChangeNotify.m_byReady = false;
		readyChangeNotify.m_nClientGUID = pNewMaster->GetGUID();
		Broadcast( &readyChangeNotify , sizeof(readyChangeNotify) );
	}

	//    同步
	MODI_GS2C_Notify_NewMaster newMastNotify;
	newMastNotify.m_roomID = this->GetRoomID();
	newMastNotify.m_objID = pNewMaster->GetGUID();
	this->Broadcast(&newMastNotify, sizeof(newMastNotify));
}


/** 
 * @brief 设置房间主类型
 * 
 */
void MODI_MultiGameRoom::SetMainType(ROOM_TYPE room_main_type)
{
	m_enRoomMainType = room_main_type;
}


/** 
 * @brief 获取房间主类型
 * 
 */
const ROOM_TYPE MODI_MultiGameRoom::GetMainType()
{
	return m_enRoomMainType;
}


/** 
 * @brief 设置房间子类型
 * 
 */
void MODI_MultiGameRoom::SetSubType(ROOM_SUB_TYPE room_sub_type)
{
	m_enRoomSubType = room_sub_type;
}


/** 
 * @brief 获取房间子类型
 * 
 */
const ROOM_SUB_TYPE & MODI_MultiGameRoom::GetSubType()
{
	return m_enRoomSubType;
}


/** 
 * @brief 设置组队
 * 
 * 
 */
void  MODI_MultiGameRoom::SetAllTeam()
{
	ClearAllTeam();
	if(m_Players[0].m_pClient != 0 && m_Players[0].m_bClosed == false)
	{
		m_byTeamState[0] = ROOM_TEAM_RED;
	}

	if(m_Players[1].m_pClient != 0 && m_Players[1].m_bClosed == false)
	{
		m_byTeamState[1] = ROOM_TEAM_RED;
	}


	if(m_Players[2].m_pClient != 0 && m_Players[2].m_bClosed == false)
	{
		m_byTeamState[2] = ROOM_TEAM_YELLOW;
	}


	if(m_Players[3].m_pClient != 0 && m_Players[3].m_bClosed == false)
	{
		m_byTeamState[3] = ROOM_TEAM_YELLOW;
	}

	if(m_Players[4].m_pClient != 0 && m_Players[4].m_bClosed == false)
	{
		m_byTeamState[4] = ROOM_TEAM_BLUE;
	}

	if(m_Players[5].m_pClient != 0 && m_Players[5].m_bClosed == false)
	{
		m_byTeamState[5] = ROOM_TEAM_BLUE;
	}
}


void MODI_MultiGameRoom::ClearAllTeam()
{
	m_byTeamState[0] = 0;
	m_byTeamState[1] = 0;
	m_byTeamState[2] = 0;
	m_byTeamState[3] = 0;
	m_byTeamState[4] = 0;
	m_byTeamState[5] = 0;
}

void MODI_MultiGameRoom::SyncKeyModeParam(MODI_ClientAvatar * pClient)
{
	MODI_GS2C_Notify_SbKeyModeParamChange change;
	change.m_Guid = pClient->GetGUID();
	change.m_eVolType = pClient->GetKeyVolType();
	change.m_fSped = pClient->GetKeySped();
	Global::logger->debug("[modifykey] client %s changed key mode prarm sped=%f,type=%u room ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`",pClient->GetRoleName(),change.m_fSped,change.m_fSped);
	Broadcast( &change,sizeof(MODI_GS2C_Notify_SbKeyModeParamChange));
}


/// 增加一个ispeakid
bool MODI_MultiGameRoom::AddISpeakId(const MODI_ISpeak_Id & id)
{
	MODI_GUID guid = id.m_stGuid;
	int is_id = id.m_stISpeakId;
	std::map<MODI_GUID, int>::iterator iter = m_stISpeakIdMap.find(guid);
	if(iter != m_stISpeakIdMap.end())
	{
		Global::logger->debug("[add_ispeak_id] add_ispeak_id al have <guid=%llu>", (QWORD)guid);
		//MODI_ASSERT(0);
		return false;
	}
	m_stISpeakIdMap.insert(std::map<MODI_GUID, int>::value_type(guid, is_id));
	
#ifdef _DEBUG
	Global::logger->debug("[add_ispeak_id] add_ispeak_id successful <guid=%llu, isid=%d>", (QWORD)guid, is_id);
#endif
	
	/// 广播isspeakid
	BroadcastISpeakId();
	return true;
}

/// yy更改
bool MODI_MultiGameRoom::ChangeYYChatState(const MODI_YYChatState & state)
{
	m_stYYChatState[state.m_stGuid] = state.m_byState;
	
	/// 广播
	BroadcastYYChatState();
	return true;
}


/// 减少一个ispeakid
bool MODI_MultiGameRoom::DelISpeakId(const MODI_GUID & guid)
{
	std::map<MODI_GUID, int>::iterator iter = m_stISpeakIdMap.find(guid);
	if(iter == m_stISpeakIdMap.end())
	{
		Global::logger->debug("[del_ispeak_id] del_ispeak_id but not find <guid=%llu>", (QWORD)guid);
		//MODI_ASSERT(0);
		return false;
	}
	
#ifdef _DEBUG	
	Global::logger->debug("[del_ispeak_id] del_ispeak_id successful <guid=%llu>", (QWORD)guid);
#endif
	
	m_stISpeakIdMap.erase(iter);
	return true;
}

/// 减少一个YYChat
bool MODI_MultiGameRoom::DelYYChatState(const MODI_GUID & guid)
{
	std::map<MODI_GUID, BYTE>::iterator iter = m_stYYChatState.find(guid);
	if(iter == m_stYYChatState.end())
	{
		Global::logger->debug("[del_yy_ch] del_yych but not find <guid=%llu>", (QWORD)guid);
		//MODI_ASSERT(0);
		return false;
	}
#ifdef _DEBUG	
	Global::logger->debug("[del_yy_chat] del_yy_chat successful <guid=%llu>", (QWORD)guid);
#endif
	
	m_stYYChatState.erase(iter);
	return true;
}

/// 广播
void MODI_MultiGameRoom::BroadcastISpeakId()
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_ISpeak * p_send_cmd = (MODI_GS2C_Notify_ISpeak *)buf;
	AutoConstruct(p_send_cmd);
	MODI_ISpeak_Id * p_buf = (MODI_ISpeak_Id *)p_send_cmd->m_pData;
	p_send_cmd->m_wdSize = 0;
	
	std::map<MODI_GUID, int>::iterator iter = m_stISpeakIdMap.begin();
	for(; iter != m_stISpeakIdMap.end(); iter++)
	{
		p_buf->m_stGuid = iter->first;
		p_buf->m_stISpeakId = iter->second;
		p_buf++;
		p_send_cmd->m_wdSize++;
	}

	if(p_send_cmd->m_wdSize > 0)
	{		
		Broadcast(p_send_cmd, sizeof(MODI_GS2C_Notify_ISpeak) + sizeof(MODI_ISpeak_Id) * p_send_cmd->m_wdSize);
	}

#ifdef _DEBUG
	char recv_buf[1024];
	memset(recv_buf, 0, sizeof(recv_buf));
	memcpy(recv_buf, p_send_cmd, sizeof(MODI_GS2C_Notify_ISpeak) + sizeof(MODI_ISpeak_Id) * p_send_cmd->m_wdSize);

	const MODI_GS2C_Notify_ISpeak * p_recv_cmd = (const MODI_GS2C_Notify_ISpeak *)recv_buf;
	const MODI_ISpeak_Id * p_id = (const MODI_ISpeak_Id * )p_recv_cmd->m_pData;
	for(WORD i=0; i<p_recv_cmd->m_wdSize; i++)
	{
		Global::logger->debug("[send_is_id] <guid=%llu, isid=%d>", (QWORD)(p_id->m_stGuid), p_id->m_stISpeakId);
		p_id++;
	}
	
#endif	
}


/// 广播
void MODI_MultiGameRoom::BroadcastYYChatState()
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_YYChatState_Cmd * p_send_cmd = (MODI_GS2C_Notify_YYChatState_Cmd *)buf;
	AutoConstruct(p_send_cmd);
	MODI_YYChatState * p_buf = (MODI_YYChatState *)p_send_cmd->m_pDate;
	p_send_cmd->m_wdSize = 0;
	
	std::map<MODI_GUID, BYTE>::iterator iter = m_stYYChatState.begin();
	for(; iter != m_stYYChatState.end(); iter++)
	{
		p_buf->m_stGuid = iter->first;
		p_buf->m_byState = iter->second;
		p_buf++;
		p_send_cmd->m_wdSize++;
	}

	if(p_send_cmd->m_wdSize > 0)
	{
		Broadcast(p_send_cmd, sizeof(MODI_GS2C_Notify_YYChatState_Cmd) + sizeof(MODI_YYChatState) * p_send_cmd->m_wdSize);
	}

#ifdef _DEBUG
	char recv_buf[1024];
	memset(recv_buf, 0, sizeof(recv_buf));
	memcpy(recv_buf, p_send_cmd, sizeof(MODI_GS2C_Notify_YYChatState_Cmd) + sizeof(MODI_YYChatState) * p_send_cmd->m_wdSize);

	const MODI_GS2C_Notify_YYChatState_Cmd * p_recv_cmd = (const MODI_GS2C_Notify_YYChatState_Cmd *)recv_buf;
	const MODI_YYChatState * p_id = (const MODI_YYChatState * )p_recv_cmd->m_pDate;
	for(WORD i=0; i<p_recv_cmd->m_wdSize; i++)
	{
		Global::logger->debug("[send_is_id] <guid=%llu, isid=%d>", (QWORD)(p_id->m_stGuid), p_id->m_byState);
		p_id++;
	}
	
#endif	
}
