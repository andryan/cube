#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "gamestructdef.h"

/// 客户端准备就绪的处理
int MODI_PackageHandler_c2gs::IamReady_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);

	CHECK_PACKAGE( MODI_C2GS_Request_IamReady , cmd_size , 0 , 0 , pClient );
    MODI_C2GS_Request_IamReady & req = *((MODI_C2GS_Request_IamReady *)(pt_null_cmd));
	DISABLE_UNUSED_WARNING(req);

    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent ready request. ", GS_ROOMOPT, pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

    if (!room->IsStart())
    {
        Global::logger->warn("[%s] room <%u> not yet ready, player <%s> sent ready request. ", GS_ROOMOPT,
                pClient->GetRoomID(), pClient->GetRoleName());
        return enPHandler_Warning;
    }

    ///    客户端就绪
    if (room->ClientReadyPlay(pClient))
    {

    }

    return enPHandler_OK;
}

///     客户端告知服务器，一句歌词的得分
int MODI_PackageHandler_c2gs::SentenceScore_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_SentenceScore , cmd_size , 0 , 0 , pClient );

	MODI_C2GS_Request_SentenceScore * pReq = ( MODI_C2GS_Request_SentenceScore * )( pt_null_cmd );
    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent score of lyric. ", 
				GS_ROOMOPT, 
                pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if ( !room )
	{
		return enPHandler_Warning;
	}

	QWORD _flag = pClient->GetFlag();
	if (_flag == 0)
	{
		Global::logger->debug("[%s] room <%u>, player <%s> initialise flag (%lld).",
				GS_ROOMOPT,
				pClient->GetRoomID(), pClient->GetRoleName(), pReq->m_nFlag);
	}

	if (pReq->m_nFlag <= _flag)
	{
		Global::logger->warn("[%s] room <%u>, player <%s> sent duplicate packet (%lld <= %lld).",
				GS_ROOMOPT,
				pClient->GetRoomID(), pClient->GetRoleName(), pReq->m_nFlag, _flag);
        return enPHandler_Warning;
	}
	pClient->SetFlag( pReq->m_nFlag );

	room->GameScore( pClient , 
			pReq->m_nNo,
			pReq->m_fTime,
			pReq->m_nScore , 
			pReq->m_nType , 
			pReq->m_fExact,
			pReq->m_fRightSec,
			pReq->m_nCombat );

	return enPHandler_OK;
}

/// 	客户端告知服务器，某个效果开始
int MODI_PackageHandler_c2gs::GameEffectBegin( void * pObj , 
		const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_EffectBegin , cmd_size , 0 , 0 , pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent start GameEffect request. ", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

    if (room->IsPlaying() == false )
    {
        Global::logger->warn("[%s] room <%u> not in match game status ,but player <%s> sent start GameEffect request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return enPHandler_Warning;
	}

	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is spectator , but sent start GameEffect request." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return enPHandler_Warning;
	}

	MODI_C2GS_Request_EffectBegin * pReq = (MODI_C2GS_Request_EffectBegin  * )(pt_null_cmd);
	if( pReq->m_byEffectType ==  enEffect_FerverTime )
	{
		MODI_GS2C_Notify_EffectBegin  notify;
		notify.m_nClientID = pClient->GetGUID();
		notify.m_byEffectType = enEffect_FerverTime;
		pClient->SetEffectState( enEffect_FerverTime );
		room->Broadcast_Except( &notify , sizeof(notify) , pClient );
	}
	else 
	{
		Global::logger->warn("[%s] room <%u> player <%s> sent an invalid start GameEffect request." , 
				GS_ROOMOPT , 
				room->GetRoomID() , 
				pClient->GetRoleName() );
	}

	return enPHandler_OK;
}

///    客户端告知服务器某个游戏效果结束
int MODI_PackageHandler_c2gs::GameEffectOver(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, 
		const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_EffectOver , cmd_size , 0 , 0 , pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent end GameEffect request. ", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

    if (room->IsPlaying() == false )
    {
        Global::logger->warn("[%s] room <%u> not in match game status ,but player <%s> sent end GameEffect request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return enPHandler_Warning;
	}

	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is spectator , but sent end GameEffect request." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return enPHandler_Warning;
	}

	MODI_C2GS_Request_EffectOver * pReq = (MODI_C2GS_Request_EffectOver * )(pt_null_cmd);
	if( pReq->m_byEffectType ==  enEffect_FerverTime )
	{
		MODI_GS2C_Notify_EffectOver notify;
		notify.m_nClientID = pClient->GetGUID();
		notify.m_byEffectType = enEffect_FerverTime;
		pClient->SetEffectTime( 0.0f );
		pClient->ClearEffectState( enEffect_FerverTime );
		room->Broadcast_Except( &notify , sizeof(notify) , pClient );
	}
	else 
	{
		Global::logger->warn("[%s] room <%u> player <%s> sent an invalid end GameEffect request." , 
				GS_ROOMOPT , 
				room->GetRoomID() , 
				pClient->GetRoleName() );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::GameSyncGPower( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);

	CHECK_PACKAGE( MODI_C2GS_Request_SyncGPower , cmd_size , 0 , 0 , pClient );
	MODI_C2GS_Request_SyncGPower * pReq = (MODI_C2GS_Request_SyncGPower * )(pt_null_cmd);

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent end SyncGPower request. ", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

    if (room->IsPlaying() == false )
    {
        Global::logger->warn("[%s] room <%u> not in match game status ,but player <%s> sent SyncGPower<release=%d,time=%f> request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName(),
				pReq->m_bRelease ,
				pReq->m_fSynctime );
        return enPHandler_Warning;
	}

	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is spectator , but sent SyncGPower request<release=%d,time=%f>." ,
			   	GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() ,
				pReq->m_bRelease , 
				pReq->m_fSynctime );
		return enPHandler_Warning;
	}

	MODI_GS2C_Notify_SyncGPower notify;
	notify.m_bRelease = pReq->m_bRelease;
	notify.m_fSynctime = pReq->m_fSynctime;
	notify.m_guidClient = pClient->GetGUID();
	pClient->SetEffectRelease( pReq->m_bRelease );
	pClient->SetEffectTime( pReq->m_fSynctime );

	room->Broadcast_Spectators( pClient , &notify , sizeof(notify) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2gs::ListernOver( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ListenersOver , cmd_size , 0 , 0 , pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent ListenersOver", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

	if (room->IsPlaying() == false )
	{
        Global::logger->warn("[%s] room <%u> not in match game status ,but player <%s> sent end ListenersOver request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return enPHandler_Warning;
	}

	if( pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is player , but sent end ListenersOver request." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return enPHandler_Warning;
	}

	room->OnListernOver( pClient->GetGUID() );

	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::VoteTo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_VoteTo , cmd_size , 0 , 0 , pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent voteto", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

	if( pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is player , but sent voteto request." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return enPHandler_Warning;
	}

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

    if ( room->IsVoting() == false )
    {
        Global::logger->warn("[%s] room <%u> not in vote status ,but player <%s> sent voteto request. ", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return enPHandler_Warning;
	}
	
	const MODI_C2GS_Request_VoteTo * pReq = (const MODI_C2GS_Request_VoteTo *)(pt_null_cmd);
	room->OnVoteTo( pClient , pReq->m_player );

	return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::MicHasData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_MicHasData , cmd_size , 0 , 0 , pClient );

	if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent MicHasData package.", 
				GS_ROOMOPT, 
				pClient->GetRoleName());
        return enPHandler_Warning;
    }

	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is not player , but sent MicHasData package." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return enPHandler_Warning;
	}

	const MODI_C2GS_Request_MicHasData * pReq = (const MODI_C2GS_Request_MicHasData *)(pt_null_cmd);
    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());

	if( !room )
	{
		return enPHandler_Warning;
	}

	MODI_GS2C_Notify_MicHasData msg;
	msg.m_bHasData = pReq->m_bHasData;
	msg.m_guid = pClient->GetGUID();
	msg.m_fTime = pReq->m_fTime;
	room->Broadcast_Except( &msg , sizeof(msg) , pClient );

	return enPHandler_OK;
}



int MODI_PackageHandler_c2gs::OneKeyScore_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_OneKeyScore , cmd_size , 0 , 0 , pClient );

	MODI_C2GS_Request_OneKeyScore * pReq = ( MODI_C2GS_Request_OneKeyScore * )( pt_null_cmd );
    if (!pClient->IsInMulitRoom())
    {
        Global::logger->warn("[%s] player <%s> not in room but sent score of lyric. ", 
				GS_ROOMOPT, 
                pClient->GetRoleName());
        return enPHandler_Warning;
    }

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_MultiGameRoom * room = pLobby->GetMultiRoom(pClient->GetRoomID());
	if ( !room )
	{
		return enPHandler_Warning;
	}

	QWORD _flag = pClient->GetFlag();
	if (_flag == 0)
	{
		Global::logger->debug("[%s] room <%u>, player <%s> initialise flag (%lld).",
				GS_ROOMOPT,
				pClient->GetRoomID(), pClient->GetRoleName(), pReq->m_nFlag);
	}

	if (pReq->m_nFlag <= _flag)
	{
		Global::logger->warn("[%s] room <%u>, player <%s> sent duplicate packet (%lld <= %lld).",
				GS_ROOMOPT,
				pClient->GetRoomID(), pClient->GetRoleName(), pReq->m_nFlag, _flag);
        return enPHandler_Warning;
	}
	pClient->SetFlag( pReq->m_nFlag );

	room->GameScore_Key( pClient , 
			pReq->m_fTime,
			pReq->m_nScore , 
			pReq->m_nType , 
			pReq->m_fExact,
			pReq->m_nCombat,
		       pReq->m_tFever	);


	return enPHandler_OK;
}

