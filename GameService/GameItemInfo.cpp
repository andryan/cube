/**
 * @file   ItemInfo.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 17:41:53 2011
 * 
 * @brief  游戏中的物品
 * 
 * 
 */


#include "Global.h"
#include "GameItemInfo.h"
#include "BagPosInfo.h"
#include "Bag.h"
#include "GameServerAPP.h"
#include "ItemOperator.h"
#include "ScriptManager.h"

/** 
 * @brief 初始化
 *
 */
bool MODI_GameItemInfo::Init(const MODI_CreateItemInfo & create_item_info)
{
	Global::m_stItemIdLock.wrlock();
	Global::g_qdItemId++;
	m_stItemInfo.m_qdItemId = Global::g_qdItemId;
	Global::m_stItemIdLock.unlock();
	m_stItemInfo.m_dwConfigId = create_item_info.m_dwConfigId;
	m_stItemInfo.m_byServerId = create_item_info.m_byServerId;
	m_stItemInfo.m_byCreateReason = (BYTE)create_item_info.m_enCreateReason;
	m_stItemInfo.m_byLimit = create_item_info.m_byLimit;
	
	DWORD item_limit = 0;
	MODI_ItemOperator::GetAvatarLimit(create_item_info.m_dwConfigId, (enShopGoodTimeType)create_item_info.m_byLimit, item_limit);
	
	if(item_limit != enForever)
	{
		m_stItemInfo.m_qdExpireTime = item_limit + Global::m_stLogicRTime.GetSec();
	}
	
	m_enItemType = ::GetItemType((WORD)create_item_info.m_dwConfigId);

	return true;
}


/** 
 * @brief 初始化
 *
 */
bool MODI_GameItemInfo::Init(MODI_ClientAvatar * p_client, const MODI_DBItemInfo * p_item_info)
{
	m_stItemInfo = *p_item_info;
	m_enItemType = ::GetItemType((WORD)m_stItemInfo.m_dwConfigId);
	
	/// 更新包裹信息
	MODI_Bag * p_bag = NULL;
	if(GetBagType() == enBagType_Avatar)
	{
		p_bag = &(p_client->GetAvatarBag());
	}
	else if(GetBagType() == enBagType_PlayerBag)
	{
		p_bag = &(p_client->GetPlayerBag());
	}
	else
	{
		Global::logger->error("[add_item] item not have bag type <itemid=%llu>", GetItemId());
		MODI_ASSERT(0);
		return false;
	}


	time_t	expiretime= p_item_info->m_qdExpireTime - Global::m_stRTime.GetSec();
	p_bag->AddItemNoUpdate(this);
	MODI_ItemOperator::HaveItemImpact(p_client, this);
	MODI_TriggerItemImpact  script( GetConfigId());
	MODI_ItemImpactManager::GetInstance().Execute(p_client,script);
	if( expiretime < 24*3600 ) 	
	{
		return false;
	}
	

	return true;
}



/** 
 * @brief 增加到列表和数据库中，并通知客户端
 * 
 */
bool MODI_GameItemInfo::AddMeToBag(MODI_Bag * p_bag)
{
	if(!p_bag)
	{
		Global::logger->debug("[add_me_to_bag] get bag null");
		MODI_ASSERT(0);
		return false;
	}
	MODI_ClientAvatar * p_client = p_bag->GetOwner();
	if(! p_client)
	{
		Global::logger->debug("[add_me_to_bag] get pclient null");
		MODI_ASSERT(0);
		return false;
	}

	m_pOwnBag = p_bag;
	m_pOwner = p_client;

	m_stItemInfo.m_byBagType = p_bag->GetBagType();
	
	
	/// 更新包裹
	if(! p_bag->AddItemUpdate(this))
	{
		Global::logger->error("[add_item] add item failed, but save to db <itemid=%llu>", GetItemId());
		MODI_ASSERT(0);
		return false;
	}
	
	/// 插入到数据库
	if(!SaveToDB())
	{
		MODI_ASSERT(0);
		return false;
	}
	
	/// 通知客户端
	NotifyClientAddItem();

	/// 是否拥有效果
	MODI_ItemOperator::HaveItemImpact(p_client, this);

	/// 执行拥有物品的脚本
	//MODI_TriggerItemImpact  script(GetConfigId());	
	//MODI_ItemImpactManager::GetInstance().Execute(p_client,script);
	
	return true;
}



/** 
 * @brief 增加到列表，并通知客户端
 * 
 */
bool MODI_GameItemInfo::AddMeToBag(MODI_Bag * p_bag, MODI_ByteBuffer & save_sql)
{
	if(!p_bag)
	{
		Global::logger->debug("[add_me_to_bag] get bag null");
		MODI_ASSERT(0);
		return false;
	}
	MODI_ClientAvatar * p_client = p_bag->GetOwner();
	if(! p_client)
	{
		Global::logger->debug("[add_me_to_bag] get pclient null");
		MODI_ASSERT(0);
		return false;
	}

	m_pOwnBag = p_bag;
	m_pOwner = p_client;

	m_stItemInfo.m_byBagType = p_bag->GetBagType();
	
	
	/// 更新包裹
	if(! p_bag->AddItemUpdate(this))
	{
		Global::logger->error("[add_item] add item failed, but save to db <itemid=%llu>", GetItemId());
		MODI_ASSERT(0);
		return false;
	}
	
	/// 插入到数据库
	if(! MakeSaveSql(save_sql))
	{
		MODI_ASSERT(0);
		return false;
	}
	
	/// 通知客户端
	NotifyClientAddItem();

	/// 是否拥有效果
	MODI_ItemOperator::HaveItemImpact(p_client, this);

	return true;
}


/** 
 * @brief 从包裹中删除某个道具
 * 
 * @param del_reason 删除理由
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_GameItemInfo::DelMeFromBag(const enDelItemReason & del_reason)
{
	if(! m_pOwnBag)
	{
		Global::logger->error("[del_from_bag] own bag null");
		MODI_ASSERT(0);
		return false;
	}
		
	MODI_ClientAvatar * p_client = m_pOwnBag->GetOwner();
	if(! p_client)
	{
		Global::logger->error("[del_from_bag] own client null");
		MODI_ASSERT(0);
		return false;
	}

	bool need_update = false;
	if(GetBagType() == enBagType_Avatar)
	{
		need_update = true;
	}
	
	/// 从数据库中删除
	DelFromDB();
	
	/// 通知客户端
	NotifyClientDelItem(del_reason);
	
	/// 更新包裹(删除道具释放道具)
	m_pOwnBag->DelItemUpdate(this);

	/// 需要更新avatar形象
	if(need_update)
	{
		p_client->UpdateAvatarByAvatarBag();
	}
	
	return true;
}



/** 
 * @brief 保存到数据库
 * 
 * 
 * @return 失败false
 *
 */
bool MODI_GameItemInfo::MakeSaveSql(MODI_ByteBuffer & result)
{
	MODI_ClientAvatar * p_client = GetOwner();
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_Record record_insert;
	record_insert.Put("itemid", GetItemId());
	record_insert.Put("accid", p_client->GetAccountID());
	record_insert.Put("configid",GetConfigId());
	record_insert.Put("create_reason",GetCreateReason());
	record_insert.Put("bag_type", GetBagType());
	record_insert.Put("bag_pos", GetBagPos());
	record_insert.Put("time_type", m_stItemInfo.m_byLimit);
	std::ostringstream os;
	os<< "from_unixtime(" << m_stItemInfo.m_qdExpireTime<< ")";
	record_insert.Put("expire_time",os.str());
	record_insert.Put("server_id",GetServerId());
	
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);
	if(! MODI_DBClient::MakeInsertSql(result, GlobalDB::m_pItemInfoTbl, &record_container))
	{
		Global::logger->debug("[make_save_item_todb] make save item sql famild <itemid=%u>", GetItemId());
		return false;
	}
	
	return true;
}


/** 
 * @brief 保存到数据库
 * 
 * 
 * @return 失败false
 *
 */
bool MODI_GameItemInfo::SaveToDB()
{
	MODI_ClientAvatar * p_client = GetOwner();
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	MODI_Record record_insert;
	record_insert.Put("itemid", GetItemId());
	record_insert.Put("accid", p_client->GetAccountID());
	record_insert.Put("configid",GetConfigId());
	record_insert.Put("create_reason",GetCreateReason());
	record_insert.Put("bag_type", GetBagType());
	record_insert.Put("bag_pos", GetBagPos());
	record_insert.Put("time_type", m_stItemInfo.m_byLimit);
	std::ostringstream os;
	os<< "from_unixtime(" << m_stItemInfo.m_qdExpireTime<< ")";
	record_insert.Put("expire_time",os.str());
	record_insert.Put("server_id",GetServerId());
	
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);
	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeInsertSql(result, GlobalDB::m_pItemInfoTbl, &record_container))
	{
		Global::logger->debug("[save_item_todb] make save item sql famild <itemid=%u>", GetItemId());
		return false;
	}

	MODI_GameServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
	return true;
}


/** 
 * @brief 更新到服务器
 * 
 */
void MODI_GameItemInfo::UpdateToDB()
{
	MODI_Record record_update;
	std::ostringstream where;
	where << "itemid=" << GetItemId();
	record_update.Put("bag_type", GetBagType());
	record_update.Put("bag_pos", GetBagPos());
	record_update.Put("where", where.str());

	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeUpdateSql(result, GlobalDB::m_pItemInfoTbl, &record_update))
	{
		Global::logger->fatal("[update_item] update item pos failed <itemid=%llu>", GetItemId());
		return;
	}

	MODI_GameServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 从数据库中删除
 * 
 */
void MODI_GameItemInfo::DelFromDB()
{
	std::ostringstream os;
	os<< "update " << GlobalDB::m_pItemInfoTbl->GetName() << " set is_valid=0 where itemid=" << GetItemId() << ";";
	MODI_GameServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());	
}


/** 
 * @brief 获取过期时间
 * 
 * @return 过期时间
 *
 */
QWORD MODI_GameItemInfo::GetToExpireTime() const
{
	return m_stItemInfo.m_qdExpireTime;
}


/** 
 * @brief 是否过期
 * 
 * @return 过期true
 *
 */
bool MODI_GameItemInfo::IsToExpireTime(const MODI_RTime & cur_rtime)
{
	if( IsForeverTime() )
		return false;

	if(GetToExpireTime() < (QWORD)cur_rtime.GetSec())
	{
		return true;
	}
	
	return false;
}


/** 
 * @brief 是否是永久的道具
 * 
 * @return 永久true
 *
 */
bool MODI_GameItemInfo::IsForeverTime() const
{
	if(m_stItemInfo.m_qdExpireTime == enForever)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 过期时间
 * 
 * @return 过期时间
 *
 */
DWORD MODI_GameItemInfo::GetRemainTime() const
{
	if( IsForeverTime() )
		return enForever;

	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	return ptm->DiffTimeT(  ptm->GetSavedANSITime() , GetToExpireTime() );
}


/** 
 * @brief 更新
 * 
 * @param cur_rtime 更新时间
 * @param cur_ttime 更新时间结构体
 *
 */
void MODI_GameItemInfo::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	if( IsForeverTime() == false && IsToExpireTime(cur_rtime))
	{
		MODI_TriggerExpireItem  script( GetConfigId());
		MODI_ItemExpireManager::GetInstance().Execute(GetOwner(),script);
		DelMeFromBag(enDelReason_Exprie);
	}
}


/** 
 * @brief 通知客户端增加一个道具
 * 
 */
void MODI_GameItemInfo::NotifyClientAddItem()
{
	MODI_GS2C_AddItem_Cmd send_cmd;
	send_cmd.m_enAddReason = (enAddItemReason)GetCreateReason();
	send_cmd.m_stItemInfo.m_qdItemId = GetItemId();
	send_cmd.m_stItemInfo.m_dwConfigId = GetConfigId();
	send_cmd.m_stItemInfo.m_byBagType = GetBagType();
	send_cmd.m_stItemInfo.m_wdBagPos = GetBagPos();
	send_cmd.m_stItemInfo.m_dwRemainTime = GetRemainTime();

	MODI_ClientAvatar * p_client = GetOwner();
	if(!p_client)
	{
		Global::logger->error("[notify_add_item] not have client <%llu>", GetItemId());
		MODI_ASSERT(0);
		return;
	}
	p_client->SendPackage(&send_cmd, sizeof(send_cmd));
}


/** 
 * @brief 通知客户端删除一个道具
 * 
 */
void MODI_GameItemInfo::NotifyClientDelItem(const enDelItemReason & del_relase)
{
	MODI_GS2C_DelItem_Cmd send_cmd;
	send_cmd.m_enDelReason = del_relase;
	send_cmd.m_qdItemId = GetItemId();

	MODI_ClientAvatar * p_client = GetOwner();
	if(!p_client)
	{
		Global::logger->error("[notify_add_item] not have client <%llu>", GetItemId());
		MODI_ASSERT(0);
		return;
	}
	p_client->SendPackage(&send_cmd, sizeof(send_cmd));
}

