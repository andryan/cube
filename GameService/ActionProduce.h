/**
 * @file   ActionProduce.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:19:14 2011
 * 
 * @brief  动作生成
 * 
 * 
 */

#ifndef _MD_ACTIONPRODUCE_H
#define _MD_ACTIONPRODUCE_H

#include "Global.h"
#include "Action.h"

/**
 * @brief 动作生成
 * 
 */
class MODI_ActionProduce
{
 public:
	template< typename class_type >
	MODI_Action * ActionProduce(class_type classname)
	{
	   	MODI_Action * ret_ptr = NULL;
		ret_ptr = new class_type();
		return ret_ptr;
	}

	static MODI_ActionProduce &  GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_ActionProduce;
		}
		return * m_pInstance;
	}
	
 private:
	static MODI_ActionProduce * m_pInstance;
};

#endif
