#include "YunYingKeyExchange.h"
#include "Base/SplitString.h"
#include "Base/HelpFuns.h"
#include "ItemOperator.h"
#include "stdlib.h"
#include "GameServerAPP.h"		

static	void	GetKeyDate( char *out,const char *szkey)
{
	int i =0;
	for( i=0;  i< CDKEY_DATE_LEN; ++i)
	{
		out[i] = szkey[i+CDKEY_DATE_OFF];
	}
	out[i] = '\0';
}

static	void	GetKeyClass( char *out,const char * szKey)
{
	int i=0;
	for( i=0;  i< CDKEY_CLASS_LEN; ++i)
	{

		out[i] = szKey[i+CDKEY_CLASS_OFF];
	}
	out[i] ='\0';
}

MODI_YunYingKeyExchange::MODI_YunYingKeyExchange()
{

}

MODI_YunYingKeyExchange::~MODI_YunYingKeyExchange()
{

}


enYunYingKeyExchangeResult 	MODI_YunYingKeyExchange::Exchange( MODI_ClientAvatar * pClient , const char * szKey , const char * szContent , 
		unsigned int nContentLen, MODI_GoodsPackageArray & goods )
{
	if( !pClient )
	{
		MODI_ASSERT(0);
		return kYYKeyExchange_UnKnowError;
	}

	if( !PublicFun::StrConvToGoodsPackage( goods , szContent , nContentLen ) )
		return kYYKeyExchange_UnKnowError;

	if( pClient->GetPlayerBag().GetEmptyPosCount() < goods.m_count )
		return kYYKeyExchange_BagFull;

	//根据CDKEY制作变量
	string keyvalue="forever_cdkey_";

	char tmp_date[CDKEY_DATE_LEN+1];
	char tmp_class[CDKEY_CLASS_LEN+1];

	GetKeyDate( tmp_date,szKey);
	GetKeyClass( tmp_class,szKey);

	keyvalue+=tmp_date;
	keyvalue+=tmp_class;

	
	int inttmpi = atoi( tmp_class);

	if( inttmpi != NO_LIMIT_KEY)
	{
		//此处判断是不是已经使用了相同类型的cdkey
		if(pClient->m_stGameUserVarMgr.IsHaveVar(keyvalue.c_str())   )
		{
			MODI_GameUserVar  *tmp;

			if( (tmp=pClient->m_stGameUserVarMgr.GetVar(keyvalue.c_str()))!= NULL)
			{
				WORD tmpw = tmp->GetValue();
				if( tmpw > 0)
				{
					char * sendchat="您已经领取了同类型的CD-KEY，无法重复领取";
					pClient->SendSystemChat(sendchat,strlen(sendchat));
					//已经使用了本类激活码
					return kYYKeyExchange_ClassCof;

				}	
				else
				{
					tmpw++;
					
					tmp->SetValue( tmpw);
				}
			}

		}
		else
		{
			//新建一个变量并且插入数据库
			MODI_GameUserVar  *newval = new MODI_GameUserVar( keyvalue.c_str());
			std::string var_attribute = "type=normal";
			newval->Init(pClient, var_attribute.c_str());
			newval->SetValue( 1); 			

			if( !pClient->m_stGameUserVarMgr.AddVar( newval))
			{

				//插入失败的处理
			}
			newval->SaveToDB();
		}
	}

	// 增加物品
	for( BYTE n = 0; n < goods.m_count; n++ )
	{
// 		DWORD nLimitTime = enOneDay;
// 		if( goods.m_array[n].time == kShopGoodT_7Day )
// 		{
// 			nLimitTime = enOneWeak;
// 		}
// 		else if( goods.m_array[n].time == kShopGoodT_30Day )
// 		{
// 			nLimitTime = enOneMonth;
// 		}
// 		else if( goods.m_array[n].time == kShopGoodT_Forver )
// 		{
// 			nLimitTime = enForever;
// 		}

		if(MODI_ItemOperator::CheckItemSexReq(pClient, goods.m_array[n].nGoodID) != kUseItem_Ok)
		{
			Global::logger->debug("[cd_key] sex req failed ");
			continue;
		}
		
// 		int iRes = MODI_ItemOperator::CreateItemAddToBag( &pClient->GetPlayerBag() , goods.m_array[n].nGoodID , goods.m_array[n].nCount , nLimitTime );
// 		if( iRes != MODI_ItemOperator::enOK )
// 		{
// 			Global::logger->error("[cdkey] exchange by cdkey. but add item faild <name=%s,res=%d,goodconfig=%u,count=%u,time=%d>" , 
// 								  pClient->GetRoleName(),
// 					iRes , 
// 					goods.m_array[n].nGoodID , 
// 					goods.m_array[n].nCount , 
// 					goods.m_array[n].time );
// 		}
		MODI_GameServerAPP * pApp = (MODI_GameServerAPP *)MODI_IAppFrame::GetInstancePtr();
		if(!pApp)
		{
			MODI_ASSERT(0);
			return kYYKeyExchange_UnKnowError;
		}
	
		MODI_Bag * p_player_bag = &(pClient->GetPlayerBag());
		if(!p_player_bag)
		{
			MODI_ASSERT(0);
			return kYYKeyExchange_UnKnowError;
		}

		MODI_CreateItemInfo create_info;
		create_info.m_dwConfigId = goods.m_array[n].nGoodID;
		create_info.m_byLimit = goods.m_array[n].time;
		create_info.m_enCreateReason = enAddReason_Cdkey;
		create_info.m_byServerId = pApp->GetServerID();

		for(WORD deal_count=0; deal_count<goods.m_array[n].nCount; deal_count++)
		{
			QWORD new_item_id = 0;
			if(!p_player_bag->AddItemToBag(create_info, new_item_id))
			{
				Global::logger->debug("[cdkey] change goods failed <deal_count=%u,name=%s>", deal_count,pClient->GetRoleName());
				return kYYKeyExchange_UnKnowError;
			}
		}
	
		Global::logger->info("[cdkey] exchange cdkey success <name=%s,config=%u,count=%u>", pClient->GetRoleName(), goods.m_array[n].nGoodID,
							 goods.m_array[n].nCount);
	}
 
	return kYYKeyExchange_Successful;  
}


