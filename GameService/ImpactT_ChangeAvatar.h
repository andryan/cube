/** 
 * @file ImpactT_ChangeAvatar.h
 * @brief 变形效果
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MODI_IMPACT_MODIFY_AVATAR_H_
#define MODI_IMPACT_MODIFY_AVATAR_H_

#include "ImpactTemplate.h"

/** 
 * @brief 按系数修改经验
 */
class 	MODI_ImpactT_ChangeAvatar : public MODI_ImpactTemplate
{
		enum
		{
			kArg_ModfiyScore = 0,
			kArg_ModfiyRenqi = 1,
			kArg_ChangeID = 2,
		};

	public:
		MODI_ImpactT_ChangeAvatar();
		virtual ~MODI_ImpactT_ChangeAvatar();

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData );

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetChangeID( MODI_ImpactObj & impactObj , WORD nChangeID );
		WORD 	GetChangeID( const MODI_ImpactObj & impactObj ) const;

		void 	SetModfiyScore( MODI_ImpactObj & impactObj , long lValue );
		long 	GetModfiyScore( const MODI_ImpactObj & impactObj ) const;

		void 	SetModfiyRenqi( MODI_ImpactObj & impactObj , long lValue );
		long 	GetModfiyRenqi( const MODI_ImpactObj & impactObj ) const;
};

#endif
