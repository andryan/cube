#include "ImpactObj.h"
#include "FreeList.h"

// 测试用
static gems::FreeList< MODI_ImpactObj,
	gems::PlacementNewLinkedList<MODI_ImpactObj>,
	gems::ConstantGrowthPolicy<1000,5000,1000000> > 	gs_ImpactObjPoolInst;


MODI_ImpactObj::MODI_ImpactObj():MODI_BaseObject(INVAILD_GUID),
	m_nConfigID(INVAILD_CONFIGID),
	m_nClientID(INVAILD_CONFIGID),
	m_nContinuance(0),
	m_nContinuanceElapsed(0),
	m_nIntervalElapsed(0),
	m_pUserdata(0),
	m_bOutGameFadeout(false)
{

}

MODI_ImpactObj::MODI_ImpactObj(MODI_GUID id):MODI_BaseObject(id),
	m_nConfigID(INVAILD_CONFIGID),
	m_nContinuance(0),
	m_nContinuanceElapsed(0),
	m_nIntervalElapsed(0),
	m_pUserdata(0),
	m_bOutGameFadeout(false)
{

}

MODI_ImpactObj::~MODI_ImpactObj()
{

}

void MODI_ImpactObj::SetGUID( const MODI_GUID & guid )
{
	this->m_GUID = guid;
}

void MODI_ImpactObj::SetArg( DWORD nIdx , const MODI_ImpactArg & arg )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		m_Param[nIdx] = arg;
	}
}

void MODI_ImpactObj::SetArgInt( DWORD nIdx , int nSet )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		m_Param[nIdx].SetInt( nSet );
	}

}

void MODI_ImpactObj::SetArgDWORD( DWORD nIdx , DWORD nSet )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		m_Param[nIdx].SetDWORD( nSet );
	}
}

void MODI_ImpactObj::SetArgFloat( DWORD nIdx , float fSet )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		m_Param[nIdx].SetFloat( fSet );
	}
}

void MODI_ImpactObj::SetArgLong( DWORD nIdx , long lSet )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		m_Param[nIdx].SetLong( lSet );
	}
}

const MODI_ImpactArg * 	MODI_ImpactObj::GetArg( DWORD nIdx )
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		return &m_Param[nIdx];
	}
	return 0;
}

bool MODI_ImpactObj::GetArgInt( DWORD nIdx , int & nOut ) const
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		nOut = m_Param[nIdx].GetInt();
		return true;
	}
	return false;
}

bool MODI_ImpactObj::GetArgDWORD( DWORD nIdx , DWORD & nOut ) const
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		nOut = m_Param[nIdx].GetDWORD();
		return true;
	}
	return false;
}

bool MODI_ImpactObj::GetArgFloat( DWORD nIdx , float & fOut ) const
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		fOut = m_Param[nIdx].GetFloat();
		return true;
	}
	return false;
}

bool MODI_ImpactObj::GetArgLong( DWORD nIdx , long & lOut ) const
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		lOut = m_Param[nIdx].GetLong();
		return true;
	}
	return false;
}

bool MODI_ImpactObj::IsValidArgIndex( DWORD nIdx ) const
{
	if( nIdx < IMPACT_MAX_PARAMS )
	{
		return true;
	}
	return false;
}

MODI_ImpactObj * 	MODI_ImpactObjPool::New()
{
	return gs_ImpactObjPoolInst.Allocate();
}

void 				MODI_ImpactObjPool::Delete( MODI_ImpactObj * p )
{
	gs_ImpactObjPoolInst.Free( p );
}
