/** 
 * @file ImpactTemplate.h
 * @brief 效果模板接口
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-07
 */

#ifndef IMPACT_TEMPLATE_H_
#define IMPACT_TEMPLATE_H_

#include "BinFileMgr.h"


class MODI_ImpactObj;
class MODI_ClientAvatar;

class 	MODI_ImpactTemplate
{
	public:

	MODI_ImpactTemplate();
	virtual ~MODI_ImpactTemplate() = 0;

	public:


	virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData ) = 0;
	virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj ) = 0;
	virtual int Update( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj , unsigned long nTime );
	virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj ) = 0;
	virtual int CleanUp( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );
	virtual int SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );
};




class 	MODI_ImpactT_Null : public MODI_ImpactTemplate
{
	public:
	MODI_ImpactT_Null(){}
	virtual ~MODI_ImpactT_Null(){}

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData ){return true;}

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData ){return kImpactH_Successful;}

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj ){return kImpactH_Successful;}

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj ){return kImpactH_Successful;}

		virtual int SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj ){return kImpactH_Successful;}

		void 	SetModfiyScore( MODI_ImpactObj & impactObj , long lValue ){}
		long 	GetModfiyScore( const MODI_ImpactObj & impactObj ) const{return 0;}

		void 	SetModfiyRenqi( MODI_ImpactObj & impactObj , long lValue ){}
		long 	GetModfiyRenqi( const MODI_ImpactObj & impactObj ) const{return 0;}
	   
};


#endif
