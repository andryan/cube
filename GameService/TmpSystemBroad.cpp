#include "TmpSystemBroad.h"
#include "protocol/c2gs_chatcmd.h"
#include "protocol/c2gs_rolecmd.h"
#include "GameChannel.h"
#include "Global.h"

MODI_TempSystemBroad  *MODI_TempSystemBroad::m_pInstance = NULL;


MODI_TempSystemBroad * MODI_TempSystemBroad::GetInstance()
{
	if( !m_pInstance)
	{
		m_pInstance= new MODI_TempSystemBroad();
	}
	return m_pInstance;
}

void	MODI_TempSystemBroad::Update()
{
	if( m_lList.empty() )
	{
		return;
	}

	if( Global::m_stRTime.GetSec()- m_tLast >MAX_SECS)
	{
		Broadcast( m_lList.front());
		m_tLast = Global::m_stRTime.GetSec();
		m_lList.pop_front();
	}

}



void	MODI_TempSystemBroad::Push(std::string & str)
{

	if( Global::m_stRTime.GetSec() - m_tLast > MAX_SECS)
	{
		if( m_lList.empty())
		{
			m_tLast = Global::m_stRTime.GetSec();
			Broadcast(str);
			return ;
		}
	}

	m_lList.push_back(str);

}

void	MODI_TempSystemBroad::Broadcast(std::string &str)
{

	char packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_GS2C_Notify_SystemBroast * notify = (MODI_GS2C_Notify_SystemBroast * )packet;
	AutoConstruct(notify);
	strncpy( notify->m_pContents,str.c_str(),str.size());
	notify->m_nSize = str.size();
	size_t  size= sizeof( MODI_GS2C_Notify_SystemBroast)+str.size();

	MODI_GameChannel::GetInstancePtr()->Broadcast(notify,size); 

}



