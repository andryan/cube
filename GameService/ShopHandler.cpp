#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"
#include "GameShop.h"
#include "GameTick.h"
#include "GameHelpFun.h"
#include "Base/GameConstant.h"
#include "Base/HelpFuns.h"

int MODI_PackageHandler_c2gs::EnterShop_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_EnterShop , cmd_size , 0 , 0, pClient );
	const MODI_C2GS_Request_EnterShop * pReq = ( const MODI_C2GS_Request_EnterShop *)(pt_null_cmd);
	DISABLE_UNUSED_WARNING( pReq );

	// 是否已经在商城中
	if( pClient->IsInShop() )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> is alreay in shop , but send enter shop req." ,
				SHOP_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_OK;
	}

	MODI_GS2C_Notify_EnterShopResult res;
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	if( pClient->GetRareShopTimer()( pTick->GetTimer() ) )
	{
		// 随机时间是否到
		if( pClient->IsVip())
		{
			res.m_bRareShop = RandomBingo( MODI_GameConstant::get_EnterRareShopPro()*3 );
		}
		else
			res.m_bRareShop = RandomBingo( MODI_GameConstant::get_EnterRareShopPro() );
	}

	MODI_GameShop * pShop =  0;
	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	if( res.m_bRareShop )
	{
		pShop = (MODI_GameShop *)(&pChannel->GetRareShop());
	}
	else 
	{
		pShop = (MODI_GameShop *)(&pChannel->GetNormalShop());
	}

	if( !pShop )
	{
		Global::logger->error("[%s] client<charid=%u,name=%s> req enter shop , but can't get shop." , 
				SHOP_MODULE ,
				pClient->GetCharID() ,
				pClient->GetRoleName() );
		MODI_ASSERT(0);
		return enPHandler_Warning;
	}

	pShop->OnEnter( pClient );

	// 发送进入结果
	res.m_result  = kIO_Shop_Successful; 
	pClient->SendPackage( &res , sizeof(res) );

	return enPHandler_OK;
}



/** 
 * @brief 购买或者赠送物品
 * 
 * @param pObj 发起者
 * @param pt_null_cmd 命令 
 * @param cmd_size 命令大小
 * 
 * @return
 *
 */
int MODI_PackageHandler_c2gs::ShopTransaction_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	
	size_t nMinSize = sizeof(MODI_C2GS_Request_Transaction);
	if( cmd_size < nMinSize )
	{
		return enPHandler_Kick;
	}
	
	MODI_C2GS_Request_Transaction * pReq = (MODI_C2GS_Request_Transaction *)(pt_null_cmd);
	if( cmd_size != pReq->m_nContentSize * sizeof(char) + nMinSize )
	{
		return enPHandler_Kick;
	}
	

	MODI_GameShop * pShop = pClient->GetShop();
	
	MODI_BinFileMgr  *pin = MODI_BinFileMgr::GetInstancePtr();
	/// 商场状态不对
	if( !pShop && pReq->m_actionType != kTransaction_BuyAndUse )
	{
		///  模拟商城
		if( !pin )
			return enPHandler_Warning;
		BYTE i=0;
		for( i=0; i< pReq->m_nCount; i++)
		{
			if( !pin->IsValidConfigInTables(pReq->m_pGoods[i].m_pConfigID,enBinFT_Quickbuy))
			{
				return enPHandler_Warning;
			}
		}
	}

	/// 内容是否正确
	if( !pReq->m_nCount || pReq->m_nCount > ONCE_TRANSACTION_MAX_GOODSCOUNT || pReq->m_nContentSize > MAIL_CONTENTS_MAX_LEN  + 1 )
	{
		return enPHandler_Warning;
	}

	/// 返回结果
	MODI_GS2C_Notify_TransactionResult notify;
	notify.m_actionType = pReq->m_actionType;

	if(pReq->m_actionType == kTransaction_ToGive)
	{
		safe_strncpy( notify.m_szTargetName , pReq->m_szTargetName , sizeof(notify.m_szTargetName));
		unsigned int nContentLen = 0;
		if( !MODI_GameHelpFun::StrSafeLength( pReq->m_pContent , pReq->m_nContentSize ,  nContentLen ) )
		{
			return enPHandler_Warning;
		}
	}

	enShopTransactionResult res = kTransaction_UnknowError;

	/// 直接购买
	if( pReq->m_actionType == kTransaction_Buy )
	{
		//for(BYTE n=0; n<buy_count; n++)
		//		{
		//			const GameTable::MODI_Shop * pElement = shopBin.Get(p_goods_info[n].m_pConfigID);
		if( !pShop)
		{
		
			MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
			pShop = (MODI_GameShop *)(&channel->GetNormalShop());
		}	
		res = pShop->BuyItem(pClient, pReq->m_pGoods, pReq->m_nCount, kTransaction_Buy);
		if(Global::g_byGameShop > 0 && res != kTransaction_Successful)
		{
			return enPHandler_OK;
		}

		notify.m_result = res;
		pClient->SendPackage( &notify, sizeof(notify) );
	}

 	/// 购买并使用
	else if( pReq->m_actionType == kTransaction_BuyAndUse )
	{
		if( !pShop )
		{
			MODI_GameChannel * channel = MODI_GameChannel::GetInstancePtr();
			pShop = (MODI_GameShop *)(&channel->GetNormalShop());
		}
		res = pShop->BuyItem(pClient, pReq->m_pGoods, pReq->m_nCount, kTransaction_BuyAndUse);
		if(Global::g_byGameShop > 0 && res != kTransaction_Successful)
		{
			return enPHandler_OK;
		}
		
		notify.m_result = res;
		pClient->SendPackage( &notify, sizeof(notify));
	}

	
	/// 赠送
	else if( pReq->m_actionType == kTransaction_ToGive)
	{
		/// 赠送人
		res = pShop->SendGiveItemMail(pClient, pReq, cmd_size);
		if(Global::g_byGameShop > 0 && res != kTransaction_Successful)
		{
			return enPHandler_OK;
		}
		
		if(res != kTransaction_Successful)
		{
			notify.m_result = res;
			pClient->SendPackage( &notify, sizeof(notify));
		}
	}
	else
	{
		MODI_ASSERT(0);
	}

	return enPHandler_OK;
}


int MODI_PackageHandler_c2gs::LeaveShop_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_LeaveShop , cmd_size , 0 , 0, pClient );
	const  MODI_C2GS_Request_LeaveShop * pReq = ( const  MODI_C2GS_Request_LeaveShop * )(pt_null_cmd);
	DISABLE_UNUSED_WARNING( pReq );

	MODI_GameShop * pShop  = pClient->GetShop();
	if( !pShop )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> req leave shop.but not in shop.",
				SHOP_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_OK;
	}

	MODI_GS2C_Notify_LeaveShopResult res;
	pShop->OnLeave( pClient );

	res.m_result  = kIO_Shop_Successful; 
	pClient->SendPackage( &res , sizeof(res) );
	return enPHandler_OK;
}


