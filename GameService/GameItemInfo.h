/**
 * @file   ItemInfo.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 15:49:37 2011
 * 
 * @brief  道具信息
 * 
 * 
 */

#ifndef _MD_GAMEITEMINFO_H
#define _MD_GAMEITEMINFO_H

#include "Global.h"
#include "Buffer.h"
#include "gamestructdef.h"

class MODI_Bag;
class MODI_BagPosInfo;
class MODI_ClientAvatar;


/**
 * @brief 道具信息
 * 
 */
class MODI_GameItemInfo
{
 public:
	MODI_GameItemInfo(){}

	/** 
	 * @brief 初始化
	 * 
	 */
	bool Init(const MODI_CreateItemInfo & create_item_info);


	/** 
	 * @brief 初始化信息
	 * 
	 */
	bool Init(MODI_ClientAvatar * p_client, const MODI_DBItemInfo * p_item_info);

	/** 
	 * @brief 获取包裹
	 * 
	 */
	MODI_Bag * GetOwnBag()
  	{
		return m_pOwnBag;
	}


	/** 
	 * @brief 获取角色
	 * 
	 */
	MODI_ClientAvatar * GetOwner()
	{
		return m_pOwner;
	}

	/** 
	 * @brief 获取该道具的类型
	 * 
	 */
	const MODI_ItemType & GetItemType() const 
	{
		return m_enItemType;
	}


	/** 
	 * @brief 获取道具id
	 * 
	 */
	const QWORD & GetItemId()
	{
		return m_stItemInfo.m_qdItemId;
	}


	/** 
	 * @brief获取创建的理由
	 * 
	 * 
	 * @return 
	 */
	const BYTE & GetCreateReason()
	{
		return m_stItemInfo.m_byCreateReason;
	}

	/** 
	 * @brief 获取服务器id
	 * 
	 * 
	 */
	const BYTE & GetServerId()
	{
		return m_stItemInfo.m_byServerId;
	}
	

	/** 
	 * @brief 获取道具的id
	 * 
	 */
	const DWORD & GetConfigId()
	{
		return m_stItemInfo.m_dwConfigId;
	}

	/** 
	 * @brief 获取包裹类型
	 * 
	 */
	const BYTE & GetBagType()
	{
		return m_stItemInfo.m_byBagType;
	}


	/** 
	 * @brief 获取包裹位置
	 * 
	 */
	const WORD & GetBagPos()
	{
		return m_stItemInfo.m_wdBagPos;
	}

	/** 
	 * @brief 设置包裹
	 * 
	 * @param p_bag 道具存放的bag
	 *
	 */
	void SetBag(MODI_Bag * p_bag)
	{
		m_pOwnBag = p_bag;
	}

	/** 
	 * @brief 设置物品主
	 * 
	 * @param p_client 谁的物品
	 *
	 */
	void SetClient(MODI_ClientAvatar * p_client)
	{
		m_pOwner = p_client;
	}
	

	/** 
	 * @brief 设置包裹类型
	 * 
	 */
	void SetBagType(const BYTE bag_type)
	{
		m_stItemInfo.m_byBagType = bag_type;
	}


	/** 
	 * @brief 设置包裹位置
	 * 
	 */
	void SetBagPos(const WORD bag_pos)
	{
		m_stItemInfo.m_wdBagPos = bag_pos;
	}

	
	/** 
	 * @brief 从数据库中和列表中删除物品,并通知客户端
	 * 
	 */
	bool DelMeFromBag(const enDelItemReason & del_reason);


	/** 
	 * @brief 增加到列表和数据库中，并通知客户端
	 * 
	 */
	bool AddMeToBag(MODI_Bag * p_bag);


	/** 
	 * @brief 增加到列表，并通知客户端
	 *
	 * 不保存数据库，返回sql语句
	 *
	 */
	bool AddMeToBag(MODI_Bag * p_bag, MODI_ByteBuffer & save_sql);

	
	/** 
	 * @brief 设置过期时间
	 * 
	 * @param time 过期时间
	 *
	 */
	void SetToExpireTime( const QWORD & ex_time );


	/** 
	 * @brief 获取过期时间
	 * 
	 * @return 过期时间
	 *
	 */
	QWORD GetToExpireTime() const;


	/** 
	 * @brief 是否过期
	 * 
	 * @return 过期true
	 *
	 */
	bool IsToExpireTime(const MODI_RTime & cur_rtime);


	/** 
	 * @brief 是否是永久的道具
	 * 
	 * @return 永久true
	 *
	 */
	bool IsForeverTime() const;


	/** 
	 * @brief 过期时间
	 * 
	 * @return 过期时间
	 *
	 */
	DWORD GetRemainTime() const;


	/** 
	 * @brief 更新
	 * 
	 * @param cur_rtime 更新时间
	 * @param cur_ttime 更新时间结构体
	 *
	 */
	void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);


	/** 
	 * @brief 生成保存数据库
	 * 
	 */
	bool MakeSaveSql(MODI_ByteBuffer & result);

	/** 
	 * @brief 保存到数据库
	 * 
	 */
	bool SaveToDB();


	/** 
	 * @brief 更新到数据库
	 * 
	 */
	void UpdateToDB();


	/** 
	 * @brief 从数据库中删除
	 * 
	 */
	void DelFromDB();


	/** 
	 * @brief 通知增加一个道具
	 * 
	 */
	void NotifyClientAddItem();


	/** 
	 * @brief 通知客户端删除一个道具
	 * 
	 */
	void NotifyClientDelItem(const enDelItemReason & del_relase);
	

	/// 物品基本信息
	MODI_DBItemInfo m_stItemInfo;
	
 private:
	/// 属于哪个pos
	MODI_BagPosInfo * m_pOwnPos;

	/// 属于谁
	MODI_ClientAvatar * m_pOwner;
	
	/// 属于哪个包裹
	MODI_Bag * m_pOwnBag;
	
	/// 物品类型
	MODI_ItemType m_enItemType;
};

#endif
