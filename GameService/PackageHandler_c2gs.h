/** 
 * @file PackageHandler_c2gs.h
 * @brief GAMESERVER 对客户端数据包的派发处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-07
 */
#ifndef PACKAGE_HANDLER_H_C2GS_
#define PACKAGE_HANDLER_H_C2GS_


#include "IPackageHandler.h"
#include "SingleObject.h"

class MODI_ClientAvatar;

/**
 * @brief 游戏逻辑消息的派发处理
 *
 */
class MODI_PackageHandler_c2gs : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_c2gs>
{

public:

    MODI_PackageHandler_c2gs();
	virtual ~MODI_PackageHandler_c2gs();


public:


	/// 音乐头的处理
	static int MusicHeader_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	/// 客户端请求切换频道
	static int ChangeGameChannel_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	/// 客户端选定需要切换的频道
	static int ReqChangeGameChannel_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    /*
     *      聊天的相关处理
     */
//    ///	私聊的处理，通过ID查找
//    static int PrivateChatByID_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
//            const unsigned int cmd_size);
//
//    ///	私聊的处理，通过名字查找
//	static  int PrivateChatByName_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
//            const unsigned int cmd_size);
//
    ///	范围聊天的处理
	static  int RangeChat_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	/// gm 命令处理
	static  int GMCommand_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);
    /*
     *     大厅中操作的相关处理
     */

    ///    创建房间的处理
    static int CreateRoom_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    请求进入房间的处理
    static int JoinRoom_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	/// 	请求随机进入房间的处理
    static int RandomJoinRoom_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    请求离开房间的处理
    static int LeaveRoom_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	///  	邀请某个玩家
    static int InvitePlay_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	///  	对邀请请求的回复
    static int InvitePlayRes_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    /*
     *     房间中相关操作的处理
     */

    ///    准备请求
    static int Ready_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    取消准备
    static int UnReady_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    房主开始游戏请求
    static int Start_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    房主踢人请求
	static  int KickPlayer_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    房主开位置请求
    static int OnPostion_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///    房主关位置请求
    static int OffPostion_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

    ///   房主请求更换音乐
    static int ChangeMusic_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);
    ///   房主请求更换场景
    static int ChangeMap_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);
    static int ChangeGameMode_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);
    ///    交换位置的请求
    static int ChangePostion_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	///    房主更改房间信息
	static int ChangeRoomInfo_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd,
			const unsigned int cmd_size);
	/*
	 * 游戏中的相关操作
	 */

	///	客户端通知服务器前半部分资源加载完毕
    static int FirstHalfReady_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);
    ///    客户端通知服务器后半部分资源加载完毕
    static int IamReady_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	///     客户端请求改变收听的对象
	static int ChangeObSinger(void * pObj,const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	///     客户端告知服务器，一句歌词的得分
	static int SentenceScore_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	///	客户端告知服务器，一个按键的得分
	static int OneKeyScore_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 	客户端告知服务器，开始某个效果
	static	int GameEffectBegin( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 	客户端告知服务器，某个效果结束
	static	int GameEffectOver( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 	客户端同步G槽情况
	static	int GameSyncGPower( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 	客户端告知服务器听歌结束
	static	int ListernOver( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	//  	客户端投票
	static	int VoteTo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );
	///	修改键盘模式的参数
    static int ModifyKeyModeParam_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	static	int UseItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int PlayExpression_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static  int MicHasData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	// 请求mdm文件
	static  int ReqMdmFile_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );


	/*
			Avatar 相关操作
	 */

	/// 	客户端请求换装
	static	int ChangeAvatar( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	///  	修改角色信息
	static int ModfiyRoleinfo_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size);

	static int DirectionToZoneServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int Movement_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int RequestMove_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
							 const unsigned int cmd_size );
	
	static int ReadHelp_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	// shop
	static int EnterShop_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int ShopTransaction_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int LeaveShop_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int YYExchangeKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int RecordMusic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int TransferMaster_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int PointError_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int LogOut_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int ChangeTeam_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int LogoutFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int LoginFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 创建家族
	static int CreateFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 某人对家族的操作
	static int FamilyOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 对申请成员的操作
	static int RequestMemberOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 对成员职位更改
	static int FModifyPosition_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 家族名字检查
	static int FamilyNameCheck_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 修改宣言
	static int ModiFamilyXuanyan_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	/// 修改公告
	static int ModiFamilyPublic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int EnterZone_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int EnterISpeak_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int  RequestActivities_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int  Signup_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	static int  AddItemBox_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
								   const unsigned int cmd_size );
    static int  OpenItemBox_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
									const unsigned int cmd_size );

	static int  YYChatState_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
protected:

    virtual bool FillPackageHandlerTable();
};

#endif
