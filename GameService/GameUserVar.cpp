/**
 * @file   GameUserVar.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 13 16:06:10 2011
 * 
 * @brief  clientavatar用户变量
 * 
 * 
 */

#include "GameUserVar.h"
#include "GameServerAPP.h"
#include "ClientAvatar.h"


/** 
 * @brief clientavatar用户变量
 * 
 * @param sql 要执行的sql
 * @param sql_size sql的大小
 *
 */
void MODI_GameUserVar::ExecSqlToDB(const char * sql, const WORD sql_size)
{
	if(!sql || sql_size == 0)
		return;
	MODI_GameServerAPP::ExecSqlToDB(sql, sql_size);
}


