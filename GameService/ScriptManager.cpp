/**
 * @file   ScriptManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 12:03:42 2011
 * 
 * @brief  脚本管理
 * 
 * 
 */

#include "Global.h"
#include "XMLParser.h"
#include "ScriptManager.h"

const std::string MODI_DIRTask::m_strDir =  "./Script/TriggerTask/";
const std::string MODI_DIREnter::m_strDir = "./Script/TriggerEnter/";
const std::string MODI_DIRTime::m_strDir = "./Script/TriggerTime/";
const std::string MODI_DIRActivity::m_strDir = "./Script/system/TriggerActive/";
const std::string MODI_DIRChenghao::m_strDir = "./Script/system/TriggerChenghao/";
const std::string MODI_DIRBuyItem::m_strDir= "./Script/TriggerBuyItem/";
const std::string MODI_DIRUseItem::m_strDir= "./Script/TriggerUseItem/";
const std::string MODI_DIRImpactItem::m_strDir= "./Script/TriggerHaveItem/";
const std::string MODI_DIRExpireItem::m_strDir="./Script/TriggerExpireItem/";
const std::string MODI_DIRSingMusic::m_strDir = "./Script/system/TriggerSingMusic/";
const std::string MODI_DIRBeUsedItem::m_strDir = "./Script/system/TriggerBeUsedItem/";


