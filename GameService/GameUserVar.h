/**
 * @file   GameUserVar.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 13 15:59:42 2011
 * 
 * @brief  gameservice用户变量
 * 
 */


#ifndef _MD_GAMEUSERVAR_H
#define _MD_GAMEUSERVAR_H

class MODI_ClientAvatar;

#include "UserVar.h"
#include "GlobalDB.h"


/**
 * @brief client用户变量
 * 
 */
class MODI_GameUserVar: public MODI_UserVar<MODI_ClientAvatar>
{
 public:
	MODI_GameUserVar(const char * var_name, MODI_TableStruct * p_table = GlobalDB::m_pGameUserVarTbl):
		MODI_UserVar<MODI_ClientAvatar>(var_name,p_table)
	{
			
	}

		
	virtual ~MODI_GameUserVar(){}
	
	/** 
	 * @brief  执行数据库sql语句
	 * 
	 * @param sql 
	 * @param sql_size 
	 */
	virtual void ExecSqlToDB(const char * sql, const WORD sql_size);
	
};

typedef MODI_UserVarManager<MODI_GameUserVar> MODI_GameUserVarMgr;

#endif
