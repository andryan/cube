/**
 * @file GameLobby.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 游戏中的大厅对象
 *
 */

#ifndef GAME_LOBBY_H_
#define GAME_LOBBY_H_

#include "SingleObject.h"
#include "NormalGameRoom.h"
#include "SingleGameRoom.h"
#include "Timer.h"

class MODI_GameLobby : public  CSingleObject<MODI_GameLobby>
{
public:

    MODI_GameLobby();
    virtual ~MODI_GameLobby();

	WORD CreateRoom(MODI_ClientAvatar * pClient, defRoomType roomType,defMusicID nMusicID , 
					defMapID nMapID ,WORD scene_id, const char * szRoomName , const char * szPassword , bool bAllowSpectator );

    ///	广播整个大厅(只会广播在大厅中的玩家)
    bool Broadcast(const void * pt_null_cmd, int cmd_size);

	bool Broadcast_Except(const void * pt_null_cmd, int cmd_size , MODI_ClientAvatar * pExcept );

    ///    向某个客户端发送房间列表
    bool SendRoomList(MODI_ClientAvatar * pSession);

    ///    向某个客户端发送玩家列表
    bool SendPlayerList(MODI_ClientAvatar * pSession);

    ///    有房间创建时被调用
    bool OnCreateRoom( MODI_ClientAvatar * pClient , const MODI_GameRoom & newRoom);

    ///    有房间销毁时被调用
    bool OnDestroyRoom(defRoomID roomID);

	int CreateSyncPlaylistPackage( char * szBuf , int nBufSize );
public:

	BYTE GetRoomSize();
	const unsigned int GetLobbyPlayerNum() const
	{
		return m_mapClients.size();
	}

    bool Create();

    void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);


    ///    判断某个玩家是否在大厅中
    bool IsInLobby(MODI_GUID client_guid);

    ///    在大厅中查找一个玩家（不会查找在房间中的玩家）
    MODI_ClientAvatar * FindClientInLobby(MODI_GUID client_guid);

    /// 进入大厅
    bool EnterLobby(MODI_ClientAvatar * pClient);

    /// 离开大厅
    void LeaveLobby(MODI_GUID client_guid);

    ///    获得一个房间
    MODI_MultiGameRoom * GetMultiRoom(defRoomID id);

	///   	获得某个客户端的单人房间
	MODI_SingleGameRoom * GetSingleRoom( MODI_ClientAvatar * pClient )  { return &m_SingleRoom; }

	/// 获得一个等待的房间
	MODI_MultiGameRoom * GetWaittingRoom( ROOM_TYPE type , ROOM_SUB_TYPE subtype , 
			defMusicID musicid , defMapID mapid , bool bPwd = false );

	WORD IsCanEnterRoom( MODI_MultiGameRoom * room );

private:

    ///    备份房间信息
    void BackupRooms();

    ///    同步脏的房间信息
    void SyncDirtyRooms();

    ///    构建普通房间的更新信息
    void BuildNormalRoomUpdateInfo(BYTE id, void * p,defRoomUpdateMask  mask);

	///    构建房间基本信息的更新信息
	void BuildBaseRoomUpdateInfo(BYTE id , void * p,defRoomUpdateMask  mask);

	///    获得房间更新掩码
	defRoomUpdateMask GetRoomUpdateMask(BYTE id);

    ///    获得一个空闲的房间id
    defRoomID GetFreeRoom();

private:

    /// 500ms定时
    MODI_Timer m_SyncDirtyTimer;
    MODI_MultiGameRoom * m_Rooms[MAX_ROOM_COUNT]; //  房间对象
	MODI_MultiGameRoom * m_NormalRooms[MAX_ROOM_COUNT]; // 普通的房间对象

	MODI_SingleGameRoom  m_SingleRoom;

    MODI_RoomInfo m_RoomsBackUp[MAX_ROOM_COUNT]; //备份的房间信息
    MAP_CLIENTAVATARS m_mapClients; //  在大厅中的客户端
};


#endif
