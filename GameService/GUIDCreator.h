/*
* @file GUIDCreator.h
* @brief GUID生成器
* @author Tang Teng
* @version v0.1
* @date 2010-03-23
*/
#ifndef MODI_GUID_CREATOR_H_
#define MODI_GUID_CREATOR_H_

#include "gamestructdef.h"
#include "SingleObject.h"


class 	MODI_GUIDCreator : public CSingleObject<MODI_GUIDCreator>
{
	public:


		MODI_GUIDCreator( BYTE nWorldID , defServerChannelID nSvrID );

		/** 
		 * @brief  产生一个代表游戏玩家对象的GUID  
		 * 
		 * @param nCharID 游戏玩家的角色ID 
		 * 
		 * @return 返回guid	
		 */
		MODI_GUID 	CreatePlayerGUID( MODI_CHARID nCharID );

		/** 
		 * @brief 产生一个代表游戏道具（物品）的GUID
		 * 
		 * @return 返回guid	
		 */
		MODI_GUID 	CreateItemGUID();

		
		MODI_GUID 	CreateImpactGUID();


		BYTE 	GetWorldID() const { return m_nWorldID; }
		defServerChannelID  	GetServerID() const { return m_nChannelID; }


		void 		LoadLastItemGUIDFromDB();
		void 		OnLoadLastItemGUIDFromDB( void * p );

		void 		SaveLastItemSerialToDB( bool bForce =false );

	private:


		void 		SetLastItemGUID( DWORD nLast ) { m_nLastItemID = nLast; }

		BYTE 				m_nWorldID;
		defServerChannelID 	m_nChannelID;
		DWORD 				m_nLastItemID;
		BYTE 				m_nItemIDLayer;
		DWORD 				m_nLastSaveDBItemID;
		BYTE 				m_nImpactIDLayer;
		DWORD 				m_nLastImpactGUID;
};


#endif
