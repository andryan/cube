#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"
#include "YunYingKeyExchange.h"
#include "ZoneClient.h"
#include "GameHelpFun.h"
#include "Base/IAppFrame.h"
#include "Base/s2rdb_cmd.h"

	
int MODI_PackageHandler_c2gs::YYExchangeKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ExchangeYunYingKey , cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_ExchangeYunYingKey * pReq = (const MODI_C2GS_Request_ExchangeYunYingKey *)(pt_null_cmd);
	// 检查KEY有效性
	unsigned int nSafeLen = 0;
	if( !MODI_GameHelpFun::StrSafeLength(  pReq->m_szKey , sizeof(pReq->m_szKey) , nSafeLen ) )
		return enPHandler_Warning;
	if( nSafeLen > YUNYING_CDKEY_MAXSIZE )
		return enPHandler_Warning;

	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( !pZone )
	{
		MODI_GS2C_Notify_ExchangeYunYingKeyRes res;
		res.m_result = kYYKeyExchange_UnKnowError;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_Warning;
	}

	MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
	MODI_S2RDB_Request_ExchangeByYunyingKey msg;
	msg.m_charid = pClient->GetCharID();
	msg.m_serverid = pApp->GetServerID();
	memcpy( msg.m_szKey  , pReq->m_szKey , sizeof(pReq->m_szKey) );
	pZone->SendCmd( &msg , sizeof(msg) );
	
	return enPHandler_OK;
}
