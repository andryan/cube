/** 
 * @file MusicHeaderData.h
 * @brief 歌曲的文件头信息
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-02
 */

#ifndef MODI_MUSICHEADERDATA
#define MODI_MUSICHEADERDATA

class  MODI_MusicHeaderData
{
	char   m_szData[12]; // 12k
	char * m_pDataBuf;
	bool   m_bOwner;
	unsigned int m_nSize;
	bool   m_bHasData;

	public:

	MODI_MusicHeaderData();
	~MODI_MusicHeaderData();
	
	/** 
	 * @brief 设置音乐头数据
	 * 
	 * @param pData 数据内容
	 * @param nSize 数据大小
	 * 
	 * @return 	数据内容过大，则返回false
	 */
	bool 	SetData( const char * pData , unsigned int nSize );

	/** 
	 * @brief 重置音乐头数据
	 */
	void 	Reset();

	/** 
	 * @brief 是否有数据
	 * 
	 * @return 如果有数据，则返回TRUE	
	 */
	bool 	IsHasData() const { return m_bHasData; }

	/** 
	 * @brief 返回音乐头数据的大小
	 * 
	 * @return 	返回音乐头数据的大小，没有则为零
	 */
	unsigned int GetSize() const
	{
		if( IsHasData() )
			return m_nSize;
		return 0;
	}

	/** 
	 * @brief 获得音乐头数据
	 * 
	 * @return 	返回数据开始的地址
	 */
	const char * GetDataPtr() const
	{
		return m_pDataBuf;
	}
};

#endif
