/**
 * @file   SceneManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Dec 15 15:47:51 2011
 * 
 * @brief  场景管理
 * 
 * 
 */

#include "SceneManager.h"
#include "GameRoom.h"
#include "BinFileMgr.h"

MODI_SceneManager * MODI_SceneManager::m_pInstance = NULL;

MODI_SceneManager::MODI_SceneManager(){}
MODI_SceneManager::~MODI_SceneManager(){}

MODI_SceneManager & MODI_SceneManager::GetInstance()
{
	if(m_pInstance == NULL)
	{
		m_pInstance = new MODI_SceneManager;
	}
	return *m_pInstance;
}


void MODI_SceneManager::DelInstance()
{
	if(m_pInstance)
	{
		delete m_pInstance;
	}
	m_pInstance = NULL;
}


bool MODI_SceneManager::Init()
{
	defWaitRoomBinFile & waitroombin = MODI_BinFileMgr::GetInstancePtr()->GetWaitRoomBinFile();
	unsigned int size = waitroombin.GetSize();
	for( unsigned int i=1; i < size+1; i++)
	{
		const GameTable::MODI_WaitRoom * room = waitroombin.Get(i);
		if(!room)
		{
			continue;
		}

		std::string file_name = room->get_LoadFile();
		std::string load_file_name = "./Data/" + file_name;
		
		defSceneArray scene_array;
		for(unsigned int i=0; i<MAX_ROOM_COUNT; i++)
		{
			MODI_Scene * p_scene = new MODI_Scene();
			if(! p_scene->Init(load_file_name.c_str(), room->get_ConfigID()))
			{
				Global::logger->fatal("[scene_manger_init] init failed <%s>", room->get_LoadFile());
				return false;
			}
			p_scene->SetType(room->get_SceneType());
			scene_array.push_back(p_scene);
		}
		
		m_SceneMap[room->get_ConfigID()] = scene_array;
		
#ifdef _DEBUG
		Global::logger->debug("[load_scene] laod scene successful <id=%u,name=%s,file=%s>", room->get_ConfigID(),
							  room->get_SceneName(), room->get_LoadFile());
#endif
		
	}
	return true;
}

MODI_Scene * MODI_SceneManager::GetScene(MODI_GameRoom * p_room, const WORD scene_id)
{
	WORD get_scene_id = 0;
	if(scene_id == 0)
	{
		get_scene_id = PublicFun::GetRandNum(1, m_SceneMap.size());
	}
	else
	{
		get_scene_id = scene_id;
	}

#ifdef _DEBUG	
	Global::logger->debug("[get_scene] get scene <id=%u>", get_scene_id);
#endif
	
	defSceneArray & scene_vec = m_SceneMap[get_scene_id];
	if(scene_vec.size() == 0)
	{
		MODI_ASSERT(0);
		return NULL;
	}
#ifdef _DEBUG	
	defSceneArray::iterator iter_test = scene_vec.begin();
	WORD used_scene = 0;
	WORD unused_scene = 0;
	for(; iter_test != scene_vec.end(); iter_test++)
	{
		MODI_Scene * p_scene = *iter_test;
		if(p_scene)
		{
			if( ! (p_scene->IsUsed()))
			{
				unused_scene++;
			}
			else
			{
				used_scene++;
			}
		}
	}
	Global::logger->debug("[scene_info] <used_scene=%u,unused_scene=%u>", used_scene, unused_scene);
	
#endif	

	MODI_Scene * p_scene = NULL;
	defSceneArray::iterator iter = scene_vec.begin();
	for(; iter != scene_vec.end(); iter++)
	{
		p_scene = *iter;
		if(p_scene)
		{
			Global::logger->debug("[check_scene] is use <%d>", p_scene->IsUsed());
			if( ! (p_scene->IsUsed()))
			{
				return p_scene;
			}
		}
	}
	MODI_ASSERT(0);
	return NULL;
}


void MODI_SceneManager::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_Scene * p_scene = NULL;
	std::map<DWORD, defSceneArray >::iterator iter = m_SceneMap.begin();
	for(; iter != m_SceneMap.end(); iter++)
	{
		defSceneArray & scene_vec = iter->second;
		if(scene_vec.size() > 0)
		{
			defSceneArray::iterator iter = scene_vec.begin();
			for(; iter != scene_vec.end(); iter++)
			{
				p_scene = *iter;
				if(p_scene->IsUsed())
				{
					p_scene->Update(cur_rtime, cur_ttime);
				}
			}
		}
	}
}
