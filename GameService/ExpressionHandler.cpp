#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ExpressionCore.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"



int MODI_PackageHandler_c2gs::PlayExpression_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_PlayExpression , cmd_size , 0 , 0 , pClient );

	const MODI_C2GS_Request_PlayExpression * pReq = (const MODI_C2GS_Request_PlayExpression *)(pt_null_cmd);
	
	if( (pReq->m_nIndex >= EXPRESS_INDEX_BEGIN &&  pReq->m_nIndex <= EXPRESS_INDEX_END) == false )
	{
		Global::logger->info("[%s] client<charid=%u,name=%s> request play express.but index invaild." , 
				EXPRESSION_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_Kick;
	}

	MODI_GS2C_Notify_PlayExpressionResult res;
	res.m_byResult = MODI_ExpressionCore::IsCanPlayExpression( pClient );
	if( res.m_byResult == kPExpression_Successful )
	{
		res.m_byResult = MODI_ExpressionCore::PlayExpression( pClient , pReq->m_nIndex );
	}

	pClient->SendPackage( &res, sizeof(res) );
	return enPHandler_OK;
}
