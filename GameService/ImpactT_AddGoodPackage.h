#ifndef MODI_IMPACT_ADD_ITEM_H_
#define MODI_IMPACT_ADD_ITEM_H_

#include "ImpactTemplate.h"

class 	MODI_ImpactT_AddItem : public MODI_ImpactTemplate
{
		enum
		{
			kArg_AddID = 0,
		};

	public:
		MODI_ImpactT_AddItem();
		virtual ~MODI_ImpactT_AddItem();

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData );

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetAddID( MODI_ImpactObj & impactObj , WORD nAddID );
		WORD 	GetAddID( const MODI_ImpactObj & impactObj ) const;
};

#endif
