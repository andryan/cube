#include "GamePackt.h"
#include "BinTable.h"
#include "GameTableDefine/Musiclist.h"
#include "GameTableDefine/Levellist.h"
#include "GameTableDefine/DefaultAvatar.h"
#include "GameTableDefine/Suit.h"
#include "GameTableDefine/Avatar.h"
#include "GameTableDefine/Itemlist.h"
#include "GameTableDefine/Shop.h"
#include "GameTableDefine/Scenelist.h"
#include "GameTableDefine/Impactlist.h"
#include "GameTableDefine/Shop.h"
#include "GameTableDefine/GoodsPackage.h"
#include "GameTableDefine/Chenghao.h"
#include "protocol/gamedefine.h"
#include "BinFileMgr.h"
#include "GameTableDefine/AllResource.h"
#include <string>
#include "protocol/c2gs_itemcmd.h"
using namespace GameTable;

MODI_DynamicPackt * MODI_DynamicPackt::m_pInstance = NULL;


MODI_DynamicPackt::MODI_DynamicPackt()
{

}

MODI_DynamicPackt::~MODI_DynamicPackt()
{

}

MODI_DynamicPackt * MODI_DynamicPackt::GetInstance()
{
	if( !m_pInstance )
	{
		m_pInstance = new MODI_DynamicPackt();
	}
	return m_pInstance;
}

void	MODI_DynamicPackt::ReGenPacket()
{
	m_lList.clear();

	defItemlistBinFile  & ItemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();		 
      	defGoodsPackageBinFile & GoodsBin = MODI_BinFileMgr::GetInstancePtr()->GetGoodsPackageBinFile();
	defImpactlistBinFile & ImpactBin = MODI_BinFileMgr::GetInstancePtr()->GetImpactBinFile();

	defAllResourceBinFile & AllResourceBin = MODI_BinFileMgr::GetInstancePtr()->GetAllResourceBinFile();

	WORD i=0;
	size_t 	size;
	size = ItemBin.GetSize();
//	MODI_GameChannel::GetInstancePtr()->Broadcast();		
	for( i=0; i< ItemBin.GetSize(); ++ i)
	{
		const MODI_Itemlist * item = ItemBin.GetByIndex(i);	
		///	如果是宝箱的话，做如下处理
		if( enItemType_Item_Box == GetItemType(item->get_ConfigID()))
		{
			const MODI_AllResource * resource = AllResourceBin.Get(item->get_ConfigID());
			///	读取一些驻留信息	
			if( item->get_UseImpactID())
			{
				PacketInfo  tmpinfo;
				tmpinfo.m_nConfigId = item->get_ConfigID();
				strncpy(tmpinfo.m_szName,item->get_Name(),sizeof(tmpinfo.m_szName) - 1);
				strncpy(tmpinfo.m_szDes,item->get_Depict(),sizeof(tmpinfo.m_szDes) - 1);
				strncpy(tmpinfo.m_szPicture,resource->get_IconId(),sizeof(tmpinfo.m_szPicture) - 1);
				tmpinfo.m_nLevelReq = item->get_LevelReq();
				tmpinfo.m_bSexReq = item->get_SexReq();

				///	找到对应的效果的结构
				const MODI_Impactlist * imp = ImpactBin.Get((WORD)item->get_UseImpactID());
				MODI_ASSERT(imp);
				if( imp->get_Arg1())
				{
					///	找到对应的包的内容
					const MODI_GoodsPackage * goods = GoodsBin.Get((WORD)imp->get_Arg1());	
					if( goods)
					{
#define		get_Goods(a,n)	a->get_Goods_##n();
#define		get_Limits(a,n)	a->get_TimeLimit_##n();
#define		get_Count(a,n)	a->get_Count_##n();

#define		fill_Temp(tmp,n) \
			{ tmp.m_aItems[n-1].m_wConfigID = get_Goods(goods,n); \
			tmp.m_aItems[n-1].m_nCount = get_Count(goods,n); \
			tmp.m_aItems[n-1].m_tLimit = get_Limits(goods,n);}

					fill_Temp(tmpinfo,1);
					fill_Temp(tmpinfo,2);
					fill_Temp(tmpinfo,3);
					fill_Temp(tmpinfo,4);
					fill_Temp(tmpinfo,5);
					fill_Temp(tmpinfo,6);
					fill_Temp(tmpinfo,7);
					fill_Temp(tmpinfo,8);
					fill_Temp(tmpinfo,9);

					fill_Temp(tmpinfo,10);
					}
				}
				m_lList.push_back(tmpinfo);
			}
		}		
		}
	///	下面就是制作包
	MODI_GS2C_Notify_PacketList * res = (MODI_GS2C_Notify_PacketList *)m_szPacket;	
	
	AutoConstruct(res);
	MODI_ASSERT(sizeof(PacketInfo) * m_lList.size() < Skt::MAX_PACKETSIZE);
	PACKET_ITOR   itor = m_lList.begin();

	WORD j=0;
	while( itor != m_lList.end())
	{
		res->m_pPacket[j++] = *itor++;	
	}
	res->m_wSize = j;

}

void MODI_DynamicPackt::OnPlayerLogin(MODI_ClientAvatar * m_pclient)
{
	
	MODI_GS2C_Notify_PacketList * res = (MODI_GS2C_Notify_PacketList *)m_szPacket;	
	m_pclient->SendPackage(res,sizeof(PacketInfo)*res->m_wSize+sizeof(MODI_GS2C_Notify_PacketList));
}

void MODI_DynamicPackt::BroadCastForChange()
{

	MODI_GS2C_Notify_PacketList * res = (MODI_GS2C_Notify_PacketList *)m_szPacket;	
	MODI_GameChannel::GetInstancePtr()->Broadcast(res,sizeof(PacketInfo)*res->m_wSize+sizeof(MODI_GS2C_Notify_PacketList));
}

bool MODI_DynamicPackt::ReloadPackt()
{

	const char * szItemListBinFileBaseName = "MODI_Itemlist.bin";
	const char * szImpactListBinFileBaseName = "MODI_Impactlist.bin";
	const char * szGoodsPackageBinFileBaseName = "MODI_GoodsPackage.bin";
	const char * szAllResourceBinFileBaseName = "MODI_AllResource.bin";

	defItemlistBinFile  & ItemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();		 
      	defGoodsPackageBinFile & GoodsBin = MODI_BinFileMgr::GetInstancePtr()->GetGoodsPackageBinFile();
	defImpactlistBinFile & ImpactBin = MODI_BinFileMgr::GetInstancePtr()->GetImpactBinFile();

	defAllResourceBinFile & AllResourceBin = MODI_BinFileMgr::GetInstancePtr()->GetAllResourceBinFile();

/*	ItemBin.Unload();
	GoodsBin.Unload();
	ImpactBin.Unload();
	*/
	const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
	if( !pResInfo )
	{
		return false; 
	}
	std::string itempath=pResInfo->strBinDir.c_str();
	std::string goodspath=pResInfo->strBinDir.c_str();
	std::string impactpath=pResInfo->strBinDir.c_str();
	std::string allresourcepath = pResInfo->strBinDir.c_str();

	allresourcepath += "/";
	itempath += "/";
	goodspath += "/";
	impactpath += "/";

	allresourcepath += szAllResourceBinFileBaseName;
	itempath += szItemListBinFileBaseName;
	goodspath += szGoodsPackageBinFileBaseName;
	impactpath += szImpactListBinFileBaseName;

	if( !ItemBin.Load(itempath.c_str()))
	{
		return false;	
	}
	if( !GoodsBin.Load(goodspath.c_str()))
	{
		return false;
	}
	if( !ImpactBin.Load(impactpath.c_str()))
	{
		return false;
	}
	if( !AllResourceBin.Load(allresourcepath.c_str()))
	{
		return false;
	}

	ReGenPacket();
	///	广播礼包数据
	BroadCastForChange();	
	return true;
}


bool	MODI_DynamicPackt::Init()
{
	ReGenPacket();	
	return true;
}


