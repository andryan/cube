#include "GameLobby.h"
#include "GameTick.h"
#include "protocol/c2gs.h"
#include <vector>
#include "Type.h"
#include "Share.h"
#include "AssertEx.h"
#include "SceneManager.h"

MODI_GameLobby::MODI_GameLobby():
    m_SyncDirtyTimer(500),m_SingleRoom(SINGLE_ROOM_ID)
{

}

MODI_GameLobby::~MODI_GameLobby()
{
	m_mapClients.clear();

	int i = 0;
	for( i = 0; i < MAX_ROOM_COUNT; i++ )
	{
		delete m_NormalRooms[i];
	}
}

bool MODI_GameLobby::Create()
{
	int i = 0;
	defRoomID roomid = 1;
	for( i = 0; i < MAX_ROOM_COUNT; i++ )
	{
		m_NormalRooms[i] = new MODI_NormalGameRoom(roomid);
		roomid += 1;
	}

	for( i = 0; i < MAX_ROOM_COUNT; i++ )
	{
		m_Rooms[i] = 0;
	}

	m_mapClients.clear();
    return true;
}

///	广播整个大厅(只会广播在大厅中的玩家)
bool MODI_GameLobby::Broadcast(const void * pt_null_cmd, int cmd_size)
{
#if 0	
	MAP_CLIENTAVATARS_ITER itor = m_mapClients.begin();
	while (itor != m_mapClients.end())
	{
		MODI_ClientAvatar * pTemp = itor->second;
		if (pTemp)
		{
			pTemp->SendPackage(pt_null_cmd, cmd_size);
		}
		itor++;
	}
	return true;
#endif
	
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	MAP_CLIENTAVATARS_ITER itor = m_mapClients.begin();
	while (itor != m_mapClients.end())
	{
		MODI_ClientAvatar * pTemp = itor->second;
		if( pTemp )
		{
			(*p_write_id) = pTemp->GetSession()->GetSessionID();
			p_write_id++;
			p_send_cmd->m_wdSessionSize++;
		}
		itor++;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
}

bool MODI_GameLobby::Broadcast_Except(const void * pt_null_cmd, int cmd_size, MODI_ClientAvatar * pExcept )
{
#if 0	
	MAP_CLIENTAVATARS_ITER itor = m_mapClients.begin();
	while (itor != m_mapClients.end())
	{
		MODI_ClientAvatar * pTemp = itor->second;
		if ( pTemp != pExcept )
		{
			pTemp->SendPackage(pt_null_cmd, cmd_size);
		}
		itor++;
	}
	return true;
#endif

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2GW_Broadcast_Cmd * p_send_cmd = (MODI_GS2GW_Broadcast_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_wdCmdSize = cmd_size;
	memcpy(p_send_cmd->m_pData, pt_null_cmd, cmd_size);
	char * p_write_session = p_send_cmd->m_pData + cmd_size;

	MODI_SessionID * p_write_id = (MODI_SessionID *)p_write_session;
	MAP_CLIENTAVATARS_ITER itor = m_mapClients.begin();
	while (itor != m_mapClients.end())
	{
		MODI_ClientAvatar * pTemp = itor->second;
		if( pTemp != pExcept)
		{
			(*p_write_id) = pTemp->GetSession()->GetSessionID();
			p_write_id++;
			p_send_cmd->m_wdSessionSize++;
		}
		itor++;
	}

	MODI_GameTask::ms_pInstancePtr->SendCmd((const Cmd::stNullCmd *)p_send_cmd, p_send_cmd->GetSize());
	return true;
}

///    向某个客户端发送房间列表
bool MODI_GameLobby::SendRoomList(MODI_ClientAvatar * pClient)
{
    char szPackageBuf[Skt::MAX_USERDATASIZE];
    memset(szPackageBuf, 0, sizeof(szPackageBuf));

    MODI_GS2C_Notify_Roomlist * p_notify = (MODI_GS2C_Notify_Roomlist *) (szPackageBuf);
    AutoConstruct(p_notify);

    p_notify->m_nCount = 0;
    MODI_RoomInfo * pRoomsStartAddr = (MODI_RoomInfo *) (&p_notify->m_pRooms[0]);

    for (int i = 0; i < MAX_ROOM_COUNT; i++)
    {
        if (m_Rooms[i] )
        {
            pRoomsStartAddr[p_notify->m_nCount] = m_Rooms[i]->GetRoomInfo();
            p_notify->m_nCount += 1;
        }
    }

    int iSendSize = p_notify->m_nCount * sizeof(MODI_RoomInfo) + sizeof(MODI_GS2C_Notify_Roomlist);
    // 发送给客户端
    return pClient->SendPackage(p_notify, iSendSize);
}

///    向某个客户端发送玩家列表
bool MODI_GameLobby::SendPlayerList(MODI_ClientAvatar * pClient)
{
    char szPackageBuf[Skt::MAX_USERDATASIZE];
    int iSendSize = CreateSyncPlaylistPackage( szPackageBuf , Skt::MAX_USERDATASIZE );
    // 发送给客户端
    return pClient->SendPackage( szPackageBuf , iSendSize, true);
}

int MODI_GameLobby::CreateSyncPlaylistPackage( char * szBuf , int nBufSize )
{
    memset( szBuf, 0, nBufSize );
    MODI_GS2C_Notify_Playerlist * p_notify = (MODI_GS2C_Notify_Playerlist *) (szBuf);
    AutoConstruct(p_notify);
    p_notify->m_nSize = 0;
    MODI_RoleInfo_Base * pRolesStartAddr = (MODI_RoleInfo_Base *) (&p_notify->m_pRoles[0]);
    MAP_CLIENTAVATARS_ITER itor = m_mapClients.begin();
    while (itor != m_mapClients.end())
    {
        MODI_ClientAvatar * pTemp = itor->second;
//		if( pTemp->IsInSingleGame() == false )

		if( pTemp->GetSession()->IsInChannelRechangedStatus() ||
			pTemp->GetSession()->IsInRechangeChannelingStatus() )

		{
			itor++;
			continue ;
		}

		if( pTemp->IsInShop() )
		{
			itor++;
			continue ;
		}
		/// 发送过同步信息，客户端知道这个人的基本信息了
		pRolesStartAddr[p_notify->m_nSize] = pTemp->GetRoleBaseInfo();
		p_notify->m_nSize += 1;
		itor++;
    }

    int iSendSize = p_notify->m_nSize * sizeof(MODI_RoleInfo_Base)
            + sizeof(MODI_GS2C_Notify_Playerlist);
	
	return iSendSize;
}



///    有房间创建时被调用
bool MODI_GameLobby::OnCreateRoom( MODI_ClientAvatar * pClient , const MODI_GameRoom & newRoom)
{
    Global::logger->debug("[%s] client<%s> create room successful, room_id<%u>, password<%s>", 
			GS_ROOMINFO , 
			pClient->GetRoleName(),
			newRoom.GetRoomID(),
			newRoom.GetPassword());

	defRoomID id = newRoom.GetRoomID();
    // 复写备份数据，避免被当作脏数据同步出去
	if( id != SINGLE_ROOM_ID )
	{
		m_RoomsBackUp[id] = newRoom.GetRoomInfo();
	   	MODI_GS2C_Notify_AddRoom notify;
		notify.stRoom = newRoom.GetRoomInfo();
		Broadcast(&notify, sizeof(notify));
	}

	return true;
}

///    有房间销毁时被调用
bool MODI_GameLobby::OnDestroyRoom(defRoomID roomID)
{
	
    Global::logger->debug("[%s] room deleted, room_id<%u>", GS_ROOMINFO , roomID);
	/// 先收回等待房间场景
	MODI_MultiGameRoom * p_room = m_Rooms[roomID - 1];
	if(p_room)
	{
		MODI_Scene * p_scene = p_room->GetRoomScene();
		if(p_scene)
		{
			p_room->SetRoomScene(NULL);
			p_scene->Reset();
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	else
	{
		MODI_ASSERT(0);
	}

	if( roomID != INVAILD_ROOM_ID && roomID != SINGLE_ROOM_ID )
	{
		m_Rooms[roomID - 1] = 0;
	}

    MODI_GS2C_Notify_DelRoom notify;
    notify.roomID = roomID;
    return this->Broadcast(&notify, sizeof(notify));
}

defRoomID MODI_GameLobby::GetFreeRoom()
{
    defRoomID id = INVAILD_ROOM_ID;

    for (int i = 0; i < MAX_ROOM_COUNT; i++)
    {
        if ( !m_Rooms[i] )
        {
            id = i + 1;
            break;
        }
    }

    return id;
}

MODI_ClientAvatar * MODI_GameLobby::FindClientInLobby(MODI_GUID client_guid)
{
    MODI_ClientAvatar * pRet = 0;
    MAP_CLIENTAVATARS_ITER itor = m_mapClients.find(client_guid);
    if (itor != m_mapClients.end())
    {
        pRet = itor->second;
    }
    return pRet;
}

bool MODI_GameLobby::IsInLobby(MODI_GUID client_guid)
{
    return FindClientInLobby(client_guid) ? true : false;
}

bool MODI_GameLobby::EnterLobby(MODI_ClientAvatar * pClient)
{
    MAP_CLIENTAVATARS_INSERT_RESULT result = m_mapClients.insert(std::make_pair(pClient->GetGUID(),
            pClient));

    if (result.second)
    {
		pClient->SetGameState( kInLobby );

        //   进入大厅时，需要同步房间列表
        SendRoomList(pClient);
        //   同步玩家列表
        SendPlayerList(pClient);
    }

    return result.second;
}

void MODI_GameLobby::LeaveLobby(MODI_GUID client_guid)
{
    m_mapClients.erase(client_guid);
}

MODI_MultiGameRoom * MODI_GameLobby::GetMultiRoom(defRoomID id)
{
	if( id == INVAILD_ROOM_ID || id == SINGLE_ROOM_ID )
		return 0;

    MODI_MultiGameRoom * pRet = (MODI_MultiGameRoom *)(m_Rooms[id - 1]);
	return pRet;
}

MODI_MultiGameRoom * MODI_GameLobby::GetWaittingRoom( ROOM_TYPE type , ROOM_SUB_TYPE subtype , 
			defMusicID musicid , defMapID mapid , bool bPwd )
{
	MODI_MultiGameRoom * pRet = 0;

	vector<int>  rooms;

	vector<int>  startgame;

	for( int i = 0; i < MAX_ROOM_COUNT; i++ )
	{
		if( !m_Rooms[i] )
			continue ;

		if( IsCanEnterRoom( m_Rooms[i] ) == SvrResult_Ok && 
			bPwd == m_Rooms[i]->IsPasswordRoom() &&
			type == m_Rooms[i]->GetRoomType() &&
			(subtype == m_Rooms[i]->GetRoomSubType() || subtype == ROOM_STYPE_END))
		{
			if( musicid != INVAILD_CONFIGID &&
				musicid != m_Rooms[i]->GetMusicID() )
				continue ;

			if( mapid != INVAILD_CONFIGID &&
				mapid != m_Rooms[i]->GetMapID() )
				continue ;

	//		pRet = m_Rooms[i];
			if( m_Rooms[i]->GetRoomStatus() == ROOM_STATUS_USING)
			{
				rooms.push_back(i);
			}
			else
			{
				startgame.push_back(i);
			}
		}
	}
	int i=0;
#ifdef _XXP_DEBUG
	Global::logger->debug("<<<<<<<<<<<<<<<<<<<<<<<<get  rooms  = %d, get gamerooms= %d >>>>>>>>>>>>>>>>>>>>>>>>>>>",rooms.size(),startgame.size());
#endif

	if( !rooms.empty())
	{

		i= PublicFun::GetRandNum(1,10000) % rooms.size();
		pRet = m_Rooms[rooms[i]];
#ifdef _XXP_DEBUG
	Global::logger->debug("<<<<<<<<<<<<<<<<<<<<<<<<get  random %d >>>>>>>>>>>>>>>>>>>>>>>>>>>",i);
#endif
	}
	else if( !startgame.empty())
	{
		i= PublicFun::GetRandNum(1,10000) % startgame.size();
		pRet = m_Rooms[startgame[i]];
#ifdef _XXP_DEBUG
	Global::logger->debug("<<<<<<<<<<<<<<<<<<<<<<<<get  random %d >>>>>>>>>>>>>>>>>>>>>>>>>>>",i);
#endif
	}
	else
	{
		pRet = 0;
	}
	return pRet;

}

void MODI_GameLobby::BackupRooms()
{
    // 目前只要简单COPY就可以
    for (int i = 0; i < MAX_ROOM_COUNT; i++)
    {
		if( m_Rooms[i] )
		{
			m_RoomsBackUp[i] = m_Rooms[i]->GetRoomInfo();
		}
    }
}

void MODI_GameLobby::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
    //    更新所有游戏对象
    for (int i = 0; i < MAX_ROOM_COUNT; i++)
    {
        if (m_Rooms[i])
        {
            m_Rooms[i]->Update(cur_rtime, cur_ttime);
        }
    }

	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
    ///    定时同步脏数据
	if (m_SyncDirtyTimer( pTick->GetTimer() ) )
	{
		//    定时同步脏的房间信息
		SyncDirtyRooms();

		//    同步完成后，重新备份
		BackupRooms();
	}
}


defRoomUpdateMask MODI_GameLobby::GetRoomUpdateMask(BYTE id)
{
	defRoomUpdateMask mask = 0;
	const	MODI_NormalRoomInfo & backupRoomInfo = m_RoomsBackUp[id].m_detailNormalRoom;
	const	MODI_NormalRoomInfo & curRoomInfo = m_Rooms[id]->GetNormalInfo();
	if( strncmp( m_RoomsBackUp[id].m_cstrRoomName , 
				m_Rooms[id]->GetRoomInfo().m_cstrRoomName , 
				sizeof(m_RoomsBackUp[id].m_cstrRoomName)))
	{
		mask |= enRoomUP_Name; 
	}
	
	if( backupRoomInfo.m_nMusicId != curRoomInfo.m_nMusicId )
	{
		mask |= enRoomUP_MusicID;
	}
	
    if( backupRoomInfo.m_nMapId != curRoomInfo.m_nMapId )
	{
		mask |= enRoomUP_MapID;
	}	

	if( backupRoomInfo.m_nStateMask != curRoomInfo.m_nStateMask )
	{
		mask |= enRoomUP_PosState;
	}

	if( m_RoomsBackUp[id].m_roomType != m_Rooms[id]->GetRoomInfo().m_roomType )
	{
		mask |= enRoomUP_RoomType;
	}

	if( m_RoomsBackUp[id].m_bPlaying != m_Rooms[id]->GetRoomInfo().m_bPlaying )
	{
		mask |= enRoomUP_IsPlaying;
	}

	return mask;
}

void MODI_GameLobby::BuildBaseRoomUpdateInfo(BYTE id , void * p,defRoomUpdateMask  mask)
{
	MODI_GS2C_Notify_RoomUpdate * p_notify = (MODI_GS2C_Notify_RoomUpdate *)(p);
	/*
	 * 大厅只需要房间的基本信息中的 《房间名》 信息
	 */
	if( mask & enRoomUP_Name )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , 
				m_Rooms[id]->GetRoomInfo().m_cstrRoomName , 
				sizeof(m_Rooms[id]->GetRoomInfo().m_cstrRoomName));
		p_notify->m_byUpdateSize += sizeof(m_Rooms[id]->GetRoomInfo().m_cstrRoomName);
	}	
}

void MODI_GameLobby::BuildNormalRoomUpdateInfo(BYTE id, void * p,defRoomUpdateMask mask)
{
	MODI_GS2C_Notify_RoomUpdate * p_notify = (MODI_GS2C_Notify_RoomUpdate *) (p);
	const	MODI_NormalRoomInfo & curRoomInfo = m_Rooms[id]->GetNormalInfo();

	/*
	 * 大厅只需要普通房间的 《音乐ID》 《地图ID》 《位置》 信息
	 */
	if( mask & enRoomUP_MusicID )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , &curRoomInfo.m_nMusicId , sizeof(curRoomInfo.m_nMusicId) );
		p_notify->m_byUpdateSize += sizeof(curRoomInfo.m_nMusicId);
	}
	
    if( mask & enRoomUP_MapID )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , &curRoomInfo.m_nMapId , sizeof(curRoomInfo.m_nMapId) );
		p_notify->m_byUpdateSize += sizeof(curRoomInfo.m_nMapId);
	}	

	if( mask & enRoomUP_PosState )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , &curRoomInfo.m_nStateMask , sizeof(curRoomInfo.m_nStateMask) );
		p_notify->m_byUpdateSize += sizeof(curRoomInfo.m_nStateMask);
	}

	if( mask & enRoomUP_RoomType )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize ,
			   	&(m_Rooms[id]->GetRoomInfo().m_roomType) ,
			   	sizeof(m_Rooms[id]->GetRoomInfo().m_roomType) );
		p_notify->m_byUpdateSize += sizeof(m_Rooms[id]->GetRoomInfo().m_roomType);
	}

	if( mask & enRoomUP_IsPlaying )
	{
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize ,
			   	&(m_Rooms[id]->GetRoomInfo().m_bPlaying) ,
			   	sizeof(m_Rooms[id]->GetRoomInfo().m_bPlaying) );
		p_notify->m_byUpdateSize += sizeof(m_Rooms[id]->GetRoomInfo().m_bPlaying);
	}
}

void MODI_GameLobby::SyncDirtyRooms()
{
    if (m_mapClients.empty())
        return;

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof( szPackageBuf ) );
	
    MODI_GS2C_Notify_RoomUpdate * p_notify = (MODI_GS2C_Notify_RoomUpdate *)(szPackageBuf);
	AutoConstruct( p_notify );
	p_notify->m_byUpdateSize = 0;

    for (int i = 0; i < MAX_ROOM_COUNT; i++)
    {
		if( !m_Rooms[i] )
			continue ;

		// 是否有信息改变了，生成掩码
		defRoomUpdateMask mask = GetRoomUpdateMask(i);
		if( !mask )
			continue;

		// 先写房间ID
		defRoomID roomID = m_Rooms[i]->GetRoomID();
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , &roomID, sizeof(defRoomID) );
		p_notify->m_byUpdateSize += sizeof(defRoomID);

		// 接着是更新掩码信息
		memcpy( p_notify->m_pUpdateInfos + p_notify->m_byUpdateSize , &mask , sizeof(mask) );
		p_notify->m_byUpdateSize += sizeof(mask);

		// 生成房间基本信息的更新内容
		BuildBaseRoomUpdateInfo( i , p_notify , mask);
		// 根据房间类型，生成具体的房间更新信息
        if (m_Rooms[i]->IsNormalRoom())
        {
            BuildNormalRoomUpdateInfo(i, p_notify,mask);
        }
    }

	if( p_notify->m_byUpdateSize )
	{
		int iSendSize = sizeof(char) * p_notify->m_byUpdateSize + sizeof(MODI_GS2C_Notify_RoomUpdate);
		this->Broadcast(p_notify,iSendSize); 
	}
}

WORD MODI_GameLobby::IsCanEnterRoom( MODI_MultiGameRoom * room )
{
	if( !room )
	{
		return SvrResult_Room_NotExistJ;
	}

    // 房间是否正在开始游戏中（只能开始完之后才能进入)
    if (room->IsStarting())
    {
        return SvrResult_Room_StartedJ;
    }

    // 房间人数是否已满
    if (room->IsFull())
    {
        return SvrResult_Room_PlayerFullJ;
    }

	return SvrResult_Ok;
}

WORD MODI_GameLobby::CreateRoom(MODI_ClientAvatar * pClient, defRoomType roomType,defMusicID nMusicID , 
								defMapID nMapID , WORD scene_id, const char * szRoomName , const char * szPassword , bool bAllowSpectator )
{
	if(!pClient)
	{
		return SvrResult_Room_CInvaildPara;
	}
	
	// 获得个空房间
	defRoomID roomid = GetFreeRoom();

	if( roomid == INVAILD_ROOM_ID )
	{
		// 大厅满
		return SvrResult_Room_LobbyFullC;
	}

	// 创建房间的主类型
	unsigned char ucMainType = RoomHelpFuns::GetRoomMainType(roomType);
	unsigned char ucSubType = RoomHelpFuns::GetRoomSubType(roomType);
	DISABLE_UNUSED_WARNING(ucSubType);

	MODI_GameRoom * room = 0;

	DWORD nMaxPlayer = 0;
	DWORD nMaxSpector = 0;
    if (ucMainType == ROOM_TYPE_NORMAL)
    {
		scene_id = 1;
		// 获得房间对象
		room = m_NormalRooms[roomid - 1];

		if(ucSubType == ROOM_STYPE_TEAM  || ucSubType == ROOM_STYPE_KEYBOARD_TEAM )
		{
			if(room)
			{
				MODI_MultiGameRoom * p_mulite_room = (MODI_MultiGameRoom *)room;
				p_mulite_room->SetSubType(ROOM_SUB_TYPE(ucSubType));
				p_mulite_room->SetMainType(ROOM_TYPE_NORMAL);
			}
		}
		else
		{

			if(room)
			{
				MODI_MultiGameRoom * p_mulite_room = (MODI_MultiGameRoom *)room;
				p_mulite_room->SetSubType(ROOM_SUB_TYPE(ucSubType));
				
			}

		}

		nMaxPlayer = NORMAL_PLAYSLOT_INROOM;
        nMaxSpector = NORMAL_SPECTATORSLOT_INROOM;
    }
    else if (ucMainType == ROOM_TYPE_VIP)
    {
		/// 先不限制，放到外网使用
#ifdef _ML_VERSION
#else		
		if(! pClient->IsVip())
		{
			return SvrResult_Room_CInvaildPara;
		}
#endif
		scene_id = 2;
		
		// 获得房间对象
		room = m_NormalRooms[roomid - 1];

		if(ucSubType == ROOM_STYPE_TEAM  || ucSubType == ROOM_STYPE_KEYBOARD_TEAM )
		{
			if(room)
			{
				MODI_MultiGameRoom * p_mulite_room = (MODI_MultiGameRoom *)room;
				p_mulite_room->SetSubType(ROOM_SUB_TYPE(ucSubType));
				p_mulite_room->SetMainType(ROOM_TYPE_VIP);

			}
		}
		else
		{
			if(room)
			{
				MODI_MultiGameRoom * p_mulite_room = (MODI_MultiGameRoom *)room;
				p_mulite_room->SetSubType(ROOM_SUB_TYPE(ucSubType));
			}

		}

		nMaxPlayer = VIP1_PLAYSLOT_INROOM;
        nMaxSpector = VIP1_SPECTATORSLOT_INROOM;
	}
	
    else if (ucMainType == ROOM_TYPE_BIG_PRESENTER)
    {

		return SvrResult_Room_CInvaildPara;
    }
    else if(ucMainType == ROOM_TYPE_SINGLE )
    {
		// 单人房间
		room = &m_SingleRoom;
    }

	if( room )
	{
		MODI_Scene * p_scene = NULL;
		// 设置房间对象信息
		room->SetRoomType( roomType );
		room->SetRoomName( szRoomName );
		if(ucMainType != ROOM_TYPE_SINGLE )
		{
			/// 获取一张等待场景
			p_scene = MODI_SceneManager::GetInstance().GetScene(room, scene_id);
			if(!p_scene)
			{
				MODI_ASSERT(0);
				return SvrResult_Room_CInvaildPara;
			}

			room->SetRoomScene(p_scene);
			p_scene->SetSceneRoom((MODI_MultiGameRoom *)room);
			p_scene->SetUsed(true);
		}
		
		if(RoomHelpFuns::IsHasPassword(roomType) &&  szPassword && szPassword[0] )
		{
			room->SetPassword( szPassword );
		}

		if( !room->OnCreate( pClient , nMusicID , nMapID , nMaxPlayer , nMaxSpector , bAllowSpectator ) )
		{
			MODI_ASSERT(0);
			return SvrResult_Room_CreateFaild;
		}

		if( !room->EnterRoom( pClient ) )
		{
			room->OnDestroy();
			if(ucMainType != ROOM_TYPE_SINGLE )
			{
				if(p_scene)
					p_scene->Reset();
			}
			return SvrResult_Room_CreateFaild;
		}

//    	if(ucMainType != ROOM_TYPE_SINGLE )
//		{
//			//    从大厅容器中移出创建者
//			LeaveLobby(pClient->GetGUID());
//		}

		if(ucMainType != ROOM_TYPE_SINGLE )
		{
			// 广播给大厅中的所有人，有新房间建立
			OnCreateRoom(pClient , *room);
			m_Rooms[roomid - 1] = (MODI_MultiGameRoom *)room;

#ifdef _ROOM_INFO			
		static unsigned int success_count = 0;
		success_count++;
		Global::logger->debug("[create_room] create %u room success", success_count);
#endif 
#ifdef _FB_LOGGER
		Global::fb_logger->debug("\t%u\tCREATES ROOM\t%d\t%d\t%s\t%s",Global::m_stRTime.GetSec(),room->GetRoomID(),pClient->GetCharID(),pClient->GetAccName(),room->GetRoomInfo().m_cstrRoomName);
#endif

		}
	}
	else 
	{
		MODI_ASSERT(0);
		return SvrResult_Room_CreateFaild;
	}
	
	return SvrResult_Ok;
}

BYTE MODI_GameLobby::GetRoomSize()
{
	BYTE size = 0;
	for (int i = 0; i < MAX_ROOM_COUNT; i++)
	{
		if (m_Rooms[i])
		{
			size++;
		}
	}
	return size;
}

