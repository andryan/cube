#include "ImpactT_RecordMusic.h"
#include "gamestructdef.h"
#include "ClientAvatar.h"
#include "protocol/c2gs_rolecmd.h"
#include "protocol/c2gs_itemcmd.h"

MODI_ImpactT_RecordMusic::MODI_ImpactT_RecordMusic()
{

}
		
MODI_ImpactT_RecordMusic::~MODI_ImpactT_RecordMusic()
{

}

bool MODI_ImpactT_RecordMusic::CheckImpactData( GameTable::MODI_Impactlist & impactData )
{
	return true;
}

int MODI_ImpactT_RecordMusic::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	return kImpactH_Successful;
}

int MODI_ImpactT_RecordMusic::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( !pObj )
	{
		MODI_ASSERT(0);
		return kImpactH_ParamFaild;
	}

	MODI_GS2C_Notify_RecordMusicRes res;
	res.result = MODI_GS2C_Notify_RecordMusicRes::kSuccessful;
	pObj->SendPackage( &res , sizeof(res) );

	return kImpactH_Successful;
}

int MODI_ImpactT_RecordMusic::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	return kImpactH_Successful;
}

