/** 
 * @file MoveableObj.h
 * @brief 游戏中的移动对象
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */
#ifndef MODI_MOVEABLEOBJECT_H_
#define MODI_MOVEABLEOBJECT_H_

#include "BaseObject.h"
#include "G3D/Vector3.h"
#include "protocol/c2gs_rolecmd.h"

/**
 * @brief 具有移动能力的对象
 *
 */
class MODI_MoveableObject : public MODI_BaseObject
{
public:
    MODI_MoveableObject(MODI_GUID id);
    virtual ~MODI_MoveableObject();


	/** 
	 * @brief 获得对象所在位置的X坐标
	 * 
	 * @return 	返回X坐标位置
	 */
	float 	GetPosX() const { return m_Postion.x; }

	 /** 
	 * @brief 获得对象所在位置的Y坐标
	 * 
	 * @return 	返回Y坐标位置
	 */
	float 	GetPosY() const { return m_Postion.y; }

	 /** 
	 * @brief 获得对象所在位置的Z坐标
	 * 
	 * @return 	返回Z坐标位置
	 */
	float 	GetPosZ() const { return m_Postion.z; }


	/** 
	 * @brief 获得对象的朝向
	 * 
	 * @return 	返回当前朝向
	 */
	float 	GetDir() const { return m_fDir; }

	void 	SetPostion( const G3D::Vector3 & pos )
	{
		m_Postion = pos;
	}

	void 	SetPosX( float x )
	{
		m_Postion.x = x;
	}

	void 	SetPosY( float y )
	{
		m_Postion.y = y;
	}

	void 	SetPosZ( float z )
	{
		m_Postion.z = z;
	}

	void 	SetDir( float f )
	{
		m_fDir = f;
	}
	
	void 	SetMovementFlag( enMovementFlags flag )
	{
		m_flag = flag;
	}

	enMovementFlags GetMovementFlag() const
	{
		return m_flag;
	}

	void SetMoveState(const MODI_C2GS_Request_Move_Cmd * p_recv_cmd)
	{
		m_stDestState = p_recv_cmd->m_stSrcState;
	}

	const bool IsInScene()
	{
		return m_blIsInScene;
	}

	void SetInScene(bool is_in)
	{
		m_blIsInScene = is_in;
	}

	const DWORD  GetScreen()
	{
		return m_dwScreen;
	}

	void SetScreen(DWORD screen)
	{
		m_dwScreen = screen;
	}

	const MODI_MoveState & GetMoveState()
	{
		return m_stSrcState;
	}

	const void SetSrcState(const MODI_MoveState & move_state)
	{
		m_stSrcState = move_state;
	}

	const void SetDestState(const MODI_MoveState & move_state)
	{
		m_stDestState = move_state;
	}
	
	const void StopMove()
	{
		m_stSrcState.m_enMoveFlag = kMoveMentFlag_None;
	}

public:
	/// 客户端移动
	MODI_MoveState m_stSrcState;
	MODI_MoveState m_stDestState;
	
	DWORD m_dwScreen;
	bool m_blIsInScene;

protected:

	enMovementFlags 	m_flag;

	/// 对象坐标
	G3D::Vector3 m_Postion;

	/// 对象的朝向
	float m_fDir;

	
};

#endif
