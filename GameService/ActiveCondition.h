#ifndef _ACTION_CONDITION_H__
#define _ACTION_CONDITION_H__

#include "Condition.h"

class MODI_ActiveSignupdays : public MODI_Condition
{

 public:
	bool Init(const char * con_line);
	bool is_valid(MODI_ClientAvatar * p_client);
 private:
	std::string m_stOp;
	WORD	m_wdCondition;

};



#endif


