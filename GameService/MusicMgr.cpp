#include "MusicMgr.h"
#include "protocol/c2gs.h"
#include "GameChannel.h"
#include "GameTick.h"
#include "Share.h"
#include "BinFileMgr.h"
#include <math.h>
#include "protocol/gamedefine.h"
#include "MdmMgr.h"
#include "LoggerInfo.h"
#include "LoggerAction.h"
#include "AssertEx.h"
#include "Base/HelpFuns.h"
#include "BaseOS.h"

MODI_MusicMgr *MODI_MusicMgr::m_pMgr=NULL;

MODI_MusicMgr::MODI_MusicMgr()
{
	m_bAomInit = false;
	m_bHideInit=false;
}
MODI_MusicMgr::~MODI_MusicMgr()
{

}


defMusicID 	MODI_MusicMgr::GetRandomHideID()
{
	defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
	if( musicBin.GetSize() == 0 )
		return RANDOM_MUSIC_ID;
	if(! m_bHideInit)
	{
		unsigned int i=0; 
		for( i=0;  i< musicBin.GetSize(); ++i)
		{

			const GameTable::MODI_Musiclist * pMusicInfo = musicBin.GetByIndex( i );
			if (!pMusicInfo)
			{
				MODI_ASSERT(0);
				return 1;
			}
			if( pMusicInfo->get_SingType() == kMusicSongType_Hide)
			{
				m_vHideMusic.push_back((int)i);
			}
		}
		m_bHideInit = true;
	}	
	if( m_vHideMusic.size() == 0)
	{
		return RANDOM_MUSIC_ID;
	}

	size_t nRmusic = PublicFun::GetRandNum(1,100000) % (m_vHideMusic.size());
	const GameTable::MODI_Musiclist *pMusicInfo = musicBin.GetByIndex( m_vHideMusic[nRmusic] );
	if( pMusicInfo )
	{

		return pMusicInfo->get_ConfigID();

	}

	return RANDOM_MUSIC_ID;
}


/**
 * @brief 获取INA的AOM随机歌曲
 * 
 */
defMusicID 	MODI_MusicMgr::GetRandomAom()
{
	defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
	if( musicBin.GetSize() == 0 )
	{
		return RANDOM_MUSIC_ID;
	}
	
	if(! m_bAomInit)
	{
		unsigned int i=0; 
		for( i=0;  i< musicBin.GetSize(); ++i)
		{

			const GameTable::MODI_Musiclist * pMusicInfo = musicBin.GetByIndex( i );
			if (!pMusicInfo)
			{
				MODI_ASSERT(0);
				return 1;
			}
			if( pMusicInfo->get_MonthlySinger() == 1)
			{
				m_vAomMusic.push_back((int)i);
			}
		}
		m_bAomInit = true;
	}	
	if( m_vAomMusic.size() == 0)
	{
		return RANDOM_MUSIC_ID;
	}

	size_t nRmusic = PublicFun::GetRandNum(1,100000) % (m_vAomMusic.size());
	const GameTable::MODI_Musiclist *pMusicInfo = musicBin.GetByIndex( m_vAomMusic[nRmusic] );
	if( pMusicInfo )
	{
		return pMusicInfo->get_ConfigID();
	}

	return RANDOM_MUSIC_ID;
}
