#include "ImpactT_Animation.h"
#include "gamestructdef.h"
#include "ClientAvatar.h"
#include "protocol/c2gs_rolecmd.h"
#include "GameLobby.h"
#include "NormalGameRoom.h"

MODI_ImpactT_Animation::MODI_ImpactT_Animation()
{

}
		
MODI_ImpactT_Animation::~MODI_ImpactT_Animation()
{

}

bool MODI_ImpactT_Animation::CheckImpactData( GameTable::MODI_Impactlist & impactData )
{
	return true;
}

int MODI_ImpactT_Animation::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	long modfiy_score = (long)impactData.get_Arg1();
	SetModfiyScore( impactObj , modfiy_score  );
	long modfiy_renqi = (long)impactData.get_Arg2();
	SetModfiyRenqi( impactObj , modfiy_renqi );

	return kImpactH_Successful;
}

int MODI_ImpactT_Animation::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( !pObj )
	{
		MODI_ASSERT(0);
		return kImpactH_ParamFaild;
	}

	if( pObj->IsObjStateFlag( kObjState_PlayAnimation ) )
	{
		Global::logger->debug("[%s] client<charid=%u,name=%s> already in play animation state, active impact faild.", 
				IMPACT_MODULE ,
				pObj->GetCharID(),
				pObj->GetRoleName() );
		return kImpactH_AntherExist;
	}

	pObj->SetObjStateFlag( kObjState_PlayAnimation );

	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	if( pLobby && pObj->IsInMulitRoom() )
	{
		MODI_NormalGameRoom * pRoom = (MODI_NormalGameRoom *)(pLobby->GetMultiRoom( pObj->GetRoomID() ));
		if( pRoom )
		{
			pRoom->AdujstGameResult_Score( pObj->GetGUID() , GetModfiyScore( impactObj ) );
		}
	}


	return kImpactH_Successful;
}

int MODI_ImpactT_Animation::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( !pObj )
	{
		MODI_ASSERT(0);
		return kImpactH_ParamFaild;
	}

	pObj->ClearObjStateFlag( kObjState_PlayAnimation );
	return kImpactH_Successful;
}

int MODI_ImpactT_Animation::SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( !pObj )
	{
		MODI_ASSERT(0);
		return kImpactH_ParamFaild;
	}

	MODI_GS2C_Notify_ImpactModfiyData imd;
	imd.m_Impactguid = impactObj.GetGUID();
	imd.m_ModfiyRenqi = GetModfiyRenqi( impactObj );
	imd.m_ModfiyScore = GetModfiyScore( impactObj );
	Global::logger->debug("[ImpactModify]  ------------client <name=%s,score=%f)",pObj->GetRoleName(),imd.m_ModfiyScore);
	pObj->SendOrBroadcast( &imd , sizeof(imd) );
	return kImpactH_Successful;
}

void 	MODI_ImpactT_Animation::SetModfiyScore( MODI_ImpactObj & impactObj , long lValue )
{
	impactObj.SetArgLong( kArg_ModfiyScore , lValue );
}

long 	MODI_ImpactT_Animation::GetModfiyScore( const MODI_ImpactObj & impactObj ) const
{
	long lValue = 0;
	impactObj.GetArgLong( kArg_ModfiyScore , lValue );
	return lValue;
}

void 	MODI_ImpactT_Animation::SetModfiyRenqi( MODI_ImpactObj & impactObj , long lValue )
{
	impactObj.SetArgLong( kArg_ModfiyRenqi , lValue );

}

long 	MODI_ImpactT_Animation::GetModfiyRenqi( const MODI_ImpactObj & impactObj ) const
{
	long lValue = 0;
	impactObj.GetArgLong( kArg_ModfiyRenqi , lValue );
	return lValue;
}

