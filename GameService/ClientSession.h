/** 
 * @file ClientSession.h
 * @brief 客户端会话
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */

#ifndef CLIENT_SESSION_GS_H_
#define CLIENT_SESSION_GS_H_

#include "cmdhelp.h"
#include "ServiceTask.h"
#include "GameTask.h"
#include "gw2gs.h"
#include "protocol/gamedefine.h"

class MODI_ClientAvatar;

///	客户端会话
class MODI_ClientSession
{
	enum
	{
		enInit, 			// 初始化状态
		enWaitAuth, 		// 等待客户端请求验证
		enWaitAuthResult, 	// 等待对客户端的验证结果 
		enAuthedWaitMusicHeader, 			// 已经验证通过 , 等待客户端发送音乐头
		enLogined, 							// 客户端已经发送音乐头,算作登入成功
		enRechangeChanneling, // 正在进行重新选择频道操作
		enChannelRechanged,   // 已经重新选择了频道
		enWaitAntherLogin, // 等待另外一个玩家登入
		enWaitBekick, // 等待被踢
		enWaitBekickSave,
	};

public:
    MODI_ClientSession(const MODI_SessionID & id, MODI_ServiceTask * p);
    ~MODI_ClientSession();

public:

    const MODI_SessionID & GetSessionID()
    {
        return m_SessionID;
    }

    MODI_ClientAvatar * GetClientAvatar()
    {
        return m_pClient;
    }

    void SetClientAvatar(MODI_ClientAvatar * p)
    {
        m_pClient = p;
    }

	void SetFangChenMi( bool bSet ) { m_bFangChenmi = bSet; }
	bool IsFangChenMi() const { return m_bFangChenmi; }

    bool IsVaildSessionID();

	/** 
	 * @brief 发送数据包
	 * 
	 * @param pt_null_cmd 要发送的数据包
	 * @param cmd_size 数据包大小
	 * 
	 * @return 成功返回TRUE 
	 */
    bool SendPackage(const void * pt_null_cmd, int cmd_size, bool is_zip = false);

	/** 
	 * @brief 发送数据包
	 * 
	 * @param pt_null_cmd 要发送的数据包
	 * @param cmd_size 数据包大小
	 * 
	 * @return 成功返回TRUE 
	 */
    bool SendPackageNoBuffer(const void * pt_null_cmd, int cmd_size, bool is_zip = false);
	

	MODI_ServiceTask * GetNetIO() {
		return m_pNetIO;
	}

	void 	SwitchWaitAuthStatus(); 
	void 	SwitchWaitAuthResultStatus();
	void 	SwitchAuthedWaitMusicHeaderStatus();
	void 	SwitchLoginedStatus();
	void 	SwitchRechangeChannelingStatus();
	void 	SwitchChannelRechangedStatus();
	void 	SwitchAntherLoginStatus();
	void 	SwitchWaitBekickStatus();
	void 	SwitchWaitBekickSaveStatus();

	bool 	IsInWaitAuthStatus() const;
	bool 	IsInWaitAuthResultStatus() const;
	bool 	IsInAuthedWaitMusicHeaderStatus() const;
	bool 	IsInLoginedStatus() const;
	bool 	IsInRechangeChannelingStatus() const;
	bool 	IsInChannelRechangedStatus() const;
	bool 	IsInAntherLoginStatus() const;
	bool 	IsInWaitBekickStatus() const;
	bool 	IsInWaitBekickSaveStatus() const;

	bool 	IsWaitCloseTimeout();
	bool 	IsAuthTimeOut();

	const char * GetStatusStringFormat();

private:

	int 		m_iStatus;
    MODI_SessionID m_SessionID;
    MODI_ServiceTask * m_pNetIO;
    MODI_ClientAvatar * m_pClient;
	MODI_Timer 	m_timerWaitClose;
	MODI_Timer 	m_timerAuth;
	bool 		m_bFangChenmi;
};


class 	MODI_SessionStatusHandler
{
	public:


	// 登入验证的处理
	static int 	OnHandler_LoginAuth( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size );
	
	// 选择频道的处理
	static int 	OnHandler_SelectChannel( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size );

	// 音乐头的处理
	static int 	OnHandler_MusicHeader( MODI_ClientSession * pSession , 
			const Cmd::stNullCmd * pt_null_cmd , 
            const unsigned int cmd_size );
};

#endif
