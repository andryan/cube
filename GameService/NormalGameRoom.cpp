#include "GameRoom.h"
#include "protocol/c2gs.h"
#include "GameChannel.h"
#include "GameTick.h"
#include "Share.h"
#include "BinFileMgr.h"
#include <math.h>
#include "protocol/gamedefine.h"
#include "MdmMgr.h"
#include "LoggerInfo.h"
#include "LoggerAction.h"
#include "AssertEx.h"
#include "BaseOS.h"
#include "Base/Aformula.h"
#include "FunctionTime.h"
#include "Base/GameConstant.h"
#include "ZoneClient.h"
#include "s2rdb_cmd.h"
#include "ScriptManager.h"
#include "MusicMgr.h"
#include "GameSingleMusicMgr.h"
#include "SceneManager.h"

#define   GAMESTART_TIMEOUT 1000 * 60
#define   GAMEMATCH_END_TIMEOUT  1000 * 5 
#define   SHOWTIME_CYCLE 15 * 1000 // showtime 效果时间
#define   WAIT_SPECTATORS_LISTERNOVER_TIMEOUT 1000 * 10 //  听歌超时
#define   WAIT_TOUPIAO_TIMEOUT 10 * 1000 // 投票时间
#define   MAX_VOTETO_COUNT 			1 	// 每个人最大的投票数量
#define   SHOW_GAMERESULT_TIME 		8 * 1000 // 展示游戏结果的时间
#define   BACK_ROOM_TIME 			5 * 1000 	// 回房间倒计时
#define	  SHOW_TIME_PRO			0.1f
#define	  MAX_KEY_PER_SEC		22

MODI_NormalGameRoom::MODI_NormalGameRoom( defRoomID id ):Parent(id),
	m_GameStartTimeOut( GAMESTART_TIMEOUT ),
	m_nBeginTime(0),
	m_nShowTimeCount(0),
	m_nEndTime(0),
	m_bShowTimeing( false ),
	m_ShowTimeTimer( SHOWTIME_CYCLE ),
	m_pShowTimeClient(0),
	m_pNotifyGameResult(0),
	m_TimerListernTimeout( WAIT_SPECTATORS_LISTERNOVER_TIMEOUT ),
	m_TimerToupiaoTimeout( WAIT_TOUPIAO_TIMEOUT ),
	m_TimerShowGameResult( SHOW_GAMERESULT_TIME ),
	m_TimerBacking( BACK_ROOM_TIME )
{
	m_nMaxCombat = 0;

}

MODI_NormalGameRoom::~MODI_NormalGameRoom()
{

}

//    更新房间状态掩码
void MODI_NormalGameRoom::UpdateRoomStateMask()
{
    unsigned char ucMaxPlayers = GetMaxPlayers();
    unsigned char ucCurPlayers = GetCurPlayersNum();
    unsigned char ucCurSpectators = GetCurSpectatorsNum();
    unsigned char ucCurBoys = GetCurBoysNum();
//	unsigned char ucCurGril = GetCurGirlNum();
	unsigned char ucMaxSpectators = m_nSpectatorMaxSize;
    bool bAllowSpectator = IsAllowSpectator();
	
#ifdef _DEBUG	
	Global::logger->debug("->>>>>>>>>>>>>set room <max=%u,player=%u,spec=%u,boys=%u,maxspec=%u,is=%u>",
						  (WORD)ucMaxPlayers, (WORD)ucCurPlayers,(WORD)ucCurSpectators,(WORD)ucCurBoys,(WORD)ucMaxSpectators,(WORD)bAllowSpectator);
#endif
	
	defRoomStateMask nSet =  RoomHelpFuns::EncodeRoomState(ucMaxPlayers, ucCurPlayers,
            ucMaxSpectators, ucCurSpectators, ucCurBoys, !bAllowSpectator);

    // 更新房间状态掩码
	SetStateMask( nSet );
#ifdef _DEBUG	
	Global::logger->debug("->>>>>>>>>>>>>set room <%llu>", nSet);
#endif	
}

bool MODI_NormalGameRoom::OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator )
{
	if( !Parent::OnCreate( pClient , musicid , mapid , nPlayMax , nSpectorMax , bAllowSpectator )	)
		return false;

	//    所有位置都重置
	unsigned int n = 0;
	for (n = 0; n < MAX_PLAYSLOT_INROOM; n++)
	{
		m_PlayersGameResults[n].Reset();
	}

	SetMaster( pClient->GetGUID() );
	m_nEnterCounter = 0;
	MODI_NormalRoomInfo & detailNormalRoom = m_info.m_detailNormalRoom;
	detailNormalRoom.m_nMusicId = musicid;//音乐ID
	detailNormalRoom.m_nMapId = mapid;//场景ID
	detailNormalRoom.m_nStateMask = 0;//位置状态
	detailNormalRoom.m_flag = kMusicFlag_Yuanchang;
	detailNormalRoom.m_timeType = kMusicTime_All;

	// 随机音乐ID 
	if( musicid == RANDOM_MUSIC_ID )
	{
		GetNormalInfo_Modfiy().m_bRandomMusic = true;
		GetNormalInfo_Modfiy().m_nMusicId = RANDOM_MUSIC_ID;
	}
	
	// 随机地图 
	if( mapid == RANDOM_MAP_ID )
	{
		GetNormalInfo_Modfiy().m_bRandomMap = true;
		GetNormalInfo_Modfiy().m_nMapId = RANDOM_MAP_ID;
	}

	m_iStatus = ROOM_STATUS_USING;

	return true;
}

bool MODI_NormalGameRoom::OnDestroy()
{

	return true;
}

void MODI_NormalGameRoom::SetAllowSpectator( bool bSet )
{
//	bool bOld = m_bOffSpectator;
	Parent::SetAllowSpectator( bSet );
//	bool bNew = !IsAllowSpectator();
	{
		UpdateRoomStateMask();
	}
}

bool MODI_NormalGameRoom::EnterRoom(MODI_ClientAvatar * pClient)
{
	MODI_Scene * p_scene = GetRoomScene();
	if(!p_scene)
	{
		MODI_ASSERT(0);
		return false;
	}

	if( !Parent::EnterRoom( pClient ) )
		return false;

    // 给该客户端分配一个位置
    bool bIsPlayer;
    defRoomPosSolt n;
	if(GetFreePosSolt( bIsPlayer , n )  == false )
	{
		MODI_ASSERT( "MODI_NormalGameRoom::EnterRoom can't get posSolt. " && 0 );
		return false;
	}

	MODI_ClientAvatarInRoom * pTemp = GetClientInRoomByPos(bIsPlayer, n);
	if( !pTemp )
		return false;

    MODI_ClientAvatarInRoom & clientInRoom = *pTemp;

	// 给这个位置设置信息
    m_nEnterCounter += 1;
    clientInRoom.Reset();
    clientInRoom.m_nEnterCounter = m_nEnterCounter;
    clientInRoom.m_pClient = pClient;
	defRoomPosSolt nPosInRoom =  RoleHelpFuns::EncodePosSolt(bIsPlayer, n); // 所在位置
	pClient->OnEnterRoom( *this , nPosInRoom );

	//     更新房间状态（大厅中需要显示）
	UpdateRoomStateMask();

	clientInRoom.m_iEnterRoomStatus = m_iStatus;
	
	if( !IsPlaying() )
	{
		// 成功进入房间
		if( GetCurClientsNum() == 1 )
		{
			//  成功创建房间
			MODI_C2GS_Response_CreateRoom res;
			res.m_roomInfo = GetRoomInfo();
#ifdef _DEBUG
			Global::logger->debug("[send_scene_id] <id=%u>", res.m_roomInfo.m_wdSceneId);
#endif			
			res.m_selfPos = pClient->GetRoomSlot();
			pClient->SendPackage(&res,sizeof(res));

			// 初始化玩家位置
			//InitPostion( pClient );
			p_scene->UpdateAttr(pClient);
		}
		else 
		{
			MODI_GS2C_Notify_JoinRoomSuccessful res;
			res.master = GetMaster();
			res.m_roomid = GetRoomID();
			res.m_selfPos = pClient->GetRoomSlot();
			res.m_bIsPlaying = IsPlaying();
			pClient->SendPackage(&res,sizeof(res));

			// 初始化玩家位置
			//InitPostion( pClient );
			// 将自己的信息发送给其他客户端
			SendRoleInfoToClients( pClient );
			// 将其他客户端的信息发给自己
			SendRolesInfoToClient( pClient );

			p_scene->UpdateAttr(pClient);
		}

		SyncPosionState();

		SyncReadyState();

		// 同步表情
		SyncExpression( pClient );

		//  为没有歌手的旁观者分配歌手
		AllocSongerForNoOBSpector();

		pClient->SetGameState( kInRoomWaiting );
	/// 如果是组队模式通知大家组队情况
	if(GetSubType() == ROOM_STYPE_TEAM  || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		if(bIsPlayer)
		{
			SetTeam(n);
		}
		
		BroadcastTeamState();
	}
	}
	else 
	{
		// 初始化玩家位置
		//InitPostion( pClient );
		
		/*  一定要注意次序... */
		// 将自己的信息发送给其他客户端
		SendRoleInfoToClients( pClient );
		// 将其他客户端的信息发给自己
		SendRolesInfoToClient( pClient );

		p_scene->UpdateAttr(pClient);
		
		// 同步效果
		SyncImpacts( pClient );
		// 同步表情
		SyncExpression( pClient );


		MODI_GS2C_Notify_ReadyPlay ready_msg;
		ready_msg.m_roomid = GetRoomID();
		ready_msg.m_musicid = GetMusicID();
		ready_msg.m_songType = GetSongType();
		ready_msg.m_flag = GetMusicFlag();
		ready_msg.m_mapid = GetMapID();
		ready_msg.m_mt = GetNormalInfo().m_timeType;

		pClient->SendPackage( &ready_msg , sizeof(ready_msg) );

		MODI_GS2C_Notify_JoinRoomSuccessful res;
		res.master = GetMaster();
		res.m_roomid = GetRoomID();
		res.m_selfPos = pClient->GetRoomSlot();
		res.m_bIsPlaying = IsPlaying();
		pClient->SendPackage(&res,sizeof(res));

		AllocSongerForNoOBSpector();
//		MODI_GS2C_Notify_SentenceScore notify_ss;
//		notify_ss.m_byEffect
		//		SendMdmToClient( pClient );
		
		// 如果唱歌的人已经常完，忽略该位置
		
		
		if( IsVoting() )
		{
			//  后面进来的，同步投票结果

			SyncVoteState( pClient );
		} 
		
		pClient->SetGameState( kInRoomPlaying );
		/// 如果是组队模式通知大家组队情况
		if(GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM )
		{
			if(bIsPlayer)
			{
				SetTeam(n);
			}
			
			BroadcastTeamState();
		}
		if( m_iStatus >= ROOM_STATUS_SONGEROVER )
		{
			clientInRoom.m_bListernOver = true;
			int iSendSize = m_pNotifyGameResult->m_bySize * sizeof(MODI_GameResult) + sizeof(MODI_GS2C_Notify_GameResult);
			pClient->SendPackage( m_pNotifyGameResult , iSendSize );
		}
	}


	///tmp_begin	一下是一段临时代码
	for( n=0;  n < m_nPlayerMaxSize; n++)
	{
		if( m_Players[n].m_pClient )
		{
#ifdef	_DEBUG
			Global::logger->debug("[enter_room_script_exe] it time to execute script for the client %s",m_Players[n].m_pClient->GetRoleName());
#endif
			///	如果该位置有人的话，那么就全部执行一下脚本
			MODI_TriggerEnter  script(3);
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(m_Players[n].m_pClient,script);
		}
	}
	/// tmp_end
	
#ifdef	_DEBUG
			Global::logger->debug("[enter_room_script_exe]  client %s enter room end",pClient->GetRoleName());
#endif
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	pLobby->LeaveLobby(pClient->GetGUID());
    return true;
}

// 获得新房主
MODI_ClientAvatar * MODI_NormalGameRoom::GetNewMaster()
{
	MODI_ClientAvatar * avatar = NULL;

	/// 先不限制
#ifdef _ML_VERSION
#else	
	if(GetRoomType() == ROOM_TYPE_VIP)
	{
		///	首先找到适合当房主的人
		unsigned int i=0;
		for( i=0;   i< m_nPlayerMaxSize ; ++i)
		{
			if (m_Players[i].m_pClient &&  AVATAR_CAN( m_Players[i].m_pClient ,PREVILEGE_ROOMOWNER)) 
			{
				avatar = m_Players[i].m_pClient;
				if(avatar)
				{
					if(avatar->IsVip())
						return avatar;
				}
			}
		}
		
		for( i=0; i< m_nSpectatorMaxSize ; ++i )
		{
			if( m_Spectators[i].m_pClient && AVATAR_CAN( m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				avatar = m_Spectators[i].m_pClient;
				if(avatar)
				{
					if(avatar->IsVip())
					{
						return avatar;
					}
				}
			}
		}
	}
	else
#endif		
	{
		///	首先找到适合当房主的人
		unsigned int i=0;
		for( i=0;   i< m_nPlayerMaxSize ; ++i)
		{
			if (m_Players[i].m_pClient &&  AVATAR_CAN( m_Players[i].m_pClient ,PREVILEGE_ROOMOWNER)) 
			{
				avatar = m_Players[i].m_pClient;
				return avatar;
			}
		}
		
		for( i=0; i< m_nSpectatorMaxSize ; ++i )
		{
			if( m_Spectators[i].m_pClient && AVATAR_CAN( m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				return m_Spectators[i].m_pClient;
			}
		}
	}

	return NULL;

	// 先进入的人成为新房主
    MODI_ClientAvatarInRoom * pPlayer = GetEarliestEnterInPlayers();
    MODI_ClientAvatarInRoom * pSpectator = GetEarliestEnterInSpectators();
    if (pPlayer && pSpectator)
    {
        if (pPlayer->m_nEnterCounter < pSpectator->m_nEnterCounter)
            return pPlayer->m_pClient;
        return pSpectator->m_pClient;
    }
    else if (pPlayer)
    {
        return pPlayer->m_pClient;
    }
    else if (pSpectator)
    {
        return pSpectator->m_pClient;
    }

    return 0;
}



void MODI_NormalGameRoom::OnAllSongerHalfExit()
{
	// 结束SHOW TIME 状态
	EndShowTime();

	// 计算分数
	ComputeGameResult();

	this->SendGameResult( true , true );

	// 游戏结束
	MODI_GS2C_Notify_GameMatchEnd 	msg;
	msg.m_bBackRoom = false;
	msg.m_nTime = 0;
	Broadcast(&msg, sizeof(msg));

//	OnGameMatchEnd( GAMEMATCHEND_TYPE_HALFWAY  );

}

void MODI_NormalGameRoom::OnAllSongerOver()
{
	// 结束SHOW TIME 状态
	EndShowTime();

	// 计算分数
	ComputeGameResult();
	m_iStatus = ROOM_STATUS_SONGEROVER;

	this->SendGameResult( true , false );

	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_TimerListernTimeout.Reload( WAIT_SPECTATORS_LISTERNOVER_TIMEOUT , pTick->GetTimer() );
}
	
void MODI_NormalGameRoom::OnAllSpectarOver()
{
	this->SendGameResult( false , true );

	// 开始投票
	MODI_GS2C_Notify_BeginVote msg;
	msg.m_nVoteTime = WAIT_TOUPIAO_TIMEOUT;
	this->Broadcast( &msg , sizeof(msg) );

	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_TimerToupiaoTimeout.Reload( msg.m_nVoteTime , pTick->GetTimer() );

	m_iStatus = ROOM_STATUS_VOTE;
}

void MODI_NormalGameRoom::GameEndReset()
{
	unsigned int n = 0;

	// 让网关删除全部的转发
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		if( m_Players[n].m_pClient )
		{
			NotifyDelTransMission( m_Players[n].m_pClient->GetSessionID() , 0 , 0 );
		}
	}

	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	MODI_Scene * p_scene = GetRoomScene();
	if(p_scene)
	{
		p_scene->SyncAllPos();
	}
	else
	{
		MODI_ASSERT(0);
	}
			
	// 同步角色信息
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		m_Players[n].m_bReadyInGame = false;
		m_Players[n].m_bReadyInRoom = false;
		m_Players[n].m_byVoteToCount = 0;
		m_Players[n].m_byVoteCount = 0;

		if( m_Players[n].m_pClient )
		{
			//   同步大厅中的玩家列表
			pLobby->SendPlayerList( m_Players[n].m_pClient );
#if 0			
			//  同步位置
			m_Players[n].m_pClient->IncLogicCount();
			MODI_GS2C_Notify_AdjustPostion msg;
			msg.m_obj = m_Players[n].m_pClient->GetGUID();
			msg.m_fPosX  = m_Players[n].m_pClient->GetPosX();
			msg.m_fPosZ = m_Players[n].m_pClient->GetPosZ();
			msg.m_fDir = m_Players[n].m_pClient->GetDir();
			msg.m_nServerLogicCount = m_Players[n].m_pClient->GetLogicCount();
			Broadcast( &msg , sizeof(msg) );
#endif

//			MODI_GS2C_Notify_RoleInfo notify;
//			notify.m_baseInfo = m_Players[n].m_pClient->GetRoleBaseInfo();
//			notify.m_normalInfo = m_Players[n].m_pClient->GetRoleNormalInfo();
//			Broadcast(&notify, sizeof(notify));
		}
	}

	for( n = 0; n < m_nSpectatorMaxSize; n++ )
	{
		m_Spectators[n].m_bReadyInGame = false;
		m_Spectators[n].m_bReadyInRoom = false;
		m_Spectators[n].m_byVoteToCount = 0;
		m_Spectators[n].m_byVoteCount = 0;

		if( m_Spectators[n].m_pClient )
		{
			//   同步大厅中的玩家列表
			pLobby->SendPlayerList( m_Spectators[n].m_pClient );
#if 0			
			//  同步位置
			m_Spectators[n].m_pClient->IncLogicCount();
			MODI_GS2C_Notify_AdjustPostion msg;
			msg.m_obj = m_Spectators[n].m_pClient->GetGUID();
			msg.m_fPosX  = m_Spectators[n].m_pClient->GetPosX();
			msg.m_fPosZ = m_Spectators[n].m_pClient->GetPosZ();
			msg.m_fDir = m_Spectators[n].m_pClient->GetDir();
			msg.m_nServerLogicCount = m_Spectators[n].m_pClient->GetLogicCount();
			Broadcast( &msg , sizeof(msg) );
#endif

//			MODI_GS2C_Notify_RoleInfo notify;
//			notify.m_baseInfo = m_Spectators[n].m_pClient->GetRoleBaseInfo();
//			notify.m_normalInfo = m_Spectators[n].m_pClient->GetRoleNormalInfo();
//			Broadcast(&notify,sizeof(notify));
		}
	}

	
	SyncPosionState();

	SyncReadyState();

	GetRoomInfo_Modfiy().m_bPlaying = false;
}

void MODI_NormalGameRoom::OnVoteEnd()
{
	// 展示游戏结果
	MODI_GS2C_Notify_ShowGameResult 	res;
	res.m_nShowTime = SHOW_GAMERESULT_TIME; // 游戏结果展示时间
	res.m_renqiKing = GetRenqiKing(); // 人气王
	Broadcast( &res , sizeof(res) );

	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pClient = pChannel->FindClientByID( res.m_renqiKing );
	if( pClient )
	{
		Global::logger->debug("[%s] renqiKing is <charid=%u,name=%s> ." , SVR_TEST ,
				pClient->GetCharID() , pClient->GetRoleName() );
	}

	// 进入展示游戏结果状态
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_TimerShowGameResult.Reload(  SHOW_GAMERESULT_TIME , pTick->GetTimer() );
	m_iStatus = ROOM_STATUS_SHOWVOTERESULT;
}

void MODI_NormalGameRoom::OnShowGameResultEnd()
{
	// 游戏结束
	MODI_GS2C_Notify_GameMatchEnd 	msg;
	msg.m_bBackRoom = false;
	msg.m_nTime = BACK_ROOM_TIME;
	Broadcast(&msg, sizeof(msg));
	// 倒计时
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_TimerBacking.Reload(  BACK_ROOM_TIME , pTick->GetTimer() );
	m_iStatus = ROOM_STATUS_BACKING;
}

//    游戏比赛结束的处理
void MODI_NormalGameRoom::OnGameMatchEnd(int iEndType,bool force)
{
	// 修改历史票数

	// 修改人气值

	// 重置
	
	// 改变房间状态
	m_iStatus = ROOM_STATUS_USING;
	
	for( DWORD i = 0; i < m_nPlayerMaxSize; i++ )
	{
		if( m_Players[i].m_pClient )
		{
			m_Players[i].m_pClient->OnEndGame();
		}
	}

	for( DWORD i = 0; i < m_nSpectatorMaxSize; i++ )
	{
		if( m_Spectators[i].m_pClient )
		{
			m_Spectators[i].m_pClient->OnEndGame();
		}
	}

	MODI_GS2C_Notify_GameMatchEnd 	msg;
	msg.m_bBackRoom = true;
	msg.m_nTime = 0;
	Broadcast(&msg, sizeof(msg));
	
	GameEndReset();
	
	SyncPosionState();
	
	if( force)
	{
		MODI_GS2C_Notify_MusicChanged change;
		change.m_bRandom = IsRandomMusic();
		change.m_flag = GetMusicFlag();
		change.m_mt =GetMusicTimeType();
		change.m_musicID = GetMusicID();
		change.m_songType = GetSongType();
		Broadcast( &change,sizeof( MODI_GS2C_Notify_MusicChanged));

		MODI_GS2C_Notify_MapChanged  map;
		if( IsRandomMap())
		{
			map.m_MapID = RANDOM_MAP_ID;
		}
		else
		{
			map.m_MapID = GetMapID();	
		}

		map.m_bRamdom = GetNormalInfo_Modfiy().m_bRandomMap;
		Broadcast( &map,sizeof( MODI_GS2C_Notify_MapChanged));
	}

	/// 如果是组队模式通知大家组队情况
	unsigned int n=0;
	if(GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM )
	{
		for( n = 0; n< m_nPlayerMaxSize; n++)
		{

			if( ! m_Players[n].m_pClient)
			{
				m_byTeamState[n] = 0;
			}
		}	
		MODI_GS2C_Notify_RoomTeam_State send_cmd;
		send_cmd.m_byState[0] = (BYTE)m_byTeamState[0];
		send_cmd.m_byState[1] = (BYTE)m_byTeamState[1];
		send_cmd.m_byState[2] = (BYTE)m_byTeamState[2];
		send_cmd.m_byState[3] = (BYTE)m_byTeamState[3];
		send_cmd.m_byState[4] = (BYTE)m_byTeamState[4];
		send_cmd.m_byState[5] = (BYTE)m_byTeamState[5];
		Broadcast( &send_cmd, sizeof(send_cmd) );
	}
	///tmp_begin	一下是一段临时代码
	for( n=0;  n < m_nPlayerMaxSize; n++)
	{
		if( m_Players[n].m_pClient )
		{
			///	如果该位置有人的话，那么就全部执行一下脚本
			MODI_TriggerEnter  script(2);
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(m_Players[n].m_pClient,script);
		}
	}
	/// tmp_end
	
	/// 是时候让称号低的人离开房间了
		
	if( !HasPreOwnerExcept(NULL) )
	{
		KillAllExcept( NULL);
	}	
	///有可低称号的人是房主
	
	ReFixOwner();	
}

bool MODI_NormalGameRoom::LeaveRoom(MODI_ClientAvatar * pClient, int iReason)
{
	static std::string strDebugs[] = { "" ,  
		"kick" , 
		"left" , 
		"not ready, timeout" , 
		"quit game" , 
		"another login,be kick" , 
		"rechange chanel" ,
		"chenghao" ,
									   "vipleaveroom",									   
	};

	Global::logger->debug("[%s] player<%s> trying to leave room<%u> : reason <%s>",
			GS_ROOMOPT ,pClient->GetRoleName(),GetRoomID(),strDebugs[iReason].c_str());

	bool bMaster = pClient->GetGUID() == GetMaster();
	bool bIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家

	/// 如果没有其他有权限当房主的话，那么房间销毁，没有在游戏中
	if( bMaster && ROOM_STATUS_USING == m_iStatus  &&  ! HasPreOwnerExcept( pClient)  )
	{
		KillAllExcept( pClient);
	}	

	bool bFind = RemoveClient(pClient);
	if (!bFind)
	{
		MODI_ASSERT(0);
		return false;
	}

	DelISpeakId(pClient->GetGUID());
	DelYYChatState(pClient->GetGUID());

	// 对客户端，只通知大概原因
/*	if( iReason  > enLeaveReason_ExitGame )
		iReason = enLeaveReason_ExitGame;
		*/


	if( pClient->GetRoleNormalInfo().m_SingerID != INVAILD_GUID )
	{
		MODI_ClientAvatar * pSonger = FindPlayerByGUID( pClient->GetRoleNormalInfo().m_SingerID );
		if( pSonger )
		{
			MODI_SessionID  id = pClient->GetSessionID();
			NotifyDelTransMission( pSonger->GetSessionID() , &id , 1 );
		}
	}

	if( bMaster )
	{
		SetPassword(0);

		MODI_GS2C_Notify_RoomInfoChanged notify;
		memcpy( notify.m_szRoomName , GetRoomInfo().m_cstrRoomName , sizeof(notify.m_szRoomName) );
		notify.m_bHasPwd = false;
		notify.m_ucNewOffSpectator = IsAllowSpectator();
		Broadcast( &notify , sizeof(notify) );
	}

	if( bIsPlayer && ( GetSubType() == ROOM_STYPE_TEAM  || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM))
	{
		if(! IsPlaying()) 
		{
			defRoomPosSolt pos = pClient->GetPostionInRoom();
			if(pos > MAX_PLAYSLOT_INROOM)
			{
				Global::logger->debug("--->>>>%u-->>>", pos);
				MODI_ASSERT(0);
			}
			else
			{
				m_byTeamState[pos] = 0;
			}
			BroadcastTeamState();
		}
	}
	

	pClient->OnLeaveRoom( *this );
	/// 离开场景
	MODI_Scene * p_scene = GetRoomScene();
	if(p_scene)
	{
		p_scene->RemoveAttr(pClient);
	}
	else
	{
		MODI_ASSERT(0);
	}

	AllocSongerForNoOBSpector();

	UpdateRoomStateMask();

	bool bDestory = false;
	if (GetCurClientsNum() == 0)
	{
		MODI_GS2C_Notify_LeaveRoom notify;
		notify.m_objID = pClient->GetGUID();
		notify.m_byLeaveReason = iReason;

		pClient->SendPackage( &notify , sizeof(notify) );

		bDestory = true;
	}
	else
	{
		// 通知给其他玩家,有人离开房间
		MODI_GS2C_Notify_LeaveRoom notify;
		notify.m_objID = pClient->GetGUID();
		notify.m_byLeaveReason = iReason;
		this->Broadcast(&notify, sizeof(notify));
		pClient->SendPackage( &notify , sizeof(notify) );

		if( IsStarting() )
		{
			// 各玩家资源装在过程中有人退出房间，退出之后要尝试开始游戏
			TryStartGame();
		}

		if( bIsPlayer )
		{
			if( IsPlaying() || IsStarting()) 
			{
				pClient->IncLoss();

				if( GetCurPlayersNum() == 0 )
				{
					Global::logger->debug("[%s] player<%s>trying to leave room<%u> : he/she is last player remain, end game.", 
							GS_ROOMOPT ,
							pClient->GetRoleName() , 
							GetRoomID() );
					OnClientExitPlayerState( pClient->GetGUID() );

					OnAllSongerHalfExit();

					// 如果没有人在唱歌了，则直接结束游戏，回到等待房间界面
					OnGameMatchEnd( GAMEMATCHEND_TYPE_HALFWAY ,true );
				}
				else 
				{
					// show time 中退出，结束show time 状态
					if( pClient == m_pShowTimeClient )
					{
						EndShowTime();
					}
				}
			}
		}

		if (bMaster)
		{
			//    更换房主
		    MODI_ClientAvatar * pNewMaster = GetNewMaster();
			if(pNewMaster)
			{
				SetMaster( pNewMaster->GetGUID() );
				OnMasterChanged( pNewMaster );
				Global::logger->debug("[%s] player<%s>trying to leave room<%u> : allocate new room master <%s>.", 
						GS_ROOMOPT ,
						pClient->GetRoleName() , 
						GetRoomID() , 
						pNewMaster->GetRoleName() );
			}
		}

		Global::logger->debug("[%s] player<%s>trying to leave room<%u> : successful.", GS_ROOMOPT ,
			pClient->GetRoleName() , GetRoomID() );
	}

	// 从房间会到大厅
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	if( pLobby )
	{
		pLobby->EnterLobby( pClient );
	}

	if( bDestory )
	{
		//  房间没人，销毁房间
		MODI_GameLobby::GetInstancePtr()->OnDestroyRoom(this->GetRoomID());
	}

    return true;
}



// 房间是否已满
bool MODI_NormalGameRoom::IsFull()
{
	if( IsPlaying() )
	{
		// 游戏中的处理
		if( IsAllowSpectator() )
		{
			// 观战位置还有空位，则房间未满
			if( GetFreeSpectatorPosSolt() !=  INVAILD_POSSOLT )
				return false;
			return true;
		}
		// 不能观战，则算是房间已满
		return true;
	}

	// 游戏未开始
	if( IsAllowSpectator() )
	{
		bool bIsPlayer = false;
		defRoomPosSolt nPos = INVAILD_POSSOLT;
		if( GetFreePosSolt( bIsPlayer , nPos ) )
			return false;
	}
	else 
	{
		// 无法观战，则看参战位置是否还有空位
		if( GetFreePlayersPosSolt() != INVAILD_POSSOLT )
			return false;
	}

	return true;
}


void MODI_NormalGameRoom::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_GameTick * pGameTick = MODI_GameTick::GetInstancePtr();
    if (m_iStatus == ROOM_STATUS_START)
    {
        if (m_GameStartTimeOut( pGameTick->GetTimer() ))
        {
            // 客户端准备就绪超过时间限制,踢掉没有准备就绪的人
            unsigned int n = 0;
            for (n = 0; n < m_nPlayerMaxSize; n++)
            {
                if (m_Players[n].m_pClient && m_Players[n].m_bReadyInGame == false)
                {
#ifdef	_DEBUG
		Global::logger->debug("[FirstHalf] player %s  will be killed ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",m_Players[n].m_pClient->GetRoleName());
#endif
                    LeaveRoom(m_Players[n].m_pClient, enLeaveReason_GameStartTimeout);
                }
            }
            for (n = 0; n < m_nSpectatorMaxSize; n++)
            {
                if (m_Spectators[n].m_pClient && m_Spectators[n].m_bReadyInGame == false)
                {
#ifdef  _DEBUG
			Global::logger->debug("[FirstHalf] Spector %s will be killed ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",m_Spectators[n].m_pClient->GetRoleName());
#endif
                    LeaveRoom(m_Spectators[n].m_pClient, enLeaveReason_GameStartTimeout);
                }
            }
            TryStartGame();
        }
    }
	else if( m_iStatus == ROOM_STATUS_PLAYING )
	{
		if( pGameTick->GetTimer().GetMSec() > m_nEndTime )
		{
			// 游戏结束
			if( IsAllSongOver() )
			{
				OnAllSongerOver();
			}
			else 
			{
				// 游戏结束时间超时很久,还有人没唱玩的处理
				unsigned int nTimeOut = pGameTick->GetTimer().GetMSec() - m_nEndTime;
				if( nTimeOut > GAMEMATCH_END_TIMEOUT )
				{
					OnAllSongerOver();
				}
			}
		}
	
		if( m_bShowTimeing )
		{
			if( m_ShowTimeTimer( pGameTick->GetTimer() ) )
			{
				EndShowTime();
			}
		}	
	}
	else if( m_iStatus == ROOM_STATUS_SONGEROVER )
	{
		if( IsAllSpectarorListernOver() )
		{
			OnAllSpectarOver();
		}
		else if( m_TimerListernTimeout( pGameTick->GetTimer() ) )
		{
			OnAllSpectarOver();
		}
	}
	else if( m_iStatus == ROOM_STATUS_VOTE )
	{
		if( m_TimerToupiaoTimeout( pGameTick->GetTimer() ) )
		{
			OnVoteEnd();
		}
	}
	else if( m_iStatus == ROOM_STATUS_SHOWVOTERESULT )
	{
		if( m_TimerShowGameResult( pGameTick->GetTimer() ) )
		{
			OnShowGameResultEnd();
		}
	}
	else if( m_iStatus == ROOM_STATUS_BACKING )
	{
		if( m_TimerBacking( pGameTick->GetTimer() ) )
		{
			OnGameMatchEnd( GAMEMATCHEND_TYPE_NORMAL,true );
		}
	}
}


//    获得一个空的位置
//    是否可以开始游戏
bool MODI_NormalGameRoom::IsCanStart() 
{
    if (IsStart())
        return false;

	if( !GetCurPlayersNum() )
	{
		return false;
	}

	// 所有参战者是否都准备就绪
    MODI_GUID masterGUID = this->GetMaster();
    for (DWORD i = 0; i < m_nPlayerMaxSize; i++)
    {
        if (m_Players[i].m_pClient && m_Players[i].m_pClient->GetGUID() != masterGUID
                && !m_Players[i].m_bReadyInRoom)
        {
            return false;
        }
    }
    //组队判断，是否可以开始，人数相等
	if(GetSubType() == ROOM_STYPE_TEAM  || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		unsigned short iRed=0;
		unsigned short	iYellow=0;
		unsigned short	iBlue=0;

		for( unsigned char i=0; i< m_nPlayerMaxSize; i++)
		{
			switch( m_byTeamState[i] )
			{
				case ROOM_TEAM_RED:
					iRed++;
					break;
				case ROOM_TEAM_YELLOW:
					iYellow++;
					break;
				case ROOM_TEAM_BLUE:
					iBlue++;
					break;

				default:
					break;

			}
		}

		unsigned short icolorSum = iRed + iYellow + iBlue;

		if ( icolorSum == 0)
		{
		 	return false;
		}
		else if ( iRed == iYellow && iYellow == iBlue)
		{
			return  true;
		}
		else if ( iRed == 0 || iYellow == 0 || iBlue == 0)
		{
			if ( iRed+iYellow == 0 || iYellow+iBlue == 0 || iBlue+iRed == 0 )
			{
				return  false;
			}
			else if (icolorSum%2 == 0 && ( iRed == iYellow || iYellow == iBlue || iBlue == iRed))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

															
	}
    return true;
}

//    尝试开始游戏
bool MODI_NormalGameRoom::Start()
{
    if (IsCanStart())
    {
		/// 等待场景所有人停止走路
// 		MODI_Scene * p_scene = GetRoomScene();
// 		if(p_scene)
// 		{
// 			p_scene->AllAttrStop();
// 		}
// 		else
// 		{
// 			MODI_ASSERT(0);
// 		}
		
        //  可以开始游戏
		MODI_GS2C_Notify_ReadyPlay msg;

		

		if( IsRandomMap() )
		{
			GetNormalInfo_Modfiy().m_nMapId = GetRandomMapID();
		}


		if( IsRandomMusic() )
		{
			/// INA版本
			if(Global::g_byGameShop == 1)
			{
				GetNormalInfo_Modfiy().m_nMusicId = RANDOM_MUSIC_ID;
				GetNormalInfo_Modfiy().m_nMusicId = MODI_MusicMgr::GetInstancePtr()->GetRandomAom();
				defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
				const GameTable::MODI_Musiclist *pMusicInfo = musicBin.GetByIndex(GetNormalInfo_Modfiy().m_nMusicId);
				if( pMusicInfo )
				{
					if(pMusicInfo->get_accompaniment())
					{
						GetNormalInfo_Modfiy().m_flag = kMusicFlag_Banzou;
					}
					else
					{
						GetNormalInfo_Modfiy().m_flag = kMusicFlag_Yuanchang;
					}
				}
			}
			else
			{
				GetNormalInfo_Modfiy().m_nMusicId = RANDOM_MUSIC_ID;
				//首先获得的就是隐藏概率
				if( CanGetHiden())
				{
					GetNormalInfo_Modfiy().m_nMusicId = MODI_MusicMgr::GetInstancePtr()->GetRandomHideID();
					GetNormalInfo_Modfiy().m_songType = kMusicSongType_Hide;
#ifdef _DEBUG
					if( GetNormalInfo_Modfiy().m_nMusicId == RANDOM_MUSIC_ID)
					{
						Global::logger->debug("_DDDDDDDDDDDDDDDDDDDDDD can't get RANDOM Hiden Music  will get normal");
					}
					else
					{
						Global::logger->debug("_DDDDDDDDDDDDDDDDDDDDDD  get RANDOM Hiden Music  will get normal %u",GetNormalInfo_Modfiy().m_nMusicId);
					}

#endif
				}
				if( GetNormalInfo_Modfiy().m_nMusicId == RANDOM_MUSIC_ID)
				{
					GetNormalInfo_Modfiy().m_nMusicId = GetRandomMusicID();
					GetNormalInfo_Modfiy().m_songType = GetRandomSongType( GetMusicID() );
				}
				GetNormalInfo_Modfiy().m_flag = GetRandomMusicFlag( GetMusicID() );
			}
		}
		/// 随机音乐结束
		if( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
		{
			#ifdef _DEBUG
				if( GetMusicFlag() == kMusicFlag_Banzou)
				{
					Global::logger->debug("_DDDDDDDDDDDDDDDDDDDDDD  key board mode  get a   BANZOUGEQU DDDDDDDDDDDDDDDD_____ %u",GetNormalInfo_Modfiy().m_nMusicId);
				}
			#endif
				/// 	应策划要求，任何歌曲只要是键盘模式 就必须是原唱的
			GetNormalInfo_Modfiy().m_flag = kMusicFlag_Yuanchang;
		}

		msg.m_mapid = GetMapID();
		msg.m_musicid = GetMusicID();
		msg.m_flag = GetMusicFlag();
		msg.m_songType = GetSongType();
		msg.m_roomid = GetRoomID();
		msg.m_mt = GetMusicTimeType();

		Global::logger->debug("[random music] broadcast to everyone in room mapid=%u,musicid=%u,flag=%u,songtype=%u,musictimetype=%u<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>",msg.m_mapid,msg.m_musicid,msg.m_flag,msg.m_songType,msg.m_roomid,msg.m_mt);

		Broadcast( &msg , sizeof(msg) );

		AllocSongerForNoOBSpector();
		///	发送给旁听人音乐头
		MODI_GS2C_Notify_MusicHeaderData send;
		for( unsigned int n = 0; n < m_nSpectatorMaxSize; n++ )
		{
			if( m_Spectators[n].m_pClient )
			{

				MODI_GUID tmp=m_Spectators[n].m_pClient->GetRoleNormalInfo().m_SingerID ;
				send.m_nSongerGUID=tmp;
				send.m_nSize=1;
				m_Spectators[n].m_pClient->SendPackage( &send,sizeof(send)+1);

			}
		}
		MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
        //  超时设置
        m_GameStartTimeOut.Reload(GAMESTART_TIMEOUT, 
				pTick->GetTimer());

        //   改变状态
        m_iStatus = ROOM_STATUS_START;

		GetRoomInfo_Modfiy().m_bPlaying = true;

        return true;
    }
    return false;
}


//    改变准备状态(只有参战玩家能够准备/取消准备)
void MODI_NormalGameRoom::ChangeReady(MODI_ClientAvatar * pClient, bool bReady)
{
    bool bIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家
	if( !bIsPlayer )
	{
		Global::logger->debug("[%s] room<%u> player<%s> ready status update to <%s> : \
				player is spectator, cannot switch status. ", 
				GS_ROOMOPT ,
				GetRoomID() , 
				pClient->GetRoleName() , 
				bReady ? "Ready" : "Unready" );
		return ;
	}

    defRoomPosSolt nPos = RoleHelpFuns::GetPosSolt(pClient->GetRoleNormalInfo().m_roomSolt); // 位置索引
    MODI_ClientAvatarInRoom & client = *GetClientInRoomByPos(true, nPos);
	MODI_ASSERT( client.m_pClient == pClient );
	if( client.m_pClient == pClient && client.m_bReadyInRoom != bReady )
	{
		client.m_bReadyInRoom = bReady;
		// 有人状态改变了
		MODI_GS2C_Notify_SbReadyStateChanged notify;
		notify.m_nClientGUID = pClient->GetGUID();
		notify.m_byReady = bReady ? 1 : 0;		
		Broadcast( &notify , sizeof(notify) );

		Global::logger->debug("[%s] room<%u> player<%s> ready status update to <%s> : successful. ", 
				GS_ROOMOPT ,
				GetRoomID() , 
				pClient->GetRoleName() , 
				bReady ? "Ready" : "Unready" );
	}
}

bool MODI_NormalGameRoom::InCanLeaveRoom(MODI_ClientAvatar * pClient)
{
	// 参战玩家开始之后不让退出
	if( IsStart() && pClient->IsPlayer() )
	{
		return false;
	}

	return true;
}

//    某个参战位置上的角色是否准备好
bool MODI_NormalGameRoom::IsReady(defRoomPosSolt nPos)
{
    MODI_ClientAvatarInRoom & client = *GetClientInRoomByPos(true, nPos);
	if( client.m_pClient )
		return client.m_bReadyInRoom;
	return false;
}

//    开位置
bool MODI_NormalGameRoom::OnPostion(MODI_ClientAvatar * pClient  , bool bPlayer, defRoomPosSolt nPos)
{
    if (bPlayer && nPos >= m_nPlayerMaxSize)
	{
		Global::logger->debug("[%s] room<%u> player<%s> player on <%d> : invalid pos index. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
		return false;
	}
    else if (nPos >= m_nSpectatorMaxSize)
	{
		Global::logger->debug("[%s] room<%u> player<%s> spectator on <%d> : invalid pos index. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
		return false;
	}

    MODI_ClientAvatarInRoom & client = *GetClientInRoomByPos(bPlayer, nPos);
    if (!client.m_pClient)
    {
        client.Reset();
		
		MODI_GS2C_Notify_PostionStateChagned notify;
		notify.m_nTargetPos = RoleHelpFuns::EncodePosSolt( bPlayer , nPos );
		notify.m_byClosed = client.m_bClosed ? 1 : 0;
		Broadcast( &notify , sizeof(notify) );
		Global::logger->debug("[%s] room<%u> player<%s> spectator on <%d> : successful. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 

		UpdateRoomStateMask();

        return true;
    }
    return false;
}

//    关位置
bool MODI_NormalGameRoom::OffPostion(MODI_ClientAvatar * pClient , bool bPlayer, defRoomPosSolt nPos, bool bKick)
{
	if (bPlayer && nPos >= m_nPlayerMaxSize)
	{
		Global::logger->debug("[%s] room<%u> player<%s> player off <%d> : invalid pos index.", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
		return false;
	}
    else if (nPos >= m_nSpectatorMaxSize)
	{
		Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : invalid pos index. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
		return false;
	}

    MODI_ClientAvatarInRoom & client = *GetClientInRoomByPos(bPlayer, nPos);
    if (!client.m_pClient)
    {
		Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : successful. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
        client.m_bClosed = true;
		MODI_GS2C_Notify_PostionStateChagned notify;
		notify.m_nTargetPos = RoleHelpFuns::EncodePosSolt( bPlayer , nPos );
		notify.m_byClosed = client.m_bClosed ? 1 : 0;
		Broadcast( &notify , sizeof(notify) );
		UpdateRoomStateMask();
        return true;
    }
    else if (bKick)
    {
		Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : pos occupied, try kick player. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
		MODI_ClientAvatar * pDestClient = client.m_pClient;

		if( pDestClient->GetGUID() == pClient->GetGUID() )
		{
			Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : cannot kick yourself. ", 
					GS_ROOMOPT,
					GetRoomID() , 
					pClient->GetRoleName() , 
					nPos ); 
			return false;
		}

        //    位置上有人，先踢
        if (LeaveRoom(client.m_pClient, enLeaveReason_Kick))
        {
            client.Reset();
            client.m_bClosed = true;

			// 位置状态改变了，同步给客户端
			MODI_GS2C_Notify_PostionStateChagned notify;
			notify.m_nTargetPos = RoleHelpFuns::EncodePosSolt( bPlayer , nPos );
			notify.m_byClosed = client.m_bClosed ? 1 : 0;
			Broadcast( &notify , sizeof(notify) );
			Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : successful. ", 
					GS_ROOMOPT,
					GetRoomID() , 
					pClient->GetRoleName() , 
					nPos ); 
			UpdateRoomStateMask();
			return true;
        }
		Global::logger->debug("[%s] room<%u> player<%s> spectator off <%d> : fail. ", 
				GS_ROOMOPT,
				GetRoomID() , 
				pClient->GetRoleName() , 
				nPos ); 
    }
    return false;
}

// 交换位置
bool MODI_NormalGameRoom::ExchangePostion(MODI_ClientAvatar * pClient)
{
    bool bSrcIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家
    defRoomPosSolt nSrcPos = RoleHelpFuns::GetPosSolt(pClient->GetRoleNormalInfo().m_roomSolt); // 位置索引

    // 源位置
    MODI_ClientAvatarInRoom & srcClient = *GetClientInRoomByPos(bSrcIsPlayer, nSrcPos);
	if( srcClient.m_pClient != pClient )
	{
		MODI_ASSERT( 0 );
		return false;
	}

	// 源位置为参战位置，并且不允许观战。无法切换位置
	if( bSrcIsPlayer )
	{
		if( IsAllowSpectator() == false )
		{
			Global::logger->debug("[%s] <room=%d,player=%s> cannot switch, no spectator allowed." , 
					GS_ROOMOPT ,
					GetRoomID() , 
					pClient->GetRoleName() );
			return true;
		}
	}

    if (srcClient.m_bReadyInRoom)
    {
		Global::logger->debug("[%s] <room=%d,player=%s> cannot switch, player is ready." , 
				GS_ROOMOPT ,
			   	GetRoomID() , 
				pClient->GetRoleName()	);

        // 先取消准备，再交换位置
        MODI_GS2C_Respone_OptResult result;
        result.m_nResult = SvrResult_Room_ReadyCC;
        pClient->SendPackage(&result, sizeof(result));

        return true;
    }

	
	// 获得另外一边的一个空位置
	bool bDestIsPlayer   = !bSrcIsPlayer;
	defRoomPosSolt nDestPos = INVAILD_POSSOLT;
	if( bDestIsPlayer )
	{
		nDestPos = GetFreePlayersPosSolt();
	}
	else 
	{
		nDestPos = GetFreeSpectatorPosSolt();
	}

	// 对面没有空位置，无法交换位置
	if( nDestPos == INVAILD_POSSOLT )
	{
		Global::logger->debug("[%s] <room=%u,player=%s> cannot switch, pos full." , 
				GS_ROOMOPT ,
				this->GetRoomID() , 
				pClient->GetRoleName() );

		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_FullCC;
		pClient->SendPackage(&result,sizeof(result));
		return true;
	}

    // 目标位置
    MODI_ClientAvatarInRoom & destClient = *GetClientInRoomByPos(bDestIsPlayer, nDestPos);
    if (destClient.m_pClient)
    {
		MODI_ASSERT(0);
		// 空位置阿，不应该有人,代码有问题
		return false;
    }

	// 位置交换
	destClient.Reset();
    MODI_ClientAvatarInRoom tempClient = srcClient;
    srcClient = destClient;
    destClient = tempClient;

	// 位置改变了，更新人物属性
	defRoomPosSolt nOldPos = pClient->GetRoleNormalInfo().m_roomSolt;
    defRoomPosSolt nNewPos = RoleHelpFuns::EncodePosSolt( bDestIsPlayer , nDestPos );
	pClient->SetRoomSlot( nNewPos );

	// 同步给所有客户端，有人交换位置
    MODI_GS2C_Notify_PostionChagned notify;
	notify.m_srcPos = nOldPos;
    notify.m_targetPos = pClient->GetRoleNormalInfo().m_roomSolt;
    this->Broadcast(&notify, sizeof(notify));

	Global::logger->debug("[%s] <room=%d,player=%s,from=%s,d=%d,to=%s,d=%d>." , 
			GS_ROOMOPT , 
			GetRoomID() , 
			pClient->GetRoleName() , bSrcIsPlayer ? "play" : "spectator" , 
			RoleHelpFuns::GetPosSolt( notify.m_srcPos ) ,
			bDestIsPlayer ? "play" : "spectator" , 
			RoleHelpFuns::GetPosSolt( notify.m_targetPos ) );


	if( !bSrcIsPlayer )
	{
		// 如果是从观战切换到参战，则重置所听对象的GUID
		pClient->SetSingerGUID( INVAILD_GUID );
	}
	else
	{
		OnClientExitPlayerState( pClient->GetGUID() );
	}

	if(GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		if(bSrcIsPlayer)
		{
			ClearTeam(nSrcPos);
		}
		else
		{
			SetTeam(nDestPos);
		}
		BroadcastTeamState();
	}
	
	AllocSongerForNoOBSpector();

	// 更新房间状态掩码
	UpdateRoomStateMask();

   	return true;
}

bool MODI_NormalGameRoom::ClientReadyPlay(MODI_ClientAvatar * pClient)
{
    bool bSrcIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false: true; //  是否参战玩家
    defRoomPosSolt nSrcPos = RoleHelpFuns::GetPosSolt(pClient->GetRoleNormalInfo().m_roomSolt); // 位置索引
    MODI_ClientAvatarInRoom & Client = *GetClientInRoomByPos(bSrcIsPlayer, nSrcPos);
	if( Client.m_pClient != pClient )
	{
		MODI_ASSERT(0);
		return false;
	}

	Global::logger->debug("[FirstHalf] client %s loading last half ok ~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",pClient->GetRoleName());

	if( Client.m_bReadyInGame == false )
	{
		// 某个客户端资源加载就绪，同步给所有客户端
		Client.m_bReadyInGame = true;

		if( this->IsPlaying() )
		{
			// 已经开始游戏
			if( !bSrcIsPlayer )
			{
				MODI_GS2C_Notify_StartGame notify;
				pClient->SendPackage( &notify , sizeof(notify) );

				// 非参战玩家资源就绪，分配歌手
				AllocSongerForNoOBSpector();

			}
		}
		else 
		{
			MODI_GS2C_Notify_SbReady notify;
			notify.m_nClientID = pClient->GetGUID();
			Broadcast_Except( &notify , sizeof(notify) , pClient );

			// 尝试开始游戏
			TryStartGame();
		}
	}

    return true;
}

bool MODI_NormalGameRoom::TryStartGame()
{
	///	每一个玩家的后半部分资源准备好后，都会尝试开始游戏一次
	    unsigned int n = 0;
	    for (n = 0; n < m_nPlayerMaxSize; n++)
	    {
		if (m_Players[n].m_pClient && !m_Players[n].m_bReadyInGame)
		{
		    return false;
		}
	    }
	    ///	旁观者可能出现问题：
	    /// 旁观者不一定会发来后半部分资源装载完成的命令
	    /// 
	    for (n = 0; n < m_nSpectatorMaxSize; n++)
	    {
		if (m_Spectators[n].m_pClient && !m_Spectators[n].m_bReadyInGame)
		{
		    return false;
		}
	    }

    // 可以开始游戏

	if( !MODI_MdmMgr::GetInstancePtr()->GetMusicTotalNoteLength( GetNormalInfo().m_nMusicId , m_fTotalNodeLength ) )
	{
		MODI_ASSERT(0);
		Global::logger->fatal("[%s] Fail to start game, total length of NODE equals zero. MusicID<%u>" , 
				SVR_RESOURCES_ERR ,
			   	GetNormalInfo().m_nMusicId  );
		return false;
	}

	/// 统计一下歌曲的点击率
	MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
	if( pZone )
	{
		MODI_S2RDB_Notify_MusicCount send_cmd;
		send_cmd.m_wdMusicID = GetNormalInfo().m_nMusicId;
		send_cmd.m_byState = 1;
		pZone->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	
	const GameTable::MODI_Musiclist * pTempMusicItem = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get( GetNormalInfo().m_nMusicId );
	if( !pTempMusicItem )
	{
		MODI_ASSERT(0);
		Global::logger->fatal("[%s] Fail to start game, cannot find <config_id=%u> matching data sheet." , 
				SVR_RESOURCES_ERR ,
			   	GetNormalInfo().m_nMusicId );
		return false;
	}
    
	bool	is_sing = true;
	if( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		is_sing = false;
	}
    
    //    还有客户端在游戏中的话，则要继续游戏
    if (GetCurClientsNum() > 0)
    {
		// 将音乐头数据发送给旁听对象
		for( n = 0; n < m_nSpectatorMaxSize; n++ )
		{
			m_Spectators[n].m_bListernOver = false;
			if( m_Spectators[n].m_pClient )
			{
				MODI_ClientAvatar * pSonger = FindPlayerByGUID( m_Spectators[n].m_pClient->GetRoleNormalInfo().m_SingerID  );
				if( pSonger )
				{
					MODI_ASSERT( pSonger->IsPlayer() );
					pSonger->SendMusicHeader( m_Spectators[n].m_pClient );
				}
				else 
				{
					Global::logger->error("[%s] Allocate <player=%s,room=%u>, no player found." , 
							GS_ROOMOPT , 
							m_Spectators[n].m_pClient->GetRoleName() , 
							this->GetRoomID() );
				}
			}
		}

		// 通知网关，将音乐数据传输数据告知他
		NotifyTransMissionMapToGW();

		// 重置游戏分数
		for( n = 0; n < m_nPlayerMaxSize; n++ )
		{
			m_PlayersGameResults[n].Reset();
			if( m_Players[n].m_pClient )
			{
				SNPRINTF( m_PlayersGameResults[n].m_szRoleName , 
						sizeof(m_PlayersGameResults[n].m_szRoleName) , 
						m_Players[n].m_pClient->GetRoleName() );
				m_PlayersGameResults[n].m_nClientID = m_Players[n].m_pClient->GetGUID();
				m_PlayersGameResults[n].m_byLevel = m_Players[n].m_pClient->GetRoleBaseInfo().m_ucLevel;
				m_Players[n].m_pClient->OnStartGame();

#ifdef _FB_LOGGER
				if( is_sing)
				{
					Global::fb_logger->debug("\t%u\tSINGING MODE STARTS\t%d\t%d\t%s\t%s\t%s",Global::m_stRTime.GetSec(),GetRoomID(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(),pTempMusicItem->get_Songname(),pTempMusicItem->get_OriginalSinger());
				}
				else
				{
					Global::fb_logger->debug("\t%u\tKEYBOARD MODE STARTS\t%d\t%d\t%s\t%s\t%s",Global::m_stRTime.GetSec(),GetRoomID(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(),pTempMusicItem->get_Songname(),pTempMusicItem->get_OriginalSinger());
				}
#endif

			}
		}

		SetShowTimeCount( 0 );
		m_pShowTimeClient = 0;
		m_bShowTimeing = false;
	

		// 可以开始游戏了
		MODI_GS2C_Notify_StartGame notify;
		this->Broadcast( &notify , sizeof(notify) );

		SetRoomStatus( ROOM_STATUS_PLAYING);
		//m_iStatus = ROOM_STATUS_PLAYING;
		MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
		// 记录开始的时间
		m_nBeginTime = pTick->GetTimer().GetMSec();
		//m_nEndTime = m_nBeginTime + pTempMusicItem->get_Duration() * 1000 + 2000 + 3000 + 3000;
		if( GetNormalInfo().m_timeType == kMusicTime_Half )
		{
			m_nEndTime = m_nBeginTime + (unsigned long long)(pTempMusicItem->get_HalfTime()) * 1000 + 6000 + 5000+3000;
		}
		else 
		{
			m_nEndTime = m_nBeginTime + pTempMusicItem->get_Duration() * 1000 + 6000 + 5000+3000;
		}
		
		m_nMaxCombat = MAX_KEY_PER_SEC * pTempMusicItem->get_Duration();
		m_pNotifyGameResult = 0;
		return true;
    }

    return false;
}


bool MODI_NormalGameRoom::ChangeObSinger(MODI_ClientAvatar * pClient , MODI_GUID nObserverClientID )
{
	if( pClient->GetRoleNormalInfo().m_roomSolt == INVAILD_POSSOLT )
		return false;

	bool bSrcIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false : true;
	defRoomPosSolt nSrcPos = RoleHelpFuns::GetPosSolt(pClient->GetRoleNormalInfo().m_roomSolt);
	MODI_ClientAvatar *p_client = GetClientAvatarByPos( bSrcIsPlayer , nSrcPos );
	
	if( !p_client )
	{
		MODI_ASSERT(0);
		return false;
	}

//	MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
//	MODI_ClientAvatar * p1 = pChannel->FindClientByID( pClient->GetGUID() );
//	MODI_ClientAvatar * p2 = pChannel->FindClientByID( nObserverClientID );
//	MODI_ClientAvatar * p3 = pChannel->FindClientByID( p_client->GetGUID() );
//
//	DISABLE_UNUSED_WARNING( p1 );
//	DISABLE_UNUSED_WARNING( p2 );
//	DISABLE_UNUSED_WARNING( p3 );

	if( p_client != pClient ) 
	{
		MODI_ASSERT(0);
		return false;
	}

	if( nObserverClientID == INVAILD_GUID )
	{
		Global::logger->debug("[%s] <player=%s,GUID=%llu,room=%u> request switch songer by  in room , but songer GUID is invaild . " , 
				GS_ROOMOPT ,
				pClient->GetRoleName() , 
				nObserverClientID.ToUint64() , 
				this->GetRoomID() );
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_ChangeSonger_InvaildGUID;
		pClient->SendPackage( &result , sizeof(result) );
		return false;
	}

	if( nObserverClientID == pClient->GetGUID() )
	{
		Global::logger->debug("[%s] player<%s> request switch songer by GUID<%llu> in room<%u> , but new songer GUID is self . " , 
				GS_ROOMOPT ,
				pClient->GetRoleName() , 
				nObserverClientID.ToUint64() , 
				this->GetRoomID() );
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_ChangeSonger_SelfGUID;
		pClient->SendPackage( &result , sizeof(result) );
		return false;
	}

	if( nObserverClientID == pClient->GetRoleNormalInfo().m_SingerID )
	{
		Global::logger->debug("[%s] player<%s> request switch songer by GUID<%llu> in room<%u> , new songer not change . " , 
				GS_ROOMOPT ,
				pClient->GetRoleName() , 
				nObserverClientID.ToUint64() , 
				this->GetRoomID() );
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_ChangeSonger_NewSongerNotChange;
		pClient->SendPackage( &result , sizeof(result) );
		return false;
	}

	if( bSrcIsPlayer )
	{
		Global::logger->debug("[%s] player<%s> request switch songer by GUID<%llu> in room<%u> , but self is songer . " , 
				GS_ROOMOPT ,
				pClient->GetRoleName() , 
				nObserverClientID.ToUint64() , 
				this->GetRoomID() );
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_ChangeSonger_DestSongerNotPlayer;
		pClient->SendPackage( &result , sizeof(result) );
		return false;
	}

	// 找到要收听的歌手
	MODI_GUID nNewSingerID = INVAILD_GUID;
	MODI_ClientAvatar * pSonger = 0;
	for( unsigned int n = 0; n < m_nPlayerMaxSize; n++)
	{
		if( m_Players[n].m_pClient && m_Players[n].m_pClient->GetGUID() == nObserverClientID  )
		{
			nNewSingerID = nObserverClientID;
			pSonger = m_Players[n].m_pClient;
			break;
		}
	}

	if( nNewSingerID != nObserverClientID )
	{
		Global::logger->debug("[%s] player<%s> request switch songer by GUID<%llu> in room<%u> , but can't find songer . " , 
				GS_ROOMOPT ,
				pClient->GetRoleName() , nObserverClientID.ToUint64() , this->GetRoomID() );
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_ChangeSonger_DestSongerNotExist;
		pClient->SendPackage( &result , sizeof(result) );
		return false;
	}

	MODI_ClientAvatar * pOldClient = 0;
	MODI_ClientAvatar * pNewClient = 0;

	if( IsPlaying() )
	{
		// 正在游戏中，则需要通知网关删除之前收听的通道，增加新的收听通道
		
		MODI_SessionID nNewSessionID; 
		MODI_SessionID nOldSessionID;
		
		// 之前的收听客户端
		pOldClient = FindPlayerByGUID( pClient->GetRoleNormalInfo().m_SingerID );
		if( pOldClient )
		{
			nOldSessionID  = pOldClient->GetSessionID();
		}

		// 新的收听客户端
		pNewClient = FindPlayerByGUID( nObserverClientID );
		if( pNewClient )
		{
			nNewSessionID = pNewClient->GetSessionID();
		}

		if( nNewSessionID.IsInvaild() == false )
		{

			// 向该客户端发送音乐头
			// 注意次序
			const MODI_SessionID destSessionID = pClient->GetSessionID();
			NotifyDelTransMission( nOldSessionID , &destSessionID , 1 );
			pSonger->SendMusicHeader( pClient );
			NotifyAddTransMission( nNewSessionID , &destSessionID , 1 );


			// 通知网关删除之前收听的通道，增加新的收听通道
//			NotifyChangeTransMissionMapToGW( nNewSessionID , nOldSessionID , &destSessionID , 1 ); 
		}
	}

	pClient->SetSingerGUID( nObserverClientID );
	{
	// 广播给所有客户端，某人听歌对象改变
	MODI_GS2C_Notify_SbObSingerChanged notify;
	notify.m_nTargetGUID = pClient->GetGUID();
	notify.m_nObserverGUID = nObserverClientID;
	this->Broadcast( &notify , sizeof(notify) );
	}

	{
	// 同步一次新的听歌对象，最后一次GPOWER信息
	pClient->OnChangeSinger( this , 
			pOldClient ,
			pNewClient );
	}

	return true;
}

bool MODI_NormalGameRoom::GameScore(MODI_ClientAvatar * pClient , WORD nNo , float fTime , 
			DWORD nScore , BYTE byType , float fExact , 
			float fRightSec , WORD nCombat )
{
	if(nScore > 200000)
	{
		Global::logger->error("[%s] recv score error.client<%s> ,score<%u> , level<%u> , pre<%3f> " , 
 				SVR_TEST ,
 				pClient->GetRoleName() , 
 				nScore,
 				byType,
 				fExact );
		return false;
	}
	
	if (IsPlaying() == false )
    {
        Global::logger->warn("[%s] room <%u> not in match game status , but player <%s> sent score of lyric", 
				GS_ROOMOPT,
                pClient->GetRoomID(), 
				pClient->GetRoleName());
        return false;
	}

	// 非参战玩家不能发送分送
	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is spectator but sent score." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return false;
	}

	MODI_GameResult * pGameResult = GetGameResult( pClient );
	if( !pGameResult )
	{
		return false;
	}

	unsigned int nTotalSentencesCount = GetSentencesCount();
	unsigned int nSeq = pGameResult->m_nSentensCount;
	if( nSeq + 1 > nTotalSentencesCount )
	{
		return false;
	}

	if( byType > enScore_Begin && byType < enScore_End && fExact <= 1.0f  )
	{
		// 对分数的有效性进行验证
		// to do ...
		//
		//
		//
		// 更新游戏成绩结果
		switch( byType )
		{
			case enScore_Bad:
				pGameResult->m_byBad += 1;
				pClient->SetGoodPlusCount( 0 );
				break;
			case enScore_Cool:
				pGameResult->m_byCool += 1;
				pClient->SetGoodPlusCount( pClient->GetGoodPlusCount() + 1 );
				pClient->UpdateMaxGoodPlusCount();
				pGameResult->m_byCombo = pClient->GetMaxGoodPlusCount();
				break;
			case enScore_Miss:
				pGameResult->m_byMiss += 1;
				pClient->SetGoodPlusCount( 0 );
				break;
			case enScore_Pefect:
				pGameResult->m_byPefect += 1;
				pClient->SetGoodPlusCount( pClient->GetGoodPlusCount() + 1 );
				pClient->UpdateMaxGoodPlusCount();
				pGameResult->m_byCombo = pClient->GetMaxGoodPlusCount();
				break;
			case enScore_Good:
				pGameResult->m_byGood += 1;
				break;
		}
		
		pGameResult->m_nTotalScore += nScore;
		pGameResult->m_fExact = fExact;
		pGameResult->m_nSentensCount += 1;

		// 增加唱对的秒数
		pClient->AddRightSec( fRightSec );
	
		MODI_GS2C_Notify_SentenceScore notifySentenceScore;

		// 该房间是否还能产生SHOW TIME效果
		if( IsCanProduceShowTime() )
		{
//			// 歌曲NODE总共长度
//			float fTotalSec = GetTotalNodeLength();
//			// show time 需要的时间长度
//			float fShowTimeSec = fTotalSec * MODI_GameConstant::get_ShowTimePro();
//			//float fShowTimeSec = fTotalSec * 0.05f;
//
//			if( pClient->GetRightSec() >= fShowTimeSec )
			if( fExact > MODI_GameConstant::get_ShowTimePro() )
			{
				// show time 效果
				OnShowTime( pClient );
			}
		}

		notifySentenceScore.m_nClient = pClient->GetGUID();
		notifySentenceScore.m_nScore = pGameResult->m_nTotalScore;
		notifySentenceScore.m_byEvaluation = byType;
		notifySentenceScore.m_nCombat = nCombat;
		notifySentenceScore.m_nNo = nNo;
		notifySentenceScore.m_fTime = fTime;

		//Broadcast_Except( &notifySentenceScore , sizeof(notifySentenceScore) , pClient );
		Broadcast( &notifySentenceScore , sizeof(notifySentenceScore));

// 		Global::logger->debug("[%s] recv score .client<%s> ,score<%u> , level<%u> , pre<%3f> " , 
// 				SVR_TEST ,
// 				pClient->GetRoleName() , 
// 				nScore,
// 				byType,
// 				fExact );
	}	
	else 
	{
		Global::logger->warn("The parameter of <%u>the lyric score is incorrect. score:<%u>, level:<%u>, precision:<%3f> ." , 
				GS_ROOMOPT ,
		        pGameResult->m_nSentensCount + 1 ,
				nScore , 
				byType , 
				fExact );
	}
	
	return true;
}

bool MODI_NormalGameRoom::IsRoomMaster(MODI_ClientAvatar * pClient )
{
	if( GetMaster() == pClient->GetGUID() )
		return true;
	return false;
}

//   获得某个参战位置上的分数
MODI_GameResult * 	MODI_NormalGameRoom::GetGameResult( MODI_ClientAvatar * pClient )
{
	if( pClient->IsPlayer() )
	{
		MODI_ClientAvatar * pDest = GetClientAvatarByPos( pClient->IsPlayer() , pClient->GetPostionInRoom() );
		if( pDest == pClient )
		{
			return &m_PlayersGameResults[pDest->GetPostionInRoom()];
		}

		MODI_ASSERT( 0 );
	}
	return 0;
}

MODI_GUID 	MODI_NormalGameRoom::GetRenqiKing()
{
	unsigned int n = 0;
	BYTE byVoteCount = 0;
	DWORD nScore = 0;
	MODI_GUID guid;
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		MODI_ClientAvatar * pPlayer = m_Players[n].m_pClient;
		if( pPlayer )
		{
			if( guid.IsInvalid() )
			{
				guid = pPlayer->GetGUID();
				byVoteCount = m_Players[n].m_byVoteCount;
				nScore = m_PlayersGameResults[n].m_nTotalScore;
			}
			else 
			{
				if( m_Players[n].m_byVoteCount  > byVoteCount )
				{
					guid = pPlayer->GetGUID();
					byVoteCount = m_Players[n].m_byVoteCount;
					nScore = m_PlayersGameResults[n].m_nTotalScore;
				}
				else if( m_Players[n].m_byVoteCount == byVoteCount )
				{
					if( m_PlayersGameResults[n].m_nTotalScore > nScore )
					{
						guid = pPlayer->GetGUID();
						byVoteCount = m_Players[n].m_byVoteCount;
						nScore = m_PlayersGameResults[n].m_nTotalScore;
					}
				}
			}
		}
	}

	MODI_ASSERT( guid.IsInvalid() == false );

	return guid;
}

bool MODI_NormalGameRoom::IsAllSongOver()
{
	unsigned int n = 0;
	unsigned int nTotalSens = GetSentencesCount();
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		if( m_Players[n].m_pClient )
		{
			if( m_PlayersGameResults[n].m_nSentensCount < nTotalSens )
				return false;
		}
	}

	return true;
}

void MODI_NormalGameRoom::AllocSongerForNoOBSpector()
{
	for( unsigned int n = 0; n < m_nSpectatorMaxSize; n++ )
	{
		if( m_Spectators[n].m_pClient )
		{
			// 旁观者所观看的歌手如果不存在，则重新分配
			if( FindPlayerByGUID( m_Spectators[n].m_pClient->GetRoleNormalInfo().m_SingerID ) )
				continue ;

			// 分配个新的对象
			MODI_GUID nNewSongerGUID  = GetSingerForSpector();
			if( nNewSongerGUID != INVAILD_GUID )
			{
				// 改变听歌对象
				if( !this->ChangeObSinger( m_Spectators[n].m_pClient , nNewSongerGUID ) )
				{
					Global::logger->error("[%s] spectator change obsinger faild.<ptr=%p,name=%s,charid=%u,newsonger_guid=%s" , 
							SVR_TEST ,
							m_Spectators[n].m_pClient,
							m_Spectators[n].m_pClient->GetRoleName(),
							m_Spectators[n].m_pClient->GetCharID(),
							nNewSongerGUID.ToString()
							);
				}
			}
			else 
			{
				// 没有新的听歌对象，重置ID
				m_Spectators[n].m_pClient->SetSingerGUID( INVAILD_GUID );
			}
		}
	}
}

void 	MODI_NormalGameRoom::OnShowTime(MODI_ClientAvatar * pClient)
{
	// show time 效果
	MODI_GS2C_Notify_EffectBegin effectBegin;
	effectBegin.m_byEffectType = enEffect_ShowTime;
	effectBegin.m_nClientID = pClient->GetGUID();
	Broadcast( &effectBegin , sizeof(effectBegin) );
	SetShowTimeCount( GetShowTimeCount() + 1 );

	m_bShowTimeing = true;
	MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
	m_ShowTimeTimer.Reload( SHOWTIME_CYCLE  , pTick->GetTimer() );
	m_pShowTimeClient = pClient;

	Global::logger->debug("[%s] room <%u> player <%s> is now in show time." , GS_ROOMOPT ,
					   	pClient->GetRoomID() , pClient->GetRoleName() );
}

void MODI_NormalGameRoom::EndShowTime()
{
	if( !m_pShowTimeClient )
		return ;

	MODI_GS2C_Notify_EffectOver notify;
	notify.m_byEffectType = enEffect_ShowTime;
	notify.m_nClientID = m_pShowTimeClient->GetGUID();
	Broadcast( &notify , sizeof(notify) );
	m_pShowTimeClient = 0;
	m_bShowTimeing  = false;
}

void MODI_NormalGameRoom::ComputeGameResult()
{
	unsigned int n = 0;
	// 下发游戏比赛结果
	memset( m_szPackageBuf , 0 , sizeof(m_szPackageBuf) );

	MODI_GS2C_Notify_GameResult * p_notifyGameResult = (MODI_GS2C_Notify_GameResult * )(m_szPackageBuf);
	AutoConstruct( p_notifyGameResult );
	
	float fExacts[enEval_End - 1] = { 1.0f , 0.9f , 0.8f , 0.7f , 0.6f ,0.5f ,0.4f, 0.3f,0.2f,0.1f };
	unsigned char byEvalutaion[enEval_End - 1] = { enEval_SS_Type , 
		enEval_S_Type,
		enEval_SDEC_Type,
		enEval_A_Type,
		enEval_ADEC_Type,
		enEval_B_Type,
		enEval_C_Type,
		enEval_D_Type,
		enEval_E_Type,
		enEval_F_Type	};

	unsigned int nCount = GetSentencesCount();
	MODI_ASSERT(nCount);
	unsigned int nResultIdx = 0;

	MODI_GUID winner_guid;
	DWORD nHighestScore = 0;
	unsigned int max= 0;
	unsigned int last=0;

	unsigned int team_money[3];
	memset( team_money,0,sizeof(team_money));

	bool	is_sing = true;
	if( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		is_sing = false;
	}
#ifdef _FB_LOGGER
	const GameTable::MODI_Musiclist * pTempMusicItem = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get( GetNormalInfo().m_nMusicId );
	if( !pTempMusicItem )
	{
		MODI_ASSERT(0);
	}
#endif
	//下面是关于组队的计算
	if( GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM )
	{
		//
		for( n = 0; n < m_nPlayerMaxSize; n++ )
		{

			
			//如果有人的话，那么就计算组队的分数
			if( m_Players[n].m_pClient )
			{
				if( m_byTeamState[n] > ROOM_TEAM_BEGIN && m_byTeamState[n]< ROOM_TEAM_END)
				{
					m_TeamGameResults[m_byTeamState[n]-1]+=m_PlayersGameResults[n].m_nTotalScore;
				}
				else
				{

					assert(0);
				}

			}
		
		}
		//找出分数最高的队伍
		
		for( n=0;   n< MAX_PLAYSLOT_INROOM/2; ++n)
		{
			if( m_TeamGameResults[n] > m_TeamGameResults[max] )
			{
				max = n;	
			}			
			team_money[n]=0;
		}

		//分数第三的队伍	
		for( n=0;  n< MAX_PLAYSLOT_INROOM/2; ++n)
		{

			if( m_TeamGameResults[n] <= m_TeamGameResults[last])
			{
				last = n;
			}
		}


	}
	for( n = 0; n < m_nPlayerMaxSize; n++ )
	{
		// 位置上有过人
		if(  strlen(m_PlayersGameResults[n].m_szRoleName) > 0 )
		{
//			p_notifyGameResult->m_bySize += 1;
			m_PlayersGameResults[n].m_nClientID = INVAILD_GUID;
			if( m_Players[n].m_pClient )
			{
				m_PlayersGameResults[n].m_nClientID = m_Players[n].m_pClient->GetGUID();
			}

			m_PlayersGameResults[n].m_byEvaluation = enEval_F_Type;
			m_PlayersGameResults[n].m_nMoney = 0;

			// 计算准度
			if( nCount )
			{
				//m_PlayersGameResults[n].m_fExact = m_PlayersGameResults[n].m_fExact / (float)nCount;
				// 计算评价
				for( int iExact = 0; iExact < enEval_End - 1; iExact++ )
				{
					if( m_PlayersGameResults[n].m_fExact <= fExacts[iExact] && 
							m_PlayersGameResults[n].m_fExact > fExacts[iExact + 1] )
					{
						m_PlayersGameResults[n].m_byEvaluation = byEvalutaion[iExact];
						break;
					}
				}
			}
			else 
			{
				m_PlayersGameResults[n].m_nTotalScore = 0;
			}

			// 玩家还在游戏中的话，需要加经验，加钱
			if( m_Players[n].m_pClient )
			{
				MODI_ClientAvatar * pTmpClient = m_Players[n].m_pClient;
				////	临时代码，写死一个变量来与脚本通信
				//记录演唱歌的准确度，演唱歌曲的ID
				

					
				// 执行脚本处理

				DWORD nAddExp =0;
				DWORD nMoney =0;
				ComputeExpAndMoney( n,nAddExp,nMoney);
				
				//组队经验修正
				m_PlayersGameResults[n].m_nExp = nAddExp;

				// 计算获得的游戏比
				
				if( GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
				{
					assert( m_byTeamState[n]);
					team_money[ m_byTeamState[n]-1] += nMoney; 
				}

				m_PlayersGameResults[n].m_nMoney = nMoney;

				if( m_PlayersGameResults[n].m_nTotalScore >= nHighestScore )
				{
					nHighestScore  = m_PlayersGameResults[n].m_nTotalScore;
					winner_guid = m_PlayersGameResults[n].m_nClientID;
				}

				if( GetSubType() != ROOM_STYPE_KEYBOARD  && GetSubType() != ROOM_STYPE_KEYBOARD_TEAM)
				{

					if( GetNormalInfo().m_songType  == kMusicSongType_Song )
					{
						pTmpClient->IncChangCount( 1 );
					}
					else if( GetNormalInfo().m_songType == kMusicSongType_Heng )
					{
						pTmpClient->IncHengCount( 1 );
					}
					if( pTmpClient->GetHighestSocre() < m_PlayersGameResults[n].m_nTotalScore )
					{
#ifdef _D_VERSION_SGP
						if( m_PlayersGameResults[n].m_nTotalScore < 15000000)
						{
#endif
#ifdef _D_VERSION_INA
						if( m_PlayersGameResults[n].m_nTotalScore < 15000000)
						{
#endif
						pTmpClient->SetHighestScore( m_PlayersGameResults[n].m_nTotalScore );
						pTmpClient->SetHighestScoreMusicid( GetMusicID() );
#ifdef _D_VERSION_SGP
						}
#endif
#ifdef _D_VERSION_INA
						}
#endif

					}

					if( pTmpClient->GetHighestPrecision() < m_PlayersGameResults[n].m_fExact )
					{
						pTmpClient->SetHighestPrecision( m_PlayersGameResults[n].m_fExact );
						pTmpClient->SetHighestPrecisionMusicid( GetMusicID() );
					}
				}
				else
				{
					//	增加键盘模式的数量
					pTmpClient->IncKeyboardCount(1);

					if( m_Players[n].m_pClient->GetFullData().m_roleInfo.m_detailInfo.key_highest_precision < m_PlayersGameResults[n].m_fExact)
					{
						m_Players[n].m_pClient->SetKeyPrecise( m_PlayersGameResults[n].m_fExact);
						m_Players[n].m_pClient->SetPreciseKeyMusic( GetMusicID());
		
					}
					if( m_Players[n].m_pClient->GetFullData().m_roleInfo.m_detailInfo.key_highest_socre < m_PlayersGameResults[n].m_nTotalScore)
					{
						m_Players[n].m_pClient->SetKeyScore( m_PlayersGameResults[n].m_nTotalScore);
						m_Players[n].m_pClient->SetScoreKeyMusic( GetMusicID());

					}
				}
				

			}
//
//			memcpy( &p_notifyGameResult->m_pResults[nResultIdx++] , &m_PlayersGameResults[n] , sizeof( MODI_GameResult ) );
		}
	}
	float32 	m_fExact = 0;
	int 	n_exact=0;
	DWORD		m_score=0;
	int	n_score=0;

#ifdef _FB_LOGGER
		n=0;
		m_score=0;
		n_score=0;
		string queue;
		for( n=0;  n < m_nPlayerMaxSize; n++)
		{
			if( m_Players[n].m_pClient )
			{
				Global::logger->info("[music_end] gameuser music end <accid=%d,musicid=%d,type=%d>",m_Players[n].m_pClient->GetAccountID(),GetMusicID(),GetSongType());	

				if( m_PlayersGameResults[n].m_nTotalScore >= m_score)
				{
					m_score = m_PlayersGameResults[n].m_nTotalScore;
					n_score = n;
				}
			}
		}
		if(m_Players[n_score].m_pClient)
		{
		if( m_Players[n_score].m_pClient->GetSex() == 0)
		{
			queue = "Queen";	
		}
		else
		{
			queue = "King";
		}
		}
#endif

	//增加经验 和金钱 以及加成
	for( n=0;  n < m_nPlayerMaxSize; n++)
	{
		if( m_Players[n].m_pClient )
		{
			///  执行演唱歌曲脚本
			MODI_TriggerSingMusic sing(GetMusicID());
			MODI_SingMusicManager::GetInstance().Execute(m_Players[n].m_pClient,sing);
			p_notifyGameResult->m_bySize += 1;
			// 增加经验
			m_Players[n].m_pClient->AdjustExp( m_PlayersGameResults[n].m_nExp );
			
// 尝试升级
			m_PlayersGameResults[n].m_byLevelUP = m_Players[n].m_pClient->TryLevelUp();

			// 当前等级
			m_PlayersGameResults[n].m_byLevel = m_Players[n].m_pClient->GetLevel();

#ifdef _FB_LOGGER
			if( m_PlayersGameResults[n].m_byLevelUP)
			{
				Global::fb_logger->debug("\t%u\tLEVELS UP\t%d\t%s\t%d",Global::m_stRTime.GetSec(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(),m_PlayersGameResults[n].m_byLevel);
			}
			if( is_sing)
			{
				if((unsigned int)n_score == (unsigned int)n)
				{
					Global::fb_logger->debug("\t%u\tSINGING MODE END\t%d\t%d\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t%f",Global::m_stRTime.GetSec(),GetRoomID(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(),pTempMusicItem->get_Songname(),pTempMusicItem->get_OriginalSinger(),m_Players[n].m_pClient->GetLevel(),m_PlayersGameResults[n].m_nTotalScore,queue.c_str(),m_Players[n_score].m_pClient->GetRoleName(),m_PlayersGameResults[n_score].m_fExact);
				}
				else
				{
					Global::fb_logger->debug("\t%u\tSINGING MODE END\t%d\t%d\t%s\t%s\t%s\t%d\t%d\t%f",Global::m_stRTime.GetSec(),GetRoomID(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(),pTempMusicItem->get_Songname(),pTempMusicItem->get_OriginalSinger(),m_Players[n].m_pClient->GetLevel(),m_PlayersGameResults[n].m_nTotalScore,m_PlayersGameResults[n].m_fExact);
				}
			}
			else
			{
				Global::fb_logger->debug("\t%u\tKEYBOARD MODE END\t%d\t%d\t%s\t%s\t%s\t%d\t%d",Global::m_stRTime.GetSec(),GetRoomID(),m_Players[n].m_pClient->GetCharID(),m_Players[n].m_pClient->GetAccName(), pTempMusicItem->get_Songname(),pTempMusicItem->get_OriginalSinger(),m_Players[n].m_pClient->GetLevel(),m_PlayersGameResults[n].m_nTotalScore);
			}
#endif
			if( GetSubType() == ROOM_STYPE_TEAM || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
			{
				if( m_byTeamState[n] == max)
				{
					m_PlayersGameResults[n].m_nMoney+=( team_money[m_byTeamState[n]-1]*30/100);
				}
				else if( m_byTeamState[n] == last)
				{
					m_PlayersGameResults[n].m_nMoney +=(team_money[m_byTeamState[n]-1]*5/100);
				}
				else
				{
					m_PlayersGameResults[n].m_nMoney += ( team_money[m_byTeamState[n]-1]*15/100);
				}
			}
				
			m_PlayersGameResults[n].m_byCombo = m_Players[n].m_pClient->GetMaxGoodPlusCount();

			m_Players[n].m_pClient->IncMoney( m_PlayersGameResults[n].m_nMoney );
			memcpy( &p_notifyGameResult->m_pResults[nResultIdx++] , &m_PlayersGameResults[n] , sizeof( MODI_GameResult ) );
		}

	}

	if( nResultIdx > 1 )
	{
		for( n = 0; n < m_nPlayerMaxSize; n++ )
		{
			MODI_ClientAvatar * pTemp = m_Players[n].m_pClient;
			if( pTemp )
			{
				if( pTemp->GetGUID() == winner_guid )
				{
					pTemp->IncWin();
				}
				else 
				{
					pTemp->IncLoss();
				}
			}
		}
	}
	
	m_fExact = 0;
	n_exact=0;
	m_score=0;
	n_score=0;
	for( n=0;  n < m_nPlayerMaxSize; n++)
	{
		if( m_Players[n].m_pClient )
		{
			Global::logger->info("[music_end] gameuser music end <accid=%d,musicid=%d,type=%d>",m_Players[n].m_pClient->GetAccountID(),GetMusicID(),GetSongType());	

			if( m_PlayersGameResults[n].m_fExact >=  m_fExact )
			{
				m_fExact = m_PlayersGameResults[n].m_fExact;
				n_exact = n;
			}
			if( m_PlayersGameResults[n].m_nTotalScore >= m_score)
			{
				m_score = m_PlayersGameResults[n].m_nTotalScore;
				n_score = n;
			}
		}
	}
#ifdef _D_VERSION_SGP
	if(!( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM) || m_score < 15000000)
	{
#endif
#ifdef _D_VERSION_INA
	if(!( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM) || m_score < 15000000)
	{
#endif

	if( m_Players[n_score].m_pClient && m_Players[n_exact].m_pClient)
	{

		struct SingleMusicInfo  music;
		enGameMode mode=enGameMode_Sing;
		music.m_wMusicid = GetMusicID();
		music.m_dwScore = m_score;
		music.m_wPrecise = (WORD)(m_fExact*10000);	
		strncpy( music.m_szPrecisePerson,m_Players[n_exact].m_pClient->GetRoleName(),sizeof(music.m_szPrecisePerson));
		strncpy( music.m_szScorePerson, m_Players[n_score].m_pClient->GetRoleName(),sizeof( music.m_szScorePerson));
		if( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
		{
			mode=enGameMode_Key;	
		}
		MODI_GameMusicMgr::GetInstance().OnRecvMusicInfo(& music, mode);
	}
	// 变更历史成绩
#ifdef _D_VERSION_SGP
	}
#endif
#ifdef _D_VERSION_INA
	}
#endif

#ifdef _DEBUG
	Global::logger->debug("%s] Game result show, there are <%u> records." , GS_ROOMOPT ,
		   	p_notifyGameResult->m_bySize );
	for(unsigned  int idebug = 0; idebug < nResultIdx; idebug++ )
	{
		Global::logger->debug("[%s] No <%d> record: player <%s>, score <%u>, precision <%3f>, experience <%u> . \n \
     Perfect = %u , Cool = %u , Good = %u , Bad = %u , Miss = %u , Combo = %u .\n" ,
				GS_ROOMOPT , idebug , p_notifyGameResult->m_pResults[idebug].m_szRoleName ,
			  p_notifyGameResult->m_pResults[idebug].m_nTotalScore , p_notifyGameResult->m_pResults[idebug].m_fExact ,
			  p_notifyGameResult->m_pResults[idebug].m_nExp , p_notifyGameResult->m_pResults[idebug].m_byPefect ,
			  p_notifyGameResult->m_pResults[idebug].m_byCool , p_notifyGameResult->m_pResults[idebug].m_byGood , 
			  p_notifyGameResult->m_pResults[idebug].m_byBad , p_notifyGameResult->m_pResults[idebug].m_byMiss , 
			  p_notifyGameResult->m_pResults[idebug].m_byCombo );
	}
#endif

	MODI_ASSERT( p_notifyGameResult->m_bySize == nResultIdx );

	m_pNotifyGameResult = p_notifyGameResult;

	for( n=0;   n< MAX_PLAYSLOT_INROOM/2; ++n)
	{
		 m_TeamGameResults[n] =0;
	}
	// 同步分数
//	int iSendSize = p_notifyGameResult->m_bySize * sizeof(MODI_GameResult) + sizeof(MODI_GS2C_Notify_GameResult);
//	Broadcast( p_notifyGameResult , iSendSize );
}


void MODI_NormalGameRoom::SendGameResult( bool bPlayer , bool bSpectar )
{
	if( m_pNotifyGameResult )
	{
		int iSendSize = m_pNotifyGameResult->m_bySize * sizeof(MODI_GameResult) + sizeof(MODI_GS2C_Notify_GameResult);
		if( bPlayer )
		{
			this->Broadcast_Players( m_pNotifyGameResult , iSendSize );
		}

		if( bSpectar )
		{
			this->Broadcast_Spectators( m_pNotifyGameResult , iSendSize );
		}

	}
}

void MODI_NormalGameRoom::SendMusicHeader( MODI_ClientAvatar  * pClient , int i )
{
	// 要发送的客户端，观战位置上的某个客户端
	MODI_ClientAvatar * pDestClient = GetClientAvatarByPos( false , i );
	if( pDestClient )
	{
		// 发送
		pClient->SendMusicHeader( pDestClient );
	}
	else 
	{
		Global::logger->warn("[%s] can't send musicheader to client.<room id = %u , src postion = %d >." , 
			GS_ROOMOPT , GetRoomID() , i );
		MODI_ASSERT(0);
	}
}


	
void MODI_NormalGameRoom::OnListernOver( MODI_GUID nGUID )
{
	for( DWORD i = 0; i < m_nSpectatorMaxSize; i++ )
	{
		MODI_ClientAvatar * pTemp = m_Spectators[i].m_pClient;

		if( pTemp && pTemp->GetGUID() == nGUID )
		{
			m_Spectators[i].m_bListernOver = true;
			break;
		}
	}
}
	
void  	MODI_NormalGameRoom::OnVoteTo( MODI_ClientAvatar * pClient , MODI_GUID nGUID )
{
	MODI_ClientAvatar * pTarget = FindPlayerByGUID( nGUID );
	if( !pTarget )
		return ;
	MODI_ClientAvatarInRoom  * clientInRoomTarget = GetClientInRoomByPos( pTarget->IsPlayer() , pTarget->GetPostionInRoom() );
	if( !clientInRoomTarget || clientInRoomTarget->m_pClient != pTarget )
		return ;
	MODI_ClientAvatarInRoom * clientInRoomSrc = GetClientInRoomByPos( pClient->IsPlayer() , pClient->GetPostionInRoom() );
	if( !clientInRoomSrc || clientInRoomSrc->m_pClient != pClient )
		return ;

	// 是否还能投票
	if( clientInRoomSrc->m_byVoteToCount >= MAX_VOTETO_COUNT )
	{
		// 已经投过票
		return ;
	}

	// 获得的票数
	if( pClient->IsVip())
	{
		if(Global::g_byGameShop == 1)
		{
			clientInRoomTarget->m_byVoteCount += 1;
			clientInRoomSrc->m_byVoteToCount += 2;
			if( clientInRoomSrc->m_pClient )
			{
				clientInRoomSrc->m_pClient->IncToupiaoCount(1);
			}
			// 增加获得的投票数
			pTarget->IncVoteCount( 2 );
		}
		else
		{
			clientInRoomTarget->m_byVoteCount += 1;
			clientInRoomSrc->m_byVoteToCount += 3;
			if( clientInRoomSrc->m_pClient )
			{
				clientInRoomSrc->m_pClient->IncToupiaoCount(1);
			}
			// 增加获得的投票数
			pTarget->IncVoteCount( 3 );
		}
	}
	else
	{
		clientInRoomTarget->m_byVoteCount += 1;
		clientInRoomSrc->m_byVoteToCount += 1;
		if( clientInRoomSrc->m_pClient )
		{
			clientInRoomSrc->m_pClient->IncToupiaoCount(1);
		}
		// 增加获得的投票数
		pTarget->IncVoteCount( 1 );
	}
#ifdef _FB_LOGGER
	Global::fb_logger->debug("\t%u\tVOTES FOR ANOTHER\t%d\t%s\t%d\t%s",Global::m_stRTime.GetSec(),pClient->GetCharID(),pClient->GetAccName(),pTarget->GetCharID(),pTarget->GetAccName());
#endif

	SendVote( pClient->GetGUID() , pTarget->GetGUID() );

	SendVoteResult( pTarget->GetGUID() , clientInRoomTarget->m_byVoteCount );
}

bool MODI_NormalGameRoom::IsAllSpectarorListernOver() const
{
	for( DWORD i = 0; i < m_nSpectatorMaxSize; i++ )
	{
		MODI_ClientAvatar * pTemp = m_Spectators[i].m_pClient;
		if( pTemp )
		{
			if( m_Spectators[i].m_bListernOver == false )
				return false;
		}
	}

	return true;
}

void MODI_NormalGameRoom::OnClientExitPlayerState(MODI_GUID nClientID)
{
	for (DWORD n = 0; n < m_nSpectatorMaxSize; n++)
	{
		if (m_Spectators[n].m_pClient)
		{
			if( nClientID == m_Spectators[n].m_pClient->GetRoleNormalInfo().m_SingerID )
			{
				// 分配个新的对象
				MODI_GUID nNewSongerGUID  = GetSingerForSpector();
				if( nNewSongerGUID != INVAILD_GUID )
				{
					// 改变听歌对象
					this->ChangeObSinger( m_Spectators[n].m_pClient , nNewSongerGUID );
				}
				else 
				{
					// 没有新的听歌对象，重置ID
					m_Spectators[n].m_pClient->SetSingerGUID( INVAILD_GUID );
				}
			}
		}
	}
}

void MODI_NormalGameRoom::SetStateMask( defRoomStateMask nSet )
{
	GetNormalInfo_Modfiy().m_nStateMask = nSet;
}

void MODI_NormalGameRoom::SyncImpacts( MODI_ClientAvatar * pClient )
{
	for( DWORD i = 0; i < m_nPlayerMaxSize; i++ )
	{
		MODI_ClientAvatar * pPlayer = m_Players[i].m_pClient;
		if( pPlayer && pPlayer != pClient )
		{
			pPlayer->GetImpactlist().SendImpactsTo( pClient );
		}
	}
}

void MODI_NormalGameRoom::SyncExpression( MODI_ClientAvatar * pClient )
{
	for( DWORD i = 0; i < m_nPlayerMaxSize; i++ )
	{
		MODI_ClientAvatar * pPlayer = m_Players[i].m_pClient;
		if( pPlayer && pPlayer != pClient && pPlayer->IsPlayExpression() )
		{
			pPlayer->SyncPlayExpression( pClient );
		}
	}
}

bool MODI_NormalGameRoom::KickSb(MODI_ClientAvatar * pClient , defRoomPosSolt nTargetPos)
{
    bool bIsPlayer = RoleHelpFuns::IsSpectator(nTargetPos) ? false : true; //  是否参战玩家
    defRoomPosSolt nPos = RoleHelpFuns::GetPosSolt(nTargetPos); // 位置索引

    // 位置上是否有人
    MODI_ClientAvatar * pTarget = GetClientAvatarByPos(bIsPlayer, nPos);
    if (pTarget)
    {
        if( LeaveRoom(pTarget, enLeaveReason_Kick) )
		{
			Global::logger->debug("[%s] master <%s> req kick player<%s> <room:%d , on : %s , pos: %d> success .", 
				GS_ROOMOPT,
				pClient->GetRoleName() ,
				pTarget->GetRoleName() , 
				GetRoomID(),
			   	bIsPlayer ? "player" : "spector" , 
				nPos );

			return true;
		}
    }
	else 
	{
		Global::logger->debug("[%s] master <%s> req kick player <room:%d , on : %s , pos: %d>  faild.", 
				GS_ROOMOPT,pClient->GetRoleName() ,
				GetRoomID(),
			   	bIsPlayer ? "player" : "spector" , 
				nPos );
	}

    return false;
}


DWORD 	MODI_NormalGameRoom::GetGameResult_Score(const MODI_GUID & guid ) const
{
	DWORD nRet = 0;
	if( guid.IsInvalid()  == false )
	{
		for( size_t n = 0; n < MAX_PLAYSLOT_INROOM; n++ )
		{
			if( m_PlayersGameResults[n].m_nClientID == guid  )
			{
				nRet = m_PlayersGameResults[n].m_nTotalScore;
			   	break;	
			}
		}
	}

	return nRet;
}

void 	MODI_NormalGameRoom::SetGameResult_Score( const MODI_GUID & guid , DWORD nSet ) 
{
	if( guid.IsInvalid()  == false )
	{
		for( size_t n = 0; n < MAX_PLAYSLOT_INROOM; n++ )
		{
			if( m_PlayersGameResults[n].m_nClientID == guid  )
			{
				m_PlayersGameResults[n].m_nTotalScore = nSet;
			   	break;	
			}
		}
	}
}

void 	MODI_NormalGameRoom::AdujstGameResult_Score( const MODI_GUID & guid , long lValue )
{
	if( guid.IsInvalid()  == false )
	{
		for( size_t n = 0; n < MAX_PLAYSLOT_INROOM; n++ )
		{
			if( m_PlayersGameResults[n].m_nClientID == guid  )
			{
				if( lValue < 0 )
				{
					long l = -lValue;
					if( l >= m_PlayersGameResults[n].m_nTotalScore )
					{
						m_PlayersGameResults[n].m_nTotalScore = 0;
					}
					else 
					{
						m_PlayersGameResults[n].m_nTotalScore -= l;
					}
				}
				else 
				{
					m_PlayersGameResults[n].m_nTotalScore += lValue;
				}
			   	break;	
			}
		}
	}
}



/** 
 * @brief 更换组队
 * 
 */
void MODI_NormalGameRoom::ChangeTeam(MODI_ClientAvatar * pClient, BYTE team_state)
{
	bool bIsPlayer = RoleHelpFuns::IsSpectator(pClient->GetRoleNormalInfo().m_roomSolt) ? false : true; //  是否参战玩家
	if( !bIsPlayer )
	{
		Global::logger->debug("[%s] room<%u> player<%s> change team status update to player is spectator, cannot switch status", 
							  GS_ROOMOPT ,
							  GetRoomID() , 
							  pClient->GetRoleName() );
		return ;
	}

    defRoomPosSolt nPos = RoleHelpFuns::GetPosSolt(pClient->GetRoleNormalInfo().m_roomSolt); // 位置索引
    MODI_ClientAvatarInRoom & client = *GetClientInRoomByPos(true, nPos);
	MODI_ASSERT( client.m_pClient == pClient );
	if(client.m_pClient == pClient)
	{
		// 有人改变组队
		m_byTeamState[nPos] = team_state;
		BroadcastTeamState();

		Global::logger->debug("[%s] room<%u> player<%s><pos=%u> team status update to <%d> : successful. ", 
							  GS_ROOMOPT ,
							  GetRoomID() , 
							  pClient->GetRoleName(), nPos, team_state);
	}
}



/** 
 * @brief 设置组队位置
 * 
 */
void MODI_NormalGameRoom::SetTeam(defRoomPosSolt & n)
{
	if(n >= MAX_PLAYSLOT_INROOM)
	{
		MODI_ASSERT(0);
		return;
	}

	BYTE red_team = 0;
	BYTE yellow_team = 0;
	BYTE blue_team = 0;

	for(int i=0; i<MAX_PLAYSLOT_INROOM; i++)
	{
		if(m_byTeamState[i] == ROOM_TEAM_RED)
		{
			red_team++;
		}
		else if(m_byTeamState[i] == ROOM_TEAM_YELLOW)
		{
			yellow_team++;
		}
		else if(m_byTeamState[i] == ROOM_TEAM_BLUE)
		{
			blue_team++;
		}
	}
/*
	if(red_team < 2)
	{
		m_byTeamState[n] = ROOM_TEAM_RED;
	}
	else if(yellow_team < 2)
	{
		m_byTeamState[n] = ROOM_TEAM_YELLOW;
	}
	else if(blue_team < 2)
	{
		m_byTeamState[n] = ROOM_TEAM_BLUE;
	}
	else
	{
		MODI_ASSERT(0);
	}
	*/
	
	unsigned char a[3];
	a[0] = red_team;
	a[1] = yellow_team;
	a[2] = blue_team;
	unsigned char tmp;

	if( a[0] > a[2])
	{
		tmp = a[0];
		a[0] = a[2];
		a[2] =  tmp;
	}
	if( a[1] > a[2])
	{
		tmp = a[1];
		a[1] = a[2];
		a[2] = tmp;
	}
	if( a[0] > a[1])
	{
		tmp = a[0];
		a[0] = a[1];
		a[1] = tmp;
	}
	if( a[1] == a[2]  && a[1] == 2)
	{
		a[1] = a[0];
	}
	if( a[1] == red_team )
	{
		m_byTeamState[n] = ROOM_TEAM_RED;
	}
	else if( a[1] == yellow_team)
	{
		m_byTeamState[n] = ROOM_TEAM_YELLOW;
	}
	else 
	{
		m_byTeamState[n] = ROOM_TEAM_BLUE;
	}
}


/** 
 * @brief 设置组队位置
 * 
 */
void MODI_NormalGameRoom::ClearTeam(defRoomPosSolt & n)
{
	if(n >= MAX_PLAYSLOT_INROOM)
	{
		MODI_ASSERT(0);
		return;
	}
	m_byTeamState[n] = 0;
}


/** 
 * @brief 广播组队状态
 * 
 */
void MODI_NormalGameRoom::BroadcastTeamState()
{
	/*
	unsigned int n=0;
	for( n = 0; n< m_nPlayerMaxSize; n++)
	{

		if( ! m_Players[n].m_pClient)
		{
			m_byTeamState[n] = 0;
		}
	}	
	*/
	MODI_GS2C_Notify_RoomTeam_State send_cmd;
	send_cmd.m_byState[0] = (BYTE)m_byTeamState[0];
	send_cmd.m_byState[1] = (BYTE)m_byTeamState[1];
	send_cmd.m_byState[2] = (BYTE)m_byTeamState[2];
	send_cmd.m_byState[3] = (BYTE)m_byTeamState[3];
	send_cmd.m_byState[4] = (BYTE)m_byTeamState[4];
	send_cmd.m_byState[5] = (BYTE)m_byTeamState[5];
	Broadcast( &send_cmd, sizeof(send_cmd) );
}


/** 
 * @brief 改变状态
 * 
 */
void MODI_NormalGameRoom::OnChangeGameMode( ROOM_SUB_TYPE oldMode , ROOM_SUB_TYPE newMode )
{
	MODI_MultiGameRoom::OnChangeGameMode(oldMode, newMode);
	if(newMode == ROOM_STYPE_TEAM || newMode == ROOM_STYPE_KEYBOARD_TEAM)
	{
		SetAllTeam();
		BroadcastTeamState();
	}
}



/** 
 * @brief 一个按键的得分
 * 
 */
bool MODI_NormalGameRoom::GameScore_Key(MODI_ClientAvatar * pClient ,  float fTime , 
			DWORD nScore , BYTE byType , float fExact , 
			 WORD nCombat ,float fFever)
{
	
	if( GetSubType() != ROOM_STYPE_KEYBOARD &&  GetSubType() != ROOM_STYPE_KEYBOARD_TEAM)
	{
		Global::logger->warn("[gamescore] gamescore type didn't match the game's mode <roomid=%u,name=%s,score=%u>",GetRoomID(),pClient->GetRoleName(),nScore); 
		return false;
	}
	if (IsPlaying() == false )
	{
		Global::logger->warn("[%s] room <%u> not in match game status , but player <%s> sent score of lyric", 
				GS_ROOMOPT,
				pClient->GetRoomID(), 
				pClient->GetRoleName());
		return false;
	}


	// 非参战玩家不能发送分送
	if( !pClient->IsPlayer() )
	{
		Global::logger->warn("[%s] room <%u> player <%s> is spectator but sent score." , 
				GS_ROOMOPT ,
				pClient->GetRoomID() , 
				pClient->GetRoleName() );
		return false;
	}

	MODI_GameResult * pGameResult = GetGameResult( pClient );
	if( !pGameResult )
	{
		return false;
	}

	if( nCombat >= m_nMaxCombat)
	{
		
		Global::logger->warn("[keyscore_error] <accname=%s,comb=%d,max=%d>",pClient->GetRoleName(), pClient->GetAccountID(),nCombat,m_nMaxCombat );
		return false;
	}

	if( byType > enScore_Begin && byType < enScore_End && fExact <= 1.0f  )
	{
		DWORD	tmp_Score=nScore;

		//DWORD	tmp_Sstate=0;
		switch( byType )
		{
			case enScore_Bad:
				pGameResult->m_byBad += 1;
				pClient->SetGoodPlusCount( 0 );
		//		tmp_Sstate=(nScore/5);
				break;
			case enScore_Cool:
				pGameResult->m_byCool += 1;
				pClient->SetGoodPlusCount( pClient->GetGoodPlusCount() + 1 );
				pClient->UpdateMaxGoodPlusCount();
				pGameResult->m_byCombo = pClient->GetMaxGoodPlusCount();
		//		tmp_Sstate=(nScore*8/10);
				break;
			case enScore_Miss:
				pGameResult->m_byMiss += 1;
				pClient->SetGoodPlusCount( 0 );
		//		tmp_Sstate=0;
				
				break;
			case enScore_Pefect:
				pGameResult->m_byPefect += 1;
				pClient->SetGoodPlusCount( pClient->GetGoodPlusCount() + 1 );
				pClient->UpdateMaxGoodPlusCount();
				pGameResult->m_byCombo = pClient->GetMaxGoodPlusCount();
		//		tmp_Sstate=nScore;
				break;
			case enScore_Good:
				pGameResult->m_byGood += 1;
		//		tmp_Sstate=(nScore/2);
				break;
		}
	        //DWORD	tmp_Good=pClient->GetGoodPlusCount();	
		//nScore = tmp_Sstate*(1+tmp_Good/5);
		//Global::logger->debug("[~~~~~~~~~] pClient name=%s,nmiss=%u,nbad=%u,ngood=%u,ncool=%u,nperfect=%u",pClient->GetRoleName(),pGameResult->m_byMiss,pGameResult->m_byBad,pGameResult->m_byGood,pGameResult->m_byCool,pGameResult->m_byPefect);
			
		pGameResult->m_nTotalScore += tmp_Score;
		pGameResult->m_fExact = fExact;
		pGameResult->m_nSentensCount += 1;
		
	

		// 增加唱对的秒数
	
		MODI_GS2C_Notify_OneKeyScore notifySentenceScore;

		// 该房间是否还能产生SHOW TIME效果
		if( IsCanProduceShowTime() )
		{
			if( fExact > 2.0f )
			{
				// show time 效果
				OnShowTime( pClient );
			}
		}

		notifySentenceScore.m_nClient = pClient->GetGUID();
		notifySentenceScore.m_nScore = pGameResult->m_nTotalScore;
		notifySentenceScore.m_byEvaluation = byType;
		notifySentenceScore.m_nCombat = nCombat;
		notifySentenceScore.m_fTime = fTime;
		notifySentenceScore.m_tFever = fFever;

		Broadcast( &notifySentenceScore , sizeof(notifySentenceScore));

	}	
	else 
	{
		Global::logger->warn("The parameter of <%u>the lyric score is incorrect. score:<%u>, level:<%u>, precision:<%3f> ." , 
				GS_ROOMOPT ,
		        pGameResult->m_nSentensCount + 1 ,
				nScore , 
				byType , 
				fExact );
	}
	
	return true;
}



/** 
 * @brief 一个按键的得分
 * 
 */
void MODI_NormalGameRoom::OnFirstHalfOk( MODI_ClientAvatar * pClient)
{

/*	if(!pClient->IsPlayer())
		AllocSongerForNoOBSpector();
		*/
}

/** 
 * @brief 一个按键的得分
 * 
 */
void MODI_NormalGameRoom::ComputeExpAndMoney(BYTE	 n,DWORD &m_nExp,DWORD &m_nMoney)
{
	
	DWORD nOldAddExp =0;
	DWORD nAddExp =0;
	DWORD nMoney =0;
	if( GetSubType() == ROOM_STYPE_KEYBOARD || GetSubType() == ROOM_STYPE_SINGLE_2KEY || GetSubType() == ROOM_STYPE_KEYBOARD_TEAM)
	{
		nOldAddExp= MODI_Aformula::ComputeKeyExp( m_PlayersGameResults[n].m_nTotalScore , 
								m_PlayersGameResults[n].m_fExact , 1.0f);
		
		// 计算获得经验
		 nAddExp= MODI_Aformula::ComputeKeyExp( m_PlayersGameResults[n].m_nTotalScore , 
				m_PlayersGameResults[n].m_fExact , 
				m_Players[n].m_pClient->GetExpModifyFactor() );

	}
	else
	{
		nOldAddExp= MODI_Aformula::ComputeExp( m_PlayersGameResults[n].m_nTotalScore , 
								m_PlayersGameResults[n].m_fExact , 1.0f);
		
		// 计算获得经验
		 nAddExp= MODI_Aformula::ComputeExp( m_PlayersGameResults[n].m_nTotalScore , 
				m_PlayersGameResults[n].m_fExact , 
				m_Players[n].m_pClient->GetExpModifyFactor() );
	}

	nAddExp = (DWORD)(nAddExp * m_Players[n].m_pClient->GetChenmiPro());

	nMoney = MODI_Aformula::ComputeMoney( nOldAddExp, 
			m_PlayersGameResults[n].m_fExact ,
			m_Players[n].m_pClient->GetMoneyFactor() );

	nMoney = (DWORD)(nMoney * m_Players[n].m_pClient->GetChenmiPro());
	

	m_nExp = nAddExp;
	m_nMoney = nMoney;

	///	经验，金币惩罚控制
	m_nExp *= m_Players[n].m_pClient->GetExppenalty();
	m_nExp /= 100;
	///VIP经验修正

	m_nMoney *= m_Players[n].m_pClient->GetMoneypenalty();
	m_nMoney /= 100;

	if( m_Players[n].m_pClient->IsVip())
	{
		m_nExp *= VIP_EXP ;
		m_nExp /= 100;
		m_nMoney *= VIP_MONEY;
		m_nMoney /= 100;
	}
/* disable money - andryan */
	m_nMoney = 0;
/* end of disable money - andryan */

#ifdef _XXP_DEBUG

		Global::logger->debug("[cmpute_exp_and_money] <client=%s,total_score=%u,exact=%f,factor=%f,exp=%u,money=%u,befexp=%u,befmoney=%u> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~",m_Players[n].m_pClient->GetRoleName(),m_PlayersGameResults[n].m_nTotalScore,m_PlayersGameResults[n].m_fExact,m_Players[n].m_pClient->GetExpModifyFactor(),m_nExp,m_nMoney,nAddExp,nMoney);
#endif
}

bool	MODI_NormalGameRoom::HasPreOwnerExcept( MODI_ClientAvatar * pClient)
{
	unsigned int i=0;
	/// 先不限制
#ifdef _ML_VERSION
#else	
	if(GetRoomType() == ROOM_TYPE_VIP)
	{
		for( i=0; i< m_nPlayerMaxSize; ++i )
		{
			if( m_Players[i].m_pClient && m_Players[i].m_pClient != pClient &&  AVATAR_CAN(m_Players[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				if(m_Players[i].m_pClient->IsVip())
				{
					return true;
				}
			}
		}	
		for( i=0;  i< m_nSpectatorMaxSize ; ++i )
		{

			if( m_Spectators[i].m_pClient &&  m_Spectators[i].m_pClient != pClient && AVATAR_CAN(m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER) )
			{
				if(m_Spectators[i].m_pClient->IsVip())
				{
					return true;
				}
			}
		}
	}
	else
#endif		
	{
		for( i=0; i< m_nPlayerMaxSize; ++i )
		{
			if( m_Players[i].m_pClient && m_Players[i].m_pClient != pClient &&  AVATAR_CAN(m_Players[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				return true;
			}
		}	
		for( i=0;  i< m_nSpectatorMaxSize ; ++i )
		{

			if( m_Spectators[i].m_pClient &&  m_Spectators[i].m_pClient != pClient && AVATAR_CAN(m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER) )
			{
				return true;
			}
		}
	}
	return false;
}

void	MODI_NormalGameRoom::KillAllExcept( MODI_ClientAvatar  * pClient)
{
	
	unsigned int i=0;
	for( i=0; i< m_nPlayerMaxSize; ++i )
	{
		if( m_Players[i].m_pClient && m_Players[i].m_pClient != pClient )
		{
			/// 先不区分
#ifdef _ML_VERSION
#else			
			if(GetRoomType() == ROOM_TYPE_VIP)
			{
				LeaveRoom( m_Players[i].m_pClient,enLeaveReason_Vip);
			}
			else
#endif				
			{
				LeaveRoom( m_Players[i].m_pClient,enLeaveReason_Chenghao);
			}
		}
	}	
	for( i=0;  i< m_nSpectatorMaxSize ; ++i )
	{

		if( m_Spectators[i].m_pClient &&  m_Spectators[i].m_pClient != pClient )
		{
#ifdef _ML_VERSION
#else			
			if(GetRoomType() == ROOM_TYPE_VIP)
			{
				LeaveRoom( m_Spectators[i].m_pClient,enLeaveReason_Vip);
			}
			else
#endif				
			{
				LeaveRoom( m_Spectators[i].m_pClient,enLeaveReason_Chenghao);
			}
		}
	}

}

void	MODI_NormalGameRoom::ReFixOwner()
{
	unsigned int i=0;
	MODI_ClientAvatar  * ava=NULL;
	bool	change = false;

	for( i=0; i< m_nPlayerMaxSize; ++i )
	{
		if( m_Players[i].m_pClient )
		{
			if( NULL == ava  && AVATAR_CAN(m_Players[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				ava = m_Players[i].m_pClient;
			}
			if( m_Players[i].m_pClient->GetGUID() == GetMaster()  &&  AVATAR_CAN(m_Players[i].m_pClient,PREVILEGE_ROOMOWNER))
			{
				return;
			}
			if( m_Players[i].m_pClient->GetGUID() == GetMaster())
			{
				change = true;
			}
		}
	}	
	if( change && ava)
	{

		SetPassword(0);

		MODI_GS2C_Notify_RoomInfoChanged notify;
		memcpy( notify.m_szRoomName , GetRoomInfo().m_cstrRoomName , sizeof(notify.m_szRoomName) );
		notify.m_bHasPwd = false;
		notify.m_ucNewOffSpectator = IsAllowSpectator();
		Broadcast( &notify , sizeof(notify) );
		SetMaster( ava->GetGUID() );
		
		OnMasterChanged( ava);
		return;
	}
	for( i=0;  i< m_nSpectatorMaxSize ; ++i )
	{
		if( ! m_Spectators[i].m_pClient)
			continue;

		if( NULL == ava  && AVATAR_CAN(m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER))
		{
			ava = m_Spectators[i].m_pClient;
		}

		if( m_Spectators[i].m_pClient->GetGUID() == GetMaster()  &&  AVATAR_CAN(m_Spectators[i].m_pClient,PREVILEGE_ROOMOWNER))
		{
			return;
		}
		if( m_Spectators[i].m_pClient->GetGUID() == GetMaster())
		{
			change = true;
		}
	}
	if( change && ava)
	{
		SetPassword(0);

		MODI_GS2C_Notify_RoomInfoChanged notify;
		memcpy( notify.m_szRoomName , GetRoomInfo().m_cstrRoomName , sizeof(notify.m_szRoomName) );
		notify.m_bHasPwd = false;
		notify.m_ucNewOffSpectator = IsAllowSpectator();
		Broadcast( &notify , sizeof(notify) );
		SetMaster( ava->GetGUID() );
		
		OnMasterChanged( ava);
		return ;
	}


}	






