#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
/**
 * @file   UseItemHandler.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 29 16:34:09 2011
 * 
 * @brief  道具使用命令
 * 
 * 
 */

#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"


/** 
 * @brief 道具使用
 * 
 */
int MODI_PackageHandler_c2gs::UseItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	MODI_ClientAvatar * p_client = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_UseItem_Cmd , cmd_size , 0 , 0 , p_client);

	const MODI_C2GS_UseItem_Cmd * p_recv_cmd = (const MODI_C2GS_UseItem_Cmd *)(pt_null_cmd);
	
	if(p_recv_cmd->m_qdItemId == 0 ||  p_recv_cmd->m_qdTargetId.IsInvalid() == true)
	{
		Global::logger->error("[assert_use_item] use item failed <name=%s,itemid=%llu,targetid=%llu>", p_client->GetRoleName(),
							  p_recv_cmd->m_qdItemId, (QWORD)p_recv_cmd->m_qdTargetId);
		MODI_ASSERT(0);
		return enPHandler_Kick;
	}
	
	MODI_ItemOperator::UseItem(p_client, p_recv_cmd->m_qdItemId, p_recv_cmd->m_qdTargetId);
	return enPHandler_OK;
}
