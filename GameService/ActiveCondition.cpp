#include "ActiveCondition.h"

#include "Global.h"
#include "SplitString.h"
#include "Condition.h"
#include "GameUserVar.h"
#include "GameTick.h"

static int GetMonDays(int month, int year)
{
	int 	a[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	bool	leap;	

	leap=( year % 400 ==0 || year % 4== 0 && year % 100 != 0 );
	return ( a[month-1] + (leap && (month== 2 ) ));
}
static BYTE	GetDays(DWORD  condition,int days)
{
	DWORD	j=0x01;
	BYTE	n=0;
	int i=0;
	condition &= 0xfffffffe;
	condition >>= 1;
	for( i=0; i< days; ++i)
	{
		if( condition & j)
			n++;		
		condition >>= 1;
	}
	return n;
}


bool MODI_ActiveSignupdays::Init(const char * con_line)
{
	if( !con_line)
		return false;
	MODI_SplitString split;
	split.InitXML(con_line);

	m_stOp = split["op"];
	m_wdCondition = atoi(split["condition"]);

	return true;
}
bool MODI_ActiveSignupdays::is_valid(MODI_ClientAvatar * p_client)
{
	WORD cur_year = MODI_GameTick::m_stTTime.GetYear();
	WORD cur_mon = MODI_GameTick::m_stTTime.GetMon();
//	BYTE cur_day = MODI_GameTick::m_stTTime.GetMDay();
//	BYTE all_days;
	std::ostringstream os;
	os.str("");
	os<<"Sigup_";
	os<<cur_year<<"_";
	os<<cur_mon;

#define VAR_TIME_TYPE	"type=normal_del=21990901"
	MODI_GameUserVar * p_var = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());

	if(! p_var)
	{
		p_var = new MODI_GameUserVar(os.str().c_str());
		if(p_var->Init(p_client, VAR_TIME_TYPE))
		{
			if(! p_client->m_stGameUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}
	os<<"_";
	MODI_GameUserVar * p_var2 = p_client->m_stGameUserVarMgr.GetVar(os.str().c_str());
	if( !p_var2)
	{
		p_var2 = new MODI_GameUserVar(os.str().c_str());	
		if( p_var2->Init(p_client,VAR_TIME_TYPE))
		{

			if(! p_client->m_stGameUserVarMgr.AddVar(p_var2))
			{
				delete p_var2;
				p_var2 = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var2->SaveToDB();
			}
		}
		else
		{
			delete p_var2;
			p_var2 = NULL;
			MODI_ASSERT(0);

		}
	}


	DWORD	condition=0;
	DWORD	tmp=0;
	condition |= p_var->GetValue();
	condition &= 0xffff;
	tmp |= p_var2->GetValue();
	tmp &= 0xffff;
	tmp <<= 16;
	condition |= tmp;	

	int days_2 = GetMonDays(cur_mon,cur_year);
	Global::logger->debug("-----------------------------------------------------------------GetDays    ----%d  ",days_2);
	BYTE days = GetDays(condition,days_2);

	/*if( cur_mon != 1)
	{
		if( cur_mon %2 == 0)
		{
			all_days = 30;
		}
		else
		{
			all_days = 31;
		}
	}
	else
	{
		if( cur_year %4 == 0 )
		{
			all_days = 29;
		}
		else
			all_days = 30;
	}
	*/
	if( !p_client )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( m_stOp == "" )
	{
		Global::logger->fatal("No opreate found !!!!!");
		MODI_ASSERT(0);
		return false;
	}

	if( m_stOp == "equ")
	{
		return ( days ==  m_wdCondition);
			///	检查添加获奖信息
			//
	}
	else if( m_stOp == "all")
	{
		int i=0;
		unsigned int mask = 0x01;
		condition >>= 1;
		for( i=0; i< days_2; ++i)
		{
			if( condition & mask )
			{
				condition >>= 1;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
	return true;
}




