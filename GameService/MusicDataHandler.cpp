#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "BinFileMgr.h"
#include "GameTick.h"
#include "ItemOperator.h"
#include "ImpactTemplateID.h"


///     客户端请求改变收听的对象
int MODI_PackageHandler_c2gs::ChangeObSinger(void * pObj,const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ChangeObSinger , cmd_size , 0, 0, pClient );
	if( !pClient->IsInMulitRoom() )
	{
		return enPHandler_Kick;
	}

	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	MODI_MultiGameRoom * room = pLobby->GetMultiRoom( pClient->GetRoomID() );
	if( !room )
	{
		return enPHandler_OK;
	}

	MODI_C2GS_Request_ChangeObSinger * pReq = (MODI_C2GS_Request_ChangeObSinger *)(pt_null_cmd);

	if( pReq )
	{
		if( room->IsPlaying() && !pClient->IsPlayer() )
		{
//			MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
//			if( pClient->GetChangeSongerTimer()( pTick->GetTimer() ) == false )
//			if( false )
//			{
//				Global::logger->warn("[%s] client<RoleName=%s> in room<%u> , frequent request change songer ." , 
//						GS_ROOMOPT , 
//					   pClient->GetRoleName() , 
//					   room->GetRoomID() );	
//				return enPHandler_Warning;
//			}
		}
		
		if( room->ChangeObSinger( pClient , pReq->m_SingerID ) )
		{
			if( room->IsPlaying() )
			{
				MODI_GameTick * pTick = MODI_GameTick::GetInstancePtr();
				pClient->GetChangeSongerTimer().Reload( CHANGE_SONGER_TIMER , 
						pTick->GetTimer() );
			}
		}
	}

	return enPHandler_OK;
}


/// 录音卡使用
int MODI_PackageHandler_c2gs::RecordMusic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_RecordMusic , cmd_size , 0, 0, pClient );
	const MODI_C2GS_Request_RecordMusic * p_recv_cmd = (const MODI_C2GS_Request_RecordMusic *)pt_null_cmd;
	MODI_GS2C_Notify_RecordMusicRes res;
	res.result = MODI_GS2C_Notify_RecordMusicRes::kFaild;
	if( !pClient->IsInSingleGame() )
	{
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_Kick;
	}

	if(MODI_ItemOperator::UseItem(pClient, p_recv_cmd->m_qdItemId, pClient->GetGUID()) != MODI_ItemOperator::enOK)
	{
		res.result = MODI_GS2C_Notify_RecordMusicRes::kNoItem;
		pClient->SendPackage(&res,sizeof(res));
		Global::logger->error("[item_record] client req record,but not have item <name=%s,itemid=%llu>", 
							  pClient->GetRoleName(), p_recv_cmd->m_qdItemId);
		return enPHandler_OK;
	}
	
	//res.result = MODI_GS2C_Notify_RecordMusicRes::kSuccessful;
	pClient->SendPackage( &res , sizeof(res) );
	Global::logger->debug("[record_music] record successful <name=%s,itemid=%llu>", pClient->GetRoleName(), p_recv_cmd->m_qdItemId);
	return enPHandler_OK;
}
