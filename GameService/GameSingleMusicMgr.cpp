#include "GameSingleMusicMgr.h"
#include "protocol/c2gs_Activity.h"
#include "s2zs_cmd.h"
#include "ZoneClient.h"
#include "GameChannel.h"

MODI_GameMusicMgr * MODI_GameMusicMgr::m_pInstance = NULL;

void	MODI_GameMusicMgr::Cleanup(enGameMode mode)
{
	if( mode == enGameMode_Sing)
	{
		defSingleMapIter  iter =  m_mMusicMap.begin();
		for( ; iter != m_mMusicMap.end(); ++iter)
		{
			if( iter->second == NULL)
			{
				MODI_ASSERT(0);
			}
			delete iter->second;
		}	
		m_mMusicMap.clear();
	}
	else
	{
		defSingleMapIter  iter =  m_mKeyMusicMap.begin();
		for( ; iter != m_mKeyMusicMap.end(); ++iter)
		{
			if( iter->second == NULL)
			{
				MODI_ASSERT(0);
			}
			delete iter->second;
		}	
		m_mKeyMusicMap.clear();
	}
}

/**
 * @brief 从数据库直接加载单曲列表信息
 *
 */
bool	MODI_GameMusicMgr::Init( size_t size,struct SingleMusicInfo * p,enGameMode mode_t)
{
	Cleanup(mode_t);	
	size_t i=0;

	if( mode_t  == enGameMode_Sing)
	{
		for( i=0; i< size ; ++i)
		{
			struct SingleMusicInfo * newinfo= new SingleMusicInfo();
			MODI_ASSERT(newinfo);
			*newinfo = *p;
			InsertMusic(newinfo,mode_t);
			p++;
		}
	}
	else
	{

		for( i=0; i< size ; ++i)
		{
			struct SingleMusicInfo * newinfo= new SingleMusicInfo();
			MODI_ASSERT(newinfo);
			*newinfo = *p;
			InsertMusic(newinfo,mode_t);
			p++;
		}

	}
	return true;
}

bool	MODI_GameMusicMgr::InsertMusic( SingleMusicInfo * p_music, enGameMode mode_t)
{
	if( !p_music)
		return false;

	if( mode_t == enGameMode_Sing)
	{
		defSingleMapIter  itor = m_mMusicMap.find(p_music->m_wMusicid);
		if( itor != m_mMusicMap.end())
		{
			return false;
		}	

		m_mMusicMap.insert(defSingleMapValue(p_music->m_wMusicid,p_music));	
	}
	else
	{
		defSingleMapIter  itor = m_mKeyMusicMap.find(p_music->m_wMusicid);
		if( itor != m_mKeyMusicMap.end())
		{
			return false;
		}	

		m_mKeyMusicMap.insert(defSingleMapValue(p_music->m_wMusicid,p_music));	

	}
	return true;
}

SingleMusicInfo  * MODI_GameMusicMgr::GetMusic(WORD id,enGameMode mode_t)
{
	if( mode_t == enGameMode_Sing)
	{
		defSingleMapIter  itor = m_mMusicMap.find(id);
		if( itor != m_mMusicMap.end())
		{
			return itor->second;
		}
	}
	else
	{
		defSingleMapIter  itor = m_mKeyMusicMap.find(id);
		if( itor != m_mKeyMusicMap.end())
		{
			return itor->second;
		}

	}
	return NULL;
}

WORD 	MODI_GameMusicMgr::GenPackage(SingleMusicInfo * array, enGameMode mode)
{
	SingleMusicInfo  * pInfo=(SingleMusicInfo * ) array;
	WORD	i=0;
	if( mode == enGameMode_Sing)
	{
		defSingleMapIter   iter = m_mMusicMap.begin();
		for( ; iter != m_mMusicMap.end(); ++iter)
		{
			pInfo[i] = *(iter->second);	
			++i;
		}
	}
	else
	{
		defSingleMapIter   iter = m_mKeyMusicMap.begin();
		for( ; iter != m_mKeyMusicMap.end(); ++iter)
		{
			pInfo[i] = *(iter->second);	
			++i;
		}
	}
	return i;
}	

bool	MODI_GameMusicMgr::OnRecvMusicInfo(SingleMusicInfo * p_music, enGameMode mode)
{
	
	///  每当房间演唱完毕的时候，会构建一个单曲信息表
	//经过该函数检测，然后同步给ZS
	//
	MODI_S2ZS_Request_MusicEnd   Music;
	Music.m_stMusic = *p_music;
	Music.m_eMode = mode;
	MODI_ZoneServerClient::GetInstancePtr()->SendCmd(&Music,sizeof( MODI_S2ZS_Request_MusicEnd));

	return true;
}


void	MODI_GameMusicMgr::SyncToClient(MODI_ClientAvatar *pclient,enGameMode mode)
{

	char	m_szPacket[Skt::MAX_USERDATASIZE];
	size_t 	size=0;
	MODI_GS2C_Notify_SingleMusicList * notify= (MODI_GS2C_Notify_SingleMusicList *)m_szPacket;
	AutoConstruct(notify);

	notify->m_wNum = GenPackage( notify->m_stMusic,mode);
	notify->m_eMode = mode;

	size = sizeof(MODI_GS2C_Notify_SingleMusicList);
	size +=(  notify->m_wNum * sizeof(SingleMusicInfo));
	pclient->SendPackage(notify,size);

}
// 分为五批更新到客户端
// 每一分钟刷新一次
void	MODI_GameMusicMgr::Update()
{
	static	BYTE	perk0= 0;		
	static	BYTE	perk1= 0;		
	char	m_szPacket[Skt::MAX_USERDATASIZE];
	size_t 	size=0;
	MODI_GS2C_Notify_SingleMusicList * notify= (MODI_GS2C_Notify_SingleMusicList *)m_szPacket;
	AutoConstruct(notify);

	static BYTE i =0;
	++i;
	i %= 2;
	if( m_tTimer(Global::m_stRTime))
	{
		if( i == 0)
		{

			notify->m_wNum = GenPackage( notify->m_stMusic,enGameMode_Sing);
			notify->m_eMode = enGameMode_Sing;

			size = sizeof(MODI_GS2C_Notify_SingleMusicList);
			size +=(  notify->m_wNum * sizeof(SingleMusicInfo));
			MODI_GameChannel::GetInstancePtr()->Broadcast_mask(notify,size,perk0,5);
			++perk0;
			perk0 %= 5;
		}
		else
		{
			notify->m_wNum = GenPackage( notify->m_stMusic,enGameMode_Key);
			notify->m_eMode = enGameMode_Key;

			size = sizeof(MODI_GS2C_Notify_SingleMusicList);
			size +=(  notify->m_wNum * sizeof(SingleMusicInfo));
			MODI_GameChannel::GetInstancePtr()->Broadcast_mask(notify,size,perk1,5);
			++perk1;
			perk1 %= 5;
		}
	}
}




