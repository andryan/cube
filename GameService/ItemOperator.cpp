/**
 * @file   ItemOperator.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jun 16 10:18:07 2011
 * 
 * @brief  物品操作类
 * 
 * 
 */


#include "ItemOperator.h"
#include "AssertEx.h"
#include "BinFileMgr.h"
#include "HelpFuns.h"
#include "ClientAvatar.h"
#include "GUIDCreator.h"
#include "GameChannel.h"
#include "ItemRuler.h"
#include "TimeManager.h"
#include "ImpactCore.h"
#include "ImpactTemplateMgr.h"
#include "Bag.h"
#include "ScriptManager.h"
#include "RandomBox.h"

/** 
 * @brief 包裹是否够
 * 
 * @param p_bag 此包裹
 * @param config_id 配置id
 * @param item_count 道具数量
 * 
 * @return 够true
 *
 */
bool MODI_ItemOperator::CheckBagSpace(MODI_Bag * p_bag, const DWORD & config_id, const WORD & item_count)
{
	if(!p_bag || config_id == 0 || item_count == 0)
	{
		return false;
	}
	
	if(p_bag->GetEmptyPosCount() > 0)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否可以增加道具
 * 
 * @param p_client 该玩家
 * @param config_id 配置id
 * @param item_count 道具数量
 * 
 * @return 可以true
 *
 */
bool MODI_ItemOperator::CheckCanAddToClient(MODI_ClientAvatar * p_client, const DWORD & config_id, const WORD & item_count)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	/// 获取物品类型
	const MODI_ItemType item_type = ::GetItemType(config_id);
	if(item_type == enItemType_Unknow)
	{
		Global::logger->error("[check_item] check add to client failed <configid=%u>", config_id);
		MODI_ASSERT(0);
		return false;
	}

	/// 该类型拥有的数量是否限制(此和itemlist表MaxOwnNum字段感觉重叠了)
	/// 其实这样更好理解GetSubClassMaxOwn()返回true/false(true表示被限制了拥有数)
	/// 限制后则读表MaxOwnNum字段得到限制数，不然表字段MaxOwnNum就不用读了
	/// 为什么这个拥有限制不限制不放的表里面去呢? 而要用程序去配置，比较难理解
	
	WORD have_limit = 0;
	if(!MODI_ItemRuler::GetSubClassMaxOwn(config_id, have_limit))
	{
		Global::logger->error("[check_itme] get max_sub_own faild <configid=%u>", config_id);
		MODI_ASSERT(0);
		return false;
	}

	/// 该类型的道具有限制拥有
	if(have_limit != ITEM_SUBOWN_UNLIMITED && have_limit != 1 )
	{
		/// 最大拥有数量
		WORD max_own = 0;
		if(!MODI_ItemRuler::GetMaxOwn(config_id, max_own))
		{
			Global::logger->error("[check_itme] get max_own faild <configid=%u>", config_id);
			MODI_ASSERT(0);
			return false;
		}
	
		/// 玩家身上子类个数
		WORD have_count = p_client->GetItemCountBySubClass(item_type);
		
		/// 是否上限
		if((have_count + item_count) > max_own)
		{
			Global::logger->debug("[check_can_add] max own limit <configid=%u,have_count=%u,item_count=%u,max_own=%u>",
								  config_id, have_count, item_count, max_own);
			return false;
		}
	}

	return true;
}


/** 
 * @brief 是否等级符合
 * 
 */
enUseItemResult MODI_ItemOperator::CheckItemLevelReq( MODI_ClientAvatar * pCaster ,WORD nConfigID )
{
	/// 是否满足使用条件
	BYTE byReqLevel = 0;
	if( MODI_ItemRuler::GetReqLevel( nConfigID , byReqLevel ) == false )
	{
		return kUseItem_CanNot_Use;
	}

	/// 等级是否满足
	if( pCaster->GetLevel() < byReqLevel )
	{
		return kUseItem_Level_Fail;
	}

	return kUseItem_Ok;
}


/** 
 * @brief 性别是否符合
 * 
 */
enUseItemResult MODI_ItemOperator::CheckItemSexReq(  MODI_ClientAvatar * pCaster , WORD nConfigID )
{
	/// 是否满足使用条件
	BYTE byReqSex = enSexAll;
	if( MODI_ItemRuler::GetReqSex( nConfigID , byReqSex ) == false )
	{
		return kUseItem_CanNot_Use;
	}
	
	/// 性别是否满足
	if( byReqSex != enSexAll )
	{ 
		if( pCaster->GetSex() != byReqSex )
		{
			return kUseItem_Sex_Fail;
		}
	}
	
	return kUseItem_Ok;
}


/** 
 * @brief 游戏状态是否正确
 * 
 */
enUseItemResult MODI_ItemOperator::CheckItemGameStateReq(  MODI_ClientAvatar * pCaster , WORD nConfigID )
{
	/// 是否满足使用条件
	enItemCanUseInGameState canuseState;
	if( MODI_ItemRuler::GetCanUseInGameState( nConfigID , canuseState ) == false ) 
	{
		return kUseItem_CanNot_Use;
	}

	/// 游戏状态是否正确
	bool bGameStateError = false;
	if( canuseState != kItemCanUse_AllGameState )
	{
		MODI_MultiGameRoom * room = pCaster->GetMulitRoom();
		if( canuseState == kItemCanUse_InRoom )
		{
			// 只能在房间中使用
			if( !room )
			{
				bGameStateError = true;
			}
		}
		else if( canuseState == kItemCanUse_OutGame )
		{
			// 只能不在房间中使用
			if( pCaster->IsInSingleGame() || pCaster->GetMulitRoom() )
			{
				bGameStateError = true;
			}
		}
		else if( canuseState == kItemCanUse_InPlaying )
		{
			// 只能在游戏中使用
			if( (room && room->IsPlaying()) == false )
			{
				bGameStateError = true;
			}
		}
	}

	if( bGameStateError )
	{
		return kUseItem_CanNot_Use;
	}

	return kUseItem_Ok;
}


/** 
 * @brief 目标是否正确
 * 
 */
enUseItemResult MODI_ItemOperator::CheckItemTargetReq(MODI_ClientAvatar * pCaster, 	const MODI_GUID & target, WORD nConfigID, MODI_ClientAvatar ** pTargetClient)
{
	enItemCanUseTargetType target_type;
	if( MODI_ItemRuler::GetCanUseTarget( nConfigID ,  target_type ) == false )
	{
		return kUseItem_CanNot_Use;
	}

	*pTargetClient = 0;
	if( target == pCaster->GetGUID() )
	{
		if( target_type == kItemCanUse_TargetAll || 
			target_type == kItemCanUse_OnlySelf )
		{
			// 是否能对自己释放
			*pTargetClient = pCaster;
		}
		else 
		{
			// 目标类型错误
			return kUseItem_TargetType_Fail;
		}
	}
	else
	{
		MODI_GameChannel * pChannel = MODI_GameChannel::GetInstancePtr();
		*pTargetClient = pChannel->FindClientByID( target );
		if( !*pTargetClient )
		{
			return  kUseItem_Target_NotExist;
		}
	}
	return kUseItem_Ok;
}



/** 
 * @brief 是否能够拥有此道具
 * 
 * @param p_bag 包裹
 * @param config_id 物品配置id
 * @param item_count 物品数量
 * 
 * @return 能够拥有true
 *
 */
MODI_ItemOperator::enItemOperatorResult MODI_ItemOperator::IsCanHaveItem(MODI_Bag * p_bag, const DWORD & config_id, const WORD & item_count)
{
	if(!p_bag || config_id == INVAILD_CONFIGID)
	{
		Global::logger->error("[add_itme] invaild config <configid=%u>", config_id);
		MODI_ASSERT(0);
		return enInvalidParam;
	}
	
	MODI_ClientAvatar * p_client = p_bag->GetOwner();
	if(p_client == NULL)
	{
		Global::logger->fatal("[add_item] add item to bag but not owner");
		MODI_ASSERT(0);
		return enInvalidParam;
	}

	///  道具是否合法
	if( !MODI_BinFileMgr::GetInstancePtr()->IsValidConfigInTables(config_id, enBinFT_Avatar|enBinFT_Item ))
	{
		Global::logger->error("[add_item] add item config isvalid <name=%s,configid=%u>", p_client->GetRoleName(), config_id);
		MODI_ASSERT(0);
		return enInvalidParam;
	}

	//// 用户能否添加此道具
	if(! MODI_ItemOperator::CheckCanAddToClient(p_client, config_id, item_count))
	{
		Global::logger->error("[add_item] add item config isvalid <name=%s,configid=%u>", p_client->GetRoleName(), config_id);
		MODI_ASSERT(0);
		return enSubClassItemMax;
	}

	/// 用户包裹送是否够
	if(! MODI_ItemOperator::CheckBagSpace(p_bag, config_id, item_count))
	{
		Global::logger->error("[add_item] add item but bag not space <name=%s,configid=%u,item_count=%u>",
							  p_client->GetRoleName(), config_id, item_count);
		//MODI_ASSERT(0);
		return enBagPosNotEnough;
	}

	return enOK;
}


/** 
 * @brief 使用道具
 * 
 * @param p_src_client 源玩家
 * @param item_id 道具id
 * @param target_id 目标玩家
 * 
 * @return 使用状态
 *
 */
int MODI_ItemOperator::UseItem(MODI_ClientAvatar * p_src_client, const QWORD & item_id,  const MODI_GUID & target_id)
{
	if(!p_src_client || item_id == 0)
	{
		Global::logger->error("[assert_use_item] src client is null or item id is zero <itemid=%llu>", item_id);
		MODI_ASSERT(0);
		return MODI_ItemOperator::enInvalidParam;
	}
	
	MODI_GS2C_Notify_UseItemResult result;
	result.m_qdItemId = item_id;


	/// 获得物品
	MODI_GameItemInfo * p_item = p_src_client->GetPlayerBag().GetItemByItemId(item_id);
	if(!p_item)
	{
		Global::logger->error("[assert_use_item] not find item in bag <itemid=%llu>", item_id);
		MODI_ASSERT(0);
		result.m_result = kUseItem_CanNot_Use;
		p_src_client->SendPackage(&result , sizeof(result));
		return MODI_ItemOperator::enItemNotExist;
	}

	Global::logger->debug("[vip_bug] this configid1 <configid=%u>", p_item->GetConfigId());

	/// 获取属性列表
	const GameTable::MODI_Itemlist * pElement = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get(p_item->GetConfigId());
	if(!pElement )
	{
		Global::logger->error("[assert_use_item] not find item config in bag <itemid=%llu,configid=%u>", item_id, p_item->GetConfigId());
		MODI_ASSERT(0);
		result.m_result = kUseItem_CanNot_Use;
		p_src_client->SendPackage(&result , sizeof(result));
		return MODI_ItemOperator::enConfigError;
	}


	/// 等级是否满足
	/// 性别是否满足
	/// 游戏状态是否正确
 	result.m_result = CheckItemGameStateReq(p_src_client, p_item->GetConfigId());
 	if(result.m_result != kUseItem_Ok)
 	{
		Global::logger->error("[assert_use_item] check game state failed <itemid=%llu,configid=%u>", item_id, p_item->GetConfigId());
		MODI_ASSERT(0);
 		p_src_client->SendPackage(&result , sizeof(result));
 		return MODI_ItemOperator::enGameStateFaild;
 	}

	MODI_ClientAvatar * p_target_client = NULL;
	/// 目标类型是否正确(在房间是否能找到该人)
	result.m_result = CheckItemTargetReq(p_src_client, target_id, p_item->GetConfigId(), &p_target_client);
	if( result.m_result != kUseItem_Ok )
	{
		Global::logger->error("[assert_use_item] check game state failed <itemid=%llu,configid=%u>", item_id, p_item->GetConfigId());
		MODI_ASSERT(0);
		p_src_client->SendPackage( &result , sizeof(result) );
		return MODI_ItemOperator::enTargetFaild;
	}

	/// 物品是否有使用效果
	if(pElement->get_UseImpactID())
	{
		/// 产生效果
		if(MODI_ImpactCore::RegisterImpact(p_src_client, p_target_client, pElement->get_UseImpactID(), NULL, 0) == false)
		{
			Global::logger->debug("[use_item] client register impact failed <name=%s,itemid=%llu,configid=%u,impactid=%u>" ,
								  p_src_client->GetRoleName(), p_item->GetItemId(), p_item->GetConfigId(), 
								  pElement->get_UseImpactID() );
			result.m_result = kUseItem_CanNot_Use;
			p_src_client->SendPackage(&result , sizeof(result));
			return MODI_ItemOperator::enUnknowError;
		}
	}

	Global::logger->info("[use_item] use item <srcname=%s,targetname=%s,itemid=%llu,itemtype=%u>", 
						 p_src_client->GetRoleName(),
						 p_target_client->GetRoleName(), 
						 p_item->GetItemId(),
						 p_item->GetItemType());

	Global::logger->debug("[vip_bug] this configid2 <configid=%u>", p_item->GetConfigId());
	
	///	执行脚本 一个是实用道具，另外一个是被使用了道具
	p_src_client->SetTarget( p_target_client);

	if(p_item->GetConfigId() == 56007)
	{
		if(! MODI_RandomBoxMgr::GetInstance()->OpenYangWBox(p_src_client))
		{
			result.m_result = kUseItem_CanNot_Use;
			p_src_client->SendPackage(&result , sizeof(result));
			return MODI_ItemOperator::enUnknowError;
		}
	}
	
	Global::logger->debug("[vip_bug] this configid3 <configid=%u>", p_item->GetConfigId());
	/// 包裹减去物品
	MODI_TriggerUseItem  script(p_item->GetConfigId());
	MODI_UseItemManager::GetInstance().Execute( p_src_client,script);

	MODI_TriggerBeUsedItem  script2( p_item->GetConfigId());
	MODI_BeUsedItemManager::GetInstance().Execute(p_target_client,script2);


	Global::logger->debug("[vip_bug] this configid4 <configid=%u>", p_item->GetConfigId());
		
	p_item->DelMeFromBag(enDelReason_Used);
	/// 注意此处p_item已经被删除了
	result.m_result = kUseItem_Ok;
	p_src_client->SendPackage(&result , sizeof(result));

	p_src_client->ResetTarget();
	return MODI_ItemOperator::enOK;
}


/** 
 * @brief 拥有道具效果
 * 
 * @param p_client 拥有者
 * @param p_item 道具信息
 * 
 * @return 拥有状态
 *
 */
bool MODI_ItemOperator::HaveItemImpact(MODI_ClientAvatar * p_client, MODI_GameItemInfo * p_item)
{
	if(!p_client || !p_item)
	{
		MODI_ASSERT(0);
		return false;
	}

	const GameTable::MODI_Itemlist * pElement = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile().Get(p_item->GetConfigId());
   	if(!pElement)
	{
		/// 是avatar
		return false;
	}

	/// 物品是否有拥有效果
	if(pElement->get_HaveImpactID())
	{
		DWORD nContinueTime = 0;
		time_t nRemain = p_item->GetRemainTime();
		if( nRemain < 0 )
		{
			return false;
		}

		if(nRemain != enForever)
		{
			const GameTable::MODI_Impactlist * pImpactConfig = MODI_ImpactCore::GetConfigData( pElement->get_HaveImpactID());
			if( pImpactConfig )
			{
				DWORD nConfigTime = (DWORD)(pImpactConfig->get_ContinueTime() * 1000.0f);
				nRemain = (DWORD)(nRemain * 1000.0f);
				if( nRemain < nConfigTime )
				{
					nContinueTime  = nRemain;
				}
			}
		}
		
		/// 产生效果
		if(MODI_ImpactCore::RegisterImpact(p_client, p_client, pElement->get_HaveImpactID() , NULL,  nContinueTime) == false)
		{
			Global::logger->debug("[register_impact] register impact failed <name=%s,impact_id=%u>", p_client->GetRoleName(),
								  pElement->get_HaveImpactID());
			MODI_ASSERT(0);
			return false;
		}
	}
	return true;
}


/** 
 * @brief 获取过期的时间
 * 
 * @param pElement 商场道具
 * @param stt 类型
 * @param limit_time 时间
 * 
 */
bool MODI_ItemOperator::GetAvatarLimit(const DWORD & config_id, enShopGoodTimeType stt, DWORD & dwTime)
{
	defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();
	const GameTable::MODI_Itemlist * pItem =  itemBin.Get(config_id);
	if(pItem)
	{
		dwTime = enForever;
		if(config_id == 51001)
		{
			dwTime = 3*enOneHour;
		}
		return true;
	}
	
	if(stt == kShopGoodT_7Day)
	{
		dwTime =  enOneWeak;
		//dwTime =  5  * 60;
	}
	else if(stt == kShopGoodT_30Day)
	{
		dwTime = enOneMonth;
		//dwTime =  10 * 60;
	}
	else if(stt == kShopGoodT_Forver)
	{
		dwTime =enForever;
	}
	else 
	{
		MODI_ASSERT(0);
		return false;
	}

	return true;
}


bool	MODI_ItemOperator::ShouldAddToBag( MODI_ClientAvatar * p_client, const DWORD & config_id)
{
		
	WORD have_limit = 0;
	if(!MODI_ItemRuler::GetSubClassMaxOwn(config_id, have_limit))
	{
		Global::logger->error("[check_itme] get max_sub_own faild <configid=%u>", config_id);
		MODI_ASSERT(0);
		return false;
	}
	/// 当不是那一种独占的物品的时候，总是可是添加到包裹
	if( have_limit != 1 )
	{
		return true;
	}
	
	const MODI_ItemType item_type = ::GetItemType(config_id);
	if(item_type == enItemType_Unknow)
	{
		Global::logger->error("[check_item] check add to client failed <configid=%u>", config_id);
		MODI_ASSERT(0);
		return false;
	}
		
	WORD have_count = p_client->GetItemCountBySubClass(item_type);
	if( have_count == 0)
		return true;
	
	return false;

}
