#include <map>
#include <string>
#include "BaseOS.h"

#include "s2zs_cmd.h"

class MODI_GlobalVarMgr
{

public:
	static MODI_GlobalVarMgr * GetInstance();
//	void	UpdateVarList();

	void OnReceiveList(MODI_ZS2S_Notify_GlobalVarList * m_stCmd);
	void OnReceiveChange(MODI_ZS2S_Notify_VarChanged * m_stCmd);

	WORD	GetVar(const std::string & var_string);
	void	SetVar(const std::string & var_string ,WORD value);
private:
	MODI_GlobalVarMgr();
	~MODI_GlobalVarMgr();

	static MODI_GlobalVarMgr * m_pInstance;

	std::map<std::string,WORD >  m_mVarTable;



};
