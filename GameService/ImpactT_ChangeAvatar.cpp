#include "ImpactT_ChangeAvatar.h"
#include "gamestructdef.h"
#include "ClientAvatar.h"
#include "protocol/c2gs_rolecmd.h"
#include "GameLobby.h"
#include "NormalGameRoom.h"

MODI_ImpactT_ChangeAvatar::MODI_ImpactT_ChangeAvatar()
{

}
		
MODI_ImpactT_ChangeAvatar::~MODI_ImpactT_ChangeAvatar()
{

}

bool MODI_ImpactT_ChangeAvatar::CheckImpactData( GameTable::MODI_Impactlist & impactData )
{
	return true;
}

int MODI_ImpactT_ChangeAvatar::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	long modfiy_score = (long)impactData.get_Arg1();
	SetModfiyScore( impactObj , modfiy_score  );
	long modfiy_renqi = (long)impactData.get_Arg2();
	SetModfiyRenqi( impactObj , modfiy_renqi );
	WORD nChangeID = (WORD)impactData.get_Arg3();
	SetChangeID( impactObj , nChangeID );

	return kImpactH_Successful;
}

int MODI_ImpactT_ChangeAvatar::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( pObj )
	{
		if( pObj->IsObjStateFlag( kObjState_ChangeAvatar ) )
		{
			Global::logger->debug("[%s] client<charid=%u,name=%s> already in transfrom state, active impact faild.", 
					IMPACT_MODULE ,
					pObj->GetCharID(),
					pObj->GetRoleName() );
			return kImpactH_AntherExist;
		}

		WORD nChangeID = GetChangeID( impactObj );
		if( nChangeID != INVAILD_CONFIGID )
		{
			pObj->SetObjStateFlag( kObjState_ChangeAvatar );
			// 变形
			MODI_ClientAvatarData avatar;
			avatar.m_Avatars[enItemType_Suit - 1] = nChangeID;
			pObj->SetClientAvatarData( avatar , true );

			MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
			if( pLobby && pObj->IsInMulitRoom() )
			{
				MODI_NormalGameRoom * pRoom = (MODI_NormalGameRoom *)(pLobby->GetMultiRoom( pObj->GetRoomID() ));
				if( pRoom )
				{
					pRoom->AdujstGameResult_Score( pObj->GetGUID() , GetModfiyScore( impactObj ) );
				}
			}

//			MODI_GS2C_Notify_Transform msg;
//			msg.m_guid = pObj->GetGUID();
//			msg.m_nSuitID = nChangeID;
//			pObj->SendOrBroadcast( &msg , sizeof(msg) );

			Global::logger->debug("[%s] client<charid=%u,name=%s> active change avatar impact.", 
					IMPACT_MODULE ,
					pObj->GetCharID(),
					pObj->GetRoleName() );
			return kImpactH_Successful;
		}
		return kImpactH_ConfigDataFaild;
	}

	return kImpactH_ParamFaild;
}

int MODI_ImpactT_ChangeAvatar::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	// 变回来
	if( pObj )
	{
		pObj->ClearObjStateFlag( kObjState_ChangeAvatar );
		pObj->UpdateAvatarByAvatarBag();
	
		Global::logger->debug("[%s] client<charid=%u,name=%s> InActivate change avatar impact.", 
					IMPACT_MODULE ,
					pObj->GetCharID(),
					pObj->GetRoleName() );
	}

	return kImpactH_Successful;
}

int MODI_ImpactT_ChangeAvatar::SyncModfiyData( const MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	if( !pObj )
	{
		MODI_ASSERT(0);
		return kImpactH_ParamFaild;
	}

	// 数值效果
	MODI_GS2C_Notify_ImpactModfiyData imd;
	imd.m_Impactguid = impactObj.GetGUID();
	imd.m_ModfiyRenqi = GetModfiyRenqi( impactObj );

	
	imd.m_ModfiyScore = GetModfiyScore( impactObj );
		Global::logger->debug("[%s] client<charid=u,name=s> InActivate change avatar impact.", 
					"David----"
					 );
	pObj->SendOrBroadcast( &imd , sizeof(imd) );

	return kImpactH_Successful;
}

void 	MODI_ImpactT_ChangeAvatar::SetChangeID( MODI_ImpactObj & impactObj , WORD nChangeID )
{
	impactObj.SetArgDWORD( kArg_ChangeID , nChangeID );
}

WORD 	MODI_ImpactT_ChangeAvatar::GetChangeID( const MODI_ImpactObj & impactObj ) const
{
	DWORD nChangeID = INVAILD_CONFIGID;
	impactObj.GetArgDWORD( kArg_ChangeID , nChangeID );
	return nChangeID;
}

void 	MODI_ImpactT_ChangeAvatar::SetModfiyScore( MODI_ImpactObj & impactObj , long lValue )
{
	impactObj.SetArgLong( kArg_ModfiyScore , lValue );
}

long 	MODI_ImpactT_ChangeAvatar::GetModfiyScore( const MODI_ImpactObj & impactObj ) const
{
	long lValue = 0;
	impactObj.GetArgLong( kArg_ModfiyScore , lValue );
	return lValue;
}

void 	MODI_ImpactT_ChangeAvatar::SetModfiyRenqi( MODI_ImpactObj & impactObj , long lValue )
{
	impactObj.SetArgLong( kArg_ModfiyRenqi , lValue );

}

long 	MODI_ImpactT_ChangeAvatar::GetModfiyRenqi( const MODI_ImpactObj & impactObj ) const
{
	long lValue = 0;
	impactObj.GetArgLong( kArg_ModfiyRenqi , lValue );
	return lValue;
}

