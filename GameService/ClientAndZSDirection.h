#ifndef MODI_CLIENTANDZS_DIRECTION_H_
#define MODI_CLIENTANDZS_DIRECTION_H_

#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "ClientAvatar.h"



class 	MODI_ClientAndZSDirection
{
public:



	static bool 	ClientToZoneServer( MODI_ClientAvatar * pClient ,  
		const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size );

	static bool 	ZoneServerToClient(const MODI_SessionID & sessionID , 
			const MODI_GUID & guid , 
			const char * pData , 
			size_t nSize );

};


#endif
