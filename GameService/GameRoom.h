/**
 * @file GameRoom.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 游戏中的房间对象
 *
 */

#ifndef MODI_GAMEROOM_H_
#define MODI_GAMEROOM_H_

#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "Timer.h"
#include "MusicHeaderData.h"
#include <vector>

class MODI_Scene;

using namespace std;
/** 
 * @brief 游戏中的房间对象
 */
class MODI_GameRoom
{
public:
    explicit MODI_GameRoom( defRoomID id );
    virtual ~MODI_GameRoom();

    defRoomID GetRoomID() const
    {
        return m_info.m_roomID;
    }

public:

	virtual bool OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator ) = 0;
    virtual bool EnterRoom(MODI_ClientAvatar * pClient) = 0;
    virtual bool LeaveRoom(MODI_ClientAvatar * pClient, int iReason) = 0;
    virtual void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime) = 0;
	virtual void OnChangeMap( defMapID oldmap , defMapID newmap ) = 0;
	virtual void OnChangeMusic( defMusicID oldmusic , defMusicID newmusic , enMusicFlag flag , enMusicSType st , enMusicTime mt ) = 0;
	virtual void OnChangeGameMode( ROOM_SUB_TYPE oldMode , ROOM_SUB_TYPE newMode ) = 0;
	virtual bool InCanLeaveRoom(MODI_ClientAvatar * pClient) = 0;
	virtual bool OnDestroy() = 0;
	virtual const char * GetMasterName() const = 0;

    ///    是否普通类型房间
    bool IsNormalRoom() const
    {
        bool ret_code = false;
		if((RoomHelpFuns::GetRoomMainType(GetRoomInfo().m_roomType)) == ROOM_TYPE_NORMAL ||
		   (RoomHelpFuns::GetRoomMainType(GetRoomInfo().m_roomType)) == ROOM_TYPE_VIP)
		{
			ret_code = true;
		}
		return ret_code;
    }

	bool IsPasswordRoom();
	ROOM_TYPE 	GetRoomType() const;
	ROOM_SUB_TYPE GetRoomSubType() const;
	void SetRoomType( defRoomType type );
	void SetRoomName( const char * szName );


	///    获得房间密码
	const char * GetPassword() const
	{
		return m_strPassword.c_str();
	}
	/** 
	 * @brief 设置房间密码
	 * 
	 * @param szPwd 新的密码
	 */
	void SetPassword( const char * szPwd );


    ///    获得房间信息，只读版本
    const MODI_RoomInfo & GetRoomInfo() const
    {
        return m_info;
    }

    ///    获得普通房间的详细信息
    const MODI_NormalRoomInfo & GetNormalInfo() const
    {
        return m_info.m_detailNormalRoom;
    }


	/** 
	 * @brief 改变游戏音乐
	 * 
	 * @param id 新的音乐ID
	 */
    void ChangeMusicID(defMusicID id , enMusicFlag flag , enMusicSType st , enMusicTime mt );

	/** 
	 * @brief 获得音乐ID
	 * 
	 * @return 	返回当前的音乐ID
	 */
    defMusicID GetMusicID() const
	{
        return GetNormalInfo().m_nMusicId;
	}
	enRoomStatus GetRoomStatus() const
	{
		return m_iStatus;
	}

	void	SetRoomStatus( enRoomStatus eStatus)
	{

		m_iStatus = eStatus;
	}

	enMusicFlag GetMusicFlag() const 
	{
        return GetNormalInfo().m_flag;
	}

	enMusicSType GetSongType() const
	{
        return GetNormalInfo().m_songType;
	}
	
	enMusicTime GetMusicTimeType() const
	{
		return GetNormalInfo().m_timeType;
	}

	bool 	IsRandomMusic() const
	{
		return GetNormalInfo().m_bRandomMusic;
	}

	bool 	IsRandomMap() const
	{
		return GetNormalInfo().m_bRandomMap;
	}

	/** 
	 * @brief 改变游戏地图
	 * 
	 * @param id 新的地图ID
	 */
    void ChangeMapID(defMapID id);

	/** 
	 * @brief 获得地图ID
	 * 
	 * @return 	返回当前的地图ID
	 */
    defMapID GetMapID() const
	{
        return GetNormalInfo().m_nMapId;
	}

	void ChangeGameMode( ROOM_SUB_TYPE id );

	defMusicID 	GetRandomMusicID() const;

	enMusicFlag GetRandomMusicFlag(defMusicID id) const;

	enMusicSType GetRandomSongType(defMusicID id) const;

	defMapID 	GetRandomMapID() const;

	const MODI_GUID & GetMaster() const;

	void SetRoomScene(MODI_Scene * p_scene);
	MODI_Scene * GetRoomScene();

protected:

	void SendMdmToClient(MODI_ClientAvatar * pClient);
    ///    获得房间信息，可写版本
    MODI_RoomInfo & GetRoomInfo_Modfiy()
    {
        return m_info;
    }

    ///    获得普通房间的详细信息,可写版本
    MODI_NormalRoomInfo & GetNormalInfo_Modfiy()
    {
        return m_info.m_detailNormalRoom;
    }

	void 	SetMaster(const MODI_GUID & master );
protected:
	///房间状态
    enRoomStatus m_iStatus;

	/// 房间信息
    MODI_RoomInfo m_info; 

	///房间密码
	std::string m_strPassword;

	MODI_Scene * m_pScene;
};



#endif
