/** 
 * @file MultiGameRoom.h
 * @brief 游戏中的多人房间
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-01
 */
#ifndef MODI_MULTIGAMEROOM_H_
#define MODI_MULTIGAMEROOM_H_

#include "GameRoom.h"
#include "protocol/c2gs_roomcmd.h"
#include "protocol/c2gs_chatcmd.h"

#define		TEAM_MONEY_REFIX	2
#define		TEAM_EXP_REFIX		2

class MODI_MultiGameRoom : public MODI_GameRoom
{
	typedef MODI_GameRoom Parent;

	protected:

	/** 
	 * @brief 房间中的客户端对象
	 */
	typedef struct tagClientAvatarInRoom
	{
		MODI_ClientAvatar * m_pClient; // 客户端对象
		unsigned int m_nEnterCounter; //进入计数器
		bool m_bReadyInRoom;//是否准备
		bool m_bClosed;//位置是否关闭
		bool m_bReadyInGame;//游戏中准备完毕
		bool m_bListernOver; // 听歌结束
		enRoomStatus  m_iEnterRoomStatus; // 进入房间时的，房间状态
		BYTE m_byVoteToCount; // 投了几票
		BYTE m_byVoteCount; // 获得几票

		tagClientAvatarInRoom()
		{
			Reset();
		}

		void Reset()
		{
			m_pClient = 0;
			m_nEnterCounter = 0;
			m_bReadyInRoom = false;
			m_bClosed = false;
			m_bReadyInGame = false;
			m_bListernOver = false;
			m_iEnterRoomStatus = ROOM_STATUS_USING;
			m_byVoteToCount = 0;
			m_byVoteCount = 0;
		}

	} MODI_ClientAvatarInRoom;


public:

    explicit MODI_MultiGameRoom( defRoomID roomid );
    virtual ~MODI_MultiGameRoom();

	/** 
	 * @brief 踢掉某个玩家
	 * 
	 * @param pClient  	 发起踢人的玩家
	 * @param nTargetPos 目标玩家位置
	 * 
	 * @return 	成功返回true
	 */
	virtual bool KickSb(MODI_ClientAvatar * pClient , defRoomPosSolt nTargetPos) = 0;

	/** 
	 * @brief 打开某个位置
	 * 
	 * @param pClient 	操作发起者
	 * @param bPlayer 	是否参战位置
	 * @param nPos 		具体位置号
	 * 
	 * @return 	
	 */
    virtual bool OnPostion(MODI_ClientAvatar * pClient , bool bPlayer, defRoomPosSolt nPos) = 0;

	/** 
	 * @brief 关闭某个位置
	 * 
	 * @param pClient 	操作发起者
	 * @param bPlayer 	是否参战位置
	 * @param nPos 		具体位置号
	 * @param bKick 	如果位置上有人，是否要将其踢出房间
	 * 
	 * @return 	
	 */
    virtual bool OffPostion(MODI_ClientAvatar * pClient , bool bPlayer, defRoomPosSolt nPos, bool bKick) = 0;

	/** 
	 * @brief 交换位置
	 * 
	 * @param pClient 	操作发起者
	 * 
	 * @return 	
	 */
    virtual bool ExchangePostion(MODI_ClientAvatar * pClient ) = 0;

	/** 
	 * @brief 客户端准备就绪
	 * 
	 * @param pClient 	操作发起者
	 * 
	 * @return 	
	 */
    virtual bool ClientReadyPlay(MODI_ClientAvatar * pClient) = 0;

	/** 
	 * @brief 某个旁观玩家听歌结束
	 * 
	 * @param nGUID 	旁观玩家的GUID
	 */
	virtual void OnListernOver( MODI_GUID nGUID ) = 0;

	/** 
	 * @brief 投票给某个对象
	 * 
	 * @param pClient 	操作发起者
	 * @param nGUID 	投给哪个对象
	 */
	virtual void OnVoteTo( MODI_ClientAvatar * pClient , MODI_GUID nGUID ) = 0;

	/** 
	 * @brief 客户端准备状态改变
	 * 
	 * @param pClient 	操作发起者
	 * @param bReady 	是否准备状态
	 */
    virtual void ChangeReady(MODI_ClientAvatar * pClient, bool bReady) = 0;

	/** 
	 * @brief 客户端组队改变
	 * 
	 * @param pClient 	操作发起者
	 * @param bReady 	是否准备状态
	 */
    virtual void ChangeTeam(MODI_ClientAvatar * pClient, BYTE change_team) = 0;
	

	enRoomStatus GetRoomStatus() {return m_iStatus;};

	/** 
	 * @brief 客户端请求改变收听的歌手
	 * 
	 * @param pClient  操作发起者
	 * @param nObserverClientID 要听的歌手
	 * 
	 * @return 	
	 */
	virtual bool ChangeObSinger(MODI_ClientAvatar * pClient , MODI_GUID nObserverClientID ) = 0;

	virtual bool GameScore(MODI_ClientAvatar * pClient , WORD nNo , float fTime , 
			DWORD nScore , BYTE byType , float fExact , 
			float fRightSec , WORD nCombat ) = 0;
	
	virtual bool GameScore_Key(MODI_ClientAvatar * pClient , float fTime , 
			DWORD nScore , BYTE byType , float fExact , 
			 WORD nCombat, float fFever ) = 0;

	virtual bool IsRoomMaster(MODI_ClientAvatar * pClient ) = 0;

	virtual void OnFirstHalfOk( MODI_ClientAvatar * pClient)=0;
    virtual bool Start() = 0;
	virtual bool IsFull() = 0;

	virtual bool OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator );
    virtual bool EnterRoom(MODI_ClientAvatar * pClient);
    virtual bool LeaveRoom(MODI_ClientAvatar * pClient, int iReason);
    virtual void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);
	virtual void OnChangeMap( defMapID oldmap , defMapID newmap );
	virtual void OnChangeMusic( defMusicID oldmusic , defMusicID newmusic , enMusicFlag flag , enMusicSType st , enMusicTime mt );
	virtual void OnChangeGameMode( ROOM_SUB_TYPE oldMode , ROOM_SUB_TYPE newMode );
	virtual bool IsIn(MODI_ClientAvatar * pClient);
	virtual bool OnDestroy();
	virtual const char * GetMasterName() const; 

    virtual void Broadcast(const void * pt_null_cmd, int cmd_size);
    virtual void Broadcast_Except(const void * pt_null_cmd, int cmd_size, MODI_ClientAvatar * pExcept);
    virtual void Broadcast_Players(const void * pt_null_cmd, int cmd_size);
    virtual void Broadcast_Spectators(const void * pt_null_cmd, int cmd_size);
    virtual void Broadcast_Spectators(MODI_ClientAvatar * pClient , const void * pt_null_cmd, int cmd_size);
    virtual void Broadcast_Players_Except(const void * pt_null_cmd, int cmd_size,MODI_ClientAvatar * pExcept);
    virtual void Broadcast_Spectators_Except(const void * pt_null_cmd, int cmd_size,MODI_ClientAvatar * pExcept);

	virtual void SetAllowSpectator( bool bSet );
    bool IsAllowSpectator() const
    {
        return m_bOffSpectator ? false : true;
    }

	void SyncKeyModeParam(MODI_ClientAvatar * pClient);
	bool IsPlayerClient(MODI_ClientAvatar * pClient);

	/// 最大参战人数
	unsigned char GetMaxPlayers() const;

	/// 当前参战玩家个数
	unsigned char GetCurPlayersNum() const;

	/// 当前观战玩家个数
	unsigned char GetCurSpectatorsNum() const;

	/// 当前房间中的总人数
	unsigned char GetCurClientsNum() const;

	//    获得一个空的位置
	virtual bool 	GetFreePosSolt( bool & bIsPlayer , defRoomPosSolt & nPos );

	//    获得一个空的参战位置
	defRoomPosSolt GetFreePlayersPosSolt();

	// 获得一个空的观战位置
	defRoomPosSolt GetFreeSpectatorPosSolt();

	MODI_ClientAvatar * GetClientAvatarByPos(bool bPlayer, defRoomPosSolt nPos);

	//    获得房间中性别符合的客户端个数
	int  GetClientsNumBySex( int iSex ,bool bIncludeSpector = false ) const;

	// 当前男孩个数
	unsigned char GetCurBoysNum( bool bIncludeSpector = false ) const;

	// 当前女孩个数
	unsigned char GetCurGirlNum( bool bIncludeSpector = false ) const;

	/** 
	 * @brief 使用GUID在房间中查找某一客户端
	 * 
	 * @param nGUID 需要查找的客户端
	 * 
	 * @return 	
	 */
	MODI_ClientAvatar * FindClientByGUID( MODI_GUID nGUID ) const;

	/** 
	 * @brief 使用
	 * 
	 * @param nGUID
	 * 
	 * @return 	
	 */
	MODI_ClientAvatar * FindPlayerByGUID( MODI_GUID nGUID ) const;

	/** 
	 * @brief 使用GUID在观战玩家中查找某一客户端
	 * 
	 * @param nGUID 需要查找的客户端
	 * 
	 * @return 	
	 */
	MODI_ClientAvatar * FindSpectorByGUID( MODI_GUID nGUID ) const;

	void SendMusicHeader( MODI_ClientAvatar  * pClient , int i );

	/** 
	 * @brief 获得创建房间时初始设置的最大玩家个数
	 * 
	 * @return 	
	 */
	DWORD 	GetMaxPlayerInitialSetting() { return m_nPlayerMaxSize; }

	/** 
	 * @brief 获得创建房间时初始设置的最大参战玩家个数
	 * 
	 * @return 	
	 */
	DWORD 	GetMaxSpectorInitalSetting() { return m_nSpectatorMaxSize; }

	/** 
	 * @brief 房间是否开始游戏
	 * 
	 * @note  房主点击开始按钮之后到游戏结束这段时间都被视为游戏开始状态
	 * @return 如果房间已经开始游戏，则返回 TRUE
	 */
    bool IsStart() const;

	/** 
	 * @brief 房间是否正在游戏中
	 *
	 * @note  从开始看到唱歌的场景到游戏结束这段时间都被视为游戏中状态
	 * @return 如果房间正在游戏中，则返回TRUE	
	 */
    bool IsPlaying() const;

	/** 
	 * @brief 房间是否正在开始中
	 *
	 * @note  房主点击开始游戏到资源准备就绪完毕的这段时间,视为游戏正在开始中状态
	 * @return 如果房间正在开始中，则返回TRUE	
	 * 
	 */ 
	bool IsStarting() const; 

	/** 
	 * @brief 房间是否正在投票中
	 * 
	 * @return 	
	 */
	bool IsVoting() const;

	unsigned short GetSentencesCount() const;

	//   获得当前歌曲的NOTE总长度
	float 	GetTotalNodeLength() const;

	void  TransferMaster( const MODI_GUID & new_master );

	void  TransferMaster( MODI_ClientAvatar * pNewMaster  );

	void OnMasterChanged( MODI_ClientAvatar * pNewMaster );

	void SetMainType(ROOM_TYPE room_main_type);

	void SetSubType(ROOM_SUB_TYPE room_sub_type);

	const ROOM_SUB_TYPE & GetSubType();
	
	const ROOM_TYPE GetMainType();

	void  SetAllTeam();

	/// 增加一个ispeakid
	bool AddISpeakId(const MODI_ISpeak_Id & id);
	/// 减少一个ispeakid
	bool DelISpeakId(const MODI_GUID & guid);

	bool DelYYChatState(const MODI_GUID & guid);

	/// 改变yy聊天状态
	bool ChangeYYChatState(const MODI_YYChatState & state);
	
	/// 广播
	void BroadcastYYChatState();

protected:

	virtual void SendRoleInfoToClients(MODI_ClientAvatar * pClient);

	virtual void SendRolesInfoToClient(MODI_ClientAvatar * pClient);

	/** 
	 * @brief 为观战者分配一个歌手
	 * 
	 * @return 	返回歌手的GUID
	 */
	virtual MODI_GUID GetSingerForSpector();

	/** 
	 * @brief 获得房间中的玩家结构
	 * 
	 * @param bPlayer 	是否参战玩家
	 * @param nPos 		位置
	 * 
	 * @return 	
	 */
	MODI_ClientAvatarInRoom * GetClientInRoomByPos(bool bPlayer, defRoomPosSolt nPos);
	
	///    获得最先进入的参战玩家
	MODI_ClientAvatarInRoom * GetEarliestEnterInPlayers();

	///    获得最先进入的观战玩家
	MODI_ClientAvatarInRoom * GetEarliestEnterInSpectators();

	/** 
	 * @brief 移出一个客户端
	 * 
	 * @param pRemove 	需要被移出的客户端
	 * 
	 * @return 	成功返回TRUE
	 */
	bool RemoveClient(MODI_ClientAvatar * pRemove);

	/** 
	 * @brief 通知网关传输映射图
	 */
	void NotifyTransMissionMapToGW();

	/** 
	 * @brief 通知网关添加条传输映射
	 * 
	 * @param nAdd 	需要被添加的源
	 * @param pDest 添加目标
	 * @param nCount 添加目标个数
	 */
	void NotifyAddTransMission( const MODI_SessionID & nAdd , const MODI_SessionID * pDest , unsigned int nCount );

	/** 
	 * @brief 通知网关删除条传输映射
	 * 
	 * @param nDel 	需要被删除的源
	 * @param pDest 删除目标
	 * @param nCount 删除目标个数
	 */
	void NotifyDelTransMission( const MODI_SessionID & nDel , const MODI_SessionID * pDest , unsigned int nCount );

	void SyncReadyState();


	void SyncPosionState();

	void SendVote( const MODI_GUID & src , const MODI_GUID & dest );

	void SendVoteResult( const MODI_GUID & dest , BYTE byCount );

	void SyncVoteState( MODI_ClientAvatar * pClient );

	void InitPostion( MODI_ClientAvatar * pClient );
	
	void ClearAllTeam();
	
	/// 广播id
	void BroadcastISpeakId();
	
protected:

	/// 计数器，用来决定进入房间的先后次序
    unsigned int m_nEnterCounter; 

	///  是否允许观战
    bool 	m_bOffSpectator; 

	DWORD 	m_nPlayerMaxSize;

	///  参战位置插槽
    MODI_ClientAvatarInRoom m_Players[MAX_PLAYSLOT_INROOM]; 

	DWORD 	m_nSpectatorMaxSize;

	///  观战位置插槽
    MODI_ClientAvatarInRoom m_Spectators[MAX_SPECTATORSLOT_INROOM];

	/// 当前歌曲NOTE的总长度
	float 							m_fTotalNodeLength;

	///  参战位置的组队情况
    BYTE m_byTeamState[MAX_PLAYSLOT_INROOM];


	/// 房间的主类型
	ROOM_TYPE m_enRoomMainType;

	/// 房间的子类型
	ROOM_SUB_TYPE m_enRoomSubType;

	/// ispeak id
	std::map<MODI_GUID, int > m_stISpeakIdMap;

	/// YY chat state
	std::map<MODI_GUID, BYTE > m_stYYChatState;
};



#endif
