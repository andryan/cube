/**
 * @file   AvatarHandler.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 27 11:14:19 2011
 * 
 * @brief  Avatar相关的操作
 * 
 */

#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "AssertEx.h"
#include "Bag.h"
#include "ItemRuler.h"
#include "ItemOperator.h"
#include "ZoneClient.h"

/** 
 * @brief 换衣服
 * 
 */
int MODI_PackageHandler_c2gs::ChangeAvatar(void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	MODI_ClientAvatar * p_client = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_SyncAvatar , cmd_size , 0 , 0 , p_client);
	if(p_client->IsInMulitRoom())
	{
		return enPHandler_Kick;
	}

	const MODI_C2GS_Request_SyncAvatar * p_recv_cmd = (const MODI_C2GS_Request_SyncAvatar *)(pt_null_cmd);

	MODI_Bag * p_player_bag = &(p_client->GetPlayerBag());
	MODI_Bag * p_avatar_bag = &(p_client->GetAvatarBag());

	/// 检查新换的物品，是否都合法
	bool ret_code = true;
	for(int i=0; i < MAX_BAG_AVATAR_COUNT; i++)
	{
		QWORD item_id = p_recv_cmd->m_NewAvatar[i];
		if(item_id == 0)
		{
			continue ;
		}
		if(! (p_avatar_bag->IsHaveItem(item_id) || p_player_bag->IsHaveItem(item_id)))
		{
			Global::logger->error("[change_avatar] client request change avatar not find item in bag <name=%s,itemid=%llu>" ,
								 p_client->GetRoleName(), item_id); 
			ret_code = false;
		}
	}

	if(!ret_code)
	{
		MODI_ASSERT(0);
		return enPHandler_Kick;
	}

	bool change_successful = true;
	p_avatar_bag->Backup();
	p_player_bag->Backup();
	
	std::vector<MODI_GameItemInfo * > change_avatar_vec;
	MODI_BagPosInfo * p_pos_info = NULL;
	MODI_GameItemInfo * p_old_item = NULL;
	MODI_GameItemInfo * p_new_item = NULL;
	for(int i=0; i < MAX_BAG_AVATAR_COUNT; i++)
	{
		/// 获取之前的衣服
		p_pos_info = p_avatar_bag->GetPosInfo(i+1);
		if(!p_pos_info)
		{
			MODI_ASSERT(0);
			change_successful = false;
			break;
		}
		if(p_pos_info->GetCount() > 1)
		{
			Global::logger->error("[change_avatar] change avatar failed <name=%s,count=%u,pos=%u>",p_client->GetRoleName(),
								  p_pos_info->GetCount(), p_pos_info->GetBagPos());
			MODI_ASSERT(0);
			change_successful = false;
			break;
		}

		if(p_pos_info->GetCount() == 1)
		{
			/// 换一下
			p_old_item = p_pos_info->GetItemInfo();
			if(! p_old_item)
			{
				MODI_ASSERT(0);
				change_successful = false;
				break;
			}
			if(p_old_item->GetItemId() != p_recv_cmd->m_NewAvatar[i])
			{
				if((p_avatar_bag->DelItemNoUpdate(p_old_item)) && (p_player_bag->AddItemUpdate(p_old_item)))
				{
					change_avatar_vec.push_back(p_old_item);
				}
				else
				{
					MODI_ASSERT(0);
					change_successful = false;
					break;
				}
			}
			else
			{
				/// 这个位置没有换，还是穿原来的,直接换下一个占位
				continue;
			}
			
		}
		
		if(p_recv_cmd->m_NewAvatar[i] == 0)
		{
			/// 这个占位没有穿任何衣服
			continue;
		}
		
		p_new_item = p_player_bag->GetItemByItemId(p_recv_cmd->m_NewAvatar[i]);
		if(p_new_item)
		{
			if(p_player_bag->DelItemNoUpdate(p_new_item))
			{
				p_new_item->SetBagType(enBagType_Avatar);
				p_new_item->SetBagPos(i+1);
				if(p_avatar_bag->AddItemNoUpdate(p_new_item))
				{
					change_avatar_vec.push_back(p_new_item);
				}
				else
				{
					MODI_ASSERT(0);
					change_successful = false;
					break;
				}
			}
		}
		else
		{
			Global::logger->error("[change_avatar] not find a new avatar <name=%s,pos=%u, itemid=%llu>", p_client->GetRoleName(),i+1,
								  p_recv_cmd->m_NewAvatar[i]);
			MODI_ASSERT(0);
			change_successful = false;
			break;
		}
	}

	/// 没有任何更新
	if(change_avatar_vec.size() == 0)
	{
		Global::logger->debug("[change_avatar] not any change <name=%s>", p_client->GetRoleName());
		return enPHandler_Kick;
	}

	if(change_successful == false)
	{
		Global::logger->fatal("[change_avatar] change failed <name=%s>", p_client->GetRoleName());
		p_avatar_bag->ReBackup();
		p_player_bag->ReBackup();
		return enPHandler_Kick;
	}
	else
	{
		std::vector<MODI_GameItemInfo * >::iterator iter = change_avatar_vec.begin();
		MODI_ByteBuffer result(1024);
		MODI_GameItemInfo * p_update_item = NULL;

		/// 构造发送更新数据语句
		char send_sql_buf[Skt::MAX_USERDATASIZE];
		memset(send_sql_buf, 0, sizeof(send_sql_buf));
		MODI_S2RDB_Request_ExecSql_Transaction_Cmd * send_sql_cmd = (MODI_S2RDB_Request_ExecSql_Transaction_Cmd *)send_sql_buf;
		AutoConstruct(send_sql_cmd);
		char * cmd_content = send_sql_cmd->m_Data;
		WORD send_sql_size = 0;

		char send_client_buf[Skt::MAX_USERDATASIZE];
		memset(send_client_buf, 0, sizeof(send_client_buf));
		MODI_GS2C_UpdateItem_Cmd * send_client_cmd = (MODI_GS2C_UpdateItem_Cmd *)send_client_buf;
		AutoConstruct(send_client_cmd);
		MODI_ItemInfo * p_send_client_info = send_client_cmd->m_stItemInfo;
		
		for(; iter != change_avatar_vec.end(); iter++)
		{
			p_update_item = *iter;
			if(!p_update_item)
			{
				MODI_ASSERT(0);
				return false;
			}
			
			MODI_Record record_update;
			std::ostringstream where;
			where << "itemid=" << p_update_item->GetItemId();
			record_update.Put("bag_type", p_update_item->GetBagType());
			record_update.Put("bag_pos", p_update_item->GetBagPos());
			record_update.Put("where", where.str());
			result.Reset();
			if(! MODI_DBClient::MakeUpdateSql(result, GlobalDB::m_pItemInfoTbl, &record_update))
			{
				Global::logger->fatal("[update_item] update item pos failed <itemid=%llu>", p_update_item->GetItemId());
				MODI_ASSERT(0);
				return false;
			}
			MODI_Transaction_Sql * update_sql = (MODI_Transaction_Sql *)cmd_content;
			update_sql->m_wdSize = result.ReadSize();
			memcpy(update_sql->m_Sql, result.ReadBuf(), result.ReadSize());
			cmd_content += sizeof(MODI_Transaction_Sql) + result.ReadSize();
			send_sql_size += sizeof(MODI_Transaction_Sql) + result.ReadSize();
			send_sql_cmd->m_byCount++;
			
			
			/// 更新到客户端
			p_send_client_info->m_qdItemId = p_update_item->GetItemId();
			p_send_client_info->m_dwConfigId = p_update_item->GetConfigId();
			p_send_client_info->m_byBagType = p_update_item->GetBagType();
			p_send_client_info->m_wdBagPos = p_update_item->GetBagPos();
			p_send_client_info->m_dwRemainTime = p_update_item->GetRemainTime();
			send_client_cmd->m_wdSize++;
			p_send_client_info++;
#ifdef _ITEM_DEBUG			
			Global::logger->debug("[change_avatar] change avatar <itemid=%llu,newbag=%d,newpos=%u>", p_update_item->GetItemId(),
								  p_update_item->GetBagType(), p_update_item->GetBagPos());
#endif			
		}

		MODI_ZoneServerClient * pZone = MODI_ZoneServerClient::GetInstancePtr();
		if(! pZone )
		{
			Global::logger->info("[request_exec_sql] can't get zone service client <name=%s>", p_client->GetRoleName());
			p_avatar_bag->ReBackup();
			p_player_bag->ReBackup();
			return enPHandler_Kick;
		}
		
		pZone->SendCmd(send_sql_cmd, sizeof(MODI_S2RDB_Request_ExecSql_Transaction_Cmd) + send_sql_size);
		p_client->SendPackage(send_client_cmd, sizeof(MODI_GS2C_UpdateItem_Cmd) + sizeof(MODI_ItemInfo) * send_client_cmd->m_wdSize);

		/// 更新任务信息
		p_client->UpdateAvatarByAvatarBag();
		return enPHandler_OK;
	}
	
}
