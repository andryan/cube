/** 
 * @file SingleGameRoom.h
 * @brief 单人房间
 * @author Tang Teng
 * @version v0.1
 * @date 2010-06-30
 */

#ifndef MODI_SINGLEGAMEROOM_H_
#define MODI_SINGLEGAMEROOM_H_

#include "GameRoom.h"

/** 
 * @brief 游戏中的房间对象
 */
class MODI_SingleGameRoom : public MODI_GameRoom
{
	typedef MODI_GameRoom Parent;

public:

    MODI_SingleGameRoom( defRoomID id );
    virtual ~MODI_SingleGameRoom();

public:

	virtual bool OnCreate( MODI_ClientAvatar * pClient , defMusicID musicid , 
			defMapID mapid , DWORD nPlayMax , DWORD nSpectorMax , bool bAllowSpectator );
    virtual bool EnterRoom(MODI_ClientAvatar * pClient);
    virtual bool LeaveRoom(MODI_ClientAvatar * pClient, int iReason);
    virtual void Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);
	virtual bool OnDestroy();
	virtual void OnChangeMap( defMapID oldmap , defMapID newmap ) {}
	virtual void OnChangeMusic( defMusicID oldmusic , defMusicID newmusic , enMusicFlag flag , enMusicSType st  , enMusicTime mt ) {}
	virtual void OnChangeGameMode( ROOM_SUB_TYPE oldMode , ROOM_SUB_TYPE newMode ) {}
    virtual bool IsFull() { return true; }
	virtual const char * GetMasterName() const; 

	virtual bool InCanLeaveRoom(MODI_ClientAvatar * pClient) { return true; }
};



#endif
