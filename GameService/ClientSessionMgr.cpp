#include "ClientSessionMgr.h"

MODI_ClientSessionMgr::MODI_ClientSessionMgr()
{

}

MODI_ClientSessionMgr::~MODI_ClientSessionMgr()
{
	MAP_SESSION_ITER itor = m_mapSessions.begin();
    if (itor != m_mapSessions.end())
    {
		MODI_ClientSession * pTemp = itor->second;
		delete pTemp;
		itor++;
    }
    m_mapSessions.clear();
}

bool MODI_ClientSessionMgr::Add(MODI_ClientSession * p)
{
    MAP_SESSION_INSERT_RESULT result = m_mapSessions.insert(std::make_pair(p->GetSessionID(), p));
    if (result.second == false)
    {

    }
    return result.second;
}

bool MODI_ClientSessionMgr::Del(const MODI_SessionID & id)
{
    MAP_SESSION_ITER itor = m_mapSessions.find(id);
    if (itor != m_mapSessions.end())
    {
        m_mapSessions.erase(itor);
        return true;
    }
    return false;
}

bool MODI_ClientSessionMgr::Del(MODI_ClientSession * p)
{
    if (!p->IsVaildSessionID())
    {
        return false;
    }
    return this->Del(p->GetSessionID());
}

bool MODI_ClientSessionMgr::IsIn(const MODI_SessionID & id)
{
    MAP_SESSION_ITER itor = m_mapSessions.find(id);
    if (itor != m_mapSessions.end())
    {
        return true;
    }
    return false;
}

MODI_ClientSession * MODI_ClientSessionMgr::Find(const MODI_SessionID & id)
{
    MODI_ClientSession * pRet = 0;
    MAP_SESSION_ITER itor = m_mapSessions.find(id);
    if (itor != m_mapSessions.end())
    {
        pRet = itor->second;
    }
    return pRet;
}

size_t MODI_ClientSessionMgr::Size() const
{
    return m_mapSessions.size();
}

void MODI_ClientSessionMgr::DisconnectionRubbishClients( unsigned long nTimeOut )
{
	if( !MODI_GameTask::ms_pInstancePtr )
		return ;

	MAP_SESSION_ITER itor = m_mapSessions.begin();
    while (itor != m_mapSessions.end())
    {
		MAP_SESSION_ITER inext = itor;
		inext++;

		MODI_ClientSession * pSession = itor->second;
		if( pSession )
		{
			if( pSession->IsInAntherLoginStatus() && pSession->IsWaitCloseTimeout() )
			{
				// 另外一个人登入后，如果客户端没有主动断开，服务器需要主动踢
				MODI_GW2GS_DelSession 	delSession;
				delSession.m_sessionID = pSession->GetSessionID();
				MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 

				Global::logger->info("[%s] disconnect rubbish session<%s> : anther client login , this client not disconnect .", 
						GAME_SESSION ,
						pSession->GetSessionID().ToString() );

				MODI_GameTask::ms_pInstancePtr->OnDelSession( &delSession , sizeof(delSession) );
			}
			else if( pSession->IsInWaitAuthStatus() && pSession->IsAuthTimeOut() )
			{
				
				MODI_GW2GS_DelSession 	delSession;
				delSession.m_sessionID = pSession->GetSessionID();
				MODI_GameTask::ms_pInstancePtr->SendCmd( &delSession , sizeof(delSession) ); 

				Global::logger->info("[%s] disconnect rubbish session<%s> : wait auth time out .", 
						GAME_SESSION ,
						pSession->GetSessionID().ToString() );

				MODI_GameTask::ms_pInstancePtr->OnDelSession( &delSession , sizeof(delSession) );
			}
		}

		itor = inext;
    }
}
