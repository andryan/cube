/**
 * @file   MakeManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Mar 23 11:53:46 2011
 * 
 * @brief  制造管理
 * 
 * 
 */

#include "MakeManager.h"
#include "ActiveCondition.h"
#include "Active.h"

/** 
 * @brief 条件注册
 * 
 */
template<>
void MODI_MakeConditionManager::RegCondition()
{
	RegMake("var", new MODI_ConditionMake<MODI_VarCondition>);
	RegMake("gvar", new MODI_ConditionMake<MODI_GVarCondition>);
	RegMake("begintime", new MODI_ConditionMake<MODI_BeginTime_C>);
	RegMake("endtime", new MODI_ConditionMake<MODI_EndTime_C>);
	RegMake("checkitemnum", new MODI_ConditionMake<MODI_CheckItemNum_C>);
	RegMake("sex",new MODI_ConditionMake<MODI_CheckSexType_C>);
	RegMake("onlinetime",new MODI_ConditionMake<MODI_OnlineCondition_C>);
	RegMake("daytimebegin", new MODI_ConditionMake<MODI_DayTimeBegin>);
	RegMake("daytimeend", new MODI_ConditionMake<MODI_DayTimeEnd>);
	RegMake("week", new MODI_ConditionMake<MODI_WeekOp>);
	RegMake("signupdays",new MODI_ConditionMake<MODI_ActiveSignupdays>);
	RegMake("rand", new MODI_ConditionMake<MODI_Rands>);
	RegMake("checkregistertime", new MODI_ConditionMake<MODI_CheckRegisterTime>);
	RegMake("chenghaock" , new MODI_ConditionMake<MODI_ChenghaoCondition>);
	RegMake("isroommaster" , new MODI_ConditionMake<MODI_IsRoomOwner>);
	RegMake("isexpirevip" , new MODI_ConditionMake<MODI_IsExpireVip>);
	RegMake("isvip" , new MODI_ConditionMake<MODI_IsVip>);
	RegMake("timetodel", new MODI_ConditionMake<MODI_IsTimeToDeleteFriend>);
	RegMake("checklastlogintime", new MODI_ConditionMake<MODI_CheckLastLoginTime>);
	RegMake("level", new MODI_ConditionMake<MODI_CheckLevel_C>);
	RegMake("songcount", new MODI_ConditionMake<MODI_SongCountCondition>);
	RegMake("checkpaymoney", new MODI_ConditionMake<MODI_CheckPayMoney>);
}


/** 
 * @brief 动作注册
 * 
 */
template<>
void MODI_MakeActionManager::RegAction()
{
	RegMake("package",new MODI_ActionMake<MODI_ActivePackage>);
	
	RegMake("var", new MODI_ActionMake<MODI_VarAction>);
	RegMake("gvar", new MODI_ActionMake<MODI_GVarAction>);
	RegMake("senditemmail", new MODI_ActionMake<MODI_SendItemMail_A>);
	RegMake("system_notify", new MODI_ActionMake<MODI_System_Notify_A>);
	RegMake("additem_direct", new MODI_ActionMake<MODI_AddItemDirect_A>);
	RegMake("video_pass", new MODI_ActionMake<MODI_VideoPass_A>);
	RegMake("setcv", new MODI_ActionMake<MODI_SetCV_A>);
	RegMake("log", new MODI_ActionMake<MODI_TestLogger>);
	RegMake("signup",new MODI_ActionMake<MODI_ActiveSignup>);
	//RegMake("signdone",new MODI_ActionMake<MODI_ActiveSignDone>);
	RegMake("signupdesc",new MODI_ActionMake<MODI_ActiveSigupDesc>);
	RegMake("sysinfo" , new MODI_ActionMake<MODI_SysInfo_A>);
	RegMake("dispre" , new MODI_ActionMake<MODI_DisablePre>);
	RegMake("grapre" , new MODI_ActionMake<MODI_GrantPre>);
	RegMake("exppen" , new MODI_ActionMake<MODI_ExpPenalty>);
	RegMake("moneypen" , new MODI_ActionMake<MODI_MoneyPenalty>);
	RegMake("sysmail" , new MODI_ActionMake<MODI_SendSysMail>);
	RegMake("changemaster" , new MODI_ActionMake<MODI_ChangeMaster>);
	RegMake("additemtime" , new MODI_ActionMake<MODI_AddItemTime>);
	RegMake("setitemtime" , new MODI_ActionMake<MODI_SetTimeLimit>);
	RegMake("delfriend" , new MODI_ActionMake<MODI_DelFriendandIdol>);
	RegMake("expirevip" , new MODI_ActionMake<MODI_ExpireVip>);
	RegMake("activevip" , new MODI_ActionMake<MODI_ActiveVip>);
	RegMake("notifyviptime", new MODI_ActionMake<MODI_NotifyVipTime>);
	RegMake("notifyaddviptime" , new MODI_ActionMake<MODI_NotifyVipAddTime>);
	RegMake("systembroad", new MODI_ActionMake<MODI_NotifySystemBroad>);
	RegMake("modifyparam" , new MODI_ActionMake<MODI_ModifyTargetParm>);
	RegMake("setpaymoney" , new MODI_ActionMake<MODI_SetPayMoney>);
}

