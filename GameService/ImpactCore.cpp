#include "ImpactCore.h"
#include "ImpactTemplate.h"
#include "ImpactObj.h"
#include "ImpactTemplateMgr.h"
#include "ClientAvatar.h"
#include "gamestructdef.h"
#include "GUIDCreator.h"

MODI_ImpactCore::MODI_ImpactCore()
{

}

MODI_ImpactCore::~MODI_ImpactCore()
{

}

bool MODI_ImpactCore::CheckConfigFile()
{
	defImpactlistBinFile & impactBin = MODI_BinFileMgr::GetInstancePtr()->GetImpactBinFile();
	defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();

	Global::logger->info("[%s] begin check about ImpactCore *.bin files ..." , SYS_INIT );

	Global::logger->info("[%s] checking Impactlist.bin file ... " , SYS_INIT );
	for( size_t n = 0; n < impactBin.GetSize() ; n++ )
	{
		const GameTable::MODI_Impactlist * pItem = impactBin.GetByIndex( n );
		if( pItem->get_ConfigID() == INVAILD_CONFIGID )
		{
			Global::logger->info("[%s] a invalid item in Impactlist.bin . reason : invaild configid<%u>" , 
					SYS_INIT ,
					pItem->get_ConfigID() );
			return false;
		}

		MODI_ImpactTemplateMgr * pMgr = MODI_ImpactTemplateMgr::GetInstancePtr();
		if( !pMgr->GetImpactHandle( pItem->get_TemplateID() ) )
		{
			Global::logger->info("[%s] a invalid item in Impactlist.bin . reason : invaild template<%u>" , 
					SYS_INIT ,
					pItem->get_ConfigID() );
			return false;
		}
	}
	Global::logger->info("[%s] checking Impactlist.bin file successful. " , SYS_INIT );

	Global::logger->info("[%s] checking Itemlist.bin file ... " , SYS_INIT );
	for( size_t n = 0; n < itemBin.GetSize() ; n++ )
	{
		const GameTable::MODI_Itemlist * pItem = itemBin.GetByIndex( n );
		if( pItem->get_ConfigID() == INVAILD_CONFIGID )
		{
			Global::logger->info("[%s] a invalid item in Itemlist.bin . reason : invaild configid<%u>" , 
					SYS_INIT ,
					pItem->get_ConfigID() );
			return false;
		}

		if( pItem->get_UseImpactID() != INVAILD_CONFIGID )
		{
			if( !impactBin.Get( pItem->get_UseImpactID() ) )
			{
				Global::logger->info("[%s] a invalid item in Impactlist.bin . reason : invaild UseImpactID<%u>" , 
						SYS_INIT ,
						pItem->get_UseImpactID() );
				return false;
			}
		}
		
		if( pItem->get_HaveImpactID() != INVAILD_CONFIGID )
		{
			if( !impactBin.Get( pItem->get_HaveImpactID() ) )
			{
				Global::logger->info("[%s] a invalid item in Impactlist.bin . reason : invaild HaveImpactID<%u>" , 
						SYS_INIT ,
						pItem->get_HaveImpactID() );
				return false;
			}
		}
	}
	Global::logger->info("[%s] checking Itemlist.bin file successful. " , SYS_INIT );


	Global::logger->info("[%s] end check about ImpactCore *.bin files ." , SYS_INIT );
	return true;
}

void MODI_ImpactCore::Log( MODI_ClientAvatar * pCaster , MODI_ClientAvatar * pTarget , WORD nConfigID , int iRes , const char * szActionName )
{
	MODI_ImpactTemplateMgr * pMgr = MODI_ImpactTemplateMgr::GetInstancePtr();
	const char * szImpactName = pMgr->GetImpactName( nConfigID );
	if( iRes == kImpactH_Successful )
	{
		Global::logger->info("[%s] ImpactCore do action : %s.\
				caster<charid=%u,name=%s> , target<charid=%u,name=%s> , impact <configid=%u,name=%s> , \
				successful. " , 
				IMPACT_MODULE ,
				szActionName,
				pCaster->GetCharID(),
				pCaster->GetRoleName(),
				pTarget->GetCharID(),
				pTarget->GetRoleName(),
				nConfigID ,
				szImpactName );
	}
	else if( iRes == kImpactH_CannotFindConfig )
	{
		Global::logger->info("[%s] ImpactCore do action : %s.\
				caster<charid=%u,name=%s> , target<charid=%u,name=%s> , impact <configid=%u,name=%s> , \
				faild<can't find configdata>. " , 
				IMPACT_MODULE ,
				szActionName,
				pCaster->GetCharID(),
				pCaster->GetRoleName(),
				pTarget->GetCharID(),
				pTarget->GetRoleName(),
				nConfigID ,
				szImpactName );
	}
	else if( iRes == kImpactH_CannotFindT )
	{
		Global::logger->info("[%s] ImpactCore do action : %s.\
				caster<charid=%u,name=%s> , target<charid=%u,name=%s> , impact <configid=%u,name=%s> , \
				faild<can't find template>. " , 
				IMPACT_MODULE ,
				szActionName,
				pCaster->GetCharID(),
				pCaster->GetRoleName(),
				pTarget->GetCharID(),
				pTarget->GetRoleName(),
				nConfigID ,
				szImpactName );
	}
}

const GameTable::MODI_Impactlist * MODI_ImpactCore::GetConfigData( WORD nConfigID )
{
	defImpactlistBinFile & binFile = MODI_BinFileMgr::GetInstancePtr()->GetImpactBinFile();
	const GameTable::MODI_Impactlist * pElement = binFile.Get( nConfigID );
	return pElement;
}

bool 	MODI_ImpactCore::RegisterImpact( MODI_ClientAvatar * pCaster , MODI_ClientAvatar * pTarget , WORD nConfigID , void * pUserdata , DWORD nContinuance )
{
	if( !pCaster || !pTarget )
	{
		MODI_ASSERT(0);
		return false;
	}

	static const char * s_ActionName = "RegisterImpact";

	if( nConfigID == INVAILD_CONFIGID )
	{
		Log( pCaster , pTarget , nConfigID , kImpactH_CannotFindConfig , s_ActionName );
		return false;
	}

	const GameTable::MODI_Impactlist * pConfigData = GetConfigData( nConfigID );
	if( !pConfigData )
	{
		Log( pCaster , pTarget ,nConfigID , kImpactH_CannotFindConfig , s_ActionName );
		return false;
	}
	WORD	tmp = pConfigData->get_TemplateID();		

	if(tmp== kIT_AddItem)
	{
		MODI_ClientAvatar * p_client = pCaster;
		MODI_Bag * p_bag = &(p_client->GetPlayerBag());
		if(! p_bag)
		{
			MODI_ASSERT(0);
			return false;
		}

		float goodid =pConfigData->get_Arg1();
	
		MODI_GoodsPackageArray gpa;
		if( !MODI_BinFileMgr::GetInstancePtr()->GetGoodsPackage((WORD)goodid, &gpa ) )
		{
			MODI_ASSERT(0);
			return false;
		}
	
		/// 背包是否足够
		if(p_bag->GetEmptyPosCount() < gpa.m_count)
		{
			/// 空间不足
			Global::logger->info("[good_package] client not enough add item <name=%s>", p_client->GetRoleName());
			return false;
		}
	}

	MODI_GUIDCreator * pGUIDCreator = MODI_GUIDCreator::GetInstancePtr();
	MODI_GUID guid = pGUIDCreator->CreateImpactGUID();
	MODI_ImpactObj * impactObj = MODI_ImpactObjPool::New();
		//new MODI_ImpactObj(guid);
	impactObj->SetGUID( guid );
	impactObj->SetContinuanceElapsed(0);
	impactObj->SetIntervalElapsed(0);
	impactObj->SetConfigID( nConfigID );
	impactObj->SetClientID( pConfigData->get_ClientID() );

	if( nContinuance  )
	{
		impactObj->SetContinuance( nContinuance );
	}
	else 
	{
		impactObj->SetContinuance( (DWORD)(pConfigData->get_ContinueTime() * 1000.0f) );
	}

	impactObj->SetUserdata( pUserdata );
	impactObj->SetCasterGUID( pCaster->GetGUID() );
	impactObj->SetOutGameFadeOut( pConfigData->get_OutGameFadeOut() > 0 ? true : false );

	int iRes = kImpactH_Successful;
	iRes = Initial( *impactObj ,  *pConfigData ); 
	if( iRes != kImpactH_Successful )
	{
		Log( pCaster , pTarget , nConfigID , iRes , s_ActionName );
		return false;
	}

	if( !pTarget->GetImpactlist().Register( *impactObj ) )
	{
		return false;
	}

	int iDumpRes = 0;
	MODI_ImpactTemplate * pTemplate = MODI_ImpactCore::GetImpactTempalte(  *impactObj , &iDumpRes );
	if( pTemplate )
	{
		pTemplate->SyncModfiyData( *impactObj , pTarget );
	}

	Log( pCaster , pTarget , nConfigID , iRes , s_ActionName );

	return true;
}

MODI_ImpactTemplate * MODI_ImpactCore::GetImpactTempalte( WORD nConfigID , int * pResult )
{
	int iResult = kImpactH_Successful;
	if( nConfigID == INVAILD_CONFIGID )
	{
		iResult = kImpactH_CannotFindConfig;
	}

	WORD nTemplateID = INVAILD_CONFIGID;
	if( iResult == kImpactH_Successful )
	{
		const GameTable::MODI_Impactlist * pElement = GetConfigData( nConfigID );
		if( !pElement )
		{
			iResult = kImpactH_CannotFindConfig;
		}
		nTemplateID = pElement->get_TemplateID();
	}		

	MODI_ImpactTemplate * pRet = 0;
	if( iResult == kImpactH_Successful )
	{
		MODI_ImpactTemplateMgr * pMgr = MODI_ImpactTemplateMgr::GetInstancePtr();
		pRet = pMgr->GetImpactHandle( nTemplateID );
		if( !pRet  )
		{
			iResult = kImpactH_CannotFindT;
		}
	}

	if( pResult )
	{
		*pResult = iResult;
	}

	return pRet;

}

MODI_ImpactTemplate * MODI_ImpactCore::GetImpactTempalte( const MODI_ImpactObj & impactObj , int * pResult )
{
	return	GetImpactTempalte( impactObj.GetConfigID() , pResult );
}

int MODI_ImpactCore::Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData )
{
	int iRes = kImpactH_CannotFindConfig;
	MODI_ImpactTemplate * pTemplate = GetImpactTempalte( impactObj , &iRes );
	if( pTemplate )
	{
		iRes = pTemplate->Initial( impactObj , impactData );
	}

	return iRes;
}

int MODI_ImpactCore::Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	static const char * s_ActionName = "Active";

	int iRes = kImpactH_Successful;
	MODI_ImpactTemplate * pTemplate = GetImpactTempalte( impactObj , &iRes );
	if( pTemplate )
	{
		MODI_ASSERT( iRes == kImpactH_Successful );
		iRes = pTemplate->Active( impactObj , pObj );
	}

	Log( pObj , pObj , impactObj.GetConfigID() , iRes ,  s_ActionName );

	return iRes;
}

int MODI_ImpactCore::Update( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj , unsigned long nTime )
{
	int iRes = kImpactH_Successful;
	MODI_ImpactTemplate * pTemplate = GetImpactTempalte( impactObj , &iRes );
	if( pTemplate )
	{
		MODI_ASSERT( iRes == kImpactH_Successful );
		iRes = pTemplate->Update( impactObj , pObj , nTime );
	}
	return iRes;
}

int MODI_ImpactCore::InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	static const char * s_ActionName = "InActivate";
	DISABLE_UNUSED_WARNING( s_ActionName );

	int iRes = kImpactH_Successful;
	MODI_ImpactTemplate * pTemplate = GetImpactTempalte( impactObj , &iRes );
	if( pTemplate )
	{
		MODI_ASSERT( iRes == kImpactH_Successful );
		iRes = pTemplate->InActivate( impactObj , pObj );
	}


	return iRes;
}

int MODI_ImpactCore::CleanUp( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj )
{
	static const char * s_ActionName = "CleanUp";
	DISABLE_UNUSED_WARNING( s_ActionName );

	int iRes = kImpactH_Successful;
	MODI_ImpactTemplate * pTemplate = GetImpactTempalte( impactObj , &iRes );
	if( pTemplate )
	{
		MODI_ASSERT( iRes == kImpactH_Successful );
		iRes = pTemplate->CleanUp( impactObj , pObj );
	}


	return iRes;
}

