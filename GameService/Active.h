#ifndef _MODI_ACTIVE_H
#define _MODI_ACTIVE_H

#include "Action.h"
#include "GameUserVar.h"

// 任务分为3个阶段
// 1：接收
// 2：提交
// 3：完成


/*
 *@Brief:签到活动
 */
class MODI_ActiveSignup : public MODI_Action
{
public:	
	bool Init(const char * action_line);
	bool do_it( MODI_ClientAvatar * p_client);

private:
	unsigned int 	m_ntimes;	
};

class MODI_ActiveSignDone : public MODI_Action
{

public:	
	bool Init(const char * action_line);
	bool do_it( MODI_ClientAvatar * p_client);
private:

};


class MODI_ActiveSigupDesc : public MODI_Action
{
public:
	bool Init(const char * action_line);
	bool do_it( MODI_ClientAvatar * p_client);
private:
	unsigned int 	m_ntimes;
	BYTE		m_nSec1;
	BYTE		m_nSec2;
	BYTE		m_nSec3;
	BYTE		m_nSec4;

	WORD		m_nPack1;
	WORD		m_nPack2;
	WORD		m_nPack3;
	WORD		m_nPack4;
	WORD		m_nPack5;

};

class MODI_ActivePackage : public MODI_Action
{
public:
	bool	Init( const char * con_line);
	bool	do_it(MODI_ClientAvatar * p_client);

private:


};
#endif



