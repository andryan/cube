#include "PackageHandler_c2gs.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "ClientAvatar.h"
#include "ClientSession.h"
#include "ClientSessionMgr.h"
#include "GameChannel.h"
#include "GameLobby.h"
#include "BinFileMgr.h"
#include "AssertEx.h"
#include "MdmMgr.h"
#include "Base/HelpFuns.h"

///    创建房间的处理
int MODI_PackageHandler_c2gs::CreateRoom_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);

	CHECK_PACKAGE( MODI_C2GS_Request_CreateRoom , cmd_size , 0 , 0 , pClient );


    if (pClient->IsInMulitRoom())
    {
        Global::logger->debug("[%s] player<%s> in room but sent create room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName());
        return enPHandler_Kick;
    }

	if(pClient->IsInSingleGame())
	{
		Global::logger->debug("[%s] player<%s> in singlegame but sent create room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName());
        return enPHandler_Kick;
	}

    MODI_C2GS_Request_CreateRoom & req = *((MODI_C2GS_Request_CreateRoom *)(pt_null_cmd));
    MODI_GS2C_Respone_OptResult result;

    //    检查房间模式是否有效
    int iRoomMainType = RoomHelpFuns::GetRoomMainType(req.m_roomType);
    if ((iRoomMainType > ROOM_TYPE_BEGIN && iRoomMainType < ROOM_TYPE_END) == false)
    {
        Global::logger->debug("[%s] player<%s> fail to create room: invalid room main type.", 
				SVR_TEST , 
				pClient->GetRoleName() );
        result.m_nResult = SvrResult_Room_CInvaildPara;
        pClient->SendPackage(&result,sizeof(result));
        return enPHandler_Kick;
    }

	int iRoomSubType = RoomHelpFuns::GetRoomSubType(req.m_roomType);
	if( (iRoomSubType > ROOM_STYPE_BEGIN && iRoomSubType < ROOM_STYPE_END) == false )
	{
        Global::logger->debug("[%s] player<%s> fail to create room: invalid room sub type.", 
				SVR_TEST , 
				pClient->GetRoleName() );
        result.m_nResult = SvrResult_Room_CInvaildPara;
        pClient->SendPackage(&result,sizeof(result));
        return enPHandler_Kick;
	}

	if( iRoomMainType != ROOM_TYPE_SINGLE )
	{
		if(! AVATAR_CAN( pClient,PREVILEGE_CREATEROOM))
		{
			Global::logger->debug("[req_previllege] client %s request to create room without previllege",pClient->GetRoleName());
			return enPHandler_OK;
		}
		size_t nRoomNameLen = strlen( req.m_cstrRoomName );
		if ( !nRoomNameLen )
		{
			Global::logger->debug("[%s] player<%s> fail to create room: no room name.", 
					SVR_TEST , 
					pClient->GetRoleName() );
			result.m_nResult = SvrResult_Room_CInvaildPara;
			pClient->SendPackage(&result,sizeof(result));
			return enPHandler_Kick;
		}

		if (nRoomNameLen > ROOM_NAME_MAX_LEN)
		{
			Global::logger->debug("[%s] player<%s> fail to create room: room name exceed limit.", 
					SVR_TEST , 
					pClient->GetRoleName() );
			result.m_nResult = SvrResult_Room_CInvaildPara;
			pClient->SendPackage(&result,sizeof(result));
			return enPHandler_Kick;
		}

	   // 是否有密码
		bool bIsPwd = RoomHelpFuns::IsHasPassword(req.m_roomType);
		if (bIsPwd)
		{
			size_t nPwdLen = strlen( req.m_cstrPassword );
			//    密码不能为空
			if ( !nPwdLen )
			{
				Global::logger->debug("[%s] player<%s> fail to create room: empty password created.", 
						SVR_TEST , 
						pClient->GetRoleName() );
				result.m_nResult = SvrResult_Room_CInvaildPara;
				pClient->SendPackage(&result,sizeof(result));
				return enPHandler_Kick;
			}
			//    密码长度判断
			if (nPwdLen > ROOM_PWD_MAX_LEN)
			{
				Global::logger->debug("[%s] player<%s> fail to create room: password exceed limit.", 
						SVR_TEST , 
						pClient->GetRoleName() );
				result.m_nResult = SvrResult_Room_CInvaildPara;
				pClient->SendPackage(&result,sizeof(result));
				return enPHandler_Kick;
			}
		}


		if( req.m_MusicID != RANDOM_MUSIC_ID && 
				MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get( req.m_MusicID ) == 0 )
		{
			Global::logger->debug("[%s] player<%s> fail to create room: invalid music ID <%u>." , 
					SVR_TEST , 
					pClient->GetRoleName() , 
					req.m_MusicID );
			return enPHandler_Kick;
		}

		if( req.m_MapID != RANDOM_MAP_ID )
		{
			const GameTable::MODI_Scenelist * pScene = MODI_BinFileMgr::GetInstancePtr()->GetSceneBinFile().Get( req.m_MapID );
			if( !pScene )
			{
				return enPHandler_Kick;
			}
		}
	}
	else 
	{
		const char * szSingleRoomName = "SingleRoom";
		safe_strncpy(req.m_cstrRoomName , 
				szSingleRoomName , 
				sizeof(req.m_cstrRoomName) );
		req.m_bIsOffSpectator = true;
	}
 
    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();

	result.m_nResult = pLobby->CreateRoom( pClient , 
										   req.m_roomType , 
										   req.m_MusicID ,
										   req.m_MapID ,
										   req.m_wdSceneID,
										   req.m_cstrRoomName , 
										   req.m_cstrPassword ,
										   !req.m_bIsOffSpectator );

	if( result.m_nResult != SvrResult_Ok )
	{
        pClient->SendPackage(&result,sizeof(result));
    }

    return enPHandler_OK;
}

///    请求进入房间的处理
int MODI_PackageHandler_c2gs::JoinRoom_Handler(void * pObj,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_JoinRoom , cmd_size , 0 , 0 , pClient );

    if (pClient->IsInMulitRoom())
    {
        Global::logger->debug("[%s] player<%s> in room, but sent enter room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName() );
        return enPHandler_Kick;
    }

	if(pClient->IsInSingleGame())
	{
		Global::logger->debug("[%s] player<%s> in singlegame, but sent enter room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName() );
        return enPHandler_Kick;
	}


    //    要进入的房间
    MODI_MultiGameRoom * room = 0;
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_GS2C_Respone_OptResult result;

    MODI_C2GS_Request_JoinRoom & req = *((MODI_C2GS_Request_JoinRoom *)(pt_null_cmd));
    if (req.m_roomID == INVAILD_ROOM_ID)
    {
		return enPHandler_Kick;
    }
	else 
	{
		room = pLobby->GetMultiRoom( req.m_roomID );
		WORD nRet = pLobby->IsCanEnterRoom( room );
		// 房间是否存在
		if (nRet == SvrResult_Room_NotExistJ)
		{
			Global::logger->debug("[%s] player<%s> fail to join room: room <%u> does not exist." , 
					GS_ROOMOPT , 
					pClient->GetRoleName() , 
					req.m_roomID );
			//   房间不存在
			result.m_nResult = SvrResult_Room_NotExistJ;
			pClient->SendPackage(&result,sizeof(result));
			return enPHandler_OK;
		}

		// 房间是否正在开始游戏中（只能开始完之后才能进入)
		if (nRet == SvrResult_Room_StartedJ)
		{
			Global::logger->debug("[%s] player<%s> fail to join room: room <%u> is playing." , 
					GS_ROOMOPT , 
					pClient->GetRoleName() , 
					req.m_roomID );

			result.m_nResult = SvrResult_Room_StartedJ;
			pClient->SendPackage(&result,sizeof(result));
			return enPHandler_OK;
		}

		// 房间人数是否已满
		if (nRet == SvrResult_Room_PlayerFullJ)
		{
			Global::logger->debug("[%s] player<%s> fail to join room: room <%u> is full." , 
					GS_ROOMOPT , 
					pClient->GetRoleName() , 
					req.m_roomID );
			result.m_nResult = SvrResult_Room_PlayerFullJ;
			pClient->SendPackage(&result,sizeof(result));
			return enPHandler_OK;
		}

		//    房间是否有密码
		if (room->IsPasswordRoom() && pClient->GetFullData().m_roleExtraData.m_iGMLevel != 3)
		{
			bool bVaild = strncmp( req.m_cstrPassword , room->GetPassword() , sizeof(req.m_cstrPassword) );
			if ( bVaild )
			{
				Global::logger->debug("[%s] player<%s> fail to join room: room <%u> wrong password." , 
						GS_ROOMOPT , 
						pClient->GetRoleName() , 
						req.m_roomID );
				//  密码不正确
				result.m_nResult = SvrResult_Room_InvaildPwdJ;
				pClient->SendPackage(&result,sizeof(result));
				return enPHandler_OK;
			}
		}
	}

    // 进入房间
    if (room->EnterRoom(pClient))
    {
		Global::logger->debug("[%s] player<%s> enter room <%u> successful." , 
				GS_ROOMOPT , 
				pClient->GetRoleName() , 
				req.m_roomID );

    }

    return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::RandomJoinRoom_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_RandomJoinRoom , cmd_size , 0 , 0, pClient );

    if (pClient->IsInMulitRoom())
    {
        Global::logger->debug("[%s] player<%s> in room, \
				but sent random enter room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName() );
        return enPHandler_Kick;
    }

	if(pClient->IsInSingleGame())
	{
		Global::logger->debug("[%s] player<%s> in singlegame, \
				but sent random enter room packet(bad status).",
			   	SVR_TEST , 
				pClient->GetRoleName() );
        return enPHandler_Kick;
	}

    MODI_C2GS_Request_RandomJoinRoom & req = *((MODI_C2GS_Request_RandomJoinRoom *)(pt_null_cmd));
    //    要进入的房间
    MODI_MultiGameRoom * room = 0;
	MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	room = pLobby->GetWaittingRoom( req.m_type, req.m_subtype , 
			req.m_musicid , req.m_mapid , false );
	if( !room )
	{
		MODI_GS2C_Respone_OptResult result;
		result.m_nResult = SvrResult_Room_CannotRandomJ;
		pClient->SendPackage(&result,sizeof(result));
		return enPHandler_OK;
	}

    // 进入房间
    if (room->EnterRoom(pClient))
    {
		Global::logger->debug("[%s] player<%s> random enter room <%u> successful." , 
				GS_ROOMOPT , 
				pClient->GetRoleName() , 
				room->GetRoomID() );
    }

    return enPHandler_OK;
}

int MODI_PackageHandler_c2gs::LeaveRoom_Handler(void * pObj ,
        const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_LeaveRoom  , cmd_size , 0 , 0 , pClient );
    if (!pClient->IsInMulitRoom() && !pClient->IsInSingleGame() )
    {
        Global::logger->debug("[%s] player<%s> not in room, but sent leave room packet(bad status).", GS_ROOMOPT , pClient->GetRoleName() );
        return enPHandler_Kick;
    }
	
    // 离开房间
    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
    MODI_GameRoom  * room = 0;
	if( pClient->IsInMulitRoom() )
	{
		room = pLobby->GetMultiRoom( pClient->GetRoomID() );
	}
	else 
	{
		room = pLobby->GetSingleRoom( pClient );
	}

	if( !room->InCanLeaveRoom( pClient ) )
	{
		return enPHandler_Kick;
	}

    if (room->LeaveRoom(pClient, enLeaveReason_Initiative))
    {
#ifdef _ROOM_INFO
	static unsigned int leave_success = 0;
	leave_success++;
	Global::logger->debug("[leave_room] leave room success %u ", leave_success);
#endif
	}
	
	
    return enPHandler_OK;
}


int MODI_PackageHandler_c2gs::InvitePlay_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_InvitePlay , cmd_size , 0, 0, pClient );
	const MODI_C2GS_Request_InvitePlay * pReq = (const MODI_C2GS_Request_InvitePlay *)(pt_null_cmd);
	if( pReq->m_nTarget.IsInvalid() )
		return enPHandler_Kick;

	if( !pClient->IsInMulitRoom() )
		return enPHandler_Warning;

	MODI_GameChannel * pGameChannel = MODI_GameChannel::GetInstancePtr();
	MODI_ClientAvatar * pTarget = pGameChannel->FindClientByID( pReq->m_nTarget );
	if( !pTarget )
	{
		// 目标玩家不再线
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_InvitePlay_NotExist;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	if( !pTarget->GetSession()->IsInLoginedStatus() )
	{
		// 目标玩家不处于正确的状态
		return enPHandler_OK;
	}

	// 目标玩家相对于邀请发起者是否拒绝邀请
	if( pTarget->IsRejectInvitePlay( pClient ) )
	{
		return enPHandler_OK;
	}

	if( pTarget->IsInMulitRoom() )
	{
		// 目标玩家已经在房间中了
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_InvitePlay_TargetIsInRoom;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	if( pTarget->IsInShop() )
	{
		// 目标玩家已经在房间中了
		MODI_GS2C_Respone_OptResult res;
		res.m_nResult = SvrResult_InvitePlay_TargetIsInRoom;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

    MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
	MODI_MultiGameRoom * pRoom = pLobby->GetMultiRoom( pClient->GetRoomID());

	// 目标玩家可以接受邀请

	MODI_GS2C_Notify_SbInviteYou 	msg;
	msg.m_nRoomID = pClient->GetRoomID();
	msg.m_roomState = pRoom->GetNormalInfo().m_nStateMask;
	size_t nRoleNameLen = strlen( pClient->GetRoleName() );
	size_t nRoomNameLen = strlen( pRoom->GetRoomInfo().m_cstrRoomName );
	safe_strncpy( msg.m_szHostName  ,pClient->GetRoleName() ,  sizeof(msg.m_szHostName) );
	msg.m_szHostName[nRoleNameLen] = '\0';
	safe_strncpy( msg.m_szRoomName , pRoom->GetRoomInfo().m_cstrRoomName , sizeof(msg.m_szRoomName) );
	msg.m_szRoomName[nRoomNameLen] = '\0';
	pTarget->SendPackage( &msg , sizeof(msg) );

	pTarget->SetInviteRoomID( pClient->GetRoomID() );
	
	return enPHandler_OK;
}

///  	对邀请请求的回复
int MODI_PackageHandler_c2gs::InvitePlayRes_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd,
            const unsigned int cmd_size)
{
	MODI_ClientAvatar * pClient = static_cast<MODI_ClientAvatar *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_InvitePlayRes  , cmd_size , 0,0,pClient );

	if( pClient->IsInMulitRoom() )
		return enPHandler_OK;

	const MODI_C2GS_Request_InvitePlayRes * pReq = (const MODI_C2GS_Request_InvitePlayRes *)(pt_null_cmd);
	if( pReq->m_byResult == MODI_C2GS_Request_InvitePlayRes::enNo )
	{
		// 
		pClient->SetInviteRoomID( INVAILD_ROOM_ID );

	}
	else if( pReq->m_byResult == MODI_C2GS_Request_InvitePlayRes::enNoAndForver )
	{
		pClient->SetInviteHandlerMode( enInviteH_RejectAll );
		pClient->SetInviteRoomID( INVAILD_ROOM_ID );
	}
	else if( pReq->m_byResult == MODI_C2GS_Request_InvitePlayRes::enSure )
	{
		if( pClient->GetInviteRoomID() != INVAILD_ROOM_ID  )
		{
			// 房间是否还存在
			MODI_GameLobby * pLobby = MODI_GameLobby::GetInstancePtr();
			MODI_GS2C_Respone_OptResult res;
			MODI_MultiGameRoom * pRoom = pLobby->GetMultiRoom( pClient->GetInviteRoomID() );
			if( !pRoom )
			{
				// 房间不存在了
				res.m_nResult = SvrResult_InvitePlayRes_RoomNotExist;
				pClient->SendPackage( &res , sizeof(res) );
				return enPHandler_OK;
			}

			if( pRoom->IsStarting() )
			{
				// 房间已经开始游戏了
				res.m_nResult = SvrResult_InVitePlayRes_RoomIsPlaying;
				pClient->SendPackage( &res , sizeof(res) );
				return enPHandler_OK;
			}

			if( pRoom->IsFull() )
			{
				// 房间已经满了
				res.m_nResult = SvrResult_InVitePlayRes_RoomIsFull;
				pClient->SendPackage( &res , sizeof(res) );
				return enPHandler_OK;
			}

			// 是否有密码

			// 进入房间
			pRoom->EnterRoom( pClient );
		}

		pClient->SetInviteRoomID( INVAILD_ROOM_ID );
	}
	else 
	{
		Global::logger->error("[%s] client<%s> send invaild invite play respone<%u> ." , GS_ROOMOPT ,
				pClient->GetRoleName(),
				pReq->m_byResult );
		return enPHandler_Kick;
	}

	return enPHandler_OK;
}




