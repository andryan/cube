/**
 * @file   Action.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:04:28 2011
 * 
 * @brief  脚本动作
 * 
 * 
 */

#ifndef _MD_ACTION_H
#define _MD_ACTION_H


#include "Global.h"
#include "ClientAvatar.h"


/**
 * @brief 脚本动作
 * 
 */
class MODI_Action
{
 public:
	virtual ~MODI_Action(){}
	virtual bool Init(const char * action_line) = 0;
	virtual bool do_it(MODI_ClientAvatar * p_client) = 0;
};


class MODI_VarAction: public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	std::string m_strName;
	std::string m_strAttribute;
	WORD m_wdValue;
	std::string m_strOp;
	std::string m_strVarToOp;
};


/**
 * @brief 发送送东西mail动作
 * 
 */
class MODI_SendItemMail_A: public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	/// 邮件标题
	std::string m_strCaption;

	/// 邮件内容
	std::string m_strContent;

	///附件
	std::string m_strAttach;
};


/** 
 * @brief 系统公告接口
 * 
 * 
 */
class MODI_System_Notify_A: public MODI_Action
{
 public:
	MODI_System_Notify_A()
	{
		m_byType = 0;
	}
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	BYTE m_byType;
	std::string m_strContent;
};


/**
 * @brief 增加物品
 * 
 */
class MODI_AddItemDirect_A: public MODI_Action
{
 public:
	MODI_AddItemDirect_A()
	{
		m_wdItemID = 0;
		m_wdNum = 0;
		m_wdLimit = 0;
	}
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	WORD m_wdItemID;
	WORD m_wdNum;
	WORD m_wdLimit;
};


/**
 * @brief 通过视频验证
 * 
 */
class MODI_VideoPass_A: public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
};



/**
 * @brief 修改环境变量
 * 
 */
class MODI_SetCV_A: public MODI_Action
{
 public:
	MODI_SetCV_A()
	{
	}
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
	
 private:
	std::string name;
	std::string value;
};

/**
 * @brief 系统信息
 * 
 */

class 	MODI_SysInfo_A : public  MODI_Action
{

public:
	MODI_SysInfo_A()
	{
	}

	bool  Init(const char * action_line);
	bool  do_it(MODI_ClientAvatar * p_client);

private:
	std::string	info;	
	std::string	title;
	BYTE m_byType;

};


/**
 * @brief 测试日志 
 * 
 */

class MODI_TestLogger : public  MODI_Action 
{
public:
	MODI_TestLogger()
	{
	}
	bool	Init(const char * action_line);
	bool	do_it(MODI_ClientAvatar * p_client);

private:
	std::string	m_sAction;
	std::string	m_sInfo;

};

/**
 * @brief   全局变量
 * 
 */

class MODI_GVarAction : public MODI_Action
{

 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	std::string m_strName;
	WORD m_wdValue;
	std::string m_strOp;


};

/**
 * @brief  取消权限
 *
 */
class	MODI_DisablePre : public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	DWORD	m_dwPre;

};

/**
 *@brief 授予权限
 *
 */

class	MODI_GrantPre : public MODI_Action
{

 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	DWORD	m_dwPre;

};


/**
 * @brief 经验处罚
 *
 */

class	MODI_ExpPenalty : public MODI_Action
{

 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	WORD	m_wExpPerc;

};

/**
 * @brief 金币处罚
 *
 */

class	MODI_MoneyPenalty : public MODI_Action
{

 public:
	bool Init(const char * action_line);
	bool do_it(MODI_ClientAvatar * p_client);
 private:
	WORD	m_wMonPerc;

};

/**
 * @brief 发送系统通知邮件 
 *
 */
class	MODI_SendSysMail : public MODI_Action
{
public:	
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);

private:
	std::string	m_stSenderName;
	std::string	m_stReceverName;
	std::string	m_stContent;
	std::string	m_stCaption;
};

/**
 * @brief 更换合适的房主
 *
 */
class	MODI_ChangeMaster : public MODI_Action
{
public:
	bool	Init( const char * action_line)
	{
		return true;
	}
	bool	do_it( MODI_ClientAvatar *p_client);
private:

};
/**
 * @brief 增加某一个道具的时间
 *
 * @parm	m_wConfigid  物品的configid
 * @parm	m_wTime		时间单位是分钟 
 */
class	MODI_AddItemTime : public MODI_Action
{
public:
	bool	Init(const char *action_line);
	bool	do_it(MODI_ClientAvatar * p_client);
private:
	WORD	m_wConfigid;
	DWORD	m_wTime;
};

/**
 *@brief  成为会员
 */
class	MODI_ActiveVip : public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);
private:
	DWORD	m_dwTime;

};



/**
 *@brief  会员过期
 */

class	MODI_ExpireVip :public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);
private:

};

/**
 * @brief 删除好友和偶像
 *
 */
class	MODI_DelFriendandIdol : public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);

private:
	WORD	m_nFriend;
	WORD	m_nIdole;
	WORD	m_nBlack;

};

/**
 * @brief 设置道具的时间
 *
 */
class	MODI_SetTimeLimit : public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar  * p_client);


private:

	WORD	m_wConfigid;
	DWORD	m_wTime;
};

/**
 * @brief 通知VIP时间
 *
 */

class	MODI_NotifyVipTime : public MODI_Action
{

public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);
private:

	WORD	m_wConfigid;
};


/**
 * @brief 发送系统通知邮件 
 *
 */

/**
 * @brief 通知添加时间
 *
 */
class MODI_NotifyVipAddTime : public MODI_Action
{

public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar * p_client);
private:

	BYTE	m_bTime;
};


/**
 * @brief 系统公告
 *
 */

class	MODI_NotifySystemBroad : public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar  * p_client);


private:
	std::string	m_stContent;

};


/**
 * @brief  修改目标的数据
 *
 */
class	MODI_ModifyTargetParm : public MODI_Action
{
public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar  * p_client);

private:
	DWORD	m_nConfigid;
	WORD	m_nContinue;
	WORD	m_nElapsed;

	DWORD	m_nScore;
	DWORD	m_nRenqi;
	
};


class MODI_SetPayMoney: public MODI_Action
{
 public:
	bool	Init( const char * action_line);
	bool	do_it( MODI_ClientAvatar  * p_client);
	
 private:
	DWORD m_dwValue;
};

#endif





