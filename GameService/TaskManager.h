/**
 * @file   TaskManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 09:56:32 2011
 * 
 * @brief  任务管理
 * 
 */

#ifndef _MD_TASKMANAGER_H
#define _MD_TASKMANAGER_H

#include "Global.h"

class MODI_Task;

/**
 * @brief 任务管理
 * 
 */
class MODI_TaskManager
{
 public:
	typedef std::map<WORD, MODI_Task * >  defTaskMap;
	typedef std::map<WORD, MODI_Task * >::iterator  defTaskMapIter;
	typedef std::map<WORD, MODI_Task * >::value_type defTaskMapValue;
	
	static MODI_TaskManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_TaskManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	/** 
	 * @brief 任务加载
	 * 
	 */
	bool Init();

	/** 
	 * @brief 增加一个任务
	 * 
	 * @param p_task 任务
	 *
	 */
	void AddTask(MODI_Task * p_task);


	/** 
	 * @brief 查找一个任务
	 * 
	 * @return 失败返回NULL
	 *
	 */
	MODI_Task * GetTask(const WORD & task_id);



	/** 
	 * @brief 重新加载脚本
	 * 
	 */
	bool ReloadScript();

	

 private:
	static MODI_TaskManager * m_pInstance;

	/// 任务容器
	defTaskMap m_stTaskMap;

	/// 锁
	MODI_RWLock m_stRWLock;
};

#endif
