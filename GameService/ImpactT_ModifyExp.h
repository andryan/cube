/** 
 * @file ImpactT_ModifyExp.h
 * @brief 修改经验的效果模板
 * @author Tang Teng
 * @version v0.1
 * @date 2010-06-29
 */
#ifndef MODI_IMPACT_MODIFY_EXP_H_
#define MODI_IMPACT_MODIFY_EXP_H_


#include "ImpactTemplate.h"

/** 
 * @brief 按系数修改经验
 */
class 	MODI_ImpactT_ModifyExpByFactor : public MODI_ImpactTemplate
{
		enum
		{
			kArg_Factoy = 0,
		};

	public:

		MODI_ImpactT_ModifyExpByFactor();
		virtual ~MODI_ImpactT_ModifyExpByFactor();

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData );

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetFactor( MODI_ImpactObj & impactObj , float fFactor );
		float 	GetFactor( const MODI_ImpactObj & impactObj ) const;
};

/** 
 * @brief 按常量修改经验
 */
class 	MODI_ImpactT_ModifyExpByConst : public MODI_ImpactTemplate
{
	enum
	{
		kArg_ModifyConst = 0,
	};

	public:

		MODI_ImpactT_ModifyExpByConst();
		virtual ~MODI_ImpactT_ModifyExpByConst();

	public:

		virtual bool CheckImpactData( GameTable::MODI_Impactlist & impactData );

		virtual int Initial( MODI_ImpactObj & impactObj , const GameTable::MODI_Impactlist & impactData );

		virtual int Active( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		virtual int InActivate( MODI_ImpactObj & impactObj , MODI_ClientAvatar * pObj );

		void 	SetModifyConst( MODI_ImpactObj & impactObj , long fModify );
		long 	GetModifyConst( MODI_ImpactObj & impactObj ) const;
};

#endif
