/**
 * @file   TaskManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 10:12:59 2011
 * 
 * @brief  任务系统管理
 * 
 * 
 */

#include "Global.h"
#include "XMLParser.h"
#include "TaskManager.h"
#include "Condition.h"
#include "Action.h"
#include "ScriptManager.h"
#include "MakeManager.h"

MODI_TaskManager * MODI_TaskManager::m_pInstance = NULL;

/** 
 * @brief 加载脚本
 * 
 */
bool MODI_TaskManager::Init()
{
	/// 注册条件和动作
	MODI_MakeConditionManager::GetInstance().RegCondition();
	MODI_MakeActionManager::GetInstance().RegAction();
	
	/// 加载任务
	
	/// 加载脚本
	MODI_XMLParser xml;
	if(! xml.InitFile("./Script/scripts.xml"))
	{
		Global::logger->fatal("[load_script] not find file Script/scripts.xml");
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("scripts");
	if(!p_root)
	{
		Global::logger->fatal("[load_script] not find a root node scripts");
		return false;
	}

	xmlNodePtr p_script = xml.GetChildNode(p_root, "script");
	while(p_script)
	{
		std::string script_type;
		std::string script_source;
		
		if(!xml.GetNodeStr(p_script, "type", script_type))
		{
			Global::logger->debug("[load_script] load script failed type not get");
			MODI_ASSERT(0);
			return false;
		}

		if(!xml.GetNodeStr(p_script, "source", script_source))
		{
			Global::logger->debug("[load_script] load script failed source not get");
			MODI_ASSERT(0);
			return false;
		}

		if(script_type == "enter")
		{
			if(!MODI_EnterScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if(script_type == "task")
		{
			if(!MODI_TaskScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if(script_type == "time")
		{
			if(!MODI_TimeScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "active")
		{
			if( !MODI_ActivityManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if ( script_type == "chenghao" )
		{
			if( !MODI_ChenghaoScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "buyitem")
		{
			if( !MODI_BuyItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "useitem")
		{
			if( !MODI_UseItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "haveitem" )
		{
			if( !MODI_ItemImpactManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}	
		}
		else if( script_type == "expire" )
		{
			if( !MODI_ItemExpireManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}	
		}
		else if( script_type == "music" )
		{
			if( !MODI_SingMusicManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "beuseditem")
		{
			if( !MODI_BeUsedItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else
		{
			Global::logger->debug("[load_script] load script failed type not find");
			MODI_ASSERT(0);
			return false;
		}
		
		/// 下一个节点
		p_script = xml.GetNextNode(p_script, "script");
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[load_script] load script successful");
#endif
	
	return true;
}


/** 
 * @brief  增加一个任务
 * 
 * @param p_task 要增加的任务
 */
void MODI_TaskManager::AddTask(MODI_Task * p_task)
{
// 	m_stRWLock.wrlock();
// 	m_stTaskMap.insert(defTaskMapValue(p_task->GetTaskID(), p_task));
// 	m_stRWLock.unlock();
}


/** 
 * @brief 查找一个任务
 * 
 * @return 失败返回NULL
 */
MODI_Task * MODI_TaskManager::GetTask(const WORD & task_id)
{
	MODI_Task * p_task = NULL;
	m_stRWLock.rdlock();
	defTaskMapIter iter = m_stTaskMap.find(task_id);
	if(iter != m_stTaskMap.end())
	{
		p_task = iter->second;
	}
	m_stRWLock.unlock();
	return p_task;
}


/** 
 * @brief 重新加载脚本
 * 
 */
bool MODI_TaskManager::ReloadScript()
{
	MODI_EnterScriptManager::GetInstance().Unload();
	MODI_TaskScriptManager::GetInstance().Unload();
	MODI_TimeScriptManager::GetInstance().Unload();
	MODI_ActivityManager::GetInstance().Unload();
	MODI_ChenghaoScriptManager::GetInstance().Unload();
	MODI_UseItemManager::GetInstance().Unload();
	MODI_BuyItemManager::GetInstance().Unload();
	MODI_ItemImpactManager::GetInstance().Unload();
	MODI_ItemExpireManager::GetInstance().Unload();
	MODI_SingMusicManager::GetInstance().Unload();
	MODI_BeUsedItemManager::GetInstance().Unload();

	/// 加载脚本
	MODI_XMLParser xml;
	if(! xml.InitFile("./Script/scripts.xml"))
	{
		Global::logger->fatal("[load_script] not find file Script/scripts.xml");
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("scripts");
	if(!p_root)
	{
		Global::logger->fatal("[load_script] not find a root node scripts");
		return false;
	}

	xmlNodePtr p_script = xml.GetChildNode(p_root, "script");
	while(p_script)
	{
		std::string script_type;
		std::string script_source;
		
		if(!xml.GetNodeStr(p_script, "type", script_type))
		{
			Global::logger->debug("[load_script] load script failed type not get");
			MODI_ASSERT(0);
			return false;
		}

		if(!xml.GetNodeStr(p_script, "source", script_source))
		{
			Global::logger->debug("[load_script] load script failed source not get");
			MODI_ASSERT(0);
			return false;
		}

		if(script_type == "enter")
		{
			if(!MODI_EnterScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if(script_type == "task")
		{
			if(!MODI_TaskScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if(script_type == "time")
		{
			if(!MODI_TimeScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "active")
		{
			if( !MODI_ActivityManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if ( script_type == "chenghao" )
		{
			if( !MODI_ChenghaoScriptManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "buyitem")
		{
			if( !MODI_BuyItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "useitem")
		{
			if( !MODI_UseItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "haveitem" )
		{
			if( !MODI_ItemImpactManager::GetInstance().LoadScript( script_source.c_str()))
			{
				return false;
			}	
		}
		else if( script_type == "expire" )
		{
			if( !MODI_ItemExpireManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}	
		}
		else if( script_type == "music" )
		{
			if( !MODI_SingMusicManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else if( script_type == "beuseditem")
		{
			if( !MODI_BeUsedItemManager::GetInstance().LoadScript(script_source.c_str()))
			{
				return false;
			}
		}
		else
		{
			Global::logger->debug("[load_script] load script failed type not find");
			MODI_ASSERT(0);
			return false;
		}
		
		/// 下一个节点
		p_script = xml.GetNextNode(p_script, "script");
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[load_script] reload load script successful");
#endif

	return true;
}
