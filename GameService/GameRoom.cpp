#include "GameRoom.h"
#include "protocol/c2gs.h"
#include "GameChannel.h"
#include "GameTick.h"
#include "Share.h"
#include "BinFileMgr.h"
#include <math.h>
#include "protocol/gamedefine.h"
#include "MdmMgr.h"
#include "LoggerInfo.h"
#include "LoggerAction.h"
#include "AssertEx.h"
#include "Base/HelpFuns.h"
#include "BaseOS.h"
#include "Scene.h"


MODI_GameRoom::MODI_GameRoom( defRoomID id ):m_iStatus(ROOM_STATUS_USING)
{
	m_info.m_roomID = id;
	m_pScene = NULL;
}

MODI_GameRoom::~MODI_GameRoom()
{

}

void MODI_GameRoom::SetRoomScene(MODI_Scene * p_scene)
{
	if(p_scene)
	{
		m_pScene = p_scene;
#ifdef _DEBUG		
		Global::logger->debug("[set_room_scene] set scene <id=%u>", p_scene->GetSceneId());
#endif		
		m_info.m_wdSceneId = p_scene->GetSceneId();
	}
	else
	{
		m_pScene = p_scene;
		m_info.m_wdSceneId = 0;
	}
}

MODI_Scene * MODI_GameRoom::GetRoomScene()
{
	return m_pScene;
}

void MODI_GameRoom::ChangeMusicID(defMusicID id , enMusicFlag flag , enMusicSType st , enMusicTime mt )
{
    if (GetNormalInfo().m_nMusicId != id || 
			GetNormalInfo().m_flag != flag || 
			GetNormalInfo().m_songType != st ||
			GetNormalInfo().m_timeType != mt )
    {
		OnChangeMusic( GetNormalInfo().m_nMusicId , id , flag , st , mt );
       	GetNormalInfo_Modfiy().m_nMusicId = id;
		GetNormalInfo_Modfiy().m_flag = flag;
		GetNormalInfo_Modfiy().m_songType = st;
		GetNormalInfo_Modfiy().m_timeType = mt;
    }
}

void MODI_GameRoom::ChangeMapID(defMapID id)
{
    if (GetNormalInfo().m_nMapId != id)
    {
		OnChangeMap( GetNormalInfo().m_nMapId , id );
        GetNormalInfo_Modfiy().m_nMapId = id;
    }
}


void MODI_GameRoom::ChangeGameMode( ROOM_SUB_TYPE newtype )
{
	ROOM_SUB_TYPE old = GetRoomSubType();
	if( old != newtype && newtype > ROOM_STYPE_BEGIN && newtype < ROOM_STYPE_END )
	{
		OnChangeGameMode( old , newtype );
	}
}

void MODI_GameRoom::SendMdmToClient(MODI_ClientAvatar  * pClient)
{
	if( pClient )
	{
		pClient->SendMdmToClient( GetMusicID() );
	}
}


bool MODI_GameRoom::IsPasswordRoom()
{
    return RoomHelpFuns::IsHasPassword(GetRoomInfo().m_roomType);
}

ROOM_TYPE 	MODI_GameRoom::GetRoomType() const
{
	return (ROOM_TYPE)RoomHelpFuns::GetRoomMainType( GetRoomInfo().m_roomType );
}

ROOM_SUB_TYPE MODI_GameRoom::GetRoomSubType() const
{
	return (ROOM_SUB_TYPE)RoomHelpFuns::GetRoomSubType( GetRoomInfo().m_roomType );
}

void MODI_GameRoom::SetRoomType( defRoomType type )
{
	GetRoomInfo_Modfiy().m_roomType = type;
}

void MODI_GameRoom::SetRoomName( const char * szName )
{
	safe_strncpy( GetRoomInfo_Modfiy().m_cstrRoomName , 
			szName , 
			sizeof(GetRoomInfo().m_cstrRoomName) );
}

void MODI_GameRoom::SetPassword( const char * szPwd )
{
	if( !szPwd )
	{
		m_strPassword.clear();
		RoomHelpFuns::SetHasPassword( GetRoomInfo_Modfiy().m_roomType , false );
		return ;
	}
	
	m_strPassword = szPwd;
	if( !m_strPassword.empty() )
	{
		RoomHelpFuns::SetHasPassword( GetRoomInfo_Modfiy().m_roomType , true );
	}
	else 
	{
		RoomHelpFuns::SetHasPassword( GetRoomInfo_Modfiy().m_roomType , false );
	}
}
	
defMusicID 	MODI_GameRoom::GetRandomMusicID() const
{
	defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
	if( musicBin.GetSize() == 0 )
		return RANDOM_MUSIC_ID;

	int i = 0;
	while( i < 1000)
	{
		i++;
		size_t nRmusic = PublicFun::GetRandNum(1,100000) % (musicBin.GetSize());
		const GameTable::MODI_Musiclist * pMusicInfo = musicBin.GetByIndex( nRmusic );
		if (!pMusicInfo)
		{
			MODI_ASSERT(0);
			return 1;
		}
		if( pMusicInfo->get_SingType() != kMusicSongType_Off && pMusicInfo->get_SingType() != kMusicSongType_NewPlayer )
		{
			return pMusicInfo->get_ConfigID();
		}
	}

	return RANDOM_MUSIC_ID;
}

enMusicFlag MODI_GameRoom::GetRandomMusicFlag( defMusicID id ) const
{
	defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
	if( musicBin.GetSize() == 0 )
		return kMusicFlag_Yuanchang;

	const GameTable::MODI_Musiclist * pMusicInfo = musicBin.Get( id );
	if (!pMusicInfo)
	{
		MODI_ASSERT(0);
		return kMusicFlag_Yuanchang;
	}

	if( pMusicInfo->get_accompaniment() == 0 )
		return kMusicFlag_Yuanchang;

	enMusicFlag ret = (enMusicFlag)(PublicFun::GetRandNum(1,10000) % kMusicFlag_Count);
	return ret;
}

enMusicSType MODI_GameRoom::GetRandomSongType( defMusicID id ) const
{
	defMusicBinFile & musicBin = MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile();
	if( musicBin.GetSize() == 0 )
		return kMusicSongType_Heng;

	const GameTable::MODI_Musiclist * pMusicInfo = musicBin.Get( id );
	if (!pMusicInfo)
	{
		MODI_ASSERT(0);
		return kMusicSongType_Heng;
	}

	if( pMusicInfo->get_SingType() == kMusicSongType_NewPlayer || pMusicInfo->get_SingType() == kMusicSongType_Off )
		return kMusicSongType_Heng;

	if( pMusicInfo->get_SingType() == kMusicSongType_Heng )
		return kMusicSongType_Heng;

	if( pMusicInfo->get_SingType() == kMusicSongType_Song)
		return kMusicSongType_Song;
	enMusicSType ret = (enMusicSType)(PublicFun::GetRandNum(1,10000) % (kMusicSongType_Heng) + 1);
	return ret;
}

defMapID 	MODI_GameRoom::GetRandomMapID() const
{
	static unsigned int nRandMapMax = 255;
	defMapID 	randmaps[nRandMapMax];
	defScenelistBinFile & sBin = MODI_BinFileMgr::GetInstancePtr()->GetSceneBinFile();
	size_t nSize = sBin.GetSize();
	if( nSize > nRandMapMax )
		nSize = nRandMapMax;

	size_t nRandMapCount = 0;
	for( size_t n = 0; n < nSize; n++ )
	{
		const GameTable::MODI_Scenelist * pConfig = sBin.GetByIndex( n );
		if( !pConfig )
			continue ;
		if( pConfig->get_Type() != kGameScene )
			continue ;

// 		if( GetRoomType() == ROOM_TYPE_NORMAL &&  pConfig->get_Small() )
// 		{
// 			randmaps[nRandMapCount++] =  pConfig->get_ConfigID();
// 		}
// 		else if( GetRoomType() == ROOM_TYPE_BIG && pConfig->get_Large() )
// 		{
// 			randmaps[nRandMapCount++] =  pConfig->get_ConfigID();
// 		}
		randmaps[nRandMapCount++] = pConfig->get_ConfigID();
	}
	
	if( !nRandMapCount )
	{
		return false;
	}

	size_t nRandIdx = PublicFun::GetRandNum(1,10000) % nRandMapCount;
	return randmaps[nRandIdx];
}

const MODI_GUID & MODI_GameRoom::GetMaster() const
{
	return m_info.m_masterID;
}

void 	MODI_GameRoom::SetMaster(const MODI_GUID & master )
{
	m_info.m_masterID = master;
}

