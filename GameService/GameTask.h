/** 
 * @file GameTask.h
 * @brief 网关连接
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef _MDGAMETASK_H
#define _MDGAMETASK_H

#include "Type.h"
#include "SvrPackageProcessBase.h"
#include "CmdFlux.h"

/**
 * @brief 网关连接类
 *
 */

class MODI_GameTask : public MODI_IServerClientTask
{
public:
    MODI_GameTask(const int sock, const struct sockaddr_in *addr);

    virtual ~MODI_GameTask();

	// 连接上的回调,可以安全的在逻辑线程调用
	virtual void OnConnection(); 

	// 断开的回调，可以安全的在逻辑线程调用
	virtual void OnDisconnection(); 

    /// 命令处理
    bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

    /// 回收连接
    int RecycleConn();

    /// 命令处理
    bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen);


    ///   添加game session的处理
    void OnAddSession(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);
    ///    删除game session的处理
    void OnDelSession(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);


	/// 处理网关命令
	int OnHandlerGatewayCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	/// 处理游戏命令
	int OnHandlerGameClientCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	/** 
	 * @brief 处理所有客户端的数据包
	 */
	static void 	ProcessAllPackage();

	static MODI_SvrPackageProcessBase 	ms_ProcessBase;

private:

    //// 游戏用户
    MODI_RTime m_stLifeTime;

public:

    static MODI_GameTask * ms_pInstancePtr;

	static MODI_CmdFlux m_stRecvCmdFlux;
	static MODI_CmdFlux m_stSendCmdFlux;

};

#endif
