/**
 * @file   Bag.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jun 15 16:25:35 2011
 * 
 * @brief  包裹对象
 * 
 * 
 */

#include "Bag.h"
#include "AssertEx.h"
#include "ZoneClient.h"
#include "s2rdb_cmd.h"
#include "ClientAvatar.h"
#include "ItemRuler.h"
#include "BinFileMgr.h"
#include "ItemOperator.h"

/** 
 * @brief 构造函数
 * 
 */
MODI_Bag::MODI_Bag( MODI_ClientAvatar * pOwner , const MODI_PackageType & type ): m_bagType(type),m_pOwner(pOwner)
{
	if(type == enBagType_Avatar)
	{
		m_wdBagSize = MAX_BAG_AVATAR_COUNT;
	}
	else if(type == enBagType_PlayerBag)
	{
		m_wdBagSize = MAX_BAG_PLAYER_COUNT;
	}

	/// 初始化位置信息
	for(WORD i=1; i<=m_wdBagSize; i++)
	{
		m_stBagPosInfo[i].Init(i, this);
	}
}


MODI_Bag::~MODI_Bag()
{
	///是否内存
	MODI_GameItemInfo * p_itme = NULL;
	defItemInfoIter iter = m_stItemInfoMap.begin();
	for(; iter != m_stItemInfoMap.end(); iter++)
	{
		p_itme = iter->second;
		if(p_itme)
		{
			delete p_itme;
			p_itme = NULL;
			iter->second = p_itme;
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
}

/// 清空
void MODI_Bag::Reset()
{
	m_stItemInfoMap.clear();
	m_stPosInfoMap.clear();
	
	/// 初始化位置信息
	for(WORD i=1; i<=m_wdBagSize; i++)
	{
		m_stBagPosInfo[i].Init(i, this);
	}
}

/// 备份
void MODI_Bag::Backup()
{
	m_stItemInfoMapBackup.clear();
	m_stItemInfoMapBackup = m_stItemInfoMap;
}


/// 还原
void MODI_Bag::ReBackup()
{
	if(m_stItemInfoMapBackup.size() == 0)
	{
		MODI_ASSERT(0);
		return;
	}

	Reset();
	MODI_GameItemInfo * p_item = NULL;
	defItemInfoIter iter = m_stItemInfoMapBackup.begin();
	for(; iter!=m_stItemInfoMapBackup.end(); iter++)
	{
		p_item = iter->second;
		AddItemNoUpdate(p_item);
	}
	m_stItemInfoMapBackup.clear();
}

	
/** 
 * @brief 添加一个物品
 * 
 * @param create_info 物品属性
 *
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::AddItemToBag(const MODI_CreateItemInfo & create_info, QWORD & get_item_id)
{
	const DWORD & config_id = create_info.m_dwConfigId;
	const DWORD & item_limit = create_info.m_byLimit;
	const enAddItemReason & add_reason = create_info.m_enCreateReason;
	const WORD item_count = 1;
	
	MODI_ClientAvatar * p_client = GetOwner();
	if(p_client == NULL)
	{
		Global::logger->fatal("[add_item] add item to bag but not owner");
		MODI_ASSERT(0);
		return false;
	}

	if((MODI_ItemOperator::IsCanHaveItem(this, config_id, item_count)) != MODI_ItemOperator::enOK)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_GameItemInfo * item_info = new MODI_GameItemInfo();
	if(! item_info)
	{
		MODI_ASSERT(0);
		return false;
	}
		
	item_info->Init(create_info);
	
	if(! item_info->AddMeToBag(this))
	{
		Global::logger->debug("[add_item_to_bag] add failed <configid=%u,item_limit=%u,add_reason=%d,itme_id=%llu>",
							  config_id, item_limit, add_reason, item_info->GetItemId());
		MODI_ASSERT(0);
		return false;
	}
	
	get_item_id = item_info->GetItemId();
	
	return true;
}




/** 
 * @brief 添加一个物品
 * 
 * @param create_info 物品属性
 *
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::AddItemToBag(const MODI_CreateItemInfo & create_info, QWORD & get_item_id, MODI_ByteBuffer & save_sql)
{
	const DWORD & config_id = create_info.m_dwConfigId;
	const DWORD & item_limit = create_info.m_byLimit;
	const enAddItemReason & add_reason = create_info.m_enCreateReason;
	const WORD item_count = 1;
	
	MODI_ClientAvatar * p_client = GetOwner();
	if(p_client == NULL)
	{
		Global::logger->fatal("[add_item] add item to bag but not owner");
		MODI_ASSERT(0);
		return false;
	}

	if((MODI_ItemOperator::IsCanHaveItem(this, config_id, item_count)) != MODI_ItemOperator::enOK)
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_GameItemInfo * item_info = new MODI_GameItemInfo();
	if(! item_info)
	{
		MODI_ASSERT(0);
		return false;
	}
		
	item_info->Init(create_info);
	
	if(! item_info->AddMeToBag(this, save_sql))
	{
		Global::logger->debug("[add_item_to_bag] add failed <configid=%u,item_limit=%u,add_reason=%d,itme_id=%llu>",
							  config_id, item_limit, add_reason, item_info->GetItemId());
		MODI_ASSERT(0);
		return false;
	}
	
	get_item_id = item_info->GetItemId();
	return true;
}


/** 
 * @brief 从包裹里面删除某件道具
 * 
 * @param item_id 道具id
 * 
 * @return 成功true
 *
 */
bool MODI_Bag::DelItemFromBag(const QWORD & item_id, const enDelItemReason del_reason)
{
	MODI_GameItemInfo * p_itme = NULL;
	defItemInfoIter iter = m_stItemInfoMap.find(item_id);
	if(iter == m_stItemInfoMap.end())
	{
		Global::logger->error("[del_item] not find item in bag <%llu>", item_id);
		MODI_ASSERT(0);
		return false;
	}
	
	p_itme = iter->second;	
	if(! p_itme)
	{
		Global::logger->error("[del_item] del item but not find <itemid=%llu>", item_id);
		MODI_ASSERT(0);
		return false;
	}

	p_itme->DelMeFromBag(del_reason);
	
	return true;
}



/** 
 * @brief 根据位置获取物品
 * 
 * @param pos 位置信息
 * 
 * @return 物品
 *
 */
MODI_GameItemInfo * MODI_Bag::GetItemByPos(const WORD & pos)
{
	MODI_GameItemInfo * p_item = NULL;
	MODI_BagPosInfo * p_pos = NULL;
	p_pos = GetPosInfo(pos);
	if(! p_pos)
	{
		MODI_ASSERT(0);
		return p_item ;
	}

   	p_item = p_pos->GetItemInfo();
	return p_item;
}


/** 
 * @brief 获取某个道具
 * 
 * @param item_id 道具id
 * 
 * @return null则是失败
 *
 */
MODI_GameItemInfo * MODI_Bag::GetItemByItemId(const QWORD & item_id)
{
	MODI_GameItemInfo * p_itme = NULL;
	defItemInfoIter iter = m_stItemInfoMap.find(item_id);
	if(iter != m_stItemInfoMap.end())
	{
		p_itme = iter->second;
	}
	return p_itme;
}


/** 
 * @brief 获取某个道具
 * 
 * @param config_id 物品属性id
 * 
 * @return null 失败
 */
MODI_GameItemInfo *  MODI_Bag::GetItemByConfigId(const DWORD & config_id)
{
	MODI_GameItemInfo * p_itme = NULL;
	MODI_BagPosInfo * p_bag_pos;
	defPosInfoIter iter = m_stPosInfoMap.find(config_id);
	if(iter == m_stPosInfoMap.end())
	{
		return p_itme;
	}
	p_bag_pos = iter->second;
	if(! p_bag_pos)
	{
		Global::logger->error("[get_item_by_config] not find pos info <configid=%u>", config_id);
		MODI_ASSERT(0);
		return p_itme;
	}

	QWORD item_id = p_bag_pos->GetItemId();
	if(item_id == 0)
	{
		Global::logger->error("[get_item_by_config] not find item id");
		//MODI_ASSERT(0);
		return NULL;
	}

	p_itme = GetItemByItemId(item_id);
	return p_itme;
}


/** 
 * @brief 获取某个道具
 * 
 * @param type 道具类型(喇叭)
 *
 * @param sub_class 子类型(频道喇叭)
 * 
 * @return null失败
 *
 */
MODI_GameItemInfo * MODI_Bag::GetItemBySubClass(const MODI_ItemType & type, const WORD & sub_class)
{
	MODI_GameItemInfo * p_item = NULL;
	defItemlistBinFile & itemBin = MODI_BinFileMgr::GetInstancePtr()->GetItemBinFile();

	for(int i = 1; i <= m_wdBagSize; i++)
	{
		MODI_BagPosInfo & pos_info = m_stBagPosInfo[i];
		if(pos_info.GetItemType() == type)
		{
			const GameTable::MODI_Itemlist * pElement = itemBin.Get(pos_info.GetConfigId());
			if(! pElement)
			{
				Global::logger->error("[get_item_by_sub] not find pElement <configid=%u>", pos_info.GetConfigId());
				MODI_ASSERT(0);
				return p_item;
			}
			if(pElement->get_SubClass() == sub_class)
			{
				p_item = pos_info.GetItemInfo();
				break;
			}
		}
	}
	return p_item;
}



/** 
 * 获取某类型的物品个数
 * 
 * @param type 类型
 * 
 * @return 类型个数
 *
 */
WORD MODI_Bag::GetCountByItemType(const MODI_ItemType & type)
{
	WORD nRet = 0;
	for( int i = 1; i <= m_wdBagSize; i++ )
	{
	    const MODI_BagPosInfo & pos_info = m_stBagPosInfo[i];
		if(pos_info.GetItemType() == type)
		{
			nRet += pos_info.GetCount();
		}
	}

	return nRet;
}


/** 
 * @brief 获取某种id的道具个数
 * 
 * @param config_id 道具id
 * 
 * @return 个数
 *
 */
WORD MODI_Bag::GetCountByItemConfigId(const WORD & config_id)
{
	WORD ret_count = 0;
	MODI_BagPosInfo * p_pos_info = NULL;
	defPosInfoRange ret_iter;
	ret_iter = m_stPosInfoMap.equal_range(config_id);
	for(; ret_iter.first != ret_iter.second; ret_iter.first++)
	{
		p_pos_info = (ret_iter.first)->second;
		if(p_pos_info)
		{
			ret_count += p_pos_info->GetCount();
		}
	}
	return ret_count;
}


/** 
 * @brief 获取空位置
 * 
 * @return 位置
 *
 */
MODI_BagPosInfo * MODI_Bag::GetEmptyPos()
{
	MODI_BagPosInfo * p_info = 0;
	for(int i = 1; i <= m_wdBagSize; i++)
	{
		p_info = &(m_stBagPosInfo[i]);
		if(p_info)
		{
			if(p_info->IsEmpty())
			{
				return p_info;
			}
		}
	}
	p_info = NULL;
	return p_info;
}


/** 
 * @brief 包裹是否满了
 * 
 * @return 满了true
 *
 */
bool MODI_Bag::IsFull() const
{
	for( int i = 1; i <= m_wdBagSize; i++ )
	{
		const MODI_BagPosInfo & pos_info = m_stBagPosInfo[i];
		if(pos_info.IsEmpty())
		{
			return false;
		}
	}

	return true;
}


/** 
 * @brief  获取空位置的个数
 * 
 * @return 空位置的个数
 *
 */
const WORD MODI_Bag::GetEmptyPosCount() const
{
	WORD nRet = 0;
	for( int i = 1; i <= m_wdBagSize; i++ )
	{
		const MODI_BagPosInfo & pos_info = m_stBagPosInfo[i];
		if(pos_info.IsEmpty())
		{
			nRet += 1;
		}
	}
	
	/// 留着换衣服
	if(nRet > MAX_BAG_AVATAR_COUNT)
	{
		return nRet - MAX_BAG_AVATAR_COUNT;
	}
	
	return 0;
}


/** 
 * @brief 包裹更新
 * 
 */
void MODI_Bag::Update(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_GameItemInfo * p_item = NULL;
	defItemInfoIter iter,next;
	for(next=m_stItemInfoMap.begin(),iter=next; iter!=m_stItemInfoMap.end(); iter=next)
	{
		if(next != m_stItemInfoMap.end())
		{
			next++;
		}
		
		p_item = iter->second;
		if(p_item)
		{
			p_item->Update(cur_rtime, cur_ttime);
		}
	}
}


/** 
 * @brief 增加一个道具,自动分配一个位置
 * 
 * @param p_item 此道具
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::AddItemUpdate(MODI_GameItemInfo * p_item)
{
	if(! p_item)
	{
		Global::logger->debug("[add_item_update] add item update faild null item");
		MODI_ASSERT(0);
		return false;
	}

	defItemInfoIter iter = m_stItemInfoMap.find(p_item->GetItemId());
	if(iter != m_stItemInfoMap.end())
	{
		MODI_GameItemInfo * p_info = iter->second;
		if(!p_info)
		{
			MODI_ASSERT(0);
			return false;
		}
		
		Global::logger->fatal("[add_item] add same item <oldid=%llu,newid=%llu,oldreason=%d,newreason=%d>",
							  p_info->GetItemId(),p_item->GetItemId(), p_info->GetCreateReason(), p_item->GetCreateReason());
		MODI_ASSERT(0);
		return false;
	}

	/// 加入到道具管理器里面
	m_stItemInfoMap.insert(defItemInfoValue(p_item->GetItemId(), p_item));
	
	int item_pos_count = m_stPosInfoMap.count(p_item->GetConfigId());
	MODI_BagPosInfo * p_pos_info = NULL;
	bool is_add = false;
	
	/// 之前有过
	if(item_pos_count > 0 && MODI_ItemRuler::IsCanLap(p_item->GetConfigId()))
	{
		defPosInfoRange ret_iter;
		ret_iter = m_stPosInfoMap.equal_range(p_item->GetConfigId());
		for(; ret_iter.first != ret_iter.second; ret_iter.first++)
		{
			p_pos_info = (ret_iter.first)->second;
			if(p_pos_info)
			{
				if(p_pos_info->AddItem(p_item))
				{
					is_add = true;
					break;
				}
			}
		}
	}
	
	/// 所有的位置都不能叠加了或者包裹里从来没有过
	if(is_add == false)
	{
		p_pos_info = GetEmptyPos();
		if(! p_pos_info)
		{
			Global::logger->error("[add_to_pos] add pos failed <configid=%u,itemid=%llu>", p_item->GetConfigId(), p_item->GetItemId());
			MODI_ASSERT(0);
			return false;
		}
		if(!p_pos_info->AddItem(p_item))
		{
			Global::logger->error("[add_to_pos] add pos failed <pos=%u,configid=%u,itemid=%llu>",p_pos_info->GetBagPos(),
								  p_item->GetConfigId(), p_item->GetItemId());
			MODI_ASSERT(0);
			return false;
		}

		///更新位置信息
		//m_stPosInfoMap.insert(defPosInfoValue(p_item->GetConfigId(), p_pos_info));
	}
	
	return true;
}



/** 
 * @brief 增加一个道具,但不需要分配位置
 * 
 * @param p_item 此道具
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::AddItemNoUpdate(MODI_GameItemInfo * p_item)
{
	if(! p_item)
	{
		Global::logger->debug("[add_item_update] add item update faild null item");
		MODI_ASSERT(0);
		return false;
	}

	defItemInfoIter iter = m_stItemInfoMap.find(p_item->GetItemId());
	if(iter != m_stItemInfoMap.end())
	{
		MODI_GameItemInfo * p_info = iter->second;
		if(!p_info)
		{
			MODI_ASSERT(0);
			return false;
		}
		
		Global::logger->fatal("[add_item] add same item <oldid=%llu,newid=%llu,oldreason=%d,newreason=%d>",
							  p_info->GetItemId(),p_item->GetItemId(), p_info->GetCreateReason(), p_item->GetCreateReason());
		MODI_ASSERT(0);
		return false;
	}

	if(p_item->GetBagType() != GetBagType())
	{
		MODI_ASSERT(0);
		return false;
	}

	/// 加入到道具管理器里面
	m_stItemInfoMap.insert(defItemInfoValue(p_item->GetItemId(), p_item));

	MODI_BagPosInfo * p_pos_info = NULL;
	p_pos_info = GetPosInfo(p_item->GetBagPos());
	if(! p_pos_info)
	{
		Global::logger->error("[add_to_pos] add pos failed <configid=%u,itemid=%llu>", p_item->GetConfigId(), p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}
	
	if(!p_pos_info->AddItem(p_item))
	{
		Global::logger->error("[add_to_pos] add pos failed <configid=%u,itemid=%llu>", p_item->GetConfigId(), p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	return true;
}


/** 
 * @brief 更新包裹位置信息
 * 
 */
void MODI_Bag::UpdatePosInfo(const DWORD config_id, MODI_BagPosInfo * p_info)
{
	m_stPosInfoMap.insert(defPosInfoValue(config_id, p_info));
}



/** 
 * @brief 删除道具
 * 
 * @param p_item 道具
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::DelItemUpdate(MODI_GameItemInfo * p_item)
{
	if(! p_item)
	{
		Global::logger->error("[del_item_update] del item but item null");
		MODI_ASSERT(0);
		return false;
	}
	
	defItemInfoIter item_iter = m_stItemInfoMap.find(p_item->GetItemId());
	if(item_iter == m_stItemInfoMap.end())
	{
		Global::logger->error("[del_item_update] del item but not find <itemid=%llu>", p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	MODI_BagPosInfo * p_pos_info = GetPosInfo(p_item->GetBagPos());
	if(!p_pos_info)
	{
		Global::logger->error("[del_item_update] del item but not pos <itemid=%llu>", p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	p_pos_info->DelItem(p_item);
	if(p_pos_info->GetCount() == 0)
	{
		/// 删除这个位置信息
		defPosInfoIter iter = m_stPosInfoMap.begin();
		MODI_BagPosInfo * p_info = NULL;
		for(; iter != m_stPosInfoMap.end(); iter++)
		{
			p_info = iter->second;
			if(p_info)
			{
				if(p_info->GetBagPos() == p_pos_info->GetBagPos())
				{
					m_stPosInfoMap.erase(iter);
					break;
				}
			}
		}
	}

	/// 从管理器中删除
	delete p_item;
	p_item = NULL;
	m_stItemInfoMap.erase(item_iter);
	return true;
}



/** 
 * @brief 删除道具,只是更新包裹
 * 
 * @param p_item 道具
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::DelItemNoUpdate(MODI_GameItemInfo * p_item)
{
	if(! p_item)
	{
		Global::logger->error("[del_item_update] del item but item null");
		MODI_ASSERT(0);
		return false;
	}
	
	defItemInfoIter item_iter = m_stItemInfoMap.find(p_item->GetItemId());
	if(item_iter == m_stItemInfoMap.end())
	{
		Global::logger->error("[del_item_update] del item but not find <itemid=%llu>", p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	MODI_BagPosInfo * p_pos_info = GetPosInfo(p_item->GetBagPos());
	if(!p_pos_info)
	{
		Global::logger->error("[del_item_update] del item but not pos <itemid=%llu>", p_item->GetItemId());
		MODI_ASSERT(0);
		return false;
	}

	p_pos_info->DelItem(p_item);
	if(p_pos_info->GetCount() == 0)
	{
		/// 删除这个位置信息
		defPosInfoIter iter = m_stPosInfoMap.begin();
		MODI_BagPosInfo * p_info = NULL;
		for(; iter != m_stPosInfoMap.end(); iter++)
		{
			p_info = iter->second;
			if(p_info)
			{
				if(p_info->GetBagPos() == p_pos_info->GetBagPos())
				{
					m_stPosInfoMap.erase(iter);
					break;
				}
			}
		}
	}

	m_stItemInfoMap.erase(item_iter);
	return true;
}



/** 
 * @brief 发送道具列表
 * 
 * @param p_item_info 道具信息
 * 
 * @return 道具个数
 *
 */
const WORD MODI_Bag::NotifyItemList()
{
	WORD item_size = 0;
	MODI_ClientAvatar * p_client = GetOwner();
	if(!p_client)
	{
		MODI_ASSERT(0);
		return 0;
	}
	
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_Itemlist * p_send_cmd = (MODI_GS2C_Notify_Itemlist *)buf;
	AutoConstruct(p_send_cmd);
	MODI_ItemInfo * p_item_info = NULL;
	p_item_info = p_send_cmd->m_pItemlist;

	defItemInfoIter iter = m_stItemInfoMap.begin();
	MODI_GameItemInfo * p_item = NULL;
	for(; iter != m_stItemInfoMap.end(); iter++)
	{
		p_item = iter->second;
		if(p_item)
		{			
			p_item_info->m_qdItemId = p_item->GetItemId();
			p_item_info->m_dwConfigId = p_item->GetConfigId();
			p_item_info->m_byBagType = p_item->GetBagType();
			p_item_info->m_wdBagPos = p_item->GetBagPos();
			p_item_info->m_dwRemainTime = p_item->GetRemainTime();
		}
		else
		{
			MODI_ASSERT(0);
		}
		p_item_info++;
		p_send_cmd->m_nItemSize++;
		item_size++;
		if(sizeof(MODI_GS2C_Notify_Itemlist) + sizeof(MODI_ItemInfo) * p_send_cmd->m_nItemSize >
		   (Skt::MAX_USERDATASIZE - (sizeof(MODI_ItemInfo) * p_send_cmd->m_nItemSize+128)))
		{
			p_client->SendPackage(p_send_cmd, sizeof(MODI_GS2C_Notify_Itemlist)+sizeof(MODI_ItemInfo)*p_send_cmd->m_nItemSize);
			memset(buf, 0, sizeof(buf));
			AutoConstruct(p_send_cmd);
			p_item_info = p_send_cmd->m_pItemlist;
			
#ifdef _ITEM_DEBUG
			Global::logger->debug("[send_item_info_to_count] <size=%u>",p_send_cmd->m_nItemSize);
			MODI_ItemInfo * p_debug_info = p_send_cmd->m_pItemlist;
			for(WORD i=0; i<p_send_cmd->m_nItemSize; i++)
			{
				Global::logger->debug("[send_item_info] <itemid=%llu,configid=%u,bag_type=%d,bag_pos=%u,time=%u>" ,
							  p_debug_info->m_qdItemId, p_debug_info->m_dwConfigId,p_debug_info->m_byBagType,p_debug_info->m_wdBagPos,
							  p_debug_info->m_dwRemainTime);
				p_debug_info++;
			}
#endif
			
		}
	}
	if(p_send_cmd->m_nItemSize > 0)
	{
		p_client->SendPackage(p_send_cmd, sizeof(MODI_GS2C_Notify_Itemlist)+sizeof(MODI_ItemInfo)*p_send_cmd->m_nItemSize);
#ifdef _ITEM_DEBUG
			Global::logger->debug("[send_item_info_to_count] <size=%u>",p_send_cmd->m_nItemSize);
			MODI_ItemInfo * p_debug_info = p_send_cmd->m_pItemlist;
			for(WORD i=0; i<p_send_cmd->m_nItemSize; i++)
			{
				Global::logger->debug("[send_item_info] <itemid=%llu,configid=%u,bag_type=%d,bag_pos=%u,time=%u>" ,
							  p_debug_info->m_qdItemId, p_debug_info->m_dwConfigId,p_debug_info->m_byBagType,p_debug_info->m_wdBagPos,
							  p_debug_info->m_dwRemainTime);
				p_debug_info++;
			}
#endif
	}
	return item_size;
}



/** 
 * @brief 是否拥有道具
 * 
 * @param item_id 道具id
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Bag::IsHaveItem(const QWORD & item_id)
{
	defItemInfoIter iter = m_stItemInfoMap.find(item_id);
	if(iter != m_stItemInfoMap.end())
	{
		return true;
	}
	return false;
}

