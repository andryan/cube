/**
 * @file GameService.h
 * @date 2009-12-25 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 游戏服务器
 *
 */

#ifndef _MDGAMESERVICE_H
#define _MDGAMESERVICE_H

#include "NetService.h"
//#include "ServiceTaskPoll.h"
#include "ServiceTaskSched.h"
#include "SingleObject.h"

/**
 * @brief 主游戏服务器
 *
 */
class MODI_GameService: public MODI_NetService , public CSingleObject<MODI_GameService>
{
public:

    /// 一个新的连接
    bool CreateTask(const int sock, const struct sockaddr_in *addr);

    /// 初始化
    bool Init();

    /// 释放资源
    virtual void Final()
    {
		m_pTaskSched.Final();
    }

    MODI_GameService() :
        MODI_NetService("GameService")
    {

    }

    ~MODI_GameService()
    {
		
    }

	void DealSignalUsr2();

	void ReloadConfig();

private:

    /// 事件轮询
    //MODI_ServiceTaskPoll * m_pTaskPoll;
	MODI_ServiceTaskSched m_pTaskSched;
};

#endif
