/**
 * @file BinTable
 * @date 2009/12/15
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief	
 *			Bin�ļ���
 */	

#ifndef MODI_BIN_FILE_H_
#define MODI_BIN_FILE_H_


#ifdef _MSC_VER
#pragma once 
#endif

#include <map>
#include <assert.h>

/**
* @brief  ģ����,������ȡ*.Bin�ļ�.����Ϸ�б����ݽṹ�Ĺ���
*
* 
*/
template <typename K,typename V,typename l = std::less<K> >
class	MODI_BinFile
{
public:
	MODI_BinFile() {}
	~MODI_BinFile() { Unload(); }

public:

	/**
	 * @brief 	װ��һ��*.BIN�ļ�
	 *
	 * @param	�ļ���
	 * @return	�ɹ����� TRUE
	 *
	 */
	bool	Load( const char * szFile )
	{
		FILE * fp = fopen( szFile , "rb" );
		if( !fp )
			return false;

		Unload();

		//	����ݿ�
		while ( !feof(fp) )
		{
			V * v = new V();
			size_t nObjSize = sizeof(V);
			size_t nRet = fread( (void *)v , nObjSize  , 1 , fp );
			if( !nRet )
			{
				delete v;
				continue ;
			}

			TABLES_INSERT_RESULT r = m_mapTable.insert( std::make_pair( v->get_ConfigID() , v ) );
			if( !r.second )
			{
                printf("Config Id %d \n",v->get_ConfigID());

				delete v;
				fclose(fp);
#ifdef _DEBUG
				char szErr[512];
				printf( szErr , "[MODI_BinFile::Load] , File <%s> is Invaild !" , szFile );
				assert( szErr && 0 );
#endif
				return false;
			}
		}

		fclose(fp);
		return true;
	}

	bool LoadFromMemory( const char* pContext,unsigned int Size)
	{
		if(!pContext)
			return false;

		Unload();

		unsigned int CurrentPos = 0;
		while(CurrentPos < Size)
		{
			V * v = new V();
			size_t nObjSize = sizeof(V);
			memcpy((void *)v , (void*)pContext, nObjSize);
			CurrentPos += nObjSize;
			pContext += nObjSize;

			TABLES_INSERT_RESULT r = m_mapTable.insert( std::make_pair( v->get_ConfigID() , v ) );
			if( !r.second )
			{
				delete v;
				return false;
			}
		}
		return true;
	}

	/**
	 * @brief 	ж��һ�� *.Bin �ļ�
	 *
	 * @return
	 *
	 */
	void	Unload()
	{
		TABLES_CONST_ITER  itor = m_mapTable.begin();
		while ( itor != m_mapTable.end() )
		{
			const V * pTemp = itor->second;
			delete pTemp;
			itor++;
		}
		m_mapTable.clear();
	}


	/**
	 * @brief 	��ݱ��ID�����һ��������
	 *
	 * @param 
	 * @return	������ݵ�ֻ��ָ��
	 *
	 */
	const V *	Get( K key ) const
	{
		TABLES_CONST_ITER  itor = m_mapTable.find( key );
		if( itor != m_mapTable.end() )
		{
			return itor->second;
		}
		return 0;
	}

	/**
	 * @brief 	��ݱ��ID�����ã����һ��������
	 *
	 * @param 
	 * @return	������ݵ�ֻ��ָ��
	 *
	 */
	const V *	GetByKeyRef( const K & key ) const
	{
		TABLES_CONST_ITER  itor = m_mapTable.find( key );
		if( itor != m_mapTable.end() )
		{
			return itor->second;
		}
		return 0;
	}

    size_t   GetSize()
    {
        return m_mapTable.size();
    }

	const V * GetByIndex( size_t nIdx )
	{
		if( nIdx < GetSize() )
		{
			TABLES_ITER itor = m_mapTable.begin();
			size_t n = 0;
			while( n < nIdx )
			{
				itor++;
				n++;
			}
	
			return itor->second;
		}
		return 0;
	}
	
private:

	typedef typename std::map<K,const V *,l>				TABLES;
	typedef typename TABLES::iterator						TABLES_ITER;
	typedef typename TABLES::const_iterator					TABLES_CONST_ITER;
	typedef typename std::pair<TABLES_ITER,bool>			TABLES_INSERT_RESULT;

	TABLES													m_mapTable;

};

#endif
