#include "DBCDKey.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "Global.h"
#include "gamestructdef.h"
#include "Base/HelpFuns.h"


MODI_DBCDKey::MODI_DBCDKey( MODI_DBClient * pDBHandler ):MODI_IDBBase(pDBHandler)
{
	memset( m_szKey , 0 , sizeof(m_szKey) );
	m_charid = INVAILD_CHARID;
	m_bExist = false;
}

MODI_DBCDKey::~MODI_DBCDKey()
{

}

bool	MODI_DBCDKey::Load()
{
	if( GetKey()[0] == '\0' )
		return false;

	const char * szSql = MODI_SqlStatement::CreateStatement_GetYunYingCDKey( GetKey() );
	if( !szSql )
	{
		MODI_ASSERT(0);
		return false;
	}

	SetSqlStatement( szSql );
	if( !MODI_IDBBase::Load() )
		return false;

	return true;
}

bool	MODI_DBCDKey::ParseLoadResult(void* pResult)
{
	MODI_Record * pRecord = m_pResults->GetRecord( 0 ); 
	
	if( !pRecord )
		return false;

	m_bExist = true;
	const MODI_VarType & v_item = pRecord->GetValue( MODI_SqlStatement::GetFieldInYunYingCDKey( enYunYingCDKey_items ) );
	const MODI_VarType & v_isvalid = pRecord->GetValue( MODI_SqlStatement::GetFieldInYunYingCDKey( enYunYingCDKey_isvalid ) );


	MODI_YunYingCDKeyContent * res = (MODI_YunYingCDKeyContent *)(pResult);
	res->m_bIsValid = v_isvalid.ToInt() == 0 ? false : true;
	safe_strncpy( res->m_szContent  , v_item.ToStr() , sizeof(res->m_szContent) );
	return true;
}

bool	MODI_DBCDKey::Save(const void* pSource)
{
	int iIsvalid = *((int *)(pSource));

	const char * szSql = MODI_SqlStatement::CreateStatement_SaveYunYingCDKeyValid( GetKey() , iIsvalid , m_charid );
	if( !szSql )
	{
		MODI_ASSERT(0);
		return false;
	}

	SetSqlStatement( szSql );
	if( MODI_IDBBase::Save( pSource ) == false )
		return false;
	return true;
}


