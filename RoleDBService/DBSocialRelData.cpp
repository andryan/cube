#include "DBSocialRelData.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "Global.h"
#include "gamestructdef.h"


MODI_DBSocialRelData::MODI_DBSocialRelData( MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler ) , 
	m_nCharID(INVAILD_CHARID)
{

}

MODI_DBSocialRelData::~MODI_DBSocialRelData()
{

}

bool	MODI_DBSocialRelData::Save(const void* pSource)
{
	if( !m_pDBClient )
		return  false;

	const MODI_SocialRelationData * pSocial = (const MODI_SocialRelationData *)(pSource);
	const char * szSql = MODI_SqlStatement::CreateStatement_SaveRelData( GetCharGuid() , pSocial );
	if( !szSql )
	{
		Global::logger->error("[%s] can't save charid<%u>'s social relation data to db , %s." ,
				ROLEDATA_OPT ,
				this->GetCharGuid()	,
				this->GetLastErrorDes() );
		MODI_ASSERT( szSql );
		return false;
	}
	this->SetSqlStatement( szSql );

	if( !MODI_IDBBase::Save( pSource ) )
	{
		Global::logger->error("[%s] can't save charid<%u>'s social relation to db , %s." ,
				ROLEDATA_OPT ,
				this->GetCharGuid()	,
				this->GetLastErrorDes() );
		return false;
	}

	return true;
}

void	MODI_DBSocialRelData::SetCharGuid(MODI_CHARID guid)
{
	m_nCharID = guid;
}

MODI_CHARID		MODI_DBSocialRelData::GetCharGuid()
{
	return m_nCharID;
}
