#include "DBCharBaseinfo.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "s2rdb_cmd.h"
#include "Global.h"
#include "HelpFuns.h"
#include "Encrypt.h"
#include "SplitString.h"

MODI_DBCharBaseinfo::MODI_DBCharBaseinfo( MODI_DBClient * pDBHandler ):
	MODI_IDBBase(pDBHandler),
	m_nAccount(INVAILD_ACCOUNT_ID),
	m_nCharID(INVAILD_CHARID)
{

}

MODI_DBCharBaseinfo::~MODI_DBCharBaseinfo()
{

}

bool	MODI_DBCharBaseinfo::Load()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_Load_CharData( this->GetAccountID() , 
			this->GetCharGuid() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );

	if( MODI_IDBBase::Load() )
		return true;

	return false;
}

bool	MODI_DBCharBaseinfo::Save(const void* pSource)
{
	const MODI_DBSaveCharBaseInfo * pSave = (const MODI_DBSaveCharBaseInfo *)(pSource);
	if( !pSave->pExtraData  ||
		!pSave->pRoleData)
		return false;

	const char * szSql = MODI_SqlStatement::CreateStatement_Update_CharData( this->GetCharGuid() , pSave);

	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );

	if( MODI_IDBBase::Save( pSource ) )
		return true;

	return false;
}

bool	MODI_DBCharBaseinfo::AddNew(const void * pSource)
{
	return false;
}

bool	MODI_DBCharBaseinfo::Delete()
{
	return false;
}

bool	MODI_DBCharBaseinfo::ParseLoadResult(void* pResult)
{
	if( !m_pResults )
		return false;

	MODI_Record * pRecord = m_pResults->GetRecord( 0 );
	if( !pRecord )
	{
		Global::logger->warn("[%s] accid<%u> load char data was faild . get record by index=0 , it's null ptr " , 
				ROLEDATA_OPT , 
				this->GetAccountID() );
		return false;
	}

#ifdef _XXP_DEBUG
	Global::logger->debug("[server_debug] the size of full datat is ..................%d,",sizeof( MODI_CharactarFullData));
#endif

	MODI_RDB2S_Notify_LoadCharData * msg  = (MODI_RDB2S_Notify_LoadCharData *)(pResult);
	MODI_CharactarFullData * pFullData = &(msg->m_CharFullData);
	MODI_RoleInfo * pRoleinfo = &pFullData->m_roleInfo;
	MODI_RoleExtraData * pRoleExtraData = &pFullData->m_roleExtraData;

	const char * szUnixTimeStamp_login = "UNIX_TIMESTAMP(`lastlogintime`)";
	const char * szUnixTimeStamp_idol = "UNIX_TIMESTAMP(`lastidoltime`)";
	const char * szUnixTimeStamp_register = "UNIX_TIMESTAMP(`registertime`)";

	const MODI_VarType & v_charid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_guid));
	const MODI_VarType & v_accountid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_accountid));
	const MODI_VarType & v_accname = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_accname));
	const MODI_VarType & v_rolename = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_name));
	const MODI_VarType & v_age = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_age));
	const MODI_VarType & v_city = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_city));
	const MODI_VarType & v_sign = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_personalsign));
	const MODI_VarType & v_group = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_group));
	const MODI_VarType & v_clan = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_clan));
	const MODI_VarType & v_exp = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_exp));
	const MODI_VarType & v_qq = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_qq));
	const MODI_VarType & v_sex = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_sex));
	const MODI_VarType & v_blood = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_blood));
	const MODI_VarType & v_level = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_level));
	const MODI_VarType & v_birthday = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_birthday));
	const MODI_VarType & v_renqi = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_renqi));
	const MODI_VarType & v_fensib = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_fensiboy));
	const MODI_VarType & v_fensig = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_fensigril));
	const MODI_VarType & v_honeyinfo = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_honeyinfo));
	const MODI_VarType & v_defavatar = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_defavatar));
	const MODI_VarType & v_lastlogintime = pRecord->GetValue(szUnixTimeStamp_login );
	const MODI_VarType & v_lastloginip = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_lastloginip));
	const MODI_VarType & v_money = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_money));
	const MODI_VarType & v_moneyRMB = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_moneyRMB));
	const MODI_VarType & v_lastidoltime = pRecord->GetValue(szUnixTimeStamp_idol );
	const MODI_VarType & v_registertime = pRecord->GetValue(szUnixTimeStamp_register);
	const MODI_VarType & v_win = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_wincount ) );
	const MODI_VarType & v_loss = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_losscount ) );
	const MODI_VarType & v_tie = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_tiecount ) );
	const MODI_VarType & v_vote = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_votecount ) );
	const MODI_VarType & v_gmlevel = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_gmlevel ) );
	const MODI_VarType & v_readhelp = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_readhelp ) );
	const MODI_VarType & v_normalflags = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_normalflag ) );
	const MODI_VarType & v_chang = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_chang_count ) );
	const MODI_VarType & v_heng = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_heng_count ) );
	const MODI_VarType & v_keyboard = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_keyboard_count ) );
	const MODI_VarType & v_toupiao = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_vote_count ) );
	const MODI_VarType & v_totalot = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_total_online_time ) );
	const MODI_VarType & v_todayot = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_today_online_time ) );
	const MODI_VarType & v_consume_rmb = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_consume_rmb ) );
	const MODI_VarType & v_highest_score = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_highest_score ) );
	const MODI_VarType & v_highest_score_musicid = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_highest_score_musicid ) );
	const MODI_VarType & v_highest_pre = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_highest_precision ) );
	const MODI_VarType & v_highest_pre_musicid = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_highest_precision_musicid ) );
	const MODI_VarType & v_blockcount = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_blockcount));
	const MODI_VarType & v_chenghaoid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_chenghao));
	const MODI_VarType & v_otherdata = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_otherdata));
	const MODI_VarType  & v_vip = pRecord->GetValue("vip");

	const MODI_VarType & v_key_highest_score = pRecord->GetValue( "key_highest_score");
	const MODI_VarType & v_key_highest_score_musicid = pRecord->GetValue("key_highest_score_musicid");
	const MODI_VarType  &v_key_highest_precise = pRecord->GetValue("key_highest_precision");
	const MODI_VarType &v_key_highest_precise_musicid =  pRecord->GetValue("key_highest_precision_musicid");

	MODI_CHARID nCharid = v_charid.ToUInt();
	defAccountID nAccid = v_accountid.ToUInt();
	const char * szHoneyData = v_honeyinfo.ToStr();

	if( nCharid != this->GetCharGuid() ||
		nAccid != this->GetAccountID() ||
		!szHoneyData)
	{
		MODI_ASSERT(0);
		return false;
	}
	

	unsigned int nOutLen = 0;
	if( DBStr2Binary( szHoneyData , 200 , (char*)(&pRoleinfo->m_detailInfo.m_SocialRelData.m_HoneyData) , 
				sizeof(MODI_HoneyData) , nOutLen ) == false )
	{
		MODI_ASSERT(0);
		Global::logger->error("[%s] DBCharBaseInfo<charid=%u> ParseLoadResult faild. function :DBStr2Binary == false" , 
				GAMEDB_OPT ,
				GetCharGuid());
		return false;
	}

	/// 把账号名和拉黑提出来

	// 基本信息
	safe_strncpy( pRoleinfo->m_baseInfo.m_cstrRoleName  , 
			v_rolename.ToStr() , 
			sizeof(pRoleinfo->m_baseInfo.m_cstrRoleName) );
	pRoleinfo->m_baseInfo.m_ucLevel = v_level.ToUInt();
	pRoleinfo->m_baseInfo.m_ucSex = v_sex.ToUInt();

	// 普通信息
	pRoleinfo->m_normalInfo.m_nExp = v_exp.ToUInt();
	pRoleinfo->m_normalInfo.m_nDefavatarIdx = v_defavatar.ToUShort();
	/// 用户称号
	pRoleinfo->m_normalInfo.m_wdChenghao = v_chenghaoid.ToUShort();
	/// vip信息
	pRoleinfo->m_baseInfo.m_bVip = v_vip.ToUChar();
/* all users are now VIP - andryan */
//	pRoleinfo->m_baseInfo.m_bVip = 1;
/* end all users are now VIP - andryan */

	// 详细信息
	pRoleinfo->m_detailInfo.m_byAge = v_age.ToUInt();
	pRoleinfo->m_detailInfo.m_byBlood = v_blood.ToUInt();
	pRoleinfo->m_detailInfo.m_nCity = v_city.ToUInt();
	pRoleinfo->m_detailInfo.m_nClan = v_clan.ToUInt();
	pRoleinfo->m_detailInfo.m_nGroup = v_group.ToUInt();
	pRoleinfo->m_detailInfo.m_nQQ = v_qq.ToUInt();
	pRoleinfo->m_detailInfo.m_SocialRelData.m_nRenqi = v_renqi.ToUInt();
	pRoleinfo->m_detailInfo.m_SocialRelData.m_nFensiBoy = v_fensib.ToUInt();
	pRoleinfo->m_detailInfo.m_SocialRelData.m_nFensiGril = v_fensig.ToUInt();
	pRoleinfo->m_detailInfo.m_nWin = v_win.ToUInt();
	pRoleinfo->m_detailInfo.m_nLoss = v_loss.ToUInt();
	pRoleinfo->m_detailInfo.m_nTie = v_tie.ToUInt();
	pRoleinfo->m_detailInfo.m_nVoteCount = v_vote.ToUInt();
	pRoleinfo->m_detailInfo.m_byReadHelp = v_readhelp.ToUChar();
	pRoleinfo->m_detailInfo.m_Chang = v_chang.ToUInt();
	pRoleinfo->m_detailInfo.m_Heng = v_heng.ToUInt();
	pRoleinfo->m_detailInfo.m_KeyBoard = v_keyboard.ToUInt();
	pRoleinfo->m_detailInfo.m_Toupiao = v_toupiao.ToUInt();

	std::string strBirthday = v_birthday.ToStr();
	pRoleinfo->m_detailInfo.m_nBirthDay = TimeHelpFuns::EncodeDateFromString( strBirthday.c_str() );

	safe_strncpy( pRoleinfo->m_detailInfo.m_szPersonalSign , 
			v_sign.ToStr() , 
			sizeof(pRoleinfo->m_detailInfo.m_szPersonalSign) );

	// 游戏货币和人民币
	pRoleinfo->m_detailInfo.m_nMoney = v_money.ToUInt();
	pRoleinfo->m_detailInfo.m_nRMBMoney = v_moneyRMB.ToUInt();
	pRoleinfo->m_detailInfo.highest_socre = v_highest_score.ToUInt();
	pRoleinfo->m_detailInfo.highest_score_musicid = v_highest_score_musicid.ToUInt();
	pRoleinfo->m_detailInfo.highest_precision = v_highest_pre.ToFloat();
	pRoleinfo->m_detailInfo.highest_precision_musicid = v_highest_pre_musicid.ToUInt();
	
	pRoleinfo->m_detailInfo.key_highest_precision = v_key_highest_precise.ToFloat();
	pRoleinfo->m_detailInfo.key_highest_precision_musicid = v_key_highest_precise_musicid.ToUInt();
	pRoleinfo->m_detailInfo.key_highest_socre = v_key_highest_score.ToUInt();
	pRoleinfo->m_detailInfo.key_highest_score_musicid = v_key_highest_score_musicid.ToUInt();

	// 额外信息(客户端无须知道)
	safe_strncpy( pRoleExtraData->m_szLastLoginIP , v_lastloginip.ToStr() , sizeof(pRoleExtraData->m_szLastLoginIP) );
	strncpy(msg->m_cstrAccName, v_accname.ToStr(), sizeof(msg->m_cstrAccName) - 1);
	pRoleExtraData->m_wdBlockCount = (WORD)v_blockcount;

	// 
	const char * szTempDataTime = v_lastlogintime.ToStr();
	time_t * ptmp1 = (time_t *)(&pRoleExtraData->m_nLastLoginTime);
	unsigned long long * pTempOut = (unsigned long long *)(ptmp1);
	if( !safe_strtoull( szTempDataTime , pTempOut ) )
	{
		MODI_ASSERT(0);
		return false;
	}

	szTempDataTime = v_lastidoltime.ToStr();
	ptmp1 = (time_t *)(&pRoleinfo->m_detailInfo.m_SocialRelData.m_nLastAddIdolTime);
	pTempOut = (unsigned long long *)(ptmp1);
	if( !safe_strtoull( szTempDataTime , pTempOut ) )
	{
		MODI_ASSERT(0);
		return false;
	}


	szTempDataTime = v_registertime.ToStr();
	ptmp1 = (time_t *)(&pRoleExtraData->m_nRegisterTime);
	pTempOut = (unsigned long long *)(ptmp1);
	if( !safe_strtoull( szTempDataTime , pTempOut ) )
	{
		MODI_ASSERT(0);
		return false;
	}

	pRoleExtraData->m_iGMLevel = v_gmlevel.ToInt();


	unsigned int nNormalFlagsOutLen = 0;
	memset( pRoleExtraData->m_szNormalFlags , 0 , sizeof(pRoleExtraData->m_szNormalFlags) );
	if( !DBStr2Binary( v_normalflags.ToStr() ,  strlen(v_normalflags.ToStr()),
			pRoleExtraData->m_szNormalFlags , sizeof(pRoleExtraData->m_szNormalFlags) ,
			nNormalFlagsOutLen  ) )
	{
		Global::logger->warn("[%s] dbstring to bin falid. normalflags ... " , SVR_TEST );
		MODI_ASSERT(0);
		return false;
	}
	pRoleExtraData->m_nTotalOnlineTime = v_totalot.ToUInt();
	pRoleExtraData->m_nTodayOnlineTime = v_todayot.ToUInt();
	pRoleExtraData->consume_rmb = v_consume_rmb.ToUInt();

	if(v_otherdata.Size())
	{
		memcpy(msg->m_Data, (const char *)(v_otherdata), v_otherdata.Size());
		msg->m_dwSize = v_otherdata.Size();
#if 0
		/// 直接对比,不行不加载了
		MODI_Encrypt m_stEncrypt;
		m_stEncrypt.SetEncryptType(MODI_Encrypt::ENCRYPT_TYPE_RC5);
		char other_out[v_otherdata.Size()];
		memset(other_out, 0, sizeof(other_out));
		if(v_otherdata.Size())
		{
			memcpy(other_out, (const char *)(v_otherdata), v_otherdata.Size());
		}
		m_stEncrypt.Decode((unsigned char *)other_out, sizeof(other_out));
		std::string out = other_out;
		MODI_SplitString split;
		split.Init(other_out, ",");
		std::string lin_money = split["moneyrmb"];
		std::string lin_name = split["accname"];

		DWORD m_m = atoi(lin_money.c_str());
		DWORD have_money = pRoleinfo->m_detailInfo.m_nRMBMoney;
		std::string have_name = v_accname.ToStr();
		if((m_m != have_money) || (lin_name != have_name))
		{
			Global::logger->fatal("[character_data_error] characters data error1 <name=%s>", have_name.c_str());
			MODI_ASSERT(0);
			return false;
		}
#endif		
	}
	else
	{
		msg->m_dwSize = 0;
#if 0 
		DWORD have_money = pRoleinfo->m_detailInfo.m_nRMBMoney;
		std::string have_name = v_accname.ToStr();
		if(have_money != 0)
		{
			Global::logger->fatal("[character_data_error] characters data error2 <name=%s>", have_name.c_str());
			return false;
		}
#endif		
	}

	
	return true;
}



void	MODI_DBCharBaseinfo::SetCharGuid(MODI_CHARID guid)
{
	m_nCharID = guid;
}

MODI_CHARID		MODI_DBCharBaseinfo::GetCharGuid()
{
	return m_nCharID;
}

bool 	MODI_DBCharBaseinfo::SaveLastLogin(time_t nTime,const char * szIP)
{
	const char * szSql = MODI_SqlStatement::CreateStatement_UpdateLastLogin( nTime , szIP , GetCharGuid() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );
	if( MODI_IDBBase::Save( 0 ) )
		return true;
	return false;
}
