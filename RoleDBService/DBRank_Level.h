#ifndef DB_RANK_LEVEL_H_
#define DB_RANK_LEVEL_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

class MODI_DBLevelRank : public MODI_IDBBase
{

public:

	explicit MODI_DBLevelRank( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBLevelRank();

	virtual bool	Load(); 

	virtual bool	Save(const void* pSource) { return false; }

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	ParseLoadResult(void* pResult);
	
	bool 	LoadSelfRank();

	bool	ParseLoadResult_SelfRank(void* pResult);

};


#endif
 
