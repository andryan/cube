#ifndef DB_RANK_HIGHESTSCORE_H_
#define DB_RANK_HIGHESTSCORE_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

class MODI_DBHighestScore : public MODI_IDBBase
{

public:

	explicit MODI_DBHighestScore( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBHighestScore();

	virtual bool	Load(); 

	virtual bool	Save(const void* pSource) { return false; }

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	ParseLoadResult(void* pResult);
	
	bool 	LoadSelfRank();

	bool	ParseLoadResult_SelfRank(void* pResult);

};


#endif
 
