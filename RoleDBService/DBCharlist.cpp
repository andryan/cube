#include "DBCharlist.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "s2rdb_cmd.h"
#include "Global.h"
#include "DefaultAvatarCreator.h"
//#include "DBItemlist.h"
#include <mysqld_error.h>
//#include "DBItemSerial.h"
#include "IAppFrame.h"
#include "Base/HelpFuns.h"

MODI_DBCharlist::MODI_DBCharlist(MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler ),
	m_nAccount(INVAILD_ACCOUNT_ID),
	//m_dbAvatarcid( pDBHandler ),
	m_IsHasDupChar( false )
{
	m_bHasChar=false;
	memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
}

MODI_DBCharlist::~MODI_DBCharlist()
{

}

bool	MODI_DBCharlist::Load()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_Sel_Charslist( this->GetAccountID() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );

	if( !MODI_IDBBase::Load() )
		return false;

	if( !m_pResults )
		return true;

	MODI_Record * pRecord = m_pResults->GetRecord( 0 );
	if( !pRecord )
	{
		return true;
	}

	m_bHasChar=true;

	return true;
}

bool	MODI_DBCharlist::AddNew(const void * pSource)
{
	MODI_AddCharacter * pAddChar = (MODI_AddCharacter *)(pSource);

	if( !m_pDBClient )
		return false;

	// 开始事物
	m_pDBClient->BeginTransaction();

	try
	{
		const char * szSql = MODI_SqlStatement::CreateStatement_Sel_Charslist( this->GetAccountID() );
		if( !szSql )
			throw "create sql statement faild. <select charlist>";
		this->SetSqlStatement( szSql );

		if( !MODI_IDBBase::Load() )
			throw "load charlist faild.";

		MODI_Record * pRecord = m_pResults->GetRecord( 0 );
		if( pRecord )
			throw "already has character.";

		// 查询下一个guid
		const char * szFeild = MODI_SqlStatement::GetFieldInCharsTable(enCharTF_guid);
		szSql =  MODI_SqlStatement::CreateStatement_Max( enTable_Character , szFeild );
		if( !szSql )
		{
			Global::logger->error("[%s] can't add accid<%u>'s character to db , create sql statement faild. <select max>." ,
					ROLEDATA_OPT ,
					this->GetAccountID());

			throw "create sql statement faild. <select max>";
		}

		this->SetSqlStatement( szSql );
		
		if( !MODI_IDBBase::Load() )
		{
			Global::logger->error("[%s] can't add accid<%u>'s character to db , cselect max(guid) faild." ,
					ROLEDATA_OPT ,
					this->GetAccountID());

			throw "select max(guid) faild.";
		}

		pRecord = m_pResults->GetRecord( 0 );
		if( !pRecord )
		{
			Global::logger->fatal("[%s] can't add accid<%u>'s character to db , can't get record." ,
					ROLEDATA_OPT ,
					this->GetAccountID());

			throw "can't get record.";
		}

		const char * szField = "MAX(GUID)";
		const MODI_VarType & v_charid = pRecord->GetValue(szField);
		DWORD nLastCharID = v_charid.ToUInt();
		MODI_CHARID nCharid = nLastCharID + 1;

		// 创建角色
		szSql =  MODI_SqlStatement::CreateStatement_Ins_CharacterEx( this->GetAccountID() ,  nCharid , this->GetAccount(), pAddChar->m_pCreateInfo );
		MODI_ASSERT( szSql );
		if( !szSql )
		{
			Global::logger->error("[%s] can't add accid<%u>'s character to db , create sql statement faild. <insert character>." ,
					ROLEDATA_OPT ,
					this->GetAccountID());

			throw "create sql statement faild. <insert character>";
		}
		

		this->SetSqlStatement( szSql );
		if( !MODI_IDBBase::AddNew( pSource ) )
		{
			// 是否重复字段
			if( this->GetLastError() == ER_DUP_ENTRY )
			{
				m_IsHasDupChar = true;
			}
			else
			{
				Global::logger->error("[%s] can't add accid<%u>'s character to db , insert into character faild." ,
						ROLEDATA_OPT ,
						this->GetAccountID());
			}

			throw "insert into character faild.";
		}
		m_nCharID = nCharid;

		// 给物品
#if 0
		for( WORD n = 0; n < pAddChar->m_itemAdd.m_nItemAddCount; n++ )
		{
			if( pAddChar->m_itemAdd.m_pItemInfoAdd[n].m_nConfigID == INVAILD_CONFIGID )
			{
				MODI_ASSERT(0);
				throw  "Add item to new character , but there are invaild item config.";
			}
		}

		const char * szResetSqlTemplate = "UPDATE iteminfos SET `isvalid` = 0 WHERE `charguid` = %u ";
		char szResetBuf[4096];
		memset(  szResetBuf, 0 , sizeof(szResetBuf) );
		SNPRINTF( szResetBuf , sizeof(szResetBuf) , szResetSqlTemplate , GetCharID() );
		SetSqlStatement( szResetBuf );
		if( !MODI_IDBBase::Save(0) )
		{
			MODI_ASSERT(0);
			throw "reset character's item list faild";
		}

		MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
		BYTE byWorldID = pApp->GetWorldID();
		BYTE byServerID = pApp->GetServerID();
		if( byServerID != 255 )
			throw "error server id .";
		MODI_ASSERT( byServerID == 255 );

		const char * szMaxDBItemSerialTemplate = "SELECT MAX(serial) FROM `iteminfos` WHERE world = %u and server =%u";
		char szMaxDBItemSerial[4096];
		memset( szMaxDBItemSerial , 0 , sizeof(szMaxDBItemSerial) );
		SNPRINTF( szMaxDBItemSerial , sizeof(szMaxDBItemSerial) , szMaxDBItemSerialTemplate , 
				byWorldID,
				byServerID );

		SetSqlStatement( szMaxDBItemSerial );
		if( !MODI_IDBBase::Load() )
		{
			MODI_ASSERT(0);
			throw "extuce select max('serial') faild.";
		}
		pRecord = m_pResults->GetRecord( 0 );
		if( !pRecord )
		{
			throw "can't get record<select max('serial')>.";
		}
		const char * szMaxSerialField = "MAX(serial)";
		const MODI_VarType & v_maxserial = pRecord->GetValue(szMaxSerialField);
		DWORD nSerialBegin = v_maxserial.ToUInt() + 1;
//		DWORD nSerialEnd = nSerialBegin + pAddChar->m_itemAdd.m_nItemAddCount;

		MODI_DBItemInfo dbiteminfo[32];
		MODI_SaveItem 	saveitem;
		saveitem.m_bTrans =  true;
		saveitem.m_nItemSaveCount = pAddChar->m_itemAdd.m_nItemAddCount;
		saveitem.m_pItemInfoSave = dbiteminfo;


		

		// 给物品
		for( WORD n = 0; n < pAddChar->m_itemAdd.m_nItemAddCount; n++ )
		{
			dbiteminfo[n].m_nConfigID = pAddChar->m_itemAdd.m_pItemInfoAdd[n].m_nConfigID;
			dbiteminfo[n].m_byValid = 1;
			dbiteminfo[n].m_byWorldID = byWorldID;
			dbiteminfo[n].m_byServerID = byServerID;
			dbiteminfo[n].m_nBagType = pAddChar->m_itemAdd.m_pItemInfoAdd[n].m_nBagType;
			dbiteminfo[n].m_nCount = 1;
			dbiteminfo[n].m_nItemPos = pAddChar->m_itemAdd.m_pItemInfoAdd[n].m_nItemPos;
			dbiteminfo[n].m_ExpireTime = enForever;
			dbiteminfo[n].m_nItemSerial = nSerialBegin + n;
		}

		MODI_DBItemlist 	dbitemlist(m_pDBClient);
		dbitemlist.SetCharGuid( GetCharID() );
		if( !dbitemlist.Save( &saveitem ) )
		{
			MODI_ASSERT(0);
			throw "save default item faild.";
		}
#endif		
	}
	catch( const char * szErr )
	{
		// 回滚
		m_pDBClient->RollbackTransaction();

		if( !this->IsHasDupCharacter() )
		{
			Global::logger->info("[%s] Add new character faild <%s>. rollback transcation ." , 
				ROLEDATA_OPT ,
				szErr );
			MODI_IDBBase::DumpLastError();
		}

		return false;
	}

	// 提交， 结束事务
	m_pDBClient->CommitTransaction();

	return true;
}

bool	MODI_DBCharlist::ParseLoadResult(void* pResult)
{
	if( !m_pResults )
		return false;

	MODI_CharInfo * pCharinfo = (MODI_CharInfo *)(pResult);	
	MODI_Record * pRecord = m_pResults->GetRecord( 0 );
	if( !pRecord )
		return false;

	const MODI_VarType & v_charid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_guid));
	//	m_dbAvatarcid.SetCharGuid( v_charid.ToUInt() );
	//	if( !m_dbAvatarcid.Load() )
	//		return false;

	const MODI_VarType & v_rolename = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_name));
	const MODI_VarType & v_level = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_level));
	const MODI_VarType & v_sex = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_sex));
	const MODI_VarType & v_defavatar = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_defavatar ) );
	const MODI_VarType & v_readhelp = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_readhelp ) );
	const MODI_VarType & v_gmlevel = pRecord->GetValue( MODI_SqlStatement::GetFieldInCharsTable( enCharTF_gmlevel ) );


	// copy 
	pCharinfo->m_nCharID = v_charid.ToUInt();
	pCharinfo->m_byLevel = v_level.ToUChar();
	pCharinfo->m_bySex = v_sex.ToUChar();
	pCharinfo->m_nDefaultAvatarIdx = v_defavatar.ToUShort();
	safe_strncpy( pCharinfo->m_szRoleName , v_rolename.ToStr() , sizeof(pCharinfo->m_szRoleName) );
	pCharinfo->m_byReadHelp = v_readhelp.ToUChar();

	int gm_level = v_gmlevel.ToInt();
	if(gm_level == 200)
	{
#ifdef _HRX_DEBUG		
		Global::logger->info("[kick_acc] kick acc by gm <name=%s>", pCharinfo->m_szRoleName);
#endif		
		return false;
	}

	// avatar configid 
	//	if( !m_dbAvatarcid.ParseLoadResult( &(pCharinfo->m_AvatarInfo) ) )
	//	{
	//		return false;
	//	}

	return true;
}

void 			MODI_DBCharlist::SetAccountID( defAccountID accid )
{
	m_nAccount = accid;

}

defAccountID 	MODI_DBCharlist::GetAccountID() const
{
	return m_nAccount;
}


bool 	MODI_DBCharlist::QueryHasChar( const char * szName )
{
	const char * szSql = MODI_SqlStatement::CreateStatement_HasChar( szName );
	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Load() )
		return false;
	if( !m_pResults )
		return true;
	MODI_Record * pRecord = m_pResults->GetRecord( 0 );
	if( pRecord )
		return true;
	return false;
}
