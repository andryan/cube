/** 
 * @file DBCDKey.h
 * @brief 
 * @author Tang Teng
 * @version v0.1
 * @date 2010-11-24
 */
#ifndef MODI_DBCDKEY_H_
#define MODI_DBCDKEY_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"
#include "Base/gamestructdef.h"
#include "Base/HelpFuns.h"

class MODI_DBCDKey : public MODI_IDBBase
{
public:

	explicit MODI_DBCDKey( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBCDKey();

	virtual bool	Load(); 
	virtual bool	AddNew(const void * pSource) { return false; }
	virtual bool	Delete() { return false; }
	virtual bool	Save(const void* pSource); 

	virtual bool	ParseLoadResult(void* pResult);

	void 	SetKey( const char * szKey )
	{
		safe_strncpy( m_szKey , szKey , sizeof(m_szKey) );
	}

	const char * GetKey() const
	{
		return m_szKey;
	}

	void 	SetCharID( MODI_CHARID charid ) { m_charid = charid;}

	bool 	IsExist() const { return m_bExist; }

private:

	char 		m_szKey[YUNYING_CDKEY_MAXSIZE + 1];
	MODI_CHARID m_charid;
	bool 		m_bExist;
};

#endif
