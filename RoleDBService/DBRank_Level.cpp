#include "DBRank_Level.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "s2rdb_cmd.h"
#include "Global.h"
#include "HelpFuns.h"


MODI_DBLevelRank::MODI_DBLevelRank( MODI_DBClient * pDBHandler ):
	MODI_IDBBase(pDBHandler)
{

}

MODI_DBLevelRank::~MODI_DBLevelRank()
{

}

bool	MODI_DBLevelRank::Load()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_RankLevel();
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );
	if( MODI_IDBBase::Load() )
		return true;
	return false;
}

bool 	MODI_DBLevelRank::LoadSelfRank()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_Self_RankLevel();
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );
	if( MODI_IDBBase::Load() )
		return true;
	return false;
}

bool	MODI_DBLevelRank::ParseLoadResult_SelfRank(void* pResult)
{
	if( !m_pResults )
		return false;

	MODI_DBSelfRank * self_rank = (MODI_DBSelfRank *)(pResult);
	size_t nRecordSize = m_pResults->Size();
	self_rank->size = 0;
	for( size_t n = 0; n < nRecordSize; n++ )
	{
		MODI_Record * pRecord = m_pResults->GetRecord( n );
		if( pRecord )
		{
			const MODI_VarType & v_charid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_guid));
			self_rank->ranks[self_rank->size] = v_charid.ToUInt();
			self_rank->size += 1;
		}
		else
		{
			MODI_ASSERT(0);
			Global::logger->warn("[%s] self rank<type=level>'s can't get record by index=%u" , 
					SVR_TEST ,
					n );
		}
	}

	return true;
}


bool	MODI_DBLevelRank::ParseLoadResult(void* pResult)
{
	if( !m_pResults )
		return false;

	MODI_DBRank_Level_List * pList = (MODI_DBRank_Level_List *)(pResult);
	size_t nRecordSize = m_pResults->Size();
	if( nRecordSize > RANK_LEVEL_MAX_TOP )
	{
		MODI_ASSERT(0);
		nRecordSize = RANK_LEVEL_MAX_TOP;
		Global::logger->warn("[%s] rank<type=level>'s max top too large<size=%u>. " , SVR_TEST ,
				m_pResults->Size() );
	}

	pList->size = 0;
	for( size_t n = 0; n < nRecordSize; n++ )
	{
		MODI_Record * pRecord = m_pResults->GetRecord( n );
		if( pRecord )
		{
			const MODI_VarType & v_charid = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_guid));
			const MODI_VarType & v_level = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_level));
			const MODI_VarType & v_rolename = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_name));
			const MODI_VarType & v_title = pRecord->GetValue(MODI_SqlStatement::GetFieldInCharsTable(enCharTF_chenghao));

			MODI_DBRank_Level & element = pList->ranks[n];
			element.charid = v_charid.ToUInt();
			element.level = v_level.ToUInt();
			element.title = v_title.ToUInt();
			safe_strncpy( element.role_name  , v_rolename.ToStr() , sizeof(element.role_name) );
			pList->size += 1;
		}
		else
		{
			MODI_ASSERT(0);
			Global::logger->warn("[%s] rank<type=level>'s can't get record by index=%u" , 
					SVR_TEST ,
					n );
		}
	}

	return true;
}

