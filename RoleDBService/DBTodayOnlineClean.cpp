#include "DBTodayOnlineClean.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "Global.h"
#include "gamestructdef.h"
#include "Base/HelpFuns.h"

MODI_DBTodayOnlineTimeClean::MODI_DBTodayOnlineTimeClean( MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler )
{

}

MODI_DBTodayOnlineTimeClean::~MODI_DBTodayOnlineTimeClean()
{

}

bool MODI_DBTodayOnlineTimeClean::Excute()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_ResetTodayOnlineTIme();
	if( !szSql )
	{
		MODI_ASSERT(0);
		return false;
	}

	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Save( 0 ) )
	{
		Global::logger->error("[%s] reset today online time on db faild." , 
				ROLEDATA_OPT );
		return false;
	}

	return true;
}
