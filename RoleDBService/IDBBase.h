/** 
 * @file IDBBase.h
 * @brief db操作的基础类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */
#ifndef IDATABASE_H_H_
#define IDATABASE_H_H_

#include "BaseOS.h"
#include <string>
#include "DBClient.h"
#include "DBStruct.h"
#include "AssertEx.h"

class MODI_IDBBase
{
protected:

	std::string m_strSql;
	MODI_RecordContainer * m_pResults;
	unsigned int 	m_nLastError;
	std::string 	m_strLastErrorDes;
	MODI_DBClient * m_pDBClient; 

	void 			SetSqlStatement( const char * szSql );
	const char 	* 	GetSqlStatement() const;
	void 			ResetSqlStatement();

public:
	

	explicit MODI_IDBBase( MODI_DBClient * pDBHandler );
	virtual ~MODI_IDBBase();

	virtual bool	Load();
	virtual bool	AddNew(const void * pSource);
	virtual bool	Delete();
	virtual bool	Save(const void* pSource);

	unsigned int 	GetLastError() const { return m_nLastError; }
	const char * 	GetLastErrorDes() const { return m_strLastErrorDes.c_str(); }

	virtual bool	ParseLoadResult(void* pResult)	{ return true ; }
	virtual bool	ParseAddNewResult(void* pResult){ return true ; }
	virtual bool	ParseDelResult(void* pResult)	{ return true ; }
	virtual bool	ParseSaveResult(void* pResult){ return true ; }

	virtual void 	DumpLastError();

	void 			CleanUpResult();
	
};

#ifdef _DEBUG

class 	MODI_DBOperatorTimeLog
{
	public:

	MODI_DBOperatorTimeLog( const char * szOptName ):m_strOptName(szOptName),m_Begin(0),m_End(0)
	{

	}

	void Begin()
	{
		m_time.GetNow();
		m_Begin = m_time.GetMSec();
	}

	void End()
	{
		m_time.GetNow();
		m_End = m_time.GetMSec();
		MODI_ASSERT( m_End >= m_Begin );
		QWORD nTime = m_End - m_Begin;

		Global::logger->debug("[%s] do <%s> use time = %llu." ,SVR_TEST ,
				m_strOptName.c_str() ,
				nTime );
	}


	private:

	MODI_RTime m_time;
	std::string m_strOptName;
	QWORD 	m_Begin;
	QWORD 	m_End;
};

#else 

class 	MODI_DBOperatorTimeLog
{
	public:

	MODI_DBOperatorTimeLog( const char * szOptName )
	{

	}

	void Begin()
	{
	}

	void End()
	{
	}

};
#endif

#endif
