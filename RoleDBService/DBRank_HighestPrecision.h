#ifndef DB_RANK_HIGHESTPRECISION_H_
#define DB_RANK_HIGHESTPRECISION_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

class MODI_DBHighestPrecision : public MODI_IDBBase
{

public:

	explicit MODI_DBHighestPrecision( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBHighestPrecision();

	virtual bool	Load(); 

	virtual bool	Save(const void* pSource) { return false; }

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	ParseLoadResult(void* pResult);
	
	bool 	LoadSelfRank();

	bool	ParseLoadResult_SelfRank(void* pResult);

};


#endif
 
