#ifndef RDBSVR_LOGIC_THREAD_H_
#define RDBSVR_LOGIC_THREAD_H_

#include "Type.h"
#include "Timer.h"
#include "Thread.h"
#include "SingleObject.h"
#include "RecurisveMutex.h"


class MODI_RoleDBLogic: public MODI_Thread , public CSingleObject<MODI_RoleDBLogic>
{

public:

    /// 主循环
    virtual void Run();

	void Final();

    /// 构造
    MODI_RoleDBLogic();

    /// 析构
    virtual ~MODI_RoleDBLogic();

	static pthread_t ms_tid; 
private:

	

	unsigned long 	m_nTimer;
	MODI_RTime 		m_stRTime;
	MODI_Timer 		m_timerItemSerialSave;
	MODI_Timer 		m_timerLogicThread;
};

#endif
