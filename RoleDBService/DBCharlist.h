/** 
 * @file DBCharlist.h
 * @brief DB中关于角色列表的操作
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-19
 */
#ifndef DB_CHARLIST_H_
#define DB_CHARLIST_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"
//#include "DBAvatarConfigID.h"

/** 
 * @brief DB中关于角色列表的操作
 */
class MODI_DBCharlist : public MODI_IDBBase
{
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	defAccountID 			m_nAccount;
	//MODI_DBAvatarConfigID 	m_dbAvatarcid;
	bool 					m_IsHasDupChar;	
	bool 					m_bHasChar;
	MODI_CHARID 			m_nCharID;
	DWORD  					m_nMaxItemSerial;
	bool 					m_bIsHasChar;

public:

	explicit MODI_DBCharlist( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBCharlist();

	/** 
	 * @brief 加载某个指定帐号的角色列表
	 * 
	 * @return 	SQL 语句查询成功，返回TRUE
	 */
	virtual bool	Load(); 

	/** 
	 * @brief 添加一个角色数据到角色列表
	 * 
	 * @param pSource 需要添加的角色
	 * 
	 * @return 	SQL 语句查询成功，返回TRUE
	 */
	virtual bool	AddNew(const void * pSource);

	/** 
	 * @brief 从角色列表中删除一个角色，目前未实现
	 * 
	 * @return 	永远返回FALSE
	 */
	virtual bool	Delete() { return false; }

	virtual bool	Save(const void* pSource) { return false; }

	/** 
	 * @brief 解析加载角色列表的结果
	 * 
	 * @param pResult 输出结果
	 * 
	 * @return 	成功返回TRUE
	 */
	virtual bool	ParseLoadResult(void* pResult);


	virtual bool	ParseSaveResult(void* pResult) { return false; }

	bool 	QueryHasChar( const char * szName );

public:

	void SetAccount(const char * account_name)
	{
		strncpy(m_cstrAccName, account_name, sizeof(m_cstrAccName) - 1);
	}
	const char * GetAccount()
	{
		return m_cstrAccName;
	}
	void 			SetAccountID( defAccountID accid );
	defAccountID 	GetAccountID() const;

	void 			SetMaxItemSerial(DWORD nMax)
	{
		m_nMaxItemSerial = nMax;
	}

	DWORD 			GetMaxItemSerial() const
	{
		return m_nMaxItemSerial;
	}

	MODI_CHARID 	GetCharID() const 
	{
		return m_nCharID;
	}

	bool 			IsHasDupCharacter() const
	{
		return  m_IsHasDupChar;
	}

	bool 	IsHasCharacter() const
	{
		return m_bHasChar;
	}
};


#endif
