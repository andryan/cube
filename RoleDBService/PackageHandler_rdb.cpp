#include "PackageHandler_rdb.h"
#include "SqlStatement.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "s2rdb_cmd.h"
#include "DBCharlist.h"
#include "DBCharFullData.h"
#include "gamestructdef.h"
#include "RoleDBAPP.h"
#include "DBRelation.h"
//#include "DBCharDetailInfo.h"
#include "GameHelpFun.h"
#include "DBMail.h"
//#include "DBShopHistroy.h"
#include "DBTodayOnlineClean.h"
#include "DBCDKey.h"
#include "DBRank_Level.h"
#include "DBRank_Renqi.h"
#include "DBRank_Consume.h"
#include "DBRank_HighestScore.h"
#include "DBRank_HighestPrecision.h"
#include "GlobalDB.h"


MODI_PackageHandler_rdb::MODI_PackageHandler_rdb()
{

}

MODI_PackageHandler_rdb::~MODI_PackageHandler_rdb()
{

}

bool MODI_PackageHandler_rdb::FillPackageHandlerTable()
{
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_Charlist::ms_SubCmd ,
			&MODI_PackageHandler_rdb::LoadCharslist_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_LoadCharData::ms_SubCmd,
			&MODI_PackageHandler_rdb::LoadRoleData_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveCharData::ms_SubCmd , 
			&MODI_PackageHandler_rdb::SaveRoleData_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_CreateChar::ms_SubCmd, 
			&MODI_PackageHandler_rdb::CreateChar_Handler );
#if 0	
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveItem::ms_SubCmd, 
			&MODI_PackageHandler_rdb::SaveItem_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ItemSerialTableOpt::ms_SubCmd, 
			&MODI_PackageHandler_rdb::ItemSerialTableOpt_Handler);
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ItemSerial::ms_SubCmd, 
			&MODI_PackageHandler_rdb::LoadItemSerial_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveSerial::ms_SubCmd, 
			&MODI_PackageHandler_rdb::SaveItemSerial_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_CreateItemHistroy::ms_SubCmd ,
			&MODI_PackageHandler_rdb::ItemCreateHistroy_Handler );
#endif	
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveRelation::ms_SubCmd ,
			&MODI_PackageHandler_rdb::SaveRelations_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveSocialData::ms_SubCmd ,
			&MODI_PackageHandler_rdb::SaveSocial_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_RoleDetailInfo::ms_SubCmd ,
			&MODI_PackageHandler_rdb::RoleDetailInfo_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_LoadMaillist::ms_SubCmd,
			&MODI_PackageHandler_rdb::LoadMaillist_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveMail::ms_SubCmd ,
			&MODI_PackageHandler_rdb::SaveMail_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_UpdateLastLogin::ms_SubCmd ,
			&MODI_PackageHandler_rdb::UpdateLastLogin_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ResetTodayOnlineTime::ms_SubCmd,
			&ResetTodayOnlineTime_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ExchangeByYunyingKey::ms_SubCmd,
			&ExchagneYYKey_Handler );
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveYunYingKey::ms_SubCmd,
			&SaveYYKey_Handler);
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_LoadRanks::ms_SubCmd,
			&LoadRanks_Handler);
	
	/// 0点清除数据库某些标志
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ClearZero::ms_SubCmd,
			&ClearZero_Handler);

	/// 音乐点击率统计
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Notify_MusicCount::ms_SubCmd,
			&MusicCount_Handler);

	/// 玩家变量操作
	//	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_ChangeUserVar_Cmd::ms_SubCmd,
	//			&ChangeUserVar_Handler);

	/// 加载用户变量
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Request_LoadUserVar_Cmd::ms_SubCmd, &LoadUserVar_Handler);

	/// 请求加载zone_user_var的变量
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Request_ZoneUserVar_Cmd::ms_SubCmd, &ZoneUserVar_Handler);
	
	/// 请求执行sql语句
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Request_ExecSql_Cmd::ms_SubCmd, &RequestExecSql_Handler);

	/// 事务sql语句
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Request_ExecSql_Transaction_Cmd::ms_SubCmd, &RequestExecSqlTransaction_Handler);

	//// gameservice加载包裹信息
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Request_ItemInfo::ms_SubCmd, &RequestItemInfo_Handler);

	/// 减钱
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_DecMoney_Cmd::ms_SubCmd, &DecMoney_Handler);

	/// 购买物品
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_BuyItem_Cmd::ms_SubCmd, &BuyItem_Handler);

	/// 家族升级
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_FamilyLevel_Cmd::ms_SubCmd, &FamilyLevel_Handler);

	/// 获取网页购买道具
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_Req_WebItem::ms_SubCmd, &GetWebItem_Handler);

//	AddPackageHandler(MAINCMD_S2RDB, MODI_RDBS2S_SaveRelation::ms_SubCmd, &SaveRelation_Handler);
	
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_NewSingleMusic::ms_SubCmd, &NewMusic_Handler);
	AddPackageHandler(MAINCMD_S2RDB, MODI_S2RDB_FlushSingleMusicList::ms_SubCmd, &FlushMusicList_Handler);


	this->AddPackageHandler( MAINCMD_S2RDB , MODI_SendMailByCondition_Cmd::ms_SubCmd , &MODI_PackageHandler_rdb::SendMailByCondition_Handler );
	
	return true;
}



/** 
 * @brief 加载角色列表
 * 
 */
int MODI_PackageHandler_rdb::LoadCharslist_Handler( void * pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	const MODI_S2RDB_Request_Charlist * p_recv_cmd = (const MODI_S2RDB_Request_Charlist *)(pt_null_cmd);
	
	MODI_RDB2S_Notify_Charlist send_cmd;
	send_cmd.m_bySuccessful = 1;
	send_cmd.m_nRoleCount = 0;
	send_cmd.m_nAccountID = p_recv_cmd->m_nAccountID;
	send_cmd.m_nSessionID = p_recv_cmd->m_nSessionID;

	int ret_code = 0;
	if((ret_code = LoadCharList(p_recv_cmd->m_nAccountID, send_cmd.m_pData)) != -1)
	{
		send_cmd.m_nRoleCount = ret_code;
	}
	else
	{
		send_cmd.m_bySuccessful = 0;
	}

	MODI_DBProxyClient::GetInstancePtr()->SendPackage(&send_cmd, sizeof(send_cmd));

	return enPHandler_OK;
}

#if 0
int MODI_PackageHandler_rdb::LoadCharslist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size)
{

	const MODI_S2RDB_Request_Charlist * pReq = (const MODI_S2RDB_Request_Charlist *)(pt_null_cmd);
	Global::logger->debug("[%s] Call Function: LoadCharslist_Handler .<m_nAccountID=%u>" , 
			GAMEDB_OPT ,
			pReq->m_nAccountID);

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );


	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_RDB2S_Notify_Charlist 	* p_notify = (MODI_RDB2S_Notify_Charlist *)(szPackageBuf);
	AutoConstruct( p_notify );
	int iSendSize = sizeof(MODI_RDB2S_Notify_Charlist);
	p_notify->m_bySuccessful = 0;
	p_notify->m_nRoleCount = 0;
	p_notify->m_nAccountID = pReq->m_nAccountID;
	p_notify->m_nSessionID = pReq->m_nSessionID;

	MODI_DBCharlist dbcl( pDBClient );
	dbcl.SetAccountID( pReq->m_nAccountID );

	MODI_DBOperatorTimeLog 	debug_log("MODI_DBCharlist");
	debug_log.Begin();

	if( dbcl.Load() )
	{
		if( !dbcl.IsHasCharacter() )
		{
			p_notify->m_bySuccessful = 1;
			p_notify->m_nRoleCount = 0;
		}
		else if( dbcl.ParseLoadResult( p_notify->m_pData ) )
		{
			p_notify->m_bySuccessful = 1;
			p_notify->m_nRoleCount = 1;
			iSendSize += sizeof(MODI_CharInfo);
		}
		else 
		{
			Global::logger->debug("[%s] client<accid=%u> request rolelist , parse load result faild ." , 
					ROLEDATA_OPT , 
					pReq->m_nAccountID);
		}
	}
	debug_log.End();

	MODI_DBProxyClient::GetInstancePtr()->SendPackage( p_notify , iSendSize );

	return enPHandler_OK;
}
#endif


/** 
 * @brief 用户登录，加载用户数据
 * 
 *
 */
int MODI_PackageHandler_rdb::LoadRoleData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_LoadCharData * pReq = (const MODI_S2RDB_Request_LoadCharData *)(pt_null_cmd);

#ifdef _DEBUG	
	Global::logger->debug("[request_load_role_data] <accid=%u,guid=%u>", pReq->m_nAccountID ,pReq->m_nCharID);
#endif	

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBCharfullData dbcfd( pDBClient );
	dbcfd.SetCharGuid( pReq->m_nCharID );
	dbcfd.SetAccId( pReq->m_nAccountID );

	char cmd_buf[Skt::MAX_USERDATASIZE];
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_RDB2S_Notify_LoadCharData * msg = (MODI_RDB2S_Notify_LoadCharData *)cmd_buf;
	AutoConstruct(msg);

	msg->m_bySuccessful = 0;
	msg->m_nCharID = pReq->m_nCharID;
	msg->m_nAccountID = pReq->m_nAccountID;
	msg->m_nServerID = pReq->m_nServerID;
	msg->m_nSessionID = pReq->m_nSessionID;

	MODI_DBOperatorTimeLog 	debug_log("MODI_DBCharfullData");
	debug_log.Begin();

	if( dbcfd.Load() )
	{
		if( dbcfd.ParseLoadResult(msg) )
		{
			msg->m_bySuccessful = 1;
			Global::logger->debug("[%s] accid<%u> load char data was successful .: " , 
					ROLEDATA_OPT , 
					pReq->m_nAccountID );
		}
		else 
		{
			Global::logger->debug("[%s] accid<%u> load char data was faild .: " , 
					ROLEDATA_OPT , 
					pReq->m_nAccountID );
		}
	}
	else 
	{
		Global::logger->warn("[%s] accid<%u> load char data was faild . exe sql statement for loadchardata faild . " , 
				ROLEDATA_OPT , 
				pReq->m_nAccountID );
	}
	debug_log.End();

	// 同步结果
	MODI_DBProxyClient::GetInstancePtr()->SendPackage( msg , sizeof(MODI_RDB2S_Notify_LoadCharData) + msg->m_dwSize );
	

	///  加载社会关系
	//2011,10,31 把关系分开包
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_RDB2S_RelationLoad  * relation = (MODI_RDB2S_RelationLoad *) cmd_buf;
	AutoConstruct(relation);

	MODI_RecordContainer record_select;
	MODI_Record select_field;
	std::ostringstream  os;

	select_field.Put("relid");
	select_field.Put("charid");
	select_field.Put("relationguid");
	select_field.Put("relationname");
	select_field.Put("sex");
	select_field.Put("friendpoint");
	select_field.Put("reltype");
	
	os.str("");
	os<<"charid="<<pReq->m_nCharID<<" and isvalid=1";
	int ret_count=pDBClient->ExecSelect(record_select,MODI_DBProxyClient::m_pRelationTbl,os.str().c_str(),&select_field,true);

		///  加载社会关系
	//2011,10,31 把关系分开包
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_RDB2S_RelationLoad  * send_cmd = (MODI_RDB2S_RelationLoad *) cmd_buf;
	AutoConstruct(send_cmd);
	send_cmd->m_id = pReq->m_nCharID;
	send_cmd->m_wNum = 0;

	MODI_DBRelationInfo * p_startaddr = (MODI_DBRelationInfo * )(&(send_cmd->m_info[0]));
	
	for(int i=0; i<ret_count; i++)
	{
		MODI_Record * pRecord = record_select.GetRecord(i);
		if(pRecord == NULL)
		{
			MODI_ASSERT(0);
			continue;
		}

		const MODI_VarType & v_relguid = pRecord->GetValue("relationguid");
		const MODI_VarType & v_reltype = pRecord->GetValue("reltype");
		const MODI_VarType & v_relsex = pRecord->GetValue("sex");
		const MODI_VarType & v_relname = pRecord->GetValue("relationname");
		const MODI_VarType & v_id = pRecord->GetValue("relid");

		if(v_relguid.Empty() || v_reltype.Empty() || v_relsex.Empty() || v_relname.Empty() || v_id.Empty())
		{
			MODI_ASSERT(0);
			continue;
		}
			
		p_startaddr->m_guid = MAKE_GUID( v_relguid.ToUInt() , 0 );
		p_startaddr->m_RelationType = (enRelationType)v_reltype.ToInt();
		p_startaddr->m_bySex = v_relsex.ToUChar();
		p_startaddr->m_dwRelid = (DWORD)v_id;
		strncpy( p_startaddr->m_szName  , v_relname.ToStr() , sizeof(p_startaddr->m_szName) - 1 );
		p_startaddr->m_bOnline = false;
		
		p_startaddr++;
		send_cmd->m_wNum++;
		
		if(sizeof(MODI_RDB2S_RelationLoad) + send_cmd->m_wNum * sizeof(MODI_DBRelationInfo) > (Skt::MAX_USERDATASIZE - (sizeof(MODI_DBRelationInfo) + 1024)))
		{
			if(i+1 == ret_count)
			{
				send_cmd->m_byIsLoadEnd = 1;
			}
			
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_RelationLoad) + send_cmd->m_wNum * sizeof(MODI_DBRelationInfo));
			memset(cmd_buf, 0, sizeof(cmd_buf));
			AutoConstruct(send_cmd);
			send_cmd->m_id = pReq->m_nCharID;
			p_startaddr = (MODI_DBRelationInfo * )(&(send_cmd->m_info[0]));
			send_cmd->m_wNum = 0;
		}
	}

	if((send_cmd->m_wNum == 0) && (send_cmd->m_byIsLoadEnd == 1))
	{
		return enPHandler_OK;
	}
	
	//	if(send_cmd->m_wNum > 0)
	{
		send_cmd->m_byIsLoadEnd = 1;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_RelationLoad) + send_cmd->m_wNum * sizeof(MODI_DBRelationInfo));
	}

	return enPHandler_OK;
#if 0	

	int i=0;
	for( i=0;  i< 500 && i<retcount; ++i )
	{
		MODI_Record * pRecord = record_select.GetRecord(i);
		if( !pRecord)
		{
			Global::logger->debug("[relation_load] relation load error ");
			MODI_ASSERT(0);
			continue;
		}		
		
		const MODI_VarType & v_relguid = pRecord->GetValue("relationguid");
		const MODI_VarType & v_reltype = pRecord->GetValue("reltype");
		const MODI_VarType & v_relsex = pRecord->GetValue("sex");
		const MODI_VarType & v_relname = pRecord->GetValue("relationname");
		const MODI_VarType & v_id = pRecord->GetValue("relid");
			
		MODI_DBRelationInfo * pTemp = &(relation->m_info[i]);
		pTemp->m_guid = MAKE_GUID( v_relguid.ToUInt() , 0 );
		pTemp->m_RelationType = (enRelationType)v_reltype.ToInt();
		pTemp->m_bySex = v_relsex.ToUChar();
		pTemp->m_dwRelid = (DWORD)v_id;
		memset( pTemp->m_szName , 0 , sizeof(pTemp->m_szName) );
		safe_strncpy( pTemp->m_szName  , v_relname.ToStr() , sizeof(pTemp->m_szName) );
		pTemp->m_bOnline = false;

	}	
		
	relation->m_id = pReq->m_nCharID;
	relation->m_wNum = i;
	MODI_DBProxyClient::GetInstancePtr()->SendPackage( relation , sizeof(MODI_RDB2S_RelationLoad) + i * sizeof(MODI_DBRelationInfo) );

	memset(cmd_buf, 0, sizeof(cmd_buf));
	relation = (MODI_RDB2S_RelationLoad *) cmd_buf;
	AutoConstruct(relation);
	if( retcount > 500)
	{
		for( ; i< retcount ; ++i )
		{
			MODI_Record * pRecord = record_select.GetRecord(i);
			if( !pRecord)
			{
				Global::logger->debug("[relation_load] relation load error ");
				MODI_ASSERT(0);
				continue;
			}		
			
			const MODI_VarType & v_relguid = pRecord->GetValue("relationguid");
			const MODI_VarType & v_reltype = pRecord->GetValue("reltype");
			const MODI_VarType & v_relsex = pRecord->GetValue("sex");
			const MODI_VarType & v_relname = pRecord->GetValue("relationname");
			const MODI_VarType & v_id = pRecord->GetValue("relid");
				
			MODI_DBRelationInfo * pTemp = &(relation->m_info[i]);
			pTemp->m_guid = MAKE_GUID( v_relguid.ToUInt() , 0 );
			pTemp->m_RelationType = (enRelationType)v_reltype.ToInt();
			pTemp->m_bySex = v_relsex.ToUChar();
			pTemp->m_dwRelid = (DWORD)v_id;
			memset( pTemp->m_szName , 0 , sizeof(pTemp->m_szName) );
			safe_strncpy( pTemp->m_szName  , v_relname.ToStr() , sizeof(pTemp->m_szName) );
			pTemp->m_bOnline = false;
		}
		i-=500;
		relation->m_id = pReq->m_nCharID;
		relation->m_wNum = i;

		MODI_DBProxyClient::GetInstancePtr()->SendPackage( relation , sizeof(MODI_RDB2S_RelationLoad) + i * sizeof(MODI_DBRelationInfo) );
	}
	return enPHandler_OK;
#endif	
}


/// 保存角色数据
int MODI_PackageHandler_rdb::SaveRoleData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_S2RDB_Request_SaveCharData * pReq = (MODI_S2RDB_Request_SaveCharData *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: SaveRoleData_Handler .<m_nCharID=%u,m_nChannelID=%u>" , 
			GAMEDB_OPT ,
			pReq->m_nCharID ,
			pReq->m_nChannelID );

	if( pReq->m_nCharID == INVAILD_CHARID )
		return enPHandler_OK;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

//	MODI_DBCharfullData  dbcfd( pDBClient );
//	dbcfd.SetCharGuid( pReq->m_nCharID );

	MODI_DBOperatorTimeLog 	debug_log("MODI_DBCharBaseinfo");

	MODI_DBSaveCharBaseInfo saveInfo;
	saveInfo.pRoleData = &(pReq->m_roleInfo);
	saveInfo.pExtraData = &(pReq->m_roleExtraData);
	saveInfo.pOtherData = &(pReq->m_Data[0]);
	saveInfo.m_dwSize = pReq->m_dwSize;

//	if( dbcfd.Save( &(pReq->m_CharFullData ) ) )
	{
	const MODI_DBSaveCharBaseInfo * pSave = (const MODI_DBSaveCharBaseInfo *)(&saveInfo);
	if( !pSave->pExtraData  ||
		!pSave->pRoleData)
		return false;
	std::ostringstream _sWhere;

       _sWhere	<< "guid="<<pReq->m_nCharID;
	MODI_TableStruct  * _tCharTable = MODI_DBManager::GetInstance().GetTable("characters");
	if( !_tCharTable)
	{
		Global::logger->debug("Can't get characters table ");
		return -1;
	}
	
	std::string strBirthday = TimeHelpFuns::GetDateString( pSave->pRoleData->m_detailInfo.m_nBirthDay  );
	char szNormalFlags[128];
	memset( szNormalFlags , 0 , sizeof(szNormalFlags) );

	Binary2String( pSave->pExtraData->m_szNormalFlags , 
			sizeof(pSave->pExtraData->m_szNormalFlags),
			szNormalFlags );
	MODI_Record   _CharField;
	_CharField.Put("age", pSave->pRoleData->m_detailInfo.m_byAge);
	_CharField.Put("city",pSave->pRoleData->m_detailInfo.m_nCity);
	_CharField.Put("personalsign",pSave->pRoleData->m_detailInfo.m_szPersonalSign);
	_CharField.Put("clan" ,pSave->pRoleData->m_detailInfo.m_nClan);
	_CharField.Put("birthday",strBirthday.c_str()); 
	//_CharField.Put( "group",pSave->pRoleData->m_detailInfo.m_nGroup);
	_CharField.Put("exp" ,pSave->pRoleData->m_normalInfo.m_nExp);
	_CharField.Put("qq",pSave->pRoleData->m_detailInfo.m_nQQ);
	_CharField.Put("blood",pSave->pRoleData->m_detailInfo.m_byBlood);
	_CharField.Put("level",pSave->pRoleData->m_baseInfo.m_ucLevel);
	_CharField.Put("votecount",pSave->pRoleData->m_detailInfo.m_nVoteCount);
	_CharField.Put("wincount",pSave->pRoleData->m_detailInfo.m_nWin);
	_CharField.Put("losscount",pSave->pRoleData->m_detailInfo.m_nLoss);
	_CharField.Put("tiecount",pSave->pRoleData->m_detailInfo.m_nTie);
	_CharField.Put("readhelp",pSave->pRoleData->m_detailInfo.m_byReadHelp);
	_CharField.Put("normalflags",szNormalFlags);
	_CharField.Put("chang_count",pSave->pRoleData->m_detailInfo.m_Chang);
	_CharField.Put("heng_count",pSave->pRoleData->m_detailInfo.m_Heng);
	_CharField.Put("keyboard_count",pSave->pRoleData->m_detailInfo.m_KeyBoard);
	_CharField.Put("vote_count",pSave->pRoleData->m_detailInfo.m_Toupiao);
	_CharField.Put("today_online_time",pSave->pExtraData->m_nTodayOnlineTime);
	_CharField.Put("total_online_time",pSave->pExtraData->m_nTotalOnlineTime);
	_CharField.Put("highest_score",pSave->pRoleData->m_detailInfo.highest_socre);
	_CharField.Put("highest_score_musicid",pSave->pRoleData->m_detailInfo.highest_score_musicid);
	_CharField.Put("vip" ,pSave->pRoleData->m_baseInfo.m_bVip);
	_CharField.Put("highest_precision",pSave->pRoleData->m_detailInfo.highest_precision);
	_CharField.Put("chenghao",pSave->pRoleData->m_normalInfo.m_wdChenghao);
	_CharField.Put("highest_precision_musicid",pSave->pRoleData->m_detailInfo.highest_precision_musicid);
	_CharField.Put("block_count",pSave->pExtraData->m_wdBlockCount);
	_CharField.Put("key_highest_score",pSave->pRoleData->m_detailInfo.key_highest_socre);
	_CharField.Put("key_highest_score_musicid",pSave->pRoleData->m_detailInfo.key_highest_score_musicid);
	_CharField.Put("key_highest_precision",pSave->pRoleData->m_detailInfo.key_highest_precision);
	_CharField.Put("key_highest_precision_musicid",pSave->pRoleData->m_detailInfo.key_highest_precision_musicid);
	_CharField.Put("where",_sWhere.str());
	

	pDBClient->ExecUpdate( _tCharTable,&_CharField);

	}

	return enPHandler_OK;
}


/** 
 * @brief 创建角色
 * 
 *
 */
int MODI_PackageHandler_rdb::CreateChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_CreateChar * p_recv_cmd = (const MODI_S2RDB_Request_CreateChar *)(pt_null_cmd);
	Global::logger->debug("[create_role] create role info <accountid=%u,accname=%s,rolename=%s>",
						  p_recv_cmd->m_nAccountID ,p_recv_cmd->m_cstrAccName,p_recv_cmd->m_createInfo.m_szRoleName);

	MODI_RoleDBAPP * pApp = (MODI_RoleDBAPP *)(MODI_IAppFrame::GetInstancePtr());
	
	/// 操作结果回复
	MODI_RDB2S_Notify_CreateCharResult 	createResult;
	createResult.m_bySuccessful = SvrResult_Role_CreateRoleSuccessful;
	createResult.m_nSessionID = p_recv_cmd->m_nSessionID;
	createResult.m_nAccountID = p_recv_cmd->m_nAccountID;
	
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_db_client);

	/// 获取默认的item
	MODI_ItemInfoAdd avatar_item[MAX_BAG_AVATAR_COUNT];
	memset(avatar_item, 0, sizeof(avatar_item));
	WORD avatar_item_count = MAX_BAG_AVATAR_COUNT;
	
	MODI_DefaultAvatarCreator & dav = pApp->GetDefaultAvatarCreator();
	if(!dav.CreaterDefaultAvatar(p_recv_cmd->m_createInfo.m_AvatarIdx, &(avatar_item[0]), avatar_item_count))
	{
		createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(&createResult, sizeof(createResult));
		Global::logger->warn("[get_default_avatar] accid<%u> unable get default avatar <accid=%u,id=%d>",
							 p_recv_cmd->m_nAccountID, p_recv_cmd->m_createInfo.m_AvatarIdx);
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	p_db_client->BeginTransaction();
	bool ret_code = true;
	bool is_dup = false;
	do
	{
		/// 插入到角色表
		MODI_Record insert_character;
		insert_character.Put("accountid", p_recv_cmd->m_nAccountID);
		insert_character.Put("accname", p_recv_cmd->m_cstrAccName);
		insert_character.Put("sex", p_recv_cmd->m_createInfo.m_bySex);
		insert_character.Put("city", p_recv_cmd->m_createInfo.m_nCityID);
		insert_character.Put("blood", p_recv_cmd->m_createInfo.m_byXuexing);
		insert_character.Put("qq", p_recv_cmd->m_createInfo.m_qq);
		insert_character.Put("defavatar", p_recv_cmd->m_createInfo.m_AvatarIdx);

		std::string str_birthday = TimeHelpFuns::GetDateString(p_recv_cmd->m_createInfo.m_nBirthday);
		insert_character.Put("birthday", str_birthday);
	
		char person[MAX_PERSONAL_SIGN + 1];
		memset(person, 0, sizeof(person));
		strncpy(person, p_recv_cmd->m_createInfo.m_Sign, sizeof(person) - 1);
		insert_character.Put("personalsign", person);

		char role_name[ROLE_NAME_MAX_LEN + 1];
		memset(role_name, 0, sizeof(role_name));
		strncpy(role_name, p_recv_cmd->m_createInfo.m_szRoleName, sizeof(role_name) - 1);
		insert_character.Put("name", role_name);

		MODI_RecordContainer insert_character_container;
		insert_character_container.Put(&insert_character);

		if(p_db_client->ExecInsert(GlobalDB::m_pCharacterTbl, &insert_character_container) != 1)
		{
			if(p_db_client->GetLastError() == ER_DUP_ENTRY)
			{
				is_dup = true;
			}
			ret_code = false;
			break;
		}

		/// 网页道具使用的信息
		MODI_Record insert_charinfo;
		insert_charinfo.Put("accountid", p_recv_cmd->m_nAccountID);
		insert_charinfo.Put("sex", p_recv_cmd->m_createInfo.m_bySex);

		MODI_RecordContainer insert_charinfo_container;
		insert_charinfo_container.Put(&insert_charinfo);

		if(p_db_client->ExecInsert(GlobalDB::m_pCharInfoTbl, &insert_charinfo_container) != 1)
		{
			ret_code = false;
			break;
		}
		
		
		/// 把道具表清理一下，出现这种情况肯定错误了，但为了能创建还是先清理
		std::ostringstream del_where;
		del_where<< "accid=" << p_recv_cmd->m_nAccountID;
		DWORD del_num = p_db_client->ExecDelete(GlobalDB::m_pItemInfoTbl, del_where.str().c_str());
		if(del_num > 0)
		{
			Global::logger->error("[create_role] before create role have item <accid=%u,del_num=%u>", p_recv_cmd->m_nAccountID, del_num);
			MODI_ASSERT(0);
		}
		
	
		/// 插入到item_infos
		for(WORD i=0; i<avatar_item_count; i++)
		{
			if(avatar_item[i].m_nConfigID == INVAILD_CONFIGID || avatar_item[i].m_nBagType != enBagType_Avatar)
			{
				Global::logger->debug("[add_default_item] get default id <pos=%u>", i);
				MODI_ASSERT(0);
				continue;
			}
			
			MODI_Record insert_item;
			Global::m_stItemIdLock.wrlock();
			Global::g_qdItemId++;
			insert_item.Put("itemid", Global::g_qdItemId);
			Global::m_stItemIdLock.unlock();
			insert_item.Put("accid", p_recv_cmd->m_nAccountID);
			insert_item.Put("configid", avatar_item[i].m_nConfigID);
			insert_item.Put("bag_type", avatar_item[i].m_nBagType);
			insert_item.Put("bag_pos", avatar_item[i].m_nItemPos);
			insert_item.Put("create_reason", enAddReason_CreateRole);
			insert_item.Put("server_id", pApp->GetServerID());
			std::ostringstream os;
			QWORD m_qdExpireTime = 0;
			os<< "from_unixtime(" << m_qdExpireTime<< ")";
			insert_item.Put("expire_time", os.str());//enForever
			
			MODI_RecordContainer insert_item_container;
			insert_item_container.Put(&insert_item);

			if(p_db_client->ExecInsert(GlobalDB::m_pItemInfoTbl, &insert_item_container) != 1)
			{
				Global::logger->error("[insert_default] insert default failed <itemid=%llu,configid=%u>", Global::g_qdItemId, avatar_item[i].m_nConfigID);
				MODI_ASSERT(0);
				ret_code = false;
				break;
			}
		}

	}while(0);

	if(ret_code)
	{
		/// 创建成功，读取角色和avatar发送给客户端
		p_db_client->CommitTransaction();
		MODI_CharInfo & char_info = createResult.m_roleInfo;
		if(LoadCharList(p_recv_cmd->m_nAccountID, char_info) != 1)
		{
			Global::logger->fatal("[create_role] load data failed <accid=%u>", p_recv_cmd->m_nAccountID);
			MODI_ASSERT(0);
			createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
		}
	}
	else
	{
		p_db_client->RollbackTransaction();
		if(is_dup)
		{
			createResult.m_bySuccessful = SvrResult_Role_CRFNameAlreadyExist;
		}
		else
		{
			createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
			MODI_ASSERT(0);
		}
	}
				
	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );
	return enPHandler_OK;
}

#if 0
int MODI_PackageHandler_rdb::CreateChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_CreateChar * pReq = (const MODI_S2RDB_Request_CreateChar *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: CreateChar_Handler .<m_nAccountID=%u,m_nAccName=%s,RoleName=%s>",
						  GAMEDB_OPT , pReq->m_nAccountID ,pReq->m_cstrAccName,pReq->m_createInfo.m_szRoleName);


	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_RoleDBAPP * pApp = (MODI_RoleDBAPP *)(MODI_IAppFrame::GetInstancePtr());
	// DB中角色列表操作对象
	MODI_DBCharlist dbcl( pDBClient );
	dbcl.SetAccountID( pReq->m_nAccountID );
	dbcl.SetAccount(pReq->m_cstrAccName);
	dbcl.SetMaxItemSerial( pApp->GetMaxItemSerial() );

	// 操作结果回复
	MODI_RDB2S_Notify_CreateCharResult 	createResult;
	createResult.m_nSessionID = pReq->m_nSessionID;
	createResult.m_nAccountID = pReq->m_nAccountID;

	MODI_AddCharacter 	addChar;
	MODI_ItemInfoAdd 	addItems[MAX_BAG_AVATAR_COUNT];

	addChar.m_pCreateInfo = &(pReq->m_createInfo);
	addChar.m_itemAdd.m_nItemAddCount = 0;
	addChar.m_itemAdd.m_pItemInfoAdd = 0;

	addChar.m_itemAdd.m_pItemInfoAdd = addItems;
	addChar.m_itemAdd.m_nItemAddCount = MAX_BAG_AVATAR_COUNT;

	// 获得默认的AVATAR形象
	MODI_DefaultAvatarCreator & dav = pApp->GetDefaultAvatarCreator();
	if( !dav.CreaterDefaultAvatar( addChar.m_pCreateInfo->m_AvatarIdx ,  
				addChar.m_itemAdd.m_pItemInfoAdd , 
				addChar.m_itemAdd.m_nItemAddCount ) )
	{
		createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );
		Global::logger->warn("[%s] accid<%u> create char was faild . invalid default avatar index<%u> . " , 
				ROLEDATA_OPT , 
				pReq->m_nAccountID , 
				addChar.m_pCreateInfo->m_AvatarIdx );

		return enPHandler_OK;

		return false;
	}

	// 添加一个新的角色到角色列表
	if( !dbcl.AddNew( &addChar ) )
	{
		if( dbcl.IsHasDupCharacter() )
		{
			// 重复的角色名字
			createResult.m_bySuccessful = SvrResult_Role_CRFNameAlreadyExist;
			Global::logger->debug("[%s] accid<%u> create char was faild . dup rolename. " , 
					ROLEDATA_OPT , 
					pReq->m_nAccountID );
		}
		else 
		{
			createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
			Global::logger->warn("[%s] accid<%u> create char was faild . sql statement for create role exe faild . " , 
					ROLEDATA_OPT , 
					pReq->m_nAccountID );
		}

		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );
		return enPHandler_OK;
	}

//	pApp->SetMaxItemSerial( pApp->GetMaxItemSerial() + addChar.m_itemAdd.m_nItemAddCount );

	// 加载角色列表
	if( !dbcl.Load() )
	{
		createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );
		Global::logger->warn("[%s] accid<%u> create char was faild . sql statement for load charlist faild . " , 
				ROLEDATA_OPT , 
				pReq->m_nAccountID );
		return enPHandler_OK;
	}	

	// 解析角色列表
	if( !dbcl.ParseLoadResult( &(createResult.m_roleInfo )) )
	{
		createResult.m_bySuccessful  = SvrResult_Role_CreateRoleFaild;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );
		Global::logger->warn("[%s] accid<%u> create char was faild . parse charlist load result . " , 
				ROLEDATA_OPT , 
				pReq->m_nAccountID );
		return enPHandler_OK;
	}

	// 结果
	createResult.m_bySuccessful = SvrResult_Role_CreateRoleSuccessful;
	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &createResult , sizeof(createResult) );

// 	/// 增加一个表
// 	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
// 	if(! p_dbclient)
// 	{
// 		Global::logger->fatal("not get db handle in create role");
// 	}
// 	else
// 	{		
// 		MODI_Record record_insert;
// 		record_insert.Put("accountid", pReq->m_nAccountID);
// 		MODI_RecordContainer record_container;
// 		record_container.Put(&record_insert);
// 		if(p_dbclient->ExecInsert(MODI_DBProxyClient::m_pCharacterOther, &record_container) != 1)
// 		{
// 			Global::logger->fatal("not insert into characters_other table <acc=%u>",pReq->m_nAccountID);
// 		}
// 	}

// 	Global::logger->debug("[%s] accid<%u> create char was successful ." , ROLEDATA_OPT , 
// 			pReq->m_nAccountID ); 

	return enPHandler_OK;
}

#endif

#if 0
int MODI_PackageHandler_rdb::ItemCreateHistroy_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_CreateItemHistroy * pReq = (const MODI_S2RDB_Request_CreateItemHistroy *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: ItemCreateHistroy ." , GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBShopHistroy dbsh( pDBClient );

	if( !dbsh.Save( &pReq->m_Histroy  ) )
	{
		Global::logger->error("[%s] save createitem histroy faild.<transid=%llu,charid=%u,createtype=%u,recvstate=%u>" , GAMEDB_OPT , 
				pReq->m_Histroy.transid ,
				pReq->m_Histroy.charid ,
				pReq->m_Histroy.createtype ,
				pReq->m_Histroy.recvstate );
	}

	return enPHandler_OK;
}


/// 保存物品
int MODI_PackageHandler_rdb::SaveItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{

	const MODI_S2RDB_Request_SaveItem * pReq = (const MODI_S2RDB_Request_SaveItem *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: SaveItem_Handler .<m_nCharID=%u,m_nSize=%u>",
			GAMEDB_OPT ,
			pReq->m_nCharID , 
			pReq->m_nSize );

	if( pReq->m_nCharID == INVAILD_CHARID )
		return enPHandler_OK;

	if( !pReq->m_nSize  )
		return enPHandler_OK;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBItemlist dbil( pDBClient );
	dbil.SetCharGuid( pReq->m_nCharID );

	MODI_SaveItem saveItems;
	saveItems.m_bTrans = true;
	saveItems.m_nItemSaveCount = pReq->m_nSize;
	saveItems.m_pItemInfoSave = (MODI_DBItemInfo *)(pReq->m_pData);

	MODI_DBOperatorTimeLog 	debug_log("MODI_DBSaveItem");
	debug_log.Begin();

	if( !dbil.Save( &saveItems ) )
	{
		Global::logger->warn("[%s] save character's<%u> items to db faild." ,
				ROLEDATA_OPT , 
				pReq->m_nCharID );
		return enPHandler_OK;
	}

	debug_log.End();

	Global::logger->debug("[%s] save character's<%u> items to db successful." ,
			ROLEDATA_OPT ,
			pReq->m_nCharID);

	return enPHandler_OK;
}

int MODI_PackageHandler_rdb::ItemSerialTableOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ItemSerialTableOpt * pReq = (const MODI_S2RDB_Request_ItemSerialTableOpt *)(pt_null_cmd);
	MODI_ItemSerialSet * pItemSerialSet = MODI_ItemSerialSet::GetInstancePtr();

	Global::logger->debug("[%s] Call Function: ItemSerialTableOpt_Handler .<m_opt=%d>",
			GAMEDB_OPT ,
			(int)pReq->m_opt);

	if( pReq->m_opt == MODI_S2RDB_Request_ItemSerialTableOpt::kLoad )
	{
		Global::logger->info("[%s] server request load item serial table." , ROLEDATA_OPT );
		MODI_DBOperatorTimeLog 	debug_log("MODI_DBInitItemSerialSet");
		debug_log.Begin();
		if( pItemSerialSet->Init() == false )
		{
			Global::logger->fatal("[%s] load item serial faild. shut down db service." , 
					ROLEDATA_OPT );

			MODI_RoleDBAPP * pApp = (MODI_RoleDBAPP *)MODI_IAppFrame::GetInstancePtr();
			pApp->Terminate();
		}
		debug_log.End();
	}
	else if( pReq->m_opt == MODI_S2RDB_Request_ItemSerialTableOpt::kSave )
	{
		Global::logger->info("[%s] server request save item serial table." , 
				ROLEDATA_OPT );
		MODI_DBOperatorTimeLog 	debug_log("MODI_DBSaveItemSerialSet");
		debug_log.Begin();
		pItemSerialSet->DoSaveAll();
		debug_log.End();
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_rdb::LoadItemSerial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ItemSerial * pReq = (const MODI_S2RDB_Request_ItemSerial *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: LoadItemSerial .<m_byWorldID=%u , m_byServerID=%u>",
			GAMEDB_OPT ,
			pReq->m_byWorldID,
			pReq->m_byServerID);

	if( !pReq->m_byServerID || !pReq->m_byWorldID )
		return enPHandler_OK;

	MODI_RDB2S_Notify_ItemSerial msg;
	msg.m_byServerID = pReq->m_byServerID;
	msg.m_byWorldID = pReq->m_byWorldID;

	MODI_ItemSerialSet * pSet = MODI_ItemSerialSet::GetInstancePtr();
	if( pSet->GetItemSerial( pReq->m_byWorldID , pReq->m_byServerID , msg.m_nItemSerial )  )
	{
		msg.m_bySuccessful = 1;
	}
	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &msg , sizeof(msg) );
	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::SaveItemSerial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_SaveSerial * pReq = (const MODI_S2RDB_Request_SaveSerial *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: SaveItemSerial .<m_byWorldID=%u , m_byServerID=%u>",
			GAMEDB_OPT ,
			pReq->m_byWorldID,
			pReq->m_byServerID);

	if( !pReq->m_byServerID || !pReq->m_byServerID )
		return enPHandler_OK;

	MODI_ItemSerialSet * pSet = MODI_ItemSerialSet::GetInstancePtr();
	pSet->SetItemSerial( pReq->m_byWorldID , pReq->m_byServerID , pReq->m_nItemSerial );

	if( pReq->m_byForce )
	{
		pSet->Save( pReq->m_byWorldID , pReq->m_byServerID );
	}
	return enPHandler_OK;
}

#endif

int MODI_PackageHandler_rdb::SaveRelations_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_SaveRelation * pReq = (const MODI_S2RDB_Request_SaveRelation *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: SaveRelations_Handler .<m_Charid=%u , m_nSize=%u>",
			GAMEDB_OPT ,
			pReq->m_Charid,
			pReq->m_nSize );

	if( !pReq->m_nSize ||
		pReq->m_Charid == INVAILD_CHARID )
		return enPHandler_OK;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBRelation dbrel( pDBClient );
	dbrel.SetCharGuid( pReq->m_Charid );

	MODI_SaveRelationInfo saveInfo;
	saveInfo.m_bTrans = false;
	saveInfo.m_nSize = pReq->m_nSize;
	saveInfo.m_pRelationList = (MODI_DBRelationInfo *)(pReq->m_pData);

	MODI_DBOperatorTimeLog 	debug_log("MODI_DBSaveRelations");
	debug_log.Begin();
	if( !dbrel.Save( &saveInfo ) )
	{
		Global::logger->warn("[%s] Save Character<charid=%u>'s Relations faild." , 
				GAMEDB_OPT ,
				pReq->m_Charid );
	}
	debug_log.End();

	return enPHandler_OK;
}



/** 
 * @brief 加载名片信息
 * 
 * 
 */
int MODI_PackageHandler_rdb::RoleDetailInfo_Handler( void * pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	const MODI_S2RDB_Request_RoleDetailInfo * p_recv_cmd = (const MODI_S2RDB_Request_RoleDetailInfo *)(pt_null_cmd);
	
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Notify_RoleDetailInfo * p_send_cmd = (MODI_RDB2S_Notify_RoleDetailInfo *)(buf);
	AutoConstruct(p_send_cmd);

	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_db_client);

	/// 保留的数据返回
	p_send_cmd->m_reqCharid = p_recv_cmd->m_reqCharid;
	p_send_cmd->m_opt = p_recv_cmd->m_opt;
	p_send_cmd->m_RelationType = p_recv_cmd->m_RelationType;
	p_send_cmd->m_nExtraSize = p_recv_cmd->m_nExtraSize;
	p_send_cmd->m_dwAccId = p_recv_cmd->m_dwAccId;
	memcpy(p_send_cmd->m_szExtra , p_recv_cmd->m_szExtra , p_recv_cmd->m_nExtraSize);
	strncpy(p_send_cmd->m_namecard.m_szName, p_recv_cmd->m_szName, sizeof(p_send_cmd->m_namecard.m_szName) - 1);
	p_send_cmd->m_bCharExist = true;
	
	int send_cmd_size = sizeof(MODI_RDB2S_Notify_RoleDetailInfo) + (sizeof(char) * (p_send_cmd->m_nExtraSize));

	/// 获取角色信息
	std::ostringstream exec_sql;
	if(p_recv_cmd->m_opt == kReqPayAction)
	{
		exec_sql << "accountid=\'" << p_recv_cmd->m_dwAccId<< "\'";
	}
	else if(p_recv_cmd->m_opt == kSendGMMails)
	{
		if(p_recv_cmd->m_RelationType == 1)
		{
			exec_sql << "name=\'" << p_send_cmd->m_namecard.m_szName<< "\'";
		}
		else if(p_recv_cmd->m_RelationType == 2)
		{
			exec_sql << "accname=\'" << p_recv_cmd->m_szAccName<< "\'";
		}
	}
	else 
	{
		exec_sql << "name=\'" << p_send_cmd->m_namecard.m_szName<< "\'";
	}

	MODI_RecordContainer character_container;
	int char_num = 0;
	if((char_num = p_db_client->ExecSelect(character_container, GlobalDB::m_pCharacterTbl, exec_sql.str().c_str())) != 1)
	{
		/// 没有角色
		p_send_cmd->m_bCharExist = false;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_cmd, send_cmd_size);
		return enPHandler_OK;
	}

	MODI_Record * pRecord = character_container.GetRecord(0);
	if(! pRecord)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	const MODI_VarType & v_charid = pRecord->GetValue("guid");
	const MODI_VarType & v_accid = pRecord->GetValue("accountid");
	const MODI_VarType & v_age = pRecord->GetValue("age");
	const MODI_VarType & v_city = pRecord->GetValue("city");
	const MODI_VarType & v_sign = pRecord->GetValue("personalsign");
	const MODI_VarType & v_group = pRecord->GetValue("group");
	const MODI_VarType & v_clan = pRecord->GetValue("clan");
	const MODI_VarType & v_qq = pRecord->GetValue("qq");
	const MODI_VarType & v_sex = pRecord->GetValue("sex");
	const MODI_VarType & v_blood = pRecord->GetValue("blood");
	const MODI_VarType & v_level = pRecord->GetValue("level");
	const MODI_VarType & v_birthday = pRecord->GetValue("birthday");
	const MODI_VarType & v_renqi = pRecord->GetValue("renqi");
	const MODI_VarType & v_fensib = pRecord->GetValue("fensiboy");
	const MODI_VarType & v_fensig = pRecord->GetValue("fensigril");
	const MODI_VarType & v_defavatar = pRecord->GetValue("defavatar");
	const MODI_VarType & v_win = pRecord->GetValue("wincount");
	const MODI_VarType & v_loss = pRecord->GetValue("lostcount");
	const MODI_VarType & v_tie = pRecord->GetValue("tiecount");
	const MODI_VarType & v_chenghaoid = pRecord->GetValue("chenghao");
	const MODI_VarType & v_Heng = pRecord->GetValue("heng_count");
	const MODI_VarType & v_change =pRecord->GetValue("chang_count");
	const MODI_VarType & v_jianpan =pRecord->GetValue("keyboard_count");
	const MODI_VarType  & v_vip = pRecord->GetValue("vip");
	const MODI_VarType & v_accname = pRecord->GetValue("accname");
	const MODI_VarType & v_name = pRecord->GetValue("name");
 
	p_send_cmd->m_bVip = v_vip.ToUChar();
	strncpy(p_send_cmd->m_namecard.m_szName, (const char *)v_name, sizeof(p_send_cmd->m_namecard.m_szName) - 1);
	strncpy(p_send_cmd->m_namecard.m_szAccName, (const char *)v_accname, sizeof(p_send_cmd->m_namecard.m_szAccName) - 1);
	p_send_cmd->m_dwAccId = v_accid.ToUInt();
	p_send_cmd->m_charid = v_charid.ToUInt();
	p_send_cmd->m_namecard.m_byLevel = v_level.ToUChar();
	p_send_cmd->m_namecard.m_bySex = v_sex.ToUChar();
	p_send_cmd->m_namecard.m_wdChenghao = v_chenghaoid.ToUShort();
	p_send_cmd->m_namecard.m_RoleDetail.m_byAge = v_age.ToUChar();
	p_send_cmd->m_namecard.m_RoleDetail.m_byBlood = v_blood.ToUChar();
	p_send_cmd->m_namecard.m_RoleDetail.m_nCity = v_city.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_SocialRelData.m_nFensiBoy = v_fensib.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_SocialRelData.m_nFensiGril = v_fensig.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_SocialRelData.m_nRenqi = v_renqi.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_nQQ = v_qq.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_nCity = v_group.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_nClan = v_clan.ToUInt();
	strncpy(p_send_cmd->m_namecard.m_RoleDetail.m_szPersonalSign , v_sign.ToStr() , sizeof(p_send_cmd->m_namecard.m_RoleDetail.m_szPersonalSign) - 1);
	std::string strBirthday = v_birthday.ToStr();
	p_send_cmd->m_namecard.m_RoleDetail.m_nBirthDay= TimeHelpFuns::EncodeDateFromString(strBirthday.c_str());
	p_send_cmd->m_namecard.m_nDefaultAvatarIdx = v_defavatar.ToUShort();
	p_send_cmd->m_namecard.m_RoleDetail.m_nWin = v_win.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_nTie = v_tie.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_nLoss = v_loss.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_Heng  = v_Heng.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_Chang = v_change.ToUInt();
	p_send_cmd->m_namecard.m_RoleDetail.m_KeyBoard = v_jianpan.ToUInt();

	/// 加载avatar信息
	DWORD acc_id = (DWORD)v_accid;
	MODI_ClientAvatarData & avatar_info = p_send_cmd->m_namecard.m_avatar;
	if(LoadCharAvatar(acc_id, avatar_info) == -1)
	{
		return enPHandler_OK;
	}
	
	MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_cmd, send_cmd_size);
	return enPHandler_OK;
}


#if 0
int MODI_PackageHandler_rdb::RoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_RoleDetailInfo * pReq = (const MODI_S2RDB_Request_RoleDetailInfo *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: RoleDetailInfo_Handler .<m_reqCharid=%u,m_szName=%s>" , 
			GAMEDB_OPT ,
			pReq->m_reqCharid,
			pReq->m_szName );

	if( pReq->m_reqCharid == INVAILD_CHARID ||
		pReq->m_szName[0] == '\0' )
		return enPHandler_OK;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBCharDetailinfo dbcd( pDBClient );
	dbcd.SetName( pReq->m_szName );
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_RDB2S_Notify_RoleDetailInfo * pTemp = (MODI_RDB2S_Notify_RoleDetailInfo *)(szPackageBuf);
	AutoConstruct( pTemp );
	MODI_RDB2S_Notify_RoleDetailInfo  & res  = *pTemp;
	res.m_nExtraSize = pReq->m_nExtraSize;
	memcpy( res.m_szExtra , pReq->m_szExtra , pReq->m_nExtraSize );

	int iSendSize = sizeof(MODI_RDB2S_Notify_RoleDetailInfo) + sizeof(char) * res.m_nExtraSize;
	res.m_bCharExist = true;

	if( !MODI_GameHelpFun::Safe_Strncpy( res.m_namecard.m_szName , 
			sizeof(res.m_namecard.m_szName) , 
			pReq->m_szName,
			sizeof(pReq->m_szName) ) )
		return enPHandler_OK;
	
	if( !dbcd.Load() )
	{
		if( dbcd.GetResult() == MODI_DBCharDetailinfo::kInternalError )
		{
			Global::logger->warn("[%s] Load Character<name=%s> Detail info faild." , 
					GAMEDB_OPT ,
					pReq->m_szName );
			return enPHandler_OK;
			MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , iSendSize );
		}
	}

	res.m_charid = dbcd.GetCharid();
	res.m_reqCharid = pReq->m_reqCharid;
	res.m_opt = pReq->m_opt;
	res.m_RelationType = pReq->m_RelationType;

	if( !dbcd.ParseLoadResult( &(res.m_namecard) ) )
	{
		if( dbcd.GetResult() == MODI_DBCharDetailinfo::kInternalError )
		{
			Global::logger->warn("[%s] ParseLoadResult Character<name=%s> Detail info faild." , 
					GAMEDB_OPT ,
					pReq->m_szName );
			return enPHandler_OK;
			MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , iSendSize );
		}
	}

	if( dbcd.GetResult() == MODI_DBCharDetailinfo::kNoCharacter )
	{
		res.m_bCharExist = false;
	}

	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , iSendSize );

	return enPHandler_OK;
}
#endif
	
int MODI_PackageHandler_rdb::SaveSocial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_SaveSocialData * pReq = (const MODI_S2RDB_Request_SaveSocialData *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function: SaveSocial_Handler .<m_charid=%u>",
			GAMEDB_OPT ,
			pReq->m_charid );

	if( pReq->m_charid == INVAILD_CHARID )
		return enPHandler_OK;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBSocialRelData db( pDBClient );
	db.SetCharGuid( pReq->m_charid );
	if( !db.Save( &(pReq->m_data) ) )
	{
		Global::logger->warn("[%s] save charactar<charid=%u> 's social relation data faild." , ROLEDATA_OPT ,
				pReq->m_charid );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::LoadMaillist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	const MODI_S2RDB_Request_LoadMaillist * pReq = (const MODI_S2RDB_Request_LoadMaillist *)(pt_null_cmd);
	
	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBMails dbMails( pDBClient );
	dbMails.SetRoleName( pReq->m_szName );

	MODI_RDB2S_Notify_LoadMaillist resMails;
	resMails.m_result = MODI_RDB2S_Notify_LoadMaillist::kLoadFaild;
	safe_strncpy( resMails.m_szName  , pReq->m_szName , sizeof(resMails.m_szName) );
	resMails.m_reason = pReq->m_reason;
	resMails.m_mail = pReq->m_mail;

	if( dbMails.Load() )
	{
		if( dbMails.IsHasChar() == false )
		{
			resMails.m_result = MODI_RDB2S_Notify_LoadMaillist::kNoChar;
		}
		else 
		{
			MODI_DBLoadMail loadMail;
			loadMail.m_bTrans = false;
			loadMail.m_pList = &(resMails.m_mails);
			if( dbMails.ParseLoadResult( &loadMail ) )
			{
				resMails.m_result = MODI_RDB2S_Notify_LoadMaillist::kSuccessful;
			}
		}
	}

	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &resMails , sizeof(resMails) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::SaveMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_SaveMail * pReq = ( const MODI_S2RDB_Request_SaveMail *)(pt_null_cmd);
	Global::logger->debug("[%s] Call Function: SaveMail_Handler",
			GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBMails dbMails( pDBClient );
	dbMails.SetRoleName( pReq->m_mail.m_MailInfo.m_szRecevier );
	if( !dbMails.Save( &(pReq->m_mail) ) )
	{
		Global::logger->info("[%s] save mail<dbid=%llu,sender=%s,name=%s,caption=%s,type=%d,isvalid=%d> faild."  , 
				GAMEDB_OPT ,
				pReq->m_mail.m_dbID,
				pReq->m_mail.m_MailInfo.m_szSender ,
				pReq->m_mail.m_MailInfo.m_szRecevier,
				pReq->m_mail.m_MailInfo.m_szCaption,
				(int)pReq->m_mail.m_MailInfo.m_Type,
				(int)pReq->m_mail.m_bIsValid );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::UpdateLastLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_UpdateLastLogin * pReq = ( const MODI_S2RDB_Request_UpdateLastLogin *)(pt_null_cmd);
	Global::logger->debug("[%s] Call Function: UpdateLastLogin",GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	MODI_DBCharBaseinfo dbChar( pDBClient );
	dbChar.SetCharGuid( pReq->m_charid );
	if( !dbChar.SaveLastLogin( pReq->m_Time , pReq->m_szIP ) )
	{
		Global::logger->info("[%s] save lastlogin information <charid=%u> faild."  , 
				GAMEDB_OPT ,
				pReq->m_charid );
	}

	return enPHandler_OK;
}
	
//int MODI_PackageHandler_rdb::QueryHasChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
//			const unsigned int cmd_size )
//{
//	const MODI_S2RDB_Request_QueryHasChar * pReq = (const MODI_S2RDB_Request_QueryHasChar *)(pt_null_cmd);
//
//	Global::logger->debug("[%s] Call Function: QueryHasChar .<queryClient charid =%u, rolename =%s>" , 
//			GAMEDB_OPT ,
//			pReq->m_queryClient,
//			pReq->m_szRoleName );
//
//	if( pReq->m_queryClient == INVAILD_CHARID ||
//		pReq->m_szRoleName[0] == '\0' )
//		return enPHandler_OK;
//
//	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
//	MODI_ScopeHandlerRelease lock( pDBClient );
//	MODI_DBCharlist dbcharl(pDBClient);
//
//	char szPackageBuf[Skt::MAX_USERDATASIZE];
//    memset(szPackageBuf, 0, sizeof(szPackageBuf));
//	MODI_RDB2S_Notify_QueryHasCharResult * pResult = (MODI_RDB2S_Notify_QueryHasCharResult *)(szPackageBuf);
//	AutoConstruct( pResult );
//
//	pResult->m_bHashChar = false;
//	pResult->m_nSize = pReq->m_nSize;
//	pResult->m_queryClient = pReq->m_queryClient;
//	pResult->m_type = pReq->m_type;
//	safe_strncpy( pResult->m_szRoleName  , pReq->m_szRoleName , ROLE_NAME_MAX_LEN );
//	pResult->m_nServerID = pReq->m_nServerID;
//	memcpy(pResult->m_pExtraData , pReq->m_pExtraData , pReq->m_nSize );
//
//	if( dbcharl.QueryHasChar( pReq->m_szRoleName ) )
//	{
//		pResult->m_bHashChar = true;
//	}
//
//	int iSendSize = sizeof(MODI_RDB2S_Notify_QueryHasCharResult) + sizeof(char) * pResult->m_nSize;
//
//	MODI_DBProxyClient::GetInstancePtr()->SendPackage( pResult , iSendSize );
//	return enPHandler_OK;
//}



int MODI_PackageHandler_rdb::ResetTodayOnlineTime_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ResetTodayOnlineTime * pReq = (const MODI_S2RDB_Request_ResetTodayOnlineTime *)(pt_null_cmd);
	DISABLE_UNUSED_WARNING( pReq );

	Global::logger->debug("[%s] Call Function:  ResetTodayOnlineTime_Handler." , GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBTodayOnlineTimeClean db( pDBClient );

	db.Excute();

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::ExchagneYYKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ExchangeByYunyingKey * pReq = (const MODI_S2RDB_Request_ExchangeByYunyingKey *)(pt_null_cmd);

	Global::logger->debug("[%s] Call Function:  ExchagneYYKey_Handler." , GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBCDKey db( pDBClient );
	db.SetKey( pReq->m_szKey );

	MODI_RDB2S_Notify_ExchangeByYunyingKeyRes res;
	res.m_charid = pReq->m_charid;
	res.m_result = kYYKeyExchange_UnKnowError;
	res.m_serverid = pReq->m_serverid;
	memcpy( res.m_szKey , pReq->m_szKey , sizeof(pReq->m_szKey) );

	if( !db.Load() )
	{
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	if( db.ParseLoadResult( &(res.m_content) ) == false )
	{
		if( db.IsExist() == false )
		{
			res.m_result = kYYKeyExchange_Invalid;
		}
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	res.m_result = kYYKeyExchange_Successful;

	MODI_DBProxyClient::GetInstancePtr()->SendPackage( &res , sizeof(res) );
	return enPHandler_OK;
}

int MODI_PackageHandler_rdb::SaveYYKey_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_SaveYunYingKey * pReq = (const MODI_S2RDB_Request_SaveYunYingKey *)(pt_null_cmd);

	if( pReq->m_charid == INVAILD_CHARID || pReq->m_szKey[0] == '\0' )
	{
		Global::logger->warn("[%s] save yunying key's valid falid. invalid package <charid=%u,key=%s>" , 
				GAMEDB_OPT ,
				pReq->m_charid , 
				pReq->m_szKey );
		return enPHandler_OK;
	}

	Global::logger->debug("[%s] Call Function:  SaveYYKey_Handler." , GAMEDB_OPT );

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_DBCDKey db( pDBClient );
	db.SetKey( pReq->m_szKey );
	db.SetCharID( pReq->m_charid );
	int iValid = 0;
	if( db.Save( &iValid ) == false )
	{
		Global::logger->warn("[%s] save yunying key's valid falid. exchanged by charid=%u" , GAMEDB_OPT ,
				pReq->m_charid );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb::LoadRanks_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_LoadRanks * pReq = (const MODI_S2RDB_Request_LoadRanks *)(pt_null_cmd);


	char szPackageBuf[Skt::MAX_USERDATASIZE];

	{

		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBLevelRank db_rank_lvl( pDBClient );
		if( db_rank_lvl.Load() )
		{
			int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_Level_List);
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_RDB2S_Notify_LoadRanksRes * res_level = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
			AutoConstruct( res_level );
			res_level->version = pReq->version;
			res_level->type = kRank_Level;
			if( db_rank_lvl.ParseLoadResult(  res_level->data  ) )
			{
				MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_level , iSendSize );
			}

			if( db_rank_lvl.LoadSelfRank() )
			{
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
				MODI_RDB2S_Notify_LoadSelfRanksRes * res_level_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
				AutoConstruct( res_level_self );
				res_level_self->type = kRank_Level;
				res_level_self->size = 0;

				MODI_DBSelfRank temp;
				temp.size = 0;
				temp.ranks = (MODI_CHARID *)res_level_self->data;
				if( db_rank_lvl.ParseLoadResult_SelfRank( &temp ) )
				{
					iSendSize += temp.size * sizeof(MODI_CHARID);
					res_level_self->size = temp.size;
					MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_level_self , iSendSize );
				}
			}
		}

	}

	{
		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBRenqiRank db_rank_renqi( pDBClient );
		if( db_rank_renqi.Load() )
		{
			int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_Renqi_List);
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_RDB2S_Notify_LoadRanksRes * res_renqi = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
			AutoConstruct( res_renqi );
			res_renqi->version = pReq->version;
 			res_renqi->type = kRank_Renqi;
			if( db_rank_renqi.ParseLoadResult(  res_renqi->data  ) )
			{
				MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_renqi , iSendSize );
			}

			if( db_rank_renqi.LoadSelfRank() )
			{
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
				MODI_RDB2S_Notify_LoadSelfRanksRes * res_renqi_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
				AutoConstruct( res_renqi_self );
				res_renqi_self ->type = kRank_Renqi;

				MODI_DBSelfRank temp;
				temp.size = 0;
				temp.ranks = (MODI_CHARID *)res_renqi_self->data;

				if( db_rank_renqi.ParseLoadResult_SelfRank( &temp ) )
				{
					iSendSize += temp.size * sizeof(MODI_CHARID);
					res_renqi_self->size = temp.size;
					MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_renqi_self , iSendSize );
				}
			}
		}
	}

	{

		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBConsumeRank db_rank_consumermb( pDBClient );
		if( db_rank_consumermb.Load() )
		{
			int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_ConsumeRMB_List);
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_RDB2S_Notify_LoadRanksRes * res_consume_rmb = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
			AutoConstruct( res_consume_rmb );
			res_consume_rmb->version = pReq->version;
			res_consume_rmb->type = kRank_Consume;
			if( db_rank_consumermb.ParseLoadResult(  res_consume_rmb->data  ) )
			{
				MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_consume_rmb , iSendSize );
			}

			if( db_rank_consumermb.LoadSelfRank() )
			{
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
				MODI_RDB2S_Notify_LoadSelfRanksRes * res_consume_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
				AutoConstruct( res_consume_self );
				res_consume_self->type = kRank_Consume;

				MODI_DBSelfRank temp;
				temp.size = 0;
				temp.ranks = (MODI_CHARID *)res_consume_self->data;
				if( db_rank_consumermb.ParseLoadResult_SelfRank( &temp ) )
				{
					iSendSize += temp.size * sizeof(MODI_CHARID);
					res_consume_self->size = temp.size;
					MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_consume_self , iSendSize );
				}
			}
		}
	}

	{

		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBHighestScore db_rank_highestscore( pDBClient );
		if( db_rank_highestscore.Load() )
		{
			int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_HighestScore_List);
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_RDB2S_Notify_LoadRanksRes * res_highest_score = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
			AutoConstruct( res_highest_score );
			res_highest_score->version = pReq->version;
			res_highest_score->type = kRank_HighestScore;
			if( db_rank_highestscore.ParseLoadResult(  res_highest_score->data  ) )
			{
				MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_highest_score , iSendSize );
			}

			if( db_rank_highestscore.LoadSelfRank() )
			{
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );			
				iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
				MODI_RDB2S_Notify_LoadSelfRanksRes * res_hs_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
				AutoConstruct( res_hs_self );
				res_hs_self->type = kRank_HighestScore;

				MODI_DBSelfRank temp;
				temp.size = 0;
				temp.ranks = (MODI_CHARID *)res_hs_self->data;
				if( db_rank_highestscore.ParseLoadResult_SelfRank( &temp ) )
				{
					iSendSize += temp.size * sizeof(MODI_CHARID);
					res_hs_self->size = temp.size;
					MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_hs_self , iSendSize );
				}
			}
		}

	}

	{
		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBHighestPrecision db_rank_precision( pDBClient );
		if( db_rank_precision.Load() )
		{
			int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_HighestPrecision_List);
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			MODI_RDB2S_Notify_LoadRanksRes * res_highest_precision = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
			AutoConstruct( res_highest_precision );
			res_highest_precision->version = pReq->version;
			res_highest_precision->type = kRank_HighestPrecision;
			if( db_rank_precision.ParseLoadResult(  res_highest_precision->data  ) )
			{
				MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_highest_precision , iSendSize );
			}

			if( db_rank_precision.LoadSelfRank() )
			{
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );			
				iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
				MODI_RDB2S_Notify_LoadSelfRanksRes * res_hp_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
				AutoConstruct( res_hp_self );
				res_hp_self->type = kRank_HighestPrecision;

				MODI_DBSelfRank temp;
				temp.size = 0;
				temp.ranks = (MODI_CHARID *)res_hp_self->data;
				if( db_rank_precision.ParseLoadResult_SelfRank( &temp ) )
				{
					iSendSize += temp.size * sizeof(MODI_CHARID);
					res_hp_self->size = temp.size;
					MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_hp_self , iSendSize );
				}
			}
		}
	}
	///	键盘准备榜
	{
		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_Record   record;
		MODI_RecordContainer  container;
		record.Put("guid");
		record.Put("name");
		record.Put("chenghao");
		record.Put("key_highest_score");
		record.Put("key_highest_score_musicid");
		int count=pDBClient->ExecSelect( container,MODI_DBProxyClient::m_pCharacter," key_highest_score_musicid != 0  and key_highest_score < 15000000 order by characters.key_highest_score desc limit 0,100",&record,true);
		int iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes) + sizeof(MODI_DBRank_HighestScore_List);
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_RDB2S_Notify_LoadRanksRes * res_keyscore = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
		AutoConstruct(res_keyscore);
		MODI_DBRank_HighestScore_List  * score_list = (MODI_DBRank_HighestScore_List *)(res_keyscore->data);
		AutoConstruct( res_keyscore);
		res_keyscore->version = pReq->version;
		res_keyscore->type = kRank_KeyScore;
		score_list->size = 0;
		for(int i=0; i< count; ++i )
		{
			MODI_Record * temp = container.GetRecord(i);
			if( temp)
			{
				const MODI_VarType  & v_guid= temp->GetValue("guid");
				const MODI_VarType  & v_name = temp->GetValue("name");
				const MODI_VarType  & v_chenghao = temp->GetValue("chenghao");
				const MODI_VarType   & v_precise = temp->GetValue("key_highest_score");
				const MODI_VarType  & v_musid = temp->GetValue("key_highest_score_musicid");

				score_list->ranks[i].charid =v_guid.ToUInt();
				score_list->ranks[i].highest_score = v_precise.ToUInt(); 
				score_list->ranks[i].musicid = v_musid.ToUInt();
				strncpy(score_list->ranks[i].role_name,(const char *)v_name, sizeof(score_list->ranks[i].role_name));
				score_list->ranks[i].title = v_chenghao.ToShort();
				score_list->size += 1;
			}
		}
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_keyscore, iSendSize );
		///加载个人的信息
		
		record.Reset();
		container.Clear();
		record.Put("guid");
		count = pDBClient->ExecSelect( container,MODI_DBProxyClient::m_pCharacter," key_highest_score_musicid != 0 order by characters.key_highest_score desc limit 0,10000",&record, true);
		if( count <= 0)
		{
			Global::logger->debug("[key_self_score] load eerror  less than one");
		}
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );			
		iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
		MODI_RDB2S_Notify_LoadSelfRanksRes * res_hp_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
		res_hp_self->type = kRank_KeyScore;
		res_hp_self->size = 0;
		AutoConstruct( res_hp_self );
		MODI_CHARID  * id_array  = (MODI_CHARID *)res_hp_self->data;
		for( int i=0;  i< count; ++i )
		{
			MODI_Record * tmp = container.GetRecord(i);
			if( tmp)
			{
				const MODI_VarType & v_charid = tmp->GetValue("guid");
				res_hp_self->size ++;
				*id_array = v_charid.ToUInt();
				id_array++;
			}
		}
		iSendSize += (sizeof( MODI_CHARID)*res_hp_self->size);
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_hp_self , iSendSize );


	}
	/// 键盘精度榜
	{
		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_Record   record2;
		MODI_RecordContainer  container2;
		record2.Put("guid");
		record2.Put("name");
		record2.Put("chenghao");
		record2.Put("key_highest_precision");
		record2.Put("key_highest_precision_musicid");
		size_t iSendSize = sizeof(MODI_RDB2S_Notify_LoadRanksRes)+sizeof(MODI_DBRank_HighestPrecision_List);
		int count=pDBClient->ExecSelect( container2,MODI_DBProxyClient::m_pCharacter," 1=1 order by characters.key_highest_precision desc limit 0,100",&record2,true);
		if( count <= 0)
		{
			Global::logger->debug("[key_precise_list] can't get any record!");
		}
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_RDB2S_Notify_LoadRanksRes * res_keypres = (MODI_RDB2S_Notify_LoadRanksRes *)(szPackageBuf);
		AutoConstruct(res_keypres);
		MODI_DBRank_HighestPrecision_List * key_pre = (MODI_DBRank_HighestPrecision_List *)(res_keypres->data);
		res_keypres->type = kRank_KeyPrecision ;
		res_keypres->version = pReq->version;
		key_pre->size = 0;

		for(int i=0; i< count; ++i )
		{
			
			MODI_Record * temp = container2.GetRecord(i);
			if( temp)
			{
				const MODI_VarType  & v_guid= temp->GetValue("guid");
				const MODI_VarType  & v_name = temp->GetValue("name");
				const MODI_VarType  & v_chenghao = temp->GetValue("chenghao");
				const MODI_VarType   & v_precise = temp->GetValue("key_highest_precision");
				const MODI_VarType  & v_musid = temp->GetValue("key_highest_precision_musicid");
				
				key_pre->ranks[i].charid = v_guid.ToUInt();
				key_pre->ranks[i].highest_precision = v_precise.ToFloat();
				key_pre->ranks[i].musicid = v_musid.ToUInt();
				strncpy(key_pre->ranks[i].role_name,(const char *)v_name,sizeof(key_pre->ranks[i].role_name));
				key_pre->ranks[i].title = v_chenghao.ToShort();
				key_pre->size ++;
			}

		}
		
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_keypres, iSendSize );

		///加载个人的信息
		
		record2.Reset();
		container2.Clear();
		record2.Put("guid");
		count = pDBClient->ExecSelect( container2,MODI_DBProxyClient::m_pCharacter," 1=1 order by characters.key_highest_precision desc limit 0,10000",&record2, true);
		if( count <= 0)
		{
			Global::logger->debug("[key_self_score] load eerror  less than one");
		}
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );			
		iSendSize = sizeof(MODI_RDB2S_Notify_LoadSelfRanksRes);
		MODI_RDB2S_Notify_LoadSelfRanksRes * res_hp_self = (MODI_RDB2S_Notify_LoadSelfRanksRes *)(szPackageBuf);
		res_hp_self->type = kRank_KeyPrecision;
		res_hp_self->size = 0;
		AutoConstruct( res_hp_self );
		MODI_CHARID  * id_array  = (MODI_CHARID *)res_hp_self->data;
		for( int i=0;  i< count; ++i )
		{
			MODI_Record * tmp = container2.GetRecord(i);
			if( tmp)
			{
				const MODI_VarType & v_charid = tmp->GetValue("guid");
				res_hp_self->size ++;
				*id_array = v_charid.ToUInt();
				id_array++;
			}
		}
		iSendSize += (sizeof( MODI_CHARID)*res_hp_self->size);
		MODI_DBProxyClient::GetInstancePtr()->SendPackage( res_hp_self , iSendSize );
	}

	return enPHandler_OK;
}

/// 0点清除某些数据库的标志
int MODI_PackageHandler_rdb::ClearZero_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ClearZero * p_recv_cmd = (const MODI_S2RDB_Request_ClearZero *)(pt_null_cmd);
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in create role");
		return enPHandler_OK;
	}
	
	/// 0点所有的用户送钱标志清0
	if(p_recv_cmd->m_byState == 1)
	{
		/// 1.送钱标识清除
		std::ostringstream sql;
		sql<<"update character_other set login_money=1;";
		p_dbclient->ExecSql(sql.str().c_str(), sql.str().size());

		/// 2.导出音了乐点击率
		MODI_RecordContainer record_select;
		sql.str("");
		sql << "update music_count set old_count=old_count+now_count;";
		p_dbclient->ExecSql(sql.str().c_str(), sql.str().size());
		
		/// 增加天记录
		MODI_TTime m_stCurTime;
		std::ostringstream now_time;
		WORD mon = m_stCurTime.GetMon();
		WORD day = m_stCurTime.GetMDay();
		if(mon < 10)
		{
			now_time<< m_stCurTime.GetYear() << "-0" <<m_stCurTime.GetMon();
		}
		else
		{
			now_time<< m_stCurTime.GetYear() << "-" <<m_stCurTime.GetMon();
		}
		if(day < 10)
		{
			now_time<<"-0" <<day;
		}
		else
		{
			now_time<<"-" <<day;
		}
			
		sql.str("");
		sql << "select now_count,musicid from music_count;";
	   	int rec_count = p_dbclient->ExecSelect(record_select, sql.str().c_str(), sql.str().size());
		for(int i = 0; i < rec_count; i++)
		{
			MODI_Record * p_select_record = record_select.GetRecord(i);
			if(p_select_record == NULL)
			{
				continue;
			}

			const MODI_VarType  & v_id = p_select_record->GetValue("musicid");
			const MODI_VarType  & v_count = p_select_record->GetValue("now_count");
			if(v_id.Empty() || v_count.Empty())
			{
				continue;
			}

			WORD music_id = (WORD)v_id;
			DWORD now_count = (DWORD)v_count;

			if(now_count > 0)
			{
				sql.str("");
				sql<< "insert into day_music_count value(NULL,"<<music_id<<","<<"\'"<<now_time.str()<<"\'"<<","<<now_count<<");";
				p_dbclient->ExecSql(sql.str().c_str(), sql.str().size());
			}
		}

		sql.str("");
		/// 清除当天的点击
		sql << "update music_count set now_count=0;";
		p_dbclient->ExecSql(sql.str().c_str(), sql.str().size());
	}
// 	/// 可否要加钱
// 	else if(p_recv_cmd->m_byState == 2)
// 	{
// 		MODI_RecordContainer record_select;
// 		std::ostringstream where;
// 		where << "accountid=\'" << p_recv_cmd->m_dwAccountID << "\'";
// 		MODI_Record select_field;
// 		select_field.Put("login_money");

// 		if((p_dbclient->ExecSelect(record_select, MODI_DBProxyClient::m_pCharacterOther, where.str().c_str(), &select_field)) != 1)
// 		{
// 			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
// 			return enPHandler_OK;
// 		}
// 		MODI_Record * pRecord = record_select.GetRecord(0);
// 		const MODI_VarType & v_money = pRecord->GetValue("login_money");
// 		WORD is_send = (WORD)v_money;
// 		if(is_send)
// 		{
// 			const_cast<MODI_S2RDB_Request_ClearZero *>(p_recv_cmd)->m_byState = 3;
// 			MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_recv_cmd, sizeof(MODI_S2RDB_Request_ClearZero));
// 		}
// 	}
// 	/// 加钱
// 	else if(p_recv_cmd->m_byState == 4)
// 	{
// 		MODI_RecordContainer record_select;
// 		std::ostringstream where;
// 		where << "accountid=\'" << p_recv_cmd->m_dwAccountID << "\'";
// 		MODI_Record select_field;
// 		select_field.Put("moneyRMB");

// 		if((p_dbclient->ExecSelect(record_select, MODI_DBProxyClient::m_pCharacter, where.str().c_str(), &select_field)) != 1)
// 		{
// 			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
// 			return enPHandler_OK;
// 		}

// 		MODI_Record * p_select_record = record_select.GetRecord(0);
// 		if(p_select_record == NULL)
// 		{
// 			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
// 			return enPHandler_OK;
// 		}
		
// 		MODI_VarType v_moneyRMB = p_select_record->GetValue("moneyRMB");
// 		if(v_moneyRMB.Empty())
// 		{
// 			Global::logger->fatal("[song_money] updata money failed<accid=%u>", p_recv_cmd->m_dwAccountID);
// 			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
// 			return enPHandler_OK;
// 		}
		
// 		DWORD money_rmb = (DWORD)v_moneyRMB;

// 		MODI_Record record_update;
// 		DWORD m_dwLastMoney = money_rmb + 100;
// 		record_update.Put("moneyRMB", m_dwLastMoney);
// 		record_update.Put("where", where.str());
// 		if(p_dbclient->ExecUpdate(MODI_DBProxyClient::m_pCharacter, &record_update) != 1)
// 		{
// 			Global::logger->fatal("[song_money] updata money failed<accid=%u>", p_recv_cmd->m_dwAccountID);
// 			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
// 			return enPHandler_OK;
// 		}
// 		const_cast<MODI_S2RDB_Request_ClearZero *>(p_recv_cmd)->m_byState = 5;
// 		const_cast<MODI_S2RDB_Request_ClearZero *>(p_recv_cmd)->m_dwMoney = m_dwLastMoney;
// 		MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_recv_cmd, sizeof(MODI_S2RDB_Request_ClearZero));
// 		/// 清标志
// 		std::ostringstream sql;
// 		sql <<"update character_other set login_money=0 where accountid=\'" << p_recv_cmd->m_dwAccountID << "\'";
// 		p_dbclient->ExecSql(sql.str().c_str(), sql.str().size());
// 	}
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	return enPHandler_OK;
}


/// 音乐点击率统计
int MODI_PackageHandler_rdb::MusicCount_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
											   const unsigned int cmd_size )
{
	const MODI_S2RDB_Notify_MusicCount * p_recv_cmd = (const MODI_S2RDB_Notify_MusicCount *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in create role");
		return enPHandler_OK;
	}
	/// 增加一个音乐点击
	if(p_recv_cmd->m_byState == 1)
	{
		MODI_RecordContainer record_select;
		std::ostringstream where;
		where << "select * from music_count where musicid="<<p_recv_cmd->m_wdMusicID;
		
		if((p_dbclient->ExecSelect(record_select, where.str().c_str(), where.str().size())) != 1)
		{
			/// 新歌没有记录
			where.str("");
			where << "insert into music_count values("<<p_recv_cmd->m_wdMusicID<< ",default,1);";
			p_dbclient->ExecSql(where.str().c_str(), where.str().size());
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			return enPHandler_OK;
		}
		/// 有记录，直接加
		where.str("");
		where << "update music_count set now_count=now_count+1 where musicid=" << p_recv_cmd->m_wdMusicID << ";";
		p_dbclient->ExecSql(where.str().c_str(), where.str().size());
	}
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	return enPHandler_OK;
}


/** 
 * @brief 加载用户变量
 * 
 */
int MODI_PackageHandler_rdb::LoadUserVar_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
											   const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_LoadUserVar_Cmd * p_recv_cmd = (const MODI_S2RDB_Request_LoadUserVar_Cmd *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in load user var");
		return enPHandler_OK;
	}
	

	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("accountid");
	select_field.Put("name");
	select_field.Put("value");
	select_field.Put("type");
	select_field.Put("update_time");
	select_field.Put("add_time");
	select_field.Put("del_time");
	
	std::ostringstream where;
	where << "accountid=" << p_recv_cmd->m_dwAccountID;
	
	int rec_count = p_dbclient->ExecSelect(record_select, MODI_DBProxyClient::m_pUserVarTbl, where.str().c_str(), &select_field, true);
	
	MODI_ASSERT(rec_count < 16000);
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Return_LoadUserVar_Cmd * send_cmd = (MODI_RDB2S_Return_LoadUserVar_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
	send_cmd->m_byServerID = p_recv_cmd->m_byServerID;
	send_cmd->m_blIsLoadEnd = 0;
	
	MODI_VarData * p_startaddr = (MODI_VarData * )(&(send_cmd->m_pData[0]));
	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		const MODI_VarType & v_accountid = p_select_record->GetValue("accountid");
		const MODI_VarType & v_name = p_select_record->GetValue("name");
		const MODI_VarType & v_value = p_select_record->GetValue("value");
		const MODI_VarType & v_type = p_select_record->GetValue("type");
		const MODI_VarType & v_update_time = p_select_record->GetValue("update_time");
		const MODI_VarType & v_add_time = p_select_record->GetValue("add_time");
		const MODI_VarType & v_del_time = p_select_record->GetValue("del_time");
		
		if(v_accountid.Empty() || v_name.Empty() || v_value.Empty() || v_type.Empty() || v_update_time.Empty() || 
		   v_add_time.Empty() || v_del_time.Empty())
		{
			continue;
		}

		strncpy(p_startaddr->m_szName, (const char *)v_name, sizeof(p_startaddr->m_szName) - 1);
		p_startaddr->m_wdValue = (WORD)v_value;
		p_startaddr->m_byType = (BYTE)v_type;
		p_startaddr->m_dwUpdateTime = (DWORD)v_update_time;
		p_startaddr->m_dwAddTime = (DWORD)v_add_time;
		p_startaddr->m_dwDelTime = (DWORD)v_del_time;
		
		p_startaddr++;
		send_cmd->m_wdSize++;

		if( (sizeof(MODI_RDB2S_Return_LoadUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData)) > Skt::MAX_USERDATASIZE - (sizeof(MODI_VarData) + 128) )
		{
			if(i+1 == rec_count)
			{
				send_cmd->m_blIsLoadEnd = 1;
			}
			
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Return_LoadUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData));
			memset(buf, 0, sizeof(buf));
			AutoConstruct(send_cmd);
			send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
			send_cmd->m_byServerID = p_recv_cmd->m_byServerID;
			send_cmd->m_blIsLoadEnd = 0;
			send_cmd->m_wdSize = 0;
			p_startaddr = (MODI_VarData * )(&(send_cmd->m_pData[0]));
		}
		
	}
	//	if(send_cmd->m_wdSize > 0)
	//	{
		send_cmd->m_blIsLoadEnd = 1;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Return_LoadUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData));
		//	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	return enPHandler_OK;
}


/** 
 * @brief 请求执行sql
 *
 * @return 
 */
int MODI_PackageHandler_rdb::RequestExecSql_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
											   const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ExecSql_Cmd * p_recv_cmd = (const MODI_S2RDB_Request_ExecSql_Cmd *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in request exec sql");
		return enPHandler_OK;
	}

#ifdef _HRX_DEBUG
	Global::logger->debug("[request_exec_sql] %s", p_recv_cmd->m_Sql);
#endif
	
	p_dbclient->ExecSqlResult(p_recv_cmd->m_Sql, p_recv_cmd->m_wdSize);

	MODI_DBManager::GetInstance().PutHandle(p_dbclient);

	return enPHandler_OK;
}


/** 
 * @brief 请求执行事务
 * 
 */
int MODI_PackageHandler_rdb::RequestExecSqlTransaction_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
													const unsigned int cmd_size )
{
	/// 保证在一个数据库中(game_db)
	const MODI_S2RDB_Request_ExecSql_Transaction_Cmd * p_recv_cmd = (const MODI_S2RDB_Request_ExecSql_Transaction_Cmd *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in request exec sql");
		return enPHandler_OK;
	}

	p_dbclient->BeginTransaction();
	char get_sql[1024];
	memset(get_sql, 0, sizeof(get_sql));
	bool ret_code = true;
	WORD run_size = 0;
	
	for(BYTE i=0; i<p_recv_cmd->m_byCount; i++)
	{
		MODI_Transaction_Sql * p_sql = (MODI_Transaction_Sql *)((const char * )p_recv_cmd->m_Data + run_size);
		memset(get_sql, 0, sizeof(get_sql));
		memcpy(get_sql, p_sql->m_Sql, p_sql->m_wdSize);
#ifdef _HRX_DEBUG
		Global::logger->debug("[request_exec_sql_transaction] %s", get_sql);
#endif
		if(p_dbclient->ExecSqlResult(get_sql, p_sql->m_wdSize) == -1)
		{
			ret_code = false;
			break;
		}
		
		run_size += sizeof(MODI_Transaction_Sql) + p_sql->m_wdSize;
	}

	if(ret_code)
	{
		p_dbclient->CommitTransaction();
		
	}
	else
	{
		Global::logger->error("[request_exec_sql_transaction] failed");
		p_dbclient->RollbackTransaction();
	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);

	return enPHandler_OK;
}



/** 
 * @brief 加载zone用户变量
 * 
 */
int MODI_PackageHandler_rdb::ZoneUserVar_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
											   const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ZoneUserVar_Cmd * p_recv_cmd = (const MODI_S2RDB_Request_ZoneUserVar_Cmd *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in load user var");
		return enPHandler_OK;
	}
	

	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("accountid");
	select_field.Put("name");
	select_field.Put("value");
	select_field.Put("type");
	select_field.Put("update_time");
	select_field.Put("add_time");
	select_field.Put("del_time");
	
	std::ostringstream where;
	where << "accountid=" << p_recv_cmd->m_dwAccountID;
	
	int rec_count = p_dbclient->ExecSelect(record_select, MODI_DBProxyClient::m_pZoneUserVarTbl, where.str().c_str(), &select_field, true);
	
	MODI_ASSERT(rec_count < 16000);
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Return_ZoneUserVar_Cmd * send_cmd = (MODI_RDB2S_Return_ZoneUserVar_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
	
	MODI_VarData * p_startaddr = (MODI_VarData * )(&(send_cmd->m_pData[0]));
	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		const MODI_VarType  & v_accountid = p_select_record->GetValue("accountid");
		const MODI_VarType & v_name = p_select_record->GetValue("name");
		const MODI_VarType & v_value = p_select_record->GetValue("value");
		const MODI_VarType & v_type = p_select_record->GetValue("type");
		const MODI_VarType & v_update_time = p_select_record->GetValue("update_time");
		const MODI_VarType & v_add_time = p_select_record->GetValue("add_time");
		const MODI_VarType & v_del_time = p_select_record->GetValue("del_time");
		
		if(v_accountid.Empty() || v_name.Empty() || v_value.Empty() || v_type.Empty() || v_update_time.Empty() || 
		   v_add_time.Empty() || v_del_time.Empty())
		{
			continue;
		}

		strncpy(p_startaddr->m_szName, (const char *)v_name, sizeof(p_startaddr->m_szName) - 1);
		p_startaddr->m_wdValue = (WORD)v_value;
		p_startaddr->m_byType = (BYTE)v_type;
		p_startaddr->m_dwUpdateTime = (DWORD)v_update_time;
		p_startaddr->m_dwAddTime = (DWORD)v_add_time;
		p_startaddr->m_dwDelTime = (DWORD)v_del_time;
		
		p_startaddr++;
		send_cmd->m_wdSize++;
		if( (sizeof(MODI_RDB2S_Return_ZoneUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData)) >
			(Skt::MAX_USERDATASIZE - (sizeof(MODI_VarData) * send_cmd->m_wdSize+128)))
		{
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Return_ZoneUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData));
			memset(buf, 0, sizeof(buf));
			AutoConstruct(send_cmd);
			send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
			send_cmd->m_wdSize = 0;
			p_startaddr = (MODI_VarData * )(&(send_cmd->m_pData[0]));
		}
	}

	if(send_cmd->m_wdSize > 0)
	{
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Return_ZoneUserVar_Cmd) + send_cmd->m_wdSize * sizeof(MODI_VarData));
	}
	
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	return enPHandler_OK;
}


/** 
 * @brief 加载用户包裹
 * 
 */
int MODI_PackageHandler_rdb::RequestItemInfo_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ItemInfo * p_recv_cmd = (const MODI_S2RDB_Request_ItemInfo *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{

		Global::logger->fatal("not get db handle in load item info");
		return enPHandler_OK;
	}
	

	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("itemid");
	select_field.Put("configid");
	select_field.Put("bag_type");
	select_field.Put("bag_pos");
	select_field.Put("UNIX_TIMESTAMP(`expire_time`)");
	select_field.Put("create_reason");
	select_field.Put("server_id");
	
	std::ostringstream where;
	where << "accid=" << p_recv_cmd->m_dwAccountID <<" and is_valid=1";
	
	int rec_count = p_dbclient->ExecSelect(record_select, GlobalDB::m_pItemInfoTbl, where.str().c_str(), &select_field, true);
	
	//MODI_ASSERT(rec_count < 16000);
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Notify_ItemInfo * send_cmd = (MODI_RDB2S_Notify_ItemInfo *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
	send_cmd->m_byServerID = p_recv_cmd->m_byServerID;
	
	MODI_DBItemInfo * p_startaddr = (MODI_DBItemInfo * )(&(send_cmd->m_pData[0]));
	
	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			MODI_ASSERT(0);
			continue;
		}

		const MODI_VarType &  v_itemid = p_select_record->GetValue("itemid");
		const MODI_VarType  & v_configid = p_select_record->GetValue("configid");
		const MODI_VarType & v_bag_type = p_select_record->GetValue("bag_type");
		const MODI_VarType & v_bag_pos = p_select_record->GetValue("bag_pos");
		const MODI_VarType & v_expire_time = p_select_record->GetValue("UNIX_TIMESTAMP(`expire_time`)");
		const MODI_VarType & v_create_reason = p_select_record->GetValue("create_reason");
		const MODI_VarType & v_server_id = p_select_record->GetValue("server_id");
		
		if(v_itemid.Empty() || v_configid.Empty() || v_bag_type.Empty() || v_bag_pos.Empty() || v_expire_time.Empty() ||
		   v_create_reason.Empty() || v_server_id.Empty())
		{
			MODI_ASSERT(0);
			continue;
		}

		p_startaddr->m_qdItemId = (QWORD)v_itemid;
		p_startaddr->m_dwConfigId = (DWORD)v_configid;
		p_startaddr->m_qdExpireTime = (QWORD)v_expire_time;
		p_startaddr->m_wdBagPos = (WORD)v_bag_pos;
		p_startaddr->m_byBagType = (BYTE)v_bag_type;
		p_startaddr->m_byCreateReason = (BYTE)v_create_reason;
		p_startaddr->m_byServerId = (BYTE)v_server_id;
		
		p_startaddr++;
		send_cmd->m_wdSize++;
		if(sizeof(MODI_RDB2S_Notify_ItemInfo) + send_cmd->m_wdSize * sizeof(MODI_DBItemInfo) > (Skt::MAX_USERDATASIZE - (sizeof(MODI_DBItemInfo) + 128)))
		{
			if(i+1 == rec_count)
			{
				send_cmd->m_byIsLoadEnd = 1;
			}
			
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Notify_ItemInfo) + send_cmd->m_wdSize * sizeof(MODI_DBItemInfo));
			memset(buf, 0, sizeof(buf));
			AutoConstruct(send_cmd);
			send_cmd->m_dwAccountID = p_recv_cmd->m_dwAccountID;
			send_cmd->m_byServerID = p_recv_cmd->m_byServerID;
			p_startaddr = (MODI_DBItemInfo * )(&(send_cmd->m_pData[0]));
			send_cmd->m_wdSize = 0;
		}
	}

	if(send_cmd->m_wdSize > 0)
	{
		send_cmd->m_byIsLoadEnd = 1;
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(send_cmd, sizeof(MODI_RDB2S_Notify_ItemInfo) + send_cmd->m_wdSize * sizeof(MODI_DBItemInfo));
	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	return enPHandler_OK;
}


/** 
 * @brief 加载用户角色列表
 * 
 * @param char_info 角色信息
 * 
 * @return 1 有角色， 0没有角色， -1出错
 *
 */
int MODI_PackageHandler_rdb::LoadCharList(const defAccountID & acc_id,  MODI_CharInfo & char_info)
{
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	if(!p_db_client)
	{
		return -1;
	}
	MODI_ScopeHandlerRelease lock(p_db_client);
	memset(&char_info, 0, sizeof(char_info));

	/// 加载角色信息
	MODI_RecordContainer character_container;
	MODI_Record character_record;
	character_record.Put("guid");
	character_record.Put("name");
	character_record.Put("level");
	character_record.Put("sex");
	character_record.Put("defavatar");
	character_record.Put("readhelp");
	character_record.Put("gmlevel");
	character_record.Put("vip");

	std::ostringstream os;
	os<< "accountid="  << acc_id;

	int char_num = 0;
	if((char_num = p_db_client->ExecSelect(character_container, GlobalDB::m_pCharacterTbl, os.str().c_str(), & character_record)) != 1)
	{
		return char_num;
	}

	MODI_Record * pRecord = character_container.GetRecord(0);
	if(! pRecord)
	{
		MODI_ASSERT(0);
		return -1;
	}

	const MODI_VarType & v_charid = pRecord->GetValue("guid");
	const MODI_VarType & v_rolename = pRecord->GetValue("name");
	const MODI_VarType & v_level = pRecord->GetValue("level");
    const MODI_VarType & v_sex = pRecord->GetValue("sex");
    const MODI_VarType & v_defavatar = pRecord->GetValue("defavatar");
    const MODI_VarType & v_readhelp = pRecord->GetValue("readhelp");
    const MODI_VarType & v_gmlevel = pRecord->GetValue("gmlevel");
	const MODI_VarType & v_vip = pRecord->GetValue("vip");

    char_info.m_nCharID = v_charid.ToUInt();
    char_info.m_byLevel = v_level.ToUChar();
    char_info.m_bySex = v_sex.ToUChar();
    char_info.m_nDefaultAvatarIdx = v_defavatar.ToUShort();
    strncpy(char_info.m_szRoleName , v_rolename.ToStr() , sizeof(char_info.m_szRoleName) - 1);
    char_info.m_byReadHelp = v_readhelp.ToUChar();
	char_info.m_byVip = v_vip.ToUChar();

    int gm_level = v_gmlevel.ToInt();
    if(gm_level == 200)
	{
		Global::logger->info("[frost_acc] frost acc by gm <name=%s>", char_info.m_szRoleName);
		return -1;
	}

	/// 加载avatar信息
	if(LoadCharAvatar(acc_id, char_info.m_AvatarInfo) == -1)
	{
		return -1;
	}
	
	return 1;
}
	

/** 
 * @brief 获取玩家的装备信息
 * 
 * @param acc_id 玩家的acc_id
 * @param avatar_info 玩家的装备信息
 * 
 * @return -1为系统错误
 *
 */
int MODI_PackageHandler_rdb::LoadCharAvatar(const defAccountID & acc_id, MODI_ClientAvatarData & avatar_info)
{
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	if(!p_db_client)
	{
		return -1;
	}
	MODI_ScopeHandlerRelease lock(p_db_client);
	
	MODI_RecordContainer item_container;
	std::ostringstream item_sql;
	item_sql << "select * from "<< GlobalDB::m_pItemInfoTbl->GetName() << " where accid=" <<acc_id << " and bag_type=1 and is_valid=1";
	int item_num = p_db_client->ExecSelect(item_container, item_sql.str().c_str(), item_sql.str().size());
	if(item_num == 0)
	{
		MODI_ASSERT(0);
		return -1;
	}
	
	for(int i=0; i<item_num; i++)
	{
		MODI_Record * item_record = item_container.GetRecord(i);
		if(! item_record)
		{
			MODI_ASSERT(0);
			return -1;
		}
		const MODI_VarType & v_configid = item_record->GetValue("configid");
		const MODI_VarType & v_bagpos = item_record->GetValue("bag_pos");

		DWORD config_id = (DWORD)v_configid;
		WORD bag_pos = (WORD)v_bagpos;

		if(bag_pos == INVAILD_POS_INBAG || config_id == INVAILD_CONFIGID)
		{
			Global::logger->error("[load_char_list] load char list failed <record_num=%u>", i);
			MODI_ASSERT(0);
		   	return -1;
		}

		if(avatar_info.m_Avatars[bag_pos -1] != INVAILD_CONFIGID)
		{
			Global::logger->error("[load_char_list] load char list failed <record_num=%u>", i);
			MODI_ASSERT(0);
			return -1;
		}
		
		avatar_info.m_Avatars[bag_pos - 1] = config_id;
	}
	return 1;
}


/// 减钱
int MODI_PackageHandler_rdb::DecMoney_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_dbclient);
	MODI_S2RDB_DecMoney_Cmd * p_recv_cmd = (MODI_S2RDB_DecMoney_Cmd *)pt_null_cmd;
	p_dbclient->BeginTransaction();
	bool ret_code = true;
	DWORD m_dwLastMoney = 0;
	DWORD money_rmb = 0;
	DWORD money = 0;
	DWORD consume_rmb = 0;
	std::string acc_name;
	do
	{
		/// second
		MODI_RecordContainer record_select;
		std::ostringstream where;
		where << "accountid=" << p_recv_cmd->m_dwAccountID;
		MODI_Record select_field;
		select_field.Put("moneyRMB");
		select_field.Put("money");
		select_field.Put("consume_rmb");
		select_field.Put("accname");

		if((p_dbclient->ExecSelect(record_select, GlobalDB::m_pCharacterTbl, where.str().c_str(), &select_field)) != 1)
		{
			Global::logger->fatal("[dec_money] read money form character faield <accid=%u,decmone=%u>",p_recv_cmd->m_dwAccountID,
								  p_recv_cmd->m_dwMoney);
			ret_code = false;
			break;
		}

		MODI_Record * p_select_record = record_select.GetRecord(0);
		if(p_select_record == NULL)
		{
			ret_code = false;
			break;
		}
		
		const MODI_VarType v_moneyRMB = p_select_record->GetValue("moneyRMB");
		if(v_moneyRMB.Empty())
		{
			ret_code = false;
			break;
		}

		const MODI_VarType v_money = p_select_record->GetValue("money");
		if(v_money.Empty())
		{
			ret_code = false;
			break;
		}

		const MODI_VarType v_accname = p_select_record->GetValue("accname");
		if(v_accname.Empty())
		{
			ret_code = false;
			break;
		}

		const MODI_VarType v_consume_rmb = p_select_record->GetValue("consume_rmb");
		if(v_consume_rmb.Empty())
		{
			ret_code = false;
			break;
		}

		consume_rmb = (DWORD)v_consume_rmb;
		money_rmb = (DWORD)v_moneyRMB;
		money = (DWORD)v_money;
		acc_name = (const char *)v_accname;

		/// 金币
		if(p_recv_cmd->m_byMoneyType == kGameMoney)
		{
			if(p_recv_cmd->m_dwMoney > money)
			{
				Global::logger->fatal("[assert_dec_money] dec money but money not enought <decmoney=%u,havemoney=%u,accid=%u>",
									  p_recv_cmd->m_dwMoney, money, p_recv_cmd->m_dwAccountID);
				MODI_ASSERT(0);
				m_dwLastMoney = 0;
			}
			else
			{
				m_dwLastMoney = money_rmb - p_recv_cmd->m_dwMoney;
				Global::logger->debug("[dec_money] dec money <name=%s,havemone=%u,decmoney=%u,lastmoney=%u>",
									  acc_name.c_str(), money, p_recv_cmd->m_dwMoney, m_dwLastMoney);
			}

			MODI_Record record_update;
			record_update.Put("money", m_dwLastMoney);
			record_update.Put("where", where.str());
		
			if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update) != 1)
			{
				Global::logger->fatal("[dec_money] updata money failed<accname=%s,lastmoney=%u,havemoney=%u>", acc_name.c_str(),
									  m_dwLastMoney, money);
				ret_code = false;
				break;
			}		
		}
		/// 人民币
		else if(p_recv_cmd->m_byMoneyType == kRMBMoney)
		{
			if(p_recv_cmd->m_dwMoney > money_rmb)
			{
				Global::logger->fatal("[assert_dec_money_rmb] dec moneyrmb but moneyrmb not enought <decmoney=%u,havemoney=%u,accid=%u>",
									  p_recv_cmd->m_dwMoney, money_rmb, p_recv_cmd->m_dwAccountID);
				MODI_ASSERT(0);
				m_dwLastMoney = 0;
				
			}
			else
			{
				m_dwLastMoney = money_rmb - p_recv_cmd->m_dwMoney;
				Global::logger->debug("[dec_money_rmb] dec money rmb <name=%s,havemone=%u,decmone=%u,lastmone=%u>",
									  acc_name.c_str(), money_rmb, p_recv_cmd->m_dwMoney, m_dwLastMoney);
			}

			MODI_Record record_update;
			DWORD en_size = 0;
			char buf[255];
			memset(buf, 0, sizeof(buf));
			PublicFun::EncryptMoney(buf, en_size, m_dwLastMoney, acc_name.c_str());
		
			record_update.Put("moneyRMB", m_dwLastMoney);
			record_update.Put("consume_rmb", consume_rmb+p_recv_cmd->m_dwMoney);
			record_update.Put("otherdata", buf, en_size);
			record_update.Put("where", where.str());
		
			if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update) != 1)
			{
				Global::logger->fatal("[dec_money_rmb] updata money rmb failed<accname=%s,lastmoney=%u,havemoney=%u>", acc_name.c_str(),
									  m_dwLastMoney, money_rmb);
				ret_code = false;
				break;
			}		
		}
		else
		{
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}
	}
	while(0);

	if(ret_code)
	{
		p_dbclient->CommitTransaction();
		p_recv_cmd->m_dwMoney = m_dwLastMoney;
		
		if(p_recv_cmd->m_byMoneyType == kGameMoney)
		{
			Global::logger->debug("[dec_money_successful] <accname=%s,accid=%u,havemoney=%u,decmoney=%u,lastmoney=%u>",
								  acc_name.c_str(), p_recv_cmd->m_dwAccountID, money, p_recv_cmd->m_dwMoney, m_dwLastMoney);
		}
		else if(p_recv_cmd->m_byMoneyType == kRMBMoney)
		{
			Global::logger->debug("[dec_money_rmb_successful] <accname=%s,accid=%u,havemoneyrmb=%u,decmoneyrmb=%u,lastmoneyrmb=%u>",
								  acc_name.c_str(), p_recv_cmd->m_dwAccountID, money_rmb, p_recv_cmd->m_dwMoney, m_dwLastMoney);
			/// 通知钱		
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_recv_cmd, sizeof(*p_recv_cmd));
		}
		
		return true;
	}
	else
	{
		p_dbclient->RollbackTransaction();
		if(p_recv_cmd->m_byMoneyType == kGameMoney)
		{
			Global::logger->debug("[dec_money_failed] <accid=%u,havemoney=%u,decmoney=%u,lastmoney=%u>",
								  p_recv_cmd->m_dwAccountID, money, p_recv_cmd->m_dwMoney, m_dwLastMoney);
		}
		else if(p_recv_cmd->m_byMoneyType == kRMBMoney)
		{
			Global::logger->debug("[dec_money_rmb_failed] <accid=%u,havemoneyrmb=%u,decmoneyrmb=%u,lastmoneyrmb=%u>",
								  p_recv_cmd->m_dwAccountID, money_rmb, p_recv_cmd->m_dwMoney, m_dwLastMoney);
		}
		return false;
	}
}


/// 购买物品
int MODI_PackageHandler_rdb::BuyItem_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_dbclient);
	MODI_S2RDB_BuyItem_Cmd * p_recv_cmd = (MODI_S2RDB_BuyItem_Cmd *)pt_null_cmd;
	p_dbclient->BeginTransaction();
	bool ret_code = true;
	DWORD m_dwLastMoney = 0;
	DWORD money_rmb = 0;
	DWORD money = 0;
	DWORD consume_rmb = 0;
	std::string acc_name;
	do
	{
		/// 网页购买置为无效，商场购买扣钱
		if(p_recv_cmd->m_qdRecordSerial == 0)
		{
			/// first 扣钱
			MODI_RecordContainer record_select;
			std::ostringstream where;
			where << "accountid=" << p_recv_cmd->m_dwAccountID;
			MODI_Record select_field;
			select_field.Put("moneyRMB");
			select_field.Put("money");
			select_field.Put("consume_rmb");
			select_field.Put("accname");

			if((p_dbclient->ExecSelect(record_select, GlobalDB::m_pCharacterTbl, where.str().c_str(), &select_field)) != 1)
			{
				Global::logger->fatal("[buy_item] read money form character faield <accid=%u,buy_serial=%llu>",p_recv_cmd->m_dwAccountID,
									  p_recv_cmd->m_qdBuySerial);
				MODI_ASSERT(0);
				ret_code = false;
				break;
			}

			MODI_Record * p_select_record = record_select.GetRecord(0);
			if(p_select_record == NULL)
			{
				ret_code = false;
				break;
			}
		
			const MODI_VarType v_moneyRMB = p_select_record->GetValue("moneyRMB");
			if(v_moneyRMB.Empty())
			{
				ret_code = false;
				break;
			}

			const MODI_VarType v_money = p_select_record->GetValue("money");
			if(v_money.Empty())
			{
				ret_code = false;
				break;
			}

			const MODI_VarType v_accname = p_select_record->GetValue("accname");
			if(v_accname.Empty())
			{
				ret_code = false;
				break;
			}

			const MODI_VarType v_consume_rmb = p_select_record->GetValue("consume_rmb");
			if(v_consume_rmb.Empty())
			{
				ret_code = false;
				break;
			}
		
			money_rmb = (DWORD)v_moneyRMB;
			money = (DWORD)v_money;
			acc_name = (const char *)v_accname;
			consume_rmb = (DWORD)v_consume_rmb;

			/// 金币
			if(p_recv_cmd->m_byMoneyType == kGameMoney)
			{
				if(p_recv_cmd->m_dwMoney > money)
				{
					Global::logger->fatal("[assert_buy_item] dec money but money not enought <decmoney=%u,havemoney=%u,accid=%u,buy_serial=%llu>",
										  p_recv_cmd->m_dwMoney, money, p_recv_cmd->m_dwAccountID, p_recv_cmd->m_qdBuySerial);
					//MODI_ASSERT(0);
					ret_code = false;
					break;
				}
				else
				{
					m_dwLastMoney = money - p_recv_cmd->m_dwMoney;
					Global::logger->debug("[buy_item] dec money <name=%s,havemone=%u,decmoney=%u,lastmoney=%u,buy_serial=%llu>",
										  acc_name.c_str(), money, p_recv_cmd->m_dwMoney, m_dwLastMoney,p_recv_cmd->m_qdBuySerial);
				}

				MODI_Record record_update;
				record_update.Put("money", m_dwLastMoney);
				record_update.Put("where", where.str());
		
				if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update) != 1)
				{
					Global::logger->fatal("[buy_item] updata money failed<accname=%s,lastmoney=%u,havemoney=%u,buy_serial=%llu>", acc_name.c_str(),
										  m_dwLastMoney, money, p_recv_cmd->m_qdBuySerial);
					ret_code = false;
					break;
				}		
			}
			/// 人民币
			else if(p_recv_cmd->m_byMoneyType == kRMBMoney)
			{
				if(p_recv_cmd->m_dwMoney > money_rmb)
				{
					Global::logger->fatal("[assert_buy_item] dec moneyrmb but moneyrmb not enought <decmoney=%u,havemoney=%u,accid=%u,buy_serial=%llu>",
										  p_recv_cmd->m_dwMoney, money_rmb, p_recv_cmd->m_dwAccountID,p_recv_cmd->m_qdBuySerial);
					//MODI_ASSERT(0);
					ret_code = false;
					break;
					
				}
				else
				{
					m_dwLastMoney = money_rmb - p_recv_cmd->m_dwMoney;
					Global::logger->debug("[buy_item] dec money rmb <name=%s,havemone=%u,decmone=%u,lastmone=%u,buy_serial=%llu>",
										  acc_name.c_str(), money_rmb, p_recv_cmd->m_dwMoney, m_dwLastMoney,p_recv_cmd->m_qdBuySerial);
				}

				MODI_Record record_update;
				DWORD en_size = 0;
				char buf[255];
				memset(buf, 0, sizeof(buf));
				PublicFun::EncryptMoney(buf, en_size, m_dwLastMoney, acc_name.c_str());
		
				record_update.Put("moneyRMB", m_dwLastMoney);
				record_update.Put("consume_rmb", consume_rmb+p_recv_cmd->m_dwMoney);
				record_update.Put("otherdata", buf, en_size);
				record_update.Put("where", where.str());
		
				if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update) != 1)
				{
					Global::logger->fatal("[assert_buy_item] updata money rmb failed<accname=%s,lastmoney=%u,havemoney=%u,buy_serial=%llu>",
										  acc_name.c_str(),
										  m_dwLastMoney, money_rmb, p_recv_cmd->m_qdBuySerial);
					MODI_ASSERT(0);
					ret_code = false;
					break;
				}
			}
			else
			{
				MODI_ASSERT(0);
				ret_code = false;
				break;
			}
		}
		else
		{
			/// 直接把网页道具表置为无效
			MODI_RecordContainer record_select;
			std::ostringstream where;
			where << "recordserial=" << p_recv_cmd->m_qdRecordSerial;
			MODI_Record select_field;
			select_field.Put("is_valid");

			if((p_dbclient->ExecSelect(record_select, GlobalDB::m_pWebItemTbl, where.str().c_str(), &select_field)) != 1)
			{
				Global::logger->fatal("[web_buy_item] read is_valid form trade_goods faield <accid=%u,record_serial=%llu>",p_recv_cmd->m_dwAccountID,
									  p_recv_cmd->m_qdRecordSerial);
				MODI_ASSERT(0);
				ret_code = false;
				break;
			}

			MODI_Record * p_select_record = record_select.GetRecord(0);
			if(p_select_record == NULL)
			{
				ret_code = false;
				break;
			}
		
			const MODI_VarType v_is_valid = p_select_record->GetValue("is_valid");
			if(v_is_valid.Empty())
			{
				ret_code = false;
				break;
			}

			BYTE get_is_valid = (BYTE)v_is_valid;
			MODI_Record record_update;
			record_update.Put("is_valid", get_is_valid+1);
			record_update.Put("where", where.str());
		
			if(p_dbclient->ExecUpdate(GlobalDB::m_pWebItemTbl, &record_update) != 1)
			{
				Global::logger->fatal("[web_item] web_item set is_vaild failed <recordserial=%llu>", p_recv_cmd->m_qdRecordSerial);
				ret_code = false;
				break;
			}		
		}
		
		/// second 加物品
		if(p_dbclient->ExecSqlResult(p_recv_cmd->m_strSaveItem, p_recv_cmd->m_wdSaveItemSize) == -1)
		{
			Global::logger->fatal("[buy_item] save item failed <accid=%u,buy_serial=%llu>", p_recv_cmd->m_dwAccountID, p_recv_cmd->m_qdBuySerial);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}

		/// 保存到购买记录
		if(p_dbclient->ExecSqlResult(p_recv_cmd->m_strBuyRecord, p_recv_cmd->m_wdRecordSize) == -1)
		{
			Global::logger->fatal("[buy_item] save item failed <accid=%u,buy_serial=%llu>", p_recv_cmd->m_dwAccountID, p_recv_cmd->m_qdBuySerial);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}
	}
	while(0);

	if(ret_code)
	{
		p_dbclient->CommitTransaction();
		
		if(p_recv_cmd->m_byMoneyType == kGameMoney)
		{
			Global::logger->debug("[buy_successful] <accname=%s,accid=%u,havemoney=%u,decmoney=%u,lastmoney=%u,buy_serial=%llu>",
								  acc_name.c_str(), p_recv_cmd->m_dwAccountID, money, p_recv_cmd->m_dwMoney, m_dwLastMoney,p_recv_cmd->m_qdBuySerial);
		}
		else if(p_recv_cmd->m_byMoneyType == kRMBMoney)
		{
			Global::logger->debug("[buy_successful] <accname=%s,accid=%u,havemoneyrmb=%u,decmoneyrmb=%u,lastmoneyrmb=%u,buy_serial=%llu>",
								  acc_name.c_str(), p_recv_cmd->m_dwAccountID, money_rmb, p_recv_cmd->m_dwMoney, m_dwLastMoney,
								  p_recv_cmd->m_qdBuySerial);
					
		}
		if(p_recv_cmd->m_qdRecordSerial == 0)
		{
			MODI_S2RDB_DecMoney_Cmd send_notify_money;
			send_notify_money.m_byMoneyType = p_recv_cmd->m_byMoneyType;
			send_notify_money.m_dwMoney = m_dwLastMoney;
			send_notify_money.m_dwAccountID = p_recv_cmd->m_dwAccountID;
			
			/// 通知钱		
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(&send_notify_money, sizeof(send_notify_money));
		}
		return true;
	}
	else
	{
		p_dbclient->RollbackTransaction();
		Global::logger->fatal("[buy_failed] <accname=%s,buy_serial=%llu>", acc_name.c_str(), p_recv_cmd->m_qdBuySerial);
		return false;
	}
}


///  家族升级
int MODI_PackageHandler_rdb::FamilyLevel_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_dbclient);
	const MODI_S2RDB_FamilyLevel_Cmd * p_recv_cmd = (const MODI_S2RDB_FamilyLevel_Cmd *)pt_null_cmd;
	p_dbclient->BeginTransaction();
	bool ret_code = true;
	DWORD m_dwLastMoney = 0;
	DWORD money_rmb = 0;
	std::string acc_name;
	std::ostringstream where;
	WORD level = 0;
	do
	{
		/// first 扣钱
		MODI_RecordContainer record_select_char;
		where.str("");
		where << "accountid=" << p_recv_cmd->m_dwAccountID;
		MODI_Record select_field_char;
		select_field_char.Put("moneyRMB");
		select_field_char.Put("accname");

		if((p_dbclient->ExecSelect(record_select_char, GlobalDB::m_pCharacterTbl, where.str().c_str(), &select_field_char)) != 1)
		{
			Global::logger->fatal("[family_level] read money form character faield <accid=%u>",p_recv_cmd->m_dwAccountID);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}

		MODI_Record * p_select_record_char = record_select_char.GetRecord(0);
		if(p_select_record_char == NULL)
		{
			ret_code = false;
			break;
		}
		
		const MODI_VarType v_moneyRMB = p_select_record_char->GetValue("moneyRMB");
		if(v_moneyRMB.Empty())
		{
			ret_code = false;
			break;
		}

		const MODI_VarType v_accname = p_select_record_char->GetValue("accname");
		if(v_accname.Empty())
		{
			ret_code = false;
			break;
		}
		
		money_rmb = (DWORD)v_moneyRMB;
		acc_name = (const char *)v_accname;

		if(nsFamilyLevel::LEVEL_MONEY > money_rmb)
		{
			Global::logger->fatal("[assert_family_level] dec moneyrmb but moneyrmb not enought <havemoney=%u,accid=%u>",
								  money_rmb, p_recv_cmd->m_dwAccountID);
			ret_code = false;
			break;
		}
		else
		{
			m_dwLastMoney = money_rmb - nsFamilyLevel::LEVEL_MONEY;
			Global::logger->debug("[family_level] dec money rmb <name=%s,havemone=%u,lastmone=%u>",
								  acc_name.c_str(), money_rmb, m_dwLastMoney);
		}

		MODI_Record record_update_money;
		DWORD en_size = 0;
		char buf[255];
		memset(buf, 0, sizeof(buf));
		PublicFun::EncryptMoney(buf, en_size, m_dwLastMoney, acc_name.c_str());
		
		record_update_money.Put("moneyRMB", m_dwLastMoney);
		record_update_money.Put("otherdata", buf, en_size);
		record_update_money.Put("where", where.str());
		
		if(p_dbclient->ExecUpdate(GlobalDB::m_pCharacterTbl, &record_update_money) != 1)
		{
			Global::logger->fatal("[assert_family_level] updata money rmb failed<accname=%s,lastmoney=%u,havemoney=%u>",
									  acc_name.c_str(), m_dwLastMoney, money_rmb);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}
		
		/// second 升级
		MODI_RecordContainer record_select_level;
		where.str("");
		//std::ostringstream where;
		where << "familyid=" << p_recv_cmd->m_dwFamilyID;
		MODI_Record select_field_level;
		select_field_level.Put("level");
	
		if((p_dbclient->ExecSelect(record_select_level, GlobalDB::m_pFamilyTbl, where.str().c_str(), &select_field_level)) != 1)
		{
			Global::logger->fatal("[family_level] read level from familys faield <familyid=%u>",p_recv_cmd->m_dwFamilyID);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}

		MODI_Record * p_select_record_level = record_select_level.GetRecord(0);
		if(p_select_record_level == NULL)
		{
			ret_code = false;
			break;
		}
		
		const MODI_VarType v_level = p_select_record_level->GetValue("level");
		if(v_level.Empty())
		{
			ret_code = false;
			break;
		}

		level = (WORD)v_level;
		if(level >= nsFamilyLevel::MAX_FAMILY_LEVEL)
		{
			Global::logger->fatal("[assert_family_level] inc level but level amx <level=%u,familyid=%u>",
								  level, p_recv_cmd->m_dwFamilyID);
			ret_code = false;
			break;
		}
		
		MODI_Record record_update_level;
		
		record_update_level.Put("level", level+1);
		record_update_level.Put("where", where.str());
		where.str("");
		where << "familyid=" << p_recv_cmd->m_dwFamilyID;
		if(p_dbclient->ExecUpdate(GlobalDB::m_pFamilyTbl, &record_update_level) != 1)
		{
			Global::logger->fatal("[assert_family_level] updata family level failed <familyid=%u,level=%u>", p_recv_cmd->m_dwFamilyID, level);
			MODI_ASSERT(0);
			ret_code = false;
			break;
		}
	}
	while(0);

	/// 告知结果
	MODI_RDB2S_Notify_FamilyLevel send_result;
	send_result.m_byResult = ret_code;
	send_result.m_dwFamilyID = p_recv_cmd->m_dwFamilyID;
	send_result.m_dwAccountID = p_recv_cmd->m_dwAccountID;
	MODI_DBProxyClient::GetInstancePtr()->SendPackage(&send_result, sizeof(send_result));

	if(ret_code)
	{
		p_dbclient->CommitTransaction();
		Global::logger->debug("[family_level_successful] <familyid=%u,accid=%u,level=%u>",p_recv_cmd->m_dwFamilyID, p_recv_cmd->m_dwAccountID, level+1);
		
		MODI_S2RDB_DecMoney_Cmd send_notify_money;
		send_notify_money.m_byMoneyType = kRMBMoney;
		send_notify_money.m_dwMoney = m_dwLastMoney;
		send_notify_money.m_dwAccountID = p_recv_cmd->m_dwAccountID;
			
		/// 通知钱		
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(&send_notify_money, sizeof(send_notify_money));
		
		return true;
	}
	else
	{
		p_dbclient->RollbackTransaction();
		Global::logger->debug("[family_level_failed] <familyid=%u,accid=%u,level=%u>",p_recv_cmd->m_dwFamilyID, p_recv_cmd->m_dwAccountID, level+1);
		return false;
	}
}


/// 获取网页道具
int MODI_PackageHandler_rdb::GetWebItem_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_S2RDB_Req_WebItem * p_recv_cmd = (const MODI_S2RDB_Req_WebItem *)pt_null_cmd;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{

		Global::logger->fatal("not get db handle in load item info");
		return enPHandler_OK;
	}
	MODI_ScopeHandlerRelease lock(p_dbclient);

	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("recordserial");
	select_field.Put("tradeserial");
	select_field.Put("action");
	select_field.Put("configid");
	select_field.Put("money");
	select_field.Put("goodtype");
	
	std::ostringstream where;
	where << "accid=" << p_recv_cmd->m_dwAccountId <<" and is_valid=0";
	
	int rec_count = p_dbclient->ExecSelect(record_select, GlobalDB::m_pWebItemTbl, where.str().c_str(), &select_field, true);

	if(rec_count == -1)
	{
		return enPHandler_OK;
	}

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Ret_WebItem * p_send_cmd = (MODI_RDB2S_Ret_WebItem *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_byServerId = p_recv_cmd->m_byServerId;
	p_send_cmd->m_dwAccountId = p_recv_cmd->m_dwAccountId;

	MODI_WebItemInfo * p_startaddr = (MODI_WebItemInfo *)p_send_cmd->m_pData;

	/// 这里不需要考虑拆包，不会一下买上千个道具
	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			MODI_ASSERT(0);
			continue;
		}

		const MODI_VarType &  v_recordserial = p_select_record->GetValue("recordserial");
		const MODI_VarType  & v_tradeserial = p_select_record->GetValue("tradeserial");
		const MODI_VarType & v_actoin = p_select_record->GetValue("action");
		const MODI_VarType & v_configid = p_select_record->GetValue("configid");
		const MODI_VarType & v_goodtype = p_select_record->GetValue("goodtype");
		const MODI_VarType & v_money = p_select_record->GetValue("money");
		
		if(v_recordserial.Empty() || v_tradeserial.Empty() || v_actoin.Empty() ||
		   v_configid.Empty() || v_goodtype.Empty() || v_money.Empty() )
		{
			MODI_ASSERT(0);
			continue;
		}

		p_startaddr->m_qdRecordSerial = (QWORD)v_recordserial;
		p_startaddr->m_qdBuySerial = (QWORD)v_tradeserial;
		p_startaddr->m_byAction = (BYTE)v_actoin;
		p_startaddr->m_dwMoney = (DWORD)v_money;
		p_startaddr->m_pConfigID = (DWORD)v_configid;
		p_startaddr->m_TimeStrategy = (enShopGoodTimeType)(BYTE)v_goodtype;
		
		p_startaddr++;
		p_send_cmd->m_wdSize++;
	}
	if(p_send_cmd->m_wdSize > 0)
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_cmd, sizeof(MODI_RDB2S_Ret_WebItem) + p_send_cmd->m_wdSize * sizeof(MODI_WebItemInfo));

	/// 获取赠送的道具
	MODI_RecordContainer record_select_present;
	MODI_Record select_field_present;
	select_field_present.Put("recordserial");
	select_field_present.Put("presentserial");
	select_field_present.Put("action");
	select_field_present.Put("itemid");
	select_field_present.Put("num");
	select_field_present.Put("goodtype");
	
	rec_count = p_dbclient->ExecSelect(record_select_present, GlobalDB::m_pWebPresentTbl, where.str().c_str(), &select_field_present, true);

	if(rec_count == -1)
	{
		return enPHandler_OK;
	}

	///char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_RDB2S_Ret_WebPresent * p_send_present = (MODI_RDB2S_Ret_WebPresent *)buf;
	AutoConstruct(p_send_present);

	p_send_present->m_byServerId = p_recv_cmd->m_byServerId;
	p_send_present->m_dwAccountId = p_recv_cmd->m_dwAccountId;

	MODI_WebPresentInfo * p_startaddr_present = (MODI_WebPresentInfo *)p_send_present->m_pData;

	/// 这里不需要考虑拆包，不会一下送上千个道具
	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select_present.GetRecord(i);
		if(p_select_record == NULL)
		{
			MODI_ASSERT(0);
			continue;
		}

		const MODI_VarType &  v_recordserial = p_select_record->GetValue("recordserial");
		const MODI_VarType  & v_tradeserial = p_select_record->GetValue("presentserial");
		const MODI_VarType & v_actoin = p_select_record->GetValue("action");
		const MODI_VarType & v_configid = p_select_record->GetValue("itemid");
		const MODI_VarType & v_goodtype = p_select_record->GetValue("goodtype");
		const MODI_VarType & v_money = p_select_record->GetValue("num");
		
		if(v_recordserial.Empty() || v_tradeserial.Empty() || v_actoin.Empty() ||
		   v_configid.Empty() || v_goodtype.Empty() || v_money.Empty() )
		{
			MODI_ASSERT(0);
			continue;
		}

		//		p_startaddr_present->m_qdRecordSerial = (QWORD)v_recordserial;
		//		p_startaddr_present->m_qdBuySerial = (QWORD)v_tradeserial;
		//		p_startaddr_present->m_byAction = (BYTE)v_actoin;
		p_startaddr_present->m_qdSerial = (QWORD)v_recordserial;
		p_startaddr_present->m_wdNum = (DWORD)v_money;
		p_startaddr_present->m_dwItemId = (DWORD)v_configid;
		p_startaddr_present->m_byLimit = (BYTE)v_goodtype;
		
		p_startaddr_present++;
		p_send_present->m_wdSize++;
	}

	if(p_send_present->m_wdSize > 0)
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_present, sizeof(MODI_RDB2S_Ret_WebPresent) + p_send_cmd->m_wdSize * sizeof(MODI_WebPresentInfo));
	
	return enPHandler_OK;
}


int	MODI_PackageHandler_rdb::NewMusic_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{

	MODI_S2RDB_NewSingleMusic  * music = (MODI_S2RDB_NewSingleMusic *) pt_null_cmd;
	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );
	MODI_Record insert_music;
	insert_music.Put("musicid", music->m_stMusic.m_wMusicid);
	insert_music.Put("singcount", music->m_stMusic.m_dwCount);
	insert_music.Put("highestscore",music->m_stMusic.m_dwScore);
	insert_music.Put("highestprecise",music->m_stMusic.m_wPrecise);
	insert_music.Put("scoreperson",music->m_stMusic.m_szScorePerson);
	insert_music.Put("preciseperson",music->m_stMusic.m_szPrecisePerson);
	/// Key MOde
	insert_music.Put("Keyscore",music->m_stMusic.m_dwKeyScore);
	insert_music.Put("Keyprecise",music->m_stMusic.m_wKeyPrecise);
	insert_music.Put("Keyscoreperson",music->m_stMusic.m_szKeyScorePerson);
	insert_music.Put("Keypreciseperson",music->m_stMusic.m_szKeyPrecisePerson);

	std::ostringstream	os;
	os<<"from_unixtime("<<music->m_stMusic.m_tPreciseFresh<<")";
	insert_music.Put("precisetime",os.str());
	os.str("");
	os<<"from_unixtime("<<music->m_stMusic.m_tScoreFresh<<")";
	insert_music.Put( "scoretime",os.str());

	/// Key MOde
	os.str("");
	os<<"from_unixtime("<<music->m_stMusic.m_tKeyPreciseFresh<<")";
	insert_music.Put("Keyprecisetime",os.str());
	os.str("");
	os<<"from_unixtime("<<music->m_stMusic.m_tKeyScoreFresh<<")";
	insert_music.Put( "Keyscoretime",os.str());


	MODI_RecordContainer insert_music_container;
	insert_music_container.Put(&insert_music);

	if(pDBClient->ExecInsert(MODI_DBProxyClient::m_pSingleMusic, &insert_music_container) != 1)
	{
		Global::logger->fatal("[insert_new_music] not insert into music  table <id=%u>",music->m_stMusic.m_wMusicid);
	}
	return enPHandler_OK;
}

int	MODI_PackageHandler_rdb::FlushMusicList_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size )
{

	MODI_S2RDB_FlushSingleMusicList   * flush = (MODI_S2RDB_FlushSingleMusicList *)pt_null_cmd;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	WORD	i =0;
	for( i=0;  i< flush->m_wNum; ++i )
	{
		SingleMusic *music = &(flush->m_stMusic[i]);
		MODI_Record insert_music;
		insert_music.Put("singcount", music->m_dwCount);
		insert_music.Put("highestscore",music->m_dwScore);
		insert_music.Put("highestprecise",music->m_wPrecise);
		insert_music.Put("scoreperson",music->m_szScorePerson);
		insert_music.Put("preciseperson",music->m_szPrecisePerson);

		/// Key Mode
		insert_music.Put("Keyscore",music->m_dwKeyScore);
		insert_music.Put("Keyprecise",music->m_wKeyPrecise);
		insert_music.Put("Keyscoreperson",music->m_szKeyScorePerson);
		insert_music.Put("Keypreciseperson",music->m_szKeyPrecisePerson);

		std::ostringstream	os;
		os.str("");
		os<<"from_unixtime("<<music->m_tPreciseFresh<<")";
		insert_music.Put("precisetime",os.str());
		os.str("");
		os<<"from_unixtime("<<music->m_tScoreFresh<<")";
		insert_music.Put( "scoretime",os.str());
		/// Key Mode
		os.str("");
		os<<"from_unixtime("<<music->m_tKeyPreciseFresh<<")";
		insert_music.Put("Keyprecisetime",os.str());
		os.str("");
		os<<"from_unixtime("<<music->m_tKeyScoreFresh<<")";
		insert_music.Put( "Keyscoretime",os.str());

		os.str("");
		os<<"musicid="<<music->m_wMusicid;
		insert_music.Put( "where", os.str());
	
		pDBClient->ExecUpdate( MODI_DBProxyClient::m_pSingleMusic,&insert_music);
	}

	return enPHandler_OK;
}




int MODI_PackageHandler_rdb::SaveRelation_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	return enPHandler_OK;
}


/// 条件发送邮件
int MODI_PackageHandler_rdb::SendMailByCondition_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size )
{
	const MODI_SendMailByCondition_Cmd * p_recv_cmd = (MODI_SendMailByCondition_Cmd * )pt_null_cmd;

	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock(p_db_client);

	MODI_RecordContainer character_container;
	int char_num = 0;
	if((char_num = p_db_client->ExecSelect(character_container, GlobalDB::m_pCharacterTbl, p_recv_cmd->m_stMailCmd.m_strName)) == 0)
	{
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(pt_null_cmd, cmd_size);
		return enPHandler_OK;
	}

	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_SendMailByCondition_Cmd * p_send_cmd = (MODI_SendMailByCondition_Cmd *)buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_stMailCmd = p_recv_cmd->m_stMailCmd;
	MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo * p_startaddr = &(p_send_cmd->m_pData[0]);
	p_send_cmd->m_wdSize = 0;
	
	for(int i=0; i<char_num; i++)
	{
		MODI_Record * pRecord = character_container.GetRecord(i);
		if(! pRecord)
		{
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		
		const MODI_VarType & v_name = pRecord->GetValue("name");
		strncpy(p_startaddr->m_cstrName, (const char *)v_name, sizeof(p_startaddr->m_cstrName) - 1);
		p_startaddr++;
		p_send_cmd->m_wdSize++;
		if(sizeof(MODI_SendMailByCondition_Cmd) + (p_send_cmd->m_wdSize * sizeof(MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo)) > Skt::MAX_USERDATASIZE - 256)
		{
			DWORD send_cmd_size = sizeof(MODI_SendMailByCondition_Cmd) + (p_send_cmd->m_wdSize * sizeof(MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo));
			MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_cmd, send_cmd_size);

			memset(buf, 0, sizeof(buf));
			AutoConstruct(p_send_cmd);
			p_send_cmd->m_stMailCmd = p_recv_cmd->m_stMailCmd;
			p_startaddr = &(p_send_cmd->m_pData[0]);
			p_send_cmd->m_wdSize = 0;
		}
	}

	if(p_send_cmd->m_wdSize > 0)
	{		
		DWORD send_cmd_size = sizeof(MODI_SendMailByCondition_Cmd) + (p_send_cmd->m_wdSize * sizeof(MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo));
		MODI_DBProxyClient::GetInstancePtr()->SendPackage(p_send_cmd, send_cmd_size);
	}
	
	return enPHandler_OK;
}

