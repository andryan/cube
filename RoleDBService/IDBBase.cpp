#include "IDBBase.h"
#include "AssertEx.h"

MODI_IDBBase::MODI_IDBBase( MODI_DBClient * pDBHandler ):
	m_pResults(0),
	m_nLastError(0),
	m_pDBClient( pDBHandler )
{
}

MODI_IDBBase::~MODI_IDBBase()
{
	CleanUpResult();
}

void 			MODI_IDBBase::CleanUpResult()
{
	if( m_pResults )
	{
		delete m_pResults;
		m_pResults = 0;
	}
}

void 			MODI_IDBBase::SetSqlStatement( const char * szSql )
{
	m_strSql = szSql;
}

const char 	* 	MODI_IDBBase::GetSqlStatement() const
{
	return m_strSql.c_str();
}

void 			MODI_IDBBase::ResetSqlStatement()
{
	m_strSql.clear();
}

bool	MODI_IDBBase::Load()
{
	MODI_ASSERT( m_pDBClient );

	if( !m_pDBClient )
		return false;

	if( m_pResults )
		delete m_pResults;
	m_pResults = 0;
	m_pResults = new MODI_RecordContainer();

	int nSqlRet = m_pDBClient->ExecSelect( *m_pResults , m_strSql.c_str() , m_strSql.size() );

	if( nSqlRet < 0 )
	{
		m_nLastError = m_pDBClient->GetLastError();
		m_strLastErrorDes = m_pDBClient->GetLastErrorDes();
		DumpLastError();
		return false;
	}

	MODI_ASSERT( nSqlRet == m_pResults->Size() );

	return true;
}


bool	MODI_IDBBase::AddNew(const void * pSource)
{
	MODI_ASSERT( m_pDBClient );
	if( !m_pDBClient )
		return false;

	int nSqlRet =	m_pDBClient->ExecSql( m_strSql.c_str() , m_strSql.size() );

	if( !nSqlRet )
		return true;

	m_nLastError = m_pDBClient->GetLastError();
	m_strLastErrorDes = m_pDBClient->GetLastErrorDes();
	DumpLastError();

	return false;
}

bool	MODI_IDBBase::Delete()
{
	MODI_ASSERT( m_pDBClient );
	if( !m_pDBClient )
		return false;

	int nSqlRet =	m_pDBClient->ExecSql( m_strSql.c_str() , m_strSql.size() );
	
	if( !nSqlRet )
		return true;
	
	m_nLastError = m_pDBClient->GetLastError();
	m_strLastErrorDes = m_pDBClient->GetLastErrorDes();
	DumpLastError();

	return false;
}

bool	MODI_IDBBase::Save(const void* pSource)
{
	MODI_ASSERT( m_pDBClient );

	if( !m_pDBClient )
		return false;

	int nSqlRet =	m_pDBClient->ExecSql( m_strSql.c_str() , m_strSql.size() );
	
	if( !nSqlRet )
		return true;

	m_nLastError = m_pDBClient->GetLastError();
	m_strLastErrorDes = m_pDBClient->GetLastErrorDes();
	DumpLastError();

	return false;
}

void 	MODI_IDBBase::DumpLastError()
{
	Global::logger->error("[%s] mysql lasterror<code=%u,desc=%s>." ,
			GAMEDB_OPT,
			m_nLastError,
			m_strLastErrorDes.c_str() );
}
