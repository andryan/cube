#include "DBCharFullData.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "s2rdb_cmd.h"
#include "Global.h"
#include "gamestructdef.h"
#include "Base/HelpFuns.h"



MODI_DBCharfullData::MODI_DBCharfullData( MODI_DBClient * pDBHandler ):
	m_nCharID(INVAILD_CHARID),
	m_dbCharbaseinfo( pDBHandler ),
	//m_dbItemlist( pDBHandler ),
	m_dbRels( pDBHandler )
{
	m_bHasChar = false;
}

MODI_DBCharfullData::~MODI_DBCharfullData()
{
	m_dbCharbaseinfo.CleanUpResult();
	//m_dbItemlist.CleanUpResult();
	m_dbRels.CleanUpResult();
}

bool	MODI_DBCharfullData::Load()
{
	Global::logger->debug("[%s] begin load character<accid=%u,charid=%u> full data." ,
			SVR_TEST ,
			this->GetAccId(),
			GetCharGuid() );

	// 加载角色的基本信息
	m_dbCharbaseinfo.SetAccountID( this->GetAccId() );
	m_dbCharbaseinfo.SetCharGuid( this->GetCharGuid() );
	if( !m_dbCharbaseinfo.Load() )
	{
		m_bHasChar = false;
		Global::logger->info("[%s] load character 's baseinfo faild." ,
			SVR_TEST );
		m_dbCharbaseinfo.DumpLastError();
		return false;
	}

	m_bHasChar = true;
	// 加载角色的物品列表
	//m_dbItemlist.SetAccId( this->GetAccId());
	//if( !m_dbItemlist.Load() )
	//{
	//		Global::logger->info("[%s] load character 's items faild." ,
	//			SVR_TEST );
	//		m_dbItemlist.DumpLastError();
	//		return false;
	//	}

	//  加载联系人列表
	m_dbRels.SetCharGuid( this->GetCharGuid() );
	if( !m_dbRels.Load() )
	{
		Global::logger->info("[%s] load character 's realations faild." ,
			SVR_TEST );
		m_dbRels.DumpLastError();
		return false;
	}

	return true;
}

#if 0
bool 	MODI_DBCharfullData::SaveItemList(const void * pSource)
{
	const MODI_CharactarFullData * pFullData = ( const MODI_CharactarFullData * )(pSource);
	static MODI_DBItemInfo temp[(MAX_BAG_AVATAR_COUNT + MAX_BAG_PLAYER_COUNT) * 2];
	WORD nCurIdx = 0;
	MODI_SaveItem saveItem;
	saveItem.m_bTrans = false;
	saveItem.m_pItemInfoSave = temp;
	WORD nAvatarBagSize = pFullData->m_avatarBag.Size();
	WORD nPlayerBagSize = pFullData->m_playerBag.Size();

	// 填充AVATAR背包中需要保存的物品
	WORD n = 0; 
	for( n = 0; n < nAvatarBagSize; n++ )
	{
		if( pFullData->m_avatarBag.m_Items[n].IsInValid() )
			continue ;

		temp[nCurIdx].Reset();
		temp[nCurIdx] = pFullData->m_avatarBag.m_Items[n];
		nCurIdx+=1;
	}

	// 填充PLAYER背包中需要保存的物品
	for( n = 0; n < nPlayerBagSize; n++ )
	{
		if( pFullData->m_playerBag.m_Items[n].IsInValid() )
			continue ;

		temp[nCurIdx].Reset();
		temp[nCurIdx] = pFullData->m_avatarBag.m_Items[n];
		nCurIdx+=1;
	}
	saveItem.m_nItemSaveCount = nCurIdx;

	// 保存物品
	m_dbItemlist.SetCharGuid( this->GetCharGuid() );
	if( !m_dbItemlist.Save( &saveItem ) )
	{
		return false;
	}

	return true;
}

#endif

bool 	MODI_DBCharfullData::SaveRelationList(const void * pSource)
{
/*	const MODI_CharactarFullData * pFullData = ( const MODI_CharactarFullData * )(pSource);

	static MODI_DBRelationInfo 	s_DBRelInfo[RELATIONLIST_MAX_COUNT * 2];
	MODI_SaveRelationInfo saveRel;
	saveRel.m_bTrans = false;
	saveRel.m_nSize = 0;
	saveRel.m_pRelationList = &s_DBRelInfo[0];

	for( int i = 0; i < RELATIONLIST_MAX_COUNT; i++ )
	{
		MODI_CHARID charid = GUID_LOPART( pFullData->m_Relations[i].m_guid );
		if( charid == INVAILD_CHARID )
			continue ;

		saveRel.m_pRelationList[saveRel.m_nSize].Reset();

		saveRel.m_pRelationList[saveRel.m_nSize].m_RelationType = pFullData->m_Relations[i].m_RelationType;
		saveRel.m_pRelationList[saveRel.m_nSize].m_bIsValid = 1;
		saveRel.m_pRelationList[saveRel.m_nSize].m_bySex = pFullData->m_Relations[i].m_bySex;
		safe_strncpy( saveRel.m_pRelationList[saveRel.m_nSize].m_szName , 
				pFullData->m_Relations[i].m_szName ,
				sizeof(saveRel.m_pRelationList[saveRel.m_nSize].m_szName ) );

		saveRel.m_pRelationList[saveRel.m_nSize].m_guid = pFullData->m_Relations[i].m_guid;
		saveRel.m_nSize += 1;
	}
	*/

	

	// 保存联系人
//	if( m_dbRels.Save( &saveRel ) == false )
//		return false;

	return true;
}

bool	MODI_DBCharfullData::Save(const void* pSource)
{
	const MODI_CharactarFullData * pFullData = ( const MODI_CharactarFullData * )(pSource);
	m_dbCharbaseinfo.SetAccountID( this->GetAccId() );
	m_dbCharbaseinfo.SetCharGuid( this->GetCharGuid() );
	if( !m_dbCharbaseinfo.Save( pFullData ) )
	{
		return false;
	}

// 	m_dbItemlist.SetCharGuid( GetCharGuid() );
// 	if( !SaveItemList( pFullData ) )
// 		return false;

	m_dbRels.SetCharGuid( GetCharGuid() );
	if( !SaveRelationList( pFullData ) )
		return false;
	
	return true;
}

bool	MODI_DBCharfullData::ParseLoadResult(void* pResult)
{
	MODI_RDB2S_Notify_LoadCharData * msg  = (MODI_RDB2S_Notify_LoadCharData *)(pResult);
	//MODI_CharactarFullData * pFullData = ( MODI_CharactarFullData * )(pResult);
	//MODI_CharactarFullData * pFullData = &(msg->m_CharFullData);
	if( !m_dbCharbaseinfo.ParseLoadResult(msg) )
	{
		Global::logger->warn("[%s] parse characterFullData's load result , <parse baseinfo faild >" ,
				SVR_TEST);

		return false;
	}

	//	MODI_LoadItem loadItem;
	//	loadItem.m_bTrans = false;
	//	loadItem.m_pAvatarBag = &(pFullData->m_avatarBag);
	//	loadItem.m_pPlayerBag = &(pFullData->m_playerBag);

	//	if( !m_dbItemlist.ParseLoadResult( &loadItem ) )
	//	{
	//		Global::logger->warn("[%s] parse characterFullData's load result , <items faild >" ,
	//				SVR_TEST);
	//		return false;
	//	}

	/*MODI_LoadRelationInfo loadRels;
	loadRels.m_bTrans = false;
	loadRels.m_nSize = RELATIONLIST_MAX_COUNT;
	loadRels.m_pRelationList = &(pFullData->m_Relations[0]);
	if( !m_dbRels.ParseLoadResult( &loadRels ) )
	{
		Global::logger->warn("[%s] parse characterFullData's load result , <relations faild >" ,
				SVR_TEST);
		return false;
	}
	*/

	return true;
}

void MODI_DBCharfullData::SetAccId( defAccountID accid )
{
	m_nAccount = accid;
}

defAccountID 	MODI_DBCharfullData::GetAccId() const
{
	return m_nAccount;
}

void			MODI_DBCharfullData::SetCharGuid(MODI_CHARID guid)
{
	m_nCharID = guid;
}

MODI_CHARID		MODI_DBCharfullData::GetCharGuid()
{
	return m_nCharID;
}

