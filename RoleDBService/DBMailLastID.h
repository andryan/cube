#ifndef DB_MAILMAXID_H_
#define DB_MAILMAXID_H_

#include "IDBBase.h"
#include "gamestructdef.h"

class MODI_DBMailLastID : public MODI_IDBBase
{
	MODI_DBMail_ID 			m_dbLastID;

public:

	explicit MODI_DBMailLastID( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBMailLastID();

	virtual bool	Load(); 

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	Save(const void* pSource) { return false; }

	virtual bool	ParseLoadResult(void* pResult);

public:

	MODI_DBMail_ID 		GetLastID() const { return m_dbLastID; }
};


#endif
