#include "DBMail.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "Global.h"
#include "gamestructdef.h"
#include "Base/HelpFuns.h"


MODI_DBMails::MODI_DBMails( MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler )
{
	memset( m_szName , 0 , sizeof(m_szName) );
}

MODI_DBMails::~MODI_DBMails()
{

}

bool	MODI_DBMails::Load()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_HasChar( GetRoleName() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Load() )
		return false;

	// 
	m_bHasChar = false;
	if( !m_pDBClient || !m_pResults )
		return false;

	size_t nRecordSize = m_pResults->Size();
	if( nRecordSize > 0 )
	{
		MODI_ASSERT( nRecordSize == 1 );
		m_bHasChar = true;
	}
	else 
		return true;

	szSql = MODI_SqlStatement::CreateStatement_LoadMaillist( GetRoleName() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;

	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Load() )
		return false;

	return true;
}

bool	MODI_DBMails::Save(const void* pSource)
{
	if( !m_pDBClient )
		return  false;

	const MODI_DBMailInfo * pSaveMail = (const MODI_DBMailInfo *)(pSource);
	if( !pSaveMail )
		return false;

	const char * szSql = MODI_SqlStatement::CreateStatement_SaveMaillist(  pSaveMail );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;

	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Save( pSource ) )
		return false;
	return true;
}

bool	MODI_DBMails::ParseLoadResult(void* pResult)
{
	if( !m_pDBClient || !m_pResults )
		return false;

	MODI_DBLoadMail * pLoadMail = (MODI_DBLoadMail *)(pResult);
	if( !pLoadMail->m_pList )
	{
		MODI_ASSERT(0);
		return false;
	}

	pLoadMail->m_pList->Reset();

	
		#define GET_RECORD_HELP(f) pRecord->GetValue(MODI_SqlStatement::GetFieldInMailTable(f))

		int iMailCount = 0;
		int iServerMailCount = 0;
		size_t nRecordSize = m_pResults->Size();
		for( size_t n = 0; n < nRecordSize; n++ )
		{
			MODI_Record * pRecord = m_pResults->GetRecord( n );
			if( pRecord )
			{
				const char * szUnixTimeStamp_sendtime = "UNIX_TIMESTAMP(`sendtime`)";

				const MODI_VarType & v_mailid = GET_RECORD_HELP(enMailTF_id);
			   	const MODI_VarType & v_sender = GET_RECORD_HELP(enMailTF_sender);
				const MODI_VarType & v_recever = GET_RECORD_HELP(enMailTF_recever);
				const MODI_VarType & v_caption = GET_RECORD_HELP(enMailTF_caption);
				const MODI_VarType & v_content = GET_RECORD_HELP(enMailTF_content);
				const MODI_VarType & v_type = GET_RECORD_HELP(enMailTF_type);
				const MODI_VarType & v_state = GET_RECORD_HELP(enMailTF_state);
				const MODI_VarType & v_sendtime = pRecord->GetValue( szUnixTimeStamp_sendtime );
				const MODI_VarType & v_extrainfo = GET_RECORD_HELP(enMailTF_extrainfo);
				const MODI_VarType & v_isvalid = GET_RECORD_HELP(enMailTF_isvalid);
				const MODI_VarType & v_scriptdata = GET_RECORD_HELP(enMailTF_scriptdata);
				const MODI_VarType & v_transid = GET_RECORD_HELP(enMailTF_transid);

				MODI_DBMailInfo * pmail = 0;

				//				bool bFull = false;
				enMailType type = (enMailType)v_type.ToInt();
				if( type == kServerMail )
				{
					if( iServerMailCount >= SERVER_MAIL_MAX_COUNT  )
					{
						//bFull = true;
						continue;
					}
					else 
					{
						pmail = &(pLoadMail->m_pList->m_ServerMail[iServerMailCount]);
					}
				}
				else 
				{
					if( iMailCount >= MAILLIST_MAX_COUNT  )
					{
						//bFull = true;
						continue;
					}
					else 
					{
						pmail = &(pLoadMail->m_pList->m_Mails[iMailCount]);
					}
				}

				//				if( bFull )
				//					continue ;

				pmail->Reset();

				pmail->m_MailInfo.m_Type = type;
				pmail->m_dbID = v_mailid.ToULLong();
				pmail->m_bIsValid = (bool)v_isvalid.ToUChar();
				pmail->m_MailInfo.m_State = v_state.ToInt();
				safe_strncpy( pmail->m_MailInfo.m_szSender , v_sender.ToStr() , sizeof(pmail->m_MailInfo.m_szSender) );
				safe_strncpy( pmail->m_MailInfo.m_szRecevier , v_recever.ToStr() , sizeof(pmail->m_MailInfo.m_szRecevier) );
				safe_strncpy( pmail->m_MailInfo.m_szCaption , v_caption.ToStr() , sizeof(pmail->m_MailInfo.m_szCaption) );
				safe_strncpy( pmail->m_MailInfo.m_szContents , v_content.ToStr() , sizeof(pmail->m_MailInfo.m_szContents) );
				pmail->m_Transid = v_transid.ToULLong();

				const char * szTempDataTime = v_sendtime.ToStr();
				time_t * ptmp1 = (time_t *)(&pmail->m_MailInfo.m_SendTime);
				unsigned long long * pTempOut = (unsigned long long *)(ptmp1);
				if( !safe_strtoull( szTempDataTime , pTempOut ) )
				{
					MODI_ASSERT(0);
					continue ;
				}

				unsigned int nOutLen = 0;
				pmail->m_MailInfo.m_byExtraCount = 0;
				
				/// 这里得拆分了
				for(BYTE i=0; i<MAX_MAIL_ATTACH_NUM; i++)
				{
					DWORD get_len = i * sizeof(MODI_MailExtraInfo ) * 2;
					const char * get_data = v_extrainfo.ToStr() + get_len;
					if(get_len >= v_extrainfo.Size())
					{
						break;
					}
					
					if( DBStr2Binary(get_data,  sizeof(MODI_MailExtraInfo) * 2, (char*)(&pmail->m_MailInfo.m_ExtraInfo[i]) ,
									  sizeof(MODI_MailExtraInfo) , nOutLen ) == false )
					{
						MODI_ASSERT(0);
						Global::logger->error("[%s] DBMail<name=%s> ParseLoadResult faild. function :DBStr2Binary == false.<extrainfo>" , 
											  GAMEDB_OPT ,GetRoleName());
						continue ;
					}
					pmail->m_MailInfo.m_byExtraCount++;
				}

				if( v_scriptdata.Size() > 0 )
				{
					if( DBStr2Binary( v_scriptdata.ToStr() , v_scriptdata.Size() ,
							(char*)(&pmail->m_ScriptData),
							sizeof(pmail->m_ScriptData) ,
							nOutLen ) == false ) 
					{
						MODI_ASSERT(0);
						Global::logger->error("[%s] DBMail<name=%s> ParseLoadResult faild. function :DBStr2Binary == false.<scriptdata>" , 
								GAMEDB_OPT ,
								GetRoleName());
						continue ;
					}
				}
				pmail->m_bIsValid = (bool)v_isvalid.ToUChar();

				if( pmail->m_bIsValid == false )
				{
					MODI_ASSERT(0);
					continue ;
				}

				if( type == kServerMail )
				{
					iServerMailCount += 1;
				}
				else 
				{
					iMailCount += 1;
				}
		}
	}
	return true;
}

	
void 			MODI_DBMails::SetRoleName(const char * szName)
{
	safe_strncpy(m_szName , szName , sizeof(m_szName) );
}

const char * 	MODI_DBMails::GetRoleName() const
{
	return m_szName;
}
