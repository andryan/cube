/** 
 * @file DBCharFullData.h
 * @brief DB中角色的所有数据操作
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-20
 */
#ifndef DB_CHARFULL_H_
#define DB_CHARFULL_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"
#include "DBCharBaseinfo.h"
//#include "DBItemlist.h"
#include "DBRelation.h"
#include "DBSocialRelData.h"

/** 
 * @brief DB中角色数据的操作
 */
class MODI_DBCharfullData 
{
	defAccountID 	m_nAccount;
	MODI_CHARID 	m_nCharID;
	MODI_DBCharBaseinfo 	m_dbCharbaseinfo;
	//MODI_DBItemlist  m_dbItemlist;
	MODI_DBRelation 	m_dbRels;
	bool 			m_bHasChar;

public:

	explicit MODI_DBCharfullData( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBCharfullData();


	/** 
	 * @brief 加载某个指定帐号的角色的所有信息
	 * 
	 * @return 	SQL 语句查询成功，返回TRUE
	 */
	virtual bool	Load(); 

	virtual bool	Save(const void* pSource);

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	/** 
	 * @brief 解析角色所有信息的结果
	 * 
	 * @param pResult 输出结果
	 * 
	 * @return 	成功返回TRUE
	 */
	virtual bool	ParseLoadResult(void* pResult);

private:

	bool 	SaveItemList(const void * pSource);
	bool 	SaveRelationList(const void * pSource);

public:

	void 			SetAccId( defAccountID accid );
	defAccountID 	GetAccId() const;
	void			SetCharGuid(MODI_CHARID guid);
	MODI_CHARID		GetCharGuid();
	bool 			IsHasCharacter() const { return m_bHasChar; }
};


#endif
