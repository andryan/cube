#include "DBProxyClient.h"
#include "PackageHandler_rdb.h"
#include "IAppFrame.h"
#include "Base/s2rdb_cmd.h"

//MODI_TableStruct * MODI_DBProxyClient::m_pCharacterOther = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pCharacter = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pUserVarTbl = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pFamilyMemTbl = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pFamilyTbl = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pZoneUserVarTbl = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pSingleMusic = NULL;
MODI_TableStruct * MODI_DBProxyClient::m_pRelationTbl = NULL;

MODI_DBProxyClient::MODI_DBProxyClient(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(SVR_CMDSIZE);
}

MODI_DBProxyClient::~MODI_DBProxyClient()
{

}



/**
 * @brief 初始化
 *
 */
bool MODI_DBProxyClient::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

void MODI_DBProxyClient::Final()
{
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("not get db handle in load user var");
	}
	MODI_ClientTask::Final();

	Global::logger->info("[%s] Disconnection with  Server. DBProxy will Terminate. " , 
			ERROR_CON );
	MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
	pApp->Terminate();
}

bool MODI_DBProxyClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }


    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_DBProxyClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

	return false;
}

bool MODI_DBProxyClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	///	游戏命令的处理
	int iRet = MODI_PackageHandler_rdb::GetInstancePtr()->DoHandlePackage(
					pt_null_cmd->byCmd, 
					pt_null_cmd->byParam, 
					this,
					pt_null_cmd,
					dwCmdLen);

	if( iRet == enPHandler_Ban )
	{

	}
	else  if ( iRet == enPHandler_Kick )
	{

	}
	else if( iRet == enPHandler_Warning )
	{

	}

	return true;
}

void MODI_DBProxyClient::ProcessPackages()
{
	const unsigned int  nMaxGetCmd = 10000;
	MODI_CmdParse::Get(nMaxGetCmd );
}
