#ifndef DB_RANK_RENQI_H_
#define DB_RANK_RENQI_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

class MODI_DBRenqiRank : public MODI_IDBBase
{

public:

	explicit MODI_DBRenqiRank( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBRenqiRank();

	virtual bool	Load(); 

	bool 	LoadSelfRank();

	virtual bool	Save(const void* pSource) { return false; }

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	ParseLoadResult(void* pResult);

	bool	ParseLoadResult_SelfRank(void* pResult);
};


#endif
