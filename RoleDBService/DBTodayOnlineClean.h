/** 
 * @file DBTodayOnlineClean.h
 * @brief 清理当前在线时间
 * @author Tang Teng
 * @version v0.1
 * @date 2010-11-26
 */

#ifndef DB_TODAYONLINETIMECLEAN_H_
#define DB_TODAYONLINETIMECLEAN_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

class MODI_DBTodayOnlineTimeClean : public MODI_IDBBase
{
public:

	explicit MODI_DBTodayOnlineTimeClean( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBTodayOnlineTimeClean();

	virtual bool	Load() { return false; }
	virtual bool	AddNew(const void * pSource) { return false; }
	virtual bool	Delete() { return false; }
	virtual bool	Save(const void* pSource) { return false; }
	bool 			Excute();
};


#endif
