/** 
 * @file DBMail.h
 * @brief DB中某个角色的邮件列表读取
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-27
 */
#ifndef DB_MAILS_H_
#define DB_MAILS_H_

#include "IDBBase.h"
#include "gamestructdef.h"

class MODI_DBMails : public MODI_IDBBase
{
	char 					m_szName[ROLE_NAME_MAX_LEN + 1];
	bool 					m_bHasChar;

public:

	explicit MODI_DBMails( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBMails();

	virtual bool	Load(); 

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	Save(const void* pSource);

	virtual bool	ParseLoadResult(void* pResult);

public:


	void 			SetRoleName(const char * szName);
	const char * 	GetRoleName() const;

	bool 			IsHasChar() const { return m_bHasChar; }
};


#endif
