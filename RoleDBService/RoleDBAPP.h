/** 
 * @file RoleDBAPP.h
 * @brief 角色数据库服务器框架
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-23
 */
#ifndef MODI_RDB_APPFRAME_H_
#define MODI_RDB_APPFRAME_H_


#include "IAppFrame.h"
#include "DBClient.h"
#include "DBStruct.h"
#include "PackageHandler_rdb.h"
#include "SqlStatement.h"
#include "DefaultAvatarCreator.h"
//#include "ItemSerialSet.h"
#include "DBProxyClient.h"


class 	MODI_RoleDBAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_RoleDBAPP( const char * szAppName );

		virtual ~MODI_RoleDBAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

		virtual void Terminate() { m_bShutdown = true ; }

//		// 默认的avatar 形象创建
		MODI_DefaultAvatarCreator 	& 	GetDefaultAvatarCreator() 
		{
			return m_DefaultCreator; 
		}

		DWORD 		GetMaxItemSerial() const { return m_nMaxItemSerial; }
		void 		SetMaxItemSerial( DWORD nMax ) { m_nMaxItemSerial = nMax; }

	private:

		MODI_DefaultAvatarCreator 	m_DefaultCreator;
		MODI_PackageHandler_rdb 	m_PackagehandlerInst; //  数据包派发器
		DWORD 						m_nMaxItemSerial; // 
		//MODI_ItemSerialSet 			m_ItemSerialSet;
		bool 						m_bShutdown;
		MODI_DBProxyClient * 		m_pClient;
		DBProxyType 				m_Type;
		BYTE 						m_Servers[255];
};

#endif
