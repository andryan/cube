#ifndef CUBE_DBPROXY_CLIENT_H_
#define CUBE_DBPROXY_CLIENT_H_

#include "ClientTask.h"
#include "SingleObject.h"
#include "CommandQueue.h"
#include "RecurisveMutex.h"
#include "DBStruct.h"

class MODI_DBProxyClient : public MODI_ClientTask  , 
	public CSingleObject<MODI_DBProxyClient> ,
	public MODI_CmdParse
{
public:

	MODI_DBProxyClient(const char * name, const char * server_ip,const WORD & port);

	~MODI_DBProxyClient();

	bool 	SendPackage( const void * pCmd , size_t nCmdSize )
	{
		return	this->SendCmd( (const Cmd::stNullCmd *)(pCmd) , nCmdSize );
	}

	/// 发送命令
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	virtual bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen); 

	/// 初始化
	virtual	bool Init();

	void ProcessPackages();

	//	static MODI_TableStruct * m_pCharacterOther;
	static MODI_TableStruct * m_pCharacter;
	static MODI_TableStruct * m_pUserVarTbl;
	static MODI_TableStruct * m_pFamilyMemTbl;
	static MODI_TableStruct * m_pFamilyTbl;
	static MODI_TableStruct * m_pZoneUserVarTbl;
	static MODI_TableStruct * m_pSingleMusic;
	static MODI_TableStruct  * m_pRelationTbl;

 protected:

	virtual void Final();

};


#endif
