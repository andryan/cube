#include "RoleDBAPP.h" 
#include "s2rdb_cmd.h"
#include "s2ms_cmd.h"
#include "s2zs_cmd.h"
#include "RoleLogicThread.h"
#include "BinFileMgr.h"
//#include "DBItemSerial.h"
#include "DBMailLastID.h"
#include "GlobalDB.h"

MODI_RoleDBAPP::MODI_RoleDBAPP( const char * szAppName ):MODI_IAppFrame( szAppName ),
m_pClient(0)
{
	m_bShutdown = false;
	memset( m_Servers , 0 , sizeof(m_Servers) );
}


MODI_RoleDBAPP::~MODI_RoleDBAPP()
{


}

int MODI_RoleDBAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	// role db 的配置信息
	const MODI_DBProxyConfig  * pDBProxyCfg = (const MODI_DBProxyConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_DBPROXY ));
	if( !pDBProxyCfg )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_DBProxyConfig::MODI_Info * pInfo = pDBProxyCfg->GetInfoByID( GetProxyDBIDFromArgs() );
	if( !pInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
	//	SNPRINTF( m_szPidFile , sizeof(m_szPidFile) , "RoleDBServer[%u].modipid" ,   GetProxyDBIDFromArgs() );


	// 增加一个日志文件到指定目录
    Global::logger->AddLocalFileLog( pInfo->strLogPath );
	
	std::string net_log_path = pInfo->strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();
	

	const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
	if( !pResInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	// 初始化网络派发器
	if( !m_PackagehandlerInst.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;

	}
	Global::logger->info("[%s]  Loading Bin Files ...", SYS_INIT);

	int iNoLoadMask = enBinFT_Music | enBinFT_Level | enBinFT_Item | enBinFT_Shop | enBinFT_Suit | enBinFT_Avatar | enBinFT_GoodsPackage;

    // 初始化bin mgr
    if (!MODI_BinFileMgr::GetInstancePtr()->Initial( pResInfo->strBinDir.c_str() , iNoLoadMask ) )
    {
        Global::logger->fatal("[%s] Fail to initialize BIN file manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
    }

	if( !m_DefaultCreator.Init() )
	{
        Global::logger->fatal("[%s] Fail to initialize Default Avatar Creator faild.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
	}

//	// 连接数据库
	if(! MODI_DBManager::GetInstance().Init( pDBProxyCfg->db_url() ) )
	{
		return MODI_IAppFrame::enInitDBSystemFaild;
	}

	// /// 获取表结构
// 	MODI_DBProxyClient::m_pCharacterOther = MODI_DBManager::GetInstance().GetTable("character_other");
// 	if(! MODI_DBProxyClient::m_pCharacterOther)
// 	{
// 		Global::logger->fatal("[%s] Unable get character_other table struct", SYS_INIT);
// 		return MODI_IAppFrame::enInitDBSystemFaild;
// 	}

	MODI_DBProxyClient::m_pCharacter = MODI_DBManager::GetInstance().GetTable("characters");
	if(! MODI_DBProxyClient::m_pCharacter)
	{
		Global::logger->fatal("[%s] Unable get character table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}
	MODI_DBProxyClient::m_pSingleMusic = MODI_DBManager::GetInstance().GetTable("music");
	if( !MODI_DBProxyClient::m_pSingleMusic)
	{
		Global::logger->fatal("Unable get music table struct");
		return MODI_IAppFrame::enInitDBSystemFaild;
		
	}

	MODI_DBProxyClient::m_pUserVarTbl = MODI_DBManager::GetInstance().GetTable("game_user_var");
	if(! MODI_DBProxyClient::m_pUserVarTbl)
	{
		Global::logger->fatal("[%s] Unable get game_user_var table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}

	MODI_DBProxyClient::m_pZoneUserVarTbl = MODI_DBManager::GetInstance().GetTable("zone_user_var");
	if(! MODI_DBProxyClient::m_pZoneUserVarTbl)
	{
		Global::logger->fatal("[%s] Unable get zone_user_var table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}

	MODI_DBProxyClient::m_pFamilyMemTbl = MODI_DBManager::GetInstance().GetTable("family_members");
	if(! MODI_DBProxyClient::m_pFamilyMemTbl)
	{
		Global::logger->fatal("[%s] Unable get character_family table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}

	MODI_DBProxyClient::m_pFamilyTbl = MODI_DBManager::GetInstance().GetTable("familys");
	if(! MODI_DBProxyClient::m_pFamilyTbl)
	{
		Global::logger->fatal("[%s] Unable get familys table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}
	MODI_DBProxyClient::m_pRelationTbl  = MODI_DBManager::GetInstance().GetTable("relations");
	if( !MODI_DBProxyClient::m_pRelationTbl)
	{
		Global::logger->fatal("[%s] Unable get relation table struct", SYS_INIT);
		return MODI_IAppFrame::enInitDBSystemFaild;
	}

	std::string server_ip;
	WORD 	server_port;

	SetWorldID(1);
	SetServerID(0);

	if( pInfo->type == kGameServerProxy || pInfo->type == kZoneServerProxy )
	{
		const MODI_SvrZSConfig * pZoneSvrInfo = (const MODI_SvrZSConfig * )(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ZONESERVER ) );
		if( !pZoneSvrInfo )
		{
			return MODI_IAppFrame::enConfigInvaild;
		}

		server_ip = pZoneSvrInfo->strIP;
		server_port = pZoneSvrInfo->nDBProxyPort;

		if( pInfo->type == kGameServerProxy )
		{
			unsigned int nCount = 0;
			std::set<unsigned int>::iterator itor =	pInfo->setGameServerID.begin();
			while( itor != pInfo->setGameServerID.end() )
			{
				unsigned int n = *itor;
				if( n >= 255 || n == 0 )
				{
					MODI_ASSERT(0);
					return false;
				}
				m_Servers[n] = 1;
				nCount += 1;
				itor++;
			}

			if( nCount == 0 )
				return MODI_IAppFrame::enConfigInvaild;
		}
	}
	else if( pInfo->type == kManagerServerProxy )
	{
		const MODI_SvrMSConfig * pMsInfo = (const MODI_SvrMSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_MANGGERSERVER ) );
		if( !pMsInfo )
		{
			return MODI_IAppFrame::enConfigInvaild;
		}

		server_ip = pMsInfo->strIP;
		server_port = pMsInfo->nPort;
		SetServerID( 255 );
	}
	else
	{
		MODI_ASSERT(0);
		return MODI_IAppFrame::enConfigInvaild;
	}

	/// 获取最大的itemid
	MODI_DBClient * p_db_client = MODI_DBManager::GetInstance().GetHandle();
	if(!p_db_client)
	{
		MODI_ASSERT(0);
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	MODI_RecordContainer select_container;
	MODI_Record select_record;
	std::string select_field = "MAX(`itemid`)";
	select_record.Put(select_field);
	std::ostringstream where;
	where<< "server_id=" << (WORD)GetServerID();

	int rec_num = p_db_client->ExecSelect(select_container, GlobalDB::m_pItemInfoTbl, where.str().c_str(), &select_record);
	if(rec_num == 1)
	{
		MODI_Record * get_record = select_container.GetRecord(0);
		const MODI_VarType & v_max_itemid = get_record->GetValue(select_field);
		if(! v_max_itemid.Empty())
		{
			Global::g_qdItemId = (unsigned long long)v_max_itemid;
		}
		else
		{
			Global::g_qdItemId = 0;
		}
		Global::logger->debug("[get_max_itemid] <itemid=%llu>", Global::g_qdItemId);
	}
	else
	{
		MODI_ASSERT(0);
		return MODI_IAppFrame::enConfigInvaild;
	}

	MODI_DBManager::GetInstance().PutHandle(p_db_client);
	
	

	m_Type = pInfo->type;
	m_pClient = new MODI_DBProxyClient( "DBProxy " , server_ip.c_str() , server_port );
	if( !m_pClient )
		return MODI_IAppFrame::enNoMemory;

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_RoleDBLogic();
	if( !m_pLogicThread )
		return MODI_IAppFrame::enNoMemory;


	return MODI_IAppFrame::enOK;
}


int MODI_RoleDBAPP::Run()
{

	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;


	MODI_DBMail_ID lastMailId = INVAILD_DBMAIL_ID;	

	if( m_Type == kZoneServerProxy )
	{
#if 0		
		MODI_ItemSerialSet * pSet = MODI_ItemSerialSet::GetInstancePtr();
		if( pSet )
		{
			if( !pSet->Init() )
			{
				Global::logger->error("[%s] load item serial set faild." , SYS_INIT );
				return MODI_IAppFrame::enLoadResourceFaild;
			}
		}
#endif		

		MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
		MODI_ScopeHandlerRelease lock( pDBClient );
		MODI_DBMailLastID dbLastMail( pDBClient );
		if( !dbLastMail.Load() )
		{
			Global::logger->error("[%s] load last mail id faild." , SYS_INIT );
			return MODI_IAppFrame::enLoadResourceFaild;
		}

		if( !dbLastMail.ParseLoadResult( 0 ) )
		{
			Global::logger->error("[%s] parse load last mail id's result faild." , SYS_INIT );
			return MODI_IAppFrame::enLoadResourceFaild;
		}
		
		lastMailId  = dbLastMail.GetLastID();
	}

	if( m_pClient )
	{
		if( !m_pClient->Init() )
		{
			Global::logger->info("[%s]connect to server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pClient->Start();
			Global::logger->info("[%s]connect to  server successful. " , SYS_INIT );
		}
	}
	else 
	{
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	if( m_Type == kZoneServerProxy || m_Type == kGameServerProxy )
	{
		MODI_S2ZS_Request_RegDBProxy msg;
		msg.m_type = m_Type;
		memcpy( msg.m_servers , m_Servers , sizeof(m_Servers) );
		if( m_Type == kZoneServerProxy )
		{
			msg.m_lastMailID = lastMailId;
		}
		m_pClient->SendCmd( &msg , sizeof(msg) );
	}
	else if( m_Type == kManagerServerProxy )
	{
		MODI_S2MS_Request_ServerReg msg;
		msg.m_iServerType = SVR_TYPE_DB;
		m_pClient->SendCmd( &msg , sizeof(msg) );
	}

	// 启动逻辑处理线程
	if( m_pLogicThread )
	{
		m_pLogicThread->Start();
	}

	while( this->m_bShutdown == false )
	{
		usleep( 10000 * 10 );
	}

	Global::logger->info("[%s] role db server run end." , SVR_TEST );

	return MODI_IAppFrame::enOK;
}


int MODI_RoleDBAPP::Shutdown()
{
	if( m_pClient )
	{
		m_pClient->TTerminate();
		m_pClient->Join();
	}

	Global::logger->info("[%s] role db server , client thread shut down ." , SVR_TEST );
	// 终止逻辑处理线程
	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

	Global::logger->info("[%s] role db server , logic thread shut down ." , SVR_TEST );
#if 0
	if( m_Type == kZoneServerProxy )
	{
		MODI_ItemSerialSet * pSet = MODI_ItemSerialSet::GetInstancePtr();
		if( pSet )
		{
			pSet->DoSaveAll();
		}
	}
#endif
	delete m_pLogicThread;

	delete m_pClient;

	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}


