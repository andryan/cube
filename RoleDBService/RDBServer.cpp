/** 
 * @file RDBServer.cpp
 * @brief 角色数据库服务器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-11
 */

#include "RoleDBAPP.h"


//#define CODE_TEST 

#ifdef CODE_TEST

#include "DBCharlist.h"
#include "DBAvatarConfigID.h"
#include "DBCharBaseinfo.h"
#include "DBCharFullData.h"
#include "DBItemlist.h"
#include "DefaultAvatarCreator.h"



std::string 	gs_strName; 
defAccountID  	nAccid = 0; 

void CreateName()
{
	nAccid += 1;
	char szName[256];
	SNPRINTF( szName , sizeof(szName) , "n_%u"  , nAccid  );
	gs_strName = szName;
}

int TestDBCharlistOpt(MODI_DBClient * pDBClient)
{
	// DB中角色列表操作对象
	MODI_DBCharlist dbcl( pDBClient );
	dbcl.SetAccountID( nAccid );

	MODI_CreateRoleInfo 	createInfo;
	safe_strncpy( createInfo.m_szRoleName,
			gs_strName.c_str(),
			sizeof(createInfo.m_szRoleName));
	createInfo.m_AvatarIdx = 1;
	createInfo.m_nCityID = 312;
	
	MODI_AddCharacter 	addChar;
	MODI_ItemInfoAdd 	addItems[MAX_BAG_AVATAR_COUNT];

	addChar.m_pCreateInfo = &createInfo;
	addChar.m_itemAdd.m_pItemInfoAdd = addItems;
	addChar.m_itemAdd.m_nItemAddCount = MAX_BAG_AVATAR_COUNT;

	MODI_RoleDBAPP * pApp = (MODI_RoleDBAPP *)(MODI_IAppFrame::GetInstancePtr());
	MODI_DefaultAvatarCreator & dav = pApp->GetDefaultAvatarCreator();

	// 获得默认的AVATAR形象
	if( !dav.CreaterDefaultAvatar( addChar.m_pCreateInfo->m_AvatarIdx ,  
				addChar.m_itemAdd.m_pItemInfoAdd , 
				addChar.m_itemAdd.m_nItemAddCount ) )
	{
		MODI_ASSERT(0);
		return -1;
	}

	// 添加一个新的角色到角色列表
	if( !dbcl.AddNew( &addChar ) )
	{
		if( dbcl.IsHasDupCharacter() )
		{
			// 重复的角色名字
			Global::logger->debug("[%s] accid<%u> create char was faild . dup rolename. " , 
					ROLEDATA_OPT ,
					nAccid );
			MODI_ASSERT(0);
		}
		else 
		{
			Global::logger->warn("[%s] accid<%u> create char was faild . sql statement for create role exe faild . " , 
					ROLEDATA_OPT , 
					nAccid );
			MODI_ASSERT(0);
		}

		return 1;
	}

	// 加载角色列表
	if( !dbcl.Load() )
	{
		Global::logger->warn("[%s] accid<%u> create char was faild . sql statement for load charlist faild . " , 
				ROLEDATA_OPT , 
				nAccid );
		MODI_ASSERT(0);
		return -2;
	}	

	MODI_CharInfo  	roleInfo;
	// 解析角色列表
	if( !dbcl.ParseLoadResult( &(roleInfo )) )
	{
		Global::logger->warn("[%s] accid<%u> create char was faild . parse charlist load result . " , 
				ROLEDATA_OPT , 
				nAccid );
		return -3;
	}

	Global::logger->debug("[%s] accid<%u> create char was successful ." , 
			ROLEDATA_OPT , 
			nAccid ); 

	// test dup character's name , will create faild
	bool bRet =  dbcl.AddNew( &addChar );
	MODI_ASSERT( !bRet );
	bRet =  dbcl.IsHasDupCharacter(); 
	MODI_ASSERT( bRet );
		
	// test load alreay exist character's info
	MODI_CharInfo  	roleInfo2;
	bRet = dbcl.Load();
	MODI_ASSERT( bRet );
	bRet = dbcl.ParseLoadResult( &(roleInfo2 ));
	MODI_ASSERT( bRet );

	size_t ns1 = strlen(roleInfo.m_szRoleName);
	size_t ns2 = strlen(roleInfo2.m_szRoleName);

	if( roleInfo.m_nCharID != roleInfo2.m_nCharID ||
		roleInfo.m_byLevel != roleInfo2.m_byLevel ||
		roleInfo.m_bySex != roleInfo2.m_bySex ||
		ns1 != ns2 ||
		strncmp( roleInfo.m_szRoleName , roleInfo2.m_szRoleName , strlen(roleInfo2.m_szRoleName) ) )
		{
			MODI_ASSERT(0);
		}

	for( int i = 0; i < MAX_BAG_AVATAR_COUNT; i++ )
	{
		if( roleInfo.m_AvatarInfo.m_Avatars[i] != roleInfo2.m_AvatarInfo.m_Avatars[i] )
		{
			MODI_ASSERT(0);
		}
	}

	WORD nAvatarCount = 0;
	for(int i = 0; i < MAX_BAG_AVATAR_COUNT; i++ )
	{
		if( roleInfo2.m_AvatarInfo.m_Avatars[i]	!= INVAILD_CONFIGID )
		{
			bool bOk = false;
			for( int k = 0; k < addChar.m_itemAdd.m_nItemAddCount; k++ )
			{
				if( roleInfo2.m_AvatarInfo.m_Avatars[i] == addChar.m_itemAdd.m_pItemInfoAdd[k].m_nConfigID )
				{
					bOk = true;
					break;
				}
			}

			MODI_ASSERT( bOk );
			nAvatarCount += 1;
		}
	}
	MODI_ASSERT( nAvatarCount == addChar.m_itemAdd.m_nItemAddCount );


	return 0;
}

int TestDBItemlist( MODI_DBClient * pDBClient )
{
	MODI_DBItemlist dbil( pDBClient );
	dbil.SetCharGuid( nAccid );

	// 为玩家背包添加10个物品
	const WORD nTestCount = 10;
	WORD nTestConfigs[nTestCount] = {31200,31201,31202,31203,31204,31205,31206,31207,31208,31209};

	// 生成添加物品的信息结构
	MODI_ItemInfoAdd AddInfos[nTestCount];
	for( int i = 0; i < nTestCount; i++ )
	{
		AddInfos[i].m_nBagType = enBagType_PlayerBag; // 玩家背包
		AddInfos[i].m_nItemPos = i + 1;  // 物品位置
		AddInfos[i].m_nCount = 10; // 物品个数
		AddInfos[i].m_nConfigID = nTestConfigs[i]; // 物品类型
		AddInfos[i].m_nRemainTime = enForever; // 物品时间
	}

	// 执行添加物品操作
	MODI_AddItem addItem; 
	addItem.m_pItemInfoAdd = AddInfos;
	addItem.m_nItemAddCount = nTestCount;
	addItem.m_bTrans = true;
	// test add item 
	if( !dbil.AddNew( &addItem ) )
	{
		MODI_ASSERT(0);
		return -1;
	}

	// test load item
	if( !dbil.Load() )
	{
		MODI_ASSERT(0);
		return -2;
	}

	// 执行加载物品操作
	MODI_CharactarFullData charfullData;
	MODI_LoadItem loadItem;
	loadItem.m_bTrans = false;
	loadItem.m_pAvatarBag = &(charfullData.m_avatarBag);
	loadItem.m_pPlayerBag = &(charfullData.m_playerBag);
	if( !dbil.ParseLoadResult( &loadItem ) )
	{
		MODI_ASSERT(0);
		return -3;
	}

	// 最后结果应该是5个AVATAR背包中的物品 ＋ 10个玩家背包中的物品
	// check load result 's size

	MODI_RoleDBAPP * pApp = (MODI_RoleDBAPP *)(MODI_IAppFrame::GetInstancePtr());
	MODI_DefaultAvatarCreator & dav = pApp->GetDefaultAvatarCreator();
	MODI_AddItem addcheck;
	MODI_ItemInfoAdd 	addItems[MAX_BAG_AVATAR_COUNT];
	addcheck.m_pItemInfoAdd = addItems;
	addcheck.m_nItemAddCount = MAX_BAG_AVATAR_COUNT;

	// 获得默认的AVATAR形象
	if( !dav.CreaterDefaultAvatar( 1 , 
				addcheck.m_pItemInfoAdd ,
				addcheck.m_nItemAddCount ) )
				
	{
		MODI_ASSERT(0);
		return -1;
	}

	// 5个avatar形象
	MODI_ASSERT( addcheck.m_nItemAddCount == 5 );

	// check avatar bag's contents
	for(int i = 0; i < loadItem.m_pAvatarBag->Size(); i++ )
	{
		// 前面5个位置有效
		if( i < 5 )
		{
			if( loadItem.m_pAvatarBag->m_Items[i].IsInVaild() )
			{
				MODI_ASSERT(0);
				return -4;
			}

			// 内容检查
			MODI_ASSERT( addcheck.m_pItemInfoAdd[i].m_nConfigID == loadItem.m_pAvatarBag->m_Items[i].m_nConfigID );
			MODI_ASSERT( addcheck.m_pItemInfoAdd[i].m_nCount == loadItem.m_pAvatarBag->m_Items[i].m_nCount );
			MODI_ASSERT( addcheck.m_pItemInfoAdd[i].m_nRemainTime == loadItem.m_pAvatarBag->m_Items[i].m_nRemainTime );
			MODI_ASSERT( addcheck.m_pItemInfoAdd[i].m_nItemPos == i + 1 );
		}
		else if( loadItem.m_pAvatarBag->m_Items[i].IsVaild() )
		{
			// 后面所有位置应该无效
			MODI_ASSERT(0);
			return -4;
		}
	}

	// check player bag's contents
	for(int i = 0; i < loadItem.m_pPlayerBag->Size(); i++ )
	{		
		// 前面10个位置有效
		if( i < 10 )
		{
			if( loadItem.m_pPlayerBag->m_Items[i].IsInVaild() )
			{
				MODI_ASSERT(0);
				return -4;
			}

			// 和之前添加到玩家背包中的物品应该一样
			const MODI_ItemInfoAdd & temp = addItem.m_pItemInfoAdd[i];
			if( loadItem.m_pPlayerBag->m_Items[i].m_nCount != 10 ||
				loadItem.m_pPlayerBag->m_Items[i].m_nConfigID != temp.m_nConfigID ||
				loadItem.m_pPlayerBag->m_Items[i].m_nRemainTime != temp.m_nRemainTime )
			{
				MODI_ASSERT(0);
				return -4;
			}
		}
		else if( loadItem.m_pPlayerBag->m_Items[i].IsVaild() )
		{
			// 其他位置应该无效
			MODI_ASSERT(0);
			return -4;
		}
	}

	// 所有物品的位置向后移动5个,数量增加10个,时间变为1个月
	const int nPosAdd = 5;
	const int nCountAdd = 10;
	const DWORD nRemainTime = enOneMonth;

	// test save item
	MODI_SaveItem saveItem;
	MODI_ItemInfoSave saveInfo_pbag[MAX_BAG_PLAYER_COUNT + MAX_BAG_AVATAR_COUNT];
	saveItem.m_bTrans = false;
	saveItem.m_pItemInfoSave = saveInfo_pbag;
	for( int i = 0; i < nTestCount; i++ )
	{
		// 物品id
		saveItem.m_pItemInfoSave[i].m_nItemID = charfullData.m_playerBag.m_Items[i].m_ItemGUID;

		saveItem.m_pItemInfoSave[i].m_nBagType = enBagType_PlayerBag;

		// 增加数量
		saveItem.m_pItemInfoSave[i].m_nCount = charfullData.m_playerBag.m_Items[i].m_nCount + nCountAdd;
		// 改变时间
		saveItem.m_pItemInfoSave[i].m_nRemainTime = nRemainTime;
		// 改变位置
		saveItem.m_pItemInfoSave[i].m_nItemPos = i + nPosAdd + 1;

		saveItem.m_nItemSaveCount += 1;
	}

	// do save
	if( !dbil.Save( &saveItem ) )
	{
		MODI_ASSERT(0);
		return -5;
	}

	// reload , and check save result

	MODI_CharactarFullData 	charfullData2;
	MODI_LoadItem 	loadItem2;
	loadItem2.m_bTrans = false;
	loadItem2.m_pAvatarBag = &(charfullData2.m_avatarBag);
	loadItem2.m_pPlayerBag = &(charfullData2.m_playerBag);
	if( !dbil.Load( ) )
	{
		MODI_ASSERT(0);
		return -6;
	}

	if( !dbil.ParseLoadResult( &loadItem2 ) )
	{
		MODI_ASSERT(0);
		return -6;
	}

	for( int i = 0; i < loadItem2.m_pAvatarBag->Size(); i++ )
	{
		// avatar bag
		if( loadItem2.m_pAvatarBag->m_Items[i].IsVaild() )
		{
			// 位置应该在 1-5之间
			MODI_ASSERT( i >= 0 && i < 5 );
		}
		else if( i >= 0 && i < 5 )
		{
			// 这个区间的物品应该是有效的
			MODI_ASSERT(0);
		}
	}

	int iCur = 0;
	for( int i = 0; i < loadItem2.m_pPlayerBag->Size(); i++ )
	{
		// avatar bag
		if( loadItem2.m_pPlayerBag->m_Items[i].IsVaild() )
		{
			// 位置应该在 6-15之间
			MODI_ASSERT( i >= 0 + nPosAdd && i < 10 + nPosAdd );
			MODI_ASSERT(loadItem2.m_pPlayerBag->m_Items[i].m_nConfigID == AddInfos[iCur++].m_nConfigID );
			MODI_ASSERT(loadItem2.m_pPlayerBag->m_Items[i].m_nCount == 10 + nCountAdd);
			MODI_ASSERT(loadItem2.m_pPlayerBag->m_Items[i].m_nRemainTime == nRemainTime );
		}
		else if( i >= 0 + nPosAdd && i < 10 + nPosAdd )
		{
			// 这个区间的物品应该是有效的
			MODI_ASSERT(0);
		}
	}

	MODI_ASSERT(iCur == nTestCount);

	return 0;
}

int TestDBOperator(MODI_DBClient * pDBClient)
{
	const char * szCleanTable = "TRUNCATE CHARACTERS; TRUNCATE ITEMINFOS;";
	pDBClient->ExecSql( szCleanTable , strlen(szCleanTable) );

	usleep(100000);
	const DWORD nTestCount = 10000 * 1000;
	for(DWORD n = 0; n < nTestCount; n++ )
	{
		CreateName();

		if( TestDBCharlistOpt( pDBClient ) )
		{
			MODI_ASSERT(0);
			return 1;
		}

		if( TestDBItemlist( pDBClient ) )
		{
			MODI_ASSERT(0);
			return 2;
		}	
	}

	return 0;
}


int CodeTest()
{
	MODI_RoleDBAPP app("CodeTest");
	int iRet = app.Init();
	if( iRet != MODI_IAppFrame::enOK)
	{
		Global::logger->error("[%s] test code faild , init faild , error code <%u>." ,SVR_TEST , iRet );
		return -1;
	}

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	MODI_ScopeHandlerRelease lock( pDBClient );

	if( TestDBOperator( pDBClient ) )
		return -1;

	return 0;
}

int main(int argc, char * argv[])
{
	return CodeTest();
}
#else

int main(int argc, char * argv[])
{
	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	MODI_RoleDBAPP app("Role DB Server");

	int iRet = app.Init();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	app.Shutdown();

	::sleep(1);

	return iRet;
}

#endif
