#include "RoleLogicThread.h"
#include "AssertEx.h"
//#include "ItemSerialSet.h"
#include "DBProxyClient.h"
#include "RoleDBAPP.h"
#include "Base/FPSControl.h"
#include "protocol/family_def.h"

pthread_t MODI_RoleDBLogic::ms_tid; 

#define SAVE_ITEMSERIALS_TIMER 	5 * 60 * 1000

MODI_RoleDBLogic::MODI_RoleDBLogic():m_stRTime(0),
	m_timerItemSerialSave( SAVE_ITEMSERIALS_TIMER ),
	m_timerLogicThread( 30 * 1000 )
{

}


MODI_RoleDBLogic::~MODI_RoleDBLogic()
{

}

/**
 * @brief 主循环
 *
 */
void MODI_RoleDBLogic::Run()
{
	ms_tid = pthread_self();
	//	MODI_ItemSerialSet * pSet = MODI_ItemSerialSet::GetInstancePtr();
	//if( !pSet->Init() )
	//	return ;

	MODI_FPSControl fps;
	fps.Initial( 20 );
	while (!IsTTerminate())
	{
		m_stRTime.GetNow();
		Global::m_stLogicRTime.GetNow();

		m_stRTime.GetNow();
		
		QWORD 	nBegin = m_stRTime.GetMSec();
		fps.Begin();
		MODI_DBProxyClient * pClient = MODI_DBProxyClient::GetInstancePtr();
		if( pClient )
		{
			pClient->ProcessPackages();
		}
		m_stRTime.GetNow();
		QWORD 	nEnd = m_stRTime.GetMSec();

		
		MODI_ASSERT( nEnd >= nBegin );
		if( nEnd > nBegin + 1000 )
		{
			Global::logger->debug("[%s] update dbproxy client<%p> use time=%u." , 
				SVR_TEST ,
				pClient ,
				nEnd - nBegin);
		}

		if( m_timerLogicThread( m_stRTime ) )
		{
			
		}

		fps.End();
	}

    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_RoleDBLogic::Final()
{
	Global::logger->info("[%s] Logic Thread fianl." , SVR_TEST ); 
	MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
	pApp->Terminate();
}
