/** 
 * @file DBCharBaseinfo.h
 * @brief DB中角色的基本信息的操作
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-20
 */
#ifndef DB_CHARBASEINFO_H_
#define DB_CHARBASEINFO_H_

#include "IDBBase.h"
#include "protocol/gamedefine.h"

/** 
 * @brief DB中角色数据的操作
 */
class MODI_DBCharBaseinfo : public MODI_IDBBase
{
	defAccountID 	m_nAccount;
	MODI_CHARID 	m_nCharID;

public:

	explicit MODI_DBCharBaseinfo( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBCharBaseinfo();


	/** 
	 * @brief 从DB中加载角色的基本信息
	 * 
	 * @return 	成功返回true
	 */
	virtual bool	Load(); 

	/** 
	 * @brief 保存角色的基本信息到DB中
	 * 
	 * @param pSource 需要保存的角色基本信息
	 * 
	 * @return 	成功返回true
	 */
	virtual bool	Save(const void* pSource);

	/** 
	 * @brief 添加角色的进本信息到DB中
	 * 
	 * @param pSource 需要添加的角色的基本信息
	 * 
	 * @return 	成功返回true
	 */
	virtual bool	AddNew(const void * pSource); 

	/** 
	 * @brief 从DB中删除一条角色的基本信息
	 * 
	 * @return 	成功返回true
	 */
	virtual bool	Delete(); 

	/** 
	 * @brief 解析角色所有信息的结果
	 * 
	 * @param pResult 输出结果
	 * 
	 * @return 	成功返回TRUE
	 */
	virtual bool	ParseLoadResult(void* pResult);

	bool 	SaveLastLogin(time_t nTime,const char * szIP);

public:

	void 			SetAccountID( defAccountID accid )
	{
		m_nAccount = accid;
	}

	defAccountID 	GetAccountID() const 
	{
		return m_nAccount;
	}

	void			SetCharGuid(MODI_CHARID guid);
	MODI_CHARID		GetCharGuid();
};


#endif
