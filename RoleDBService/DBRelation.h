/** 
 * @file DBRelation.h
 * @brief db中联系人的相关操作
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */
#ifndef DB_RELATIONS_H_
#define DB_RELATIONS_H_

#include "IDBBase.h"
#include "gamestructdef.h"

class MODI_DBRelation : public MODI_IDBBase
{

	MODI_CHARID 			m_nCharID;

public:

	explicit MODI_DBRelation( MODI_DBClient * pDBHandler );
	virtual ~MODI_DBRelation();

	/** 
	 * @brief 加载联系人列表
	 * 
	 * @return 	
	 */
	virtual bool	Load(); 

	virtual bool	AddNew(const void * pSource) { return false; }

	virtual bool	Delete() { return false; }

	virtual bool	Save(const void* pSource);

	virtual bool	ParseLoadResult(void* pResult);

public:

	void			SetCharGuid(MODI_CHARID guid);
	MODI_CHARID		GetCharGuid();
};


#endif
