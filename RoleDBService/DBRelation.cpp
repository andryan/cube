#include "DBRelation.h"
#include "SqlStatement.h"
#include "AssertEx.h"
#include "Global.h"
#include "gamestructdef.h"
#include "Base/HelpFuns.h"


MODI_DBRelation::MODI_DBRelation( MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler ) , 
	m_nCharID(INVAILD_CHARID)
{

}

MODI_DBRelation::~MODI_DBRelation()
{

}

bool	MODI_DBRelation::Load()
{
	const char * szSql = MODI_SqlStatement::CreateStatement_LoadRelationList( this->GetCharGuid() );
	MODI_ASSERT( szSql );
	if( !szSql )
		return false;
	this->SetSqlStatement( szSql );
	if( !MODI_IDBBase::Load() )
		return false;
	return true;
}

bool	MODI_DBRelation::Save(const void* pSource)
{
	if( !m_pDBClient )
		return  false;

	MODI_SaveRelationInfo * pList = ( MODI_SaveRelationInfo *)(pSource);
	if( pList->m_bTrans )
	{
		m_pDBClient->BeginTransaction();
	}

	try
	{
		for( DWORD i = 0; i < pList->m_nSize; i++ )
		{
			MODI_DBRelationInfo * pTemp = pList->m_pRelationList + i;

			const char * szSql = MODI_SqlStatement::CreateStatement_SaveRelationList( GetCharGuid() , 
					pTemp );
			MODI_ASSERT( szSql );
			if( !szSql )
			{
				if( pList->m_bTrans )
					throw "can't create sql statement for save relation.";
				else 
					continue ;
			}
			this->SetSqlStatement( szSql );

			if( !MODI_IDBBase::Save( pSource ) )
			{
				Global::logger->error("[%s] can't save charid<%u>'s relation to db , %s." ,
					   	ROLEDATA_OPT ,
					   	this->GetCharGuid()	,
						this->GetLastErrorDes() );

				if( pList->m_bTrans )
					throw "save relation fiald.";
				else 
					continue ;
			}
		}
	}
	catch(...)
	{
		if( pList->m_bTrans )
		{
			m_pDBClient->RollbackTransaction();
		}

		return false;
	}
	
	if( pList->m_bTrans )
	{
		m_pDBClient->CommitTransaction();
	}

	return true;
}

bool	MODI_DBRelation::ParseLoadResult(void* pResult)
{
	if( !m_pDBClient )
		return false;

	if( !m_pResults )
		return false;

	MODI_LoadRelationInfo * pList = (MODI_LoadRelationInfo *)(pResult);
	if( pList->m_bTrans )
	{
		m_pDBClient->BeginTransaction();
	}

	try
	{
		size_t nSize  = m_pResults->Size();
		if( nSize > pList->m_nSize )
			nSize = pList->m_nSize;

#ifdef _DEBUG
		std::map<MODI_CHARID,MODI_RelationInfo *> 	debugMap;
#endif

		for( size_t n = 0; n < nSize; n++ )
		{
			MODI_Record * pRecord = m_pResults->GetRecord( n );
			if( !pRecord )
			{
				Global::logger->fatal("[%s] DBRelaion.cpp::ParseLoadResult ." , SVR_TEST );
				MODI_ASSERT(0);
				continue ;
			}

			const MODI_VarType & v_relguid = pRecord->GetValue(MODI_SqlStatement::GetFieldInRelationTable(enRelationTF_relid));
			const MODI_VarType & v_reltype = pRecord->GetValue(MODI_SqlStatement::GetFieldInRelationTable(enRelationTF_reltype));
			const MODI_VarType & v_relsex = pRecord->GetValue(MODI_SqlStatement::GetFieldInRelationTable(enRelationTF_relsex));
			const MODI_VarType & v_relname = pRecord->GetValue(MODI_SqlStatement::GetFieldInRelationTable(enRelationTF_relname));
			const MODI_VarType & v_id = pRecord->GetValue("relid");

			MODI_DBRelationInfo * pTemp = pList->m_pRelationList  + n;

			pTemp->m_guid = MAKE_GUID( v_relguid.ToUInt() , 0 );
			pTemp->m_RelationType = (enRelationType)v_reltype.ToInt();
			pTemp->m_bySex = v_relsex.ToUChar();
			pTemp->m_dwRelid = (DWORD)v_id;
			memset( pTemp->m_szName , 0 , sizeof(pTemp->m_szName) );
			safe_strncpy( pTemp->m_szName  , v_relname.ToStr() , sizeof(pTemp->m_szName) );
			pTemp->m_bOnline = false;

#ifdef _DEBUG
			MODI_CHARID charid = v_relguid.ToUInt();
			std::map<MODI_CHARID,MODI_RelationInfo *>::iterator it = debugMap.find( charid );
			if( it != debugMap.end() )
			{
				MODI_ASSERT(0);
			}
			debugMap[charid] = pTemp;
#endif
		}
	}
	catch(...)
	{
		if( pList->m_bTrans )
		{
			m_pDBClient->RollbackTransaction();
		}

		return false;
	}

	if( pList->m_bTrans )
	{
		m_pDBClient->CommitTransaction();
	}

	return true;
}

void	MODI_DBRelation::SetCharGuid(MODI_CHARID guid)
{
	m_nCharID = guid;
}

MODI_CHARID		MODI_DBRelation::GetCharGuid()
{
	return m_nCharID;
}
