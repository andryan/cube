/** 
 * @file PackageHandler_rdb.h
 * @brief db proxy 上的数据报处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-12-13
 */
#ifndef ROLEDB_PACKAGE_HANDLER_H_
#define ROLEDB_PACKAGE_HANDLER_H_

#include "IPackageHandler.h"
#include "SingleObject.h"
#include "protocol/gamedefine.h"

class MODI_PackageHandler_rdb : public MODI_IPackageHandler  , public CSingleObject<MODI_PackageHandler_rdb>
{

public:

    MODI_PackageHandler_rdb();
    virtual ~MODI_PackageHandler_rdb();

	/// 加载角色列表
	static int LoadCharslist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 创建角色
	static int CreateChar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 加载角色数据
	static int LoadRoleData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 保存角色数据
	static int SaveRoleData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	static int SaveRelation_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
#if 0
	/// 保存物品
	static int SaveItem_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int ItemSerialTableOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 请求加载物品序列号
	static int LoadItemSerial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 请求加载物品序列号
	static int SaveItemSerial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int ItemCreateHistroy_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );
#endif	
	/// 请求保存联系人信息
	static int SaveRelations_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int SaveSocial_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 请求详细信息
	static int RoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	/// 加载邮件列表
	static int LoadMaillist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	// 保存邮件
	static int SaveMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int UpdateLastLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int ResetTodayOnlineTime_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int ExchagneYYKey_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int SaveYYKey_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int LoadRanks_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );
	
	static int ClearZero_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int MusicCount_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int ChangeUserVar_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int LoadUserVar_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int ZoneUserVar_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int RequestExecSql_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int RequestExecSqlTransaction_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int RequestItemInfo_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int DecMoney_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int BuyItem_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int FamilyLevel_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int GetWebItem_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int LoadCharList(const defAccountID & acc_id, MODI_CharInfo & char_info);

	static int LoadCharAvatar(const defAccountID & acc_id, MODI_ClientAvatarData & avatar_info);
	static int NewMusic_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );
	static int FlushMusicList_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );

	static int SendMailByCondition_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd ,
			const unsigned int cmd_size );
private:

    virtual bool FillPackageHandlerTable();
};

#endif
