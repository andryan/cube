#include "DBMailLastID.h"


MODI_DBMailLastID::MODI_DBMailLastID( MODI_DBClient * pDBHandler ):
	MODI_IDBBase( pDBHandler )
{
	m_dbLastID = INVAILD_DBMAIL_ID;	
}
	
MODI_DBMailLastID::~MODI_DBMailLastID()
{

}

bool	MODI_DBMailLastID::Load()
{
	const char * sql = "SELECT MAX(`mailid`) FROM `mails`"; 
	SetSqlStatement( sql );
	if( !MODI_IDBBase::Load() )
		return false;
	return true;

}

bool	MODI_DBMailLastID::ParseLoadResult(void* pResult)
{
	if( !m_pResults )
		return false;

	MODI_Record * pRecord = m_pResults->GetRecord( 0 ); 
	if( !pRecord )
		return false;
#ifdef _DEBUG
	size_t nSize = m_pResults->Size();
	MODI_ASSERT( nSize == 1 );
#endif
	const char * szField = "MAX(`mailid`)";
	const MODI_VarType & v_mailid = pRecord->GetValue(szField);
	m_dbLastID  = v_mailid.ToULLong();

	return true;
}
