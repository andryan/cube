###########################################################
## File: Main Makefile
## Date: 2009-12-04 CST       
## Version: Ver0.1
## Author: hurixin hurixin@modi.com
## Brief: Main loop call subdir makefile
###########################################################

export CompilerTool = /usr/bin/g++
export CompilerOption

export LinkTool = /usr/bin/g++
export LinkOption

CompilerOptionTemp = -c -I../Base -I../ -Wall -Werror -I/usr/include/ -I../../../Public \
`xml2-config --cflags` `mysql_config --cflags`
LinkOptionTemp = -Wall -Werror -L../base -lgd -llog4cxx -lpthread -lrt -lz -ldl -lm -lsybdb\
`xml2-config --libs` `mysql_config --libs_r`


DebugOption = -ggdb -D_GNU_SOURCE -D_REENTRANT -D_XXP_DEBUG -D_HRX_DEBUG -D_CUBE_DEBUG -D_LOGGER_ENGLISH  -D_DEBUG -D_LINUX -D_CUBE_DEBUG -D_SCHED_DEBUG  -DMODI_CATCH_EXCEPTIONS -D_USE_CHENMI  -D_VAR_DEBUG -D_GMTOOL_DEBUG -DENGLISH_ENCODE -D_FB_LOGGER
ReleaseOption = -ggdb -O2 -D_GNU_SOURCE -D_REENTRANT -D_LOGGER_ENGLISH  -D_LINUX -D_CUBE_DEBUG -D_HRX_DEBUG -D_USE_CHENMI -DENGLISH_ENCODE -D_FB_LOGGER  -D_D_VERSION_INA

ifdef LUANTAN
DebugOption = -g   -D_GNU_SOURCE -D_REENTRANT -D_HRX_DEBUG -D_CUBE_DEBUG -D_LOGGER_ENGLISH  -D_DEBUG -D_LINUX -D_CUBE_DEBUG -D_SCHED_DEBUG  -DMODI_CATCH_EXCEPTIONS -D_USE_LUNTAN -D_ML_VERSION
ReleaseOption = -g -O2 -D_GNU_SOURCE -D_REENTRANT -D_LOGGER_ENGLISH  -D_LINUX -D_CUBE_DEBUG -D_HRX_DEBUG -D_USE_LUNTAN -D_ML_VERSION
endif

export CubeLib = ../Base/cubelib.a
export G3DLib = ../G3D/g3dlib.a
export OtherLib = ../Base/hintencrypt64.a
export YauthLib = ../Base/libyauth64.a


SubDir = Base G3D  ZoneService GateService GameService DBService RoleDBService LoginService ManangerService ZoneService BillService InfoService UnifyService YYBillService YYLoginService BotService


.PHONY : debug release upcode clean 

AllDir = $(SubDir) 

AllTarget = outdebug
AllObject: $(AllTarget) 

release: clean upcode makerelease rdir
debug: clean upcode outdebug releasedir

$(AllTarget):
	@for dir in $(AllDir);\
	do\
		CompilerOption='$(DebugOption) $(CompilerOptionTemp)' \
		LinkOption='$(DebugOption) $(LinkOptionTemp)' \
		$(MAKE) -C $$dir || exit 1;\
	done

upcode:
	@svn up
	@svn up ../../Public
	@svn up ../../Music
	@svn up ../../Table
	@svn up ../DBScripts
	@rm -rf ./Data/Mdm
	@mkdir -p ./Data/Mdm

makerelease:
	@for dir in $(AllDir);\
	do\
		CompilerOption='$(ReleaseOption) $(CompilerOptionTemp)' \
		LinkOption='$(ReleaseOption) $(LinkOptionTemp)' \
		$(MAKE)  -C $$dir || exit 1;\
	done

rdir:
	@rm -rf CubeServer
	@mkdir -p CubeServer/
	@mkdir -p CubeServer/GateService
	@mkdir -p CubeServer/GameService
	@mkdir -p CubeServer/ZoneService
	@mkdir -p CubeServer/DBService
	@mkdir -p CubeServer/RoleDBService
	@mkdir -p CubeServer/ManagerService
	@mkdir -p CubeServer/LoginService
	@mkdir -p CubeServer/BillService
	@mkdir -p CubeServer/UnifyService
	@mkdir -p CubeServer/InfoService
	@mkdir -p CubeServer/YYBillService
	@mkdir -p CubeServer/YYLoginService
	@mkdir -p CubeServer/Data
	@mkdir -p CubeServer/Data/Mdm
	@mkdir -p CubeServer/Data/BinFiles
	@cp -af ./restart-game ./CubeServer
	@cp -af ./restart-db ./CubeServer
	@cp -af ./stopservice ./CubeServer
	@cp -af ./stop-loginservice ./CubeServer
	@cp -af ./reloadgamescript ./CubeServer
	@cp -af ./reloadinfoconfig ./CubeServer
	@cp -af ./reloadunifyconfig ./CubeServer
	@cp -af ./restart-info ./CubeServer
	@cp -af ./restart-unify ./CubeServer
	@cp -af ./kickall ./CubeServer
	@cp -af ./Script CubeServer/Script -r
	@cp -af ./GateService/GateService CubeServer/GateService
	@cp -af ./GameService/GameService CubeServer/GameService
	@cp -af ./ZoneService/ZoneService CubeServer/ZoneService
	@cp -af ./ManangerService/ManangerService CubeServer/ManagerService/ManagerService
	@cp -af ./LoginService/LoginService CubeServer/LoginService
	@cp -af ./RoleDBService/RoleDBService CubeServer/RoleDBService
	@cp -af ./DBService/DBService CubeServer/DBService
	@cp -af ./BillService/BillService CubeServer/BillService
	@cp -af ./BillService/BillService CubeServer/BillService
	@cp -af ./UnifyService/UnifyService CubeServer/UnifyService
	@cp -af ./InfoService/InfoService CubeServer/InfoService
	@cp -af ./YYBillService/YYBillService CubeServer/YYBillService
	@cp -af ./YYLoginService/YYLoginService CubeServer/YYLoginService

makeinstall:
	@mkdir -p ../CubeServer.INA/
	@mkdir -p ../CubeServer.INA/GateService
	@mkdir -p ../CubeServer.INA/GameService
	@mkdir -p ../CubeServer.INA/ZoneService
	@mkdir -p ../CubeServer.INA/DBService
	@mkdir -p ../CubeServer.INA/RoleDBService
	@mkdir -p ../CubeServer.INA/ManagerService
	@mkdir -p ../CubeServer.INA/LoginService
	@mkdir -p ../CubeServer.INA/BillService
	@mkdir -p ../CubeServer.INA/BotService
	@mkdir -p ../CubeServer.INA/UnifyService
	@mkdir -p ../CubeServer.INA/InfoService
	@mkdir -p ../CubeServer.INA/YYBillService
	@mkdir -p ../CubeServer.INA/YYLoginService
	@cp -af ./GateService/GateService ../CubeServer.INA/GateService
	@cp -af ./GameService/GameService ../CubeServer.INA/GameService
	@cp -af ./ZoneService/ZoneService ../CubeServer.INA/ZoneService
	@cp -af ./ManangerService/ManangerService ../CubeServer.INA/ManagerService/ManagerService
	@cp -af ./LoginService/LoginService ../CubeServer.INA/LoginService
	@cp -af ./RoleDBService/RoleDBService ../CubeServer.INA/RoleDBService
	@cp -af ./DBService/DBService ../CubeServer.INA/DBService
	@cp -af ./BillService/BillService ../CubeServer.INA/BillService
	@cp -af ./BotService/BotService ../CubeServer.INA/BotService
	@cp -af ./UnifyService/UnifyService ../CubeServer.INA/UnifyService
	@cp -af ./InfoService/InfoService ../CubeServer.INA/InfoService
	@cp -af ./YYBillService/YYBillService ../CubeServer.INA/YYBillService
	@cp -af ./YYLoginService/YYLoginService ../CubeServer.INA/YYLoginService

countline:
	@find . -iname \*.h |xargs  wc -l
	@find . -iname \*.cpp |xargs wc -l

clean:
	@for dir in $(AllDir);\
	do\
		CompilerOption='$(CompilerOptionTemp)' \
		LinkOption='$(LinkOptionTemp)' \
		$(MAKE) clean -C $$dir || exit 1;\
	done
	

