#ifndef   _GLOBAL_CONFIG_H__
#define	  _GLOBAL_CONFIG_H__
#include "XMLParser.h"
#include <string.h>

using namespace std;

struct  GlobalValueSt
{
	unsigned int m_nClass;
	unsigned int m_nSerial;
	unsigned int m_nNum;
	string	m_tDate;
	string	m_sSaveFile;
	string	m_sItems;
	string	m_sSqlFile;
	GlobalValueSt()
	{

		m_nClass = 0;
		m_nSerial = 0;
		m_nNum =0;
	}

};

class	GlobalValue
{

public:
	GlobalValue( );
	static GlobalValue * GetInstancePtr()
	{
		if ( m_ptr == NULL)
		{

			m_ptr = new  GlobalValue();
		}
		return m_ptr;
	}	

	GlobalValueSt & GetGlobalValueSt() ;
	void	LoadFile( char * file);
	void	SetCurCdNum( unsigned int i)
	{
		m_nCurJob = i;
	}

private:
	MODI_XMLParser	xmlfile;
	static	GlobalValue   *m_ptr;
	GlobalValueSt  	m_sValue;
	unsigned int	m_nCurJob;

};

#endif



