#include "DBStruct.h"
#include "DBClient.h"
#include <fstream>
#include   <iomanip> 

#include "GlobalConfig.h"
const unsigned int PLATFORM_KUGOU_ID = 3; //1已用
const unsigned int PLATFORM_ZONYOU_ID = 4; //1已用
const unsigned int PLATFORM_XINJIAN_ID = 5;//1已用
const unsigned int PLATFORM_IJIAYOU_ID = 6; // 1已用


const unsigned int SERIAL_ID = 9;/// 第几批

const unsigned int PLATFORM_ID = 1;/// 第几个  同一类可能会有好几个，这一类的话只能使用一个
const unsigned int	CLASS_ID	= 10;  //类型号

#define		CDKEY_NUM		1	//CDKEY的数目
   
#define 	CREATE_DATE	"0509"    //创建时间

#define		CDKEY_ITEMS		"50003,1,3;50002,2,3;50001,5,3;28003,1,1;28004,1,1"	//cdkey附带的物品

#define		SAVE_FILE		"j.0509wangtong.txt"

const char * GenLocalSerial()
{
	static DWORD m_dwLocalSerial = 1;
 	char serial_buf[25];
 	memset(serial_buf, 0, sizeof(serial_buf));
	char rand_password[23] = {'2','3','4','5','6','7','8','9','A','C','E','F','H','K','P','Q','R','S','T','W','X','Y','Z'};
	std::ostringstream os;
	os.str("");

	GlobalValueSt  &tmpvalue = GlobalValue::GetInstancePtr()->GetGlobalValueSt(); 
	for(unsigned int i = 0; i<5; i++)
	{
		int index = PublicFun::GetRandNum(0,22);
		
 		if(index > 11)
 			index -=9;
 		else
 		{
 			index +=9;
 		}
		int set = PublicFun::GetRandNum(0,1);
		char rand_c='0';
		if( set ==  1)
		{
			int randoms = PublicFun::GetRandNum(8,22);
			rand_c = rand_password[randoms];
		}
		else
		{
			rand_c = rand_password[index];
		}
		os<< rand_c;
	}



	/// PLAYFORM_ID, SERIAL_ID, 啥东西, 系列号
	sprintf(serial_buf, "%5s%02u%4s%03u%06u",os.str().c_str(), tmpvalue.m_nSerial,
			tmpvalue.m_tDate.c_str(),
		       tmpvalue.m_nClass,
		       m_dwLocalSerial++);
	std::string ss = serial_buf;
	return ss.c_str();
}



int main()
{
	//char rand_password[23] = {'2','3','4','5','6','7','8','9','A','C','E','F','H','K','P','Q','R','S','T','W','X','Y','Z'};
	Global::logger = new MODI_Logger("testdb");
	Global::net_logger = new MODI_Logger("net_test");

	Global::logger->RemoveConsoleLog();
	Global::net_logger->RemoveConsoleLog();


	GlobalValue::GetInstancePtr()->LoadFile("./config.xml");
	
	std::ostringstream os;
	std::ofstream ofile;
	std::ofstream sqlfile;
	ofile.open(GlobalValue::GetInstancePtr()->GetGlobalValueSt().m_sSaveFile.c_str(), std::ios_base::out);
	sqlfile.open(GlobalValue::GetInstancePtr()->GetGlobalValueSt().m_sSqlFile.c_str(), std::ios_base::out);
	os.str("");
	os << "LOCK TABLES `cdkeytable` WRITE;" << "\n";
	os<<"INSERT INTO `cdkeytable` values ";
	sqlfile <<os.str();
#ifdef	_DEBUG
#endif

	for(unsigned int i = 0; i<GlobalValue::GetInstancePtr()->GetGlobalValueSt().m_nNum; i++)
	{
		MODI_RTime rt_time;
		os.str("");
		os<<"('";
		sqlfile<<os.str();

		os.str("");
		os << GenLocalSerial();
		ofile << os.str();
		sqlfile<<os.str();

		sqlfile<<"','";



		///54001 54002 54003 54004 4个礼包
		/// 礼包ID， 个数， 时间(1=7天，2=30天，3=永久)
		os.str("");
		os<< GlobalValue::GetInstancePtr()->GetGlobalValueSt().m_sItems.c_str();
		
		sqlfile<<os.str();
		sqlfile<<"',";
		ofile<< "\n";
		MODI_RecordContainer record_container;

		sqlfile<<"0,1)";

		
		if( i+1 <GlobalValue::GetInstancePtr()->GetGlobalValueSt().m_nNum)
		{
			sqlfile<<",";
		}
	}
	sqlfile<<";"<<"\n";
	sqlfile <<"UNLOCK TABLES;" << "\n";

	
	ofile.close();
	
	return 0;
}
