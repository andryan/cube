/** 
 * @file DBServerBase.h
 * @brief DB服务器的一些基本定义
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef DBSERVER_BASE_H_
#define DBSERVER_BASE_H_


enum
{
	enSqlStatement_Type_None = 0,
	enSqlStatement_Type_Begin = 0,
	enSqlStatement_Type_Select,
	enSqlStatement_Type_Delete,
	enSqlStatement_Type_Update,
	enSqlStatement_Type_Insert,
	enSqlStatement_Type_Logic,
	enSqlStatement_Type_End , 
};


#endif
