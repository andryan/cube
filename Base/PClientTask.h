/**
 * @file   PClientTask.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Oct 12 14:11:35 2010
 * @version $Id:$
 * @brief  连接客户端
 * 
 * 
 */

#ifndef _MD_PCLIENTTASK_H
#define _MD_PCLIENTTASK_H


#include "Type.h"
#include "Timer.h"
#include "Socket.h"


/**
 * @brief 客户端连接模型
 * 
 */
class MODI_PClientTask: public MODI_DisableCopy
{
 public:
	/**
	 * @brief 构造
	 *
	 * @param server_ip 服务器的IP
	 * @param port 服务器的端口
	 *
	 */
	MODI_PClientTask(const char * server_ip, const WORD & port):
		m_strServerIP(server_ip), m_wdPort(port)
	{
		m_pSocket = NULL;
		m_stLiftTime.GetNow();
		m_enTaskState = enNoUseState;
		m_blTerminate = false;
		m_blTerminateFinal = false;
		m_blTimeOut = false;
	}

	virtual ~MODI_PClientTask()
	{
		Final();
	}

	/// 连接状态
	enum enTaskState
	{
		/// 空闲状态
		enNoUseState,
		/// 使用状态
		enNormalState,
		/// 回收状态
		enRecycleState
	};

	/**
	 * @brief 设置连接的状态
	 *
	 * @param state 设置的状态
	 *
	 */ 
	inline void SetState(enTaskState  state)
	{
		m_enTaskState = state;
	}

	/**
	 * @brief 返回连接的状态
	 *
	 * @return 连接的状态
	 *
	 */ 
	inline const enTaskState & GetState() const
	{
		return m_enTaskState;
	}

	/** 
	 * @brief 获取下一个状态
	 * 
	 */
	void GetNextState();


	/**
	 * @brief 置连接结束标志
	 *
	 */ 
	void Terminate()
	{
		m_blTerminate = true;
	}

	/**
	 * @brief 连接是否结束
	 * @return 接收true,失败false
	 *
	 */ 
	bool IsTerminate()
	{
		return m_blTerminate;
	}

	/// 置服务器结束标志
	void TerminateFinal()
	{
		m_blTerminateFinal = true;
	}

	/**
	 * @brief 服务器是否结束
	 *
	 * @return 结束true,失败false
	 *
	 */
	bool IsTerminateFinal()
	{
		return m_blTerminateFinal;
	}

	/// 回收连接
	virtual int RecycleConn() = 0;
	virtual void AddTaskToManager(){}
	virtual void RemoveTaskFromManager(){}
	virtual void TimeOut(){}

	/**
	 * @brief 注册到epoll事件
	 *
	 * @param epoll_fd epoll句柄
	 * @param evts 要轮轮询事件
	 * @param p_data 要轮询的对象指针
	 *
	 */ 
	void AddEpoll(int epoll_fd, uint32_t evts, void * p_data)
	{ 
		if(m_pSocket)
		{
			m_pSocket->AddEpoll(epoll_fd, evts, p_data);
		}
	}

	/**
	 * @brief 删除epoll事件
	 *
	 * @param epoll_fd epoll句柄
	 * @param evts 轮询的事件
	 *
	 */
	void DelEpoll(int epoll_fd, uint32_t evts)
	{
		if(m_pSocket)
		{
			m_pSocket->DelEpoll(epoll_fd, evts);
		}
	}
	
	/**
	 * @brief 获取服务器IP
	 *
	 * @return 服务器的IP
	 *
	 */ 
	const char * GetServiceIP() const
	{
		return m_strServerIP.c_str();
	}

	/**
	 * @brief 获取服务器的端口
	 *
	 * @return 服务器的端口
	 *
	 */ 
	const WORD & GetServicePort()
	{
		return m_wdPort;
	}


	/// 初始化
	virtual bool Init();

	/**
	 * @brief 发送命令
	 *
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 要发送命令的大小
	 * @param is_buffer 是否要队列发送(默认不要)
	 *
	 * @return 发送成功true,发送失败false
	 *
	 */
	bool SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size, bool is_buffer = false)
	{
		if(m_pSocket)
		{
			return m_pSocket->SendCmd(pt_null_cmd, cmd_size, is_buffer);
		}
		return false;
	}

	/**
	 * @brief 发送队列里面的命令
	 *
	 * @return 发送成功true,发送失败false
	 *
	 */ 
	bool SendQueueCmd()
	{
		if(m_pSocket)
		{
			return m_pSocket->SendQueueCmd();
		}
		return false;
	}

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * p_null_cmd, const int cmd_len) = 0;

	bool SendDataNoPoll()
	{
		if(!m_pSocket)
		{
			return false;
		}
		return m_pSocket->SendQueueCmdNoPoll();
	}


	virtual bool RecvDataNoPoll();

	/** 
	 * @brief 获取自己的IP
	 * 
	 * 
	 * @return 返回ip
	 */
	const char * GetIP()
	{
		return m_strClientIP.c_str();
	}

	/** 
	 * @brief 获取自己的地址
	 * 
	 * @return 返回端口
	 */
	WORD & GetPort()
	{
		return m_wdClientPort;
	}

	/** 
	 * @brief 检测连接是否验证超时
	 * 
	 * @param real_time 当前时间
	 * 
	 * @return 超时true
	 */
	inline bool CheckTimeOut(const MODI_RTime & real_time, const DWORD delay_mses = 8000)
	{
		if((delay_mses + m_stLiftTime.GetMSec()) >  real_time.GetMSec())
		{
			return false;
		}
		if(! m_blTimeOut)
		{
			m_blTimeOut = true;
			TimeOut();
			return true;
		}
		return true;
	}
	
 protected:
	/// 套接口
	MODI_Socket * m_pSocket;
	
	/// 释放连接资源
	virtual void Final()
	{
		if(m_pSocket)
		{
			delete m_pSocket;
		}
		m_pSocket = NULL;
	}

	MODI_RTime m_stLiftTime;
	
 private:	
	
	/// 服务器IP
	const std::string m_strServerIP;
	
	/// 服务器端口
	const WORD m_wdPort;

	/// 自己的ip/port
	std::string m_strClientIP;
	WORD m_wdClientPort;
	
 private:
	/// 是否结束
	bool m_blTerminate;
	bool m_blTerminateFinal;
	bool m_blTimeOut;
	/// 状态
	enTaskState m_enTaskState;
};

#endif
