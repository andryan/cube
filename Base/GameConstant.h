/** 
 * @file GameConstant.h
 * @brief 服务器常量定义，可配置
 * @author Tang Teng
 * @version v0.1
 * @date 2010-11-02
 */

#ifndef MODI_GAMECONSTANT_H_
#define MODI_GAMECONSTANT_H_

#include "BaseOS.h"

/*
   从某种角度来说，只是逻辑意义上的常量。
   可动态配置(gm命令)，或者静态配置(配置文件)

   游戏中所有概率，以百万分之一计算
*/


namespace 	MODI_GameConstant
{
	
	enum enConstantIndex
	{
		kIdx_Begin = 0,
		kIdx_EnterRareShopInterval = 1, // 进入神秘商城间隔
		kIdx_EnterRareShopPro, // 进入神秘商城概率
		kIdx_CharacterSyncAttrInterval, // 角色数据同步间隔
		kIdx_CharacterDBSaveInterval, // 角色数据保存间隔
		kIdx_Showtime_Pro, // show time 所需的时间百分比
		kIdx_FangChenmi_Off,
		kIdx_OneLogin,
		kIdx_Leveluptoten,
		kIdx_Count,
	};


	enum enConstantAttrMask
	{
		kMask_DynmaicWrite = 0x01,  // 是否可以动态改变常量
		kMask_StaticWrite = 0x02, // 是否可以静态改变常量
	};

	enum enConstantValueType
	{
		kCVT_DWORD,
		kCVT_INT,
		kCVT_FLOAT,
	};

	// 初始化
	void 	Initial();

	void 	set_DWORDValue_ByName( const char * szName , DWORD nValue );
	void 	set_IntValue_ByName( const char * szName , int iValue );
	void 	set_FloatValue_ByName( const char * szName , float fValue );

	bool 	get_ValueType_ByName( const char * szName , enConstantValueType & vt );

	bool 	IsCanDynmaicModfiy( DWORD nIdx );
	bool 	IsCanStaticModfiy( DWORD nIdx );

	// 进入神秘商城间隔
	DWORD 	get_EnterRareShopInterval();
	void 	set_EnterRareShopInterval(DWORD nSet);

	// 进入神秘商城概率
	DWORD 	get_EnterRareShopPro();
	void 	set_EnterRareShopPro(DWORD nSet);

	// 
	DWORD 	get_CharacterSyncAttrInterval();
	void 	set_CharacterSyncAttrInterval( DWORD nSet);

	DWORD 	get_CharacterDBSaveInterval();
	void 	set_CharacterDBSaveInterval( DWORD nSet);

	float 	get_ShowTimePro();
	void 	set_ShowTimePro(float fSet);

	bool 	get_FangChenmiOff();
	void 	set_FangChenmiOff(int iSet);

	bool 	get_OneLogin();
	void 	set_OneLogin(int set);

	bool 	get_LevelupToten();
	void 	set_LevelupToten(int set);

	// 释放
	void 	CleanUP();
};



#endif
