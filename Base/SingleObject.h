/** 
 * @file SingleObject.h
 * @brief ����ģ��
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-08
 */

#ifndef SINGLE_OBJECT_H_
#define SINGLE_OBJECT_H_

#ifdef _MSC_VER
#pragma once
#endif

#include "AssertEx.h"

template <typename T>
class CSingleObject
{
protected:

	static T	*	ms_pObject;

protected:

	CSingleObject()
	{
		MODI_ASSERT( ms_pObject  == 0 );
		ms_pObject = static_cast<T*>(this);
	}

	~CSingleObject()
	{
		MODI_ASSERT( ms_pObject  );
		ms_pObject = 0;
	}

public:

	static T *	GetInstancePtr() 
	{
		return ms_pObject;
	}

	static T &	GetInstanceRef()
	{
		MODI_ASSERT( ms_pObject  );
		return *ms_pObject;
	}
};

template <typename T>
	T	*	CSingleObject<T>::ms_pObject = 0;


#endif

