/**
 * @file DBClient.h
 * @date 2010-01-08 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库连接封装
 *
 */

#ifndef _MDDBCLIENT_H
#define _MDDBCLIENT_H

#include <mysql.h>
#include <mysqld_error.h>
#include "Type.h"
#include "Timer.h"
#include "Global.h"
#include "DBStruct.h"

/**
 * @brief 句柄状态
 *
 */
enum enHandleState
{
	/// 非法
   	DBCLIENT_HANDLE_INVALID  = 1,
	/// 未使用
   	DBCLIENT_HANDLE_UNUSED    = 2,
	/// 使用中
   	DBCLIENT_HANDLE_USED     = 3,  
};


/**
 * @brief 数据库连接句柄封装
 *
 */
class MODI_DBClient: public MODI_DisableCopy
{
 public:
	static DWORD MODI_DBClientHandleID;
	
	/// 构造
	MODI_DBClient(const MODI_URLInfo & url_info): m_dwHandleID(++MODI_DBClientHandleID), m_stLifeTime(), m_stUseTime()
	{
		m_stURLInfo = url_info;
		m_enHandleState = DBCLIENT_HANDLE_INVALID;
		m_dwUsedCount = 0;
		m_stMySql = NULL;
		m_lastErrorNo = 0;
	}

	~MODI_DBClient()
	{
	   	if(m_stMySql)
		{
			mysql_close(m_stMySql);
			m_stMySql = NULL;
		}
	}

	const enHandleState & GetState() const
	{
		return m_enHandleState;
	}

	/// 获取连接ID
	const DWORD & GetHandleID() const
	{
		return m_dwHandleID;
	}
		
	/// 获取连接
	MYSQL * GetMySql()
	{
		return m_stMySql;
	}

	/// 初始化
	bool InitHandle()
	{
		if(m_stMySql)
		{
			mysql_close(m_stMySql);
			m_stMySql = NULL;
		}

		m_stMySql = mysql_init(NULL);
		
		if(m_stMySql == NULL)
		{
			Global::logger->fatal(LOGINFO_DBINFO7, LOGACT_DB_OPT, __PRETTY_FUNCTION__);
			return false;
		}

		if(mysql_real_connect(m_stMySql, m_stURLInfo.GetLoginIP().c_str(), m_stURLInfo.GetLoginName().c_str(),m_stURLInfo.GetPassWord().c_str(),
							  m_stURLInfo.GetDBName().c_str(), m_stURLInfo.GetPort(), NULL,
							  CLIENT_COMPRESS | CLIENT_MULTI_STATEMENTS | CLIENT_INTERACTIVE) == NULL)
		{
			std::string lastErrorDes = mysql_error( m_stMySql );
			int lastErrorNo = mysql_errno( m_stMySql );

			Global::logger->fatal("[db_init] ip=%s,name=%s,password=%s,dbname=%s,port=%u",m_stURLInfo.GetLoginIP().c_str(),
							  m_stURLInfo.GetLoginName().c_str(), m_stURLInfo.GetPassWord().c_str(), m_stURLInfo.GetDBName().c_str(),
							  m_stURLInfo.GetPort());
			Global::logger->fatal("[db_init] call mysql_real_connect failed <error_id=%u,error=%s>", lastErrorNo, lastErrorDes.c_str());
			return false;
		}
		
		mysql_query(m_stMySql,"SET NAMES UTF8");
//		SetAutoCommit( true );
		
		m_enHandleState = DBCLIENT_HANDLE_UNUSED;
		m_stLifeTime.GetNow();
	   	m_dwUsedCount = 0;
		return true;
	}

	/// 使用连接
	void SetHandle()
	{
		if((m_dwUsedCount++ > 18000) || (mysql_ping(m_stMySql) != 0) || ( m_stLifeTime.Elapse() > 1800))
		{
#ifdef _HRX_DEBUG
			Global::logger->debug("[%s] dbclient(%d) reconnect(usercount=%u)", LOGACT_DB_OPT, m_dwHandleID, m_dwUsedCount);
#endif			
			if(! InitHandle())
			{
				Global::logger->fatal("mysql server is stop, please restart");
				//MODI_Assert(0);
			}
		}
		m_enHandleState = DBCLIENT_HANDLE_USED;
		m_stUseTime.GetNow();
	}

	/// 回收连接
	void UnsetHandle()
	{

		m_enHandleState = DBCLIENT_HANDLE_UNUSED;
	}

	/// 使用时间
	const DWORD GetUseTime()
	{
		return m_stUseTime.Elapse();
	}

	/// 执行sql语句
	int ExecSql(const char * sql, unsigned int sql_len);

	/// 获取符合条件的记录
   	int ExecSelect(MODI_RecordContainer & container, const char * sql, unsigned int sql_len, bool is_new = false);

	/// 获取符合条件的记录
   	int ExecSelect(MODI_RecordContainer & container, MODI_TableStruct * p_table, const char * where = NULL, MODI_Record * select_field = NULL, bool is_new=false);


	/** 
	 * @brief 生成选择语句
	 * 
	 * @param result 结果
	 * @param p_table 表
	 * @param select_field 要选择的字段
	 * @param where 条件
	 * 
	 * @return 成功true,失败false
	 *
	 */
	static bool MakeSelectSql(MODI_ByteBuffer & result, MODI_TableStruct * p_table, MODI_Record * select_field = NULL, const char * where = NULL);


	/** 
	 * @brief 生成插入语句
	 * 
	 * @param result 结果存放
	 * @param p_table 表
	 * @param record_container 要插入的内容
	 * 
	 * @return 成功true,失败false
	 *
	 */
	static bool MakeInsertSql(MODI_ByteBuffer & result, MODI_TableStruct * p_table, MODI_RecordContainer * record_container);



	/** 
	 * @brief 生成更新语句
	 * 
	 * @param result 结果存放
	 * @param p_table 表
	 * @param p_record 要更新的内容
	 * 
	 * @return 成功true
	 *
	 */
	static bool MakeUpdateSql(MODI_ByteBuffer & result, MODI_TableStruct * p_table, MODI_Record * p_record);


	/** 
	 * @brief 执行sql结果
	 * 
	 * @param sql 要执行的sql
	 * @param sql_len sql长度
	 *
	 * @return 影响的行数
	 *
	 */
	int ExecSqlResult(const char * sql, unsigned int sql_len);
	
		
	/// 执行插入
	unsigned int ExecInsert(MODI_TableStruct * p_table, MODI_RecordContainer * record_container);

	/// 执行删除
	unsigned int ExecDelete(MODI_TableStruct * p_table, const char * where);

	/// 执行更新
	unsigned int ExecUpdate(MODI_TableStruct * p_table, MODI_Record * p_record);

	/// 获取记录个数
	unsigned int GetCount(MODI_TableStruct * p_table, const char * where = NULL);
	
	// add by tt
	unsigned int GetLastError() const
	{
		return m_lastErrorNo;
	}

	const char * GetLastErrorDes() const
	{
		return m_lastErrorDes.c_str();
	}


	//bool SetAutoCommit( bool bAuto );

	bool BeginTransaction();

	bool CommitTransaction();

	bool RollbackTransaction();

 private:

	bool _TransactionCmd(const char *sql);

 private:
	/// mysql 连接
	MYSQL * m_stMySql;

	/// 句柄
	const DWORD m_dwHandleID;

	/// 连接信息
   	MODI_URLInfo m_stURLInfo;
	
	/// 生命周期
	MODI_TTime m_stLifeTime;

	/// 使用次数
	unsigned int m_dwUsedCount;

	/// 使用时间
	MODI_TTime m_stUseTime;

	/// 状态
	enHandleState m_enHandleState;

	/// 最后次错误的错误码
	unsigned int 	m_lastErrorNo;

	/// 最后次错误的描述
	std::string 	m_lastErrorDes;
};


#endif
