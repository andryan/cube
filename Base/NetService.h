/**
 * @file NetService.h
 * @date 2009-12-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 网络服务器基本框架
 *
 */

#ifndef _MDNETSERVICE_H
#define _MDNETSERVICE_H

#include "Service.h"
#include "TCPService.h"


/**
 * @brief 网络服务器基本框架
 *
 */
class MODI_NetService: public MODI_Service
{
 public:
	virtual ~MODI_NetService()
	{
		m_pNetInstance = NULL;
	}

	/**
	 * @brief 获取对象指针
	 *
	 * @return 对象指针
	 *
	 */ 
	static MODI_NetService * GetNetService()
	{
		return m_pNetInstance;
	}

 protected:
	MODI_NetService(const char * net_name): MODI_Service(net_name)
	{
		m_pNetInstance = this;
		m_strNetName = net_name;
		m_pTCPService = NULL;
	}
		
	/// 创建一个新的连接
	virtual bool CreateTask(const int sock, const struct sockaddr_in * addr) = 0;
	
	/// 初始化
	bool Init(const WORD & port, const char * service_ip);

	/// 主回调
	bool ServiceCallBack();

	/// 释放资源
	virtual	void Final();
 private:
	//// 服务器名字
	std::string m_strNetName;
	
	/// TCP服务器对象
	MODI_TCPService * m_pTCPService;

	/// 对象指针
	static MODI_NetService * m_pNetInstance;
};

#endif
