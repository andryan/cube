/** 
 * @file IGMCommandHandler.h
 * @brief 游戏GM命令处理类接口
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-28
 */
#ifndef IGMMAND_HANDLER_H_
#define IGMMAND_HANDLER_H_

#include <map>
#include <string>
#include "Type.h"


// gm 命令操作结果返回
enum
{
	enGMCmdHandler_OK = 0,
	enGMCmdHandler_InvalidParam = 1,
	enGMCmdHandler_InvalidCmd = 2,
	enGMCmdHandler_SendToZone = 3, 
};

// 命令处理函数
typedef int(* pCmdHandlerFun)(int , const char ** );


// 游戏GM命令结构
struct MODI_GMCommand
{
    char const * cmd; 				// 	命令名
    pCmdHandlerFun Func; 			// 	命令处理函数
    char const * description;  		//  命令功能描述
	char const * usage; 			// 	命令使用方法
};


class MODI_IGMCommandHandler
{
public:

    MODI_IGMCommandHandler();

    virtual ~MODI_IGMCommandHandler();


	bool 	Initialization();
	

    int  	DoHandleCommand( const char * szCmd );
	

protected:

    void AddCmdHandler(const char * szCmd , const MODI_GMCommand & h);

    virtual bool FillCmdHandlerTable() = 0;

	bool IsValidFormat( const char * szCmd );

	int  SplitCmd( const char * szCmd , char ** szOut , DWORD nOutCount );

protected:

	bool 	m_bInit;

	typedef std::map<std::string, MODI_GMCommand> 	MAP_HANDLER;
	typedef MAP_HANDLER::iterator 			MAP_HANDLER_ITER;
	typedef std::pair<MAP_HANDLER_ITER,bool> 	MAP_INSERT_RESULT;

	MAP_HANDLER 		m_mapHandlers;
};


#endif

