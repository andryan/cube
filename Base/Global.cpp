/**
 * @file Global.cpp
 * @date 2009-12-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 全局数据定义
 *
 */

#include <signal.h>
#include "Global.h"
#include "Service.h"
#include "IAppFrame.h"

unsigned long GetTickCount(bool bUpdate );
unsigned int GetChannelIDFromArgs();

/**
 * @brief 全局变量
 * 
 */
namespace Global
{
	/// 全局数据
	MODI_StringMap Value;

	/// 数据库url
	MODI_StringMap g_stURLMap;
	
	/// 全局随即数
	unsigned int __thread g_dwRandNum = 0;
	
	/// 定义一个全局的日志系统
	MODI_Logger * logger = NULL;
	MODI_Logger * music_logger = NULL;
	MODI_Logger * net_logger = NULL;
#ifdef _FB_LOGGER
	MODI_Logger * fb_logger = NULL;
#endif

	/// 当前时间
	MODI_RTime m_stRTime = 0;

	MODI_RTime m_stLogicRTime = 0; 

	/// 服务器绑定端口
	WORD g_wdServicePort = 10000;

	/// 服务器IP
	std::string g_strServiceIP = "127.0.0.1";

	/// 日志路径
	std::string g_strLogPath = "/tmp/log/";

	/// 平台
	std::string g_strPlatformIP = "";
	WORD g_wdPlatformPort = 0;
   	WORD g_wdPlatformFlag = 0;

	/// bill地址
	std::string g_strBillIP = "127.0.0.1";
   	WORD g_wdBillPort = 0;

	/// info地址
	std::string g_strInfoIP = "127.0.0.1";
	WORD g_wdInfoPort = 0;

	/// unify的地址
	std::string g_strUnifyIP = "127.0.0.1";
	WORD g_wdUnifyPort = 0;

	std::string g_strPayIP = "";
    WORD g_wdPayPort = 80;

	std::string g_strAccSyncIP = "";
	WORD g_wdAccSyncPort = 80;

	BYTE g_byReloadScript = 0;
	BYTE g_byReloadPackage = 0;
	
	/// 异常退出数目
	DWORD	g_nExcepLogout;
	/// 正常退出数量
	DWORD	g_nNormalLogout;

	QWORD g_qdItemId = 0;
	MODI_RWLock m_stItemIdLock;

	/// =1 INA
	BYTE g_byGameShop = 1;
}


/** 
 * @brief 信号处理函数
 * 
 */
static void SignalHandler(int sign , siginfo_t * pinfo , void * p)
{
	MODI_Service * p_service = MODI_Service::GetBaseInstancePtr();
	if(! p_service)
	{
		/// 临时
		MODI_IAppFrame * pApp = MODI_IAppFrame::GetInstancePtr();
		if( !pApp )
			return ;
		pApp->OnSignal_Term();
		return;
	}
	
	Global::logger->info("[sign_action] service recv a sign <service=%s,sign=%d>", p_service->GetName(), sign);
	
	if(sign == SIGUSR1)
	{
		p_service->DealSignalUsr1();
	}
	else if(sign == SIGUSR2)
	{
		p_service->DealSignalUsr2();
	}
	else
	{
		p_service->Terminate();
	}
}

/** 
 * @brief 信号注册
 * 
 */
static void RegisterSignal()
{
	struct sigaction act;
	sigemptyset( &act.sa_mask );
	act.sa_sigaction = SignalHandler;
	act.sa_flags = SA_SIGINFO;

	sigaction(SIGHUP, &act, NULL);   //1
	sigaction(SIGINT , &act , NULL) ;//2
	sigaction(SIGTERM , &act , NULL);//15
	sigaction(SIGUSR1 , &act , NULL);//10
	sigaction(SIGUSR2 , &act , NULL);//12
}

/** 
 * @brief 系统调用初始化
 * 
 */
static void InitGlobal() __attribute__((constructor));
void InitGlobal()
{
	RegisterSignal();
	std::string zone_name;
	MODI_ZTime::GetSysZone(zone_name);
	Global::Value["config_path"] = "./Config/config.xml";
	Global::g_dwRandNum = time(NULL);
}


/** 
 * @brief 系统自动调试释放资源
 * 
 */
static void FinalGlobal() __attribute__((destructor));
void FinalGlobal()
{
// 	if(Global::logger)
// 		delete Global::logger;
// 	Global::logger = NULL;

// 	if(Global::net_logger)
// 		delete Global::net_logger;
// 	Global::net_logger = NULL;
}



unsigned long GetTickCount(bool bUpdate )
{
	if( bUpdate )
		Global::m_stLogicRTime.GetNow();
	return Global::m_stLogicRTime.GetMSec();
}

unsigned int GetChannelIDFromArgs()
{
	const std::string & strChannelID = Global::Value["channelid"];
	unsigned int nChannelID = atoi(strChannelID.c_str());
	return nChannelID;
}

unsigned int GetProxyDBIDFromArgs()
{
	const std::string & strProxyID = Global::Value["dbproxyid"];
	unsigned int nProxyID = atoi(strProxyID.c_str());
	return nProxyID;
}


// bool 	SavePidFile( const char * szPidFile , pid_t pid )
// {
// 	if(!szPidFile || !szPidFile[0])
// 		return false;

//     FILE *fp = 0;
//     if ((fp = fopen(szPidFile, "w")) == NULL) 
// 	{
//         return false ;
//     }

//     fprintf(fp,"%ld\n", (long)pid);
//     if (fclose(fp) == -1) 
// 	{
//         return false;
//     }
// 	return true;
// }

// bool 	RemovePidFile( const char * szPidFile )
// {
// 	if(!szPidFile || !szPidFile[0])
//       return false;

// 	if(unlink(szPidFile) != 0) 
// 	{
// 		return false;
// 	}
// 	return true;
// }


