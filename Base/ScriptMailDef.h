/** 
 * @file ScriptMailDef.h
 * @brief 脚本邮件定义
 * @author Tang Teng
 * @version v0.1
 * @date 2010-09-26
 */

#ifndef MODI_SCRIPT_MAIL_DEFINE_H_
#define MODI_SCRIPT_MAIL_DEFINE_H_

#include "protocol/gamedefine2.h"

enum enScriptMailID
{
	kScriptMail_None = 0,
	kScriptMail_Begin = 0,
    kScriptMail_AddMail = 1, // 服务器发送给客户端的邮件
	kScriptMail_DecIdolCount = 2, // 减少偶像个数
	kScriptMail_IncIdolCount = 3, // 减少偶像个数
	kScriptMail_GiveMail = 4, // 减少偶像个数
	kScriptMail_BlockOpt = 5, // 拉黑操作
	kScriptMail_End = 0,
};


namespace MODI_ScriptMailArg
{
	struct 	MODI_AddMail
	{
		enMailType 		m_AddMailType; // 邮件类型
	};

	// 减少偶像参数
	struct 	MODI_DecIdolCount
	{
		unsigned char 	bySex;
	};

	struct MODI_IncIdolCount
	{
		char 			szName[ROLE_NAME_MAX_LEN + 1];
		unsigned char 	bySex;
	};

	struct MODI_ShopGiveMail
	{
		char 		szSender[ROLE_NAME_MAX_LEN + 1];
		QWORD 	 	transid; // 交易ID 

	};

	/// 黑名单操作
	struct MODI_BlockOpt
	{
		/// 操作类型(被增加/被删除)
		BYTE m_byType;
	};
};


struct MODI_ScriptMailData
{
	enScriptMailID 		m_ScriptID;
	
	union
	{
		MODI_ScriptMailArg::MODI_AddMail  	m_AddMail;
		MODI_ScriptMailArg::MODI_DecIdolCount m_DecIdolCount;
		MODI_ScriptMailArg::MODI_IncIdolCount m_IncIdolCount;
		MODI_ScriptMailArg::MODI_ShopGiveMail m_ShopGiveMail;
		MODI_ScriptMailArg::MODI_BlockOpt m_stBlockOpt;
	}m_Arg;

	MODI_ScriptMailData()
	{
		Reset();
	}

	void 	Reset()
	{
		m_ScriptID = kScriptMail_None;
		memset( &m_Arg , 0 , sizeof(m_Arg) );
	}

	const MODI_ScriptMailArg::MODI_AddMail  	& 	GetArg_AddMail() const { return m_Arg.m_AddMail; }
	const MODI_ScriptMailArg::MODI_DecIdolCount  	& 	GetArg_DecIdolCount() const { return m_Arg.m_DecIdolCount; }
	const MODI_ScriptMailArg::MODI_IncIdolCount  	& 	GetArg_IncIdolCount() const { return m_Arg.m_IncIdolCount; }
	const MODI_ScriptMailArg::MODI_ShopGiveMail 	& 	GetArg_ShopGiveMail() const { return m_Arg.m_ShopGiveMail; }
	const MODI_ScriptMailArg::MODI_BlockOpt 	& 	GetArg_BlockOpt() const {return m_Arg.m_stBlockOpt; }

	MODI_ScriptMailArg::MODI_AddMail  	& 	GetArgM_AddMail() { return m_Arg.m_AddMail; }
	MODI_ScriptMailArg::MODI_DecIdolCount  	& 	GetArgM_DecIdolCount() { return m_Arg.m_DecIdolCount; }
	MODI_ScriptMailArg::MODI_IncIdolCount  	& 	GetArgM_IncIdolCount() { return m_Arg.m_IncIdolCount; }
	MODI_ScriptMailArg::MODI_ShopGiveMail 	& 	GetArgM_ShopGiveMail() { return m_Arg.m_ShopGiveMail; }
	MODI_ScriptMailArg::MODI_BlockOpt & GetArgM_BlockOpt(){ return m_Arg.m_stBlockOpt; }
};

#endif

