/**
 * @file   InviteManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue May  3 10:00:28 2011
 * 
 * @brief  邀请管理
 * 
 * 
 */


#ifndef _MD_INVITE_MANAGER_H
#define _MD_INVITE_MANAGER_H

#include "Global.h"
#include "protocol/gamedefine.h"

/** 
 * @brief 邀请的数据
 * 
 */
class MODI_InviteData
{
 public:
	MODI_InviteData()
	{
		m_dwInviteID = 0;
		m_qwTimeout = 0;
	}
	
	virtual ~MODI_InviteData(){}
	
	const DWORD & GetInviteID()
	{
		return m_dwInviteID;
	}

	virtual void Invite() = 0;
	virtual void Responsion(const enOptRequestResult & result) = 0;

	/** 
	 * @brief 是否超时
	 * 
	 */
	bool IsTimeout(const MODI_RTime & cur_time)
	{
		if(cur_time.GetMSec()  > m_qwTimeout)
			return true;
		return false;
	}

	void SetInviteID(const DWORD & invite_id)
	{
		m_dwInviteID = invite_id;
	}

	void SetTimeout(const QWORD & time_out)
	{
		m_qwTimeout = time_out;
	}
 private:
	DWORD m_dwInviteID;
	QWORD m_qwTimeout;
};



/**
 * @brief 邀请管理
 * 
 */
class MODI_InviteManager
{
 public:
	typedef std::map<DWORD, MODI_InviteData * > defInviteDataMap;
	typedef std::map<DWORD, MODI_InviteData * >::iterator defInviteDataMapIter;
	typedef std::map<DWORD, MODI_InviteData * >::value_type defInviteDataMapValue;

	
	MODI_InviteManager()
	{
		
	}

	static MODI_InviteManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_InviteManager;
		}
		return *m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
		
			m_pInstance = NULL;
		}
	}

	

	/** 
	 * @brief 增加一个邀请记录
	 * 
	 * @param invite_data 减少一个邀请记录
	 * 
	 */
	bool AddInvite(MODI_InviteData *  p_invite_data);


	/** 
	 * @brief 删除一个邀请
	 * 
	 */
	void RemoveInvite(const DWORD & invite_id);


	/** 
	 * @brief 查找一个邀请
	 * 
	 */
	MODI_InviteData * FindInvite(const DWORD invite_id);
		
	/** 
	 * @brief 更新
	 * 
	 */
	void UpDate(const MODI_RTime & cur_time);


	/** 
	 * @brief 获取邀请id
	 * 
	 * 
	 */
	static const DWORD & GenInviteID()
	{
		m_dwGenInviteID++;
		return m_dwGenInviteID;
	}
	
 private:
	static MODI_InviteManager * m_pInstance;
	static DWORD m_dwGenInviteID;
	
	defInviteDataMap m_stInviteDataMap;
};

#endif
