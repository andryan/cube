/**
 * @file   GlobalDB.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 13 11:34:40 2011
 * 
 * @brief  全部表结构管理
 * 
 */

#ifndef _MD_GLOBALDB_H
#define _MD_GLOBALDB_H

class MODI_TableStruct;

/**
 * @brief  表管理
 * 
 */
namespace GlobalDB
{
	
	/// zone_user_var
	extern MODI_TableStruct * m_pZoneUserVarTbl;
	extern MODI_TableStruct * m_pGameUserVarTbl;
	extern MODI_TableStruct * m_pItemInfoTbl;
	extern MODI_TableStruct * m_pCharacterTbl;
	extern MODI_TableStruct * m_pBuyrecordTbl;
	extern MODI_TableStruct * m_pGiverecordTbl;
	extern MODI_TableStruct * m_pFamilyTbl;
	extern MODI_TableStruct * m_pWebItemTbl;
	extern MODI_TableStruct * m_pCharInfoTbl;
	extern MODI_TableStruct * m_pINAConsume;
	extern MODI_TableStruct * m_pWebPresentTbl;
};

class MODI_GlobalDB
{
 public:
	/** 
	 * @brief 加载所有的表结构
	 * 
	 * 
	 */
	static bool Init();

	static std::string zone_user_var_name;
	static std::string game_user_var_name;
	static std::string item_infos_name;
	static std::string characters_name;
	static std::string buyrecord_name;
	static std::string giverecord_name;
	static std::string family_name;
	static std::string web_item;
	static std::string char_info;
	static std::string ina_consume;
	static std::string web_present;
};

#endif
