/** 
 * @file TMgr.h
 * @brief 管理器模板
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-10
 */

#ifndef TEMPALTE_MGR_H_
#define TEMPALTE_MGR_H_


#include <map>

template <typename K , typename V>
class 	MODI_TMap
{
	public:

		typedef typename std::map<K,V *>			TMAPS;
		typedef typename TMAPS::iterator 			TMAPS_ITER;
		typedef typename TMAPS::const_iterator 		TMAPS_C_ITER;
		typedef typename std::pair<TMAPS_ITER,bool>  TMAPS_INSERT_RESULT;

	public:

		MODI_TMap()
		{

		}

		~MODI_TMap()
		{
			this->Clear();
		}


	public:

		bool Add( K k , V * pValue )
		{
			TMAPS_INSERT_RESULT insertRes = this->m_mapContents.insert( std::make_pair( k , pValue ) );
			return insertRes.second;
		}

		bool Del( K k ,bool bFree = false )
		{
			TMAPS_ITER itor = this->m_mapContents.find( k );
			if( itor != this->m_mapContents.end() )
			{
				if( bFree )
				{
					V * p = itor->second;
					delete p;
				}
				this->m_mapContents.erase( itor );
				return true;
			}
			return false;
		}

		bool IsIn( K k )
		{
			return this->Find( k ) ? true : false;
		}

		const V * Find( K k )
		{
			const V * pRet = 0;
			TMAPS_ITER itor = this->m_mapContents.find( k );
			if( itor != this->m_mapContents.end() )
			{
				pRet = itor->second;
			}
			return pRet;
		}

		size_t Size()
		{
			return this->m_mapContents.size();
		}

		void Clear(bool bFree = false)
		{
			TMAPS_ITER itor = this->m_mapContents.begin();
			while( itor != this->m_mapContents.end() )
			{
				V * pTemp = itor->second;
				if( bFree )
					delete pTemp;
				itor++;
			}
			this->m_mapContents.clear();
		}

		TMAPS &	GetMap()
		{
			return m_mapContents;
		}


	protected:

		
		TMAPS 			m_mapContents;
};






#endif

