/** 
 * @file BinFileMgr.h
 * @brief *.bin 文件资源管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */

#ifndef BIN_FILE_MGR_H_
#define BIN_FILE_MGR_H_

#include "BinTable.h"
#include "GameTableDefine/Musiclist.h"
#include "GameTableDefine/AllResource.h"
#include "GameTableDefine/Levellist.h"
#include "GameTableDefine/DefaultAvatar.h"
#include "GameTableDefine/Suit.h"
#include "GameTableDefine/Avatar.h"
#include "GameTableDefine/Itemlist.h"
#include "GameTableDefine/Shop.h"
#include "GameTableDefine/Scenelist.h"
#include "GameTableDefine/Impactlist.h"
#include "GameTableDefine/Shop.h"
#include "GameTableDefine/GoodsPackage.h"
#include "GameTableDefine/Chenghao.h"
#include "GameTableDefine/Quickbuy.h"
#include "GameTableDefine/Randomdraw.h"
#include "GameTableDefine/WaitRoom.h"
#include "protocol/gamedefine.h"
#include "Base/gamestructdef.h"

enum
{
	enBinFT_Music = 1,
	enBinFT_Level = 2,
	enBinFT_Avatar = 4,
	enBinFT_DefaultAvatar = 8,
	enBinFT_Item = 16,
	enBinFT_Shop = 32,
	enBinFT_Suit = 64,
	enBinFT_Scene = 128,
	enBinFT_Impact = 256,
	enBinFT_GoodsPackage = 512,
	enBinFT_Chenghao = 1024,
	enBinFT_AllResource = 2048,
	enBinFT_Quickbuy = 4096,
	enBinFT_Randomdraw = 8192,
	enBinFT_WaitRoom = 16384,
};

typedef MODI_BinFile<unsigned short, GameTable::MODI_Musiclist> defMusicBinFile;
typedef MODI_BinFile<unsigned char, GameTable::MODI_Level> defLevelBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Avatar> defAvatarBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_DefaultAvatar> defDefaultAvatarBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Itemlist> 	defItemlistBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Shop> 		defShoplistBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Suit> 		defSuitlistBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Scenelist> 	defScenelistBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Impactlist> 	defImpactlistBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_GoodsPackage> 	defGoodsPackageBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Chenghao> 	defChenghaoBinFile;
typedef	MODI_BinFile<WORD, GameTable::MODI_AllResource> defAllResourceBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Quickbuy > defQuickbuyBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_Randomdraw > defRandomdrawBinFile;
typedef MODI_BinFile<WORD, GameTable::MODI_WaitRoom > defWaitRoomBinFile;

class MODI_BinFileMgr
{
public:

	MODI_BinFileMgr();
	~MODI_BinFileMgr();

	static MODI_BinFileMgr * GetInstancePtr(); 
public:

	bool Initial(const char * szBinFilesDir , int iNoloadMask = 0 );

	defMusicBinFile & GetMusicBinFile()
	{
		return m_MusicBinFile;
	}

	defLevelBinFile & GetLevelBinFile()
	{
		 return m_LevelBinFile;
	}

	defDefaultAvatarBinFile & GetDefaultAvatarBinFile()
	{
		return m_DefaultAvatarBinFile;
	}

	defAvatarBinFile & 	GetAvatarBinFile()
	{
		return m_AvatarBinFile;
	}

	defItemlistBinFile & GetItemBinFile()
	{
		return m_ItemBinFile;
	}

	defShoplistBinFile & GetShopBinFile()
	{
		return m_ShopBinFile;
	}

	defSuitlistBinFile & GetSuitBinFile()
	{
		return m_SuitBinFile;
	}

	defScenelistBinFile & GetSceneBinFile()
	{
		return m_SceneBinFile;
	}

	defImpactlistBinFile & GetImpactBinFile()
	{
		return m_ImpactFile;
	}

	defGoodsPackageBinFile & GetGoodsPackageBinFile()
	{
		return m_GoodsPackageBinFile;
	}

	defChenghaoBinFile & GetChenghaoBinFile()
	{
		return m_ChenghaoBinFile;
	}
	
	defAllResourceBinFile & GetAllResourceBinFile()
	{
		return m_AllResourceBinFile;
	}

	defRandomdrawBinFile & GetRandomdrawBinFile()
	{
		return m_RandomdrawBinFile;
	}
	
	defWaitRoomBinFile & GetWaitRoomBinFile()
	{
		return m_WaitRoomBinFile;
	}

	/// 获得某个等级所需的经验
	bool GetNeedExpByLevel( unsigned char byLevel , unsigned int & nNeedExp );

	/** 
	 * @brief 判断CONFIG ID 在指定表里是否存在
	 * 
	 * @param nConfigID 需要检查的ID
	 * @param iMask 要检查的表格
	 * 
	 * @return 	存在返回true
	 */
	bool IsValidConfigInTables( WORD nConfigID , int iMask );

	bool GetGoodsPackage( WORD nConfigID , MODI_GoodsPackageArray * pOut );

private:

	bool CheckDeafaultAvatarTable();

	bool CheckShopTable();

	bool CheckShopGoods();

	bool CheckGoodsPackage();

private:

	defMusicBinFile m_MusicBinFile;
	defLevelBinFile m_LevelBinFile;
	defAvatarBinFile m_AvatarBinFile;
	defDefaultAvatarBinFile m_DefaultAvatarBinFile;
	defItemlistBinFile m_ItemBinFile;
	defShoplistBinFile m_ShopBinFile;
	defSuitlistBinFile m_SuitBinFile;
	defScenelistBinFile m_SceneBinFile;
	defImpactlistBinFile m_ImpactFile;
	defGoodsPackageBinFile m_GoodsPackageBinFile;
	defAllResourceBinFile  m_AllResourceBinFile;
	/// 称号表
	defChenghaoBinFile m_ChenghaoBinFile;
	/// 快速购买表
	defQuickbuyBinFile  m_QuickbuyBinFile;
	/// 宝盒随机
	defRandomdrawBinFile m_RandomdrawBinFile;
	/// 等待房间
	defWaitRoomBinFile m_WaitRoomBinFile;
};

#endif
