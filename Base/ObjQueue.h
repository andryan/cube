/**
 * @file ObjQueue.h
 * @date 2010-09-01 17:22:00 CST
 * @version $Id $
 * @author hurixin hurixing@hotmail.com
 * @brief object queue
 *
 */
#ifndef _MD_OBJQUEUE_H
#define _MD_OBJQUEUE_H

#include <queue>
#include "Global.h"

/**
 * @brief object queue class
 * 
 */
template<typename obj_type, int queue_size = 512> 
class MODI_ObjQueue
{
 public:
 	typedef std::pair<volatile bool, obj_type *  > defObjQueue;
 	MODI_ObjQueue(): m_dwQueueSize(queue_size)
	{
		ObjQueue.resize(m_dwQueueSize);
		m_dwReadAddr = 0;
		m_dwWriteAddr = 0;
	}


	~MODI_ObjQueue()
	{
		Final();
	}

	/**
     * @brief new object to queue
     *
     */
	void Init()
	{
		for(unsigned int i=0; i<m_dwQueueSize; i++)
		{
			obj_type * p_obj = new obj_type();
			p_obj->Reset();
			Put(p_obj);
		}
	}

	/**
     * @brief put a object to queue
     *
	 * @param p_obj a new object
	 * @return successful(true),failed(false)
	 *
     */
	bool Put(obj_type * p_obj)
	{
		if(!ObjQueue[m_dwWriteAddr].first)
		{
			p_obj->Reset();
			ObjQueue[m_dwWriteAddr].second = p_obj;
			ObjQueue[m_dwWriteAddr].first = true;
			m_dwWriteAddr = (++m_dwWriteAddr) % m_dwQueueSize;
			return true;
		}
		return false;
	}

	/**
     * @brief get a null obj from queue
     *
	 * @return object 
	 *
     */
	obj_type * Get()
	{
		obj_type * p_obj = NULL;
		if(ObjQueue[m_dwReadAddr].first)
		{
			p_obj = ObjQueue[m_dwReadAddr].second;
			ObjQueue[m_dwReadAddr].first = false;
			m_dwReadAddr = (++m_dwReadAddr) % m_dwQueueSize;
			return p_obj;
		}
		Global::logger->fatal("[obj_queue] objqueue get a null command failed");
		return p_obj;
	}

 	void Resize(unsigned int _size)
 	{
		Final();
		m_dwQueueSize = _size;
		ObjQueue.resize(_size);
		Init();
 	}

 private:
	/**
     * @brief release object
     *
     */
	void Final()
	{
		for(unsigned int i=0; i<m_dwQueueSize; i++)
		{
			if(ObjQueue[i].second)
			{
				delete ObjQueue[i].second;
				ObjQueue[i].second = NULL;
				ObjQueue[i].first = false;
			}
		}
		ObjQueue.clear();
	}

 	//defObjQueue ObjQueue[queue_size];
 	std::vector<std::pair<volatile bool, obj_type *  > > ObjQueue;
 	unsigned int m_dwQueueSize;
	unsigned int m_dwReadAddr;
	unsigned int m_dwWriteAddr;
};

#endif
