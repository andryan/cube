/**
 * @file ClientTask.h
 * @date 2009-12-22 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 客户端连接模型
 *
 */

#ifndef _MDCLIENTTASK_H
#define _MDCLIENTTASK_H

#include "Type.h"
#include "Thread.h"
#include "Socket.h"

/**
 * @brief 客户端连接
 *
 * 要连接服务器就主动产生
 *
 */
class MODI_ClientTask: public MODI_DisableCopy, public MODI_Thread
{
 public:
	/**
	 * @brief 构造
	 *
	 * @param client_name 客户端的名字
	 * @param server_ip 服务器的IP
	 * @param port 服务器的端口
	 *
	 */
	MODI_ClientTask(const char * client_name, const char * server_ip, const WORD & port): MODI_Thread(client_name),
		m_strServerIP(server_ip), m_wdPort(port)
	{
		m_pSocket = NULL;
	}

	~MODI_ClientTask()
	{
		TEMP_FAILURE_RETRY(::close(m_iEpollfd));
		m_pSocket = NULL;
	}
	
	/**
	 * @brief 获取服务器IP
	 *
	 * @return 服务器的IP
	 *
	 */ 
	const char * GetServiceIP() const
	{
		return m_strServerIP.c_str();
	}

	/**
	 * @brief 获取服务器的端口
	 *
	 * @return 服务器的端口
	 *
	 */ 
	const WORD & GetServicePort()
	{
		return m_wdPort;
	}

	/// 主循环
	void Run();

	/// 初始化
	virtual bool Init();

	/**
	 * @brief 发送命令
	 *
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 要发送命令的大小
	 * @param is_buffer 是否要队列发送(默认不要)
	 *
	 * @return 发送成功true,发送失败false
	 *
	 */
	bool SendCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size, bool is_buffer = false)
	{
		if(m_pSocket)
		{
			return m_pSocket->SendCmd(pt_null_cmd, cmd_size, is_buffer);
		}
		return false;
	}

	/**
	 * @brief 发送队列里面的命令
	 *
	 * @return 发送成功true,发送失败false
	 *
	 */ 
	bool SendQueueCmd()
	{
		if(m_pSocket)
		{
			return m_pSocket->SendQueueCmd();
		}
		return false;
	}

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * p_null_cmd, const int cmd_len) = 0;

	bool SendDataNoPoll()
	{
		if(!m_pSocket)
		{
			return false;
		}
		return m_pSocket->SendQueueCmdNoPoll();
	}


	virtual bool RecvDataNoPoll();

	/** 
	 * @brief 获取自己的IP
	 * 
	 * 
	 * @return 返回ip
	 */
	const char * GetIP()
	{
		return m_strClientIP.c_str();
	}

	/** 
	 * @brief 获取自己的地址
	 * 
	 * @return 返回端口
	 */
	WORD & GetPort()
	{
		return m_wdClientPort;
	}
	
 protected:
	/// 套接口
	MODI_Socket * m_pSocket;
	/// 释放连接资源
	virtual void Final()
	{
		if(m_pSocket)
		{
			delete m_pSocket;
		}
		m_pSocket = NULL;
	}

	/// 时间
	MODI_RTime m_stRTime;
	
 private:	
	
	/// 服务器IP
	const std::string m_strServerIP;

	/// 服务器端口
	const WORD m_wdPort;

	std::string m_strClientIP;
	WORD m_wdClientPort;

	/// epoll句柄
	int m_iEpollfd;

	/// 事件
	std::vector<struct epoll_event > m_stEventVec;
	
	int m_iReadEpollfd;
	std::vector<struct epoll_event > m_stReadEventVec;
};

#endif
