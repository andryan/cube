/**
 * @file PublicFun.cpp
 * @date 2009-12-28 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 一些共享的函数定义 
 *
 */

#include "XMLParser.h"
#include "Share.h"
#include "Global.h"
#include "BaseOS.h"
#include "AssertEx.h"
#include "HelpFuns.h"

/** 
 * @brief 参数解析
 * 
 * @param argc 个数
 * @param argv 参数
 * 
 */
void PublicFun::ArgParse(const int argc, char ** argv)
{
	for( int i = 1; i <= argc; i++ )
	{
		if(! strcmp(argv[i - 1], "-d"))
		{
			Global::Value["daemon"] = "true";
		}
		else if(! strcmp(argv[i - 1], "-i"))
		{
			if( i < argc )
			{
				const char * szArg = argv[i];
				Global::Value["channelid"] = szArg;
				std::string str = Global::Value["channelid"];
				i += 1;
			}
		}
		else if(! strcmp(argv[i - 1], "-p"))
		{
			if( i < argc )
			{
				const char * szArg = argv[i];
				Global::Value["dbproxyid"] = szArg;
				std::string str = Global::Value["dbproxyid"];
				i += 1;
			}
		}
	}
}

/** 
 * @brief 字符转换成hex
 * 
 *
 */
char PublicFun::HexToChar(unsigned char n)
{
    char ret;
    
    n &= 0x0f;
    if(n < 10)
        ret = (char)(n + '0');
    else
        ret = (char)(n - 10 + 'A');
    return ret;
}


char PublicFun::Str2Bin(char *str)
{
	char tempWord[2];
	char chn;
	tempWord[0] = CharToHex(str[0]);
	tempWord[1] = CharToHex(str[1]);
	chn = (tempWord[0] << 4) | tempWord[1];
	return chn;
}


/** 
 * @brief 转换成字符
 * 
 * 
 */
char PublicFun::CharToHex(char n)
{
    char ret;

    if(n <= '9' && n >= '0')
		ret = (char)(n - '0');
    else if(n <= 'F' && n >= 'A')
		ret = (char)(n - 'A' + 10);
    else if(n <= 'f' && n >= 'a')
		ret = (char)(n - 'a' + 10);
    else
           ret = -1;
    return ret;
}

/** 
 * @brief 编码
 * 
 * @param src_line 要编码的字符
 * 
 * @return 返回编码后的字符
 */
const char * PublicFun::StrEncode(const char *src_line)
{
    unsigned char ch,ch1,ch2;
    std::string result;
    result.erase();

    unsigned int size = strlen(src_line);
    for(unsigned int i=0; i<size; i++)
    {
        ch =(unsigned char)(*(src_line + i));
        if(isalnum(ch))
            result.append(1,(char)ch);
        else if(ch == ' ')
            result.append(1,'+');
        else
        {
            ch1 = ch >> 4;
            ch2 = ch & 0x0F;
            result.append(1,'%');
            result.append(1,(char)HexToChar(ch1));
            result.append(1,(char)HexToChar(ch2));
        }
    }
    return result.c_str();
}


/** 
 * @brief 解码
 * 
 * @param src_line 要解码的字符
 * 
 * @return 解码后的字符
 */
const char * PublicFun::StrDecode(const char *src_line)
{
	std::string output="";
	char tmp[2];
	std::string str = src_line;
	int i=0,len=str.length();
	while(i<len)
	{
		if(str[i]=='%')
		{
			tmp[0]=str[i+1];
			tmp[1]=str[i+2];
			output+=Str2Bin(tmp);
			i=i+3;
		}
		else if(str[i]=='+')
		{
			output+=' ';
			i++;
		}
		else
		{
			output+=str[i];
			i++;
		}
	}
	return output.c_str();
}


void PublicFun::EncryptMoney(char * buf, DWORD & out_size, const DWORD in_money, const char * acc_name)
{
	std::stringstream os_m;
	os_m.str("");
	os_m << "moneyrmb="<< in_money << ",accname=" << acc_name;
	char encrypt_money[os_m.str().size() + 10];
	memset(encrypt_money, 0, sizeof(encrypt_money));
	strncpy(encrypt_money, os_m.str().c_str(), sizeof(encrypt_money)-1);
	
	MODI_Encrypt m_stEncrypt;
	m_stEncrypt.SetEncryptType(MODI_Encrypt::ENCRYPT_TYPE_RC5);
	m_stEncrypt.Encode((unsigned char *)encrypt_money, sizeof(encrypt_money)-2);

	DWORD size = 0;
	MODI_ByteBuffer buffer(os_m.str().size() + 16);
	buffer.Reset();
	
	buffer.Write((unsigned char *)&size, sizeof(DWORD));
	memcpy(buffer.WriteBuf(), encrypt_money, sizeof(encrypt_money) - 1);
	buffer.WriteSuccess(sizeof(encrypt_money) - 1);
	size = buffer.ReadSize() - sizeof(size);
	memcpy(buffer.ReadBuf(), (unsigned char *)&size, sizeof(DWORD));
	
	memset(buf, 0, sizeof(buf));
	memcpy(buf, buffer.ReadBuf(), buffer.ReadSize());
	out_size = buffer.ReadSize();
}



/** 
 * @brief 某天变成秒数
 * 
 * @param day 某天
 * 
 * @return 秒数
 *
 */
const DWORD MODI_TimeFun::DayToSec(const char * day)
{
	if(! day)
	{
		return 0;
	}
	
	if(strlen(day) != 8)
	{
		return 0;
	}
	
	std::string str_day = day;
	DWORD dw_year = atoi((str_day.substr(0,4)).c_str());
	DWORD dw_mon = atoi((str_day.substr(4,2)).c_str());
	DWORD dw_day = atoi((str_day.substr(6,2)).c_str());

	if(dw_year > 2999 || dw_mon > 12 || dw_day > 31 || dw_mon == 0)
	{
		return 0;
	}

	struct tm tm_time;
	
	tm_time.tm_sec = 0;
	tm_time.tm_min = 0;
	tm_time.tm_hour = 0;
	tm_time.tm_mday = dw_day;
	tm_time.tm_mon = dw_mon - 1;       
	tm_time.tm_year = (dw_year - 1900);

	DWORD ret_sec = 0;
	ret_sec = mktime(&tm_time);
	return ret_sec;
}



bool 	PublicFun::StrConvToGoodsPackage( MODI_GoodsPackageArray & goods , const char * szContent , unsigned int nContentLen )
{
	if( !szContent || !nContentLen )
		return false;

	// 组信息
	const DWORD nArrayCount = 16;
	const DWORD nStrSize = 256;
	char 	szGroup[nArrayCount][nStrSize];
	char *  pGroup[nArrayCount];
	for( DWORD i = 0; i < nArrayCount; i++ )
	{
		memset( szGroup[i] , 0 , sizeof(szGroup[i]) );
		pGroup[i] = szGroup[i];
	}
	char ** ppGroup = pGroup;
	DWORD nOutSize = nArrayCount;

	if( !SplitStr( szContent , nContentLen , ';' , ppGroup , nOutSize ) )
	{
		MODI_ASSERT(0);
		return false;
	}

	// 每组的各个参数信息
	const DWORD nGroupInfoCount = 8;
	const DWORD nGroupInfoStrSize = 32;
	char 	szGroupInfo[nGroupInfoCount][nGroupInfoStrSize];
	char *  pGroupInfo[nGroupInfoCount];
	for( DWORD i = 0; i < nGroupInfoCount; i++ )
	{
		memset( szGroupInfo[i] , 0 , sizeof(szGroupInfo[i]) );
		pGroupInfo[i] = szGroupInfo[i];
	}
	char ** ppGroupInfo = pGroupInfo;

	for( unsigned int n = 0;n < nOutSize; n++ )
	{
		DWORD nOutSizeInfo = nGroupInfoCount;
		const char * pCurGroup = szGroup[n];
		if( !SplitStr( pCurGroup , strlen(pCurGroup) , ',' , ppGroupInfo , nOutSizeInfo ) )
		{
			MODI_ASSERT(0);
			return false;
		}

		MODI_GoodsPackageArray::element & tmp = goods.m_array[goods.m_count];
		unsigned int nGoodID = INVAILD_CONFIGID;
		
		if( !safe_strtoul( szGroupInfo[0]  , &nGoodID ) )
		{
			MODI_ASSERT(0);
			return false;
		}

		unsigned int nGoodCount = 0;
		if( !safe_strtoul( szGroupInfo[1] ,  &nGoodCount ) )
		{
			MODI_ASSERT(0);
			return false;
		}

		unsigned int nGoodTime = 0;
		if( !safe_strtoul( szGroupInfo[2] , &nGoodTime) )
		{
			MODI_ASSERT(0);
			return false;
		}

		if( (nGoodTime > kShopGoodT_Begin && nGoodTime < kShopGoodT_End ) == false )
		{
			MODI_ASSERT(0);
			return false;
		}

		tmp.nGoodID = nGoodID;
		tmp.nCount = nGoodCount;
		tmp.time = (enShopGoodTimeType)nGoodTime;

		goods.m_count += 1;
	}

	return true;
}
