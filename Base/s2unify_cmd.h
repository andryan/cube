/**
 * @file   s2unify_cmd.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Feb 12 12:19:34 2011
 * 
 * @brief  游戏区到全局服务器的命令
 * 
 */

#ifndef _MD_STOUNIFYCMD_H
#define _MD_STOUNIFYCMD_H

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "session_id.h"
#include "Type.h"

#pragma pack(push,1)

/// 之前没有统一，后期整理下
static const BYTE MAINCMD_S2UNIFY = 0x20;

struct MODI_S2Unify_Cmd: public stNullCmd
{
	MODI_S2Unify_Cmd()
	{
		byCmd = MAINCMD_S2UNIFY;
	}
}__attribute__((__packed__));


////////////////沉迷命令开始///////////////

/// 返回沉迷结果
struct MODI_Unify2S_ChenMiResult_Cmd: public MODI_S2Unify_Cmd
{
	MODI_Unify2S_ChenMiResult_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
		m_enResult = enChenMi_None;
	}
	/// 沉迷ACCID
	defAccountID m_dwAccID;
	///沉迷等级
	enChenMiResult m_enResult;
	
	static const BYTE ms_SubCmd = 1;
}__attribute__((packed));


/// 用户登录
struct MODI_S2Unify_Onlogin_Cmd: public MODI_S2Unify_Cmd
{
	MODI_S2Unify_Onlogin_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
	}
	/// 登陆ACCID
	defAccountID m_dwAccID;
	
	static const BYTE ms_SubCmd = 2;
}__attribute__((packed));


/// 用户登出
struct MODI_S2Unify_Onlogout_Cmd: public MODI_S2Unify_Cmd
{
	MODI_S2Unify_Onlogout_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
	}
	/// 登出的ACCID
	defAccountID m_dwAccID;
	
	static const BYTE ms_SubCmd = 3;
}__attribute__((packed));


//// gmtool 命令
struct MODI_GMTool_Cmd: public stNullCmd
{
	MODI_GMTool_Cmd()
	{
		byCmd = MAINCMD_S2UNIFY;
		m_wdZoneId = 0;
	}

	/// 那个区
	WORD m_wdZoneId;

	/// 那个gmtool
	MODI_SessionID m_stSessionID;
	
};


/// gmtool询问某个区的数据
struct MODI_QueryChannelStatus_Cmd: public MODI_GMTool_Cmd
{
	MODI_QueryChannelStatus_Cmd()
	{
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 4;
};

struct MODI_ChannelStatusInfo
{
	MODI_ChannelStatusInfo()
	{
		m_wdNum = 0;
		memset(m_cstrName, 0, sizeof(m_cstrName));
		m_nServerID = 0;
		m_nGatewayIP = 0;
		m_nGatewayPort = 0;
	}
	
	defServerChannelID m_nServerID; 
	char 	m_cstrName[MAX_SERVERNAME_LEN + 1];
	DWORD 	m_nGatewayIP; 
	WORD 	m_nGatewayPort;
	WORD 	m_wdNum;
};

/// 返回频道信息给gmtool
struct MODI_ReturnChannelStatus_Cmd: public MODI_GMTool_Cmd
{
	MODI_ReturnChannelStatus_Cmd()
	{
		m_wdMaxNum = 0;
		m_wdChannelNum = 0;
		byParam = ms_SubCmd;
	}
	
	WORD m_wdMaxNum;
	WORD m_wdChannelNum;
	MODI_ChannelStatusInfo m_pData[0];

	static const BYTE ms_SubCmd = 5;
};


/// gm 对个人的操作
enum enGMOperateUserType
{
	enGMOperateUserNone = 0,
	
	enGMDisableChatByGuid = 11,  /// 根据 角色id 对某个用户 禁言
	enGMDisableChatByAccid = 12, /// 根据 账号id 对某个用户 禁言
	enGMDisableChatByAccName = 13, /// 根据 账号名 对某个用户 禁言
	enGMDisableChatByName = 14, /// 根据 角色名 对某个用户 禁言

	enGMEnableChatByGuid = 21, /// 根据 角色id 对某个用户 开放聊天
	enGMEnableChatByAccid = 22, ///  根据 账号id 对某个用户 开放聊天
	enGMEnableChatByAccName = 23,/// 根据 账号名 对某个用户 开放聊天
	enGMEnableChatByName = 24,/// 根据 角色名 对某个用户 开放聊天

	enKickUserByGuid = 31, /// 根据 角色id 对某个用户 踢下线
	enKickUserByAccid = 32, /// 根据 账号id 对某个用户 踢下线
	enKickUserByAccName = 33, /// 根据 账号名 对某个用户 踢下线
	enKickUserByName = 34, /// 根据 角色名 对某个用户 踢下线

	enFrostUserByGuid = 41,/// 根据 角色id 封号
	enFrostUserByAccid = 42,/// 根据 账号id 封号
	enFrostUserByAccName = 43,/// 根据 账号名 封号
	enFrostUserByName = 44,/// 根据 角色名 封号

	enThawUserByGuid = 51, /// 根据 角色id 解封
	enThawUserByAccid = 52,/// 根据 账号id 解封
	enThawUserByAccName = 53,/// 根据 账号名 解封
	enThawUserByName = 54, /// 根据 角色名 解封
};


/// gm对某个玩家操作
struct MODI_Unify2S_GMOperateUser_Cmd: public MODI_GMTool_Cmd
{
	MODI_Unify2S_GMOperateUser_Cmd()
	{
		operate_type = enGMOperateUserNone;
		m_stGuid = 0;
		m_dwAccId = 0;
		memset(m_strAccName, 0, sizeof(m_strAccName));
		memset(m_strName, 0, sizeof(m_strName));
		byParam = ms_SubCmd;
	}

	QWORD m_stGuid;
	DWORD m_dwAccId;
	char m_strAccName[MAX_ACCOUNT_LEN + 1];
	char m_strName[ROLE_NAME_MAX_LEN + 1];
	
	enGMOperateUserType operate_type;
	
	static const BYTE ms_SubCmd = 6;
};

/// 操作回复
struct MODI_S2Unify_ReturnOpteate_Cmd: public MODI_GMTool_Cmd
{
	MODI_S2Unify_ReturnOpteate_Cmd()
	{
		operate_type = enGMOperateUserNone;
		m_byValue = 1;
		memset(m_strName, 0, sizeof(m_strName));
		byParam = ms_SubCmd;
	}
	
	BYTE m_byValue;
	char m_strName[ROLE_NAME_MAX_LEN + MAX_ACCOUNT_LEN];
	enGMOperateUserType operate_type;
	static const BYTE ms_SubCmd = 7;
};

/// gmtool和某个玩家私聊
struct MODI_Unify2S_GMChat_Cmd: public MODI_GMTool_Cmd
{
	MODI_Unify2S_GMChat_Cmd()
	{
		operate_type = enGMOperateUserNone;
		m_stGuid = 0;
		m_dwAccId = 0;
		memset(m_strAccName, 0, sizeof(m_strAccName));
		memset(m_strName, 0, sizeof(m_strName));
		memset(m_strContent, 0, sizeof(m_strContent));
		byParam = ms_SubCmd;
	}

	QWORD m_stGuid;
	DWORD m_dwAccId;
	char m_strAccName[MAX_ACCOUNT_LEN + 1];
	char m_strName[ROLE_NAME_MAX_LEN + 1];
	char m_strContent[128];
	enGMOperateUserType operate_type;
	
	static const BYTE ms_SubCmd = 8;
};

/// 发系统公告
struct MODI_Unify2S_SystemChat_Cmd: public MODI_GMTool_Cmd
{
	MODI_Unify2S_SystemChat_Cmd()
	{
		memset(m_cstrContent, 0, sizeof(m_cstrContent));
		byParam = ms_SubCmd;
	}
	
	char m_cstrContent[256];
	static const BYTE ms_SubCmd = 9;
};

/// 发送邮件
struct MODI_Unify2S_SendMail_Cmd: public MODI_GMTool_Cmd
{
	MODI_Unify2S_SendMail_Cmd()
	{
		memset(m_strName, 0, sizeof(m_strName));
		memset(m_szCaption, 0, sizeof(m_szCaption));
		memset(m_szContents, 0, sizeof(m_szContents));
		memset(m_szAttach, 0, sizeof(m_szAttach));
		m_byType = 0;
		byParam = ms_SubCmd;
	}

	/// 因为可以是条件，所有大点
	char m_strName[512];
	char m_szCaption[MAIL_CAPTION_MAX_LEN + 1]; // 邮件标题
	char m_szContents[MAIL_CONTENTS_MAX_LEN + 1]; // 内容

	/// 类型
	BYTE m_byType;

	char m_szAttach[1024];
	static const BYTE ms_SubCmd = 10;
};

/// 设置频道人数
struct MODI_Unify2S_SetChannelNum_Cmd: public MODI_GMTool_Cmd
{
	MODI_Unify2S_SetChannelNum_Cmd()
	{
		memset(m_cstrContent, 0, sizeof(m_cstrContent));
		byParam = ms_SubCmd;
	}

	char m_cstrContent[256];
	static const BYTE ms_SubCmd = 11;
};
#pragma pack(pop)	





#endif

