/** 
 * @file SvrPackageProcessBase.h
 * @brief 服务器数据包处理基础类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */
#ifndef SERVERPACKAGEPROCESSBASE_H_
#define SERVERPACKAGEPROCESSBASE_H_

#include "ServiceTask.h"
#include "RecurisveMutex.h"
#include "CommandQueue.h"
#include <list>

//struct MODI_PackageCallBack
//{
//	virtual ~MODI_PackageCallBack(){}
//	virtual bool Done(const MODI_CmdParse * package) = 0;
//};
//

class  	MODI_IServerClientTask : public MODI_ServiceTask , public MODI_CmdParse
{
	public:

		MODI_IServerClientTask(const int sock, const struct sockaddr_in * addr);
		virtual ~MODI_IServerClientTask() = 0; 

	public:

		void 	SetCanDel( bool b ) { m_bCanDel = b ;}
		bool 	IsCanDel() const { return m_bCanDel; }

		// 连接上的回调,可以安全的在逻辑线程调用
		virtual void OnConnection() {}

		// 断开的回调，可以安全的在逻辑线程调用
		virtual void OnDisconnection() {}

	private:

		// 告诉回收线程是否可以安全删除对象
		bool 	m_bCanDel;

};

class 	MODI_SvrPackageProcessBase
{
	public:

		MODI_SvrPackageProcessBase( );
		virtual ~MODI_SvrPackageProcessBase();

		/** 
		 * @brief 有连接进入时调用
		 * 
		 * @param p 新的task对象
		 */
		void 	OnAcceptConnection( const MODI_IServerClientTask * p );

		// 处理所有数据包
		void 	ProcessAllPackage();

//		void	ExecAllPackage(MODI_PackageCallBack & call_back);

	private:
			

		typedef std::list<const MODI_IServerClientTask *>  	SETS;
		typedef SETS::iterator 				SETS_ITER;

		MODI_RecurisveMutex 	m_mutexAddlist;
		SETS 					m_setAddlist;

		SETS 					m_setCmdParse;
};



#endif
