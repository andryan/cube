#include "HelpFuns.h"
#include "AssertEx.h"

bool safe_strtoull(const char *str, unsigned long long *out) 
{
    assert(out != NULL);
    errno = 0;
    *out = 0;
    char *endptr;
    unsigned long long ull = strtoull(str, &endptr, 10);
    if (errno == ERANGE)
        return false;
    if (isspace(*endptr) || (*endptr == '\0' && endptr != str)) 
	{
        if ((long long) ull < 0) 
		{
            if (strchr(str, '-') != NULL) 
			{
                return false;
            }
        }
        *out = ull;
        return true;
    }
    return false;
}

bool safe_strtoll(const char *str, long long *out) 
{
    assert(out != NULL);
    errno = 0;
    *out = 0;
    char *endptr;
    long long ll = strtoll(str, &endptr, 10);
    if (errno == ERANGE)
        return false;
    if (isspace(*endptr) || (*endptr == '\0' && endptr != str)) 
	{
        *out = ll;
        return true;
    }
    return false;
}

bool safe_strtoul(const char *str, unsigned int *out) 
{
    char *endptr = NULL;
    unsigned long l = 0;
    assert(out);
    assert(str);
    *out = 0;
    errno = 0;

    l = strtoul(str, &endptr, 10);
    if (errno == ERANGE) 
	{
        return false;
    }

    if (isspace(*endptr) || (*endptr == '\0' && endptr != str)) 
	{
        if ((long) l < 0) 
		{
            if (strchr(str, '-') != NULL) 
			{
                return false;
            }
        }
        *out = l;
        return true;
    }

    return false;
}

bool safe_strtol(const char *str, long *out) 
{
    assert(out != NULL);
    errno = 0;
    *out = 0;
    char *endptr;
    long l = strtol(str, &endptr, 10);
    if (errno == ERANGE)
        return false;
    if (isspace(*endptr) || (*endptr == '\0' && endptr != str)) 
	{
        *out = l;
        return true;
    }
    return false;
}


char		Value2Ascii(char in)
{
	static const char s_Ascii[] = "0123456789ABCDEF";
	if( in >= 0 && in <=15 )
	{
		return s_Ascii[(int)in];
	}
	assert(0);
	return '?';
}

char Ascii2Value(char in)
{
	static const char s_Value1[] = { 0 , 1 , 2, 3, 4, 5, 6, 7, 8 , 9 };
	static const char s_Value2[] = { 10 , 11, 12 , 13, 14, 15 };
	
	if( in >= '0' && in <= '9' )
	{
		return s_Value1[in - '0'];
	}
	else if( in >= 'A' && in <= 'F' )
	{
		return s_Value2[in - 'A'];
	}
	assert(0);
	return '?';
}


bool	Binary2String(const char* pIn,unsigned int InLength,char* pOut)
{
	if(!InLength)
		return false;

	unsigned int iOut = 0;

	for(unsigned int i = 0 ;i<InLength;i++)
	{
		pOut[iOut] = Value2Ascii(((unsigned char)pIn[i]&0xF0)>>4);
		iOut++;
		pOut[iOut] = Value2Ascii(pIn[i]&0x0F);
		iOut++;
	}

	return true;

}


bool DBStr2Binary(const char* pIn,unsigned int InLength,char* pOut,unsigned int OutLimit,unsigned int& OutLength)
{
	if(!InLength)
		return false;

	unsigned int iOut = 0;
	unsigned int i;
	for( i = 0 ;i<InLength-1;)
	{
		if(pIn[i]=='\0'||pIn[i+1]=='\0')
		{
			break;
		}

		pOut[iOut]	=	(Ascii2Value(pIn[i])<<4) + Ascii2Value(pIn[i+1]);
		iOut++;
		i+=2;
		if(iOut>=OutLimit)
			break;
	}
	OutLength = iOut;
	return true;
}


bool RandomBingo(DWORD nPro,DWORD nMax )
{
	DWORD n = rand()%nMax;
	if( n < nPro )
		return true;
	return false;
}

bool SplitStr( const char * szIn , unsigned int nInSize ,const char szSplitChar , char ** pOut , unsigned int & nOutSize )
{
	if( !szIn || szIn[0] == '\0' || !nOutSize )
		return false;

	unsigned int nCount = 0;
	unsigned int nIdx = 0;
	for( unsigned int n = 0; n < nInSize && nCount < nOutSize; n++ )
	{
		if( szIn[n] != szSplitChar )
		{
			pOut[nCount][nIdx++]= szIn[n];
		}
		else
		{
			pOut[nCount][nIdx] = '\0';
			nIdx = 0;
			nCount += 1;
		}
	}

	if( szIn[nInSize - 1] != szSplitChar )
	{
		pOut[nCount][nIdx] = '\0';
		nCount += 1;
	}

	nOutSize = nCount;
	return true;
}

void Tolower( const char * in , unsigned int in_len , char * out )
{
	unsigned int n = 0; 
	for( n = 0; n < in_len; n++ )
	{
		out[n] = tolower(in[n]);
	}
	out[n] = '\0';
}

/*
 * "replacement" for the strncpy() function. We roll our
 * own to implement these specific changes:
 *   (1) strncpy() doesn't always null terminate and we want it to.
 *   (2) strncpy() null fills, which is bogus, esp. when copy 8byte
 *       strings into 8k blocks.
 *   (3) Instead of returning the pointer to the beginning of
 *       the destination string, we return a pointer to the
 *       terminating '\0' to allow us to "check" for truncation
 *
 * apr_cpystrn() follows the same call structure as strncpy().
 */
char * safe_strncpy(char *dst, const char *src, size_t dst_size)
{
    char *d, *end;

    if (dst_size == 0) {
        return (dst);
    }

    d = dst;
    end = dst + dst_size - 1;

    for (; d < end; ++d, ++src) {
	if (!(*d = *src)) {
	    return (d);
	}
    }

    *d = '\0';	/* always null terminate */

    return (d);
}
