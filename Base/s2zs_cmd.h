/** 
 * @file s2zs_cmd.h
 * @brief zone server 协议定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef MODI_STOZONESERVER_CMD_H_
#define MODI_STOZONESERVER_CMD_H_

#ifdef _MSC_VER
#pragma once
#endif

#include <string>
#include <list>
#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "gamestructdef.h"
#include "ServerConfig.h"

using std::list;
using std::string;

#pragma pack(push,1)

static const unsigned char MAINCMD_S2ZS = 0x14;

struct MODI_S2ZS_Cmd : public stNullCmd
{
	MODI_S2ZS_Cmd()
	{
		byCmd = MAINCMD_S2ZS;
	}
};

struct MODI_ZS2GS_Redirectional : public MODI_S2ZS_Cmd
{
    MODI_SessionID m_sessionID;
	MODI_GUID 	m_guid;
    unsigned short m_nSize;
    char m_pData[0];

	MODI_ZS2GS_Redirectional()
    {
		byParam = ms_SubCmd;
        m_nSize = 0;
    }
	static const unsigned char ms_SubCmd = 244;
};

struct MODI_GS2ZS_Redirectional : public MODI_S2ZS_Cmd
{
    MODI_SessionID m_sessionID;
	MODI_GUID 	m_guid;
    unsigned short m_nSize;
    char m_pData[0];

	MODI_GS2ZS_Redirectional()
    {
		byParam = ms_SubCmd;
        m_nSize = 0;
    }
	static const unsigned char ms_SubCmd = 245;
};


// gameserver 注册自己
struct MODI_S2ZS_Request_RegSelf : public MODI_S2ZS_Cmd
{
	BYTE 			m_nWorldID;
	BYTE 			m_ServerID;
	char 			m_szServerName[MAX_SERVERNAME_LEN + 1]; // 服务器名字
	DWORD 			m_nGatewayIP; 							// 对应网关IP
	WORD 			m_nGatewayPort;  						// 对应网关PORT 

	MODI_S2ZS_Request_RegSelf()
	{
		byParam = ms_SubCmd;
		m_nWorldID = 0;
		m_ServerID = 0;
		memset( m_szServerName , 0 , sizeof(m_szServerName) );
		m_nGatewayIP = 0;
		m_nGatewayPort = 0;
	}
	
	static const unsigned char ms_SubCmd = 1;
};

// gameserver 通知有客户端连入
struct MODI_S2ZS_Request_ClientLogin : public MODI_S2ZS_Cmd
{
	defAccountID 	m_accid;
	MODI_SessionID 	m_sessionID;

	MODI_S2ZS_Request_ClientLogin()
	{
		m_accid = INVAILD_ACCOUNT_ID;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 2;
};

// gameserver 通知有客户端登出
struct MODI_S2ZS_Request_ClientLoginOut : public MODI_S2ZS_Cmd
{
	MODI_SessionID 	m_sessionID;
	MODI_GUID 		m_guid;
	MODI_CharactarFullData 	m_fullData;

	MODI_S2ZS_Request_ClientLoginOut()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 3;
};

// gameserver 通知有客户端请求切换频道
struct MODI_S2ZS_Request_RechangeChannel : public MODI_S2ZS_Cmd
{
	MODI_GUID 		m_guid;
	MODI_LoginKey 	m_key;
	MODI_CharactarFullData m_fullData;

	MODI_S2ZS_Request_RechangeChannel()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 4;
};

// gameserver 通知某个客户端选定一个频道请求进入
struct MODI_S2ZS_Request_SelectChannel : public MODI_S2ZS_Cmd
{
	MODI_GUID 		m_guid;
	BYTE 			m_byServer;
	char m_szNetType[8];

	MODI_S2ZS_Request_SelectChannel()
	{
		byParam = ms_SubCmd;
		memset(m_szNetType, 0, sizeof(m_szNetType));
	}

	static const unsigned char ms_SubCmd = 5;
};


// zs 返回角色列表给 ls
struct MODI_ZS2S_Notify_Charlist : public MODI_S2ZS_Cmd
{
	BYTE 			m_bySuccessful;
	defAccountID 	m_accid;
	MODI_SessionID 	m_sessionID;
	BYTE  m_nRoleCount; // 角色个数
	MODI_CharInfo  m_pChars[0]; // 具体每个角色的信息

	MODI_ZS2S_Notify_Charlist()
	{
		m_bySuccessful = 0;
		m_accid = INVAILD_ACCOUNT_ID;
		m_nRoleCount = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 7;
};

struct MODI_ZS2S_Notify_LogKickSb : public MODI_S2ZS_Cmd
{
	defAccountID 	m_accid;
	MODI_SessionID 	m_sessionID;
	MODI_ZS2S_Notify_LogKickSb()
	{
		byParam = ms_SubCmd;
		m_accid = INVAILD_ACCOUNT_ID;
	}
	static const unsigned char ms_SubCmd = 9;
};

// gs 向 zs 发送 客户端请求进入频道
struct MODI_S2ZS_Request_EnterChannel : public MODI_S2ZS_Cmd
{
	MODI_SessionID 		m_nSessionID; 
	defAccountID 	 	m_accid;
	MODI_CHARID 		m_charid;

	MODI_S2ZS_Request_EnterChannel()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 30;
};

// zs 对 gs 客户端进入频道请求的回复
struct MODI_ZS2S_Notify_EnterChannelRet : public MODI_S2ZS_Cmd
{
	BYTE 			m_bySuccessful;
	defAccountID 	m_accid;
	MODI_SessionID 	m_sessionID;
	MODI_CharactarFullData 	m_fullData;

	MODI_ZS2S_Notify_EnterChannelRet()
	{
		m_bySuccessful = 0;
		m_accid = INVAILD_ACCOUNT_ID;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 31;
};

/// zs 让 gs 踢人下线
struct MODI_ZS2S_Notify_KickSb : public MODI_S2ZS_Cmd
{
	enKickReason m_byReason;
	MODI_SessionID 	m_sessionID;
	MODI_GUID 	m_guid;
	MODI_SessionID m_LoginServerSessionID;
	BYTE 		m_bySelectSverver;

	MODI_ZS2S_Notify_KickSb()
	{
		byParam = ms_SubCmd;
		m_bySelectSverver = 0;
	}

	static const unsigned char ms_SubCmd = 32;
};

// gs 告知 zs 踢人的结果
struct MODI_S2ZS_Request_KickSbResult : public MODI_S2ZS_Cmd
{
	enum enResult
	{
		enNotFound,
		enFound,
	};

	enResult 		m_byResult;
	enKickReason 	m_byReason;
	MODI_GUID 		m_guid;
	MODI_SessionID 	m_LoginServerSessionID;
	BYTE 			m_bySelectSverver;
	MODI_CharactarFullData 	m_fullData;

	MODI_S2ZS_Request_KickSbResult()
	{
		m_byResult = enNotFound;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 33;
};

struct MODI_S2ZS_Request_RegDBProxy : public MODI_S2ZS_Cmd
{
	DBProxyType 	m_type;
	BYTE 			m_servers[255];
	MODI_DBMail_ID 	m_lastMailID;

	MODI_S2ZS_Request_RegDBProxy()
	{
		m_type = kZoneServerProxy;
		memset( m_servers , 0 ,  sizeof(m_servers) );
		m_lastMailID = INVAILD_DBMAIL_ID; 	
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 50;
};

struct MODI_S2ZS_Notify_UpdateObj : public MODI_S2ZS_Cmd
{	
	MODI_GUID 			m_guid; 		// 	需要更新对象
	MODI_ObjUpdateMask 	m_UpdateMask;  	// 	更新掩码
	WORD 				m_nSize; 		// 	数据大小
	char 				m_pData[0]; 	// 	内容

	MODI_S2ZS_Notify_UpdateObj()
	{
		m_nSize = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 100;
};


enum enZS2SOptReason
{
	kNoneOptReason,
	kGetMailAttachment, /// 获得系统邮件附件
	kGetOtherAttachment, /// 玩家赠送
};

enum enZS2SOptResult
{
	kZS2S_NoneResult = 0,
	kZS2S_Successful = 1,
	kZS2S_CannotFindClient,
	kZS2S_BagFull, // 背包满
	kZS2S_UnknowErr,
};

#define ZS2S_OPT_BUFSIZE 100

// zs让gs给某个客户端加钱
struct MODI_ZS2S_Request_AddMoeny : public MODI_S2ZS_Cmd
{
	enZS2SOptReason m_reason;
	MODI_GUID 		m_guid; // 客户端
	DWORD 			m_nMoeny; // 货币数量
	char 			m_szData[ZS2S_OPT_BUFSIZE];

	MODI_ZS2S_Request_AddMoeny()
	{
		m_nMoeny = 0; // 货币数量
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 150;
};

/// gs返回加钱的结果
struct MODI_ZS2S_Notify_AddMoenyRes : public MODI_S2ZS_Cmd
{
	enZS2SOptReason m_reason;
	MODI_GUID 		m_guid; // 客户端
	enZS2SOptResult  m_result;
	char 			m_szData[ZS2S_OPT_BUFSIZE];

	MODI_ZS2S_Notify_AddMoenyRes()
	{
		m_reason = kGetMailAttachment;
		m_result = kZS2S_NoneResult;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 151;
};

// zs让gs给某个客户端加物品
struct MODI_ZS2S_Request_AddItem : public MODI_S2ZS_Cmd
{
	enZS2SOptReason m_reason;
	MODI_GUID 		m_guid; 
	WORD 			m_nConfigID; 
	WORD 			m_nCount;
	time_t 			m_ExpireTime; 
	char 			m_szData[ZS2S_OPT_BUFSIZE];
	QWORD 			m_transid;

	MODI_ZS2S_Request_AddItem()
	{
		m_reason = kNoneOptReason;
		m_nConfigID = INVAILD_CONFIGID; 
		m_nCount = 0;
		m_ExpireTime = enOneHour; 
		m_transid = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 152;
};

// gs返回添加物品的结果
struct MODI_ZS2S_Notify_AddItemRes : public MODI_S2ZS_Cmd
{
	enZS2SOptReason m_reason;
	MODI_GUID 		m_guid; 
	enZS2SOptResult m_result;
	char 			m_szData[ZS2S_OPT_BUFSIZE];
	QWORD 			m_transid;

	MODI_ZS2S_Notify_AddItemRes()
	{
		m_reason = kGetMailAttachment;
		m_result = kZS2S_NoneResult;
		m_transid = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 153;
};

// gs->zs 通过邮件给予物品
struct MODI_S2ZS_Request_SendGiveItemMail : public MODI_S2ZS_Cmd
{
	MODI_CHARID 			m_sender_charid;
	bool 					m_shopmail;
	enShopTransactionType 	m_type;
	char 					m_szSenderName[ROLE_NAME_MAX_LEN + 1];
	char 					m_szReceverName[ROLE_NAME_MAX_LEN + 1];
	char 					m_szContent[MAIL_CONTENTS_MAX_LEN + 1];
	char 					m_szCaption[MAIL_CAPTION_MAX_LEN + 1];

	struct tagGoods
	{
		WORD 		m_nItemConfigID; // 物品
		WORD 		m_nCount; // 个数
		DWORD 		m_nLimitTime; // 期限
	};

	BYTE 					m_nCount;
	tagGoods 				m_Goods[ONCE_TRANSACTION_MAX_GOODSCOUNT];
	QWORD 					m_TransID;
	bool 					m_bCoastMoneyScrpit;
	DWORD 					m_nCoastMoney;

	MODI_S2ZS_Request_SendGiveItemMail()
	{
		m_sender_charid = INVAILD_CHARID;
		m_shopmail= true;
		memset( m_szSenderName , 0 , sizeof(m_szSenderName) );
		memset( m_szReceverName , 0 , sizeof(m_szReceverName) );
		memset( m_szContent , 0 , sizeof(m_szContent) );
		memset(m_szCaption, 0, sizeof(m_szCaption));
		m_nCount = 0;
		m_TransID = 0;
		m_bCoastMoneyScrpit = false;
		m_nCoastMoney = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 154;
};

inline void 	EncodeZS2SOpt_GetAttachment( char * szData ,  const MODI_GUID & mailid )
{
	memcpy( szData , &mailid , sizeof(mailid) );
}

inline void 	DecodeZS2SOpt_GetAttachment( const char * szData , MODI_GUID & mailid )
{
	memcpy( &mailid , szData , sizeof(mailid) );
}

enum enQueryCharOptType
{
	kQueryChar_Shop_Give,
	kQueryChar_Shop_AskFor,
};

struct MODI_S2ZS_Request_QueryHasChar_Shop : public MODI_S2ZS_Cmd
{
	enQueryCharOptType	  	m_type;
	MODI_CHARID 			m_queryClient;
	BYTE 					m_nServerID;
	char 					m_szRoleName[ROLE_NAME_MAX_LEN + 1];
	WORD 					m_nSize;
	char 					m_pExtraData[0];

	MODI_S2ZS_Request_QueryHasChar_Shop()
	{
		m_type = kQueryChar_Shop_AskFor;
		m_queryClient = INVAILD_CHARID;
		m_nServerID = 0;
		memset( m_szRoleName , 0 , sizeof(m_szRoleName) );
		m_nSize = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 155;
};

struct MODI_ZS2S_Notify_QueryHasCharResult_Shop : public MODI_S2ZS_Cmd
{
	bool 					m_bHashChar;
	enQueryCharOptType  	m_type;
	BYTE 					m_sex;
	MODI_CHARID 			m_queryClient;
	BYTE 					m_nServerID;
	char 					m_szRoleName[ROLE_NAME_MAX_LEN + 1];
	WORD 					m_nSize;
	char 					m_pExtraData[0];

	MODI_ZS2S_Notify_QueryHasCharResult_Shop()
	{
		m_bHashChar = false;
		m_type = kQueryChar_Shop_Give;
		m_nServerID = 0;
		m_queryClient = INVAILD_CHARID;
		m_sex = enSexAll;
		memset( m_szRoleName , 0 , sizeof(m_szRoleName) );
		m_nSize = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 156;
};

struct MODI_S2ZS_Request_SbEnterRare : public MODI_S2ZS_Cmd
{
	char 			m_szName[ROLE_NAME_MAX_LEN + 1];

	MODI_S2ZS_Request_SbEnterRare()
	{
		memset( m_szName , 0 , sizeof(m_szName) );
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 157;
};

struct MODI_ZS2S_Notify_ResetOnlineTime : public MODI_S2ZS_Cmd
{
	DWORD 	m_todayOnlineTime;
	MODI_ZS2S_Notify_ResetOnlineTime()
	{
		m_todayOnlineTime = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 158;
};


struct MODI_ZS2S_Notify_RefreshMoney : public MODI_S2ZS_Cmd
{
	MODI_CHARID m_charid;
	DWORD 		m_RMBMoney;
	BYTE m_byMoneyType;
	enMRefreshMoney m_enReason;
	MODI_ZS2S_Notify_RefreshMoney()
	{
		m_charid = INVAILD_CHARID;
		m_RMBMoney = 0;
		m_byMoneyType = 2; /// default is rmb
		m_enReason = enMRefreshMoney_None;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 159;
};

/// 称号改变 通知gs
struct MODI_ZS2S_Notify_ChangChenghao: public MODI_S2ZS_Cmd
{
	MODI_CHARID m_charid;
	WORD m_wdChenghao;
	MODI_ZS2S_Notify_ChangChenghao()
	{
		m_charid = INVAILD_CHARID;
		m_wdChenghao = 0;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 160;
};

//通知角色防沉迷
struct MODI_ZS2S_Notify_ChenMi_Cmd: public MODI_S2ZS_Cmd
{
	MODI_ZS2S_Notify_ChenMi_Cmd()
	{
		m_charid = INVAILD_CHARID;
		m_enResult = enChenMi_None;
		byParam = ms_SubCmd;
	}
	
	MODI_CHARID m_charid;
	enChenMiResult m_enResult;

	static const BYTE ms_SubCmd = 161;
};


/// 角色家族信息通信
struct MODI_ZS2S_Notify_FamilyInfo_Cmd: public MODI_S2ZS_Cmd
{
	MODI_ZS2S_Notify_FamilyInfo_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		//m_byNeedCardName = 0;
		m_enReason = enNone_Reason;
	}

	defAccountID m_dwAccountID;
	//BYTE m_byNeedCardName; /// 是否需要告知所有频道的人家族名片
	MODI_FamilyMemBaseInfo m_stCFamilyInfo;
	enUpDateCFReason m_enReason;
	
	static const BYTE ms_SubCmd = 162;
};


/// 通知金币的处理
struct MODI_ZS2S_Notify_OptMoney : public MODI_S2ZS_Cmd
{
	enum enOptType
	{
		enOptBegin = 0,
		enOptAddMoney, /// 增加
		enOptSubMoney, /// 减
		enOptSetMoney, /// 置 
		enOptEnd,
	};

	enum enOptReason
	{
		enReasonBegin = 0,
		enReasonCFFailed, /// 家族创建失败
		enReasonEnd,
	};
	
	MODI_CHARID m_charid;
	DWORD 		m_dwMoney;
	enOptType m_enOpt;
	enOptReason m_enReason;
	
	MODI_ZS2S_Notify_OptMoney()
	{
		m_charid = INVAILD_CHARID;
		m_dwMoney = 0;
		m_enOpt = enOptBegin;
		m_enReason = enReasonBegin;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 163;
};


/// 修改gm
struct MODI_ZS2S_Notify_Modi_GMLevel: public MODI_S2ZS_Cmd
{
	MODI_ZS2S_Notify_Modi_GMLevel()
	{
		m_charid = INVAILD_CHARID;
		m_byLevel = 0;
		byParam = ms_SubCmd;
	}
	MODI_CHARID m_charid;
	BYTE m_byLevel;
	static const BYTE ms_SubCmd = 164;
};

///	服务器广播刷新全局变量列表
struct MODI_ZS2S_Notify_GlobalVarList : public MODI_S2ZS_Cmd
{

	MODI_ZS2S_Notify_GlobalVarList()
	{
		byParam = ms_SubCmd;
	}
	BYTE	m_nCount;
	MODI_GlobalVar	m_stGlobalArray[0];
	static const BYTE ms_SubCmd = 165;
};

///	 服务器同步某个变量的值
struct MODI_ZS2S_Notify_VarChanged : public MODI_S2ZS_Cmd
{
	MODI_ZS2S_Notify_VarChanged()
	{
		byParam = ms_SubCmd;
	}
	MODI_GlobalVar  m_stGlobalVar;

	static const BYTE ms_SubCmd = 167;
};


///	Game Server告知ZoneServer 某个变量修改了
struct  MODI_S2ZS_Request_ChangeVar : public  MODI_S2ZS_Cmd
{
	MODI_S2ZS_Request_ChangeVar()
	{
		byParam = ms_SubCmd;
	}

	MODI_GlobalVar  m_stGlobalVar;
	static const BYTE ms_SubCmd = 168;
};

/// 服务器通知ZONE 会员状态改变了
struct MODI_S2ZS_Request_ChangeVip : public MODI_S2ZS_Cmd
{
	BYTE	m_bVip;
	DWORD	m_dwCharid;
	MODI_S2ZS_Request_ChangeVip()
	{
		m_dwCharid = 0;	
		m_bVip= 0;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 169;
};

///	当音乐结束的时候，gameserver通知zoneserver更新数据信息
struct  MODI_S2ZS_Request_MusicEnd : public  MODI_S2ZS_Cmd
{
	enGameMode   m_eMode;
	struct SingleMusicInfo  m_stMusic;
	MODI_S2ZS_Request_MusicEnd()
	{
		m_eMode= enGameMode_Invalid;
		
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 170;
};

///ZS通知GS刷新单曲列表
struct MODI_ZS2S_Notify_SingleMusicList : public MODI_S2ZS_Cmd
{
	enGameMode  m_eMode;
	WORD	m_wNum;
	struct SingleMusicInfo   m_stMusic[0];

	MODI_ZS2S_Notify_SingleMusicList()
	{
		m_wNum = 0;
		m_eMode = enGameMode_Invalid;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 171;
};


/// 服务器通知删除好友
struct MODI_S2ZS_Request_DelFrindandIdole : public MODI_S2ZS_Cmd
{
	WORD	m_nFriend;
	WORD	m_nIdole;
	WORD	m_nBlack;
	DWORD	m_dwCharid;

	MODI_S2ZS_Request_DelFrindandIdole()
	{
		m_dwCharid = 0;
		m_nFriend = 300;
		m_nIdole = 300;
		m_nBlack = 300;	
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 172;
};

// gs->zs 通过邮件给予物品
struct MODI_S2ZS_Request_SendSysMail : public MODI_S2ZS_Cmd
{
	char 					m_szSenderName[ROLE_NAME_MAX_LEN + 1];
	char 					m_szReceverName[ROLE_NAME_MAX_LEN + 1];
	char 					m_szContent[MAIL_CONTENTS_MAX_LEN + 1];
	char 					m_szCaption[MAIL_CAPTION_MAX_LEN + 1];



	MODI_S2ZS_Request_SendSysMail()
	{
		memset( m_szSenderName , 0 , sizeof(m_szSenderName) );
		memset( m_szReceverName , 0 , sizeof(m_szReceverName) );
		memset( m_szContent , 0 , sizeof(m_szContent) );
		memset(m_szCaption, 0, sizeof(m_szCaption));
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 173;
};


struct MODI_S2ZS_Modify_Renqi : public  MODI_S2ZS_Cmd
{
	WORD	m_nRenqi;
	MODI_CHARID 	m_dwCharid;

	MODI_S2ZS_Modify_Renqi()
	{
		m_dwCharid = 0;
		m_nRenqi = 0;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 174;

};


struct MODI_ZS2S_Broadcast_Cmd : public MODI_S2ZS_Cmd
{
	WORD	m_nSize;
	char	m_csCmd[0];

	MODI_ZS2S_Broadcast_Cmd()
	{
		m_nSize= 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 175;

};


struct MODI_ZS2S_Broadcast_CmdRange : public MODI_S2ZS_Cmd
{
	WORD	m_nSize;
	WORD	m_nMemNum;

	char	m_csCmd[0];

	MODI_ZS2S_Broadcast_CmdRange()
	{
		m_nSize = 0;
		m_nMemNum = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 176;

};

///  频道发送服务器广播
struct MODI_S2ZS_Broadcast_SystemChat: public MODI_S2ZS_Cmd
{
	MODI_S2ZS_Broadcast_SystemChat()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}
	
	WORD m_wdSize;
	char m_pContent[0];
	static const BYTE ms_SubCmd = 177;
};

#pragma pack(pop)
#endif












