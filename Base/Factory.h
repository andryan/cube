/** 
 * @file Factory.h
 * @brief 工厂模板
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-08
 */
#ifndef FACTOR_H_
#define FACTOR_H_

#ifdef _MSC_VER
#pragma once
#endif

#include "SingleObject.h"
#include "functionPtr.h"

#include <map>
#include <vector>

/** 
 * @brief 工厂模板
 */
template < typename BaseType , typename ID = unsigned int , typename l = std::less<ID> >
class	CFactory	: public CSingleObject< CFactory<BaseType,ID,l> >
{

public:

	CFactory() { }
	~CFactory() { m_mapFactor.clear(); }

public:

	typedef BaseType * (*FnPtr)();

	void	EnumerateTypes( std::vector<ID> & vecOut );
	BaseType * Create( ID id );
	void  RegisterObject( ID id , FnPtr pfn );

protected:

	std::map< ID , FnPtr , l >		m_mapFactor;
};


template < typename BaseType , typename ID , typename l  >
	void CFactory<BaseType,ID,l>::RegisterObject(ID id , FnPtr pfn )
{
	std::map< ID , FnPtr , l >::iterator itor = m_mapFactor.find( id );
	if( itor == m_mapFactor.end() )
	{
		m_mapFactor.insert( std::make_pair( id , pfn ) );
	}
}

template < typename BaseType , typename ID , typename l >
	BaseType * CFactory<BaseType,ID,l>::Create(ID id )
{
	BaseType * pReturn = 0;
	std::map< ID , FnPtr , l >::iterator itor = m_mapFactor.find( id );
	if( itor != m_mapFactor.end() )
	{
		FnPtr pfn = itor->second;
		pReturn = pfn();
	}
	return pReturn;
}

template < typename BaseType , typename ID , typename l >
	void	CFactory<BaseType,ID,l>::EnumerateTypes( std::vector<ID> & vecOut )
{
	vecOut.clear();
	std::map< ID ,FnPtr , l >::iterator itor = m_mapFactor.begin();
	while( itor != m_mapFactor.end() )
	{
		const ID & id = itor->first;
		vecOut.push_back( id );
		itor++;
	}
}

#endif
