#include "Channellist.h"
#include "AssertEx.h"
#include "protocol/c2gs.h"
#include "ServiceTask.h"
#include "XMLParser.h"
#include "Share.h"

MODI_SvrChannellist::MODI_SvrChannellist()
{

}

MODI_SvrChannellist::~MODI_SvrChannellist()
{


}

int 	MODI_SvrChannellist::CreateSendStream( char * pBuf )
{
	MODI_LS2C_Notify_Channellist * pMsg = (MODI_LS2C_Notify_Channellist *)(pBuf);
	AutoConstruct( pMsg );
	unsigned int n = 0;
	pMsg->m_nCount = this->Size();
	TMAPS::const_iterator itor = this->m_mapContents.begin();
	while( itor != this->m_mapContents.end() )
	{
		MODI_ServerStatus * pTemp = itor->second;
		memcpy( &(pMsg->m_pData[n]) , pTemp , sizeof(MODI_ServerStatus) );

		itor++;
		n += 1;
	}
   	
	MODI_ASSERT( n == pMsg->m_nCount );

	int iSendSize = sizeof(MODI_LS2C_Notify_Channellist) + sizeof(MODI_ServerStatus) * pMsg->m_nCount;

	return iSendSize;
}

int 	MODI_SvrChannellist::CreateSendPackage( MODI_LS2C_Notify_Channellist  * pMsg  , bool bIsSendToGameClient , const defAccountID & acc_id)
{
	// 同步频道列表
	unsigned int n = 0;
	MODI_SvrChannellist::TMAPS & mapContents = this->GetMap();
	pMsg->m_nCount = mapContents.size();
	MODI_SvrChannellist::TMAPS::const_iterator itor = mapContents.begin();
	MODI_ServerStatus * p_startaddr = (MODI_ServerStatus *)(&(pMsg->m_pData[n]));
	while( itor != mapContents.end() )
	{
		MODI_ServerStatus temp = *(itor->second);
		if( bIsSendToGameClient )
		{
			temp.m_nGatewayIP = 0;
			temp.m_nGatewayPort = 0;
		}
		memcpy(p_startaddr , &temp , sizeof(MODI_ServerStatus) );
		MODI_ChannelAddr get_addr;
		GetChannelAddr(itor->first, "dx", get_addr);
		p_startaddr->m_nTSvrIP = PublicFun::StrIP2NumIP(get_addr.m_strTSvrIP.c_str());
		p_startaddr->m_nTSvrPort = get_addr.m_wdTSvrPort;
		itor++;
		p_startaddr++;
		n += 1;
	}
	MODI_ASSERT( n == pMsg->m_nCount );
	int iSendSize = sizeof(MODI_LS2C_Notify_Channellist) + sizeof(MODI_ServerStatus) * pMsg->m_nCount;
	return iSendSize;
}

int 	MODI_SvrChannellist::SendChannellistToGameClient( MODI_ServiceTask * pTask , int & iChannelNum , int iReason , const defAccountID & acc_id)
{
	iChannelNum = 0;
	if( !pTask )
		return 0;
	MODI_ASSERT( iReason > MODI_LS2C_Notify_Channellist::kBegin 
			&& iReason < MODI_LS2C_Notify_Channellist::kEnd );

	// 同步频道列表
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_LS2C_Notify_Channellist * pMsg = (MODI_LS2C_Notify_Channellist *)(szPackageBuf);
	pMsg->m_nReason = (MODI_LS2C_Notify_Channellist::Reason)(iReason);
	AutoConstruct( pMsg );
	int iSendSize = CreateSendPackage( pMsg , true , acc_id);
	if( iSendSize > 0 )
	{
		pTask->SendCmd( (const Cmd::stNullCmd *)(szPackageBuf) , iSendSize );
		iChannelNum = pMsg->m_nCount;
	}

	return iSendSize;
}

bool MODI_SvrChannellist::Init(const char * file_name)
{
	MODI_XMLParser xmlFile;

	if(! xmlFile.InitFile(file_name))
	{
		Global::logger->fatal("[sys_init] Unable load channel addr info" );
		return false;
	}

	xmlNodePtr p_root = xmlFile.GetRootNode("config");
	if(! p_root)
	{
		Global::logger->fatal("[sys_init] Unable get channel info file root node config");
		return false;
	}

	xmlNodePtr p_channelinfo = xmlFile.GetChildNode( p_root, "channelinfo");
	if(! p_channelinfo)
	{
		Global::logger->fatal("[sys_init] Unable get channel info node");
		return false;
	}

	MODI_AutoRDLock lock(m_stlock);
	ClearAddr();
	xmlNodePtr p_channel = xmlFile.GetChildNode(p_channelinfo, "channel");
	while(p_channel)
	{
		MODI_ChannelAddr addr;
		if(! xmlFile.GetNodeNum(p_channel, "id", &addr.m_byChannleID, sizeof(addr.m_byChannleID)))
		{
			Global::logger->fatal("[sys_init] Unable get channel id");
			return false;
		}
		
		if(! xmlFile.GetNodeStr(p_channel, "nettype", addr.m_strNetType))
		{
			Global::logger->fatal("[sys_init] Unable get channel nettype");
			return false;
		}

		if(! xmlFile.GetNodeStr(p_channel, "channelip", addr.m_strChannelIP))
		{
			Global::logger->fatal("[sys_init] Unable get channel ip");
			return false;
		}

		if(! xmlFile.GetNodeNum(p_channel, "channelport", &(addr.m_wdChannelPort), sizeof(addr.m_wdChannelPort)))
		{
			Global::logger->fatal("[sys_init] Unable get channel port");
			return false;
		}
		AddChannelAddr(addr);
		p_channel = xmlFile.GetNextNode(p_channel, "channel");
	}


	/// 获取teamspeak ip
	xmlNodePtr p_tsinfo = xmlFile.GetChildNode( p_root, "tsinfo");
	if(p_tsinfo)
	{
		MODI_AutoRDLock lock(m_stlock);
		xmlNodePtr p_tschannel = xmlFile.GetChildNode(p_tsinfo, "channel");
		while(p_tschannel)
		{
			MODI_ChannelAddr addr;
			if(! xmlFile.GetNodeNum(p_tschannel, "id", &addr.m_byChannleID, sizeof(addr.m_byChannleID)))
			{
				Global::logger->fatal("[sys_init] Unable get channel id");
				break;
			}

			if(! xmlFile.GetNodeStr(p_tschannel, "ip", addr.m_strTSvrIP))
			{
				Global::logger->fatal("[sys_init] Unable get stchannel ip");
				break;
			}

			if(! xmlFile.GetNodeNum(p_tschannel, "port", &(addr.m_wdTSvrPort), sizeof(addr.m_wdTSvrPort)))
			{
				Global::logger->fatal("[sys_init] Unable get tschannel port");
				break;
			}
			
			SetTSAddr(addr.m_byChannleID, addr);
			p_tschannel = xmlFile.GetNextNode(p_tschannel, "channel");
		}
	}
	return true;
}
