/** 
 * @file ServerConfig.h
 * @brief 服务器配置类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-05
 */

#ifndef SERVER_CONFIG_H_
#define SERVER_CONFIG_H_

#include "Type.h"
#include "XMLParser.h"
#include <vector>
#include <string>
#include <map>
#include <set>

#define SVRCFG_CHANNEL 					"gamechannel"

#define SVRCFG_MANGGERSERVER 		 	"managerservice"

#define SVRCFG_ZONESERVER 	 			"zoneservice"

#define SVRCFG_ACCOUNTSERVER  	 		"accountdbservice"

#define SVRCFG_LOGINSERVER 				"loginservice"

#define SVRCFG_GAMERESOURCE 			"gameresource"

#define SVRCFG_DBPROXY 					"dbproxy"

#define SVRCFG_BILLSERVICE "billservice"

#define SVRCFG_YYBILLSERVICE "yybillservice"

#define SVRCFG_INFOSERVICE "infoservice"

#define SVRCFG_UNIFYSERVICE "unifyservice"

#define SVRCFG_YYLOGINSERVICE "yyloginservice"

class MODI_SvrConfigBaseInfo;
class MODI_SvrGSConfig;
class MODI_SvrGWConfig;
class MODI_SvrMSConfig;
class MODI_SvrZSConfig;
class MODI_SvrADBConfig;
class MODI_SvrLSConfig;
class MODI_SvrResourceConfig;
class MODI_SvrBillConfig;
class MODI_SvrInfoConfig;
class MODI_UnifyConfig;
class MODI_YYLoginConfig;

enum 	DBProxyType
{
	kManagerServerProxy,
	kZoneServerProxy,
	kGameServerProxy,
};

extern "C" const char * GetDBProxyTypeStringFormat( DBProxyType e );


/** 
 * @brief 配置文件信息基础类
 */
class MODI_ISvrConfigInfo
{

	public:

		MODI_ISvrConfigInfo() {}
		virtual ~MODI_ISvrConfigInfo() = 0;

		MODI_SvrConfigBaseInfo * ToBaseInfoPtr()
		{
			return (MODI_SvrConfigBaseInfo *)(this);
		}

		MODI_SvrGSConfig * ToGSConfigPtr()
		{
			return (MODI_SvrGSConfig *)(this);
		}

		MODI_SvrGWConfig * ToGWConfigPtr()
		{
			return (MODI_SvrGWConfig *)(this);
		}

		MODI_SvrMSConfig * ToMSConfigPtr()
		{
			return (MODI_SvrMSConfig *)(this);
		}

		MODI_SvrZSConfig * ToZSConfigPtr()
		{
			return (MODI_SvrZSConfig *)(this);
		}

		MODI_SvrADBConfig * ToADBConfigPtr()
		{
			return (MODI_SvrADBConfig *)(this);
		}

		MODI_SvrResourceConfig * ToResourceConfigPtr()
		{
			return (MODI_SvrResourceConfig *)(this);
		}

};

/** 
 * @brief 配置文件中的服务器信息的基本字段
 */
class MODI_SvrConfigBaseInfo : public MODI_ISvrConfigInfo
{
	public:

		MODI_SvrConfigBaseInfo() {}
		virtual ~MODI_SvrConfigBaseInfo() { }
		
		std::string 	strIP;
		unsigned short  nPort;
		std::string 	strLogPath;
//		BYTE 			byWorldID;
//		BYTE 			byServerID;
};

/** 
 * @brief 游戏逻辑服务器的配置
 */
class MODI_SvrGSConfig : public MODI_ISvrConfigInfo
{
	friend class MODI_ServerConfig;

	public:

		MODI_SvrGSConfig() {}
		virtual ~MODI_SvrGSConfig();

		// 每个游戏服务器的配置（游戏频道）
		struct MODI_Info
		{
			std::string  strIP;
			unsigned short nPort;
			unsigned int nChannelID;
			std::string  strChannelName;
			std::string  strLogPath;
			std::string  strGatewayIP;
			unsigned short nGatewayPort;
			std::string  strGatewayWanIP;
			unsigned short nGatewayWanPort;
			std::string  strGatewayLogPath;


			MODI_Info():nPort(0),nChannelID(0),nGatewayPort(0),nGatewayWanPort(0)
			{

			}
		};

		
		const MODI_Info * 	GetInfoByID( unsigned int nID ) const;
		const MODI_Info * 	GetInfoByName( const char * szName ) const;
		
	private:

		std::vector<const MODI_Info *> 	vecGameServers;
};

//class MODI_SvrGWConfig : public MODI_SvrGSConfig
//{
//	public:
//
//		MODI_SvrGWConfig () {}
//		virtual ~MODI_SvrGWConfig() {}
//};
//
class MODI_SvrMSConfig : public MODI_SvrConfigBaseInfo
{
	public:

		MODI_SvrMSConfig() {}
		virtual ~MODI_SvrMSConfig() {}
};

class MODI_SvrZSConfig : public MODI_SvrConfigBaseInfo
{
	public:

		MODI_SvrZSConfig() {}
		virtual ~MODI_SvrZSConfig() {}

		unsigned short 		nDBProxyPort;
};

class MODI_SvrADBConfig : public MODI_SvrConfigBaseInfo
{
	public:

		MODI_SvrADBConfig() {}
		virtual ~MODI_SvrADBConfig() {}

		std::string 		m_strURL;
};

class MODI_DBProxyConfig : public MODI_ISvrConfigInfo
{
	friend class MODI_ServerConfig;

	public:

		MODI_DBProxyConfig() {}
		virtual ~MODI_DBProxyConfig();
		// 每个游戏服务器的配置（游戏频道）
		struct MODI_Info
		{
			unsigned int 	id;
			DBProxyType 	type;
			std::set<unsigned int> 	setGameServerID;
			std::string  	strLogPath;

			MODI_Info():id(0)
			{

			}
		};

		const MODI_Info * 	GetInfoByID( unsigned int nID ) const;
		bool CheckGSID( unsigned int nID );
		bool AddGSID( MODI_Info * pInfo , unsigned int nID );

		void set_db_url( const char * szUrl ) { strDBUrl = szUrl; }
		const char * db_url() const { return strDBUrl.c_str(); }
		
	private:

		std::vector<const MODI_Info *> 	vecDBProxy;
		std::set<unsigned int> 			setidcheck;
		std::string 					strDBUrl;
};


class MODI_SvrLSConfig : public MODI_SvrConfigBaseInfo
{
	public:

		MODI_SvrLSConfig() {}
		virtual ~MODI_SvrLSConfig() {}

		unsigned long 	nLoginTimeOut;
};


/** 
 * @brief 游戏资源配置信息
 */
class MODI_SvrResourceConfig : public MODI_ISvrConfigInfo 
{
	public:

		MODI_SvrResourceConfig() {}
		virtual ~ MODI_SvrResourceConfig() {}


		std::string 	strMdmDir;
		std::string 	strBinDir;
};

/**
 * @brief billservice配置文件
 * 
 */
class MODI_SvrBillConfig : public MODI_ISvrConfigInfo
{
public:
	std::string m_strBillIP;
	std::string m_strZoneInIP;
	std::string m_strZoneOutIP;
	WORD m_dwBillPort;
	WORD m_dwZonePort;
	std::string m_strLogPath;
	std::string m_strURL;
};


/**
 * @brief billservice配置文件
 * 
 */

class MODI_SvrYYBillConfig : public MODI_ISvrConfigInfo
{
public:
	std::string m_strYYBillIP;
	std::string m_strYYZoneIP;
	WORD m_wdYYBillPort;
	WORD m_wdYYZonePort;
	std::string m_strLogPath;
	std::map<std::string,std::string>  m_ZoneAddrMap;
};


/**
 * @brief infoservice配置文件
 * 
 */
class MODI_SvrInfoConfig : public MODI_ISvrConfigInfo
{
public:
	std::string m_strInfoIP;
	std::string m_strZoneInIP;
	std::string m_strZoneOutIP;
	WORD m_wdInfoPort;
	WORD m_wdZonePort;
	std::string m_strLogPath;
	std::string m_strURL;
	std::map<WORD, std::string> m_ZoneInfoMap;
};

/**
 * @brief unifyservice配置文件
 * 
 */
class MODI_SvrUnifyConfig : public MODI_ISvrConfigInfo
{
public:
	std::string m_strUnifyIP;
	std::string m_strZoneInIP;
	std::string m_strZoneOutIP;
	WORD m_wdUnifyPort;
	WORD m_wdZonePort;
	std::string m_strLogPath;
	std::string m_strURL;
	std::map<WORD, std::string> m_ZoneInfoMap;
	std::map<WORD, std::string> m_ZoneNameMap;
};

/** 
 * @brief YYlogin config
 * 
 */
class MODI_SvrYYLoginConfig: public MODI_ISvrConfigInfo
{
 public:
	std::string m_strYYLoginIP;
	WORD m_wdYYLoginPort;
	std::string m_strInfoIP;
	WORD m_wdInfoPort;
	std::string m_strLogPath;
};


/** 
 * @brief 服务器配置文件类
 */
class 	MODI_ServerConfig 
{
	
	public:

		MODI_ServerConfig();
		~MODI_ServerConfig();

	public:

		/** 
		 * @brief 加载服务器配置文件
		 * 
		 * @param szConfigFile 服务器配置文件的文件名
		 * 
		 * @return 成功返回TRUE	
		 */
		bool 	Load( const char * szConfigFile );

		/** 
		 * @brief 卸载服务器配置文件
		 */
		void 	Unload();

		
		/** 
		 * @brief 获取指定的配置信息
		 * 
		 * @param szKey 指定的配置字段名
		 * 
		 * @return 成功返回对应的配置信息，否则返回0	
		 */
		const MODI_ISvrConfigInfo * 	GetConfig( const char * szKey );

		static MODI_ServerConfig &	GetInstance();
		
	private:


		xmlNodePtr GetField( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot , const char * szKey  );

		bool 	LoadSvrBaseInfo(  MODI_XMLParser & xmlFile , xmlNodePtr & pRoot ,
			   	xmlNodePtr & pField , MODI_ISvrConfigInfo  * p );

		bool 	Insert( const std::string & strKey , MODI_ISvrConfigInfo * pValue );

		bool 	LoadSvrMgr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadGameSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

//		bool 	LoadGatewaySvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadRDBSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadDBSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadLoginSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadZoneSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadGameResource( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadBillSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadInfoSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );
		
		bool 	LoadUnifySvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadYYLoginSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

		bool 	LoadYYBillSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot );

	private:

		typedef std::map< std::string , MODI_ISvrConfigInfo * > 	MAP_CONFIGS;
		typedef MAP_CONFIGS::iterator 							MAP_CONFIGS_ITER;
		typedef MAP_CONFIGS::const_iterator 					MAP_CONFIGS_C_INTER;
		typedef std::pair<MAP_CONFIGS_ITER,bool> 				MAP_CONFIGS_INSERT_RESULT;

		MAP_CONFIGS 	m_mapSvrConfigs;
};

#endif
