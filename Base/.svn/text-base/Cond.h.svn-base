/**
 * @file Cond.h
 * @date 2009-12-14 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 系统条件变量封装
 *
 */

#ifndef _MDCOND_H
#define _MDCOND_H

#include <pthread.h>
#include "Type.h"

/**
 * @brief 系统条件变量封装
 *
 *
 */
class MODI_Cond: public MODI_DisableCopy
{
 public:
	/**
	 * @brief 初始化变量
	 *
	 */
	MODI_Cond()
	{
		::pthread_cond_init(&m_cond, NULL);
	}

	/**
	 * @brief 释放
	 *
	 */
	~MODI_Cond()
	{
		::pthread_cond_destroy(&m_cond);
	}

	/**
	 * @brief 广播等待此变量的线程
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	int broadcast()
	{
		int ret_code = 0;
	   	ret_code = ::pthread_cond_broadcast(&m_cond);
		return ret_code;
	}

	/**
	 * @brief 给等待此变量的线程发送信号
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
   	int signal()
	{
		int ret_code = 0;
	   	ret_code = ::pthread_cond_signal(&m_cond);
		return ret_code;
	}

	/**
	 * @brief 等待某互斥锁条件
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	int wait(MODI_Mutex & md_mutex)
	{
		int ret_code = 0;
	   	ret_code = ::pthread_cond_wait(&m_cond, &md_mutex.m_mutex);
		return ret_code;
	}

	/**
	 * @brief 超时timedwait秒就放弃等待
	 *
	 * @return 超时返回-1
	 *
	 */
	
	int timedwait(MODI_Mutex & md_mutex, int timedwait)
	{
		int ret_code = 0;
		struct timespec tv;
		if((clock_gettime(CLOCK_REALTIME, &tv)) != 0)
		{
			return -1;
		}
		tv.tv_sec += timedwait;
		ret_code = ::pthread_cond_timedwait(&m_cond, &md_mutex.m_mutex, &tv);
		return ret_code;
	}
	
 private:
	/// 系统条件变量
	::pthread_cond_t m_cond;
};

#endif
