/**
 * @file gw2gs.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 网关和GS的通信协议
 *
 */
#ifndef GATEWAY_TO_GAMESERVER_H_
#define GATEWAY_TO_GAMESERVER_H_

#include "Type.h"
#include "gw2gs_cmddef.h"
#include "cmdhelp.h"

#pragma pack(1)
/// 网关通知GS增加一个连接会话
const unsigned char SCMD_GW2GS_ADD = 0x01;
struct MODI_GW2GS_AddSession: public stNullCmd
{
    MODI_SessionID m_sessionID;

    MODI_GW2GS_AddSession()
    {
        byCmd = MCMD_GW2GS_SESSION;
        byParam = SCMD_GW2GS_ADD;
    }
};

/// 网关通知GS删除一个连接会话
const unsigned char SCMD_GW2GS_DEL = 0x02;
struct MODI_GW2GS_DelSession: public stNullCmd
{
    MODI_SessionID m_sessionID;

    MODI_GW2GS_DelSession()
    {
        byCmd = MCMD_GW2GS_SESSION;
        byParam = SCMD_GW2GS_DEL;
    }
};

/// 网关到游戏服务器的重定向包
const unsigned char SCMD_GW2GS_REDIRECTIONAL = 0x1f;
struct MODI_GW2GS_Redirectional: public stNullCmd
{
    MODI_SessionID m_sessionID;
    unsigned short m_nSize;
    char m_pData[0];

    MODI_GW2GS_Redirectional()
    {
        byCmd = (MCMD_SVR_REDIRECTIONAL);
        byParam = (SCMD_GW2GS_REDIRECTIONAL);
        m_nSize = 0;
    }
};

/// 游戏服务器到网关的重定向包
const unsigned char SCMD_GS2GW_REDIRECTIONAL = 0x2f;
struct MODI_GS2GW_Redirectional: public stNullCmd
{
    MODI_SessionID m_sessionID;
    unsigned short m_nSize;
    char m_pData[0];

    MODI_GS2GW_Redirectional()
    {
        byCmd = (MCMD_SVR_REDIRECTIONAL);
        byParam = (SCMD_GS2GW_REDIRECTIONAL);
        m_nSize = 0;
    }
};


/// 增加传输线路（语音数据）
const unsigned char SCMD_GS2GW_ADDTRANSMISSION = 0x01;
struct MODI_GS2GW_AddTransMission : public stNullCmd
{
	MODI_SessionID m_SrcSession;
	unsigned char m_nCount;
	MODI_SessionID m_pDestSession[0];
	MODI_GS2GW_AddTransMission()
	{
		byCmd = MCMD_GS2GW_MUSICTRANS;
		byParam = SCMD_GS2GW_ADDTRANSMISSION;
		m_nCount = 0;
	}
};

/// 删除传输线路(语言数据)
const unsigned char SCMD_GS2GW_DELTRANSMISSION = 0x02;
struct MODI_GS2GW_DelTransMission : public stNullCmd
{
	MODI_SessionID m_SrcSession;
	unsigned char m_nCount;
	MODI_SessionID m_pDestSession[0];

	MODI_GS2GW_DelTransMission()
	{
		byCmd = MCMD_GS2GW_MUSICTRANS;
		byParam = SCMD_GS2GW_DELTRANSMISSION;
		m_nCount = 0;
	}
};


/// 广播命令
struct MODI_GS2GW_Broadcast_Cmd: public stNullCmd
{
	
	MODI_GS2GW_Broadcast_Cmd()
	{
		byCmd = MCMD_GS2GW_BROADCAST;
        byParam = ms_SubCmd;
		m_wdCmdSize = 0;
		m_wdSessionSize = 0;
	}

	WORD GetSize()
	{
		WORD size = 0;
		size = m_wdCmdSize + sizeof(MODI_SessionID) * m_wdSessionSize + sizeof(MODI_GS2GW_Broadcast_Cmd);
		return size;
	}
	
	WORD m_wdCmdSize;
	WORD m_wdSessionSize;
	char m_pData[0];  ///包含 cmd[0],MODI_SessionID m_pDestSession[0];
	
	static const BYTE ms_SubCmd = 0x03;
};

#pragma pack()

#endif
