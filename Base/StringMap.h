/**
 * @file   StringMap.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Sat Feb 27 11:14:09 2010
 * @version Ver0.1
 * @brief  字符串容器
 * 
 */


#ifndef _MDSTRINGMAP_H
#define _MDSTRINGMAP_H

#include "Type.h"


/**
 * @brief 字符串容器
 * 
 */
class MODI_StringMap
{
 public:
	~ MODI_StringMap()
	{
		m_StringVec.clear();
	}
	
	std::string & operator[] (const std::string & key)
	{
		return m_StringVec[key];
	}
 private:
	__gnu_cxx::hash_map<std::string, std::string, str_hash > m_StringVec;
};


#endif
