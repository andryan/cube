/**
 * @file Command.h
 * @date 2010-01-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 命令文件定义
 *
 */


#ifndef _MDCOMMAND_H
#define _MDCOMMAND_H

#include <string>
#include "Define.h"
#include "Type.h"

/// 一级命令定义
namespace Cmd
{
	/// 语音相关命令
	const BYTE MUSIC_CMD = 1;

	/// 房间相关命令
	const BYTE CHANGE_CMD = 2;

	/// 语音聊天

	/**
	 * @brief 服务器内部通信命令
	 *
	 *
	 */ 
	struct stUserCmd: public stNullCmd
	{
		stUserCmd(): stNullCmd()
		{
			dwUserID = 0;
		}
		/// 用户ID
		DWORD dwUserID;
	}__attribute__((__packed__));
	
};

/// 具体命令
namespace Cmd
{
	/**
	 * @brief 语音相关空命令
	 *
	 */
	struct stMusicCmd: public stNullCmd
	{
		stMusicCmd()
		{
			byCmd = MUSIC_CMD;
		}
	}__attribute__((__packed__));
	
	/**
	 * @brief 转发语音数据
	 *
	 */
	const BYTE SEND_MUSIC_PARA = 1;
	struct stSendMusic: public stMusicCmd
	{
		stSendMusic()
		{
			byParam = SEND_MUSIC_PARA;
			m_dwSize = 0;
		}
		DWORD m_dwSize;
		BYTE m_Data[0];
	}__attribute__((__packed__));

	/**
	 * @brief 更新一个房主
	 *
	 */
	const BYTE REQUEST_CHANGE_SEND_PARA = 1;
	struct stRequestChangeSend: public stNullCmd
	{
		stRequestChangeSend()
		{
			byCmd = CHANGE_CMD;
			byParam = REQUEST_CHANGE_SEND_PARA;
			memset(m_cstrIP, 0 , sizeof(m_cstrIP));
			m_dwPort = 0;
		}
		
		/// 房主IP
		char m_cstrIP[MAX_IPSIZE];
		/// 房主port
		DWORD m_dwPort;
	}__attribute__((__packed__));

	/**
	 * @brief 更换房主成功
	 *
	 */
	const BYTE RETURN_CHANGE_SUCCESS_PARA = 2;
	struct stReturnChangeSuccess: public stNullCmd
	{
		stReturnChangeSuccess()
		{
			byCmd = CHANGE_CMD;
			byParam = RETURN_CHANGE_SUCCESS_PARA;
		}
	}__attribute__((__packed__));

	/**
	 * @brief 登录人的信息
	 *
	 */ 
	struct LoginInfo
	{
		LoginInfo()
		{
			memset(m_cstrName, 0 ,sizeof(m_cstrName));
			memset(m_cstrIP, 0 , sizeof(m_cstrIP));
			m_dwPort = 0;
			m_byHour = 0;
			m_byMinute = 0;
			m_bySecond = 0;
		}
		
		/// 已经登录的用户名字
		char m_cstrName[MAX_NAMESIZE];
		
		/// 登录的IP
		char m_cstrIP[MAX_IPSIZE];

		/// 登录的端口
		DWORD m_dwPort;

		/// 登录的时
		BYTE m_byHour;

		/// 登录的分
		BYTE m_byMinute;

		/// 登录的秒
		BYTE m_bySecond;
	}__attribute__((__packed__));
	
	/**
	 * @brief 更新失败,服务器返回哪些人登录过
	 *
	 *
	 */
	const BYTE RETURN_CHANGE_FAIL_PARA = 3;
	struct stReturnChangeFail: public stNullCmd
	{
		stReturnChangeFail()
		{
			byCmd = CHANGE_CMD;
			byParam = RETURN_CHANGE_FAIL_PARA;
			m_bySize = 0;
		}

		/// 登录个数
		BYTE m_bySize;

		/// 登录人的信息
		LoginInfo m_stLoginInfo[0];
	}__attribute__((__packed__));

	/**
	 * @brief 查询房主
	 *
	 */
	const BYTE REQUEST_SEND_ROOM_PARA = 4;
	struct stRequestSendRoom: public stNullCmd
	{
		stRequestSendRoom()
		{
			byCmd = CHANGE_CMD;
			byParam = REQUEST_SEND_ROOM_PARA;
		}
		
	}__attribute__((__packed__));

	/**
	 * @brief 返回查询房主信息
	 *
	 */
	const BYTE RETURN_SEND_ROOM_PARA = 5;
	struct stReturnSendRoom: public stNullCmd
	{
		stReturnSendRoom()
		{
			byCmd = CHANGE_CMD;
			byParam = RETURN_SEND_ROOM_PARA;
		}
		
		/// 房主信息
		LoginInfo m_stLoginInfo;
	}__attribute__((__packed__));

	/**
	 * @brief 检验命令
	 * 
	 */
	const BYTE CHECK_VERIFY = 6;
	const BYTE CHECK_VERSION_VERIFY = 6;
	struct stCheckVersionVerify: public stNullCmd
	{
		stCheckVersionVerify()
		{
			byCmd = CHECK_VERIFY;
			byParam = CHECK_VERSION_VERIFY;
			m_byVersion = 0;
		}
		/// 版本
		BYTE m_byVersion;
	}__attribute__((__packed__));

	
};

#endif
