//////////////////////////////////////////////////////////////////////////
//

#include "MIDILoader.h"

MODI_MIDILoader::MODI_MIDILoader()
{
	m_pCurNote = NULL;
	m_pCurSentence = NULL;
	m_nNumNotes = m_nNumLyrics = m_nNumSentence = m_nNumTempos = 0;
	m_bNoteOn = m_bNewChar = false;
	
	m_nBeatsPerPara = 4;
	m_dQuaNotePerBeat = 1.0;

	for (int i=0; i<MAX_TRACKS; i++)
	{
		m_TrkBuf[i] = NULL;
	}
	m_dwLastTempo = 0;
	m_dwTempo = 0;
	m_nRawDataSize = 0; 
	m_pRawData = 0;
}

MODI_MIDILoader::~MODI_MIDILoader()
{
	if( m_pSong )
	{
		delete m_pSong;
	}

	delete [] m_pRawData;

	for (int i=0; i<MAX_TRACKS; i++)
	{
		if (m_TrkBuf[i])
		{
			delete [] (m_TrkBuf[i]);
			m_TrkBuf[i] = NULL;
		}
	}
}

// ?????????
unsigned long MODI_MIDILoader::ReadVarLen(unsigned char **data)
{
	unsigned value = 0;
	unsigned char c = **data;
	(*data)++;

	if ( (value = c) & 0x80 )
	{
		value &= 0x7F;
		do
		{
			c = **data;
			(*data)++;
			value = (value << 7) + (c & 0x7F);
		} while (c & 0x80);
	}

    return(value);

}

bool MODI_MIDILoader::LoadFromMemoryToBuffer(const char* pMemory)
{
    if(!m_pSong || !pMemory)
        return false;

    const char* pNewMemory = pMemory;

    pNewMemory += 3;

    char buf[4];
    memcpy(buf,pNewMemory,4);
    pNewMemory += 4;
    if (memcmp(buf,"MThd",4))
    {
        pNewMemory = NULL;
        return false;
    }

    pNewMemory += 4;
	char buffer[2];
    memcpy(buffer,pNewMemory,2);
    pNewMemory += 2;

    unsigned short data=*(short *)buffer;
    unsigned short dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp;
    //*******************************

    int midi_type;
    midi_type=data;
    if ((midi_type != 0) && (midi_type != -1))    

    memcpy(buffer,pNewMemory,2);
    pNewMemory += 2;
    //data=fgetwc(fp);
    data=*(short *)buffer;
    dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp;

    m_nNumTracks = data;              
    //m_nNumTracks = 2;

    //fread( &num_tracks, 1, 2, fp);
    if ((m_nNumTracks < 1) || (m_nNumTracks > 256))
    {
        pNewMemory = NULL;
        return false;
    }

    memcpy(buffer,pNewMemory,2);
    pNewMemory += 2;
    data=*(short *)buffer;
    dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp; 

    if (data <=0)
    {
        pNewMemory = NULL;
        return false;
    }
    m_dDivision = (double)data;		// ????????
    m_pSong->m_dDivision = m_dDivision;


    for (int i=0; i<m_nNumTracks; i++)
    {
        memcpy(buf,pNewMemory,4);
        pNewMemory += 4;

        if (memcmp(buf,"MTrk",4))
        {
            pNewMemory = NULL;
            return false;
        }

        unsigned long longData;
        memcpy(&longData,pNewMemory,4);
        pNewMemory += 4;

        unsigned long longDataTemp_0=longData;
        unsigned long longDataTemp_1=longData;
        unsigned long longDataTemp_2=longData;
        unsigned long longDataTemp_3=longData;

        longDataTemp_0&=0xff000000;
        longDataTemp_0>>=24;

        longDataTemp_1&=0x00ff0000;
        longDataTemp_1>>=8;

        longDataTemp_2&=0x0000ff00;
        longDataTemp_2<<=8;

        longDataTemp_3&=0x000000ff;
        longDataTemp_3<<=24;

        longData=0;
        longData|=longDataTemp_0;
        longData|=longDataTemp_1;
        longData|=longDataTemp_2;
        longData|=longDataTemp_3;

        m_TrkLen[i] = longData;

        m_TrkBuf[i] = new unsigned char[m_TrkLen[i]];   
        if (m_TrkBuf[i] == NULL)
        {
            pNewMemory = NULL;
            return false;
        }

        memcpy(m_TrkBuf[i],pNewMemory,m_TrkLen[i]);
        pNewMemory += m_TrkLen[i];
        //这里的内存copy如果，内存不足，则出错
    }

    return true;
}

// ??MIDI????????????????
bool MODI_MIDILoader::LoadFromMidiFileToBuffer(const string &fileName)
{
	if(m_pSong==NULL)
		return false;

	// ?????
	FILE *fp = fopen( fileName.c_str(), "rb");        
	if(fp==NULL)
	{
		return false;
	}
	if (!fp)
	{
		string strErrInfo = "?????"+fileName;
		//ERR_LOG0(strErrInfo.c_str());
		//goto err;
		fclose(fp);
		return false;
	}

	// ?????????
	fseek( fp, 3, SEEK_CUR );     

	// ???MIDI???
	char buf[4];
	fread( buf, 1, 4, fp );                  
	if (memcmp(buf, "MThd", 4))
	{
		fclose(fp);
		return false;
		//goto err;
	}
	// ?????????
	fseek( fp, 4, SEEK_CUR );                           

	// ???MIDI?????????
	//*******************************
	char buffer[2];
	fread(buffer,1,2,fp);
    //unsigned short data=fgetwc(fp);
	unsigned short data=*(short *)buffer;
    unsigned short dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp;
	//*******************************
                  
	int midi_type;
	midi_type=data;
	if ((midi_type != 0) && (midi_type != -1))
	//	goto err;

	// ???MIDI????????1??256???
	//*****************
	fread(buffer,1,2,fp);
    //data=fgetwc(fp);
	data=*(short *)buffer;
    dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp;
	//*****************

	m_nNumTracks = data;              
	//m_nNumTracks = 2;

	//fread( &num_tracks, 1, 2, fp);
	if ((m_nNumTracks < 1) || (m_nNumTracks > 256))
	{
		goto err;
	}

	// ???MIDI????ж??????????????????????????????????????????????????MIDI?????????
	//*****************
	fread(buffer,1,2,fp);
    //data=fgetwc(fp);
	data=*(short *)buffer;
    dataTemp=data;
    data>>=8;
    dataTemp<<=8;
    data|=dataTemp; 
	//*****************

	if (data <=0)
		goto err;
	m_dDivision = (double)data;		// ????????
	m_pSong->m_dDivision = m_dDivision;

	// ?????????????
	for (int i=0; i<m_nNumTracks; i++)
	{               
		// ????????
		fread(buf, 1, 4, fp);     

		// ???????????MTrk,???????
		if (memcmp(buf, "MTrk", 4))          
			goto err;

		// ?????????????************************
		unsigned long longData;
		fread( &longData, 1, 4, fp ); 

		unsigned long longDataTemp_0=longData;
		unsigned long longDataTemp_1=longData;
		unsigned long longDataTemp_2=longData;
		unsigned long longDataTemp_3=longData;

        longDataTemp_0&=0xff000000;
        longDataTemp_0>>=24;

        longDataTemp_1&=0x00ff0000;
        longDataTemp_1>>=8;

        longDataTemp_2&=0x0000ff00;
        longDataTemp_2<<=8;

        longDataTemp_3&=0x000000ff;
        longDataTemp_3<<=24;

        longData=0;
		longData|=longDataTemp_0;
		longData|=longDataTemp_1;
		longData|=longDataTemp_2;
		longData|=longDataTemp_3;

		m_TrkLen[i] = longData;
		//*****************************************

		// ????????????              
		m_TrkBuf[i] = new unsigned char[m_TrkLen[i]];   
		if (m_TrkBuf[i] == NULL)
			goto err;

		// ??????????????????????????
		if (fread(m_TrkBuf[i], 1, m_TrkLen[i], fp) != m_TrkLen[i])
			goto err;
	}// end of for

	fseek( fp , 0 , SEEK_END );
	m_nRawDataSize = ftell( fp );
	fseek( fp , 0 , SEEK_SET );
	delete [] m_pRawData;
	m_pRawData = new char [m_nRawDataSize];
	fread( m_pRawData , 1 , m_nRawDataSize , fp );
	fclose(fp);	
	
	return true;

err:
	fclose(fp);
	return false;

}

// ????MIDI???
bool  MODI_MIDILoader::ProcessMIDIEvent()
{
	if(NULL==m_pSong)
		return false;

	unsigned long dTicks = 0;
	bool bRecallEvent = false;
	unsigned nLen;
	unsigned char type;

	for (int i=0; i<m_nNumTracks; i++)
	{
		m_dwElapsedTicks = 0;
		m_dElapsedTime = 0.0;
		m_ucEvent = 0;
		m_pPos = m_TrkBuf[i];    // m_pPos??????????????λ????????????????????????????????????????????????????????

		// ???????????
		while (m_pPos)
		{
			dTicks = ReadVarLen((unsigned char **)&m_pPos);
			m_dwElapsedTicks += dTicks;
			// ???ticks??????????????
			m_dElapsedTime = m_pSong->GetAbsTimeByTicks(m_dwElapsedTicks);

			if (*m_pPos < 0x80)
			{	// is recall event
				bRecallEvent = true;
				if (m_ucEvent == 0)
				{
					//DBG_LOG("Error: running command with no previous command\n");
					return false;
				}
			}
			else 
			{
				m_ucEvent = *m_pPos;
				m_pPos ++;
				bRecallEvent = false;
			}
			

			switch (m_ucEvent)
			{
			case event_noteoff:                // ?????????
				OnNoteOff();
				m_pPos += 2;
				break;
			case event_noteon:                // ????????
				if(m_pPos[1]==0)
				{
					OnNoteOff();
				}
				else
				{
					OnNoteOn();
				}
				m_pPos += 2;
				break;
			case event_polyaftertouch:
			case event_controlchange:
			case event_pitchbend:
				m_pPos += 2;
				break;
			case event_programchange:
			case event_aftertouch:
			case event_songpos:
				m_pPos += 1;
				break;
			case event_sysex:
			case event_endsysex: 
				// <????> <???>
				nLen = ReadVarLen(&m_pPos);
				m_pPos += nLen;
				break;
			case event_meta:      // ????
				// <????> <????> <???>
				type = *m_pPos;
				m_pPos++;
				nLen = ReadVarLen(&m_pPos);
				switch(type)
				{
				case meta_lyric:
					// <???>
					OnLyric(nLen);
					break;
				case meta_endtrack:
					// <???>
					OnTrkEnd();
					break;
				case meta_tempo:
					// <???>
					OnTempoChange();
					break;
				case meta_meter: 
					OnMeterChange();
					break;
				case meta_seqnumber: case meta_text:
				case meta_copyright: case meta_marker:
				case meta_cuepoint: case meta_prefixchannel:
				case meta_prefixport: case meta_trackname:
				case meta_instrument: case meta_smpte:
				case meta_key:
					break;
				default:
					//DBG_LOG("Invalid meta event");
					return false;
					break;
				}
				m_pPos += nLen;
				break;
			case event_songselect:
			case event_tunerequest:
			case event_clock:
			case event_start:
			case event_continue:
			case event_stop:
			case event_activesense:
				m_pPos += 0;
				break;
			default:
				return false;	// unknow event command
				break;
			}// end of switch
		}// end of while
	}// end of for

	m_pSong->m_numNotes    = m_nNumNotes;
	m_pSong->m_numLyrics   = m_nNumLyrics;
	m_pSong->m_numSentence = m_nNumSentence;
	m_pSong->m_numTempos   = m_nNumTempos;
	m_pSong->m_overTime    = (float)(m_pSong->GetCurNote()->GetEndTime());

	return true;
}

// ?????????????? ????????μ????? ??????????????????????д??????????
void  MODI_MIDILoader::OnNoteOn()
{
	// ??????note
	MODI_Note *pNewNote = new MODI_Note;
	pNewNote->pitch = m_pPos[0];
	pNewNote->startTime = m_dElapsedTime;
	if (m_bNoteOn)
	{	// ????????note on ???
		m_pCurNote->endTime = m_dElapsedTime;
		m_pCurNote->index = ++m_nNumNotes;
		m_pSong->AddNote( m_pCurNote );
	}

	m_pCurNote = pNewNote;

	m_bNoteOn = true;
	m_bNewChar = false;
}

// ??????????????? ?????????????д????????????? ????????????????
void  MODI_MIDILoader::OnNoteOff()
{
	if ( m_bNoteOn && m_pCurNote->GetPitch() == m_pPos[0] )
	{	// ?????????????note on???, ??????pitch???
		m_pCurNote->endTime = m_dElapsedTime;
		m_pCurNote->index = ++m_nNumNotes;
		m_pSong->AddNote(m_pCurNote);
		m_bNoteOn = false;

		// ?????????????????Сpitch
		if ( m_pCurNote->GetPitch() > m_pSong->GetMaxPitch() )
			m_pSong->m_maxPitch = m_pCurNote->GetPitch();
		if ( m_pCurNote->GetPitch() < m_pSong->GetMinPitch() )
			m_pSong->m_minPitch = m_pCurNote->GetPitch();
		
		// ?????????????Note???????Сduration
		if ( (m_pCurNote->GetEndTime() - m_pCurNote->GetStartTime()) > m_pSong->GetMaxDuration())
			m_pSong->m_maxDuration = (float)(m_pCurNote->GetEndTime() - m_pCurNote->GetStartTime());
		if ( (m_pCurNote->GetEndTime() - m_pCurNote->GetStartTime()) < m_pSong->GetMinDuration())
			m_pSong->m_minDuration = (float)(m_pCurNote->GetEndTime() - m_pCurNote->GetStartTime());
	}
}

// ??????????? ????????μ?????????????????????????????????????д?????????? ???????????????
void  MODI_MIDILoader::OnLyric( int nLen )
{
	unsigned char *pStr = new unsigned char[nLen+1];
	memcpy(pStr, m_pPos, nLen);
	pStr[nLen] = 0;
	if(strcmp((char*)pStr, "\r\n")==0)
	{
		delete [] pStr;
		return;
	}
	if(strcmp((char*)pStr, "")==0)
	{
		delete [] pStr;
		return;
	}
	if(strcmp((char*)pStr, "\r")==0)
	{
		delete [] pStr;
		return;
	}
	if(strcmp((char*)pStr, "\n")==0)
	{
		delete [] pStr;
		return;
	}
	if(strcmp((char*)pStr, " ")==0)
	{
		delete [] pStr;
		return;
	}
	if(strcmp((char*)pStr, "	")==0)
	{
		delete [] pStr;
		return;
	}
	if(pStr[nLen-1]=='~')
	{	// ?滻??????????
		pStr[nLen-1] = '-';
	}

	MODI_Lyric * newLyric = new MODI_Lyric;
	// ???????????????????????
	MODI_LyricParser lyricParser;
	bool result = lyricParser.FindAttribute((const char *)pStr, "sentence");

	newLyric->m_character = lyricParser.GetLyric((const char *)pStr);
	newLyric->m_startTime = m_dElapsedTime;
	newLyric->m_index = ++m_nNumLyrics;
		
	m_bNewChar = true;

	if (result)
	{	
		//???rap
        static int rap=0;

		if (2==rap)
		{
            rap=0;
		}

		if (0==rap)
		{
			if (lyricParser.FindAttribute((const char *)pStr, "rap"))
			{
				rap=1;
			}
		}

        if (1==rap)
		{
			if (lyricParser.FindAttribute((const char *)pStr, "/rap"))
			{
				rap=2;
			}
		}
		
		// ???????????
		MODI_Sentence * newSentence = new MODI_Sentence;
		if (m_pCurSentence)
		{
			m_pSong->GetCurLyric()->m_last = true;
			m_pCurSentence->m_endLyric = m_pSong->GetCurLyric();
		}
		newSentence->m_startLyric = newLyric;
		newSentence->m_attri = lyricParser.GetAttribute();
		newSentence->m_index = ++m_nNumSentence;

        if (1==rap || 2==rap)
		{
            newSentence->m_bRap = true;
		}
		else
		{
		    newSentence->m_bRap = false;
		}

		if (lyricParser.FindAttribute((const char *)pStr, "@intermission"))
		{
			newSentence->m_bInterMission = true;
		}
		else
		{
			newSentence->m_bInterMission = false;            
		}

		m_pSong->AddSentence( newSentence );
		m_pCurSentence = newSentence;
	}

	if (!result)
	{
		newLyric->m_last = true;
		m_pCurSentence->m_endLyric = newLyric;
	}
	
	m_pSong->AddLyric( newLyric );

	delete[] pStr;
}

void MODI_MIDILoader::OnMeterChange()
{
	// ???????????4??????
	// <?????????> <???????? 2=????? 3=8????, ???> 
	// <???????????> <????????????????32??????>
	m_nBeatsPerPara = m_pPos[0];
	m_dQuaNotePerBeat = pow(2.0, (2.0 - m_pPos[1]));
}

// ?????????????? ???????
void  MODI_MIDILoader::OnTempoChange()
{
	m_dwTempo = m_pPos[0] * 0x10000L + m_pPos[1] * 0x100L + m_pPos[2];
	if(m_dwTempo==m_dwLastTempo)
	{	// tempo ???б仯??
		return;
	}
	m_dwLastTempo = m_dwTempo;

	MODI_Tempo *newTempo = new MODI_Tempo;
	newTempo->m_dwTempo = m_dwTempo;
	newTempo->m_startTicks = m_dwElapsedTicks;
	newTempo->m_nBeatsPerPara = m_nBeatsPerPara;
	newTempo->m_dQuaNotePerBeat = m_dQuaNotePerBeat;

	newTempo->index = ++m_nNumTempos;
	m_pSong->AddTempo(newTempo);
}

// ??????????????
void MODI_MIDILoader::OnTrkEnd()
{
	if (m_pCurSentence)
	{	// ??????????????
		m_pSong->GetCurLyric()->m_last = true;
		m_pCurSentence->m_endLyric = m_pSong->GetCurLyric();
	}
	m_pPos = NULL;
}













