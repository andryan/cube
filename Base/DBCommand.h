/**
 * @file DBCommand.h
 * @date 2010-01-18 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库相关命令
 *
 */

#ifndef _MDDBCOMMAND_H
#define _MDDBCOMMAND_H

#include "Global.h"
#include "VarType.h"

/// 获取一个变量
#define GETVAR(var_name, data)	\
{ \
   	var_name.Put(data->m_pData, data->m_dwSize); \
	read_size += sizeof(data->m_byType)+sizeof(data->m_dwSize)+data->m_dwSize; \
}

/**
 * @brief 数据库存放二进制的格式
 * 
 */
struct MODI_DBBinStruct
{
	BYTE m_byType;
	DWORD m_dwSize;
	BYTE m_pData[0];
}__attribute__((__packed__));


/// 二进制数据类型
enum enDBBinType
{
	BIN_TYPE_ROLE_EXP, /// 人物经验
	BIN_TYPE_ROLE_OTHER_NAME, /// 人物别名
	BIN_TYPE_ROLE_RIGHT_HAND_ID, /// 人物右手ID
	BIN_TYPE_ROLE_HOME_ID,
};

	
/// 一些需要保存的零散数据
struct MODI_UserBinData
{
	MODI_UserBinData()
	{
		m_dwExp = 0;
		m_byBodyID = 0;
		m_PackageIDVec.clear();
	}
	/// 人物经验
	DWORD m_dwExp;
	/// 人物身体ID
	BYTE m_byBodyID;
	/// 人物称号
	std::string m_strOtherName;
	/// 包裹物件ID
	std::vector<WORD > m_PackageIDVec;
};

/// 序列化对应的变量
struct MODI_SerialUserBinData
{
	MODI_SerialUserBinData(const unsigned int size = 1024*64 ): buffer(size)
	{
		
	}
	/// 人物经验
	MODI_VarType m_dwExp;
	/// 人物身体ID
	MODI_VarType m_byBodyID;
	/// 人物称号
	MODI_VarType m_strOtherName;
	/// 包裹物件ID
	MODI_VarType m_PackageIDVec;

	/// 清除数据
	void Clear()
	{
		m_dwExp.Clear();
		m_strOtherName.Clear();
		m_byBodyID.Clear();
		m_PackageIDVec.Clear();
	}
	
	/// 人物零散数据赋值
	inline MODI_SerialUserBinData & operator= (const MODI_UserBinData & base)
	{
		m_dwExp = base.m_dwExp;
		m_byBodyID = base.m_byBodyID;
		m_strOtherName = base.m_strOtherName;
	   	PublicFun::SerialVec(m_PackageIDVec, base.m_PackageIDVec);
		return (*this);
	}

	/// 序列化一个变量
	inline void SerialVar(const MODI_VarType & var, enDBBinType  type)
	{
		MODI_DBBinStruct * data = NULL;
		data = (MODI_DBBinStruct *)(buffer.WriteBuf());
		data->m_byType = type;
		data->m_dwSize = var.Size();
		memcpy(data->m_pData, (const char *)var, var.Size());
		buffer.WriteSuccess(data->m_dwSize+sizeof(data->m_byType)+sizeof(data->m_dwSize));
	}

	/// 系列化结构体
	int SerialStruct()
	{
		/// 总大小
		DWORD size = 0;
		buffer.Write((unsigned char *)&size, sizeof(DWORD));

		SerialVar(m_dwExp, BIN_TYPE_ROLE_EXP);
		SerialVar(m_strOtherName, BIN_TYPE_ROLE_OTHER_NAME);
		SerialVar(m_byBodyID, BIN_TYPE_ROLE_RIGHT_HAND_ID);
		SerialVar(m_PackageIDVec, BIN_TYPE_ROLE_HOME_ID);

		size = buffer.ReadSize() - sizeof(size);
		memcpy(buffer.ReadBuf(), (unsigned char *)&size, sizeof(DWORD));
		return size;
	}

	/// 反赋值
	void UnserialStruct(MODI_UserBinData & base)
	{
		base.m_dwExp = (unsigned int)m_dwExp;
		base.m_strOtherName = (const char *)m_strOtherName;
		base.m_byBodyID = (unsigned char)m_byBodyID;
		PublicFun::UnserialVec(base.m_PackageIDVec, m_PackageIDVec);
	}
	
	/// 输出
	MODI_ByteBuffer buffer;
};


class MODI_GameUser: public MODI_DisableCopy
{
 public:
	MODI_GameUser()
	{
		m_byLevel = 0;
	}
		
	/// 人物名字
	std::string m_strName;

	/// 人物等级
	BYTE m_byLevel;

	MODI_UserBinData m_stBinData;
	MODI_SerialUserBinData m_stSerialBinData;
};


/// GS2DBCommand
const BYTE CMD_GAME_TO_DB = 1;
struct MODI_GS2DB_CmdData: public Cmd::stNullCmd
{
	MODI_GS2DB_CmdData()
	{
		byCmd = CMD_GAME_TO_DB;
		m_dwSize = 0;
	}
	DWORD m_dwSize;
	BYTE m_pData[0];
}__attribute__((packed));

/// ZS2DBCommand
const BYTE CMD_ZONE_TO_DB = 2;
struct MODI_ZS2DB_CmdData: public Cmd::stNullCmd
{
	MODI_ZS2DB_CmdData()
	{
		byCmd = CMD_ZONE_TO_DB;
		m_dwSize = 0;
	}
	DWORD m_dwSize;
	BYTE m_pData[0];
}__attribute__((packed));

/// RS2DBCommand
const BYTE CMD_ROLE_TO_DB = 3;
struct MODI_RS2DB_CmdData: public Cmd::stNullCmd
{
	MODI_RS2DB_CmdData()
	{
		byCmd = CMD_ROLE_TO_DB;
		m_dwSize = 0;
	}
	DWORD m_dwSize;
	BYTE m_pData[0];
}__attribute__((packed));

/// SS2DBCommand
const BYTE CMD_SHOP_TO_DB = 4;
struct MODI_SS2DB_CmdData: public Cmd::stNullCmd
{
	MODI_SS2DB_CmdData()
	{
		byCmd = CMD_SHOP_TO_DB;
		m_dwSize = 0;
	}
	DWORD m_dwSize;
	BYTE m_pData[0];
}__attribute__((packed));


#endif
