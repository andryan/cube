/* crypto/rc5/rc5_enc.c */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

#include <stdio.h>
#include "rc5.h"
#include "rc5_locl.h"

void RC5_32_set_key(RC5_32_KEY *key, int len, const unsigned char *data,
					int rounds)
{
	RC5_32_INT L[64],l,ll,A,B,*S,k;
	int i,j,m,c,t,ii,jj;

	if (	(rounds != RC5_16_ROUNDS) &&
		(rounds != RC5_12_ROUNDS) &&
		(rounds != RC5_8_ROUNDS))
		rounds=RC5_16_ROUNDS;

	key->rounds=rounds;
	S= &(key->data[0]);
	j=0;
	for (i=0; i<=(len-8); i+=8)
	{
		c2l(data,l);
		L[j++]=l;
		c2l(data,l);
		L[j++]=l;
	}
	ii=len-i;
	if (ii)
	{
		k=len&0x07;
		c2ln(data,l,ll,k);
		L[j+0]=l;
		L[j+1]=ll;
	}

	c=(len+3)/4;
	t=(rounds+1)*2;
	S[0]=RC5_32_P;
	for (i=1; i<t; i++)
		S[i]=(S[i-1]+RC5_32_Q)&RC5_32_MASK;

	j=(t>c)?t:c;
	j*=3;
	ii=jj=0;
	A=B=0;
	for (i=0; i<j; i++)
	{
		k=(S[ii]+A+B)&RC5_32_MASK;
		A=S[ii]=ROTATE_l32(k,3);
		m=(int)(A+B);
		k=(L[jj]+A+B)&RC5_32_MASK;
		B=L[jj]=ROTATE_l32(k,m);
		if (++ii >= t) ii=0;
		if (++jj >= c) jj=0;
	}
}


void RC5_32_encrypt(RC5_32_INT *d, RC5_32_KEY *key)
	{
	RC5_32_INT a,b,*s;

	s=key->data;

	a=d[0]+s[0];
	b=d[1]+s[1];
	E_RC5_32(a,b,s, 2);
	E_RC5_32(a,b,s, 4);
	E_RC5_32(a,b,s, 6);
	E_RC5_32(a,b,s, 8);
	E_RC5_32(a,b,s,10);
	E_RC5_32(a,b,s,12);
	E_RC5_32(a,b,s,14);
	E_RC5_32(a,b,s,16);
	if (key->rounds == 12)
		{
		E_RC5_32(a,b,s,18);
		E_RC5_32(a,b,s,20);
		E_RC5_32(a,b,s,22);
		E_RC5_32(a,b,s,24);
		}
	else if (key->rounds == 16)
		{
		/* Do a full expansion to avoid a jump */
		E_RC5_32(a,b,s,18);
		E_RC5_32(a,b,s,20);
		E_RC5_32(a,b,s,22);
		E_RC5_32(a,b,s,24);
		E_RC5_32(a,b,s,26);
		E_RC5_32(a,b,s,28);
		E_RC5_32(a,b,s,30);
		E_RC5_32(a,b,s,32);
		}
	d[0]=a;
	d[1]=b;
	}

void RC5_32_decrypt(RC5_32_INT *d, RC5_32_KEY *key)
	{
	RC5_32_INT a,b,*s;

	s=key->data;

	a=d[0];
	b=d[1];
	if (key->rounds == 16) 
		{
		D_RC5_32(a,b,s,32);
		D_RC5_32(a,b,s,30);
		D_RC5_32(a,b,s,28);
		D_RC5_32(a,b,s,26);
		/* Do a full expansion to avoid a jump */
		D_RC5_32(a,b,s,24);
		D_RC5_32(a,b,s,22);
		D_RC5_32(a,b,s,20);
		D_RC5_32(a,b,s,18);
		}
	else if (key->rounds == 12)
		{
		D_RC5_32(a,b,s,24);
		D_RC5_32(a,b,s,22);
		D_RC5_32(a,b,s,20);
		D_RC5_32(a,b,s,18);
		}
	D_RC5_32(a,b,s,16);
	D_RC5_32(a,b,s,14);
	D_RC5_32(a,b,s,12);
	D_RC5_32(a,b,s,10);
	D_RC5_32(a,b,s, 8);
	D_RC5_32(a,b,s, 6);
	D_RC5_32(a,b,s, 4);
	D_RC5_32(a,b,s, 2);
	d[0]=a-s[0];
	d[1]=b-s[1];
	}

