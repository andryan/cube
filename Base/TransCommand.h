/**
 * @file   TransCommand.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Fri Oct 15 14:57:04 2010
 * @version $Id:$
 * @brief  交易命令
 * 
 */

#ifndef _MD_TRANSCOMMAND_H
#define _MD_TRANSCOMMAND_H

#include "Global.h"
#include "protocol/gamedefine.h"
#include "protocol/gamedefine2.h"
#include "gamestructdef.h"

const WORD BILL_MSG_LENGTH = 512;

/**
 * @brief 交易结果
 * 
 */
enum enTransResult
{
	enTransResultNone, //无返回
	enTransSuccess, //成功
	enTransNoMoney, //钱不够
	enTransBillErr,
	enTransDBErr, //数据库问题
};


/**
 * @brief 购买物品信息
 * 
 */
struct MODI_TransInfo
{
	MODI_TransInfo()
	{
		memset(m_cstrItemName, 0, sizeof(m_cstrItemName));
		memset(m_cstrMenu, 0, sizeof(m_cstrMenu));
		m_wdItemCount = 0;
		m_dwItemID = 0;
	}
	
	/// 物品名字
	char m_cstrItemName[MAX_ITEM_NAME + 1];

	/// 物品ID
	DWORD m_dwItemID;
	
	/// 物品个数
	WORD m_wdItemCount;
	
	/// 备注
	char m_cstrMenu[BILL_MSG_LENGTH + 1];
	
}__attribute__((packed));


/**
 * @brief 扣款参数
 * 
 */
struct MODI_TransData
{
	MODI_TransData()
	{
		m_qdTransID = 0;
		m_wdWorldID = 0;
		m_stAccID = INVAILD_ACCOUNT_ID;
		m_stCharID = INVAILD_CHARID;
		m_dwCoastMoney = 0;
		m_byTransType = 0;
		m_qdTransTime = 0;
		m_wdTransSize = 0;
	}

	DWORD GetSendSize()
	{
		return (sizeof(*this) + (sizeof(MODI_TransInfo) * m_wdTransSize));
	}

	/// 交易号
	QWORD m_qdTransID;

	/// 区号
	WORD m_wdWorldID;

	/// 账号
	defAccountID m_stAccID;

	/// 角色号
	MODI_CHARID m_stCharID;

	/// 扣款
	DWORD m_dwCoastMoney;

	/// 交易类型
	BYTE m_byTransType;

	/// 交易时间
	QWORD m_qdTransTime;

	/// 物品信息个数
	WORD m_wdTransSize;

	/// 物品信息
	MODI_TransInfo m_stTransInfo[0];
	
}__attribute__((packed));


/// 交易命令
const BYTE TRANS_COMMAND_CMD = 100;
struct MODI_TransCommand: public Cmd::stNullCmd
{
	MODI_TransCommand()
	{
		byCmd = TRANS_COMMAND_CMD;
	}

	static const BYTE m_bySCmd = TRANS_COMMAND_CMD;
}__attribute__((packed));


/// 刷新显示
const BYTE M_REFRESHMONEY_PARA = 1;
struct MODI_MRefreshMoney: public MODI_TransCommand
{
	MODI_MRefreshMoney()
	{
		byParam = M_REFRESHMONEY_PARA;
		m_stAccID = INVAILD_ACCOUNT_ID;
		m_dwMoney = 0;
		m_dwPayMoney = 0;
		m_enReason = enMRefreshMoney_None;
	}
	
	defAccountID m_stAccID;
	DWORD m_dwMoney;
	DWORD m_dwPayMoney;
	enMRefreshMoney m_enReason;
	static const BYTE m_bySParam = M_REFRESHMONEY_PARA;
}__attribute__((packed));


/// 请求刷新显示
const BYTE S_REFRESHMONEY_PARA = 2;
struct MODI_SRefreshMoney: public MODI_TransCommand
{
	MODI_SRefreshMoney()
	{
		byParam = S_REFRESHMONEY_PARA;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		m_dwAccID = 0;
	}
	
	char m_cstrAccName[33];
	DWORD m_dwAccID;
	static const BYTE m_bySParam = S_REFRESHMONEY_PARA;
}__attribute__((packed));


/// 消费扣款
const BYTE CONSUMEMONEY_PARA = 3;
struct MODI_ConsumeMoneyCmd: public MODI_TransCommand
{
	MODI_ConsumeMoneyCmd()
	{
		byParam = CONSUMEMONEY_PARA;
		memset( m_szExtraData , 0 , sizeof(m_szExtraData) );
		m_iTransType = 0;
		m_serverid = 0;
	}
	
	char 			m_szExtraData[1024];
	int 			m_iTransType;
	BYTE 			m_serverid;
	MODI_TransData m_stTransData;
	static const BYTE m_bySParam = CONSUMEMONEY_PARA;
}__attribute__((packed));


/// 消费扣款返回
const BYTE RET_CONSUMEMONEY_PARA = 4;
struct MODI_RetConsumeMoneyCmd: public MODI_TransCommand
{
	MODI_RetConsumeMoneyCmd()
	{
		byParam = RET_CONSUMEMONEY_PARA;
		m_qdTransID = 0;
		m_enResult = enTransResultNone;
		memset( m_szExtraData , 0 , sizeof(m_szExtraData) );
		m_iTransType = 0;
		m_client = INVAILD_CHARID;
		m_serverid = 0;
		m_dwAccMoney = 0;
	}

	QWORD m_qdTransID;
	enTransResult m_enResult;
	char 			m_szExtraData[1024];
	int 			m_iTransType;
	MODI_CHARID 	m_client;
	BYTE 			m_serverid;
	DWORD 			m_dwAccMoney;
	static const BYTE m_bySParam = RET_CONSUMEMONEY_PARA;
}__attribute__((packed));


/// INA Item 物品信息
struct MODI_INAItemPrice
{
	MODI_INAItemPrice()
	{
		m_dwPrice = 0;
		memset(m_cstrItemId, 0, sizeof(m_cstrItemId));		
	}
	
	/// 物品id
	char m_cstrItemId[20];
	/// 价格
	DWORD m_dwPrice;

	MODI_ShopGood m_pGoods;
	enShopTransactionType m_enBuyType;
}__attribute__((packed));


/// INA道具购买
struct MODI_INAItemConsume: public MODI_TransCommand
{
	MODI_INAItemConsume()
	{
		byParam = m_bySParam;
		m_byServerId = 0;
		m_dwAccountId = 0;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		m_wdSize = 0;
		m_qdBuySerial = 0;
	}

	/// 服务器id
	BYTE m_byServerId;
	/// 购买者
	DWORD m_dwAccountId;
	char m_cstrAccName[50];

	QWORD m_qdBuySerial;

	DWORD m_wdSize;
	
	MODI_INAItemPrice pData[0];
	
	static const BYTE m_bySParam = 5;
}__attribute__((packed));


/// 购买扣款返回
struct MODI_INAItemReturn: public MODI_TransCommand
{
	MODI_INAItemReturn()
	{
		byParam = m_bySParam;
		m_byServerId = 0;
		m_dwAccountId = 0;
		m_qdBuySerial = 0;
		m_iResult = 0;
		m_qdTransactionId = 0;
		byParam = m_bySParam;
	}

	/// 服务器id
	BYTE m_byServerId;
	/// 购买者
	DWORD m_dwAccountId;

	QWORD m_qdBuySerial;

	int m_iResult;

	/// 对方的交易号
	QWORD m_qdTransactionId;

	MODI_INAItemPrice pData;
	static const BYTE m_bySParam = 6;
}__attribute__((packed));



/// INA赠送道具
struct MODI_INAGiveItem: public MODI_TransCommand
{
	MODI_INAGiveItem()
	{
		byParam = m_bySParam;
		m_byServerId = 0;
		m_dwAccountId = 0;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		memset(m_cstrGiftRoleName, 0, sizeof(m_cstrGiftRoleName));
		memset(m_cstrGiftAccName, 0, sizeof(m_cstrGiftAccName));
		m_wdSize = 0;
		m_qdBuySerial = 0;
	}

	/// 服务器id
	BYTE m_byServerId;
	/// 购买者
	DWORD m_dwAccountId;
	char m_cstrAccName[50];

	QWORD m_qdBuySerial;

	MODI_INAItemPrice pData;

	/// 赠送人的accname和角色名
	char m_cstrGiftRoleName[ROLE_NAME_MAX_LEN + 1];
	char m_cstrGiftAccName[50];

	/// 赠送信息
	DWORD m_wdSize;
	char m_pGiveData[0];
	
	static const BYTE m_bySParam = 7;
}__attribute__((packed));


/// 赠送扣款返回
struct MODI_INAGiveReturn: public MODI_TransCommand
{
	MODI_INAGiveReturn()
	{
		byParam = m_bySParam;
		m_byServerId = 0;
		m_dwAccountId = 0;
		m_qdBuySerial = 0;
		m_iResult = 0;
		m_qdTransactionId = 0;
		m_wdSize = 0;
		byParam = m_bySParam;
	}

	/// 服务器id
	BYTE m_byServerId;
	/// 购买者
	DWORD m_dwAccountId;

	QWORD m_qdBuySerial;

	int m_iResult;

	/// 对方的交易号
	QWORD m_qdTransactionId;

	DWORD m_wdSize;
	char m_pGiveData[0];
	
	static const BYTE m_bySParam = 8;
}__attribute__((packed));

#endif
