/** 
 * @file sessionid_creator.h
 * @brief session id 生成器定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef SESSION_ID_CREATOR_H_
#define SESSION_ID_CREATOR_H_

#include "SingleObject.h"
#include "session_id.h"

class 	MODI_SessionIDCreator : public CSingleObject<MODI_SessionIDCreator>
{
	public:

		MODI_SessionIDCreator();
		void 	Create( MODI_SessionID & id , DWORD nIP , WORD nPort );

	private:

		DWORD 	m_nSeq;
};


#endif
