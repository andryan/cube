/**
 * @file LoggerInfo.h
 * @date 2010-01-05 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 日志信息类
 *
 */

#ifndef _LOGGERINFO_H
#define _LOGGERINFO_H

/***************** 中文日志系统动作定义 *******************/
#ifdef _LOGGER_CHINESE
#define LOGINFO_SUCCESS "成功"
#define LOGINFO_FAILED "失败"
#define LOGINFO_LEGAL "合法"
#define LOGINFO_ILLEGAL "非法"
#define LOGINFO_EXEC "执行"
#define LOGINFO_INIT "初始化"
#define LOGINFO_CONN "连接"
#define LOGINFO_ADD "增加"
#define BYTEBUFFER_OUT "ByteBuffer缓冲溢出"
#define LOGINFO_URLINFO "获取URL信息失败"

#define LOGINFO_INFO1 "[%s] 参数个数(%d)过多"
#define LOGINFO_INFO2 "[%s] 服务器初始化失败"

/// SOCKET
#define LOGINFO_SOCKINFO1 "[%s] 服务器%s重复初始化"
#define LOGINFO_SOCKINFO2 "[%s] 套接口%s创建失败"
#define LOGINFO_SOCKINFO3 "[%s] 套接口%s重用选项失败"
#define LOGINFO_SOCKINFO4 "[%s] 套接口%s接收缓冲设置失败"
#define LOGINFO_SOCKINFO5 "[%s] 套接口%s发送缓冲设置失败"
#define LOGINFO_SOCKINFO6 "[%s] %s绑定端口失败"
#define LOGINFO_SOCKINFO7 "[%s] %s监听端口失败"
#define LOGINFO_SOCKINFO8 "[%s] %s注册到EPOLL失败"
#define LOGINFO_SOCKINFO9 "[%s] 连接服务器%s(%u)失败"
#define LOGINFO_SOCKINFO10 "[%s] 连接服务器%s(%u)成功"
#define LOGINFO_SOCKINFO11 "[%s] 服务器已经关闭,断开连接"
#define LOGINFO_SOCKINFO12 "[%s] 连接%s已经关闭"
#define LOGINFO_SOCKINFO13 "[%s] %s注册epoll事件失败"
#define LOGINFO_SOCKINFO14 "[%s] %s删除epoll事件失败"
#define LOGINFO_SOCKINFO15 "[%s] %s套接口非法关闭"
#define LOGINFO_SOCKINFO16 "[%s] 获取套接口文件状态失败"
#define LOGINFO_SOCKINFO17 "[%s] 设置套接口文件状态失败"

/// XML
#define LOGINFO_XMLINFO1 "[%s] 初始化xml(%s)文件失败"
#define LOGINFO_XMLINFO2 "[%s] 获取%s的根节点%s失败"
#define LOGINFO_XMLINFO3 "[%s] 获取%s的节点%s失败"
#define LOGINFO_XMLINFO4 "[%s] 获取%s节点%s的%s属性值失败"

/// MEM
#define LOGINFO_MEMINFO1 "[%s] ByteBuffer写指针异常%s(%d,%d)"
#define LOGINFO_MEMINFO2 "[%s] ByteBuffer写数据大小异常%s(%d,%d)"
#define LOGINFO_MEMINFO3 "[%s] ByteBuffer缓冲溢出%s(%d,%d)"

/// Thread
#define LOGINFO_THREADINFO1 "[%s] 线程%s已经存在"
#define LOGINFO_THREADINFO2 "[%s] 线程%s创建失败"
#define LOGINFO_THREADINFO3 "[%s] 线程%s创建成功"
#define LOGINFO_THREADINFO4 "[%s] 线程%s创建超时"
#define LOGINFO_THREADINFO5 "[%s] 线程个数超出设定值"

/// DB
#define LOGINFO_DBINFO1 "[%s] 插入的字段(%s)在此表中(%s)不存在"
#define LOGINFO_DBINFO2 "[%s] 获取数据库连接信息IP失败"
#define LOGINFO_DBINFO3 "[%s] 获取数据库连接信息USER失败"
#define LOGINFO_DBINFO4 "[%s] 获取数据库连接信息PASSWORD失败"
#define LOGINFO_DBINFO5 "[%s] 获取数据库连接信息PORT失败"
#define LOGINFO_DBINFO6 "[%s] 获取数据库连接信息DBNAME失败"
#define LOGINFO_DBINFO7 "[%s] %s调用mysql_init失败"
#define LOGINFO_DBINFO8 "[%s] %s调用mysql_real_connect失败"
#define LOGINFO_DBINFO9 "[%s] %s调用mysql_list_tables失败"
#define LOGINFO_DBINFO10 "[%s] 连接数据库%s失败"
#define LOGINFO_DBINFO11 "[%s] 执行SQL语句(%s)失败"
#define LOGINFO_DBINFO12 "[%s] 数据库连接句柄(HandleID=%d)长时间(使用时间>%d秒)没有释放"

/// GateService
#define LOGINFO_GATEINFO1 "[%s] 此连接%s没有对应的用户"
#define LOGINFO_GATEINFO2 "[%s] 接收到%s(%d)的命令byCmd=%d,byParam=%d"
#define LOGINFO_GATEINFO3 "[%s] 转发%s(%d)的命令(byCmd=%d,byParam=%d)到GS"
#define LOGINFO_GATEINFO4 "[%s] 转发命令给GS(ip=%s,port=%d)失败"
#define LOGINFO_GATEINFO5 "[%s] 接收到%s(%d)的非法数据(byCmd=%d,byParam=%d)"
#define LOGINFO_GATEINFO6 "[%s] %s接收到命令(byCmd=%d,byParam=%d)"
#define LOGINFO_GATEINFO7 "[%s] %s无法找到要转发的客户端(%u)"
#define LOGINFO_GATEINFO8 "[%s] %s解析GS2C的命令(%d,%d)失败"
#define LOGINFO_GATEINFO9 "[%s] 接收到%s(%d)的非法数据(byCmd=%d,byParam=%d)"
#define LOGINFO_GATEINFO10 "[%s] 网关新增连接(ip=%s,port=%d,sessionID=%u)"

#endif

/***************** 英文日志系统动作定义 *******************/
#ifdef _LOGGER_ENGLISH
#define LOGINFO_INFO1 "[%s] Too many parameters:(%d)"
#define LOGINFO_INFO2 "[%s] Service initialization failed"

/// SOCKET
#define LOGINFO_SOCKINFO1 "[%s] Service %s reinitialized"
#define LOGINFO_SOCKINFO2 "[%s] Socket %s creation failed"
#define LOGINFO_SOCKINFO3 "[%s] Socket %s reuse failed"
#define LOGINFO_SOCKINFO4 "[%s] Socket %s receive buffer setup failed"
#define LOGINFO_SOCKINFO5 "[%s] Socket %s send buffer setup failed"
#define LOGINFO_SOCKINFO6 "[%s] %s port binding failed"
#define LOGINFO_SOCKINFO7 "[%s] %s port listening failed"
#define LOGINFO_SOCKINFO8 "[%s] %s register to EPOLL failed"
#define LOGINFO_SOCKINFO9 "[%s] Connect to service %s(%u) failed"
#define LOGINFO_SOCKINFO10 "[%s] Connect to service %s(%u) success"
#define LOGINFO_SOCKINFO11 "[%s] Service closed, please reconnect"
#define LOGINFO_SOCKINFO12 "[%s] Connection %s closed"
#define LOGINFO_SOCKINFO13 "[%s] %s Reg epoll event failed"
#define LOGINFO_SOCKINFO14 "[%s] %s Del epoll event failed"
#define LOGINFO_SOCKINFO15 "[%s] %s Socket closed invalid"
#define LOGINFO_SOCKINFO16 "[%s] Get socket file control failed"
#define LOGINFO_SOCKINFO17 "[%s] Set socket file control failed"

/// XML
#define LOGINFO_XMLINFO1 "[%s] XML file (%s) initialization failed"
#define LOGINFO_XMLINFO2 "[%s] Obtain %s root node %s failed"
#define LOGINFO_XMLINFO3 "[%s] Obtain %s node %s failed"
#define LOGINFO_XMLINFO4 "[%s] Obtain %s node %s value %s failed"

/// MEM
#define LOGINFO_MEMINFO1 "[%s] ByteBuffer write pointer error %s(%d,%d)"
#define LOGINFO_MEMINFO2 "[%s] ByteBuffer write data size error %s(%d,%d)"
#define LOGINFO_MEMINFO3 "[%s] ByteBuffer overflow %s(%d,%d)"

/// Thread
#define LOGINFO_THREADINFO1 "[%s] Thread %s already exist"
#define LOGINFO_THREADINFO2 "[%s] Thread %s creation failed"
#define LOGINFO_THREADINFO3 "[%s] Thread %s created successfully"
#define LOGINFO_THREADINFO4 "[%s] Thread %s created timedout"
#define LOGINFO_THREADINFO5 "[%s] Thread Num more than count"

/// DB
#define LOGINFO_DBINFO1 "[%s] Unable to find field(%s) within table(%s)"
#define LOGINFO_DBINFO2 "[%s] Retrieving DB connection info:IP failed"
#define LOGINFO_DBINFO3 "[%s] Retrieving DB connection info:USER failed"
#define LOGINFO_DBINFO4 "[%s] Retrieving DB connection info:PASSWORD failed"
#define LOGINFO_DBINFO5 "[%s] Retrieving DB connection info:PORT failed"
#define LOGINFO_DBINFO6 "[%s] Retrieving DB connection info:DBNAME failed"
#define LOGINFO_DBINFO7 "[%s] %s failed to call mysql_init"
#define LOGINFO_DBINFO8 "[%s] %s failed to call mysql_real_connect"
#define LOGINFO_DBINFO9 "[%s] %s failed to call mysql_list_tables"
#define LOGINFO_DBINFO10 "[%s] Connect to DB %s failed"
#define LOGINFO_DBINFO11 "[%s] Execute SQL(%s) failed"
#define LOGINFO_DBINFO12 "[%s] DB connect handle(HandleID=%d)used long time(use time>%d second),please release"

/// GateService
#define LOGINFO_GATEINFO1 "[%s] No proper user connection %s"
#define LOGINFO_GATEINFO2 "[%s] Receive command(byCmd=%d,byParam=%d) from %s(%d)"
#define LOGINFO_GATEINFO3 "[%s] Forward %s(%d) command(byCmd=%d,byParam=%d) to GS"
#define LOGINFO_GATEINFO4 "[%s] Command forwarding to GS(ip=%s,port=%d) failed"
#define LOGINFO_GATEINFO5 "[%s] Illegal date(byCmd=%d,byParam=%d) from %s(%d)"
#define LOGINFO_GATEINFO6 "[%s] %s receive command(byCmd=%d,byParam=%d)"
#define LOGINFO_GATEINFO7 "[%s] %s unable to find relay client(%u)"
#define LOGINFO_GATEINFO8 "[%s] %s parse GS2C command(%d,%d)failed"
#define LOGINFO_GATEINFO9 "[%s] Illegal data (byCmd=%d,byParam=%d) from %s(%d)"
#define LOGINFO_GATEINFO10 "[%s] New gateway connection(ip=%s,port=%d,sessionID=%u)"

/// OLD
#define LOGINFO_SUCCESS "成功"
#define LOGINFO_FAILED "失败"
#define LOGINFO_LEGAL "合法"
#define LOGINFO_ILLEGAL "非法"
#define LOGINFO_EXEC "执行"
#define LOGINFO_INIT "初始化"
#define LOGINFO_CONN "连接"
#define LOGINFO_ADD "增加"
#define BYTEBUFFER_OUT "ByteBuffer缓冲溢出"
#define LOGINFO_URLINFO "获取URL信息失败"
#endif
/***************** 英文日志系统动作定义结束 *******************/

/* 房间操作的基本信息（操作的房间号 , 动作发起者的名字 */
#define GS_ROOM_OPT_BASEINFO "房间号<%u> , 玩家角色名<%s> ."

/* 房间操作的基本信息（操作的房间号 ， 操作的房间的房间名 , 动作发起者的名字 ， 动作发起者的GUID */
#define GS_ROOM_OPT_BASEINFO_EX "房间号<%u> ,房间名<%s>, 玩家角色名<%s> .玩家角色GUID<%ull> ."

/* 输出信息 , 格式 ＝ 动作类型 ＋ 描述参数1  + 描述参数N */
#define GS_DUMP_PARAM_1 "[%s]"
#define GS_DUMP_PARAM_2 "[%s] (%s)"
#define GS_DUMP_PARAM_3 "[%s] (%s) (%s)"
#define GS_DUMP_PARAM_4 "[%s] (%s) (%s) (%s)"


#endif

