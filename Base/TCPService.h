/**
 * @file TCPService.h
 * @date 2009-12-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 监听服务器
 *
 */

#ifndef _MDTCPSERVICE_H
#define _MDTCPSERVICE_H

/**
 * @brief 服务器监听类
 *
 */
class MODI_TCPService
{
 public:

	/**
	 * @param port 服务器要邦定的端口
	 *
	 */
	MODI_TCPService(const WORD & port, const char * service_ip);

	~MODI_TCPService();

	///初始化
	bool Init();

	/// 监听
	int Accept(struct sockaddr_in * client_addr);

	
	/** 
	 * @brief 获取服务器绑定端口
	 * 
	 * @return 
	 */
	const WORD & GetPort() const
	{
		return m_wdPort;
	}
	
 private:
	/// 最大的等待队列 
	static const int MAX_WAITQUEUE = 2000;

	/// epoll 轮询超时ms
	static const int MAX_POLLTIME = 2000;
	
	/// 套接口
	int m_stSock;

	/// bind端口
	WORD m_wdPort;

	/// 要bind的ip
	std::string m_strServiceIP;
	
	/// epoll
	int m_stEpollfd;
};

#endif
