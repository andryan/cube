/** 
 * @file s2ms_cmd.h
 * @brief mananger server 协议定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef MODI_STOMANANGERSERVER_CMD_H_
#define MODI_STOMANANGERSERVER_CMD_H_

#ifdef _MSC_VER
#pragma once
#endif

#include <string>
#include <list>
#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"

using std::list;
using std::string;

#pragma pack(push,1)

static const unsigned char MAINCMD_S2MS = 0x11;

struct MODI_S2MS_Cmd : public stNullCmd
{
	MODI_S2MS_Cmd()
	{
		byCmd = MAINCMD_S2MS;
	}
};

/// 服务器向MANANGER SERVER注册自己
struct MODI_S2MS_Request_ServerReg : public MODI_S2MS_Cmd
{
	int 				m_iServerType; // 服务器类型

	MODI_S2MS_Request_ServerReg()
	{
		byParam = ms_SubCmd;
		m_iServerType = SVR_TYPE_NULL;
	}

	static const unsigned char ms_SubCmd = 0x03;
};



#pragma pack(pop)

#endif
