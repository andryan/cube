/** 
 * @file SqlStatement.h
 * @brief sql statement 
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-26
 */

#ifndef SQL_STATEMENT_H_
#define SQL_STATEMENT_H_

#include "protocol/gamedefine.h"
#include "gamestructdef.h"

// account table field 定义
enum
{
	enAccountTableField_id,
	enAccountTableField_account,
	enAccountTableField_password,
	enAccountTableField_email,
	enAccountTableField_activeGame,
	enAccountTableFieldCount,
};

// char table field 定义
enum
{
	enCharTF_guid,
	enCharTF_accountid,
	enCharTF_accname,
	enCharTF_name,
	enCharTF_age,
	enCharTF_city,
	enCharTF_personalsign,
	enCharTF_clan,
	enCharTF_group,
	enCharTF_exp,
	enCharTF_qq,
	enCharTF_sex,
	enCharTF_blood,
	enCharTF_level,
	enCharTF_birthday,
	enCharTF_votecount,
	enCharTF_renqi,
	enCharTF_fensigril,
	enCharTF_fensiboy,
	enCharTF_honeyinfo,
	enCharTF_defavatar,
	enCharTF_lastlogintime,
	enCharTF_lastloginip,
	enCharTF_money,
	enCharTF_moneyRMB,
	enCharTF_lastidoltime,
	enCharTF_wincount,
	enCharTF_losscount,
	enCharTF_tiecount,
	enCharTF_gmlevel,
	enCharTF_readhelp,
	enCharTF_normalflag,
	enCharTF_chang_count,
	enCharTF_heng_count,
	enCharTF_keyboard_count,
	enCharTF_vote_count,
	enCharTF_total_online_time,
	enCharTF_today_online_time,
	enCharTF_consume_rmb,
	enCharTF_highest_score,
	enCharTF_highest_score_musicid,
	enCharTF_highest_precision,
	enCharTF_highest_precision_musicid,
	enCharTF_blockcount, /// 拉黑次数
	enCharTF_chenghao,
	
	/// 把二进制放最后比较好理解
	enCharTF_otherdata,
	enCharsTableFieldCount,
};

// item table field 定义
enum
{
	enItemTF_itemguid,
	enItemTF_charguid,
	enItemTF_worldid,
	enItemTF_serverid,
	enItemTF_serial,
	enItemTF_configid,
	enItemTF_isvalid,
	enItemTF_expiretime,
	enItemTF_count,
	enItemTF_bagtype,
	enItemTF_itempos,
	enItemTableFieldCount,
};

// item serial table 
enum
{
	enItemSerialTF_aid,
	enItemSerialTF_worldid,
	enItemSerialTF_serverid,
	enItemSerialTF_serial,
	enItemSerialTableFieldCount,
};

// relation table
enum
{
	enRelationTF_id,
	enRelationTF_charid,
	enRelationTF_relid,
	enRelationTF_relname,
	enRelationTF_relsex,
	enRelationTF_friendpoint,
	enRelationTF_reltype,
	enRelationTF_isvalid,
	enRelationTableFieldCount,
};

// mail table
enum
{
	enMailTF_id,
	enMailTF_sender,
	enMailTF_recever,
	enMailTF_caption,
	enMailTF_content,
	enMailTF_type,
	enMailTF_state,
	enMailTF_sendtime,
	enMailTF_extrainfo,
	enMailTF_scriptdata,
	enMailTF_isvalid,
	enMailTF_transid,
	enMailTableFieldCount,
};

// shop histroy table
enum
{
	enShopHistroy_sid,
	enShopHistroy_transid,
	enShopHistroy_charid,
	enShopHistroy_itemserial,
	enShopHistroy_createtime,
	enShopHistroy_createtype,
	enShopHistroy_mailid,
	enShopHistroy_recvstate,
	enShopHistroyTableFieldCount,
};

// 运营的活动cdkey
enum
{
	enYunYingCDKey_key,
	enYunYingCDKey_items,
	enYunYingCDKey_charid,
	enYunYingCDKey_isvalid,
	enYunYingCDKeyCount,
};

// 数据库表类型
enum
{
	enTable_Account = 1,
	enTable_Character,
	enTable_Item,
	enTable_ItemSerial,
	enTable_Relation,
	enTable_Mail,
	enTable_ShopHistroy,
	enTable_CDKeyTable,
};

/** 
 * @brief SQL 指令生成类
 */
class MODI_SqlStatement
{
	public:

		/** 
		 * @brief 创建查询帐号的SQL指令
		 * 
		 * @param szAccount 	帐号名
		 * @param szPassword    帐号密码
		 * 
		 * @return 	成功返回 sql 语句
		 */
		static const char * 	CreateStatement_Sel_Account( const char * szAccount , const char * szPassword );

		/** 
		 * @brief 创建查询角色列表的SQL指令
		 * 
		 * @param nAccountID    帐号ID
		 * 
		 * @return 	成功返回 sql 语句
		 */
		static const char * 	CreateStatement_Sel_Charslist( defAccountID nAccountID );

		/** 
		 * 
		 * @brief 创建插入角色信息的 SQL 语句
		 *
		 * @param nAccountID    帐号ID
		 * @param pInfo 		角色的信息结构
		 * 
		 * @return 	成功返回 sql 语句
		 */
		static const char * 	CreateStatement_Ins_CharacterEx( defAccountID nAccountID , MODI_CHARID charID , const char * acc_name , const void * pInfo );

		/** 
		 * @brief 创建加载角色信息的 SQL 语句
		 * 
		 * @param charID 帐号id 角色ID
		 * 
		 * @return 	 成功返回 SQL 语句
		 */
		static const char * 	CreateStatement_Load_CharData( defAccountID nAccountID , MODI_CHARID charID );

		static const char * 	CreateStatement_Load_CharData( const char * szName );

		/** 
		 * @brief 创建更新角色信息的SQL语句
		 * 
		 * @param charID 角色ID
		 * 
		 * @return 	成功返回 SQL 语句
		 */
		static const char * 	CreateStatement_Update_CharData( MODI_CHARID charID , const MODI_DBSaveCharBaseInfo * pSave);

		static const char * 	CreateStatement_ResetTodayOnlineTIme();

		/** 
		 * @brief 创建查询是否有某个角色的SQL语句
		 * 
		 * @param szRoleName
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_HasChar( const char * szRoleName );

		static const char * 	CreateStatement_UpdateLastLogin( time_t nTime , const char * szIP , MODI_CHARID charid );

		/** 
		 * @brief 创建查询最大值的SQL语句
		 * 
		 * @param iTable  表类型
		 * @param szMaxWhat 哪个字段
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_Max( int iTable , const char * szMaxWhat );

		/** 
		 * @brief 创建查询某个角色的物品列表的SQL语句
		 * 
		 * @param charID 角色ID
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_LoadItemlist( MODI_CHARID charID );


		/** 
		 * @brief 创建查询某个角色的AVATAR列表的SQL语句
		 * 
		 * @param charID 角色ID
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_LoadAvatarIDlist( MODI_CHARID charID );

		/** 
		 * @brief 创建保存物品的SQL语句
		 * 
		 * @param charID 角色ID
		 * @param pSave  物品属性
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_SaveItem( MODI_CHARID charID , void * pSave );

		/** 
		 * @brief 创建载入物品序列表的SQL语句
		 * 
		 * @param byWorld  世界号
		 * @param byServerID 服务器号
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_LoadItemSerial( BYTE byWorld , BYTE byServerID );

		/** 
		 * @brief 创建保存游戏序列号的SQL语句
		 * 
		 * @param byWorld  游戏世界号
		 * @param byServerID 游戏服务器号
		 * @param nSerial 游戏序列号
		 * @return 	成功返回sql语句
		 */
		static const char * 	CreateStatement_SaveItemSerial( BYTE byWorld , BYTE byServerID , DWORD nSerial );


		static const char * 	CreateStatement_CreateItemHistroy( const void * p , BYTE nIdx );

		static const char * 	CreateStatement_GetYunYingCDKey( const char * szKey );

		static const char * 	CreateStatement_SaveYunYingCDKeyValid( const char * szKey , int iValid , MODI_CHARID charid );

		/** 
		 * @brief 创建加载联系人列表的SQL语句
		 * 
		 * @param nCharid 需要加载的角色ID
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_LoadRelationList( MODI_CHARID nCharid );

		/** 
		 * @brief 创建保存联系人列表的SQL语句
		 * 
		 * @param nCharid 角色ID
		 * 
		 * @return 	成功返回SQL语句
		 */
		static const char * 	CreateStatement_SaveRelationList( MODI_CHARID nCharid , const void * p );

		static const char * 	CreateStatement_SaveRelData( MODI_CHARID nCharid , const void * p );

		/** 
		 * @brief 创建清理无效物品的SQL语句
		 * 
		 * @param byWorld 世界号 
		 * @param byServerID 游戏服务器号
		 * @param nSerial 序列号
		 * 
		 * @return 	成功返回sql语句
		 */
		static const char * 	CreateStatement_ClearupInvalidItems( BYTE byWorld , BYTE byServerID , DWORD nSerial );

		static const char * 	CreateStatement_LoadMaillist( const char * szName );

		static const char * 	CreateStatement_SaveMaillist( const void * pMail );


		/** 
		 * @brief 获得ACCOUNT TBALE中的某个字段的字段名
		 * 
		 * @param nIdx 字段索引
		 * 
		 * @return 	字段名
		 */
		static const char * 	GetFieldInAccountTable( unsigned int nIdx );

		static const char * 	GetFieldInCharsTable( unsigned int nIdx );

		/** 
		 * @brief 获得ITEMINFOS中某个字段的字段名
		 * 
		 * @param nIdx 字段索引
		 * 
		 * @return 	字段名
		 */
		static const char * 	GetFieldInItemTable( unsigned int nIdx );


		/** 
		 * @brief 获得ITEMSERIAL中某个字段的字段名
		 * 
		 * @param nIdx 字段索引
		 * 
		 * @return 	字段名
		 */
		static const char * 	GetFieldInItemSerialTable( unsigned int nIdx );

		/** 
		 * @brief 获得Relation表中的某个字段的字段名
		 * 
		 * @param nIdx 字段索引
		 * 
		 * @return 	字段名
		 */
		static const char * 	GetFieldInRelationTable( unsigned int nIdx );

		static const char * 	GetFieldInMailTable( unsigned int nIdx );

		static const char * 	GetFieldInShopHistroy( unsigned int nIdx );

		static const char * 	GetFieldInYunYingCDKey( unsigned int nIdx );


		/** 
		 * @brief 格式化字符串，用于SQL语句
		 * 
		 * @param szOut 格式化后的字符串（输出参数）
		 * @param nOutSize 输出BUF的大小，必须大于2倍于 nInSize
		 * @param szIn  输入字符串
		 * @param nInSize 输入字符串的大小
		 * 
		 * @return 返回格式化后的字符串	
		 */
		static const char * 	EscapeString( char * szOut , size_t nOutSize , const char * szIn , size_t nInSize );

		static const char * 	CreateStatement_RankLevel();
		static const char * 	CreateStatement_Self_RankLevel();

		static const char * 	CreateStatement_RankRenqi();
		static const char * 	CreateStatement_Self_RankRenqi();

		static const char * 	CreateStatement_RankConsumeRMB();
		static const char * 	CreateStatement_Self_RankConsumeRMB();

		static const char * 	CreateStatement_RankHighestScore();
		static const char * 	CreateStatement_Self_RankHighestScore();

		static const char * 	CreateStatement_RankHighestPrecision();
		static const char * 	CreateStatement_Self_RankHighestPrecision();

	private:


		enum
		{
			enSqlStatementBufSize = 32 * 1024,
			enSqlEscapeBufSize = enSqlStatementBufSize * 2 + 1024,
		};

		static char 			ms_szSqlEscapeBuf[enSqlEscapeBufSize];

		/// 生成SQL指令的缓冲区
		static char 			ms_szSqlStatementBuf[enSqlStatementBufSize];

		// 帐号表的名字
		static const char * 	ms_AccountTable;

		// 角色表的名字
		static const char * 	ms_CharsTable;

		// 物品表的名字
		static const char * 	ms_ItemTable;

		// item serial table's name
		static const char * 	ms_ItemSerialTable;
		
		// 联系人表
		static const char * 	ms_RelationTable;

		// 邮件表
		static const char * 	ms_MailTable; 
	
		// 商城记录表
		static const char * 	ms_ShopHistroyTable;

		// 运营cdkey表
		static const char * 	ms_YunYingCDKeyTable;

		// accounts 表格中的字段名
		static const std::string ms_strAccountTableField[enAccountTableFieldCount];

		// chars 表格中的字段名
		static const std::string ms_strCharsTableField[enCharsTableFieldCount];

		// item 表格中的字段名
		static const std::string ms_strItemTableField[enItemTableFieldCount];

		// item serial
		static const std::string ms_strItemSerialTableField[enItemSerialTableFieldCount];

		// relation 表中的字段名
		static const std::string ms_strRelationTableField[enRelationTableFieldCount];

		// mails 表中的字段名
		static const std::string ms_strMailTableField[enMailTableFieldCount];

		// shop histroy 表中的字段名
		static const std::string ms_strShopHistroyTableField[enShopHistroyTableFieldCount];

		// 运营时用的活动cdkey表
		static const std::string ms_strYunYingCDKeyTableField[enYunYingCDKeyCount];


		static const char * 	ms_SelectMax;

		/*
		 * account operator
		 */

		/// 查询帐号的 SQL 语句模版
		static const char * 	ms_szSelectAccount;


		/*
		 * characters operator
		 */

		/// 查询角色列表的 SQL 语句模板
		static const char * 	ms_szSelectCharslist;

		/// 增加角色的 SQL 语句模板
		static const char * 	ms_szInsertChar;

		/// 加载角色所有数据的 SQL 语句模板
		static const char * 	ms_szLoadCharData;

		static const char * 	ms_szLoadCharDataByName;

		/// 更新角色数据的 SQL 语句模板
		static const char * 	ms_szUpdateCharData;

		static const char * 	ms_szResetOnlineTime;

		/// 是否有某个角色
		static const char * 	ms_szIsHasChar; 

		static const char * 	ms_szUpdateLastLogin;


		/*
			item operator 
		 */

		/// 加载物品列表
		static const char * 	ms_szLoadItemlist;

		/// 保存某个物品
		static const char * 	ms_szSaveItem;

		static const char * 	ms_szSaveShopHistroy;

		static const char * 	ms_szGetYunYingCDKey;

		static const char * 	ms_szSaveYunYingCDKey;

		/// 加载AVATAR列表
		static const char * 	ms_szLoadAvatarID;

		/// 载入物品序列号
		static const char * 	ms_szLoadServerItemSerial;

		/// 保存物品序列号
		static const char * 	ms_szSaveServerItemSerial;

		/// 清无效物品
		static const char * 	ms_szClearupInvalidItems;

		///  加载邮件列表
		static const char * 	ms_szLoadMaillist;

		/// 保存邮件列表
		static const char * 	m_szSaveMaillist;

		/*
		   relation operator
		 */

		/// 加载联系人
		static const char * 	ms_LoadRelation;

		/// 保存联系人
		static const char * 	ms_SaveRelation;

		/// 保存社会关系信息
		static const char * 	ms_SaveSocialRelData;

		/*
		   ranks
		  */

		static const char * 	ms_Rank_Renqi;
		static const char * 	ms_Self_Rank_Renqi;

		static const char * 	ms_Rank_Level;
		static const char * 	ms_Self_Rank_Level;

		static const char * 	ms_Rank_ConsumeRMB;
		static const char * 	ms_Self_Rank_ConsumeRMB;

		static const char * 	ms_Rank_HighestScore;
		static const char * 	ms_Self_Rank_HighestScore;

		static const char * 	ms_Rank_HighestPrecision;
		static const char * 	ms_Self_Rank_HighestPrecision;
	
};

#endif

