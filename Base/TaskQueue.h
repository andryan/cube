/**
 * @file TaskQueue.h
 * @date 2009-12-11 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 任务池
 *
 */

#ifndef _MDTASKQUEUE_H
#define _MDTASKQUEUE_H

#include <queue>
#include "Global.h"
#include "ServiceTask.h"

typedef std::pair<volatile bool, MODI_ServiceTask *  > defTaskQueue;

const DWORD g_dwTaskSize = 6000;

class MODI_TaskQueue
{
 public:
	MODI_TaskQueue()
	{
		m_dwReadAddr = 0;
		m_dwWriteAddr = 0;
	}


	~MODI_TaskQueue()
	{
		Final();
	}
	bool Put(MODI_ServiceTask * p_task)
	{
		if(!TaskQueue[m_dwWriteAddr].first)
		{
			TaskQueue[m_dwWriteAddr].second = p_task;
			TaskQueue[m_dwWriteAddr].first = true;
			m_dwWriteAddr = (++m_dwWriteAddr) % g_dwTaskSize;
			return true;
		}
		return false;
	}

	MODI_ServiceTask * Get()
	{
		MODI_ServiceTask * p_task = NULL;
		if(TaskQueue[m_dwReadAddr].first)
		{
			p_task = TaskQueue[m_dwReadAddr].second;
			TaskQueue[m_dwReadAddr].first = false;
			m_dwReadAddr = (++m_dwReadAddr) % g_dwTaskSize;
		}
		return p_task;
	}

 private:


	/// 释放资源
	void Final()
	{

	}
	
 private:
	defTaskQueue TaskQueue[g_dwTaskSize];

	unsigned int m_dwReadAddr;
	unsigned int m_dwWriteAddr;
};

#endif
