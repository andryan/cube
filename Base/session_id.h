/** 
 * @file session_id.h
 * @brief session id 定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef MODI_SESSION_ID_H_
#define MODI_SESSION_ID_H_

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <ext/hash_map>


#include "BaseOS.h"




class MODI_SessionIDCreator;

#pragma pack(push,1)
class MODI_SessionID
{

	friend class MODI_SessionIDCreator;

	DWORD 		m_nIP;         // ip
	WORD 		m_nPort;       // port
	DWORD 		m_nSeqID;      // 序列ID

	public:


	
	MODI_SessionID() 
	{
		memset( this , 0 , sizeof(MODI_SessionID) );
	}

	DWORD 	GetIP() const
	{
		return m_nIP;
	}

	const char * GetIPString() const
	{
		struct in_addr input_addr;
		input_addr.s_addr = htonl( m_nIP );
		const char * szIP = inet_ntoa(input_addr);
		return szIP;
	}

	WORD 	GetPort() const
	{
		return m_nPort;
	}

	DWORD 	GetSeq() const
	{
		return m_nSeqID;
	}

	bool 	IsVaild() const
	{
		return m_nIP || m_nPort || m_nSeqID;
	}

	bool 	IsInvaild() const
	{
		return !IsVaild();
	}

	void 	SetInvaild()
	{
		m_nIP = 0;
		m_nPort = 0;
		m_nSeqID = 0; 
	}

	bool operator < ( const MODI_SessionID & r ) const
	{
		if( this->m_nSeqID < r.m_nSeqID )
			return true;
		else if((this->m_nSeqID == r.m_nSeqID) && (this->m_nPort < r.m_nPort))
		{
			return true;
		}

		return false;
	}

	bool operator > ( const MODI_SessionID & r ) const 
	{
		if( this->m_nSeqID > r.m_nSeqID )
		{
			return true;
		}
		else if((this->m_nSeqID == r.m_nSeqID) && (this->m_nPort > r.m_nPort))
		{
			return true;
		}

		return false;
	}

	bool operator == ( const MODI_SessionID & r ) const
	{
		return ((this->m_nSeqID == r.m_nSeqID) && 
				(this->m_nPort == r.m_nPort) &&
				(this->m_nIP == r.m_nIP));
	}

	bool operator != ( const MODI_SessionID & r ) const
	{
		return ((this->operator == ( r )) == false);
	}

	// note : not thread safe
	const char * ToString() const
	{
		static char szTemp[256];
		memset( szTemp , 0 , sizeof(szTemp) );

		struct in_addr input_addr;
		input_addr.s_addr = htonl( m_nIP );
		const char * szIP = inet_ntoa(input_addr);

		SNPRINTF( szTemp , sizeof(szTemp) , "ip=%s,port=%u,seq=%u"  ,  
				szIP , m_nPort ,  m_nSeqID );
		return szTemp;
	}

	std::string ToStringThreadSafe() const
	{
		char szTemp[256];
		memset( szTemp , 0 , sizeof(szTemp) );
		
		struct in_addr input_addr;
		input_addr.s_addr = htonl( m_nIP );
		const char * szIP = inet_ntoa(input_addr);

		SNPRINTF( szTemp , sizeof(szTemp) , "ip<%s>,port<%u>,Seq<%u>."  ,  
				szIP , m_nPort ,  m_nSeqID );
		return std::string( szTemp );
	}
};

#pragma pack(pop)




#endif
