/**
 * @file MTCPService.h
 * @date 2009-12-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 多监听服务器
 *
 */

#ifndef _MDMTCPSERVICE_H
#define _MDMTCPSERVICE_H

/**
 * @brief 多服务器监听类
 *
 */
class MODI_MTCPService
{
 public:

	/**
	 * @param port 服务器要邦定的端口
	 *
	 */
	MODI_MTCPService(std::vector<WORD> & port_vec, std::vector<std::string > & ip_vec);

	~MODI_MTCPService();

	///初始化
	bool Init();

	/// 监听
	int Accept(std::vector<struct sockaddr_in> & client_addr, std::vector<int> & sock_vec, std::vector<WORD> & port_vec);

 private:
	/// 最大的等待队列 
	static const int MAX_WAITQUEUE = 2000;

	/// epoll 轮询超时ms
	static const int MAX_POLLTIME = 2000;
	
	/// 套接口
	std::vector<int> m_stSockVec;

	/// bind端口
	std::vector<WORD> m_stPortVec;
	std::vector<std::string> m_stIPVec;

	std::map<int, WORD > m_stMap;
	
	/// epoll
	int m_stEpollfd;

	std::vector<struct epoll_event> m_stEpollEvent;
};

#endif
