/**
 * @file Global.h
 * @date 2009-12-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 全局变量
 *
 */

#ifndef _MDGLOBAL_H
#define _MDGLOBAL_H

#include "Type.h"
#include "Define.h"
#include "Mutex.h"
#include "RWLock.h"
#include "Cond.h"
#include "Timer.h"
#include "LoggerAction.h"
#include "LoggerInfo.h"
#include "Logger.h"
#include "StringMap.h"

#define		GETVAR(NAME,ATTRI,CLIENT,PVAR) \
			       	{\
			       PVAR=CLIENT->m_stGameUserVarMgr.GetVar(NAME);\
				if( !PVAR) \
				{\
					PVAR=new MODI_GameUserVar(NAME);\
					if(PVAR->Init(CLIENT,ATTRI))\
					{\
						if( !CLIENT->m_stGameUserVarMgr.AddVar(PVAR))\
						{\
							delete PVAR;\
							PVAR=NULL;\
							MODI_ASSERT(0);\
						}\
						else \
						{\
							PVAR->SaveToDB();\
						}\
					}\
					else \
					{\
						delete  PVAR;\
						PVAR=NULL;\
						MODI_ASSERT(0);\
					}\
				}\
				}
namespace Global
{

	/// 全局数据
	extern MODI_StringMap Value;

	/// 数据库配置
	extern MODI_StringMap g_stURLMap;

	///随即数
	extern unsigned int __thread g_dwRandNum;
	
	/// 声明一个全局的日志系统
	extern MODI_Logger * logger;
	extern MODI_Logger * music_logger;
	extern MODI_Logger * net_logger;
#ifdef _FB_LOGGER
	extern MODI_Logger * fb_logger;
#endif
	

	extern MODI_RTime m_stRTime; 

	/// 逻辑线程用的
	extern MODI_RTime m_stLogicRTime; 


	/// 服务器绑定端口
	extern WORD g_wdServicePort;

	/// 服务器的IP地址
	extern std::string g_strServiceIP;

	/// 日志文件路径
	extern std::string g_strLogPath;

	///平台地址
	extern std::string g_strPlatformIP;
	extern WORD g_wdPlatformPort;
	extern WORD g_wdPlatformFlag;

	/// bill地址
	extern std::string g_strBillIP;
	extern WORD g_wdBillPort;

	/// info的地址
	extern std::string g_strInfoIP;
	extern WORD g_wdInfoPort;

	/// unify的地址
	extern std::string g_strUnifyIP;
	extern WORD g_wdUnifyPort;

	extern std::string g_strPayIP;
    extern WORD g_wdPayPort;

	/// yyuser sync地址
	extern std::string g_strAccSyncIP;
	extern WORD g_wdAccSyncPort;

	/// 是否加载脚本
	extern BYTE g_byReloadScript;
	extern BYTE g_byReloadPackage;
	

	extern DWORD	g_nExcepLogout;

	extern	DWORD	g_nNormalLogout;

	/// 道具开始id
	extern QWORD g_qdItemId;
	/// 道具id锁
	extern MODI_RWLock m_stItemIdLock;

	/// 商场购买东西标识
	extern BYTE g_byGameShop;
};

extern "C" unsigned long GetTickCount(bool bUpdate = false);
extern "C" unsigned int GetChannelIDFromArgs();
extern "C" unsigned int GetProxyDBIDFromArgs();
//extern "C" bool 	SavePidFile( const char * szPidFile , pid_t pid );
//extern "C" bool 	RemovePidFile( const char * szPidFile );

#endif
