/**
 * @file   XMLParser.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Feb  9 15:02:53 2010
 * @version $Id $
 * @brief  xml解析器
 * 
 */

#ifndef _MDXMLPARSER_H
#define _MDXMLPARSER_H

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <string>

/**
 * @brief xml解析器
 * 
 */
class MODI_XMLParser
{
 public:
	MODI_XMLParser()
	{
		m_doc = NULL;
	}

	~MODI_XMLParser()
	{
		Final();
	}

	/// 根据文件初始化
	bool InitFile(const char * file_name);

	/// 根据字符串初始化 
	bool InitStr(const char * str_content);

	/// 获取xml的内容
   	bool GetXMLContent(std::string & out_content);

	/// 获取xml节点的内容
	bool GetNodeContent(xmlNodePtr p_node, std::string & out_content, bool head=true);

	/// 获取xml根节点
	xmlNodePtr GetRootNode(const char * root_name);

	/// 获取某节点的子节点
	xmlNodePtr GetChildNode(const xmlNodePtr p_parent, const char *child_name , unsigned int nIdx = 1);

	/// 获取下一个节点
	xmlNodePtr GetNextNode(const xmlNodePtr p_node, const char *next_name = NULL);

	/// 获取节点的内容(数字)
	bool GetNodeNum(const xmlNodePtr p_node, const char *sub_name, void *ret_value, const int ret_size);

	/// 获取节点的内容(字符串)
	bool GetNodeChar(const xmlNodePtr p_node, const char *sbu_name, void *ret_value, const int ret_size);

	/// 获取节点的内容(std::string)
	bool GetNodeStr(const xmlNodePtr p_node, const char *sbu_name, std::string & ret_value);

	const char * GetFilename() const
	{
		return m_strFilename.c_str();
	}
	
private:
	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final()
	{
		if(m_doc)
		{
			xmlFreeDoc(m_doc);
			m_doc = NULL;
		}
	}

	/// 文件指针
	xmlDocPtr m_doc;
	std::string m_strFilename;
	
};

#endif
