/** 
 * @file Channellist.h
 * @brief 游戏频道列表
 * @author Tang Teng
 * @version v0.1
 * @date 2010-04-28
 */

#ifndef MODI_SERVERCHANNEL_LIST_H_
#define MODI_SERVERCHANNEL_LIST_H_

#include "TMap.h"
#include "protocol/gamedefine.h"
#include "SingleObject.h"
#include "RWLock.h"

struct MODI_ChannelAddr
{
	MODI_ChannelAddr()
	{
		m_byChannleID = 0;
		m_wdChannelPort = 0;
	}
	
	BYTE m_byChannleID;
	std::string m_strNetType;
	std::string m_strChannelIP;
	WORD m_wdChannelPort;
	std::string m_strTSvrIP;
	WORD m_wdTSvrPort;
};

class 	MODI_ServiceTask; 	
struct 	MODI_LS2C_Notify_Channellist; 

class 	MODI_SvrChannellist : public MODI_TMap<unsigned int,MODI_ServerStatus > , 
	public CSingleObject<MODI_SvrChannellist>
{
	public:

		MODI_SvrChannellist(); 
		virtual ~MODI_SvrChannellist();


	public:

		int 	CreateSendStream( char * pBuf );
		int 	CreateSendPackage( MODI_LS2C_Notify_Channellist  * pMsg , bool bIsSendToGameClient, const defAccountID & acc_id);
		int 	SendChannellistToGameClient( MODI_ServiceTask * pTask , int & iChannelNum , int iReason , const defAccountID & acc_id);

		void AddChannelAddr(MODI_ChannelAddr & addr)
		{
			m_stChannelAddr[addr.m_byChannleID][addr.m_strNetType] = addr;
		}

		void ClearAddr()
		{
			m_stChannelAddr.clear();
		}

		bool GetChannelAddr(BYTE channel_id, const char * net_type, MODI_ChannelAddr & addr)
		{
			if(!net_type || channel_id == 0)
				return false;

			MODI_AutoRDLock lock(m_stlock);
			std::map<BYTE, std::map<std::string, MODI_ChannelAddr > >::iterator iter = m_stChannelAddr.find(channel_id);
			if(iter != m_stChannelAddr.end())
			{
				std::map<std::string, MODI_ChannelAddr>::iterator iter1 = (iter->second).find(net_type);
				if(iter1 != (iter->second).end())
				{
					addr = iter1->second;
					return true;
				}
				return false;
			}
			return false;
		}


		bool SetTSAddr(BYTE channel_id, MODI_ChannelAddr & addr)
		{
			if(channel_id == 0)
				return false;
			
			std::map<BYTE, std::map<std::string, MODI_ChannelAddr > >::iterator iter = m_stChannelAddr.find(channel_id);
			if(iter != m_stChannelAddr.end())
			{
				std::map<std::string, MODI_ChannelAddr>::iterator iter1 = (iter->second).find("dx");
				if(iter1 != (iter->second).end())
				{
					iter1->second.m_strTSvrIP = addr.m_strTSvrIP;
					iter1->second.m_wdTSvrPort = addr.m_wdTSvrPort;
					return true;
				}

				std::map<std::string, MODI_ChannelAddr>::iterator iter2 = (iter->second).find("wt");
				if(iter2 != (iter->second).end())
				{
					iter2->second.m_strTSvrIP = addr.m_strTSvrIP;
					iter2->second.m_wdTSvrPort = addr.m_wdTSvrPort;
					return true;
				}
				
				return false;
			}
			return false;
		}

		bool Init(const char * file_name);
		
		// private:
		std::map<BYTE, std::map<std::string, MODI_ChannelAddr> > m_stChannelAddr;
		MODI_RWLock m_stlock;
};

#endif
