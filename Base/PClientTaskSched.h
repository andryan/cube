/**
 * @file   PClientTaskSched.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Oct 12 14:33:49 2010
 * @version $Id:$
 * @brief  客户连接调度
 * 
 */


#ifndef _MDPCLIENTASKSCHED_H
#define _MDPCLIENTASKSCHED_H

#include "Global.h"
#include "ClientNormalSched.h"
#include "ClientRecycleSched.h"


/**
 * @brief 连接管理
 *
 */
class MODI_PClientTaskSched: public MODI_DisableCopy
{
 public:
	MODI_PClientTaskSched(const WORD normal_thread_num = 8,
						  const WORD recycle_thread_num = 1):
	   	m_stNormalSched(this, normal_thread_num),
	   	m_stRecycleSched(this, recycle_thread_num)
	{
		
	}
		
	/** 
	 * @brief 初始化
	 *
	 * @return 成功true,失败false
	 */
	bool Init();

	/** 
	 * @brief 结束,释放资源
	 * 
	 */
	void Final();

	/** 
	 * @brief 获取连接总数
	 * 
	 * @return 连接数
	 */
	int Size();

	/** 
	 * @brief 获取正常队列连接数量
	 * 
	 * @return 连接数
	 */
	int GetNormalSize();

	/** 
	 * @brief 获取回收队列连接数量 
	 * 
	 * @return 连接数
	 */
	int GetRecycleSize();

	/** 
	 * @brief 连接加入正常管理队列 
	 * 
	 * @param p_task 要加入的连接
	 * 
	 * @return 成功返回true,失败返回false
	 */
	bool AddNormalSched(MODI_PClientTask * p_task);

	/** 
	 * @brief 连接加入回收管理队列
	 * 
	 * @param p_task 要回收的连接 
	 * 
	 * @return 成功返回true,失败返回false
	 */
	bool AddRecycleSched(MODI_PClientTask * p_task);

 private:
	/// 正常管理
   	MODI_ClientNormalSched m_stNormalSched;

	/// 回收管理
   	MODI_ClientRecycleSched m_stRecycleSched;
};

#endif


