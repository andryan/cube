#include "Aformula.h"


const float fSanFenZhiYi = 0.33333f;
const float fErFenZHiYi = 0.5f;
const float fFloatZero = 0.00000001f;


const float fKeySanDianSanFenZhiYi = 0.30303f;

DWORD 	MODI_Aformula::ComputeExp( DWORD nTotalScore , float  fExact , float fExpFactor )
{
	if( fExact < 0.0f )
		return 0;

	// 计算获得经验
	float fAddExpFactor = pow( float(nTotalScore) , fSanFenZhiYi );
	DWORD nAddExp = (DWORD)(fAddExpFactor* fExact);
	nAddExp = (DWORD)(fExpFactor * (float)nAddExp);
	return nAddExp;
}
		
DWORD 	MODI_Aformula::ComputeMoney( DWORD nExp , float fExact , float fMoneyFactor )
{
	DWORD nMoney = 0;
	float fTemp = 0.0f;
	fTemp = pow(float(nExp) , fErFenZHiYi );
	if( fTemp > fFloatZero && fExact > 0.0f )
	{
		float fDivisor = 0.1f;
		if( fExact <= 0.9f )
		{
			fDivisor = 1.0f - fExact;
		}
		
		if( fDivisor > 0.0f )
		{
			fTemp = fTemp / fDivisor;
			fTemp += 0.5f;
			if( fTemp > fFloatZero )
			{
				nMoney = (DWORD)fTemp;
			}
		}
	}

	return (DWORD)(((float)fMoneyFactor)*nMoney);
}


DWORD 	MODI_Aformula::ComputeKeyExp( DWORD nTotalScore , float  fExact , float fExpFactor )
{
	if( fExact < 0.0f )
		return 0;

	// 计算获得经验
	float fAddExpFactor = pow( float(nTotalScore) ,fKeySanDianSanFenZhiYi );
	float tfAddExpFactor = 0.8f * fAddExpFactor;
	DWORD nAddExp = (DWORD)(tfAddExpFactor* fExact);

	nAddExp = (DWORD)(fExpFactor * (float)nAddExp);
	return nAddExp;
}
		
DWORD 	MODI_Aformula::ComputeKeyMoney( DWORD nExp , float fExact , float fMoneyFactor )
{
	DWORD nMoney = 0;
	float fTemp = 0.0f;
	fTemp = pow(float(nExp) , fSanFenZhiYi);
	if( fTemp > fFloatZero && fExact > 0.0f )
	{
		float fDivisor = 0.1f;
		if( fExact <= 0.9f )
		{
			fDivisor = 1.0f - fExact;
		}
		
		if( fDivisor > 0.0f )
		{
			fTemp = fTemp / fDivisor;
			fTemp += 0.5f;
			if( fTemp > fFloatZero )
			{
				nMoney = (DWORD)fTemp;
			}
		}
	}

	return (DWORD)(((float)fMoneyFactor)*nMoney);
}

