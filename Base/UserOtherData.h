/**
 * @file   UserOtherData.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 13 16:29:46 2011
 * 
 * @brief  zoneservice角色零散属性
 * 
 */

#ifndef _MD_USEROTHERDATA_H
#define _MD_USEROTHERDATA_H

//// 零散用户数据类型
enum enUserOtherDataType
{
	TYPE_USER_BLOCKCOUNT = 1,///被拉黑次数
	TYPE_COUNT_MANAGER = 2,  ///计数器
	
	TYPE_END = 255,
};


/// 一些角色的杂项数据
struct MODI_UserOtherData
{
    MODI_UserOtherData()
    {
        m_wdBlockCount = 0;
    }
	
    /// 被拉黑次数
    WORD m_wdBlockCount;
	
}__attribute__((__packed__));

#endif
