/** 
 * @file HelpFuns.h
 * @brief 一些帮助函数
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-05
 */

#ifndef MODI_HELPFUNS_H_
#define MODI_HELPFUNS_H_

#include "BaseOS.h"

extern "C" bool safe_strtoull(const char *str, unsigned long long *out);
extern "C" bool safe_strtoll(const char *str, long long *out); 
extern "C" bool safe_strtoul(const char *str, unsigned int *out); 
extern "C" bool safe_strtol(const char *str, long *out); 

extern "C" char	Value2Ascii(char in);
extern "C" char Ascii2Value(char in);
extern "C" bool	Binary2String(const char* pIn,unsigned int InLength,char* pOut);
extern "C" bool DBStr2Binary(const char* pIn,unsigned int InLength,char* pOut,unsigned int OutLimit,unsigned int & OutLength);

// 随机概率
extern "C" bool RandomBingo(DWORD nPro,DWORD nMax = 1000000);

extern "C" bool SplitStr( const char * szIn , unsigned int nInSize , const char szSplitChar , char ** pOut , unsigned int & nOutSize );

extern "C" void Tolower( const char * in , unsigned int in_len , char * out );


extern "C" char * safe_strncpy(char *dst, const char *src, size_t dst_size);

#endif
