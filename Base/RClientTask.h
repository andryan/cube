/**
 * @file   RClientTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Oct 28 15:14:03 2010
 * @version $Id:$
 * @brief  重连客户端
 * 
 */


#ifndef _MDRCLIENTTASK_H
#define _MDRCLIENTTASK_H

#include "Type.h"
#include "Thread.h"
#include "Socket.h"
#include "Timer.h"

/**
 * @brief 客户端连接
 *
 * 要连接服务器就主动产生
 *
 */
class MODI_RClientTask: public MODI_DisableCopy, public MODI_Thread
{
 public:
	/**
	 * @brief 构造
	 *
	 * @param client_name 客户端的名字
	 * @param server_ip 服务器的IP
	 * @param port 服务器的端口
	 *
	 */
	MODI_RClientTask(const char * client_name, const char * server_ip, const WORD & port): MODI_Thread(client_name), m_stSocket(-1, NULL),
		m_strServerIP(server_ip), m_wdPort(port)
	{
		m_blConnected = false;
		m_blTermiate = true;
		m_blCanRelease = false;
		m_pMySelf = this;
	}

	virtual ~MODI_RClientTask()
	{
		TEMP_FAILURE_RETRY(::close(m_iEpollfd));
		TEMP_FAILURE_RETRY(::close(m_iReadEpollfd));
		m_stSocket.Reset();
		m_pMySelf = NULL;
	}
	
	/**
	 * @brief 获取服务器IP
	 *
	 * @return 服务器的IP
	 *
	 */ 
	const char * GetServiceIP() const
	{
		return m_strServerIP.c_str();
	}

	void SetServiceIP(const char * ip)
	{
		m_strServerIP = ip;
	}

	void SetServicePort(const WORD & port)
	{
		m_wdPort = port;
	}

	/**
	 * @brief 获取服务器的端口
	 *
	 * @return 服务器的端口
	 *
	 */ 
	const WORD & GetServicePort()
	{
		return m_wdPort;
	}

	/// 主循环
	void Run();

	/// 初始化
	virtual bool Init();

	/**
	 * @brief 发送命令
	 *
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 要发送命令的大小
	 * @param is_buffer 是否要队列发送(默认不要)
	 *
	 * @return 发送成功true,发送失败false
	 *
	 */
   	bool SendCmdInConnect(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size, bool is_buffer = true)
	{
		m_stSocket.SendCmd(pt_null_cmd, cmd_size, is_buffer);
		return true;
	}

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * p_null_cmd, const int cmd_len) = 0;
	virtual bool RecvDataNoPoll();
	bool SendDataNoPoll();

	/** 
	 * @brief 获取自己的IP
	 * 
	 * 
	 * @return 返回ip
	 */
	const char * GetIP()
	{
		return m_strClientIP.c_str();
	}

	/** 
	 * @brief 获取自己的地址
	 * 
	 * @return 返回端口
	 */
	WORD & GetPort()
	{
		return m_wdClientPort;
	}

	bool IsTerminate()
	{
		return m_blTermiate;
	}

	void Terminate(bool bl_terminate)
	{
		if(bl_terminate == true)
		{
			m_blCanRelease = true;
		}
		m_blTermiate = bl_terminate;
	}

	/**
	 * @brief 	查看连接是否还存在
	 *
	 * @return	如果连接还存在返回TRUE
	 *
	 */
	virtual const bool IsConnected();
	void SetConnect(bool set_vlaue);
	bool Connect();

 protected:
	/// 套接口
	MODI_Socket m_stSocket;
	
	/// 释放连接资源
	virtual void Final()
	{
		m_stSocket.Reset();
	}

	/// 时间
	MODI_RTime m_stRTime;

	MODI_RClientTask * m_pMySelf;
	void ReadyReConnect();
 private:	
	void ConnectSuccess();
	volatile bool m_blConnected;
	MODI_Mutex m_stLock;
	bool m_blTermiate;
	bool m_blCanRelease;
	
	/// 服务器IP
	std::string m_strServerIP;

	/// 服务器端口
	WORD m_wdPort;

	std::string m_strClientIP;
	WORD m_wdClientPort;

	/// epoll句柄
	int m_iEpollfd;
	/// 事件
	std::vector<struct epoll_event > m_stEventVec;
	int m_iReadEpollfd;
	std::vector<struct epoll_event > m_stReadEventVec;
};

#endif
