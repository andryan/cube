/**
 * @file   NormalSched.h
 * @author hurixin <hurixin@modi.com>
 * @date   Mon Feb 22 14:40:58 2010
 * @version Ver0.1
 * @brief  正常调度
 * 
 */

#ifndef _MDNORMALSCHED_H
#define _MDNORMALSCHED_H

#include "ThreadGroup.h"
#include "PollThread.h"

class MODI_ServiceTaskSched;


class MODI_NormalThread: public MODI_PollThread
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sched 所属调度器
	 * @param name 线程名字
	 *
	 */
	MODI_NormalThread(MODI_ServiceTaskSched * sched, const char * name):
		MODI_PollThread(name), m_pSched(sched)
	{
		
	}

		
	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true,失败false
	 */
	bool Init();

		
	/** 
	 * @brief 结束
	 * 
	 */
	void Final();

	
	/** 
	 * @brief 主循环
	 * 
	 */
	void Run();

	/** 
	 * @brief 删除某个连接
	 * 
	 * @param p_task 要删除的连接
	 */
   	void RemoveTask(MODI_ServiceTask * p_task);
	
 protected:
	/** 
 	 * @brief 连接注册到列表
 	 * 
 	 * @param p_task 要注册的task
 	 */
	void AddTaskToList(MODI_ServiceTask * p_task);
	
 private:
	/// 是那个调度器
	MODI_ServiceTaskSched * m_pSched;
};


/**
 * @brief 正常调度管理
 * 
 */
class MODI_NormalSched
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param shced 那个调度器
	 * @param thread_num 线程数量
	 */
	MODI_NormalSched(MODI_ServiceTaskSched * sched, const WORD thread_num):
		m_pSched(sched), m_stThreadGroup(thread_num)
	{
		
	}

	/** 
	 * @brief 增加一个新的连接
	 * 
	 * @param p_task 要增加的连接
	 * 
	 */
   	bool AddTask(MODI_ServiceTask * p_task)
	{
		WORD i = 0;
		MODI_NormalThread * p_min_thread = (MODI_NormalThread *)m_stThreadGroup[i++];
		MODI_NormalThread * p_thread = NULL;
		for(; i<m_stThreadGroup.GetCount(); i++)
		{
			p_thread = (MODI_NormalThread *)m_stThreadGroup[i];
			if(! p_thread)
			{
				continue;
			}
			if(p_min_thread->Size() > p_thread->Size())
			{
				p_min_thread = p_thread;
				p_thread = NULL;
			}
		}
		p_task->GetNextState();
	  	p_min_thread->AddTask(p_task);
		return true;
	}

	
	/** 
	 * @brief 初始化
	 * 
	 * 
	 * @return 成功true,失败false
	 */
	bool Init();

	
	/** 
	 * @brief 结束
	 * 
	 */
	void Final()
	{
		m_stThreadGroup.DelAllThread();
	}


	/** 
	 * @brief 获取连接数的大小
	 * 
	 * 
	 * @return 连接数目
	 */
	int Size()
	{
		struct MODI_GetSize: public MODI_ThreadCallBack
		{
			MODI_GetSize()
			{
				size = 0;
			}
			
			int size;

			bool Exec(MODI_Thread * p_thread)
			{
				size += ((MODI_NormalThread *)p_thread)->Size();
				return true;
			}
		};

		MODI_GetSize getsize_callback;
		m_stThreadGroup.ExecAllThread(getsize_callback);
		return getsize_callback.size;
	}
	
 private:
	/// 归属那个调度器
	MODI_ServiceTaskSched * m_pSched;

	/// 线程组
	MODI_ThreadGroup m_stThreadGroup;
};

#endif
