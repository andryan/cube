//////////////////////////////////////////////////////////////////////////
//
#ifndef __MIC_LOADER_H__
#define __MIC_LOADER_H__

#include "AudioCommon.h"

#include "Song.h"

// standard midi tags
const unsigned long MThd = 0x4D546864ul;
const unsigned long MTrk = 0x4D54726Bul;
// xf tags
const unsigned long XFIH = 0x58464948ul;
const unsigned long XFKM = 0x58464B4Dul;

// different standard midi formats
#define VERSION_MULTICHANNEL   0
#define VERSION_SINGLECHANNEL  1
#define VERSION_MULTISONG      2

// different compression 
#define MIDI_COMPRESSION_NONE     0
#define MIDI_COMPRESSION_MEDIUM   1
#define MIDI_COMPRESSION_FULL     2

// option bit masks
#define OPTION_NOCONTROLS   1    // no control details but general information
#define OPTION_NOEVENTS     2    // no track events at all
#define OPTION_NOMETAEVENTS 4    // no meta details but general information
#define OPTION_NOSYSEVENTS  8    // no sysex details but general information
#define OPTION_NONOTEEVENTS 16   // no note events (8x or 9x)
#define OPTION_NOPOLYEVENTS 32   // no poly aftertouch events (Ax)
#define OPTION_NOCONTROLEVENTS 64 // no control events at all (Bx)
#define OPTION_NOPROGRAMEVENTS 128 // no program change events (Cx)
#define OPTION_NOAFTERTOUCHEVENTS 256 // no aftertouch events (Dx)
#define OPTION_NOPITCHBENDEVENTS 512 // no pitchbend events (Ex)
#define OPTION_NOREALTIMEEVENTS  1024 // no realtime events (Fx)

// getchannel delivers a valid channel or:
#define NOCHANNEL     (-1)
#define MULTICHANNEL  (-2)
#define VALIDCHANNEL(ch)  ((ch) >= 0 && (ch) <= 15)
#define gm_drumchannel 9
#define SAYCHANNEL(ch)  ((ch) + 1) // 0..15 in midi format but spoken 1..16!

// for use of param what in function text()
#define meta_seqnumber  0
#define meta_text       1
#define meta_copyright  2
#define meta_trackname  3
#define meta_instrument 4
#define meta_lyric	5
#define meta_marker	6
#define meta_cuepoint	7
#define meta_prefixchannel  0x20
#define meta_prefixport  0x21
#define meta_endtrack   0x2f
#define meta_tempo      0x51
#define meta_smpte      0x54
#define meta_meter      0x58
#define meta_key        0x59

#define ctrl_highbank 			0
#define ctrl_wheel 			1
#define ctrl_breath 			2
#define ctrl_foot 			4
#define ctrl_portamentotime 		5
#define ctrl_data 			6
#define ctrl_volume 			7
#define ctrl_balance 			10
#define ctrl_expression 		11
#define ctrl_effect1  			12
#define ctrl_effect2  			13
#define ctrl_slider1  			16
#define ctrl_slider2  			17
#define ctrl_slider3  			18
#define ctrl_slider4  			19
#define ctrl_lowbank 			32
#define ctrl_hold 			64
#define ctrl_portamento 		65
#define ctrl_sustenuto 			66
#define ctrl_soft 			67
#define ctrl_legato 			68
#define ctrl_hold2 			69
#define ctrl_xg_brightness 		74
#define ctrl_xg_portamento 		84
#define ctrl_reverb 			91
#define ctrl_chorus 			93
#define ctrl_xg_effect4 		94
#define ctrl_datainc 			96
#define ctrl_datadec 			97
#define ctrl_lowrpn  			100
#define ctrl_highrpn 			101
#define ctrl_allsoundoff 		120
#define ctrl_allcontroloff 		121
#define ctrl_localkeyboard 		122
#define ctrl_allnotesoff 		123
#define ctrl_omnioff   			124
#define ctrl_omnion    			125
#define ctrl_mono      			126
#define ctrl_poly      			127

#define event_noteoff                   0x80
#define event_noteon                    0x90
#define event_polyaftertouch            0xa0
#define event_controlchange             0xb0
#define event_programchange             0xc0
#define event_aftertouch                0xd0
#define event_pitchbend                 0xe0
#define event_sysex                     0xf0
#define event_songpos                   0xf2
#define event_songselect                0xf3
#define event_tunerequest               0xf6
#define event_endsysex                  0xf7
#define event_clock                     0xf8
#define event_start                     0xfa
#define event_continue                  0xfb
#define event_stop                      0xfc
#define event_activesense               0xfe
#define event_meta                      0xff

const static int MAX_TRACKS = 256;

class MODI_MIDILoader
{
public:
	MODI_MIDILoader();
	~MODI_MIDILoader();

	// 传递SONG的指针
	void  SetSong(MODI_Song *pSong){ m_pSong = pSong; }

    //
    bool LoadFromMemoryToBuffer(const char* memory);
	// 将MIDI文件的轨迹数据转载进内存
	bool  LoadFromMidiFileToBuffer(const string &fileName);
	
	// 解析MIDI事件
	bool  ProcessMIDIEvent();

	MODI_Song * GetSong() { return m_pSong; }
	unsigned int GetRawDataSize() const { return m_nRawDataSize; }
	const char * GetRawData() const { return m_pRawData; }

private:
	// 解析变长字符串
	unsigned long ReadVarLen(unsigned char **data);

	// 处理音符打开事件， 创建一个新的音符， 并将音符的起始时间以及音调写入音符数据结构
	void  OnNoteOn();

	// 处理音符关闭事件， 将音符的终止时间写入音符的数据结构， 并加入到音符链表中
	void  OnNoteOff();

	// 处理歌词事件， 创建一个新的单个歌词，将歌词的起始时间和终止时间以及歌词的字符写入歌词数据结构， 并加入到歌词链表中
	void  OnLyric( int nLen );

	// 处理速度改变事件
	void  OnTempoChange();
	
	// 拍子改变事件
	void OnMeterChange();

	// 处理音轨结束事件
	void  OnTrkEnd();



private:
	unsigned m_nBeatsPerPara;
	double m_dQuaNotePerBeat;

	int m_nNumTracks;

	unsigned char *m_TrkBuf[MAX_TRACKS];              // 保存指向轨迹数据区指针的数组
	unsigned long m_TrkLen[MAX_TRACKS];              // 保存轨迹数据区长度的数组

	unsigned long  m_dwElapsedTicks;

	unsigned char *m_pPos;

	unsigned char m_ucEvent;

	double  m_dDivision;
	unsigned m_dwTempo;
	unsigned m_dwLastTempo;
	unsigned long  m_dwBeatTime;

	bool   m_bNoteOn;
	bool   m_bNewChar;

	MODI_Note *m_pCurNote;
	MODI_Sentence *m_pCurSentence;

	MODI_Song *m_pSong;

	int    m_nNumNotes;
	int    m_nNumLyrics;
	int    m_nNumSentence;
	int    m_nNumTempos;

	double m_dElapsedTime;

	unsigned int	m_nRawDataSize; 
	char * m_pRawData;
};

#endif











