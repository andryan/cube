/**
 * @file RecordQueue.h
 * @date 2009-12-11 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 记录池
 *
 */

#ifndef _MDRECORDQUEUE_H
#define _MDRECORDQUEUE_H

#include "AssertEx.h"
#include <queue>
#include "Global.h"
#include "Mutex.h"

class MODI_Record;


typedef std::pair<volatile bool, MODI_Record *  > defRecordQueue;

const DWORD g_dwRecordSize = 2048;

class MODI_RecordQueue
{
 public:
	MODI_RecordQueue()
	{
		m_dwReadAddr = 0;
		m_dwWriteAddr = 0;
		m_dwWriteSize = 0;
	}


	~MODI_RecordQueue()
	{
		Final();
	}
	
	bool Put(MODI_Record * p_record)
	{
		m_stMutex.lock();
		if(m_dwWriteSize > g_dwRecordSize)
		{
			Global::logger->fatal("m_dwWriteSize=%u", m_dwWriteSize);
			MODI_ASSERT(0);
		}
		m_dwWriteSize++;
		if((MoveRecordToQueue()) && (!RecordQueue[m_dwWriteAddr].first))
		{
			RecordQueue[m_dwWriteAddr].second = p_record;
			RecordQueue[m_dwWriteAddr].first = true;
			m_dwWriteAddr = (++m_dwWriteAddr) % g_dwRecordSize;
			m_stMutex.unlock();
			return true;
		}
		else
		{
			RecordDeque.push(p_record);
			m_stMutex.unlock();
			return true;
		}
		m_stMutex.unlock();
		return false;
	}

	MODI_Record * Get()
	{
		m_stMutex.lock();
		MODI_Record * p_record = NULL;
		if(RecordQueue[m_dwReadAddr].first)
		{
			p_record = RecordQueue[m_dwReadAddr].second;
			RecordQueue[m_dwReadAddr].first = false;
			m_dwReadAddr = (++m_dwReadAddr) % g_dwRecordSize;
			if(m_dwWriteSize >= 0)
				m_dwWriteSize--;
			else
			{
				MODI_ASSERT(0);
			}
		}
		m_stMutex.unlock();
		return p_record;
	}

	const DWORD GetSize() 
	{
		DWORD size = 0;
		m_stMutex.lock();
		size = m_dwWriteSize;
		m_stMutex.unlock();
		return size;
	}

 private:
	bool MoveRecordToQueue()
	{
		bool ret_value = true;
		while(! RecordDeque.empty())
		{
			if(RecordQueue[m_dwWriteAddr].first)
			{
				ret_value = false;
				break;
			}
			RecordQueue[m_dwWriteAddr].second = RecordDeque.front();
			RecordQueue[m_dwWriteAddr].first = true;
			m_dwWriteAddr = (++m_dwWriteAddr) % g_dwRecordSize;
			RecordDeque.pop();
		}
		return ret_value;
	}

	/// 释放资源
	void Final()
	{

	}
	
 private:
	defRecordQueue RecordQueue[g_dwRecordSize];

	std::queue<MODI_Record *, std::deque<MODI_Record *> > RecordDeque;

	unsigned int m_dwReadAddr;
	unsigned int m_dwWriteAddr;
	unsigned int m_dwWriteSize;

	MODI_Mutex m_stMutex;
};

#endif
