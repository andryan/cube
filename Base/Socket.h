/**
 * @file Socket.h
 * @date 2009-12-17 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 套接口封装
 *
 */

#ifndef _MDSOCKET_H
#define _MDSOCKET_H

#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <errno.h>
#include <zlib.h>

#include "Global.h"
#include "Buffer.h"
#include "AssertEx.h"
#include "Encrypt.h"

namespace Skt
{
	/// 是否压缩
   	const unsigned int ZIP_FLAG = 0x01000000;

	/// 是否rc5加密
	const unsigned int RC5_FLAG = 0x02000000;

	/// 等待写的时间毫秒
	const int WAIT_WRITE_TIME = 5000;

	/// 等待读的时间毫秒
	const int WAIT_READ_TIME = 2000;
};


/**
 * @brief 套接口封装
 *
 */
class MODI_Socket: public MODI_DisableCopy
{
 public:
	MODI_Socket(const int s_sock, const struct sockaddr_in * s_addr = NULL);

	~MODI_Socket();

	/**
	 * @brief 获取对方的IPn
	 * @return 字符形式IP
	 *
	 */ 
	inline const char * GetIP() const
	{
		return inet_ntoa(m_stAddr.sin_addr);
	}

	/**
	 * @brief 获取对方的IP
	 * @return 本机字节顺序数字IP
	 *
	 */ 
	inline const DWORD GetIPNum() const
	{
		return ntohl(m_stAddr.sin_addr.s_addr);
	}

	/**
	 * @brief 获取对方的port
	 *
	 * @return 本机字节顺序的端口返回
	 *
	 */ 
	inline const WORD GetPort() const 
	{
		return ntohs(m_stAddr.sin_port);
	}

	/** 
	 * @brief 获取系统分配的socket标识
	 * 
	 * 
	 * @return 
	 */
	inline const int GetSock()
	{
		return m_stSock;
	}
	
	/**
	 * @brief 获取服务器本地的端口
	 *
	 * @return 本机字节顺序的端口返回
	 */ 
	inline const WORD GetServicePort() const
	{
		return ntohs(m_stServiceAddr.sin_port);
	}

	/** 
	 * @brief 初始化socket
	 * 
	 * @param s_sock 系统提供的sock的标识
	 * @param src_addr socket连接的地址
	 *
	 */
	void Init(const int s_sock, const struct sockaddr_in * src_addr);

	/** 
	 * @brief socket复位，断开连接，但不释放实例
	 * 
	 */
	void Reset();

	/** 
	 * @brief 发送命令，应用层直接调用
	 * 
	 * @param pt_cmd 要发送的命令
	 * @param cmd_len 命令的长度
	 * @param is_buffer 是否缓冲发送(非阻塞模式)
	 * @param is_zip 是否压缩和加密
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const Cmd::stNullCmd * pt_cmd, const int cmd_len, bool is_buffer = false, bool is_zip = false);
	

	/** 
	 * @brief 不要组包直接发生命令(流的形式),应用层直接调用
	 * 
	 * @param pt_cmd 要发送的命令
	 * @param cmd_len 发送命令的长度
	 * @param is_buffer 是否缓冲发送
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmdNoPacket(const void * pt_cmd, const int cmd_len, bool is_buffer = false);

	/** 
	 * @brief 设置发送缓冲buffer的大小
	 * 
	 * @param size 发送缓冲的大小
	 *
	 */
	inline void SetSendTempBufferSize(const unsigned int size)
	{
		m_stSendTempBuffer.ResizeBuffer(size);
	}

	/** 
	 * @brief 设置发送buffer的大小
	 * 
	 * @param size 发送缓冲的大小
	 *
	 */
	inline void SetSendBufferSize(const unsigned int size)
	{
		m_stSendBuffer.ResizeBuffer(size);
	}

	/** 
	 * @brief 设置接收buffer的大小
	 * 
	 * @param size 接收缓冲的大小
	 *
	 */
	inline void SetRecvBufferSize(const unsigned int size)
	{
		m_stRecvBuffer.ResizeBuffer(size);
	}

	/** 
	 * @brief 设置加密类型
	 * 
	 * @param type 加密的类型
	 */
	inline void SetEncryptType(MODI_Encrypt::enEncryptType type)
	{
		m_stEncrypt.SetEncryptType(type);

	}

	/** 
	 * @brief 发送缓冲里面的数据，已经poll过，可以直接发送
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendQueueCmdNoPoll();

	/** 
	 * @brief 发送缓冲里面的数据，还没poll，发送之前需查看
	 * 
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendQueueCmd();

	/** 
	 * @brief 接收数据，没有poll，接收之前查询是否有数据可以接收
	 * 
	 * @return 成功true,失败false
	 *
	 */
	const int RecvData();

	/** 
	 * @brief 接收数据，已经poll,直接接收
	 * 
	 * @return 接收的数据字节数据
	 *
	 */
	const int RecvDataNoPoll();

	/**
	 * @brief 从数据队列里面取出命令
	 *
	 * @param pt_cmd 命令存放地址,所以此内存为一个包的最大长度
	 * @return 命令长度
	 *
	 */ 
	const DWORD GetCmdFromBuffer(void * pt_cmd);

	/**
	 * @brief 注册到epoll
	 * 
	 * @param epoll_fd 注册到的epoll句柄
	 * @param evts 要注册的epoll事件
	 * @param p_data 要注册的对象指针
	 *
	 */
	void AddEpoll(int epoll_fd, uint32_t evts, void *p_data);

	/**
	 * @brief 删除epoll事件
	 * @param epoll_fd 要从此epoll删除
	 * @param evts 要删除的事件 
	 *
	 */ 
	void DelEpoll(int epoll_fd, uint32_t evts);

	/** 
	 * @brief 是否有命令
	 * 
	 * @return 命令大小
	 *
	 */
	const WORD IsHaveCommand();
	
 private:
	/** 
	 * @brief 组包
	 * 
	 * @param pt_cmd 要组包的数据
	 * @param cmd_size 组包的长度
	 * @param send_queue 结果存放处
	 * @param iszip 是否压缩和加密
	 * 
	 * @return 组包成功的包的长度
	 *
	 */
	const WORD MakePacket(const void * pt_cmd, const int cmd_size, char * send_queue, bool iszip = false);


	/** 
	 * @brief 发送缓冲的数据，直到发送完或者发生错误
	 * 
	 * @param pt_cmd 发生的数据
	 * @param cmd_size 发送数据的大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendQueueData(const void * pt_cmd, const int cmd_size);


	/** 
	 * @brief 调用send发送数据
	 * 
	 * @param pt_cmd 发送数据
	 * @param cmd_size 数据的大小
	 * 
	 * @return 发送数据的字节
	 *
	 */
	const int SendData(const void * pt_cmd, const int cmd_size);


	/** 
	 * @brief 发送缓冲的数据，主循环已经poll好了，可以直接发生数据
	 * 
	 * @param pt_cmd 发送的数据
	 * @param cmd_size 数据的大小
	 * 
	 * @return 发送的数据大小
	 *
	 */
	const int SendDataNoPoll(const void * pt_cmd, const int cmd_size);

	/**
	 * @brief 是否可以发送(阻塞式)
	 * @return =-1失败
	 *
	 */ 
	inline int WaitForWrite()
	{
		struct pollfd poll_fd;
		
		poll_fd.fd = m_stSock;
		poll_fd.events = (POLLOUT | POLLERR | POLLPRI);
		poll_fd.revents = 0;

		int ret_value = TEMP_FAILURE_RETRY(::poll(&poll_fd, 1, Skt::WAIT_WRITE_TIME));
		if((ret_value > 0) && ((poll_fd.revents & POLLOUT) == 0))
		{
			ret_value = -1;
		}

		return ret_value;
		
	}

	/**
	 * @brief 是否可读
	 * @return 1有数据,0没有数据 -1错误
	 *
	 */ 
	inline int WaitForRead()
	{
		struct pollfd poll_fd;

		poll_fd.fd = m_stSock;
		poll_fd.events = POLLIN | POLLPRI | POLLERR;
		poll_fd.revents = 0;

		int ret_code = TEMP_FAILURE_RETRY(::poll(&poll_fd, 1, Skt::WAIT_READ_TIME));

		if((ret_code > 0) && ((poll_fd.revents & POLLIN) == 0))
		{
			ret_code = -1;
		}
		return ret_code;
	}

	
	/**
	 * @brief 设置一个功能标志
	 *
	 * @param set_flag 要设置的标志位
	 *
	 */
	inline void SetFlag(const DWORD set_flag)
	{
		m_dwFlag |= set_flag;
	}

	
	/**
	 * @brief 是否设置了一个标志
	 * @param set_flag 此标志是否设置
	 * @return 设置了true, 没有设置false
	 *
	 */
	inline bool IsSet(const DWORD set_flag)
	{
		return (m_dwFlag & set_flag);
	}

	/**
	 * @brief 清除某个标志
	 *
	 * @param clear_flag 要清除某个标志
	 *
	 */ 
	inline void ClearFlag(const DWORD clear_flag)
	{
		m_dwFlag &= ~clear_flag;
	}

	/**
	 * @brief 接收到数据后更新接收缓冲
	 * 
	 * @param ret_value 已经接收到的数据长度
	 *
	 * @return 成功true,失败false
	 *
	 */ 
	inline bool UpDataRecvBuffer(int & ret_value)
	{
		m_stRecvBuffer.WriteSuccess(ret_value); 
		m_dwRecvSize += ret_value;
		if(! m_stRecvBuffer.ResizeBuffer(Skt::MAX_SOCKETSIZE))
		{
			Global::net_logger->fatal("[buffer_error] socket recv buffer error(%d)", ret_value);
			return false;
		}
		return true;
	}

	/// 设置成非阻塞模式
	bool SetNonBlock();

	/// 是否加密
	inline bool IsEncrypt()
	{
		return m_stEncrypt.GetEncryptType();
	}

#ifdef _BUFFER_DEBUG	
	/** 
	 * @brief 给所有的buffer一个名字,方便调试
	 * 
	 */
	void SetSocketName()
	{
		std::ostringstream name;
		
		name << GetIP() << "_" <<GetPort() << "_send_buffer";
		m_stSendBuffer.SetBufferName(name.str().c_str());
		name.str("");

		name << GetIP() << "_" <<GetPort() << "_send_temp_buffer";
		m_stSendTempBuffer.SetBufferName(name.str().c_str());
		name.str("");

		name << GetIP() << "_" <<GetPort() << "_recv_buffer";
		m_stRecvBuffer.SetBufferName(name.str().c_str());
	}
#endif

 private:
	/// 套接口
	int m_stSock;

	/// 套接口地址
	struct sockaddr_in m_stAddr;

	/// 本机服务器地址
	struct sockaddr_in m_stServiceAddr;
	
	/// 系统锁
	MODI_Mutex m_stMutex;

	/// 套接口标志
	DWORD m_dwFlag;

	/// 接收到多少数据
	DWORD m_dwRecvSize;

	/// 加密
	MODI_Encrypt m_stEncrypt;

	/// 接收缓冲区
	MODI_ByteBuffer m_stRecvBuffer;

	/// 发送二级缓存
	MODI_ByteBuffer m_stSendTempBuffer;

	/// 发送一级缓存
	MODI_ByteBuffer m_stSendBuffer;

	/// 加密队列
	MODI_ByteBuffer m_stZipBuffer;

	/// 解压队列
	MODI_ByteBuffer m_stUnzipBuffer;
};

#endif
