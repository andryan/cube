#include "AssertEx.h"
#include "Logger.h"
#include "Global.h"

namespace MODIExceptions
{

void AssertOut( const char * szFile , unsigned int nLine , const char * szAssertion )
{
	Global::logger->fatal("[ASSERTION FAILDED], FileName = %s , Line = %u , Assertion = %s .", szFile , nLine , szAssertion );
}

void AssertExceptions( const char * szFile , unsigned int nLine , const char * szFunction )
{
	Global::logger->fatal("[ASSERTION EXCEPTIONS], FileName = %s , Line = %u , Function = %s .", szFile , nLine , szFunction );
}

void Exceptions( const char * szFile , unsigned int nLine , const char * szFunction , const char * szExceptionDes )
{
	Global::logger->fatal("[CATCH EXCEPTIONS], FileName = %s , Line = %u , Function = %s , Assertion = %s .", szFile , nLine , szFunction , szExceptionDes );
}

}
