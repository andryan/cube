/**
 * @file   ClientPollThread.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Oct 12 15:38:21 2010
 * @version $Id:$
 * @brief  连接调度线程
 * 
 * 
 */

#ifndef _MDCLIENTPOLLTHREAD_H
#define _MDCLIENTPOLLTHREAD_H

#include <list>
#include <queue>
#include <deque>

#include "Global.h"
#include "Thread.h"
#include "PClientTask.h"

/**
 * @brief 轮询线程
 * 
 */
class MODI_ClientPollThread: public MODI_Thread
{
 public:
	virtual ~MODI_ClientPollThread(){}

	/** 
	 * @brief 构造
	 * 
	 * @param name 线程名称
	 * 
	 */
	MODI_ClientPollThread(const char * name): MODI_Thread(name)
	{
		m_dwTaskCount = 0;
	}

	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true,失败false
	 */
	virtual bool Init();

	
	/** 
	 * @brief 获取连接数
	 * 
	 * @return 连接数 
	 */
	int Size()
	{
		return m_dwTaskCount;
	}

	/** 
	 * @brief 增加一个连接到队列
	 * 
	 * @param p_task 要增加的连接
	 */
	virtual void AddTask(MODI_PClientTask * p_task)
	{
		m_stRWLock.wrlock();
		m_TaskQueue.push(p_task);
		m_stRWLock.unlock();
	}

	/** 
	 * @brief 释放资源
	 * 
	 */
	virtual void Final() = 0;

	/** 
	 * @brief 删除某个连接
	 * 
	 * @param p_task 要删除的连接
	 */
	virtual void RemoveTask(MODI_PClientTask * p_task) = 0;

 protected:
	/** 
	 * @brief 从队列里面获取任务到列表里
	 * 
	 */
	void GetTaskFromQueue();

	/** 
	 * @brief 连接注册到列表
	 * 
	 * @param p_task 要注册的task
	 */
	virtual void AddTaskToList(MODI_PClientTask * p_task) = 0;
	
	/// epoll 句柄
	int m_iEpollfd;

	/// epoll事件队列
	std::vector<struct epoll_event > m_EpollEventVec;

	/// 事件队列
	std::list< MODI_PClientTask * > m_TaskList;

	/// 事件缓冲队列
	std::queue<MODI_PClientTask *, std::deque<MODI_PClientTask * > > m_TaskQueue;

	DWORD m_dwTaskCount;

	/// 队列锁
	MODI_RWLock m_stRWLock;

	/// 时间
	MODI_RTime m_stCurrentTime;
};

#endif

