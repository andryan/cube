/** 
 * @file DefaultAvatarCreator.h
 * @brief 默认的AVATAR形象创建者
 * @author Tang Teng
 * @version v.01
 * @date 2010-04-21
 */
#ifndef DEFAULT_AVATAR_H_
#define DEFAULT_AVATAR_H_

#include "gamestructdef.h"
#include <vector>

class 	MODI_DefaultAvatarCreator
{
	public :

		MODI_DefaultAvatarCreator();
		~MODI_DefaultAvatarCreator();

		bool 	Init();
		void 	Shutdown();

	public:

		bool 	CreaterDefaultAvatar( WORD nDefaultIdx , MODI_ItemInfoAdd * pItems , WORD & nCount );
		bool 	IsValidDefaultAvatarIdx( WORD nDefaultIdx );


	private:

		struct tagDefaultAvatar
		{
			MODI_ItemInfoAdd  	Items[MAX_BAG_AVATAR_COUNT];
			WORD 				nCount;

			tagDefaultAvatar():nCount(0)
			{

			}
		};

		typedef std::map<WORD,tagDefaultAvatar *> 	 			DEFAULT_AVATAR_LIST;
		typedef DEFAULT_AVATAR_LIST::iterator 		 			DEFAULT_AVATAR_LIST_ITER;

		DEFAULT_AVATAR_LIST 		m_list;
};



#endif
