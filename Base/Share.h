/**
 * @file PublicFun.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 一些共享的函数定义 
 *
 */

#ifndef _MDPUBLICFUN_H
#define _MDPUBLICFUN_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <ext/hash_map>
#include <execinfo.h>

#include "Global.h"
#include "VarType.h"
#include "Buffer.h"
#include "SplitString.h"
#include "Encrypt.h"
#include "gamestructdef.h"

/// 定义些函数
class PublicFun
{
 public:
	/**
	 * @brief 空的系列化结构
	 * 
	 */
	struct MODI_SerialStruct
	{
		MODI_SerialStruct()
		{
			m_wdSize = 0;
		}
		WORD m_wdSize;
		char m_pData[0];
	}__attribute__((__packed__));
	

	/** 
	 * @brief 参数解析
	 * 
	 * @param argc 个数
	 * @param argv 参数
	 * 
	 */
	static void ArgParse(const int argc, char ** argv);

	
	/** 
	 * @brief 把字符串IP变成数字IP(本机字节)
	 * 
	 * @param str_ip 
	 * 
	 * @return 本机字节的网络地址
	 */
	static unsigned int StrIP2NumIP(const char * str_ip)
	{
		struct in_addr out_addr;
	   	inet_aton(str_ip, &out_addr);
		return ntohl(out_addr.s_addr);
	}
	

	/** 
	 * @brief 把数字(网络字节)IP变成字符串IP
	 * 
	 * @param num_ip 本机字节的目标IP
	 * 
	 * @return 字符串的IP
	 */
	static const char * NumIP2StrIP(const unsigned int & num_ip)
	{
		struct in_addr input_addr;
		input_addr.s_addr = htonl(num_ip);
		return inet_ntoa(input_addr);
	}

//	/** 
//	 * @brief 解码 SESSION ID转换成 IP 
//	 * 
//	 * @param id 会话ID
//	 * 
//	 * @return 字符串的IP
//	 */
//	static const char * DecodeIPBySessionID( const defSessionID & id );
//
//	/** 
//	 * @brief 
//	 * 
//	 * @param id
//	 * 
//	 * @return 	
//	 */
//	static unsigned short DecodePortBySessionID( const defSessionID & id );
//
//	/** 
//	 * @brief 解码SESSION ID，输出
//	 * 
//	 * @param id
//	 * 
//	 * @return 	
//	 */
//	static void  DecodeAddressBySessionID( const defSessionID & id , std::string & strAddress );
	

	/** 
	 * @brief 系列化vector
	 * 
	 * @param vec 要系列化的vector
	 * 
	 * @return 系列化后的变量
	 */
	template< typename var_type >
	static void SerialVec(MODI_VarType & rt_var, const std::vector<var_type> & vec)
	{
		MODI_ByteBuffer buffer(vec.size() * sizeof(var_type) + 64);
		MODI_SerialStruct serial_struct;
	 	typename std::vector<var_type>::const_iterator iter = vec.begin();
		for(; iter!= vec.end(); iter++)
		{
			MODI_VarType temp_var(*iter);
			serial_struct.m_wdSize = temp_var.Size();
			buffer.Write((const unsigned char *)&serial_struct.m_wdSize, sizeof(serial_struct.m_wdSize));
			buffer.Write((const unsigned char *)((const char *)temp_var), serial_struct.m_wdSize);
		}
		rt_var.Clear();
		rt_var.Put(buffer.ReadBuf(), buffer.ReadSize());
	}
	

	/** 
	 * @brief 反系列化vector
	 * 
	 * @param vec 返回的vector
	 * @param MODI_VarType 要系列化的变量
	 */
	template< typename var_type >
	static void UnserialVec(std::vector<var_type> & vec, const MODI_VarType & input_var)
	{
		vec.clear();
		const char * var = (const char *)input_var;
		MODI_SerialStruct  * serial_struct = NULL;
		DWORD size = 0;
		while(size < input_var.Size())
		{
			serial_struct = (MODI_SerialStruct *)&var[size];
			MODI_VarType temp_var;
		  	temp_var.Put(serial_struct->m_pData, serial_struct->m_wdSize);
		   	vec.push_back((var_type)temp_var);
			size += sizeof(MODI_SerialStruct)+serial_struct->m_wdSize;
		}
	}
	

	/** 
	 * @brief 反系列化vector,模板特化
	 * 
	 * @param vec 返回的vector
	 * @param MODI_VarType 要系列化的变量
	 */
	static void UnserialVec(std::vector<std::string > & vec, const MODI_VarType & input_var)
	{
		vec.clear();
		const char * var = (const char *)input_var;
		MODI_SerialStruct  * serial_struct = NULL;
		DWORD size = 0;
		while(size < input_var.Size())
		{
			serial_struct = (MODI_SerialStruct *)&var[size];
			MODI_VarType temp_var;
			temp_var.Put(serial_struct->m_pData, serial_struct->m_wdSize);
			std::string str_var = (const char *)temp_var;
		   	vec.push_back(str_var);
			size += sizeof(MODI_SerialStruct)+serial_struct->m_wdSize;
		}
	}

	/** 
	 * @brief 系列化map
	 * 
	 * @param var_map 要系列化的map
	 * 
	 * @return 系列化后的变量
	 */
	template< typename key_type,typename var_type >
	static void SerialMap(MODI_VarType & rt_var, const std::map<key_type, var_type> & var_map)
	{
		MODI_ByteBuffer buffer(var_map.size() * (sizeof(key_type) + sizeof(var_type)) + 64);
	 	typename std::map<key_type, var_type>::const_iterator iter = var_map.begin();
		for(; iter!= var_map.end(); iter++)
		{
			MODI_SerialStruct serial_struct_key;
			MODI_VarType key_var;
			key_var.Put(iter->first);
			serial_struct_key.m_wdSize = key_var.Size();
			buffer.Write((const unsigned char *)&serial_struct_key.m_wdSize, sizeof(serial_struct_key.m_wdSize));
			buffer.Write((const unsigned char *)((const char *)key_var), key_var.Size());

			MODI_SerialStruct serial_struct_data;
			MODI_VarType data_var;
			data_var.Put(((const char *)&(iter->second)), sizeof(var_type));
			serial_struct_data.m_wdSize = data_var.Size();
			buffer.Write((const unsigned char *)&serial_struct_data.m_wdSize, sizeof(serial_struct_data.m_wdSize));
			buffer.Write((const unsigned char *)((const char *)data_var), data_var.Size());
		}
		rt_var.Clear();
		rt_var.Put(buffer.ReadBuf(), buffer.ReadSize());
	}

	/** 
	 * @brief un系列化map
	 * 
	 * @param var_map 要系列化的map
	 * 
	 * @return 系列化后的变量
	 */
	template< typename key_type,typename var_type >
	static void UnSerialMap(std::map<key_type, var_type> & var_map, const MODI_VarType & input_var)
	{
		var_map.clear();
		const char * var = (const char *)input_var;
		DWORD size = 0;
		while(size < input_var.Size())
		{
			MODI_SerialStruct  * serial_struct_key = NULL;
			serial_struct_key = (MODI_SerialStruct *)&var[size];
			MODI_VarType key_var;
		  	key_var.Put(serial_struct_key->m_pData);//, serial_struct_key->m_wdSize);
			size += sizeof(MODI_SerialStruct)+serial_struct_key->m_wdSize;

			MODI_SerialStruct  * serial_struct_data = NULL;
			serial_struct_data = (MODI_SerialStruct *)&var[size];
			MODI_VarType var_data;
			var_data.Put(serial_struct_data->m_pData, serial_struct_data->m_wdSize);
			size += sizeof(MODI_SerialStruct)+serial_struct_data->m_wdSize;
			var_type * real_var_data = (var_type *)((const char *)var_data);
			var_map.insert(std::pair<key_type, var_type>((key_type)key_var, *real_var_data));
		}
	}
	

	/** 
	 * @brief 随机数,包括min和max
	 * 
	 * @param min 下限
	 * @param max 上限
	 * 
	 * @return 随机数
	 */
	static int GetRandNum(int min, int max)
	{
		int ret_code = min;
		if(min == max)
		{
			return min;
		}
		
		else if (max > min)
		{
			ret_code = min + (int) ( rand_r(&Global::g_dwRandNum) % max );
			//ret_code = min + (int) (((double) max - (double)min + 1.0) * rand_r(&Global::g_dwRandNum) / (RAND_MAX + 1.0));
			
		}
		else
		{
			ret_code = max + (int) ( rand_r(&Global::g_dwRandNum) % min );
		}
		
#ifdef _DEBUG		
		Global::logger->debug("[get_rand]---->>>>>>>>>>>>>>>><min=%d,max=%d,ret=%d>",min,max,ret_code);
#endif		
		return ret_code;
	}

	/**
	 * @brief 查看堆栈调用情况
	 *
	 *
	 */
	static void BackTrace()
	{
		return;
		void * trace_buffer[100] = {0};
		char ** trace_buffer_symbol;
		
		int rt_code = 0;
		rt_code = backtrace(trace_buffer, 100);
		if(rt_code)
		{
			trace_buffer_symbol = backtrace_symbols(trace_buffer, rt_code);
			for(int i = 0; i<rt_code; i++)
			{
				Global::logger->debug("[back_trace] trace[%d]-->%s", i, trace_buffer_symbol[i]);
			}
			free(trace_buffer_symbol);
		}
	}


	static char Str2Bin(char *str);
	static char HexToChar(unsigned char n);
	static char CharToHex(char n);
	static const char * StrEncode(const char *str_line);
	static const char * StrDecode(const char *str_line);
	
	/** 
	 * @brief 钱加密
	 * 
	 */
	static void EncryptMoney(char * buf, DWORD & out_size, const DWORD in_money, const char * acc_name);


	/** 
	 * @brief 转换成邮件附件
	 * 
	 */
	static bool StrConvToGoodsPackage( MODI_GoodsPackageArray & goods , const char * szContent , unsigned int nContentLen );
	
};


/**
 * @brief 时间相关的函数
 * 
 */
class MODI_TimeFun
{
 public:
	/** 
	 * @brief 某天变成秒
	 * 
	 * @param day 某天
	 * 
	 * @return 秒数
	 *
	 */
	static const DWORD DayToSec(const char * day);

};

#endif
