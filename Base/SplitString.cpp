/**
 * @file SplitString.cpp
 * @date 2010-01-27 CST
 * @verison $Id $
 * @author hurixin hurixin@modi.com
 * @brief 分割字符
 *
 */

#include "SplitString.h"

/**
 * @brief 字符分割
 * @param string_container 输出的容器
 * @param in_string 输入的字符
 *
 */
void MODI_SplitString::StringSplit(std::vector<std::string> & string_container, const char * in_string, const char * split)
{
	const std::string split_string = in_string;
	const std::string::size_type string_len = split_string.length();
	std::string::size_type begin_pos = 0;
	
	while(begin_pos < string_len)
	{
		begin_pos = split_string.find_first_not_of (split, begin_pos);
		if (begin_pos == std::string::npos)
		{
			return;
		}
		
		std::string::size_type end_pos = split_string.find_first_of(split, begin_pos);
		if (end_pos == std::string::npos)
		{
			string_container.push_back(split_string.substr(begin_pos));
			return;
		}
		else
		{
			string_container.push_back(split_string.substr(begin_pos, end_pos-begin_pos));
		}
		begin_pos = end_pos + 1;
	}
}


/**
 * @brief 初始化
 * @param split_string 要处理的字符
 *
 */
bool MODI_SplitString::Init(const char * split_string, const char * split)
{
	m_SplitMap.clear();
	std::vector<std::string > str_vec;
	StringSplit(str_vec, split_string, split);
	for(std::vector<std::string>::const_iterator iter = str_vec.begin(); iter != str_vec.end(); iter++)
	{
		std::string::size_type begin_pos = (*iter).find_first_of("=", 0);
		if(begin_pos != std::string::npos)
		{
			std::string key = (*iter).substr(0, begin_pos);
			std::string value = (*iter).substr(begin_pos+1);
			m_SplitMap[key] = value;
		}
	}
	return true;
}


/**
 * @brief 初始化
 * @param split_string 要处理的字符,去除掉""
 *
 */
bool MODI_SplitString::InitXML(const char * split_string, const char * split)
{
	m_SplitMap.clear();
	std::vector<std::string > str_vec;
	StringSplit(str_vec, split_string, split);
	for(std::vector<std::string>::const_iterator iter = str_vec.begin(); iter != str_vec.end(); iter++)
	{
		std::string::size_type begin_pos = (*iter).find_first_of("=", 0);
		if(begin_pos != std::string::npos)
		{
			std::string key = (*iter).substr(0, begin_pos);
			std::string value = (*iter).substr(begin_pos+1);
			if(value.size() > 2)
			{
				std::string real_value = value.substr(1, value.size() - 2);
				m_SplitMap[key] = real_value;
			}
			else
			{
				m_SplitMap[key] = value;
			}
			
		}
	}
	return true;
}
