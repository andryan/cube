#include "Base/BitArray.h"


const unsigned int MODI_BitArray::PER_BIT_BYTES = 8;
const unsigned int MODI_BitArray::PER_CELL_SIZE = sizeof(unsigned int) * 8;

void	MODI_BitArray::OnInit(unsigned int dwSize )
{
	if(!dwSize)
		dwSize += PER_CELL_SIZE;

	m_dwBitsNum = dwSize;
	m_dwLenght = GetIndex(dwSize - 1) + 1;

//	m_pBuffers = (unsigned int *) Memory::MemoryPool::fastPool::GetInstancePtr()->Malloc( m_dwLenght ); 
	m_pBuffers = (unsigned int *) malloc( m_dwLenght );
	
	MODI_ASSERT( m_pBuffers != 0 );
	Clear();
}

//	强制最后位以后的所有位值为0
void	MODI_BitArray::Trim()
{
	unsigned int dwOffset = GetOffSetBits(m_dwBitsNum);
	if(m_dwLenght > 0 && dwOffset > 0)
	{
		//	高前低后
		m_pBuffers[m_dwLenght - 1] &= ~(~(static_cast<unsigned int>(0)) << dwOffset);
	}
}

void	MODI_BitArray::Reset()
{
	if(m_pBuffers )
	{
//		Memory::MemoryPool::fastPool::GetInstancePtr()->Free( m_pBuffers , m_dwLenght ); 
		free( m_pBuffers );
	}

	m_dwBitsNum = 0;
	m_dwLenght = 0;
	m_pBuffers = 0;
}

MODI_BitArray::MODI_BitArray()
{
	m_dwBitsNum = 0;
	m_dwLenght = 0;
	m_pBuffers = 0;
}

MODI_BitArray::MODI_BitArray(unsigned int dwSize)
{
	m_dwBitsNum = 0;
	m_dwLenght = 0;
	m_pBuffers = 0;
	OnInit(dwSize);
	Trim();
}

MODI_BitArray::~MODI_BitArray()
{
	Reset();
}

void MODI_BitArray::Create( unsigned int nSize )
{
	if( !m_pBuffers )
	{
		OnInit( nSize );
		Trim();
	}
}

void 	MODI_BitArray::CreateFromBuf( const char * src_buf , unsigned int src_byte_size )
{
	if( !m_pBuffers )
	{
		OnInit( src_byte_size * PER_BIT_BYTES );
		Trim();
		memcpy( m_pBuffers , src_buf , src_byte_size );
	}
}

bool 	MODI_BitArray::CopyTo( char * det_buf , unsigned int & des_byte_size )
{
	if( !m_pBuffers )
		return false;

	if( des_byte_size > m_dwLenght )
		des_byte_size = m_dwLenght;

	memcpy( det_buf , m_pBuffers , des_byte_size );
	return true;
}

MODI_BitArray::MODI_BitArray(const MODI_BitArray & BitArray)
{
	m_pBuffers = 0;		//	先把指针初始化
	*this = BitArray;
}

bool	MODI_BitArray::operator == (const MODI_BitArray & BitArray) const
{
	if(m_dwBitsNum != BitArray.m_dwBitsNum)
		return false;

	for(unsigned int i = 0; i < m_dwLenght; i++)
	{
		if(m_pBuffers[i] != BitArray.m_pBuffers[i])
			return false;
	}
	return true;
}

MODI_BitArray &	MODI_BitArray::operator = (const MODI_BitArray & BitArray)
{
	if(this != &BitArray)
	{
		Reset();
		OnInit(BitArray.m_dwBitsNum);

		memcpy(m_pBuffers,BitArray.m_pBuffers,sizeof(unsigned int) * m_dwLenght);
	}

	return *this;
}

bool	MODI_BitArray::operator != (const MODI_BitArray & BitArray) const
{
	return !(*this == BitArray);
}

void	MODI_BitArray::Clear()
{ 
	memset(m_pBuffers,0,sizeof(unsigned int) * m_dwLenght);
}

MODI_BitArray::MODI_BitArrayProxy::MODI_BitArrayProxy(MODI_BitArray & BitArray,unsigned int dwPos):m_BitArray(BitArray),m_dwPos(dwPos)
{

}


