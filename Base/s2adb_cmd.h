#ifndef MODI_STOACCOUNT_DB_DEFINED_H_
#define MODI_STOACCOUNT_DB_DEFINED_H_

#ifdef _MSC_VER
#pragma once
#endif

#include <string>
#include <list>
#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "Type.h"

using std::list;
using std::string;

#pragma pack(push,1)

static const unsigned char MAINCMD_S2ADB = 0x12;

struct MODI_S2ADB_Cmd : public stNullCmd
{
	MODI_S2ADB_Cmd()
	{
		byCmd = MAINCMD_S2ADB;
	}
};

/// account db 返回登入结果
struct MODI_ADB2S_Notify_LoginResult : public MODI_S2ADB_Cmd 
{
	// 具体的结果
	struct MODI_Result
	{
		char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
		defAccountID 	m_nAccountID; // 帐号ID
		MODI_LoginKey 	m_Key; // 发给客户端的KEY

		MODI_Result()
		{
			m_nAccountID = INVAILD_ACCOUNT_ID;
			memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		}
	};
		
	MODI_SessionID 	m_SessionID;
	unsigned short 	m_nResult; // 结果,来自SvrResult枚举
	unsigned int 	m_nSize; // 如果登入成功，m_nSize的大小为sizeof(MODI_Result)
	char 			m_pData[0]; // 如果登入成功，则m_pData的实际内容为MODI_Result
	
	MODI_ADB2S_Notify_LoginResult()
    {
		byParam = ms_SubCmd;
		m_nResult = 0xFF;
		m_nSize = 0;
    }

	static const unsigned char ms_SubCmd = 1;
};

/// 服务器请求ADB验证登入请求
struct MODI_S2ADB_Request_LoginIn : public MODI_S2ADB_Cmd
{
	MODI_SessionID m_SessionID;
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; // 帐号
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1]; // 密码
	
	MODI_S2ADB_Request_LoginIn()
	{
		byParam = ms_SubCmd;
		memset( m_szAccountName , 0 , sizeof(m_szAccountName) );
		memset( m_szAccountPwd , 0 , sizeof(m_szAccountPwd) );
	}

	static const unsigned char ms_SubCmd = 2;
};


/// 客户端请求登入游戏服务器给的验证
struct MODI_S2ADB_Request_LoginAuth : public MODI_S2ADB_Cmd
{
	MODI_SessionID 		m_nSessionID;
	MODI_LoginPassport 	m_Passport;
	BYTE 				m_nServerID;


	MODI_S2ADB_Request_LoginAuth()
	{
		byParam = ms_SubCmd;
		m_nServerID = 0;
	}	

	static const unsigned char ms_SubCmd = 4;
};

/// 登入游戏服务器验证的结果返回
struct MODI_ADB2S_Notify_LoginAuthResult : public MODI_S2ADB_Cmd
{
	unsigned char 	m_byResult;
	MODI_SessionID  m_nSessionID;
	MODI_CHARID   	m_nCharID;
	defAccountID  	m_nAccountID;
	BYTE 			m_nServerID;
	bool 			m_bFangChengMi; // 是否需要防沉迷

	MODI_ADB2S_Notify_LoginAuthResult()
	{
		byParam = ms_SubCmd;
		m_byResult = 0;
		m_nCharID = INVAILD_CHARID;
		m_nAccountID =INVAILD_ACCOUNT_ID;
		m_nServerID = 0;
		m_bFangChengMi = false;
	}


	static const unsigned char ms_SubCmd = 5;
};

/// 请求ADB重新生成某个帐号的PP
struct MODI_S2ADB_Request_ReMakePP : public MODI_S2ADB_Cmd
{
	defAccountID 	m_nAccountID;
	MODI_SessionID 	m_nSessionID;
	BYTE 			m_nServerID;

	MODI_S2ADB_Request_ReMakePP()
	{
		byParam = ms_SubCmd;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		m_nServerID = 0;
	}

	static const unsigned char ms_SubCmd = 7;
};

/// 重新生成PP的结果
struct MODI_ADB2S_Notify_ReMakePPResult : public MODI_S2ADB_Cmd
{
	WORD 			m_bySuccessful;
	defAccountID 	m_nAccountID;
	MODI_SessionID 	m_nSessionID;
	MODI_LoginKey 	m_Key;
	BYTE 			m_nServerID;

	MODI_ADB2S_Notify_ReMakePPResult()
	{
		byParam = ms_SubCmd;
		m_bySuccessful = 0;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		m_nServerID = 0;
	}

	static const unsigned char ms_SubCmd = 8;
};

#pragma pack(pop)

#endif
