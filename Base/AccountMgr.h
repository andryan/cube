/** 
 * @file AccountMgr.h
 * @brief 帐号管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-02
 */
#ifndef ACCOUNT_MGR_H_
#define ACCOUNT_MGR_H_


#include <map>
#include "protocol/gamedefine.h"

template < typename V , typename K = defAccountID >
class 	MODI_AccountMgr
{
	public:

		MODI_AccountMgr()
		{

		}

		~MODI_AccountMgr()
		{
			this->Clear();
		}


	public:

		bool Add( K nAccountID , V * pValue )
		{
			MAP_ACCOUNTS_INSERT_RESULT insertRes = this->m_mapAccounts.insert( std::make_pair( nAccountID , pValue ) );
			return insertRes.second;
		}

		bool Del( K nAccountID , bool bDel = false )
		{
			MAP_ACCOUNTS_ITER itor = this->m_mapAccounts.find( nAccountID );
			if( itor != this->m_mapAccounts.end() )
			{
				if( bDel )
					delete itor->second;
				this->m_mapAccounts.erase( itor );
				return true;
			}
			return false;
		}

		bool IsIn( K nAccountID )
		{
			return this->Find( nAccountID ) ? true : false;
		}

		const V * Find( K nAccountID )
		{
			const V * pRet = 0;
			MAP_ACCOUNTS_ITER itor = this->m_mapAccounts.find( nAccountID );
			if( itor != this->m_mapAccounts.end() )
			{
				pRet = itor->second;
			}
			return pRet;
		}

		size_t Size()
		{
			return this->m_mapAccounts.size();
		}

		void Clear()
		{
			MAP_ACCOUNTS_ITER itor = this->m_mapAccounts.begin();
			while( itor != this->m_mapAccounts.end() )
			{
				V * pTemp = itor->second;
				delete pTemp;
				itor++;
			}
			this->m_mapAccounts.clear();
		}
		

	private:

		typedef typename std::map<K,V *>			MAP_ACCOUNTS;
		typedef typename MAP_ACCOUNTS::iterator 			MAP_ACCOUNTS_ITER;
		typedef typename MAP_ACCOUNTS::const_iterator 		MAP_ACCOUNTS_C_ITER;
		typedef typename std::pair<MAP_ACCOUNTS_ITER,bool>  MAP_ACCOUNTS_INSERT_RESULT;

		MAP_ACCOUNTS 			m_mapAccounts;
};






#endif

