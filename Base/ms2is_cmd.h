/**
 * @file   ms2is_cmd.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 16:25:13 2011
 * 
 * @brief  managerservice to infoservice 命令定义
 * 
 * 
 */

#ifndef _MD_MSISCMD_H
#define _MD_MSISCMD_H

#include "protocol/gamedefine.h"

/// 账号相关命令
const BYTE MSTOIS_ACCOUNT_CMD = 1;

/// 沉迷相关命令
const BYTE MSTOIS_CHENMI_CMD = 2;

/// 注册命令
const BYTE MSTOIS_REGISTER_CMD = 3;


/// 账号命令
struct MODI_MS2IS_AccountCmd: public stNullCmd
{
	MODI_MS2IS_AccountCmd()
	{
		byCmd = MSTOIS_ACCOUNT_CMD;
	}
}__attribute__((packed));


/// 沉迷相关
struct MODI_MS2IS_ChenMiCmd: public stNullCmd
{
	MODI_MS2IS_ChenMiCmd()
	{
		byCmd = MSTOIS_CHENMI_CMD;
	}
}__attribute__((packed));


///////////登陆相关账号命令开始///////////////////

/// 查询此账号是否激活
struct MODI_MS2IS_AccountIsExit_Cmd: public MODI_MS2IS_AccountCmd
{
	MODI_MS2IS_AccountIsExit_Cmd()
	{
		byParam = ms_SubCmd;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
	/// 账号名
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	
	static const BYTE ms_SubCmd = 1;
}__attribute__((packed));


/// 返回是否激活
struct MODI_IS2MS_RetIsExit_Cmd: public MODI_MS2IS_AccountCmd
{
	MODI_IS2MS_RetIsExit_Cmd()
	{
		byParam = ms_SubCmd;
		m_byResult = 0;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
	/// 0未激活,1激活
	BYTE m_byResult;
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	
	static const BYTE ms_SubCmd = 2;
}__attribute__((packed));


/// 获取此账号的相关信息
struct MODI_MS2IS_GetAccInfo_Cmd: public MODI_MS2IS_AccountCmd
{
	MODI_MS2IS_GetAccInfo_Cmd()
	{
		byParam = ms_SubCmd;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
	/// 账号名
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	
	static const BYTE ms_SubCmd = 3;
}__attribute__((packed));


/// 返回账号信息
struct MODI_IS2MS_RetAccInfo_Cmd: public MODI_MS2IS_AccountCmd
{
	MODI_IS2MS_RetAccInfo_Cmd()
	{
		byParam = ms_SubCmd;
		m_byIsChenMi = 0;
		m_dwAccID = INVAILD_ACCOUNT_ID;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
	/// 是否防沉迷,1=沉迷,0=不沉迷
	BYTE m_byIsChenMi;
	/// accid 0=没有此账号
	defAccountID m_dwAccID;
	/// 账号
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	
	static const BYTE ms_SubCmd = 4;
}__attribute__((packed));


////////////////沉迷命令开始///////////////

/// 告知沉迷时间
struct MODI_MS2IS_ChenMiTime_Cmd: public MODI_MS2IS_ChenMiCmd
{
	MODI_MS2IS_ChenMiTime_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
		m_dwChenMiTime = 0;
	}
	
	/// 沉迷accid
	defAccountID m_dwAccID;
	/// 沉迷的时间
	DWORD m_dwChenMiTime;
	static const BYTE ms_SubCmd = 1;
}__attribute__((packed));


/// 返回沉迷结果
struct MODI_IS2MS_ChenMiResult_Cmd: public MODI_MS2IS_ChenMiCmd
{
	MODI_IS2MS_ChenMiResult_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
		m_enResult = enChenMi_None;
	}
	/// 沉迷ACCID
	defAccountID m_dwAccID;
	///沉迷等级
	enChenMiResult m_enResult;
	
	static const BYTE ms_SubCmd = 2;
}__attribute__((packed));


/// 用户登录
struct MODI_MS2IS_Onlogin_Cmd: public MODI_MS2IS_ChenMiCmd
{
	MODI_MS2IS_Onlogin_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
	}
	/// 登陆ACCID
	defAccountID m_dwAccID;
	
	static const BYTE ms_SubCmd = 3;
}__attribute__((packed));


/// 用户登出
struct MODI_MS2IS_Onlogout_Cmd: public MODI_MS2IS_ChenMiCmd
{
	MODI_MS2IS_Onlogout_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccID = INVAILD_ACCOUNT_ID;
	}
	/// 登出的ACCID
	defAccountID m_dwAccID;
	
	static const BYTE ms_SubCmd = 4;
}__attribute__((packed));


#endif
