/**
 * @file Buffer.h
 * @date 2009-12-09 CST
 * @version $Id $
 * @author hurixin hurixing@modi.com
 * @brief 动态数组实现 
 *
 */

#ifndef _MDBYTEBUFFER_H
#define _MDBYTEBUFFER_H

#include "BaseOS.h"
#include "Global.h"
#include <unistd.h>

/**
 * @brief 动态数组实现
 *
 */
class MODI_ByteBuffer
{
 public:
	MODI_ByteBuffer(const unsigned int buffer_length = (Skt::MAX_SOCKETSIZE * 2)):
		m_dwAddLength(buffer_length), m_dwMaxLength(buffer_length * 200), m_dwBufLength(buffer_length)
	{
		m_dwWriteAddr = 0;
		m_dwReadAddr = 0;
		m_DataVec.resize(m_dwBufLength , 0 );
	}
		
	~MODI_ByteBuffer()
	{
		m_DataVec.clear();
	}
	
	/**
	 * @brief 获取读地址
	 *
	 * @return 读地址
	 *
	 */ 
	inline const unsigned int ReadAddr()
	{
		return m_dwReadAddr;
	}
	
#ifdef _BUFFER_DEBUG
	/** 
	 * @brief 获取buffer的名字
	 * 
	 * @return 返回buffer的名字
	 *
	 */
	const char * GetBufferName() const 
	{
		return m_strBufferName.c_str();
	}

	/** 
	 * @brief 设置buffer的名字
	 * 
	 * @param name 要设置的 名字
	 */
	void SetBufferName(const char * name)
	{
		m_strBufferName = name;
	}
#endif
	
	/**
	 * @brief 获取写地址
	 *
	 * @return 写地址
	 *
	 */ 
	inline const unsigned int WriteAddr()
	{
		return m_dwWriteAddr;
	}

	/**
	 * @brief 缓存大小
	 *
	 * @return buffer总大小
	 *
	 */
	inline const unsigned int Length()
	{
		return m_dwBufLength;
	}

	/**
	 * @brief 获取写指针
	 *
	 * @return 写指针
	 *
	 */
	inline unsigned char * WriteBuf()
	{
		return &m_DataVec[m_dwWriteAddr];
	}

	/**
	 * @brief 可写大小
	 *
	 * @return 可写大小
	 *
	 */
	inline unsigned int WriteSize()
	{
		return (m_dwBufLength - m_dwWriteAddr);
	}

	/// 调整buffer大小
	inline bool ResizeBuffer(const unsigned int size);
	
	/**
	 * @brief 写数据
	 *
	 * @param write_data 要写的数据
	 * @param write_len 写的长度
	 *
	 * @return 成功true,失败 false
	 *
	 */
	inline bool Write(const unsigned char * write_data, const unsigned int write_len)
	{

		if( (write_len == 0) || (write_data == NULL))
		{
#ifdef _BUFFER_DEBUG
			Global::logger->fatal("[%s] buffer(%s) write size(=%d) more than MAX_USERDATASIZE", "BUFFER_DEBUG",m_strBufferName.c_str(),write_len);
#endif			
			return false;
		}
		if(ResizeBuffer(write_len))
		{
			memcpy(&m_DataVec[m_dwWriteAddr], write_data, write_len);
			m_dwWriteAddr += write_len;
			return true;
		}
		return false;
	}

	/**
	 * @brief 更新写地址
	 *
	 * 直接copy到内存里面,则需要调用此函数更新写地址
	 * 
	 * @param size 已经写入数据的长度
	 *
	 * @return 成功true,失败false
	 *
	 */ 
	inline bool WriteSuccess(const unsigned int size)
	{
		if((m_dwWriteAddr + size) > m_dwBufLength)
		{
#ifdef _BUFFER_DEBUG
			Global::logger->fatal("[%s] buffer(%s) writesuccess (size=%d)>MAX_USERDATASIZE or m_dwWriteAddr(%d)+size>m_dwBufLength(%d)",
								  "BUFFER_DEBUG", m_strBufferName.c_str(),size, m_dwWriteAddr, m_dwBufLength);
#endif			
			return false;
		}
		m_dwWriteAddr += size;
		return true;
	}

	/**
	 * @brief 询问buffer是否有数据可读
	 *
	 * @return 可读true,不可读false
	 *
	 */ 
	inline bool IsCanRead()
	{
	   	if((m_dwWriteAddr - m_dwReadAddr) > 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * @brief 询问数据可读长度
	 *
	 * @return 数据可读的长度
	 *
	 */
	inline unsigned int ReadSize()
	{
		return (m_dwWriteAddr - m_dwReadAddr);
	}

	/**
	 * @brief 获取读指针
	 *
	 * @return 读指针
	 *
	 */ 
	inline unsigned char * ReadBuf()
	{
		return &m_DataVec[m_dwReadAddr];
	}
	
	/**
	 * @brief 读完数据后需要整理,不然内存泄漏
	 *
	 * @param size 成功读取的数据大小
	 *
	 */ 
	inline void ReadSuccess(const unsigned int size)
	{
		m_dwReadAddr += size;
		if(m_dwWriteAddr > m_dwReadAddr)	
		{
			unsigned int tmp = m_dwWriteAddr - m_dwReadAddr;
			if(m_dwReadAddr >= tmp)
			{
				memmove(&m_DataVec[0], &m_DataVec[m_dwReadAddr], tmp);
				m_dwReadAddr = 0;
				m_dwWriteAddr = tmp;
			}
		}
		else
		{
		   	m_dwWriteAddr = 0;
		  	m_dwReadAddr = 0;
		}
	}

	/**
	 * @brief buffer的读写指针复位
	 *
	 */ 
	inline void Reset()
	{
		m_dwWriteAddr = 0;
		m_dwReadAddr = 0;
		m_DataVec.clear();
	}

 private:
	/// 增长单位
	unsigned int m_dwAddLength;
	/// 最大长度
	const unsigned int m_dwMaxLength;
	/// 写地址
	unsigned int m_dwReadAddr;
	/// 读地址
	unsigned int m_dwWriteAddr;
	/// buffer长度
	unsigned int m_dwBufLength;
	
#ifdef _BUFFER_DEBUG	
	/// buffer名字
	std::string m_strBufferName;
#endif
	
	/// 数据类型
	std::vector<unsigned char > m_DataVec;
};



/**
 * @brief 重新调整buffer大小
 *
 * @param size 要调整的大小
 *
 * @return 成功true,失败false
 *
 */
inline bool MODI_ByteBuffer::ResizeBuffer(unsigned int size)
{
	if(m_dwBufLength < m_dwWriteAddr)
	{
#ifdef _BUFFER_DEBUG
		if(m_strBufferName.size() > 0)
			Global::logger->fatal("[%s] buffer(%s) resize check failed(m_dwBufLength=%d<m_dwWriteAddr)",
								  "BUFFER_DEBUG", m_strBufferName.c_str());
#endif
		Global::logger->fatal("[buffer_resize] writeaddr more than buflength <m_dwbuflength=%u,m_dwwriteaddr=%u>", m_dwBufLength, m_dwWriteAddr);
		return false;
	}
	
	if(m_dwBufLength + size > m_dwMaxLength)
	{
#ifdef _BUFFER_DEBUG
		if(m_strBufferName.size() > 0)
			Global::logger->fatal("[%s] buffer(%s) resize (size=%u) more than (m_dwMaxLength=%u)(m_dwBufLength=%u)"
								  "BUFFER_DEBUG", m_strBufferName.c_str(), size, m_dwMaxLength, m_dwBufLength);
#endif
		Global::logger->fatal("[buffer_resize] buflength add size more than maxlength <maxlength=%u,buflength=%u,addsize=%u,size=%u", m_dwMaxLength, m_dwBufLength, m_dwAddLength, size);
		return false;
	}
	
	if((size + 8) > WriteSize())
	{
		if(size < Skt::MAX_SOCKETSIZE)
			size = Skt::MAX_SOCKETSIZE;
		m_dwBufLength += size;
		m_DataVec.resize(m_dwBufLength);
#ifdef _BUFFER_DEBUG
		Global::logger->info("[%s] buffer(%s) resize to %d", "BUFFER_DEBUG", m_strBufferName.c_str(), m_dwBufLength);
#endif
	}
	
	return true;
}


#endif
