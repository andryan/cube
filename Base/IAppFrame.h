/** 
 * @file IAppFrame.h 
 * @brief 服务器程序框架基础类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-20
 */

#ifndef IMODI_APPFRAME_H_
#define IMODI_APPFRAME_H_

#include "Logger.h"
#include "Share.h"
#include "BaseOS.h"
#include "ServerConfig.h"
#include "AssertEx.h"
#include "SingleObject.h"
#include "sessionid_creator.h"
#include "TimeManager.h"
#include <iostream>
using namespace std;

class 	MODI_Service;
class 	MODI_Thread;
class 	MODI_SessionIDCreator;

class 	MODI_IAppFrame : public CSingleObject<MODI_IAppFrame>
{
	public:

		enum 
		{
			enOK = 0, // OK
			enLoadConfigFaild ,   // 加载配置文件失败  
			enConfigInvaild, // 配置文件无效
			enInitPackageHandlerFaild, // 初始化数据包派发器失败
			enInitDBSystemFaild, // 初始化数据库系统失败（无法连接到数据库 )
			enNoMemory, // 无法分配内存
			enCannotConnectToServer, // 无法连接到对应的服务器
			enNotExistLogicThread, // 逻辑线程不存在
			enNotExistNetService, // 网络服务模块不存在
			enLoadResourceFaild, // 资源加载失败
			enInitTimeManagerFaild,
			enPidFileFaild,


		};


		explicit MODI_IAppFrame( const char * szAppName );

		virtual ~MODI_IAppFrame();

		/** 
		 * @brief 程序初始化
		 * 
		 * @return 成功返回 MODI_IAppFrame::enOK	
		 */
		virtual 	int Init();

		/** 
		 * @brief 程序运行
		 * 
		 * @return 成功返回 MODI_IAppFrame::enOK	
		 */
		virtual 	int Run() = 0;

		/** 
		 * @brief 程序关闭
		 * 
		 * @return 成功返回 MODI_IAppFrame::enOK	
		 */
		virtual 	int Shutdown() = 0;

		/** 
		 * @brief 处理用户定义的信号1
		 */
		virtual 	void 	OnSignal_User1(); 

		/** 
		 * @brief 处理用户定义的信号2
		 */
		virtual 	void 	OnSignal_User2();

		/** 
		 * @brief 处理SIGINT 终端中断信号
		 */
		virtual 	void 	OnSignal_Int();

		/** 
		 * @brief 处理SIGTERM 要求进行结束运行。KILL/关机 默认信号
		 */
		virtual 	void 	OnSignal_Term();

		virtual void Terminate();
		virtual void ReloadConfig();

		BYTE 	GetWorldID() const { return m_byWorldID; }
		BYTE 	GetServerID() const { return m_byServerID; }

	protected:

		void 		RunDaemonIfSet();

		void 	SetWorldID( BYTE n )
		{
			m_byWorldID = n;
		}

		void 	SetServerID( BYTE n )
		{
			m_byServerID = n;
		}

		//		bool 		SavePidFile();

		//void 		RemovePidFile();

	protected:

		MODI_Logger  *  	m_pLogger; 		// 日志对象
		std::string 		m_strAppName; 	// 应用程序的名字
		//char 		 		m_szPidFile[512];
		MODI_Service 	  * m_pNetService; 	// 网络服务对象
		MODI_Thread * 		m_pLogicThread; // 逻辑线程对象
		MODI_SessionIDCreator  m_SessionIDCreator;
		BYTE 				m_byWorldID;
		BYTE 				m_byServerID;
		MODI_TimeManager 	m_timeManager;
		static const char * ms_ConfigPath;
};

#endif
