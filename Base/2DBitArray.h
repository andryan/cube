/** 
 * @file 2DBitArray.h
 * @brief 2维位数组
 * @author Tang Teng
 * @version v0.1
 * @date 2011-01-05
 */
	
#ifndef TWO_D_BIT_ARRAY_H_
#define TWO_D_BIT_ARRAY_H_


#include "BitArray.h"

//	位级别的二维数组
class MODI_BitArray2D : private MODI_BitArray
{

public:

	MODI_BitArray2D(unsigned int dwWidth1,unsigned int dwWidth2);
	~MODI_BitArray2D();
	MODI_BitArray2D(const MODI_BitArray2D & Array);

public:

	class MODI_BitArray2DProxy
	{

	public:

		MODI_BitArray2DProxy(MODI_BitArray2D & BitArray,unsigned int dwPos):m_BitArray2D(BitArray),m_dwRowPos(dwPos)
		{

		}

		MODI_BitArrayProxy operator [](unsigned int dwPos)
		{
			return MODI_BitArrayProxy(m_BitArray2D,m_dwRowPos * m_BitArray2D.m_dwWidth + dwPos);
		}
	

	private:

		MODI_BitArray2D &		m_BitArray2D;
		unsigned int				m_dwRowPos;
	};

	MODI_BitArray2D &		operator = (const MODI_BitArray2D & Array)
	{
		MODI_BitArray::operator = (Array);
		m_dwWidth = Array.m_dwWidth;
		return * this;
	}

	bool				operator == (const MODI_BitArray2D & Array) const
	{
		if(m_dwWidth != Array.m_dwWidth)
			return false;
		return MODI_BitArray::operator == (Array);
	}

	bool				operator != (const MODI_BitArray2D & Array) const
	{	
		return !(*this == Array);
	}

	const MODI_BitArray2DProxy		operator [] (unsigned int dwIndex) const
	{
		return MODI_BitArray2DProxy(const_cast<MODI_BitArray2D &>(*this),dwIndex);
	}

	MODI_BitArray2DProxy 			operator [] (unsigned int dwIndex)
	{
		return MODI_BitArray2DProxy(*this,dwIndex);
	}

	MODI_BitArray2D & operator &=	(MODI_BitArray2D & Array)
	{
		MODI_BitArray::operator &= (Array);
		return *this;
	}

	MODI_BitArray2D & operator |= (MODI_BitArray2D & Array)
	{
		MODI_BitArray::operator |= (Array);
		return *this;
	}

	MODI_BitArray2D & operator ^= (MODI_BitArray2D & Array)
	{
		MODI_BitArray::operator ^= (Array);
		return *this;
	}

	MODI_BitArray2D operator ~() const
	{
		return MODI_BitArray2D(*this).ToggleBitAllBits();
	}

	friend	MODI_BitArray2D operator & (MODI_BitArray2D & BitArray1,MODI_BitArray2D & BitArray2)
	{
		return MODI_BitArray2D(BitArray1) &= BitArray2;
	}

	friend  MODI_BitArray2D operator | (MODI_BitArray2D & BitArray1,MODI_BitArray2D & BitArray2)
	{
		return MODI_BitArray2D(BitArray1) |= BitArray2;
	}

	friend	MODI_BitArray2D operator ^ (MODI_BitArray2D & BitArray1,MODI_BitArray2D & BitArray2)
	{
		return MODI_BitArray2D(BitArray1) ^= BitArray2;
	}


	void	Clear() { MODI_BitArray::Clear(); }

	bool	IsAllBitsFalse()	const
	{
		return MODI_BitArray::IsAllBitsFalse();
	}

	bool	IsBitSet(unsigned int dwRowPos,unsigned int dwColPos)	const
	{
		return MODI_BitArray::IsBitSet(dwRowPos * m_dwWidth + dwColPos);
	}

	void	Set(unsigned int dwRowPos,unsigned int dwColPos,bool bValue)
	{
		MODI_BitArray::Set(dwRowPos * m_dwWidth + dwColPos,bValue);
	}

	void	ClearBit(unsigned int dwRowPos,unsigned int dwColPos)
	{
		MODI_BitArray::ClearBit(dwRowPos * m_dwWidth + dwColPos);
	}

	void	SetBit(unsigned int dwRowPos,unsigned int dwColPos)
	{
		MODI_BitArray::SetBit(dwRowPos * m_dwWidth + dwColPos);
	}

	void	ToggleBit(unsigned int dwRowPos,unsigned int dwColPos)
	{
		MODI_BitArray::ToggleBit(dwRowPos * m_dwWidth + dwColPos);
	}

	MODI_BitArray2D &	ToggleBitAllBits()
	{
		MODI_BitArray::ToggleBitAllBits();
		return *this;
	}


private:

	unsigned int		m_dwWidth;

};



#endif

