#include "Base/GameConstant.h"
#include "Base/AssertEx.h"
#include <map>

namespace 	MODI_GameConstant
{
	struct MODI_ConstantValue
	{
		char const * name; 				// 	名
		char const * description;  		//  描述
		int 		attr; 				// 属性
		enConstantValueType vtype; 		// 数值类型

		union
		{
			DWORD nValue;
			float fValue;
			int   iValue;
		} cur_v,def_v; // 当前值，默认值
	};

	typedef std::map<std::string,MODI_ConstantValue *> 			MAP_CONSTANTVALUE;
	typedef MAP_CONSTANTVALUE::iterator 						MAP_CONSTANTVALUE_ITER;
	typedef MAP_CONSTANTVALUE::const_iterator 					MAP_CONSTANTVALUE_C_ITER;
	typedef std::pair<MAP_CONSTANTVALUE_ITER,bool> 				MAP_CONSTANTVALUE_INSERT_RES;

	static MAP_CONSTANTVALUE 							g_ConstantValuesMap;
	static MODI_ConstantValue * 						g_ConstantValues[kIdx_Count];

	void 	RegisterToContainer( DWORD nIdx , MODI_ConstantValue * p )
	{
		if( g_ConstantValues[nIdx] )
		{
			MODI_ASSERT(0);
			throw "reduplicate dup game constant value .";
		}

		// 初始化所有配置字段
		MAP_CONSTANTVALUE_INSERT_RES ires = g_ConstantValuesMap.insert(std::make_pair(p->name,p));
		if( ires.second == false )
		{
			MODI_ASSERT(0);
			throw "reduplicate dup game constant value .";
			delete p;
		}

		g_ConstantValues[nIdx] = p;
	}

	void 	Register_DWORD( DWORD nIdx ,const char * szName , const char * szDesc , int iAttr , DWORD nValue )
	{
		MODI_ConstantValue * p = new MODI_ConstantValue();
		p->name = szName;
		p->description = szDesc;
		p->attr = iAttr;
		p->def_v.nValue = nValue;
		p->cur_v = p->def_v;
		p->vtype = kCVT_DWORD;
		RegisterToContainer( nIdx , p );
	}

	void 	Register_Int( DWORD nIdx ,const char * szName , const char * szDesc , int iAttr ,int iValue )
	{
		MODI_ConstantValue * p = new MODI_ConstantValue();
		p->name = szName;
		p->description = szDesc;
		p->attr = iAttr;
		p->def_v.iValue = iValue;
		p->cur_v = p->def_v;
		p->vtype = kCVT_INT;
		RegisterToContainer( nIdx , p );
	}

	void 	Register_Float( DWORD nIdx ,const char * szName , const char * szDesc , int iAttr ,float fValue )
	{
		MODI_ConstantValue * p = new MODI_ConstantValue();
		p->name = szName;
		p->description = szDesc;
		p->attr = iAttr;
		p->def_v.fValue = fValue;
		p->cur_v = p->def_v;
		p->vtype = kCVT_FLOAT;
		RegisterToContainer( nIdx , p );
	}

	void 	Initial()
	{
		CleanUP();

		Register_DWORD( kIdx_EnterRareShopInterval , "rareshop_enter_interval" , "神秘商城进入间隔时间" ,
				kMask_DynmaicWrite | kMask_StaticWrite , 3 * 1000 );
		Register_DWORD( kIdx_EnterRareShopPro , "rareshop_enter_pro" , "神秘商城进入概率",
				kMask_DynmaicWrite | kMask_StaticWrite , 10000 ); 
		Register_DWORD( kIdx_CharacterSyncAttrInterval , "charsync_attr_interval" , "角色数据同步间隔时间",
				kMask_StaticWrite , 2 * 1000 );
		Register_DWORD( kIdx_CharacterDBSaveInterval , "char_dbsave_interval" , "角色数据包存间隔时间" ,
			   	kMask_StaticWrite , 60 * 1000 * 10);	
		Register_Float( kIdx_Showtime_Pro , "showtime_pro" , "show time 所需时间百分比" ,
				kMask_DynmaicWrite | kMask_StaticWrite , 0.75f );
		Register_Int( kIdx_FangChenmi_Off , "fangchenmi_off" , "防沉迷是否关闭" ,
				kMask_DynmaicWrite | kMask_StaticWrite , 0 );
		Register_Int( kIdx_OneLogin , "onelogin" , "第一次登陆是否送点赠券" ,
				kMask_DynmaicWrite | kMask_StaticWrite , 1 );
		Register_Int( kIdx_Leveluptoten , "leveluptoten" , "10级是否送礼包" ,
				kMask_DynmaicWrite | kMask_StaticWrite , 1 );
	}

	void 	CleanUP()
	{
		MAP_CONSTANTVALUE_ITER it = g_ConstantValuesMap.begin();
		while( it != g_ConstantValuesMap.end() )
		{
			MODI_ConstantValue * p = it->second;
			delete p;
			it++;
		}
		for( int i = kIdx_Begin; i < kIdx_Count; i++ )
		{
			g_ConstantValues[i] = 0;
		}
		g_ConstantValuesMap.clear();
	}

	MODI_ConstantValue * FindInMap( const char * szName )
	{
		if( !szName || !szName[0] )
			return 0;

		MAP_CONSTANTVALUE_ITER it = g_ConstantValuesMap.find( szName ); 
		if( it != g_ConstantValuesMap.end() )
		{
			return it->second;
		}
		return 0;
	}

	void 	set_DWORDValue_ByName( const char * szName , DWORD nValue )
	{
		MODI_ConstantValue * p = FindInMap( szName );
		if( p )
		{
			p->cur_v.nValue = nValue;
		}
	}

	void 	set_IntValue_ByName( const char * szName , int iValue )
	{
		MODI_ConstantValue * p = FindInMap( szName );
		if( p )
		{
			p->cur_v.iValue = iValue;
		}
	}

	void 	set_FloatValue_ByName( const char * szName , float fValue )
	{
		MODI_ConstantValue * p = FindInMap( szName );
		if( p )
		{
			p->cur_v.fValue = fValue;
		}
	}

	bool 	get_ValueType_ByName( const char * szName , enConstantValueType & vt )
	{
		MODI_ConstantValue * p = FindInMap( szName );
		if( p )
		{
			vt = p->vtype;
			return true;
		}
		return false;
	}

	bool 	IsCanDynmaicModfiy( DWORD nIdx )
	{
		if ( nIdx > kIdx_Begin && nIdx < kIdx_Count )
		{
			if( g_ConstantValues[nIdx] && (g_ConstantValues[nIdx]->attr  & kMask_DynmaicWrite ) )
				return true;
		}
		return false;
	}

	bool 	IsCanStaticModfiy( DWORD nIdx )
	{
		if ( nIdx > kIdx_Begin && nIdx < kIdx_Count )
		{
			if( g_ConstantValues[nIdx] && (g_ConstantValues[nIdx]->attr  & kMask_StaticWrite ) )
				return true;
		}
		return false;
	}

	DWORD 	get_EnterRareShopInterval() 
	{
		return g_ConstantValues[kIdx_EnterRareShopInterval]->cur_v.nValue;
	}

	void 	set_EnterRareShopInterval(DWORD nSet)
	{
		g_ConstantValues[kIdx_EnterRareShopInterval]->cur_v.nValue = nSet;
	}

	// 进入神秘商城概率
	DWORD 	get_EnterRareShopPro() 
	{
		return g_ConstantValues[kIdx_EnterRareShopPro]->cur_v.nValue;
	}

	void 	set_EnterRareShopPro(DWORD nSet)
	{
		g_ConstantValues[kIdx_EnterRareShopPro]->cur_v.nValue = nSet;
	}

	DWORD 	get_CharacterSyncAttrInterval()
	{
		return g_ConstantValues[kIdx_CharacterSyncAttrInterval]->cur_v.nValue;
	}

	void 	set_CharacterSyncAttrInterval( DWORD nSet)
	{
		g_ConstantValues[kIdx_CharacterSyncAttrInterval]->cur_v.nValue = nSet;
	}

	DWORD 	get_CharacterDBSaveInterval()
	{
		return g_ConstantValues[kIdx_CharacterDBSaveInterval]->cur_v.nValue;
	}

	void 	set_CharacterDBSaveInterval( DWORD nSet)
	{
		g_ConstantValues[kIdx_CharacterDBSaveInterval]->cur_v.nValue = nSet;
	}

	float 	get_ShowTimePro()
	{
		return g_ConstantValues[kIdx_Showtime_Pro]->cur_v.fValue;
	}

	void 	set_ShowTimePro(float fSet)
	{
		g_ConstantValues[kIdx_Showtime_Pro]->cur_v.fValue = fSet;
	}

	bool 	get_FangChenmiOff()
	{
		return g_ConstantValues[kIdx_FangChenmi_Off]->cur_v.iValue;
	}

	void 	set_FangChenmiOff(int iSet)
	{
		g_ConstantValues[kIdx_FangChenmi_Off]->cur_v.iValue = iSet;
	}

	bool 	get_OneLogin()
	{
		return g_ConstantValues[kIdx_OneLogin]->cur_v.iValue;
	}

	void 	set_OneLogin(int iSet)
	{
		g_ConstantValues[kIdx_OneLogin]->cur_v.iValue = iSet;
	}

	bool 	get_LevelupToten()
	{
		return g_ConstantValues[kIdx_Leveluptoten]->cur_v.iValue;
	}

	void 	set_LevelupToten(int iSet)
	{
		g_ConstantValues[kIdx_Leveluptoten]->cur_v.iValue = iSet;
	}


};


