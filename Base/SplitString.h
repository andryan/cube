/**
 * @file SplitString.h
 * @date 2010-01-27 CST
 * @verison $Id $
 * @author hurixin hurixin@modi.com
 * @brief 分割字符
 *
 */

#ifndef _MDSPLITSTRING_H
#define _MDSPLITSTRING_H

#include <vector>
#include "Share.h"

/**
 * @brief 字符分割管理
 *
 */
class MODI_SplitString
{
 public:
	typedef __gnu_cxx::hash_map<std::string, std::string, str_hash> defSplitMap;
	typedef defSplitMap::iterator defSplitMapIter;
	typedef defSplitMap::value_type defSplitMapValue;

	MODI_SplitString()
	{
		
	}

	/// 获取某个字段的值
	const char * operator[](const char * key)
	{
		return m_SplitMap[key].c_str();
	}
	
	/// 初始化
	bool Init(const char * split_string, const char * split = "\t\n");

	/// 初始化从xml来的字符
	bool InitXML(const char * split_string, const char * split = " ");
 private:
	/// 分隔字符
  	void StringSplit(std::vector<std::string> & string_container, const char * in_string, const char * split="\t\n");
	
	/// 字符存放
	defSplitMap m_SplitMap;
};

#endif
