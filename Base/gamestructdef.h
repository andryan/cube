/*
 * @file gamestructdef.h
 * @brief 服务器游戏数据结构定义
 * @author Tang Teng
 * @version v.01
 * @date 2010-04-21
 */

#ifndef GAME_STRUCT_DEFINE_H_
#define GAME_STRUCT_DEFINE_H_

#include "BaseOS.h"
#include "protocol/gamedefine.h"
#include "protocol/gamedefine2.h"
#include "protocol/relation_def.h"
#include "protocol/family_def.h"
#include "ScriptMailDef.h"

const std::string create_family_time_var = "create_family_timeout";
const std::string request_family_time_var = "request_family_timeout";

typedef MODI_ObjUpdateMask 	MODI_Flag;


enum enObjStateMask
{
	kObjState_ChangeAvatar = 1, // 变形中
	kObjState_PlayAnimation = 2, // 播放动画中
	kObjState_PlayExpression = 3, // 播放表情中
};
///  全局变量名字的最大长度
#define  GLOBAL_VAR_NAME_LEN	 20

// 联系人最大上限
#define RELATIONLIST_MAX_COUNT 	MAX_FRIENDCOUNT + MAX_BLACKCOUNT + MAX_IDOLCOUNT +300

#define IMPACT_MAX_PARAMS 	8
#define IMPACT_MAX_COUNT 	10


#define SERVER_MAIL_MAX_COUNT 	40

#define SCRIPTMAIL_DATA_SIZE 	150


#define MAX_NORMAL_FLAGS_ONSERVER_BYTES 10 

#define ITEM_MAX_LAP_COUNT 	10000

#define YUNYING_ITEM_MAXSIZE 	512

/// 变量名最大长度
const BYTE MAX_VAR_NAME_LENGTH = 33;

enum enNormalFlags
{
	kClientNormalFlags_FirstLogin = 0, // 是否第一次登入

};

#pragma pack(push,1)
struct tagGUIDHigh
{
	char 	obj_type;
	char  	svr_id;
	char 	world_id;
	char 	id_layer;
};

struct tagFakeTransID
{
	BYTE 	servertype;
	BYTE 	serverid;
	DWORD 	date;
	WORD 	count;
};
#pragma pack(pop)

enum enKickReason
{
	kAntherLogin,
	kAntherLoginImmediatelyKick,
	kNotOnServerTimeout,
	kServerNotExist,
	kRechangeChannel,
	kAdminKick,

};

// 客户端对于邀请的处理模式
enum enInviteHandlerMode
{
	enInviteH_AcceptAll,  // 接收所有邀请
	enInviteH_RejectAll, // 拒绝所有邀请
};

// 房间状态
enum enRoomStatus
{
   	ROOM_STATUS_USING = 0,
   	ROOM_STATUS_START,
   	ROOM_STATUS_PLAYING,
	ROOM_STATUS_SONGEROVER,
	ROOM_STATUS_VOTE,
	ROOM_STATUS_SHOWVOTERESULT,
	ROOM_STATUS_BACKING, 	// 	回到房间倒计时
};

// 游戏结束类型
enum
{
	GAMEMATCHEND_TYPE_NORMAL = 0,
	GAMEMATCHEND_TYPE_HALFWAY,
};


enum
{
	kImpactH_Successful,
	kImpactH_CannotFindT,
	kImpactH_CannotFindConfig,
	kImpactH_ConfigDataFaild,
	kImpactH_ParamFaild,
	kImpactH_AntherExist,
};


inline const char * 	GetKickReasonStringFormat( enKickReason r )
{
	static const std::string strReason[] = {
		"AntherLogin",
		"kAntherLoginImmediatelyKick",
		"NotOnServerTimeout",
		"ServerNotExist",
		"RechangeChannel"
	};

	return strReason[r].c_str();
}

inline bool 	EncodeItemGUID( MODI_GUID & guid , BYTE  byWorld , BYTE  byServer , DWORD  nSerial )
{
	tagGUIDHigh h;
	h.obj_type = enObjType_Item;
	h.id_layer = 0;
	h.world_id = byWorld;
	h.svr_id = byServer;

	// 是否道具类型
	DWORD * hg = (DWORD *)(&h);
	guid = MAKE_GUID( nSerial , *hg );

	return true;
}

inline bool 	DecodeItemGUID( const MODI_GUID & guid , BYTE & byWorld , BYTE & byServer , DWORD & nSerial )
{
	// 是否道具类型
	DWORD hg = GUID_HIPART( guid );
	tagGUIDHigh * p =(tagGUIDHigh *)(&hg);
	if( p->obj_type != enObjType_Item )
	{
		return false;
	}

	byWorld = p->world_id;
	byServer = p->svr_id;
	nSerial = GUID_LOPART( guid );
	if( !byWorld || !byServer || !nSerial )
	{
		return false;
	}

	return true;
}


/** 
 * @brief 创建道具的信息
 * 
 */
struct MODI_CreateItemInfo
{
    /// 道具属性id
	DWORD m_dwConfigId;
	/// 道具期限
	BYTE m_byLimit;
	/// 创建的理由
	enAddItemReason m_enCreateReason;
	/// 服务器id
	BYTE m_byServerId;
	
	MODI_CreateItemInfo()
	{
		m_dwConfigId = INVAILD_CONFIGID;
		m_byLimit = 0;
		m_enCreateReason = enAddReason_None;
		m_byServerId = 0;
	}
};


/**
 * @brief 道具的属性，对应数据库表结构
 * 
 */
struct MODI_DBItemInfo
{
	/// 道具id
	QWORD m_qdItemId;
	/// 道具配置id
	DWORD m_dwConfigId;
	/// 包裹类型
	BYTE m_byBagType;
	/// 包裹位置
	WORD m_wdBagPos;
	/// 到期时间
	QWORD m_qdExpireTime;
	/// 创建理由
	BYTE m_byCreateReason;
	/// 服务器id
	BYTE m_byServerId;
	/// 道具时效类型
	BYTE m_byLimit;

	MODI_DBItemInfo()
	{
		Reset();
	}

	/// 清除该位置的信息
	void Reset()
	{
		m_qdItemId = 0;
		m_dwConfigId = INVAILD_CONFIGID;
		m_byBagType = enBagType_Avatar;
		m_wdBagPos = INVAILD_POS_INBAG;
		m_qdExpireTime = 0;
		m_byCreateReason = 0;
		m_byServerId = 0;
		m_byLimit = 0;
	}
};

/// 衣饰背包
struct MODI_AvatarBag
{
	MODI_DBItemInfo m_Items[MAX_BAG_AVATAR_COUNT];

	WORD Size() const
	{
		return sizeof(m_Items) / sizeof(MODI_DBItemInfo);
	}
};

/// 玩家背包
struct MODI_PlayerBag
{
	MODI_DBItemInfo m_Items[MAX_BAG_PLAYER_COUNT];
	
	WORD Size() const
	{
		return sizeof(m_Items) / sizeof(MODI_DBItemInfo);
	}
};

// 用于从数据库中加载物品
struct MODI_LoadItem
{
	MODI_AvatarBag * 	m_pAvatarBag;
	MODI_PlayerBag * 	m_pPlayerBag;
	bool 	 			m_bTrans;

	MODI_LoadItem():m_pAvatarBag(0),
	m_pPlayerBag(0),
	m_bTrans( false )
	{

	}
};


struct MODI_SaveItem
{
	MODI_DBItemInfo 	* 	m_pItemInfoSave;
	WORD 					m_nItemSaveCount;
	bool 					m_bTrans;

	MODI_SaveItem():m_pItemInfoSave(0),
	m_nItemSaveCount(0),
	m_bTrans(false)
	{

	}
};

struct MODI_ItemInfoAdd
{
	WORD 				m_nConfigID;
	DWORD 				m_nRemainTime;
	BYTE 				m_nBagType;
	BYTE 				m_nItemPos;
};


struct MODI_AddItem
{
	WORD m_nItemAddCount; 
	MODI_ItemInfoAdd * m_pItemInfoAdd; 

	MODI_AddItem():m_nItemAddCount(0),m_pItemInfoAdd(0)
	{

	}
};

struct MODI_AddCharacter
{
	const MODI_CreateRoleInfo * 	m_pCreateInfo;
	MODI_AddItem 					m_itemAdd;

	MODI_AddCharacter():m_pCreateInfo(0)
	{

	}
};


struct MODI_DBRelationInfo : public MODI_RelationInfo
{
	DWORD		m_dwRelid;
	bool 		m_bIsValid;

	MODI_DBRelationInfo()
	{
		m_bIsValid = false;
		m_dwRelid = 0;
	}

	void Reset()
	{
		m_guid = INVAILD_GUID;
		memset( m_szName , 0 ,  sizeof(m_szName) );
		m_bySex = enSexBoy;
		m_RelationType = enRelationT_None;
		m_bIsValid = false;
		m_bOnline = false;
	}

	bool IsVaildParam() const
	{
		if( GUID_LOPART(m_guid) != INVAILD_CHARID )
			return false;
		return true;
	}

	void ConvertTo( MODI_RelationInfo & out )
	{
		const MODI_RelationInfo  * p = (const MODI_RelationInfo *)(this);
		out = *p;
	}

	MODI_RelationInfo ConvertToRelationInfo()
	{
		const MODI_RelationInfo  * p = (const MODI_RelationInfo *)(this);
		return *p;
	}

	void Set( MODI_RelationInfo & info )
	{
		MODI_RelationInfo  * p = (MODI_RelationInfo *)(this);
		*p = info;
	}

};

struct MODI_LoadRelationInfo
{
	DWORD 					m_nSize;
	MODI_DBRelationInfo * 	m_pRelationList;
	bool 					m_bTrans;
	
	MODI_LoadRelationInfo():m_nSize(0),
	m_pRelationList(0),
	m_bTrans(false)
	{

	}
};

struct MODI_SaveRelationInfo
{
	DWORD 					m_nSize;
	MODI_DBRelationInfo * 	m_pRelationList;
	bool 					m_bTrans;
	
	MODI_SaveRelationInfo():m_nSize(0),
	m_pRelationList(0),
	m_bTrans(false)
	{

	}
};

struct MODI_GamePlayDataOnClient
{
	enInviteHandlerMode m_enInviteMode;
	/// 当前GOOD以上的次数
	unsigned int m_nCurGoodPlusCount;
	/// 当前的最大连击数	
	unsigned int m_nMaxGoodPlusCount; 
	/// 客户端一共唱对的秒数
	float m_fRightSec; 
	/// 上次被邀请的房间ID
	defRoomID 			m_InviteRoomID;
	int 	m_effectType;
	float 	m_ftime;
	bool 	m_bRelease;
	bool 	m_bIsSingleGame;
	
	MODI_GamePlayDataOnClient()
	{
		m_nCurGoodPlusCount = 0;
		m_nMaxGoodPlusCount = 0;
		m_fRightSec = 0.0f;
		m_InviteRoomID = INVAILD_ROOM_ID;
		m_effectType = enEffect_None;
		m_ftime = 0.0f;
		m_bRelease = false;
		m_bIsSingleGame = false;

	}
};

struct MODI_CharacterTempData
{
	MODI_GamePlayDataOnClient m_AboutGamePlay;

};

// 角色的额外数据
struct MODI_RoleExtraData
{
	time_t 	m_nLastLoginTime;
	char 	m_szLastLoginIP[32];
	int 	m_iGMLevel;
	char 	m_szNormalFlags[MAX_NORMAL_FLAGS_ONSERVER_BYTES];
	DWORD 	m_nTodayOnlineTime;
	DWORD 	m_nTotalOnlineTime;
	bool 	m_bFangChenmi;
	DWORD 	consume_rmb;
	
	/// 被拉黑的次数
	WORD m_wdBlockCount;
	time_t m_nRegisterTime;
	char m_cstrAccName[MAX_ACCOUNT_LEN];
	
	MODI_RoleExtraData()
	{
		m_nLastLoginTime = 0;
		memset( m_szLastLoginIP , 0 , sizeof(m_szLastLoginIP) ); 
		m_iGMLevel = 0;
		memset( m_szNormalFlags , 0 , sizeof(m_szNormalFlags) );
		m_nTodayOnlineTime = 0;
		m_nTotalOnlineTime = 0;
		m_bFangChenmi = false;
		consume_rmb = 0;
		m_wdBlockCount = 0;
		m_nRegisterTime = 0;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
};

struct MODI_DBSaveCharBaseInfo
{
	const MODI_RoleExtraData * pExtraData;
	const MODI_RoleInfo * 	pRoleData;
	const char * pOtherData;
	DWORD m_dwSize;
	
	
	MODI_DBSaveCharBaseInfo():pExtraData(0),pRoleData(0),pOtherData(0)
	{
		m_dwSize = 0;
	}
};

// 角色的所有属性
struct MODI_CharactarFullData
{
	MODI_RoleInfo 	m_roleInfo;  // 角色信息
	MODI_RoleExtraData 	m_roleExtraData; // 角色额外数据
	//MODI_AvatarBag  m_avatarBag; // 角色装备
	//MODI_PlayerBag 	m_playerBag; // 玩家背包
//	MODI_DBRelationInfo m_Relations[RELATIONLIST_MAX_COUNT]; // 联系人列表
};

struct MODI_ServerItemSerialInfo
{
	BYTE byWorld;
	BYTE byServer;
	DWORD nItemSerial;

	MODI_ServerItemSerialInfo():byWorld(0),byServer(0),nItemSerial(0)
	{

	}
};

typedef unsigned long long MODI_DBMail_ID;
#define INVAILD_DBMAIL_ID 	0

struct MODI_DBMailInfo 
{
	MODI_MailInfo 		m_MailInfo;
	MODI_DBMail_ID  	m_dbID;
	bool 				m_bIsValid;
	QWORD 				m_Transid;
	MODI_ScriptMailData m_ScriptData;

	MODI_DBMailInfo()
	{
		Reset();
	}

	void Reset()
	{
		m_dbID = INVAILD_DBMAIL_ID;
		m_bIsValid = false;
		m_MailInfo.Reset();
		m_ScriptData.Reset();
		m_Transid = 0;
	}

};

struct MODI_DBMaillist
{
	MODI_DBMailInfo 	m_Mails[MAILLIST_MAX_COUNT];
	MODI_DBMailInfo 	m_ServerMail[SERVER_MAIL_MAX_COUNT];

	MODI_DBMaillist()
	{

	}

	void Reset()
	{
		for( int i = 0; i < MAILLIST_MAX_COUNT; i++ )
		{
			m_Mails[i].Reset();
		}

		for( int i = 0; i < SERVER_MAIL_MAX_COUNT; i++ )
		{
			m_ServerMail[i].Reset();
		}
	}
};

struct MODI_DBLoadMail
{
	MODI_DBMaillist * 	m_pList;
	bool 				m_bTrans;

	MODI_DBLoadMail():m_pList(0),m_bTrans(false)
	{

	}
};

#define CHECK_PACKAGE( package , cmd_size , per_size , count , pClient ) 	{\
	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( package , per_size , count );\
	if( nSafePackageSize != cmd_size )\
	{\
		Global::logger->warn("[%s] client <charid=%u , name=%s> send an invaild size package<%s>." , \
				ERROR_PACKAGE ,\
			   	pClient->GetCharID() , \
				pClient->GetRoleName() , #package); \
		return enPHandler_Kick;\
	}}

inline void 	EncodeTransid( QWORD & transid , BYTE svrid , BYTE svrtype , DWORD date , WORD count )
{
	tagFakeTransID ft;
	ft.count = count;
	ft.date = date;
	ft.serverid = svrid;
	ft.servertype = svrtype;
	memcpy( &transid , &ft , sizeof(ft) );
}

struct MODI_QueryHasChar_Shop_ExtraData
{
	BYTE 			count;
	MODI_ShopGood 	goods[ONCE_TRANSACTION_MAX_GOODSCOUNT];
	char 			content[MAIL_CONTENTS_MAX_LEN + 1];
	DWORD 			money;
	DWORD 			RMBMoeny;
};

struct MODI_BillTransactionBill_ExtraData
{
	BYTE 			count;
	MODI_ShopGood 	goods[ONCE_TRANSACTION_MAX_GOODSCOUNT];
	char 			content[MAIL_CONTENTS_MAX_LEN + 1];
	DWORD 			money;
	DWORD 			RMBMoeny;
	char 			sender[ROLE_NAME_MAX_LEN + 1];
	char 			recever[ROLE_NAME_MAX_LEN + 1];
	
	MODI_BillTransactionBill_ExtraData()
	{
		count = 0;
		memset( content , 0 , sizeof(content) );
		money = 0;
		RMBMoeny = 0;
		memset( sender , 0 , sizeof(sender) );
		memset( recever , 0 , sizeof(recever) );
	}
};

enum 	enShopGoodRecevieState
{
	kShopGoodRecvState_Done, // 接受到
	kShopGoodRecvState_Mailing, // 邮寄中
};

// 商城添加物品的记录
struct MODI_ShopCreateItemHistroy
{
	QWORD 		transid;
	MODI_CHARID charid;
	BYTE 		count;
	DWORD 		itemserial[ONCE_TRANSACTION_MAX_GOODSCOUNT];
	enShopTransactionType 	createtype;
	enShopGoodRecevieState 	recvstate;
	MODI_DBMail_ID 	mailid;
	
	MODI_ShopCreateItemHistroy()
	{
		transid = 0;
		charid = INVAILD_CHARID;
		count = 0;
		memset( itemserial , 0 , sizeof(itemserial) );
		createtype = kTransaction_Buy;
		recvstate = kShopGoodRecvState_Done;
		mailid = INVAILD_DBMAIL_ID;
	}
};

	// 帐号，所在区，物品类型，数量，总价，用户ID，游戏货币数量，RMB数量 ----> BL
// 商城交易信息
struct MODI_ShopTransactionInfo
{
	QWORD 	ordid; 	// 交易号
	DWORD 	accid; // 帐号
	WORD 	worldid; // 所在世界
	MODI_ShopGood goods[ONCE_TRANSACTION_MAX_GOODSCOUNT];
	DWORD 	total_rmb; // 扣除金额
	MODI_CHARID charid; // 角色
};

struct MODI_GoodsPackageArray
{
	struct element
	{
		WORD 	nGoodID;
		WORD 	nCount;
		enShopGoodTimeType 	time;

		element()
		{
			nGoodID = INVAILD_CONFIGID;
			nCount = 0;
			time = kShopGoodT_Begin;
		}
	};
	
	BYTE 			m_count;
	element 		m_array[10];


	MODI_GoodsPackageArray()
	{
		m_count = 0;
	}
};

struct MODI_YunYingCDKeyContent
{
	char 	m_szContent[YUNYING_ITEM_MAXSIZE + 1];
	bool 	m_bIsValid;
	
	MODI_YunYingCDKeyContent()
	{
		memset( m_szContent , 0 , sizeof(m_szContent) );
		m_bIsValid = false;
	}
};


struct MODI_DBRank_Level : public MODI_Rank_Level
{
	MODI_CHARID 	charid;

	MODI_DBRank_Level()
	{
		charid = INVAILD_CHARID;
	}
};

struct MODI_DBRank_Level_List 
{
	size_t 			size;
	MODI_DBRank_Level  	ranks[RANK_LEVEL_MAX_TOP];
	
	MODI_DBRank_Level_List()
	{
		size = 0;
	}
};

struct MODI_DBRank_Renqi : public MODI_Rank_Renqi
{
	MODI_CHARID 	charid;

	MODI_DBRank_Renqi()
	{
		charid = INVAILD_CHARID;
	}
};

struct MODI_DBRank_Renqi_List 
{
	size_t 			size;
	MODI_DBRank_Renqi ranks[RANK_RENQI_MAX_TOP];
	
	MODI_DBRank_Renqi_List()
	{
		size = 0;
	}
};

struct MODI_DBRank_ConsumeRMB : public MODI_Rank_ConsumeRMB
{
	MODI_CHARID 	charid;
	MODI_DBRank_ConsumeRMB()
	{
		charid = INVAILD_CHARID;
	}
};

struct MODI_DBRank_ConsumeRMB_List 
{
	size_t 			size;
	MODI_DBRank_ConsumeRMB ranks[RANK_CONSUMERMB_MAX_TOP];

	MODI_DBRank_ConsumeRMB_List()
	{
		size = 0;
	}
};

struct MODI_DBRank_HighestScore : public MODI_Rank_HighestScore
{
	MODI_CHARID 	charid;
	MODI_DBRank_HighestScore()
	{
		charid = INVAILD_CHARID;
	}
};

struct MODI_DBRank_HighestScore_List
{
	size_t 	size;
	MODI_DBRank_HighestScore ranks[RANK_HIGHESTSCORE_MAX_TOP];

	MODI_DBRank_HighestScore_List()
	{

	}
};


struct MODI_DBRank_HighestPrecision : public MODI_Rank_HighestPrecision
{
	MODI_CHARID 	charid;
	MODI_DBRank_HighestPrecision()
	{
		charid = INVAILD_CHARID;
	}
};

struct MODI_DBRank_HighestPrecision_List
{
	size_t 	size;
	MODI_DBRank_HighestPrecision ranks[RANK_HIGHESTPRECISION_MAX_TOP];

	MODI_DBRank_HighestPrecision_List()
	{
		size = 0;
	}
};

struct MODI_ReqRoleDetail_RankSpecific_ExtraData
{
	enRankType 	rank_type;
	WORD 	rank_no;
};

struct MODI_DBSelfRank
{
	size_t size;
	MODI_CHARID * ranks;

	MODI_DBSelfRank():size(0),ranks(0)
	{

	}
};


/// 变量相关定义
namespace MODI_VarState
{
	/// 变量类型
	enum enVarType
	{
		/// 普遍
		enNormalType = 1,
		
		/// 0点
		enDayClearType = 2,

		/// 0点加的类型
		enDayIncType = 3,
		
		/// 秒计数
		enTimeClearType = 4,

		/// 时间触发某事情
		enTriggerType = 5,
	};

	/// 任务状态
	enum enTaskState
	{
		/// 任务开始
		enTaskBegin = 1,

		/// 任务结束
		enTaskEnd = 255,
	};
 
};

/// 用户变量定义
struct MODI_VarData
{
	MODI_VarData()
	{
		memset(m_szName, 0, sizeof(m_szName));
		m_wdValue = 0;
		m_byType = 0;
		m_dwUpdateTime = 0;
		m_dwAddTime = 0;
		m_dwDelTime = 0;
	}
	/// 变量名
	char m_szName[MAX_VAR_NAME_LENGTH];
	/// 变量值
	WORD m_wdValue;
	/// 变量类型
	BYTE m_byType;
	/// 变量下一次更新的时间
	DWORD m_dwUpdateTime;
	/// 变量更新的时间量
	DWORD m_dwAddTime;
	/// 变量删除的时间
	DWORD m_dwDelTime;
};



/** 
 * @brief 家族的基本信息
 * 
 */
struct MODI_FamilyBaseInfo
{
	DWORD m_dwFamilyID;
	char m_cstrFamilyName[FAMILY_NAME_LEN + 1];
	QWORD m_qdCreateTime;
	WORD m_wdLevel;
	BYTE m_byTotem;
	DWORD m_dwRenqi;
	DWORD m_dwMoney;
	char m_cstrXuanyan[FAMILY_XUANYAN_LEN + 1];
	char m_cstrPublic[FAMILY_PUBLIC_LEN + 1];

	MODI_FamilyBaseInfo()
	{
		m_dwFamilyID = INVAILD_FAMILY_ID;
		memset(m_cstrFamilyName, 0, sizeof(m_cstrFamilyName));
		m_qdCreateTime = 0;
		m_wdLevel = 1;
		m_byTotem = 0;
		m_dwRenqi = 0;
		m_dwMoney = 0;
		memset(m_cstrXuanyan, 0, sizeof(m_cstrXuanyan));
		memset(m_cstrPublic, 0, sizeof(m_cstrPublic));
	}
};


/**
 * @brief 家族成员的基本信息
 * 
 */
struct MODI_FamilyMemBaseInfo
{
	DWORD m_dwFamilyID;  
	MODI_GUID m_stGuid;
  	DWORD m_dwAccountID;
	char m_cstrFamilyName[FAMILY_NAME_LEN + 1];
  	char m_cstrRoleName[ ROLE_NAME_MAX_LEN + 1]; 
  	BYTE m_bySex;
	DWORD m_dwRenqi;
	WORD m_wdChenghao;
  	enFamilyPositionType m_enPosition;
  	enFamilyMemStateType m_enState;	
  	DWORD m_dwContribute;	
  	QWORD m_qdLastlogin;
	WORD m_wLevel;

  	MODI_FamilyMemBaseInfo()
  	{
  	  	m_dwFamilyID = 0;
  	  	m_stGuid = INVAILD_GUID;
  	  	m_dwAccountID = 0;
		memset(m_cstrFamilyName, 0, sizeof(m_cstrFamilyName));
  	  	memset(m_cstrRoleName, 0, sizeof(m_cstrRoleName));
  	  	m_bySex = 0;
  	  	m_enPosition = enPositionT_None;
  	  	m_enState = enMemStateT_None;
		m_dwContribute = 0;
  	  	m_qdLastlogin = 0;
		m_wLevel=0;
  	}

	void Clear()
	{
		m_dwFamilyID = 0;
		memset(m_cstrFamilyName, 0, sizeof(m_cstrFamilyName));
  	  	m_enPosition = enPositionT_None;
  	  	m_enState = enMemStateT_None;
		m_dwContribute = 0;
  	  	m_qdLastlogin = 0;
	}
};


enum enUpDateCFReason
{
	enNone_Reason = 0,
	enCreateFamily_Reason = 1,	///创建家族
	enLogin_Reason,         /// 刚登陆
	enRequest_Reason,		/// 刚申请
	enAccept_Reason,		/// 通过批准
	enLeaveFamily_Reason,   /// 离开家族
	enRefuseFamily_Reason,  /// 被拒绝
	enFireFamily_Reason,    /// 开除出家族
};

struct MODI_GlobalVar
{
	char m_csName[GLOBAL_VAR_NAME_LEN+1];
	WORD	m_wValue;
	MODI_GlobalVar()
	{
		m_wValue = 0;
		memset(m_csName,0,sizeof(m_csName));
	}
	~MODI_GlobalVar()
	{
	}
};


struct SingleMusic
{
	WORD	m_wMusicid;	///音乐ID
	DWORD	m_dwCount;	///音乐点击次数
	DWORD	m_dwScore;	///最高分数
	WORD	m_wPrecise;	///	最高精度
	char	m_szScorePerson[ROLE_NAME_MAX_LEN+1];	///最高得分玩家名字
	char	m_szPrecisePerson[ROLE_NAME_MAX_LEN+1];///最高精度玩家名字
	time_t	m_tScoreFresh;	///最高分刷新时间
	time_t	m_tPreciseFresh;	/// 最高准度刷新时间

	///  以下是键盘模式的
	DWORD	m_dwKeyScore;	///最高分数
	WORD	m_wKeyPrecise;	///	最高精度
	char	m_szKeyScorePerson[ROLE_NAME_MAX_LEN+1];	///最高得分玩家名字
	char	m_szKeyPrecisePerson[ROLE_NAME_MAX_LEN+1];///最高精度玩家名字
	time_t	m_tKeyScoreFresh;	///最高分刷新时间
	time_t	m_tKeyPreciseFresh;	/// 最高准度刷新时间


	SingleMusic()
	{
		memset(this,0,sizeof(SingleMusic));
	}

};


enum enMRefreshMoney
{
	enMRefreshMoney_None,

	/// 家族升级
	enMRefreshMoney_FamilyLevel,

	/// 购买
	enMRefreshMoney_BuyDec,

	/// 第一次充值
	enMRefreshMoney_FirstPay,
	
	/// 充值
	enMRefreshMoney_Pay20,
	enMRefreshMoney_Pay30,
	enMRefreshMoney_Pay60,
	enMRefreshMoney_Pay100,
	enMRefreshMoney_Pay200,
	enMRefreshMoney_Pay500,
	
};


#endif

