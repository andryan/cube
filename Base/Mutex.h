/**
 * @file Mutex.h
 * @date 2009-12-07 09::56:00 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 系统互斥体封装
 *
 * 主要是封装了使用时的初始化和释放互斥体对象的操作
 *
 */

#ifndef _MDMUTE_H_
#define _MDMUTE_H_

#include <pthread.h>
#include <iostream>

class MODI_Cond;

/**
 * @brief 互斥体封装
 *
 */
class MODI_Mutex : private MODI_DisableCopy
{
	friend class MODI_Cond;
 public:
	/**
	 * @brief 构造函数
	 *
	 */ 
	MODI_Mutex(int kind = PTHREAD_MUTEX_FAST_NP)
	{

		pthread_mutexattr_t attr;
		::pthread_mutexattr_init(&attr);
		::pthread_mutexattr_settype(&attr, kind);
		::pthread_mutex_init(&m_mutex, &attr);
	}

	/**
	 * @brief 析构
	 *
	 */
	~MODI_Mutex()
	{
		::pthread_mutex_destroy(&m_mutex);
	}

	/**
	 * @brief 加锁
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	inline int lock()
	{
		int ret_code = 0;
		ret_code = ::pthread_mutex_lock(&m_mutex);
		return ret_code;
	}

	/**
	 * @brief 解锁
	 *
	 * @return 成功返回0,失败返回错误代码
	 */ 
	inline int unlock()
	{
		int ret_code = 0;
		ret_code = ::pthread_mutex_unlock(&m_mutex);
		return ret_code;
	}
 private:
	/// 系统互斥体
	pthread_mutex_t m_mutex;
};

#endif

