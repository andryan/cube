/** 
 * @file s2rdb_cmd.h
 * @brief db proxy 协议定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef MODI_STOROLE_DB_DEFINED_H_
#define MODI_STOROLE_DB_DEFINED_H_

#ifdef _MSC_VER
#pragma once
#endif

#include <string>
#include <list>
#include "gamestructdef.h"
#include "nullCmd.h"
#include "session_id.h"
#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "protocol/gamedefine2.h"
#include "s2unify_cmd.h"

using std::list;
using std::string;

#pragma pack(push,1)

const unsigned char MAINCMD_S2RDB = 0x10;

struct MODI_S2RDB_Cmd : public stNullCmd
{
	MODI_S2RDB_Cmd()
	{
		byCmd = MAINCMD_S2RDB;
	}
};


// 请求角色列表
struct MODI_S2RDB_Request_Charlist : public MODI_S2RDB_Cmd
{
	MODI_SessionID 	m_nSessionID;
	defAccountID 	m_nAccountID;

	MODI_S2RDB_Request_Charlist()
	{
		byParam = ms_SubCmd;
		m_nAccountID = INVAILD_ACCOUNT_ID;
	}

	static const unsigned char 	ms_SubCmd = 1;
};

// 客户端请求角色列表的返回
struct MODI_RDB2S_Notify_Charlist : public MODI_S2RDB_Cmd
{
	MODI_SessionID  m_nSessionID;
	defAccountID  	m_nAccountID;
	unsigned char 	m_bySuccessful;
	unsigned char 	m_nRoleCount; // 角色个数，0代表没有角色
	MODI_CharInfo 	m_pData; // 具体每个角色的基本信息

	MODI_RDB2S_Notify_Charlist() 
    {
		byParam = ms_SubCmd;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		m_bySuccessful = 1;
		m_nRoleCount = 0;
    }
	static const unsigned char 	ms_SubCmd = 2;
};


/// 请求加载角色数据
struct MODI_S2RDB_Request_LoadCharData : public MODI_S2RDB_Cmd
{
	MODI_CHARID 		m_nCharID;
	MODI_SessionID 		m_nSessionID;
	defAccountID 		m_nAccountID;
	BYTE 				m_nServerID;

	MODI_S2RDB_Request_LoadCharData()
	{
		byParam = ms_SubCmd;
		m_nCharID = INVAILD_CHARID;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		m_nServerID = 0;
	}
	static const unsigned char 	ms_SubCmd = 3;
};


/// 对请求加载角色数据请求的返回
struct MODI_RDB2S_Notify_LoadCharData : public MODI_S2RDB_Cmd
{
	MODI_CHARID 	m_nCharID;
	defAccountID 	m_nAccountID;
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	unsigned char 	m_bySuccessful;
	MODI_CharactarFullData 	m_CharFullData;
	BYTE 			m_nServerID;
	MODI_SessionID 	m_nSessionID;

	DWORD m_dwSize;
	char m_Data[0];
	
	MODI_RDB2S_Notify_LoadCharData()
	{
		byParam = ms_SubCmd;
		m_nCharID = INVAILD_CHARID;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		m_bySuccessful = 0;
		m_dwSize = 0;
	}

	static const unsigned char 	ms_SubCmd = 4;
};


/// 请求保存角色数据
struct MODI_S2RDB_Request_SaveCharData : public MODI_S2RDB_Cmd
{
	MODI_SessionID 	m_nSessionID;
	MODI_CHARID 	m_nCharID;
	DWORD 			m_nChannelID;
	MODI_RoleInfo 	m_roleInfo;
	MODI_RoleExtraData 	m_roleExtraData; // 角色额外数据

	/// 杂项数据
	DWORD 			m_dwSize;
	char 			m_Data[0];

	MODI_S2RDB_Request_SaveCharData()
	{
		byParam = ms_SubCmd;
		m_nCharID = INVAILD_CHARID;
		m_nChannelID = 0;
		m_dwSize = 0;
	}

	static const unsigned char 	ms_SubCmd = 5;
};

/// 请求创建角色
struct MODI_S2RDB_Request_CreateChar : public MODI_S2RDB_Cmd
{
	MODI_SessionID 		m_nSessionID;
	defAccountID 		m_nAccountID;
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	MODI_CreateRoleInfo m_createInfo;	

	MODI_S2RDB_Request_CreateChar()
	{
		byParam = ms_SubCmd;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}

	static const unsigned char 	ms_SubCmd = 6;
};

/// 创建角色的结果
struct MODI_RDB2S_Notify_CreateCharResult : public MODI_S2RDB_Cmd
{
	MODI_SessionID 	m_nSessionID;
	defAccountID   	m_nAccountID; 
	WORD 		 	m_bySuccessful;
	MODI_CharInfo  	m_roleInfo;

	MODI_RDB2S_Notify_CreateCharResult()
	{
		byParam = ms_SubCmd;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		m_bySuccessful = 0;
	}

	static const unsigned char 	ms_SubCmd = 7;
};

// 保存物品的请求
struct MODI_S2RDB_Request_SaveItem : public MODI_S2RDB_Cmd
{
	MODI_CHARID 		m_nCharID;
 	WORD 				m_nSize;
	MODI_DBItemInfo  	m_pData[0];


	MODI_S2RDB_Request_SaveItem()
	{
		byParam = ms_SubCmd;
		m_nCharID = INVAILD_CHARID;
		m_nSize = 0;
	}

	static const unsigned char 	ms_SubCmd = 10;
};

// 请求加载item's serial
struct MODI_S2RDB_Request_ItemSerialTableOpt: public MODI_S2RDB_Cmd
{
	enum ItemSerialTableOpt
	{
		kLoad,
		kSave,
	};

	ItemSerialTableOpt 	m_opt;

	MODI_S2RDB_Request_ItemSerialTableOpt()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 11;
};

//// 请求某个服务器的 item's serial
struct MODI_S2RDB_Request_ItemSerial: public MODI_S2RDB_Cmd
{
	BYTE 	m_byWorldID;
	BYTE 	m_byServerID;

	MODI_S2RDB_Request_ItemSerial()
	{
		byParam = ms_SubCmd;
		m_byWorldID = 0;
		m_byServerID = 0;
	}

	static const unsigned char ms_SubCmd = 12;
};

// 对ITEM‘S SERIAL请求的回复
struct MODI_RDB2S_Notify_ItemSerial: public MODI_S2RDB_Cmd
{
	BYTE 	m_bySuccessful;
	DWORD 	m_nItemSerial;
	BYTE 	m_byWorldID;
	BYTE 	m_byServerID;

	MODI_RDB2S_Notify_ItemSerial()
	{
		byParam = ms_SubCmd;
		m_bySuccessful = 0;
		m_nItemSerial = 0;
		m_byWorldID = 0;
		m_byServerID = 0;
	}

	static const unsigned char ms_SubCmd = 13;
};

// 保存某个服务器的 item's serial
struct MODI_S2RDB_Request_SaveSerial: public MODI_S2RDB_Cmd
{
	BYTE 	m_byWorldID;
	BYTE 	m_byServerID;
	DWORD 	m_nItemSerial;
	BYTE 	m_byForce;

	MODI_S2RDB_Request_SaveSerial()
	{
		byParam = ms_SubCmd;
		m_byWorldID = 0;
		m_byServerID = 0;
		m_nItemSerial = 0;
		m_byForce = false;
	}

	static const unsigned char ms_SubCmd = 14;
};

/// 保存联系人信息
struct MODI_S2RDB_Request_SaveRelation : public MODI_S2RDB_Cmd
{
	MODI_CHARID 			m_Charid;
	WORD 					m_nSize;
	MODI_DBRelationInfo 	m_pData[0];

	MODI_S2RDB_Request_SaveRelation()
	{
		byParam = ms_SubCmd;
		m_Charid = INVAILD_CHARID;
		m_nSize = 0;
	}

	static const unsigned char ms_SubCmd = 15;
};

enum enReqRoleDetailOpt
{
	kReqRoleDetail, 
	kReqRelationOpt,
	kReqShopGiveGood,
	kReqRankSpecific,
	
	/// 充值送东西，
	kReqPayAction,
	/// 发送离线邮件
	kSendOfflineMails,
	/// gm发送邮件
	kSendGMMails,
};


// 请求某个角色的详细信息
struct MODI_S2RDB_Request_RoleDetailInfo : public MODI_S2RDB_Cmd
{
	enReqRoleDetailOpt	m_opt;
	MODI_CHARID m_reqCharid;
	char 	m_szName[ROLE_NAME_MAX_LEN + 1];
	defAccountID m_dwAccId;
	char m_szAccName[MAX_ACCOUNT_LEN + 1];
	BYTE 	m_RelationType;
	WORD 	m_nExtraSize;
	char 	m_szExtra[0];

	MODI_S2RDB_Request_RoleDetailInfo()
	{
		byParam = ms_SubCmd;
		m_opt =  kReqRoleDetail;
		m_reqCharid = INVAILD_CHARID;
		m_RelationType = 0;
		m_nExtraSize = 0;
		m_dwAccId = 0;
		memset( m_szName , 0 , sizeof(m_szName) );
		memset(m_szAccName, 0, sizeof(m_szAccName));
	}

	static const unsigned char ms_SubCmd = 16;
};

// 对某个角色请求的某个角色的详细信息返回
struct MODI_RDB2S_Notify_RoleDetailInfo : public MODI_S2RDB_Cmd
{
	bool 				m_bCharExist;
	enReqRoleDetailOpt	m_opt;
	MODI_CHARID 		m_reqCharid;
	MODI_CHARID 		m_charid;
	defAccountID 		m_dwAccId;
	MODI_NameCard 		m_namecard;
	BYTE 				m_RelationType;
	WORD 				m_nExtraSize;
	BYTE		m_bVip;			/// 是否是VIP
	char 				m_szExtra[0];

	MODI_RDB2S_Notify_RoleDetailInfo()
	{
		m_bCharExist = false;
		m_opt = kReqRoleDetail;
		m_reqCharid = INVAILD_CHARID;
		m_charid = INVAILD_CHARID; 
		m_RelationType = 0;
		m_nExtraSize = 0;
		m_dwAccId = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 17;
};

struct MODI_S2RDB_Request_SaveSocialData : public MODI_S2RDB_Cmd
{
	MODI_CHARID 		m_charid;
	MODI_SocialRelationData 	m_data;

	MODI_S2RDB_Request_SaveSocialData()
	{
		m_charid = INVAILD_CHARID; 
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 18;
};


struct MODI_S2RDB_Request_CreateItemHistroy : public MODI_S2RDB_Cmd
{
	MODI_ShopCreateItemHistroy m_Histroy;

	MODI_S2RDB_Request_CreateItemHistroy()
	{

		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 21;
};

enum enReqMailsReason
{
	kReqMailsReason_Login,
	kReqMailsReason_Recever,
	kReqMailsReason_ServerMail,
};

struct MODI_S2RDB_Request_LoadMaillist : public MODI_S2RDB_Cmd
{
	char 						m_szName[ROLE_NAME_MAX_LEN + 1];
	enReqMailsReason 			m_reason;
	MODI_MailInfo 				m_mail;

	MODI_S2RDB_Request_LoadMaillist()
	{
		memset( m_szName ,  0 , sizeof(m_szName) );
		m_reason = kReqMailsReason_Login;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 30;
};

struct MODI_RDB2S_Notify_LoadMaillist : public MODI_S2RDB_Cmd
{
	enum enResult
	{
		kSuccessful,
		kLoadFaild,
		kNoChar,
	};

	enResult 					m_result;
	char 						m_szName[ROLE_NAME_MAX_LEN + 1];
	enReqMailsReason 			m_reason;
	MODI_DBMaillist 			m_mails;
	MODI_MailInfo 				m_mail;

	MODI_RDB2S_Notify_LoadMaillist()
	{
		m_result = kLoadFaild;
		memset( m_szName ,  0 , sizeof(m_szName) );
		m_reason = kReqMailsReason_Login;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 31;
};

struct MODI_S2RDB_Request_SaveMail : public MODI_S2RDB_Cmd
{
	MODI_DBMailInfo 		m_mail;

	MODI_S2RDB_Request_SaveMail()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 50;
};

struct MODI_S2RDB_Request_LoadRanks : public MODI_S2RDB_Cmd
{
	QWORD 	version;

	MODI_S2RDB_Request_LoadRanks()
	{
		version = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 51;
};

struct MODI_RDB2S_Notify_LoadRanksRes : public MODI_S2RDB_Cmd
{
	QWORD 	version;
	enRankType 	type;
	char 		data[0];

	MODI_RDB2S_Notify_LoadRanksRes()
	{
		version = 0;
		type = kRank_Begin;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 52;
};

struct MODI_RDB2S_Notify_LoadSelfRanksRes : public MODI_S2RDB_Cmd
{
	enRankType 	type;
	size_t 		size;
	char 		data[0];

	MODI_RDB2S_Notify_LoadSelfRanksRes()
	{
		type = kRank_Begin;
		size = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 53;
};

struct MODI_S2RDB_Request_UpdateLastLogin : public MODI_S2RDB_Cmd
{
	MODI_CHARID 				m_charid;
	time_t 						m_Time;
	char 						m_szIP[32];

	MODI_S2RDB_Request_UpdateLastLogin()
	{
		m_charid = INVAILD_CHARID; 
		memset( m_szIP , 0  , sizeof(m_szIP) );
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 200;
};

struct MODI_S2RDB_Request_ResetTodayOnlineTime : public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ResetTodayOnlineTime()
	{
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 201;
};

struct MODI_S2RDB_Request_ExchangeByYunyingKey : public MODI_S2RDB_Cmd
{
	MODI_CHARID 		m_charid;
	char 				m_szKey[YUNYING_CDKEY_MAXSIZE + 1];
	BYTE 				m_serverid;

	MODI_S2RDB_Request_ExchangeByYunyingKey()
	{
		m_charid = INVAILD_CHARID;
		memset( m_szKey , 0 , sizeof(m_szKey) );
		m_serverid = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 202;
};

struct MODI_RDB2S_Notify_ExchangeByYunyingKeyRes : public MODI_S2RDB_Cmd 
{
	MODI_CHARID 		m_charid;
	char 				m_szKey[YUNYING_CDKEY_MAXSIZE + 1];
	enYunYingKeyExchangeResult 	m_result;
	MODI_YunYingCDKeyContent 	m_content;
	BYTE 				m_serverid;

	MODI_RDB2S_Notify_ExchangeByYunyingKeyRes()
	{
		m_charid = INVAILD_CHARID;
		memset( m_szKey , 0 , sizeof(m_szKey) );
		m_result = kYYKeyExchange_Invalid;
		m_serverid = 0;
		byParam = ms_SubCmd;
	}
	static const unsigned char ms_SubCmd = 203;
};

struct MODI_S2RDB_Request_SaveYunYingKey : public MODI_S2RDB_Cmd
{
	MODI_CHARID 	m_charid;
	char 			m_szKey[YUNYING_CDKEY_MAXSIZE + 1];

	MODI_S2RDB_Request_SaveYunYingKey()
	{
		m_charid = INVAILD_CHARID;
		memset( m_szKey , 0 , sizeof(m_szKey) );
		byParam = ms_SubCmd;
	}

	static const unsigned char ms_SubCmd = 204;
};

struct MODI_S2RDB_Request_ClearZero : public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ClearZero()
	{
		byParam = ms_SubCmd;
		m_byState = 0;
		//m_dwAccountID = INVALID_ACCOUNT_ID;
		m_dwAccountID = 0;
		m_dwMoney = 0;
	}
	BYTE m_byState;
	defAccountID m_dwAccountID;
	DWORD m_dwMoney;
	static const unsigned char 	ms_SubCmd = 205;
};

/// 请求增加音乐的点击率
struct MODI_S2RDB_Notify_MusicCount : public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Notify_MusicCount()
	{
		byParam = ms_SubCmd;
		m_wdMusicID = 0;
		m_byState = 0;
	}
	BYTE m_byState;
	WORD m_wdMusicID;
	static const unsigned char 	ms_SubCmd = 206;
};

/// 返回变量数据,size不能大于1600
struct MODI_RDB2S_Return_LoadZoneUserVar_Cmd: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Return_LoadZoneUserVar_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		m_byServerID = 0;
		m_wdSize = 0;
	}
	defAccountID m_dwAccountID;
	BYTE m_byServerID;
	WORD m_wdSize;
	MODI_VarData m_pData[0];
	static const BYTE ms_SubCmd = 209;
};


/// 请求加载变量
struct MODI_S2RDB_Request_LoadUserVar_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_LoadUserVar_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		m_byServerID = 0;
	}
	
	defAccountID m_dwAccountID;
	BYTE m_byServerID;

	static const BYTE ms_SubCmd = 208;
};


/// 返回变量数据,size不能大于1600
struct MODI_RDB2S_Return_LoadUserVar_Cmd: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Return_LoadUserVar_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		m_byServerID = 0;
		m_wdSize = 0;
		m_blIsLoadEnd = 0;
	}
	defAccountID m_dwAccountID;
	BYTE m_byServerID;
	BYTE m_blIsLoadEnd;
	WORD m_wdSize;
	MODI_VarData m_pData[0];
	static const BYTE ms_SubCmd = 209;
};


/** 
 * @brief zoneservice 通知执行sql语句
 * 
 */
struct MODI_S2RDB_Request_ExecSql_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ExecSql_Cmd()
	{
		m_wdSize = 0;
		byParam = ms_SubCmd;
	}

	const WORD GetSize() const 
	{
		return m_wdSize + sizeof(MODI_S2RDB_Request_ExecSql_Cmd);
	}
	
	/// sql 的长度
	WORD m_wdSize;

	/// sql语句
	char m_Sql[0];
	
	static const BYTE ms_SubCmd = 210;
};


/** 
 * @brief 事务过程中的某条语句
 * 
 */
struct MODI_Transaction_Sql
{
	MODI_Transaction_Sql()
	{
		m_wdSize = 0;
	}
	const WORD GetSize() const
	{
		return m_wdSize + sizeof(MODI_Transaction_Sql);
	}
	
	WORD m_wdSize;
	char m_Sql[0];
};


/** 
 * @brief 事务
 * 
 */
struct MODI_S2RDB_Request_ExecSql_Transaction_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ExecSql_Transaction_Cmd()
	{
		m_byCount = 0;
		byParam = ms_SubCmd;
	}
	
	/// 多少条语句
	BYTE m_byCount;

	/// 语句
	char m_Data[0];
	
	static const BYTE ms_SubCmd = 211;
};


/** 
 * @brief 加载zone_user_var
 * 
 */
struct MODI_S2RDB_Request_ZoneUserVar_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ZoneUserVar_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
	}
	
	defAccountID m_dwAccountID;
	static const BYTE ms_SubCmd = 212;
};

/// 返回zone_user_var数据,size不能大于1600
struct MODI_RDB2S_Return_ZoneUserVar_Cmd: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Return_ZoneUserVar_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		m_wdSize = 0;
	}
	
	defAccountID m_dwAccountID;
	WORD m_wdSize;
	MODI_VarData m_pData[0];
	static const BYTE ms_SubCmd = 213;
};


/// gameservice直接申请包裹信息
struct MODI_S2RDB_Request_ItemInfo: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Request_ItemInfo()
	{
		byParam = ms_SubCmd;
		m_dwAccountID = 0;
		m_byServerID = 0;
	}

	defAccountID m_dwAccountID;
	BYTE m_byServerID;
	static const BYTE ms_SubCmd = 214;
};


/// db直接给gameservice的包裹信息
struct MODI_RDB2S_Notify_ItemInfo: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Notify_ItemInfo()
	{
        byParam = ms_SubCmd;
        m_dwAccountID = 0;
        m_byServerID = 0;
        m_wdSize = 0;
		m_byIsLoadEnd = 0;
    }
	
    defAccountID m_dwAccountID;
    BYTE m_byServerID;
	BYTE m_byIsLoadEnd;
    WORD m_wdSize;
    MODI_DBItemInfo m_pData[0];
	
	static const BYTE ms_SubCmd = 215;
};

/// 减钱
struct MODI_S2RDB_DecMoney_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_DecMoney_Cmd()
	{
		m_byMoneyType = 0;
		m_dwMoney = 0;
		m_dwAccountID = 0;
		byParam = ms_SubCmd;
	}
	/// 1金币,2人民币
	BYTE m_byMoneyType;
	DWORD m_dwMoney;
	defAccountID m_dwAccountID;
	static const BYTE ms_SubCmd = 216;
};


/// 购买东西特殊处理
struct MODI_S2RDB_BuyItem_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_BuyItem_Cmd()
	{
		m_wdSaveItemSize = 0;
		m_wdRecordSize = 0;
		memset(m_strSaveItem, 0, sizeof(m_strSaveItem));
		memset(m_strBuyRecord, 0, sizeof(m_strBuyRecord));
		m_byMoneyType = 0;
		m_dwMoney = 0;
		m_dwAccountID = 0;
		byParam = ms_SubCmd;
		m_qdBuySerial = 0;
		m_qdRecordSerial = 0;
	}
	/// 道具
	WORD m_wdSaveItemSize;
	char m_strSaveItem[1024];
	
	/// 历史记录
	WORD m_wdRecordSize;
	char m_strBuyRecord[1024];

	/// 钱
	/// 1金币,2人民币
	BYTE m_byMoneyType;
	DWORD m_dwMoney;
	defAccountID m_dwAccountID;
	QWORD m_qdBuySerial;
	QWORD m_qdRecordSerial;
	static const BYTE ms_SubCmd = 217;
};


/// 家族升级 
struct MODI_S2RDB_FamilyLevel_Cmd: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_FamilyLevel_Cmd()
	{
		m_dwAccountID = 0;
		m_dwFamilyID = 0;
		m_byLevel = 0;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		byParam = ms_SubCmd;
	}

	DWORD m_dwFamilyID;
	BYTE m_byLevel;
	defAccountID m_dwAccountID;
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
	static const BYTE ms_SubCmd = 218;
};


/// 家族升级返回
struct MODI_RDB2S_Notify_FamilyLevel: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Notify_FamilyLevel()
	{
		m_byResult = 0;
		m_dwFamilyID = 0;
		m_dwAccountID = 0;
		byParam = ms_SubCmd;
	}
	
	/// 服务器返回结果
	BYTE m_byResult;
	
	/// 家族id
	DWORD m_dwFamilyID;

	/// 谁
	DWORD m_dwAccountID;
	
	static const BYTE ms_SubCmd = 219;
};

/// 获取网页道具
struct MODI_S2RDB_Req_WebItem: public MODI_S2RDB_Cmd
{
	MODI_S2RDB_Req_WebItem()
	{
		m_dwAccountId = 0;
		m_byServerId = 0;
		byParam = ms_SubCmd;
	}
	BYTE m_byServerId;
	DWORD m_dwAccountId;
	static const BYTE ms_SubCmd = 220;
};


/// 网页道具购买信息
struct MODI_WebItemInfo: public MODI_ShopGood
{
	MODI_WebItemInfo()
	{
		m_qdRecordSerial = 0;
		m_qdBuySerial = 0;
		m_byAction = 0;
		m_dwMoney = 0;
	}
	
	QWORD m_qdRecordSerial;
	QWORD m_qdBuySerial;
	BYTE m_byAction;
	DWORD m_dwMoney;
};


/// 返回道具信息
struct MODI_RDB2S_Ret_WebItem: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Ret_WebItem()
	{
		m_byServerId = 0;
		m_dwAccountId = 0;
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}

	BYTE m_byServerId;
	DWORD m_dwAccountId;
	WORD m_wdSize;
	MODI_WebItemInfo m_pData[0];
	static const BYTE ms_SubCmd = 221;
};

///新增单曲音乐记录信息
struct MODI_S2RDB_NewSingleMusic : public MODI_S2RDB_Cmd
{
	
	struct SingleMusic   m_stMusic;
	MODI_S2RDB_NewSingleMusic()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 222;
};

/// 同步刷新音乐记录表
struct MODI_S2RDB_FlushSingleMusicList : public MODI_S2RDB_Cmd
{
	WORD	m_wNum;
	struct SingleMusic   m_stMusic[0];
	MODI_S2RDB_FlushSingleMusicList()
	{
		m_wNum= 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 223;
};

///  DB加载完成RELATION 
//
struct MODI_RDB2S_RelationLoad : MODI_S2RDB_Cmd
{
	WORD	m_wNum;
	BYTE m_byIsLoadEnd;
	MODI_CHARID  m_id;
	MODI_DBRelationInfo  m_info[0];
		
	MODI_RDB2S_RelationLoad()
	{
		m_wNum = 0;
		m_byIsLoadEnd = 0;
		m_id = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 222;
};

///	DB保存社会关系

struct	MODI_RDBS2S_SaveRelation :MODI_S2RDB_Cmd
{

	WORD	m_wNum;
	MODI_DBRelationInfo  m_info[0];

	MODI_RDBS2S_SaveRelation()
	{
		m_wNum = 0;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 223;
};


/// 网页赠送信息
struct MODI_WebPresentInfo
{
	MODI_WebPresentInfo()
	{
		m_dwItemId = 0;
		m_wdNum = 0;
		m_byLimit = 0;
		m_qdSerial = 0;
	}
	QWORD m_qdSerial;
	DWORD m_dwItemId;
	WORD m_wdNum;
	BYTE m_byLimit;
};
	

/// 网页增送的道具
struct MODI_RDB2S_Ret_WebPresent: public MODI_S2RDB_Cmd
{
	MODI_RDB2S_Ret_WebPresent()
	{
		m_byServerId = 0;
		m_dwAccountId = 0;
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}

	BYTE m_byServerId;
	DWORD m_dwAccountId;
	WORD m_wdSize;
	MODI_WebPresentInfo m_pData[0];
	static const BYTE ms_SubCmd = 224;
};

/// 条件发送mails
struct MODI_SendMailByCondition_Cmd: public MODI_S2RDB_Cmd
{
	struct MODI_GetRoleInfo
	{
		MODI_GetRoleInfo()
		{
			memset(m_cstrName, 0, sizeof(m_cstrName));	
		}
		char m_cstrName[ROLE_NAME_MAX_LEN + 1];
	};
	
	MODI_SendMailByCondition_Cmd()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}
	
	MODI_Unify2S_SendMail_Cmd m_stMailCmd;
	WORD m_wdSize;
	MODI_GetRoleInfo m_pData[0];
	
	static const BYTE ms_SubCmd = 225;
};

#pragma pack(pop)

#endif



