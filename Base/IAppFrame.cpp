#include "IAppFrame.h"
#include "Service.h"
#include "sessionid_creator.h"
#include "Base/GameConstant.h"

const char * MODI_IAppFrame::ms_ConfigPath = "./Config/config.xml";


const char * gs_MODILogo = "\n \
MM   MM  MMMM  MMMMM   MM  \n \
MM   MM MM  MM MM   MM MM	\n \
MMM MMM MM  MM MM   MM MM  \n \
MM M MM MM  MM MM   MM MM  \n \
MM M MM MM  MM MM   MM MM  \n \
MM M MM MM  MM MM   MM MM  \n \
MM   MM MM  MM MM   MM MM  \n \
MM   MM MM  MM MM   MM MM  \n \
MM   MM  MMMM  MMMMM   MM  \n \
copyright@ 2010 modi \n \
http://www.modi.com \n";


MODI_IAppFrame::MODI_IAppFrame( const char * szAppName ):m_pLogger(0),
m_strAppName( szAppName ),
m_pNetService(0),
m_pLogicThread(0)
{
	//memset( m_szPidFile , 0, sizeof(m_szPidFile) );
}

MODI_IAppFrame::~MODI_IAppFrame() 
{

}




int 		MODI_IAppFrame::Init()
{
	MODI_GameConstant::Initial();

	// 初始化日志对象
	m_pLogger = new MODI_Logger(m_strAppName.c_str());
	if( !m_pLogger )
		return MODI_IAppFrame::enNoMemory;

	Global::logger = m_pLogger;
	Global::logger->info("[%s] Initialization %s  ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
	std::string net_log_name = "NET_" + m_strAppName;
	Global::net_logger = new MODI_Logger(net_log_name.c_str());
	Global::logger->SetLevel("debug");
	
#ifdef _FB_LOGGER
	std::string fb_log_name = "";
	Global::fb_logger =  new MODI_Logger(fb_log_name.c_str());
	Global::fb_logger ->SetLevel("debug");
#endif

// 	char szNoBlankName[512];
// 	memset( szNoBlankName ,  0 , sizeof(szNoBlankName) );
// 	size_t n = 0;
// 	for( size_t i = 0 ; i < m_strAppName.size(); i++ )
// 	{
// 		if( m_strAppName.c_str()[i] != ' ' && m_strAppName.c_str()[i] != '.' )
// 		{
// 			szNoBlankName[n] = m_strAppName.c_str()[i];
// 			n++;
// 		}
// 	}
// 	szNoBlankName[n] = '\0';

// 	SNPRINTF( m_szPidFile , sizeof(m_szPidFile) , "%s.modipid" , szNoBlankName );
	// 加载配置文件
	if( !MODI_ServerConfig::GetInstance().Load( MODI_IAppFrame::ms_ConfigPath ) )
	{
		Global::logger->fatal("[%s] Server<%s> Load Config falid .", SYS_INIT , 
				m_strAppName.c_str() );
		return MODI_IAppFrame::enLoadConfigFaild;
	}

	if( !m_timeManager.Init() )
	{
		Global::logger->info("[%s] Server<%s> Initial TimeManager Faild .", SYS_INIT , 
				m_strAppName.c_str() );
		return MODI_IAppFrame::enInitTimeManagerFaild;
	}

	return MODI_IAppFrame::enOK;
}

void 		MODI_IAppFrame::RunDaemonIfSet()
{
	// 解析命令参数
	// delete by tt
	// 目前该方法有问题，脚本使用& 方式，将程序切换到后台运行
//	if( Global::Value["daemon"] == "true" )
//	{
//		Global::logger->debug("[%s] %s run daemon mode." , LOGACT_SYS_INIT , m_strAppName.c_str() );
		Global::logger->RemoveConsoleLog();
//		daemon(1,1);
//	}
}

void 	MODI_IAppFrame::OnSignal_User1() 
{
	Global::logger->info("[%s] catch a signal : SIGUSR1." , SYSTEM_SIGNAL );
}

void 	MODI_IAppFrame::OnSignal_User2() 
{
	Global::logger->debug("[%s] catch a signal : SIGUSR2." , SYSTEM_SIGNAL );
}

void 	MODI_IAppFrame::OnSignal_Int() 
{
	Global::logger->debug("[%s] catch a signal : SIGINT." , SYSTEM_SIGNAL );
	Terminate();
}

void 	MODI_IAppFrame::OnSignal_Term()
{
	Global::logger->debug("[%s]  catch a signal : SIGTERM." , SYSTEM_SIGNAL );
	Terminate();
}

void 	MODI_IAppFrame::Terminate()
{
	if( m_pNetService )
		m_pNetService->Terminate();
}

void	MODI_IAppFrame::ReloadConfig()
{
	if(m_pNetService)
		m_pNetService->ReloadConfig();
}

// bool 	MODI_IAppFrame::SavePidFile()
// {
// 	if(!m_szPidFile[0])
// 		return false;

// 	if( ::SavePidFile( m_szPidFile , getpid() ) == false )
// 	{
// 		Global::logger->fatal("[%s] Could not open the pid file %s for writing.", SYS_INIT , 
// 				m_szPidFile );
//         return false ;
//     }
// 	return true;
// }

// void 	MODI_IAppFrame::RemovePidFile()
// {
// 	if(!m_szPidFile[0])
//       return;

// 	if( ::RemovePidFile( m_szPidFile ) == false )
// 	{
// 		Global::logger->warn("[%s] Could not remove the pid file %s", SYS_INIT , m_szPidFile);
// 	}
// }

