/**
 * @file GS2DBCommand.h
 * @date 2010-01-18 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库相关命令
 *
 */

#include "DBCommand.h"

#ifndef MDGS2DBCOMMAND_H
#define MDGS2DBCOMMAND_H

/****** 角色相关 **********/
const BYTE GS2DB_ROLE_CMD = 1;
const BYTE GS2DB_HOME_CMD = 2;


/// 保存角色信息
const BYTE GS2DB_SAVE_ROLEINFO_PARA = 1;
struct MODI_GS2DBSaveRoleInfo: public Cmd::stNullCmd
{
	MODI_GS2DBSaveRoleInfo()
	{
		byCmd = GS2DB_ROLE_CMD;
		byParam = GS2DB_SAVE_ROLEINFO_PARA;
	}
	/// MODI_RoleInfo;
}__attribute__((packed));

#endif
