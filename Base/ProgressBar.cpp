#include "ProgressBar.h"
#include "Global.h"
#include <assert.h>
#include <stdarg.h>

MODI_BarGoLink::~MODI_BarGoLink()
{
	Format("\n");
	Flush();
}

void MODI_BarGoLink::Flush()
{
	fflush(stdout);
}

void MODI_BarGoLink::Format(const char * szFormat , ...)
{
	char szTemp[1024];
	va_list argptr;
	va_start(argptr,szFormat);
	int iChars = vsnprintf( szTemp , sizeof(szTemp) , szFormat , argptr );
	va_end(argptr);
	if( iChars == -1 || iChars > (int)sizeof(szTemp) )
	{
		assert(0);
	}

	PutOut( szTemp );
}

void MODI_BarGoLink::PutOut(const char * szOut)
{
	printf( szOut );
}

MODI_BarGoLink::MODI_BarGoLink( int row_count )
{
    rec_no    = 0;
    rec_pos   = 0;
    indic_len = 50;
    num_rec   = row_count;
    empty     = " ";
    #ifdef _WIN32
    full      = "\x3D";
    #else
    full      = "*";
    #endif
    #ifdef _WIN32
    Format( "\x3D" );
    #else
    Format( "[" );
    #endif
    for ( int i = 0; i < indic_len; i++ ) Format( empty );
    #ifdef _WIN32
    Format( "\x3D 0%%\r\x3D" );
    #else
    Format( "] 0%%\r[" );
    #endif
	Flush();
}

void MODI_BarGoLink::step( void )
{
    int i, n;

    if ( num_rec == 0 ) return;
    rec_no++;
    n = rec_no * indic_len / num_rec;
    if ( n != rec_pos )
    {
        #ifdef _WIN32
        Format( "\r\x3D" );
        #else
        Format( "\r[" );
        #endif
        for ( i = 0; i < n; i++ ) Format( full );
        for ( ; i < indic_len; i++ ) Format( empty );
        float percent = (((float)n/(float)indic_len)*100);
        #ifdef _WIN32
        Format( "\x3D %i%%  \r\x3D", (int)percent);
        #else
        Format( "] %i%%  \r[", (int)percent);
        #endif
		Flush();

        rec_pos = n;
    }
}



void MODI_BarGoLink_Logger::PutOut(const char * szOut)
{
	strcat( m_szTempOut , szOut );
}

void MODI_BarGoLink_Logger::Flush()
{
	Global::logger->info( "[%s] %s " , "process bar" ,  m_szTempOut );
}

MODI_BarGoLink_Logger::MODI_BarGoLink_Logger( int nStep ):MODI_BarGoLink( nStep )
{

}

MODI_BarGoLink_Logger::~MODI_BarGoLink_Logger()
{

}
