/** 
 * @file BitArray.h
 * @brief λ����
 * @author Tang Teng
 * @version v0.1
 * @date 2011-01-05
 */


#ifndef BIT_ARRAY_H_
#define BIT_ARRAY_H_

#include <memory>
#include "Base/AssertEx.h"

class MODI_BitArray
{

public:

	class MODI_BitArrayProxy
	{

	public:

		MODI_BitArrayProxy(MODI_BitArray & BitArray,unsigned int nPos);

		MODI_BitArrayProxy & operator = (bool bValue)
		{
			m_BitArray.Set(m_dwPos,bValue);
			return *this;
		}

		MODI_BitArrayProxy & operator = (MODI_BitArrayProxy & Proxy)
		{
			m_BitArray.Set(m_dwPos,Proxy.m_BitArray.IsBitSet(m_dwPos));
			return *this;
		}

		operator bool () const 
		{
			return m_BitArray.IsBitSet(m_dwPos);
		}

		bool Toggle() 
		{
			m_BitArray.ToggleBit(m_dwPos);
			return m_BitArray.IsBitSet(m_dwPos);
		}


	private:

		MODI_BitArray &		m_BitArray;
		unsigned int			m_dwPos;

	};

	MODI_BitArray();
	explicit MODI_BitArray(unsigned int dwSize);
	virtual ~MODI_BitArray();
	MODI_BitArray(const MODI_BitArray & BitArray);

	void	Create( unsigned int nSize );

	void 	CreateFromBuf( const char * src_buf , unsigned int src_byte_size );

	bool 	CopyTo( char * det_buf , unsigned int & des_byte_size );

	bool		operator ==	(const MODI_BitArray & BitArray) const;
	bool		operator != (const MODI_BitArray & BitArray) const;
	MODI_BitArray &	operator =	(const MODI_BitArray & BitArray);

	MODI_BitArrayProxy  operator [] (unsigned int dwPos) 
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		return MODI_BitArrayProxy(*this,dwPos);
	}

	const MODI_BitArrayProxy  operator [] (unsigned int dwPos) const
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		return MODI_BitArrayProxy(const_cast<MODI_BitArray &>(*this),dwPos);
	}

	MODI_BitArray & operator &=	(MODI_BitArray & BitArray)
	{
		MODI_ASSERT(m_dwBitsNum == BitArray.m_dwBitsNum);
		for(unsigned int i = 0; i < m_dwLenght; i++)
		{
			m_pBuffers[i] &= BitArray.m_pBuffers[i];
		}
		return *this;
	}

	MODI_BitArray & operator |= (MODI_BitArray & BitArray)
	{
		MODI_ASSERT(m_dwBitsNum == BitArray.m_dwBitsNum);
		for(unsigned int i = 0; i < m_dwLenght; i++)
		{
			m_pBuffers[i] |= BitArray.m_pBuffers[i];
		}
		return *this;
	}

	MODI_BitArray & operator ^= (MODI_BitArray & BitArray)
	{
		MODI_ASSERT(m_dwBitsNum == BitArray.m_dwBitsNum);
		for(unsigned int i = 0; i < m_dwLenght; i++)
		{
			m_pBuffers[i] ^= BitArray.m_pBuffers[i];
		}
		return *this;
	}

	MODI_BitArray operator ~() const
	{
		return MODI_BitArray(*this).ToggleBitAllBits();
	}

	friend	MODI_BitArray operator & (MODI_BitArray & BitArray1,MODI_BitArray & BitArray2)
	{
		MODI_BitArray temp(BitArray1);
		return temp &= BitArray2;
	}

	friend  MODI_BitArray operator | (MODI_BitArray & BitArray1,MODI_BitArray & BitArray2)
	{
		MODI_BitArray temp(BitArray1);
		return temp |= BitArray2;
	}

	friend	MODI_BitArray operator ^ (MODI_BitArray & BitArray1,MODI_BitArray & BitArray2)
	{
		MODI_BitArray temp(BitArray1);
		return temp ^= BitArray2;
	}

	void	Clear();

	bool	IsAllBitsFalse()	const
	{
		for(unsigned int i = 0; i < m_dwLenght; i++)
		{
			if(m_pBuffers[i])
				return false;
		}

		return true;
	}

	bool	IsBitSet(unsigned int dwPos)	const
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		return (m_pBuffers[GetIndex(dwPos)] & (1 << GetOffSetBits(dwPos))) != 0;
	}

	void	Set(unsigned int dwPos,bool bValue)
	{
		bValue ? SetBit(dwPos) : ClearBit(dwPos);
	}

	void	ClearBit(unsigned int dwPos)
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		m_pBuffers[GetIndex(dwPos)] &= ~(1 << GetOffSetBits(dwPos));
	}

	void	SetBit(unsigned int dwPos)
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		m_pBuffers[GetIndex(dwPos)] |= (1 << GetOffSetBits(dwPos));
	}

	void	ToggleBit(unsigned int dwPos)
	{
		MODI_ASSERT(dwPos < m_dwBitsNum);
		m_pBuffers[GetIndex(dwPos)]  ^= (1 << GetOffSetBits(dwPos));
	}
	
	MODI_BitArray &	ToggleBitAllBits()
	{
		for(unsigned int i = 0; i < m_dwLenght; i++)
		{
			m_pBuffers[i] = ~m_pBuffers[i];
		}
		Trim();
		return *this;
	}

	unsigned int	GetBitSize() const
	{
		return m_dwBitsNum;
	}

	unsigned int *	GetBits() const
	{
		return m_pBuffers;
	}

	unsigned int	GetLenght() const
	{
		return m_dwLenght;
	}

private:

	void	Reset();
	void	OnInit(unsigned int dwSize);
	void	Trim();
	static	unsigned int	GetIndex(unsigned int dwPos)		{ return dwPos / PER_CELL_SIZE; }
	static	unsigned int	GetOffSetBits(unsigned int dwPos)	{ return dwPos % PER_CELL_SIZE; }

private:

	unsigned int	*	m_pBuffers;
	unsigned int		m_dwLenght;
	unsigned int		m_dwBitsNum;


private:

	static const unsigned int PER_BIT_BYTES;
	static const unsigned int PER_CELL_SIZE;

};




#endif
