/**
 * @file   ServiceTaskSched.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Feb 23 15:14:18 2010
 * @version Ver0.1
 * @brief  连接任务调度
 * 
 */

#ifndef _MDSERVICETASKSCHED_H
#define _MDSERVICETASKSCHED_H

#include "Global.h"
#include "NormalSched.h"
#include "RecycleSched.h"


/**
 * @brief 服务器连接管理
 * 分散到各个线程组管理
 *
 */
class MODI_ServiceTaskSched: public MODI_DisableCopy
{
 public:
	MODI_ServiceTaskSched(const WORD normal_thread_num = 1,
						  const WORD recycle_thread_num = 1):
	   	m_stNormalSched(this, normal_thread_num),
	   	m_stRecycleSched(this, recycle_thread_num)
	{
		
	}
		
	/** 
	 * @brief 初始化
	 *
	 * @return 成功true,失败false
	 */
	bool Init();

	/** 
	 * @brief 结束,释放资源
	 * 
	 */
	void Final();

	/** 
	 * @brief 获取连接总数
	 * 
	 * @return 连接数
	 */
	int Size();

	/** 
	 * @brief 获取正常队列连接数量
	 * 
	 * @return 连接数
	 */
	int GetNormalSize();

	/** 
	 * @brief 获取回收队列连接数量 
	 * 
	 * @return 连接数
	 */
	int GetRecycleSize();

	/** 
	 * @brief 连接加入正常管理队列 
	 * 
	 * @param p_task 要加入的连接
	 * 
	 * @return 成功返回true,失败返回false
	 */
	bool AddNormalSched(MODI_ServiceTask * p_task);

	/** 
	 * @brief 连接加入回收管理队列
	 * 
	 * @param p_task 要回收的连接 
	 * 
	 * @return 成功返回true,失败返回false
	 */
	bool AddRecycleSched(MODI_ServiceTask * p_task);

 private:
	/// 正常管理
   	MODI_NormalSched m_stNormalSched;

	/// 回收管理
   	MODI_RecycleSched m_stRecycleSched;
};

#endif


