/**
 * @file   InviteManager.cpp
 * @author  <hurixin@localhost.localdomain>
 * @date   Thu May  5 10:22:18 2011
 * 
 * @brief  邀请管理
 * 
 */

#include "AssertEx.h"
#include "InviteManager.h"

MODI_InviteManager * MODI_InviteManager::m_pInstance = NULL;

DWORD MODI_InviteManager::m_dwGenInviteID = 0;


/** 
 * @brief 增加一个邀请记录
 * 
 * @param invite_data 减少一个邀请记录
 * 
 */
bool MODI_InviteManager::AddInvite(MODI_InviteData * p_invite_data)
{
	std::pair<defInviteDataMapIter, bool > ret_code;
	ret_code = m_stInviteDataMap.insert(defInviteDataMapValue(p_invite_data->GetInviteID(), p_invite_data));
	return ret_code.second;
}


/** 
 * @brief 删除一个邀请
 * 
 */
void MODI_InviteManager::RemoveInvite(const DWORD & invite_id)
{
	defInviteDataMapIter iter = m_stInviteDataMap.find(invite_id);
	if(iter != m_stInviteDataMap.end())
	{
		if(iter->second)
			delete iter->second;
		iter->second = NULL;
		
		m_stInviteDataMap.erase(iter);
	}
}


/** 
 * @brief 查找一个邀请
 * 
 */
MODI_InviteData * MODI_InviteManager::FindInvite(const DWORD invite_id)
{
	MODI_InviteData * p_invite_data = NULL;
	defInviteDataMapIter iter = m_stInviteDataMap.find(invite_id);
	if(iter != m_stInviteDataMap.end())
	{
		p_invite_data = iter->second;
	}
	return p_invite_data;
}

/** 
 * @brief 更新
 * 
 */
void MODI_InviteManager::UpDate(const MODI_RTime & cur_time)
{
	if(m_stInviteDataMap.size() == 0)
		return;
	
	defInviteDataMapIter iter,next;
	MODI_InviteData * p_data = NULL;
	for(next=m_stInviteDataMap.begin(), iter=next; iter!=m_stInviteDataMap.end(); iter=next)
	{
		if(next != m_stInviteDataMap.end())
		{
			next++;
		}

		p_data = iter->second;
		if(p_data)
		{
			if(p_data->IsTimeout(cur_time))
			{
				Global::logger->debug("-->>>>%u-->>invite time out", iter->first);
				delete p_data;
				p_data = NULL;
				m_stInviteDataMap.erase(iter);
			}
		}
		else
		{
			MODI_ASSERT(0);
			m_stInviteDataMap.erase(iter);
		}
	}
}


