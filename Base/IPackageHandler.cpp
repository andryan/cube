#include "IPackageHandler.h"
#include "Global.h"
#include <assert.h>

MODI_IPackageHandler::MODI_IPackageHandler():m_bInit(false)
{
}

MODI_IPackageHandler::~MODI_IPackageHandler()
{

}

bool MODI_IPackageHandler::Initialization()
{
	if( m_bInit )
		return true;


	if( !this->FillPackageHandlerTable()  )
		return false;

	m_bInit = true;

	return true;
}

void MODI_IPackageHandler::AddPackageHandler(unsigned char ucCmd, unsigned char ucSubCmd,
        defPackageHandler h , const char * szCmdName )
{
    assert(h);
    unsigned short int nCmd = 0;
    nCmd |= ucCmd;
    nCmd <<= 8;
    nCmd |= ucSubCmd;
    AddPackageHandler(nCmd, h , szCmdName );
}

void MODI_IPackageHandler::AddPackageHandler(unsigned short int nCmd, defPackageHandler h, const char * szCmdName)
{
    assert(h);
    MAP_PACKAGEHANDLER_INSERT_RESULT result = m_mapHandlers.insert(std::make_pair(nCmd, h));
    if (result.second == false)
    {
		Global::logger->fatal("[%s] dup reg packagehander nCmd = %u ." , "package_register" , 
				nCmd );

		assert(result.second);
        throw "reduplicate package handler .<MODI_IPackageHandler::AddPackageHandler>";
    }
	else if( szCmdName && szCmdName[0] )
	{
		MAP_CMDNAME_INSERT_RESULT res = m_mapCmdName.insert( std::make_pair(nCmd,szCmdName) );
		if( res.second == false )
		{
			Global::logger->fatal("[%s] dup reg CommandName nCmd = %u ." , "package_register" , 
					nCmd );

			assert(res.second);
			throw "reduplicate package name .<MODI_IPackageHandler::AddPackageHandler>";
		}
	}
}

int MODI_IPackageHandler::DoHandlePackage(unsigned short int nPackage,
        void * pClient, const Cmd::stNullCmd * pt_null_cmd,
        const unsigned int cmd_size)
{
    int iRet = enPHandler_Kick;
    MAP_PACKAGEHANDLER_ITER itor = m_mapHandlers.find(nPackage);
    if (itor != m_mapHandlers.end())
    {
        defPackageHandler callback = (itor->second);
        if (callback)
        {
            iRet = callback(pClient, pt_null_cmd, cmd_size);
        }
    }
    return iRet;
}

int MODI_IPackageHandler::DoHandlePackage(unsigned char ucCmd, unsigned char ucSubCmd,
        void * pClient, const Cmd::stNullCmd * pt_null_cmd,
        const unsigned int cmd_size)
{
    unsigned short int nCmd = 0;
    nCmd |= ucCmd;
    nCmd <<= 8;
    nCmd |= ucSubCmd;
    return DoHandlePackage(nCmd, pClient, pt_null_cmd, cmd_size);
}

const char * MODI_IPackageHandler::GetCmdName( unsigned char ucCmd , unsigned char ucSubCmd )
{
	static const char * szNullCmd = "Unknow CmdName";
    unsigned short int nCmd = 0;
    nCmd |= ucCmd;
    nCmd <<= 8;
    nCmd |= ucSubCmd;
	const char * szRet = szNullCmd;
	MAP_CMDNAME_C_ITER itor = m_mapCmdName.find( nCmd );
	if( itor != m_mapCmdName.end() )
	{
		szRet = itor->second.c_str();
	}
	return szRet;
}

