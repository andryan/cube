#include "SvrPackageProcessBase.h"
#include "nullCmd.h"
#include "Global.h"

MODI_IServerClientTask::~MODI_IServerClientTask()
{


}



MODI_IServerClientTask::MODI_IServerClientTask(const int sock, const struct sockaddr_in * addr):MODI_ServiceTask( sock , addr ),
	m_bCanDel( false )
{

}

MODI_SvrPackageProcessBase::MODI_SvrPackageProcessBase()
{

}

MODI_SvrPackageProcessBase::~MODI_SvrPackageProcessBase()
{


}


void 	MODI_SvrPackageProcessBase::OnAcceptConnection( const MODI_IServerClientTask * p )
{
	// 该方法只让连接线程调用
	MODI_RecurisveMutexScope lock(&m_mutexAddlist);
	m_setAddlist.push_back(p);
}

void 	MODI_SvrPackageProcessBase::ProcessAllPackage()
{
	// 添加新的TASK到主队列
	{
		MODI_RecurisveMutexScope lock(&m_mutexAddlist);
		SETS_ITER itor = m_setAddlist.begin();
		while( itor != m_setAddlist.end() )
		{
			MODI_IServerClientTask * p = (MODI_IServerClientTask *)(*itor);
			if( p )
			{
				p->OnConnection();
				m_setCmdParse.push_back( p );
			}
			itor++;
		}
		m_setAddlist.clear();
	}

	if( m_setCmdParse.empty() )
		return ;

	// 处理所有连接的数据包，包括无效的连接
	// (对端虽然断开，但还有可能有未处理的数据包，必须处理完，才删除该连接)
	{
		SETS_ITER itor = m_setCmdParse.begin();
		while( itor != m_setCmdParse.end() )
		{
			MODI_IServerClientTask * p = (MODI_IServerClientTask *)(*itor);
			if( p )
			{
				// 处理该连接上的所有数据包
				const unsigned int nMaxGet = 10;
				p->Get( nMaxGet );
			}
			itor++;
		}
	}

	// 从队列中删除所有无效的连接
	{
		SETS_ITER itor = m_setCmdParse.begin();

		while( itor != m_setCmdParse.end() )
		{
			SETS_ITER itornext = itor;
			itornext++;
			MODI_IServerClientTask * p = (MODI_IServerClientTask *)(*itor);
			if( p && p->IsTerminate() )
			{
				p->OnDisconnection();
				// 从主队列安全移出后，设置为可以删除。让回收线程去删除
				m_setCmdParse.erase( itor );
				p->SetCanDel( true );
			}
			itor = itornext;
		}
	}
}


//void	MODI_SvrPackageProcessBase::ExecAllPackage(MODI_PackageCallBack & call_back)
//{
//	SETS_ITER iter, next;
//	if(m_setCmdParse.size() == 0)
//		return;
//	for(iter=m_setCmdParse.begin(), next=iter, next++; iter!=m_setCmdParse.end();iter=next,next++)
//	{
//		if(! call_back.Done(*iter))
//		{
//			break;
//		}
//	}
//}
//
//
