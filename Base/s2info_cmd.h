/**
 * @file   s2info_cmd.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Feb 14 09:57:08 2011
 * 
 * @brief  游戏区到infoservice的命令
 * 
 */

#ifndef _MD_STOINFOCMD_H
#define _MD_STOINFOCMD_H


#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"

/// 之前没有统一，后期整理下
static const BYTE MAINCMD_S2INFO = 0x21;

/// 返回结果
enum enVerifyResult
{
	enVerifyNone = 0,
	enVerifyNoAcc = 1,  //无账号
	enVerifyPwdErr, //密码错误
	enVerifyNoActive, //未激活游戏
	enVerifyTimeout, //超时
	enVerifyFailed, //未知错误
	enVerifySuccess = 8,//成功		
	enVerifyNull,
};

class MODI_DBServerClient;
struct MODI_AccVerifyInPara
{
	//DWORD m_dwVerifySerial;
	MODI_DBServerClient * m_pSource;
	MODI_SessionID m_SessionID;
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; 
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1]; 
};

/// 输出参数
struct MODI_AccVerifyOutPara
{
	MODI_AccVerifyOutPara()
	{
		m_enResult = enVerifyNull;
		m_stAccountID = INVAILD_ACCOUNT_ID;
	}
	enVerifyResult m_enResult;
	defAccountID m_stAccountID;
};

struct MODI_S2Info_Cmd: public stNullCmd
{
	MODI_S2Info_Cmd()
	{
		byCmd = MAINCMD_S2INFO;
	}
}__attribute__((__packed__));

/// 用户登录效验
struct MODI_S2Info_Verfiy_Account: public MODI_S2Info_Cmd
{
	MODI_S2Info_Verfiy_Account()
	{
		byParam = ms_Param;
		memset( m_szAccountName , 0 , sizeof(m_szAccountName) );
		memset( m_szAccountPwd , 0 , sizeof(m_szAccountPwd) );
		m_wdZoneID = 0;
	}

	MODI_SessionID m_SessionID;
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; 
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1];
	WORD m_wdZoneID;
	static const BYTE ms_Param = 1;
};


/// 返回用户登录
struct MODI_Info2S_Verify_Result: public MODI_S2Info_Cmd
{
	MODI_Info2S_Verify_Result()
	{
		byParam = ms_Param;
		memset(m_cstrCardNum, 0, sizeof(m_cstrCardNum));
		m_enResult = enVerifyNone;
		m_dwAccID = INVAILD_ACCOUNT_ID;
	}

	MODI_SessionID m_SessionID;

	/// 是否防沉迷
	//BYTE m_blChenMi;
	char m_cstrCardNum[MAX_CARDNUM_LENGTH];

	/// 登录结果
	enVerifyResult m_enResult;

	/// 登录账号
	defAccountID m_dwAccID;

	static const BYTE ms_Param = 2;
};

#endif
