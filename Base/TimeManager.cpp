#include "TimeManager.h"

MODI_TimeManager::MODI_TimeManager( )
{
	m_CurrentTime = 0 ;
}

MODI_TimeManager::~MODI_TimeManager( )
{


}

bool MODI_TimeManager::Init( )
{
#if _WIN32_
	m_StartTime = GetTickCount() ;
	m_CurrentTime = GetTickCount() ;
#else
	m_StartTime		= 0;
	m_CurrentTime	= 0;
	gettimeofday(&_tstart, &tz);
#endif
	SetTime( );
	return true;
}

DWORD MODI_TimeManager::CurrentTime( )
{
#if defined(_WIN32_)
	m_CurrentTime = GetTickCount() ;
#else
	gettimeofday(&_tend,&tz);
	double t1, t2;
	t1 =  (double)_tstart.tv_sec*1000 + (double)_tstart.tv_usec/1000;
	t2 =  (double)_tend.tv_sec*1000 + (double)_tend.tv_usec/1000;
	m_CurrentTime = (DWORD)(t2-t1);
#endif
	return m_CurrentTime;
}

DWORD	MODI_TimeManager::CurrentDate()
{
	SetTime( ) ;
	DWORD Date;
	ConvertTU(&m_TM,Date);
	return Date;
}

void MODI_TimeManager::ConvertTU( time_t* t, DWORD& Date ) 
{
	tm* ptm = localtime( t ) ;
	ConvertTU( ptm ,Date);
}

void MODI_TimeManager::SetTime( )
{
	time( &m_SetTime ) ;
	tm* ptm = localtime( &m_SetTime ) ;
	m_TM = *ptm ;
}

// 得到标准时间
time_t MODI_TimeManager::GetANSITime( )
{
	SetTime();
	return m_SetTime;
}

DWORD MODI_TimeManager::Time2DWORD( )
{
	SetTime( );
	DWORD uRet=0;

	uRet += GetYear( ) ;
	uRet -= 2000 ;
	uRet =uRet*100 ;

	uRet += GetMonth( )+1 ;
	uRet =uRet*100 ;

	uRet += GetDay( ) ;
	uRet =uRet*100 ;

	uRet += GetHour( ) ;
	uRet =uRet*100 ;

	uRet += GetMinute( ) ;

	return uRet ;
}

void MODI_TimeManager::DWORD2Time( DWORD date , tm * t)
{
	DWORD dwYear =   date / 100000000;
	DWORD dwMonth = date % 100000000 / 1000000;
	DWORD dwDay =  date % 100000000 % 1000000  / 10000;
	DWORD dwHour = date % 100000000 % 1000000  % 10000 / 100;
	DWORD dwMin = date % 100;

	t->tm_year = 2000 + dwYear;
	t->tm_mon = dwMonth - 1;
	t->tm_mday = dwDay;
	t->tm_hour = dwHour;
	t->tm_min = dwMin;
}

DWORD MODI_TimeManager::DiffTime( DWORD Date1, DWORD Date2 )
{
	tm S_D1, S_D2 ;
	ConvertUT( Date1, &S_D1 ) ;
	ConvertUT( Date2, &S_D2 ) ;
	time_t t1,t2 ;
	t1 = mktime(&S_D1) ;
	t2 = mktime(&S_D2) ;
	DWORD dif = (DWORD)(difftime(t2,t1)*1000) ;
	return dif ;
}

time_t	MODI_TimeManager::DiffTimeT( time_t t1, time_t t2 ) 
{
	time_t dif = (time_t)(difftime(t2,t1)) ;
	return dif ;
}

void MODI_TimeManager::ConvertUT( DWORD Date, tm* TM )
{
	MODI_ASSERT(TM) ;
	memset( TM, 0, sizeof(tm) ) ;
	TM->tm_year = (Date>>26)&0xf ;
	TM->tm_mon  = (Date>>22)&0xf ;
	TM->tm_mday = (Date>>17)&0x1f ;
	TM->tm_hour = (Date>>12)&0x1f ;
	TM->tm_min  = (Date>>6) &0x3f ;
	TM->tm_sec  = (Date)    &0x3f ;

}

void MODI_TimeManager::ConvertTU( tm* TM, DWORD& Date )
{
	MODI_ASSERT( TM ) ;
	Date = 0 ;
	Date += (TM->tm_yday%10) & 0xf ;
	Date = (Date<<4) ;
	Date += TM->tm_mon & 0xf ;
	Date = (Date<<4) ;
	Date += TM->tm_mday & 0x1f ;
	Date = (Date<<5) ;
	Date += TM->tm_hour & 0x1f ;
	Date = (Date<<5) ;
	Date += TM->tm_min & 0x3f ;
	Date = (Date<<6) ;
	Date += TM->tm_sec & 0x3f ;
}

DWORD MODI_TimeManager::GetDayTime( )
{
	time_t st ;
	time( &st ) ;
	tm* ptm = localtime( &st ) ;

	DWORD uRet=0 ;

	uRet  = (ptm->tm_year-100)*1000 ;
	uRet += ptm->tm_yday ;

	return uRet ;
}

WORD MODI_TimeManager::GetTodayTime()
{
	time_t st ;
	time( &st ) ;
	tm* ptm = localtime( &m_SetTime ) ;

	WORD uRet=0 ;

	uRet  = ptm->tm_hour*100 ;
	uRet += ptm->tm_min ;

	return uRet ;
}

bool MODI_TimeManager::FormatTodayTime(WORD& nTime)
{
	bool ret = false;

	WORD wHour = nTime / 100;
	WORD wMin = nTime % 100;
	WORD wAddHour = 0;
	if( wMin > 59 )
	{
		wAddHour = wMin / 60;
		wMin = wMin % 60;
	}
	wHour += wAddHour;
	if( wHour > 23 )
	{
		ret = true;
		wHour = wHour % 60;
	}

	return ret;
}
