#include "sessionid_creator.h"
#include "Global.h"

MODI_SessionIDCreator::MODI_SessionIDCreator():m_nSeq( 1 )
{
}

void 	MODI_SessionIDCreator::Create( MODI_SessionID & id , DWORD nIP , WORD nPort )
{
	id.m_nIP = nIP;
	id.m_nPort = nPort;
	id.m_nSeqID = m_nSeq++;      // 序列ID
}
