/**
 * @file Type.h
 * @date 2009-12-04 CST
 * @version $Id $
 * @author hurixin hrixin@modi.com
 * @brief 一些基本的声明
 *
 */

#ifndef _MDTYPE_H
#define _MDTYPE_H

#include "BaseOS.h"
#include "nullCmd.h"
#include "session_id.h"

///	定义服务器类型
#define 	SVR_TYPE_NULL 			0
#define 	SVR_TYPE_GS				1		//	gameserver
#define 	SVR_TYPE_GW				2		//	gateserver
#define 	SVR_TYPE_ZS			    4		//	zoneserver
#define 	SVR_TYPE_LS				8		//	loginserver
#define 	SVR_TYPE_SM				16		//	servermgr
#define 	SVR_TYPE_DB				32		//	db proxy
#define 	SVR_TYPE_ALL		    (SVR_TYPE_GS | SVR_TYPE_GW | SVR_TYPE_ZS | SVR_TYPE_LS | SVR_TYPE_SM | SVR_TYPE_DB)

/// 当前版本
const BYTE CURRENT_VERSION = 1;



/**
 * @brief 禁止拷贝构造和赋值
 *
 */
class MODI_DisableCopy
{
 protected:
	MODI_DisableCopy(){}
	~MODI_DisableCopy(){}
 private:
	/// 禁用拷贝构造
	MODI_DisableCopy(const MODI_DisableCopy & class_data);
	/// 禁止重载赋值符合
	const MODI_DisableCopy & operator= (const MODI_DisableCopy & class_data);
};


#ifdef _LINUX

#include <ext/hash_map>

/**
 * @brief  string hash函数
 *
 */
struct str_hash: public std::unary_function<const std::string, size_t>
{
	const size_t operator()(const std::string & str) const 
	{
		return __gnu_cxx::__stl_hash_string(str.c_str());
	}
};

#endif

#endif

