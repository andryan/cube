/**
 * @file DBClient.cpp
 * @date 2009-01-08 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库操作
 *
 */

#include <errmsg.h>
#include "DBClient.h"
#include "LoggerInfo.h"
#include "AssertEx.h"


DWORD MODI_DBClient::MODI_DBClientHandleID = 0;

/**
 * @brief 执行sql语句
 *
 * @param sql 执行的sql语句
 * @param sql_len 语句的长度
 *
 * @return mysql_real_query执行成功返回0,如果出现错误返回非0
 *
 */
int MODI_DBClient::ExecSql(const char * sql, unsigned int sql_len)
{
	if(sql == NULL || sql_len == 0 || m_stMySql == NULL)
	{
		return -1;
	}


	int ret_code = 1;

#ifdef _MYSQL_DEBUG
	Global::logger->debug("[mysql_debug] execsql <sql=%s>",sql);
#endif	
	ret_code = mysql_real_query(m_stMySql, sql, sql_len);
	if(ret_code)
	{
		Global::logger->error("[run_mysql] exec mysql failed <sql=%s>", sql);
	}
	
	if(ret_code)
	{
		if(ret_code == CR_SERVER_GONE_ERROR || ret_code == CR_SERVER_LOST)
		{
			Global::logger->fatal("[run_mysql] mysql server lost");
		}

		m_lastErrorDes = mysql_error( m_stMySql );
		m_lastErrorNo = mysql_errno( m_stMySql );
	}
	
	return ret_code;
}

/** 
 * @brief 选择操作
 * 
 * @param record_container 记录存放容器
 * @param sql 要执行的sql语句
 * @param sql_len 语句长度
 * 
 * @return 符合条件的记录个数 
 */
int MODI_DBClient::ExecSelect(MODI_RecordContainer & record_container, const char * sql, unsigned int sql_len, bool is_new)
{
	if(ExecSql(sql, sql_len) != 0)
	{
		return -1;
	}

	MYSQL_RES * result = mysql_store_result(m_stMySql);
	if(result == NULL)
	{
#ifdef _MYSQL_DEBUG
		Global::logger->debug("[mysql_debug] not any record by select <sql=%s>",sql);
#endif
		return -1;
	}

	WORD count_num = mysql_num_rows(result);
	if(count_num == 0)
	{
#ifdef _MYSQL_DEBUG
		Global::logger->debug("[mysql_debug] not any record by select <sql=%s>",sql);
#endif
		mysql_free_result(result);
		return 0;
	}

#ifdef _MYSQL_DEBUG
	Global::logger->debug("[mysql_debug] select record <num=%u>", count_num);
#endif	

	MYSQL_FIELD * field_container = mysql_fetch_fields(result);
	WORD field_num = mysql_num_fields(result);

	if(MODI_RecordContainer::m_stRecordQueue->GetSize() <= count_num)
	{
		is_new = true;
	}

	
	MYSQL_ROW row;
	while((row = mysql_fetch_row(result)))
	{
		MODI_Record * record = NULL;
		if(is_new)
		{
			record = new MODI_Record;
		}
		else
		{
			record = MODI_RecordContainer::m_stRecordQueue->Get();
		}
		if(record == NULL)
		{
			Global::logger->fatal("[run_mysql] unable new a  record or not get record from queue");
			MODI_Assert(0);
			continue;
		}
		
		for(WORD i=0; i<field_num; i++)
		{
			if(row[i] != NULL)
			{
				switch(field_container[i].type)
				{
					case FIELD_TYPE_TINY:
					case FIELD_TYPE_SHORT:
					case FIELD_TYPE_LONG:
					case FIELD_TYPE_INT24:
					case FIELD_TYPE_LONGLONG:
					case FIELD_TYPE_FLOAT:
					case FIELD_TYPE_DOUBLE:
					case FIELD_TYPE_STRING:
					case FIELD_TYPE_VAR_STRING:
					case FIELD_TYPE_DATE:
					case FIELD_TYPE_DATETIME:
					case FIELD_TYPE_TIMESTAMP:
					{
						record->Put(field_container[i].name, row[i]);
					}
					break;
					default:
					{
						DWORD row_size = *((DWORD *)row[i]);
						record->Put(field_container[i].name, (const char *)(row[i]+4), row_size);
					}
					break;
				}
			}
		}
		record_container.SetClear(is_new);
		record_container.Put(record);
	}
	
	mysql_free_result(result);
	return record_container.Size();
}


/**
 * @brief 选择操作
 * @param record_container 记录存放的容器
 * @param p_table 要操作的表
 * @param select_field 要选择的字段,null为全部
 * @param where 条件
 *
 * @return 返回记录集个数
 *
 */
int MODI_DBClient::ExecSelect(MODI_RecordContainer & record_container,
							  MODI_TableStruct * p_table, const char * where, MODI_Record * select_field,  bool is_new)
{
	std::ostringstream os;
   	if(select_field == NULL)
	{
		os << "select * from " << p_table->GetName();
	}
	else
	{
		os << "select ";
		struct all_field_call_back: public MODI_FieldCallBack
		{
			~all_field_call_back(){};

			bool Exec(const std::string & field_name, const MODI_VarType & field_value)
			{
				m_stOut  << field_name << ",";
				return true;
			}
			std::ostringstream m_stOut;
		};

		all_field_call_back field_callback;
		select_field->ExecAllRecord(field_callback);
		os << field_callback.m_stOut.str();
		std::string exec_sql = os.str();
	   	exec_sql[exec_sql.size() - 1] = ' ';
		os.str("");
		os << exec_sql;
		os << "from " << "`" << p_table->GetName() << "`";
	}

	if(where)
	{
		os << " where " << where << ";";
	}
	else
	{
		os << ";";
	}

	if(ExecSql(os.str().c_str(), (DWORD)os.str().size()) != 0)
	{
		return -1;
	}

	MYSQL_RES * result = mysql_store_result(m_stMySql);
	if(result == NULL)
	{
#ifdef _MYSQL_DEBUG
		Global::logger->debug("[mysql_debug] not any record by select <sql=%s>",os.str().c_str());
#endif
		return 0;
	}

	WORD count_num = mysql_num_rows(result);
	if(count_num == 0)
	{
#ifdef _MYSQL_DEBUG
		Global::logger->debug("[mysql_debug] not any record by select <sql=%s>", os.str().c_str());
#endif
		mysql_free_result(result);
		return 0;
	}

#ifdef _MYSQL_DEBUG
	Global::logger->debug("[mysql_debug] select record <num=%u>",count_num);
#endif	

	MYSQL_FIELD * field_container = mysql_fetch_fields(result);
	WORD field_num = mysql_num_fields(result);

	if(MODI_RecordContainer::m_stRecordQueue->GetSize() <= count_num)
	{
		is_new = true;
	}
	
	MYSQL_ROW row;
	while((row = mysql_fetch_row(result)))
	{
		MODI_Record * record = NULL;
		if(is_new)
		{
			record = new MODI_Record;
		}
		else
		{
			record = MODI_RecordContainer::m_stRecordQueue->Get();
		}
		
		if(record == NULL)
		{
			Global::logger->fatal("[run_mysql] unable new a  record or not get record from queue");
			MODI_Assert(0);
			continue;
		}
		for(WORD i=0; i<field_num; i++)
		{
			if(row[i] != NULL)
			{
				switch(field_container[i].type)
				{
					case FIELD_TYPE_TINY:
					case FIELD_TYPE_SHORT:
					case FIELD_TYPE_LONG:
					case FIELD_TYPE_INT24:
					case FIELD_TYPE_LONGLONG:
					case FIELD_TYPE_FLOAT:
					case FIELD_TYPE_DOUBLE:
					case FIELD_TYPE_STRING:
					case FIELD_TYPE_VAR_STRING:
					case FIELD_TYPE_DATE:
					case FIELD_TYPE_DATETIME:
					case FIELD_TYPE_TIMESTAMP:
					{
						record->Put(field_container[i].name, row[i]);
					}
					break;
					default:
					{
						DWORD row_size = *((DWORD *)row[i]);
						record->Put(field_container[i].name, (const char *)(row[i]+4), row_size);
					}
					break;
				}
			}
		}
		record_container.SetClear(is_new);
		record_container.Put(record);
	}
	
	mysql_free_result(result);
	return record_container.Size();
}


/**
 * @brief 执行插入
 * 
 * @param table 要操作的表
 * @param record 要插入的记录
 *
 * @return 插入数据条数
 */
unsigned int MODI_DBClient::ExecInsert(MODI_TableStruct * table, MODI_RecordContainer * record_container)
{
	int ret_code = 0;
	if(table == NULL || record_container == NULL || m_stMySql == NULL || record_container->Size() <= 0)
	{
		return ret_code;
	}

	std::ostringstream sql;
	sql << "insert into " << "`" <<table->GetName() << "`" << " values";

	class exec_all_record: public MODI_RecordCallBack
	{
	public:
		exec_all_record(MODI_TableStruct * table, MYSQL * mysql): m_pTable(table), m_stMySql(mysql)
		{
			
		}
		
		virtual ~exec_all_record(){};

		class exec_all_field_struct: public MODI_FieldStructCallBack
		{
		public:
			exec_all_field_struct(MODI_TableStruct * table, MODI_Record * record, MYSQL * mysql)
				: m_pTable(table), m_pRecord(record), m_stMySql(mysql)
			{
				
			}
			
		   	virtual ~exec_all_field_struct(){};

			bool Exec(const std::string & field_name, const int & field_type, const enFieldMask field_mask)
			{
				const MODI_VarType insert_value = (*m_pRecord)[field_name];
				if(insert_value.Empty())
				{
					os << "default,";
					return true;
				}
				switch(field_type)
				{
				   	case FIELD_TYPE_TINY:
					{
						os << "'" << (unsigned short)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_SHORT:
					{
						os << "'" << (unsigned short)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_LONG:
					{
						os << "'" << (unsigned long)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_INT24:
					{
						os << "'" << (unsigned int)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_LONGLONG:
					{
						os << "'" << (unsigned long long)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_FLOAT:
					{
						os << "'" << (float)(insert_value) << "'," ;
						return true;
					}
					break;
					case FIELD_TYPE_DOUBLE:
					{
						os << "'" << (double)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_STRING:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
					}
					break;
					case FIELD_TYPE_VAR_STRING:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
					}
					break;
					case FIELD_TYPE_DATE:
					case FIELD_TYPE_DATETIME:				
					{
						std::string insert_sql = (const char *)insert_value;
						if(insert_sql.substr(0,4) == "from")
						{
							os << (const char *)insert_value << ",";
						}
						else
						{
							os << "'" << (const char *)insert_value << "',";
						}
					}
					break;
					default:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
						return true;
					}
					break;
				}
				return true;
			}
						
			std::ostringstream os;
		private:
			MODI_TableStruct * m_pTable;
			MODI_Record * m_pRecord;
			MYSQL * m_stMySql;
		};

		bool Exec(MODI_Record * p_record)
		{
			exec_all_field_struct exec_field(m_pTable, p_record, m_stMySql);
			m_pTable->ExecAllField(exec_field);
			std::string field_sql = "(" + exec_field.os.str().substr(0, exec_field.os.str().size() - 1) + "),";
			insert_sql += field_sql;
			return true;
		}
		
		std::string insert_sql;
	private:
		MODI_TableStruct * m_pTable;
		MYSQL * m_stMySql;
	};
	exec_all_record exec_record(table, m_stMySql);
	record_container->ExecAllRecord(exec_record);
   	(exec_record.insert_sql)[exec_record.insert_sql.size()-1] = ';';
	sql << exec_record.insert_sql;

   	ExecSql(sql.str().c_str(), (DWORD)(sql.str().size()));
	ret_code = (unsigned int)mysql_affected_rows(m_stMySql);
	return ret_code;
}


/**
 * @brief 删除记录
 *
 * @param true 要操作的表
 * @param where 删除记录的条件
 *
 * @return 删除记录的条数
 *
 */
unsigned int MODI_DBClient::ExecDelete(MODI_TableStruct * p_table, const char * where)
{
	unsigned ret_code = 0;
	if(p_table == NULL || where == NULL || m_stMySql == NULL)
	{
		return ret_code;
	}

	std::ostringstream os;
	os << "delete from " << "`" <<p_table->GetName() << "`" << " where ";
	os << where;
   	ExecSql(os.str().c_str(), (unsigned int)(os.str().size()));
	ret_code = (unsigned int)mysql_affected_rows(m_stMySql);
	return ret_code;
}


/**
 * @brief 更新操作
 *
 * @param table 操作的表
 * @param where 更新的数据和条件的与集合
 *
 * @return 更新记录的条数
 *
 */
unsigned int MODI_DBClient::ExecUpdate(MODI_TableStruct * p_table, MODI_Record * p_record)
{
	unsigned int ret_code = 0;
	if(p_table == NULL || p_record == NULL || m_stMySql == NULL)
	{
		return ret_code;
	}
	std::ostringstream os;
	os << "update " << "`" <<p_table->GetName() << "`" << " set ";
	class exec_all_field_table: public MODI_FieldStructCallBack
	{
	public:
		exec_all_field_table(MODI_Record * record): m_pRecord(record)
		{
			
		}
		
		~exec_all_field_table(){};

		bool Exec(const std::string & field_name, const int & field_type, const enFieldMask field_mask)
		{
			const MODI_VarType update_value = (*m_pRecord)[field_name];
			if(update_value.Empty())
			{
				return true;
			}

			switch(field_type)
			{
				case FIELD_TYPE_TINY:
				{
					os <<  field_name  << "='" << (unsigned short)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_SHORT:
				{
					os << field_name  << "='" << (unsigned short)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_LONG:
				{
					os  << field_name << "='" << (unsigned long)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_INT24:
				{
					os  << field_name  << "='" << (unsigned int)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_LONGLONG:
				{
					os  << field_name  << "='" << (unsigned long long)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_FLOAT:
				{
					os  << field_name  << "='" << (float)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_DOUBLE:
				{
					os  << field_name  << "='" << (double)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_STRING:
				{
					char buf[(update_value.Size())*2 + 1];
				   	bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)(update_value), update_value.Size());
					os <<  field_name  << "='" << buf << "',";
					return true;
				}
				break;
				case FIELD_TYPE_VAR_STRING:
				{
					char buf[(update_value.Size())*2 + 1];
				   	bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)(update_value), update_value.Size());
					os <<  field_name  << "='" << buf << "',";
					return true;
				}
				case FIELD_TYPE_DATE:
				case FIELD_TYPE_DATETIME:				
				{
					std::string insert_sql = (const char *)update_value;
					if(insert_sql.substr(0,4) == "from")
					{
						os << field_name  << "=" << (const char *)update_value << ",";
					}
					else
					{
						os  << field_name  << "='" << (const char *)update_value << "',";
					}
				}
				break;

				default:
				{
					char buf[(update_value.Size())*2 + 1];
					bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)update_value, update_value.Size());
					os   << field_name  << "='" << buf << "',";
					return true;
				}
				break;
			}
			return true;
		}
		std::ostringstream os;
	private:
		MODI_Record * m_pRecord;
	};

	exec_all_field_table exec_field(p_record);
	p_table->ExecAllField(exec_field);
	os << exec_field.os.str();
	std::string str_sql = os.str();
	str_sql[str_sql.size() - 1]=' ';
	str_sql += "where ";
	str_sql	+= (const char *)(*p_record)["where"];
	str_sql += ";";

   	ExecSql(str_sql.c_str(), (DWORD)str_sql.size());
	ret_code = (unsigned int)mysql_affected_rows(m_stMySql);
	return ret_code;
}


/**
 * @brief 获取表的记录个数
 * 
 * @param p_table 要获取记录的表
 * @param where 条件 
 *
 * @return 返回符合条件的记录数
 *
 */
unsigned int MODI_DBClient::GetCount(MODI_TableStruct * p_table, const char * where)
{
	unsigned int ret_code = 0;
	if(p_table == NULL || m_stMySql == NULL)
	{
		return ret_code;
	}
	std::ostringstream os;

	os << "select count(*) from " << "`"<<p_table->GetName() << "`"; 
	if(where)
	{
		os << " where " << where << ";";
	}
	else
	{
		os << ";";
	}
	
	ExecSql(os.str().c_str(), (unsigned int)os.str().size());
	MYSQL_RES * result;
	result = mysql_store_result(m_stMySql);
	if(! result)
	{
		return ret_code;
	}
	
	MYSQL_ROW row;
	row = mysql_fetch_row(result);
	if(row[0])
	{
		ret_code = atoi(row[0]);
	}
	return ret_code;
}

bool MODI_DBClient::_TransactionCmd(const char *sql)
{
	if(mysql_query( m_stMySql , sql ))
	{
		m_lastErrorDes = mysql_error( m_stMySql );
		m_lastErrorNo = mysql_errno( m_stMySql );
		Global::logger->error("[run_mysql] transaction cmd faild <sql=%s,error_code=%u,des=%s>",
							  sql, m_lastErrorNo, m_lastErrorDes.c_str());
		return false;
	}

	return true;
}

// bool MODI_DBClient::SetAutoCommit( bool bAuto )
// {
// 	if( !m_stMySql )
// 		 return false;
// 	if( mysql_autocommit( m_stMySql , bAuto == true ? 1 : 0 ) != 0 )
// 		return false;
// 	return true;
// }

bool MODI_DBClient::BeginTransaction()
{
	if( !m_stMySql )
		 return false;
	if (!_TransactionCmd("START TRANSACTION"))
		return false;

	return true;
}

bool MODI_DBClient::CommitTransaction()
{
	if (!m_stMySql)
		return false;

//	if( mysql_commit( m_stMySql ) )
//	{
//		m_lastErrorDes = mysql_error( m_stMySql );
//		m_lastErrorNo = mysql_errno( m_stMySql );
//		Global::net_logger->debug("[%s] commitTransaction faild , error code = %u , des = %s  " ,
//		   	SVR_TEST, 
//			m_lastErrorNo , 
//			m_lastErrorDes.c_str() );
//		return false;
//	}

	if( !_TransactionCmd("COMMIT") )
		return false;

	return true;
}

bool MODI_DBClient::RollbackTransaction()
{
	if (!m_stMySql)
		return false;

//	if( mysql_rollback( m_stMySql ) )
//	{
//		m_lastErrorDes = mysql_error( m_stMySql );
//		m_lastErrorNo = mysql_errno( m_stMySql );
//		Global::net_logger->debug("[%s] rollback faild , error code = %u , des = %s  " ,
//		   	SVR_TEST, 
//			m_lastErrorNo , 
//			m_lastErrorDes.c_str() );
//		return false;
//	}

	if( !_TransactionCmd("ROLLBACK") )
		return false;

	return true;
}


/** 
 * @brief 生成选择语句
 * 
 * @param result 结果
 * @param p_table 表
 * @param select_field 要选择的字段
 * @param where 条件
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_DBClient::MakeSelectSql(MODI_ByteBuffer & result, MODI_TableStruct * p_table, MODI_Record * select_field,const char * where)
{
	std::ostringstream os;
	if(! p_table)
	{
		return false;
	}
	
   	if(select_field == NULL)
	{
		os << "select * from " << "`" << p_table->GetName() << "`";
	}
	else
	{
		os << "select ";
		struct all_field_call_back: public MODI_FieldCallBack
		{
			~all_field_call_back(){};

			bool Exec(const std::string & field_name, const MODI_VarType & field_value)
			{
				m_stOut  <<field_name  << ",";
				return true;
			}
			std::ostringstream m_stOut;
		};

		all_field_call_back field_callback;
		select_field->ExecAllRecord(field_callback);
		os << field_callback.m_stOut.str();
		std::string exec_sql = os.str();
	   	exec_sql[exec_sql.size() - 1] = ' ';
		os.str("");
		os << exec_sql;
		os << "from " << "`" << p_table->GetName() << "`";
	}

	if(where)
	{
		os << " where " << where << ";";
	}
	else
	{
		os << ";";
	}

	memcpy(result.ReadBuf(), os.str().c_str(), os.str().size());
	result.WriteSuccess(os.str().size());
	return true;
}



/** 
 * @brief 生成插入语句
 * 
 * @param result 结果
 * @param table 表
 * @param record_container 要插入的字段
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_DBClient::MakeInsertSql(MODI_ByteBuffer & result, MODI_TableStruct * table, MODI_RecordContainer * record_container)
{
	std::ostringstream sql;
	if(table == NULL || record_container == NULL || record_container->Size() <= 0)
	{
		return false;
	}

	sql << "insert into " << "`" << table->GetName() << "`" << " values";

	class exec_all_record: public MODI_RecordCallBack
	{
	public:
		exec_all_record(MODI_TableStruct * table): m_pTable(table)
		{
			
		}
		
		virtual ~exec_all_record(){};

		class exec_all_field_struct: public MODI_FieldStructCallBack
		{
		public:
			exec_all_field_struct(MODI_TableStruct * table, MODI_Record * record)
				: m_pTable(table), m_pRecord(record)
			{
				
			}
			
		   	virtual ~exec_all_field_struct(){};

			bool Exec(const std::string & field_name, const int & field_type, const enFieldMask field_mask)
			{
				const MODI_VarType insert_value = (*m_pRecord)[field_name];
				if(insert_value.Empty())
				{
					os << "default,";
					return true;
				}
 				switch(field_type)
				{
				   	case FIELD_TYPE_TINY:
					{
						os << "'" << (unsigned short)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_SHORT:
					{
						os << "'" << (unsigned short)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_LONG:
					{
						os << "'" << (unsigned long)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_INT24:
					{
						os << "'" << (unsigned int)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_LONGLONG:
					{
						os << "'" << (unsigned long long)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_FLOAT:
					{
						os << "'" << (float)(insert_value) << "'," ;
						return true;
					}
					break;
					case FIELD_TYPE_DOUBLE:
					{
						os << "'" << (double)(insert_value) << "',";
						return true;
					}
					break;
					case FIELD_TYPE_STRING:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
					}
					break;
					case FIELD_TYPE_VAR_STRING:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
					}
					break;
					case FIELD_TYPE_DATE:
					case FIELD_TYPE_DATETIME:				
					{
						std::string insert_sql = (const char *)insert_value;
						if(insert_sql.substr(0,4) == "from")
						{
							os << (const char *)insert_value << ",";
						}
						else
						{
							os << "'" << (const char *)insert_value << "',";
						}
					}
					break;
					default:
					{
						char buf[(insert_value.Size())*2 + 1];
					   	bzero(buf, sizeof(buf));
						mysql_escape_string( buf, (const char *)(insert_value), insert_value.Size());
						os << "'" << buf << "',";
						return true;
					}
					break;
				}
				return true;
			}
						
			std::ostringstream os;
		private:
			MODI_TableStruct * m_pTable;
			MODI_Record * m_pRecord;
		};

		bool Exec(MODI_Record * p_record)
		{
			exec_all_field_struct exec_field(m_pTable, p_record);
			m_pTable->ExecAllField(exec_field);
			std::string field_sql = "(" + exec_field.os.str().substr(0, exec_field.os.str().size() - 1) + "),";
			insert_sql += field_sql;
			return true;
		}
		
		std::string insert_sql;
	private:
		MODI_TableStruct * m_pTable;
	};
	exec_all_record exec_record(table);
	record_container->ExecAllRecord(exec_record);
   	(exec_record.insert_sql)[exec_record.insert_sql.size()-1] = ';';
	sql << exec_record.insert_sql;

	memcpy(result.ReadBuf(), sql.str().c_str(), sql.str().size());
	result.WriteSuccess(sql.str().size());
	return true;
}


/** 
 * @brief 生成更新语句
 * 
 * @param p_table 更新的表
 * @param p_record 更新的记录 
 * 
 * @return 生成的sql
 *
 */
bool MODI_DBClient::MakeUpdateSql(MODI_ByteBuffer & result, MODI_TableStruct * p_table, MODI_Record * p_record)
{
	std::ostringstream os;
	if(p_table == NULL || p_record == NULL)
	{
		return os.str().c_str();
	}
	
	os << "update " << "`" << p_table->GetName() << "`" << " set ";
	class exec_all_field_table: public MODI_FieldStructCallBack
	{
	public:
		exec_all_field_table(MODI_Record * record): m_pRecord(record)
		{
			
		}
		
		~exec_all_field_table(){};

		bool Exec(const std::string & field_name, const int & field_type, const enFieldMask field_mask)
		{
			const MODI_VarType update_value = (*m_pRecord)[field_name];
			if(update_value.Empty())
			{
				return true;
			}

			switch(field_type)
			{
				case FIELD_TYPE_TINY:
				{
					os  << field_name  << "='" << (unsigned short)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_SHORT:
				{
					os  << field_name << "='" << (unsigned short)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_LONG:
				{
					os  << field_name  << "='" << (unsigned long)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_INT24:
				{
					os  << field_name  << "='" << (unsigned int)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_LONGLONG:
				{
					os  << field_name  << "='" << (unsigned long long)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_FLOAT:
				{
					os  << field_name  << "='" << (float)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_DOUBLE:
				{
					os  << field_name  << "='" << (double)(update_value) << "',";
					return true;
				}
				break;
				case FIELD_TYPE_STRING:
				{
					char buf[(update_value.Size())*2 + 1];
				   	bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)(update_value), update_value.Size());
					os  << field_name  << "='" << buf << "',";
					return true;
				}
				break;
				case FIELD_TYPE_VAR_STRING:
				{
					char buf[(update_value.Size())*2 + 1];
				   	bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)(update_value), update_value.Size());
					os  << field_name  << "='" << buf << "',";
					return true;
				}
				case FIELD_TYPE_DATE:
				case FIELD_TYPE_DATETIME:				
				{
					std::string insert_sql = (const char *)update_value;
					if(insert_sql.substr(0,4) == "from")
					{
						os  << field_name  << "=" << (const char *)update_value << ",";
					}
					else
					{
						os  << field_name  <<  "='" << (const char *)update_value << "',";
					}
				}
				break;

				default:
				{
					char buf[(update_value.Size())*2 + 1];
					bzero(buf, sizeof(buf));
					mysql_escape_string( buf, (const char *)update_value, update_value.Size());
					os  << field_name  << "='" << buf << "',";
					return true;
				}
				break;
			}
			return true;
		}
		std::ostringstream os;
	private:
		MODI_Record * m_pRecord;
	};

	exec_all_field_table exec_field(p_record);
	p_table->ExecAllField(exec_field);
	os << exec_field.os.str();
	std::string str_sql = os.str();
	str_sql[str_sql.size() - 1]=' ';
	str_sql += "where ";
	str_sql	+= (const char *)(*p_record)["where"];
	str_sql += ";";

	memcpy(result.ReadBuf(), str_sql.c_str(), str_sql.size());
	result.WriteSuccess(str_sql.size());
	return true;
}


/** 
 * @brief 执行sql结果
 * 
 * @param sql 要执行的sql
 * @param sql_len sql长度
 *
 * @return 影响的行数
 *
 */
int MODI_DBClient::ExecSqlResult(const char * sql, unsigned int sql_len)
{
	int ret_code = 0;
	ret_code = ExecSql(sql, strlen(sql));
	if(ret_code == -1)
	{
		return ret_code;
	}
	ret_code = (unsigned int)mysql_affected_rows(m_stMySql);
	return ret_code;
}
