/** 
 * @file MdmMgr.h
 * @brief *.Mdm文件管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */

#ifndef MODI_MDM_FILE_MGR_H_
#define MODI_MDM_FILE_MGR_H_

#include <string>
#include <map>
#include "protocol/gamedefine.h"

using std::string;
class MODI_MIDILoader;
class MODI_Song;
class MODI_Lyric;
class MODI_BinFileMgr;

class 	MODI_MdmMgr
{
	public:
		MODI_MdmMgr();
		~MODI_MdmMgr();


	public:

		static 	MODI_MdmMgr * 	GetInstancePtr();


	public:
		
		/// 初始化管理器
		bool Initial(const char * szMdmFilesDir);

		/// 检查MDM资源是否完整有效
		bool CheckResource( MODI_BinFileMgr * pBinMgr );
		
		/// 获得一首音乐的对应的MDM数据流
		//  note : size_t & nOutBufSize  [in/out param]
		bool  GetMdmBuffer( defMusicID nMusicID , char * pOutBuf , unsigned int &  nOutBufSize );

		/// 获得一首音乐的歌词句数
		unsigned int GetSencentsSize( defMusicID nMusicID );

		/// 获得一首音乐中某句歌词的开始和结束时间
		bool  GetSencentsRange( defMusicID nMusicID , unsigned int nSen , float & fBegin , float  & fEnd );

		/// 获得一首音乐的长度
		bool  GetMusicLength( defMusicID nMusicID , float & fLength );

		/// 获得一首音乐所有NOTE的长度总合
		bool  GetMusicTotalNoteLength( defMusicID nMusicID , float & fLength );

		size_t Size() const { return m_mapMdm.size(); }

		/// 释放管理器
		void  CleanUP();

	private:

		MODI_MIDILoader * 	Find( defMusicID nMusicID );
		MODI_Song * FindSong( defMusicID nMusicID );

	private:

		typedef std::map<std::string,MODI_MIDILoader *> 	MAP_MDM;
		typedef MAP_MDM::iterator 							MAP_MDM_ITER;
		typedef MAP_MDM::const_iterator 					MAP_MDM_C_ITER;
		typedef std::pair<MAP_MDM_ITER,bool> 				MAP_INSERT_RESULT;

		MAP_MDM 	m_mapMdm;
};

#endif

