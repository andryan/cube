/** 
 * @file Song.h
 * @brief ����Ԫ����
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-21
 */

#ifndef __SONG_H__
#define __SONG_H__

#include "LyricParser.h"

class MODI_Note //????
{
public:
	MODI_Note();
	~MODI_Note();

	int     GetPitch(){ return pitch; }                       // the pitch of note
	double   GetStartTime(){ return startTime; }	// the absolute time of note on
	double   GetEndTime(){ return endTime; }	// the absolute time of note off
	double   GetDuration(){ return endTime - startTime; } // the first begins at 1, not 0
	int     GetIndex(){ return index; }       
	MODI_Note * GetPrev(){ return prev; }
	MODI_Note * GetNext(){ return next; }

public:
	int     pitch;
	double   startTime;
	double   endTime;

	MODI_Note * prev;
	MODI_Note * next;

	int     index;
};

/*****************************************************************************/
class MODI_Lyric //???
{
public:
	MODI_Lyric();
	~MODI_Lyric();

	string   GetCharacter()// the word of lyric, the string end of null
	{
		return m_character; 
	}
	
	inline double GetStartTime()// the absolute time to start the word
	{
		return m_startTime; 
	}

	inline double GetEndTime()	// the absolute time to end the word
	{
		return m_endTime; 
	}

	inline double GetDuration()
	{
		return m_endTime - m_startTime; 
	}

	inline MODI_Note *  GetLinkNote()// the link note at same start time
	{
		return m_linkNote; 
	}
	
	inline MODI_Note *  GetLastNote()// the link note at same start time
	{ 
		return m_lastNote;
	}           
	
	inline int GetNumLinkNote()// the number of note this word linked
	{ 
		return m_numLinkNote; 
	}             
	
	inline int GetIndex()// the first begins at 1, not 0
	{
		return m_index; 
	}                            

	MODI_Lyric * GetPrev(){ return m_prev; }
	MODI_Lyric * GetNext(){ return m_next; }

	bool     IsLast(){ return m_last; }

public:
	string  m_character;
	double   m_startTime;
	double   m_endTime;

	MODI_Note * m_linkNote;
	MODI_Note * m_lastNote;
	int     m_numLinkNote;

	MODI_Lyric * m_prev;
	MODI_Lyric * m_next;

	int      m_index;

	bool     m_last;
};

/*****************************************************************************/
class MODI_Sentence //????
{
public:
	MODI_Sentence();
	~MODI_Sentence();

	inline MODI_Lyric * GetStartLyric()
	{
		return m_startLyric; 
	}

	inline MODI_Lyric * GetEndLyric()
	{
		return m_endLyric; 
	}

	inline int GetNumLyric()
	{
		return m_endLyric->m_index - m_startLyric->m_index + 1; 
	}

	inline double GetStartTime()
	{
		return m_startLyric->m_startTime; 
	}

	inline double GetEndTime()
	{
		return m_endLyric->m_endTime; 
	}

	inline double GetDuration()
	{
		return m_endLyric->m_endTime - m_startLyric->m_startTime; 
	}

	int      GetType();
	float    GetMultiple();
	bool	 HasTag(const char* pcTag);

	bool     isRap();

	bool     isInterMission();

	/*****************************************************************************/
	double GetDurationOfAllLyric();

	MODI_Sentence * GetNext(){ return m_next; }
	MODI_Sentence * GetPrev(){ return m_prev; }

	int      GetIndex(){ return m_index; }
	int      m_index;

	MODI_Lyric * m_startLyric;
	MODI_Lyric * m_endLyric;

	MODI_Sentence * m_next;
	MODI_Sentence * m_prev;

	AttriVector m_attri;
    
	bool	m_bIsEffected;

	bool    m_bRap;

	bool    m_bInterMission;
};

/*****************************************************************************/
class MODI_Tempo //???
{
public:
	MODI_Tempo();
	~MODI_Tempo();

	inline unsigned GetTempo()
	{
		return m_dwTempo; 
	}

	inline unsigned long GetStartTicks()
	{
		return m_startTicks; 
	}

	inline double GetStartTime()
	{
		return m_startTime; 
	}

	inline MODI_Tempo *GetNext()
	{
		return next; 
	}

	inline MODI_Tempo *GetPrev()
	{
		return prev; 
	}

	inline int GetIndex()
	{
		return index; 
	}

public:
	unsigned long  m_dwTempo;
	unsigned long m_startTicks;
	double m_startTime;	
	MODI_Tempo *next;
	MODI_Tempo *prev;
	unsigned m_nBeatsPerPara;
	double m_dQuaNotePerBeat;

	int     index;
};

/*****************************************************************************/
class MODI_Song
{
	friend class MODI_MIDILoader;

public:
	MODI_Song();
	~MODI_Song();

	MODI_Song( const string& fileName );

	// ??????????????????????
	void  AddNote( MODI_Note * note );

	// ???????????????????
	void  AddLyric( MODI_Lyric* lyric);

	// ?????????????????????
	void  AddSentence( MODI_Sentence * sentence );

	// ????????????????????? ???????
	void  AddTempo(MODI_Tempo *tempo);

	// ????????????????????????????
	void  PrevNote();   

	// ??????????????????????????
	void  PrevLyric();     

	// ????????????????????????????
	void  PrevSentence();     

	// ??????????????????????????
	void  PrevTempo();

	// ?????????????????????????????
	void  NextNote();                                   

	// ???????????????????????????
	void  NextLyric();    

	// ?????????????????????????????
	void  NextSentence(); 

	// ???????????????????????????
	void  NextTempo();

	// ????????????????????????????
	void  HeadNote();          

	// ??????????????????????????
	void  HeadLyric();

	// ????????????????????????????
	void  HeadSentence(); 

	// ???????????????????????????
	void  HeadTempo();

	// ?????????????????????��
	bool  IsNoteRear();

	// ???????????????????��
	bool  IsLyricRear();

	// ????????????????????��
	bool  IsSentenceRear();

	// ????????????????????��
	bool  IsTempoRear();

	// ???????????? ?????????????
	MODI_Note*     GetNoteByTime(double time) const;     

	// ???????????? ?????????????
	MODI_Lyric*    GetLyricByTime(double time) const;         

	// ????????????? ?????????????
	MODI_Sentence* GetSentenceByTime(double time) const;   

	// ???????????? ???????
	MODI_Tempo*    GetTempoByTicks(unsigned long ticks);

	// ????????????????????????????????????
	void       Update( float time );                       

	// ?????????????????
	MODI_Note*     GetCurNote(){ return m_curNote; }

	// ???????????????
	MODI_Lyric*    GetCurLyric(){ return m_curLyric; }

	// ????????????????
	MODI_Sentence* GetCurSentence(){ return m_curSentence; }

	// ????????????
	MODI_Note*     GetHeadNote(){ return m_notesListHead; }

	// ???????????
	MODI_Lyric*    GetHeadLyric(){ return m_lyricsListHead; }

	// ????????????
	MODI_Sentence* GetHeadSentence(){ return m_sentenceListHead; }

	// ???????????
	MODI_Tempo*    GetHeadTempo(){ return m_tempoListHead; }
	
	// ???ticks??????????????
	double GetAbsTimeByTicks(unsigned nTicks);

	// ?????????????Midi??Tempo
	MODI_Tempo * GetTempoByTime( double time );

	/*	
	*	??????????????��?????????const???��??��?????????????
	*/
	//string GetName(){ return name; }
	// ?????????
	inline const string& GetName() const
	{
		return m_name;
	}

	// ??????????
	int   GetNumNotes(){ return m_numNotes; }

	// ?????????
	int   GetNumLyrics(){ return m_numLyrics; }

	// ??????????
	int   GetNumSentence(){ return m_numSentence; }

	// ?????????
	int   GetNumTempo(){ return m_numTempos; }

	// ?????????????
	float GetOverTime(){ return m_overTime; }

	// ????????????????????
	int   GetMaxPitch(){ return m_maxPitch; }                

	// ????????????????????
	int   GetMinPitch(){ return m_minPitch; }

	// ????????????????
	float  GetMaxDuration(){ return m_maxDuration; }

	// ????????????????
	float  GetMinDuration(){ return m_minDuration; }

	// ???????????????
	void   CutTime(float fHeadTime, float fEndTime);

	void GetRefrainTime(double &timeBegin, double &timeEnd);

	// ??????????????????????
	void MarkClimax();
	
	// ???????????????????
	double GetBeatNumByTime(double dTime);

private:
	string   m_name;

	int     m_numNotes;
	int     m_numLyrics;
	int     m_numSentence;
	int     m_numTempos;

	float    m_overTime;

	bool     m_noteOn;
	bool     m_oldNoteOn;

	MODI_Note *m_notesListHead;
	MODI_Note *m_curNote;

	MODI_Lyric *m_lyricsListHead;
	MODI_Lyric *m_curLyric;

	MODI_Sentence *m_sentenceListHead;
	MODI_Sentence *m_curSentence;

	MODI_Tempo *m_tempoListHead;

	int      m_maxPitch;
	int      m_minPitch;

	float    m_maxDuration;
	float    m_minDuration;
	double	 m_dDivision;
};

#endif
