/**
 * @file CommandQueue.h
 * @date 2009-12-11 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 线程安全的消息队列
 *
 */

#ifndef _MODICOMMMANDQUEUE_H
#define _MODICOMMMANDQUEUE_H

#include <queue>
#include <ext/pool_allocator.h>
#include <ext/mt_allocator.h>
#include "Global.h"
#include "ObjQueue.h"

const unsigned int CMD_BUF_LENGTH = 256;
struct MODI_CmdBuf
{
	void Reset()
	{
		memset(m_byCmdData, 0, sizeof(m_byCmdData));
	}
	
	BYTE m_byCmdData[CMD_BUF_LENGTH];
};

typedef MODI_ObjQueue<MODI_CmdBuf, 128> defCmdBuf;


/// 消息<长度,内容>
typedef std::pair<int, unsigned char * > defCmdPair;
	
/// 消息队列
typedef std::pair<volatile bool, defCmdPair > defCmdQueue;


/**
 * @brieff 实现消息处理队列
 * 循环队列解决线程安全
 *
 */
template <unsigned int dwQueueSize=2048 >
class MODI_CmdQueue
{
 public:
	MODI_CmdQueue()
	{
		m_dwReadAddr = 0;
		m_dwWriteAddr = 0;
		m_stCmdBuf.Init();
	}

	/// 释放内存
	~MODI_CmdQueue()
	{
		Final();
	}

	/**
	 * @brief 压入命令 
	 * @param cmd_data 消息内容
	 * @param cmd_size 消息大小
	 * @return
	 *
	 */
	int Put(const void * cmd_data, const unsigned int real_size)
	{
		unsigned int cmd_size = real_size;
		cmd_size &= 0x0fffffff;
		unsigned char * buf = NULL;
		if(cmd_size > CMD_BUF_LENGTH)
			buf = __mt_alloc.allocate(cmd_size);
		else
		{
			buf = (unsigned char * )(m_stCmdBuf.Get());
			if(buf == NULL)
			{
				buf = __mt_alloc.allocate(cmd_size);
				cmd_size |= 0x10000000;
			}
		}
		
		if(buf)
		{
			unsigned int real_size = cmd_size & 0x0fffffff;
			memcpy(buf, cmd_data, real_size);
			/// CmdDeque为空才可以直接写到消息数组里面去, 保证消息的顺序处理
			if((MoveCmdToQueue()) && (!CmdQueue[m_dwWriteAddr].first))
			{
				CmdQueue[m_dwWriteAddr].second = std::make_pair(cmd_size, buf);
				CmdQueue[m_dwWriteAddr].first = true;
				m_dwWriteAddr = (++m_dwWriteAddr) % dwQueueSize;
				return 1;
			}
			else
			{
				CmdDeque.push(std::make_pair(cmd_size, buf));
				return 2;
			}
		}
		return 0;
	}

	/**
	 * @brief 读取消息
	 *
	 * 不要马上删除此消息,等应用层取走了命令才删除
	 *
	 * @return 消息对指针
	 * 
	 */
	defCmdPair * Get()
	{
		defCmdPair * ret_cmd = NULL;
		if(CmdQueue[m_dwReadAddr].first)
		{
			return &(CmdQueue[m_dwReadAddr].second);
		}
		return ret_cmd;
	}

 	/**
  	 * @brief 删除消息
  	 *
  	 */ 
	void Erase()
	{
		unsigned int cmd_size = (unsigned int )CmdQueue[m_dwReadAddr].second.first;
		unsigned int real_size = cmd_size & 0x0fffffff;
		if(real_size > CMD_BUF_LENGTH)
			__mt_alloc.deallocate(CmdQueue[m_dwReadAddr].second.second, real_size);
		else
		{
			if(cmd_size & 0x10000000)
			{
				__mt_alloc.deallocate(CmdQueue[m_dwReadAddr].second.second, real_size);
			}
			else
			{
				m_stCmdBuf.Put((MODI_CmdBuf *)(CmdQueue[m_dwReadAddr].second.second));
			}
		}
		CmdQueue[m_dwReadAddr].first = false;
		m_dwReadAddr = (++m_dwReadAddr) % dwQueueSize;
	}

	void Resize(unsigned int _size)
 	{
	 	m_stCmdBuf.Resize(_size);
 	}
 private:
 	/**
  	 * @brief 把命令从队列里面移到消息数组里面,直到消息数组满或者队列空
  	 *
  	 * @return 成功true,失败false
  	 *
  	 */ 
	bool MoveCmdToQueue()
	{
		bool ret_value = true;
		while(! CmdDeque.empty())
		{
			if(CmdQueue[m_dwWriteAddr].first)
			{
				ret_value = false;
				break;
			}
			CmdQueue[m_dwWriteAddr].second = CmdDeque.front();
			CmdQueue[m_dwWriteAddr].first = true;
			m_dwWriteAddr = (++m_dwWriteAddr) % dwQueueSize;
			CmdDeque.pop();
		}
		return ret_value;
	}

	/// 释放资源
	void Final()
	{
		while(! MoveCmdToQueue())
		{
			while(Get())
			{
				Erase();
			}
		}
		while(Get())
		{
			Erase();
		}
	}
 private:
	///multi thread allocate
	__gnu_cxx::__mt_alloc<unsigned char > __mt_alloc;

	/// 消息队列
	defCmdQueue CmdQueue[dwQueueSize];

	/// 消息缓冲区
	std::queue<defCmdPair, std::deque<defCmdPair > > CmdDeque;

	/// 读取地址
	unsigned int m_dwReadAddr;

	/// 写指针
	unsigned int m_dwWriteAddr;

 	/// 命令池
 	defCmdBuf m_stCmdBuf;
};


/**
 * @brief 消息处理接口
 * 
 * 实现CmdParse()函数
 */
class MODI_CmdParse
{
 protected:
	MODI_CmdParse()
	{
		m_dwDelay = 0;
		m_dwCurrent = 0;
	}
		
	virtual ~MODI_CmdParse(){}
 public:
	/**
	 * @brief 连接收到消息, 放入缓冲队列
	 *
	 * @param pt_null_cmd 要放入的命令
	 * @param cmd_size 命令的大小
	 *
	 */
	int Put(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
	{
		int bRet = stCmdQueue.Put((void *)pt_null_cmd, cmd_size);
		if( !bRet )
		{
			Global::logger->fatal("[cmd_to_queue] put cmd to queue faild");
			MODI_ASSERT( bRet );
		}
		return bRet;
	}

	/// 消息派发机制在此实现
	virtual bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size) = 0;

	/// 消息派发
	void Get(const WORD default_get_count )
	{
		WORD get_count = 0;
		defCmdPair * p_cmd = stCmdQueue.Get();
		while(p_cmd)
		{
			CmdParseQueue((const Cmd::stNullCmd *)p_cmd->second, ((p_cmd->first)&0x0fffffff));
			stCmdQueue.Erase();
			get_count++;
			if(get_count >= default_get_count)
			{
				p_cmd = NULL;
				break;
			}
			p_cmd = stCmdQueue.Get();
		}
		if(p_cmd)
		{
			stCmdQueue.Erase();
		}
	}

	void Resize(unsigned int _size)
	{
		stCmdQueue.Resize(_size);
	}
	
 private:
	/// 消息队列
	MODI_CmdQueue<> stCmdQueue;
	MODI_RTime m_stTime;
	DWORD m_dwDelay;
	DWORD m_dwCurrent;
};

#endif
