/**
 * @file   ClientRecycleSched.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Oct 12 15:06:57 2010
 * @version $Id:$
 * @brief  连接回收
 * 
 */


#ifndef _MDCLIENTRECYCLESCHED_H
#define _MDCLIENTRECYCLESHCED_H

#include "ClientPollThread.h"
#include "ThreadGroup.h"

class MODI_PClientTaskSched;


/**
 * @brief 回收线程
 * 
 */
class MODI_ClientRecycleThread: public MODI_ClientPollThread
{
 public:
	/** 
	 * @brief 构造 
	 * 
	 * @param sched 调度器
	 * @param name 线程名字
	 * 
	 */
	MODI_ClientRecycleThread(MODI_PClientTaskSched * sched, const char * name):
	   	MODI_ClientPollThread(name), m_pSched(sched)
	{
	}

		
	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true,失败false
	 */
	bool Init();

	
	/** 
	 * @brief 结束
	 * 
	 */
	void Final();

	
	/**
	 * @brief 主程序
	 * 
	 */
	void Run();

	/** 
	 * @brief 删除某个连接
	 * 
	 * @param p_task 要删除的连接
	 */
   	void RemoveTask(MODI_PClientTask * p_task)
	{
		m_TaskList.remove(p_task);
	}
	
 protected:
	/** 
 	 * @brief 连接注册到列表
 	 * 
 	 * @param p_task 要注册的task
 	 */
	void AddTaskToList(MODI_PClientTask * p_task)
	{
		m_TaskList.push_back(p_task);
	}
	

 private:
	/// 归属那个调度器
	MODI_PClientTaskSched * m_pSched;
};


/**
 * @brief 回收调度
 * 
 */
class MODI_ClientRecycleSched
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sched 归属调度器
	 * @param thread_num 线程数
	 */
	MODI_ClientRecycleSched(MODI_PClientTaskSched * sched, const WORD thread_num):
		m_pSched(sched), m_stThreadGroup(thread_num)
	{
		
	}

		
	/** 
	 * @brief 初始化
	 * 
	 * @return 
	 */
	bool Init();

	
	/** 
	 * @brief 结束
	 * 
	 */
	void Final()
	{
		m_stThreadGroup.DelAllThread();
	}

	
	/** 
	 * @brief 获取连接数量
	 * 
	 * 
	 * @return 连接数
	 */
	int Size()
	{
		struct MODI_GetSize: public MODI_ThreadCallBack
		{
			MODI_GetSize()
			{
				size = 0;
			}
			
			int size;
			
			bool Exec(MODI_Thread * p_thread)
			{
				size += ((MODI_ClientRecycleThread *)p_thread)->Size();
				return true;
			}
		};

		MODI_GetSize getsize_callback;
		m_stThreadGroup.ExecAllThread(getsize_callback);
		return getsize_callback.size;
	}

	/** 
	 * @brief 增加一个连接任务到队列
	 * 
	 * @param p_task 连接的任务
	 */	
  	bool  AddTask(MODI_PClientTask * p_task)
	{
		static DWORD count = 0;
		MODI_ClientRecycleThread * p_thread = (MODI_ClientRecycleThread *)(m_stThreadGroup[count++ % m_stThreadGroup.GetCount()]);
		if(p_thread)
		{
			p_task->GetNextState();
		   	p_thread->AddTask(p_task);
			return true;
		}
		return false;
	}
	
 private:
	/// 归属那个调度器
	MODI_PClientTaskSched * m_pSched;

	/// 线程组
	MODI_ThreadGroup m_stThreadGroup;
	
};

#endif
