/**
 * @file Logger.h
 * @date 2010-02-01 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 封装游戏日志类
 *
 */

#ifndef _MDLOGGER_H_
#define _MDLOGGER_H_

#include <log4cxx/logger.h>
#include <log4cxx/rollingfileappender.h>
#include <log4cxx/helpers/simpledateformat.h>
#include <apr-1/apr_time.h>

#include "Define.h"
#include "Mutex.h"


/**
 * @brief 文件输出
 *
 * 小时为单位
 *
 */
class MODI_LocalFileAppender: public log4cxx::rolling::RollingFileAppenderSkeleton
{
 public:
	MODI_LocalFileAppender()
	{
		m_pDataFormat = NULL;
	}

	~MODI_LocalFileAppender()
	{
		if(m_pDataFormat)
			delete m_pDataFormat;
		m_pDataFormat = NULL;
	}

	/// 触发文件切换
   	void subAppend(const log4cxx::spi::LoggingEventPtr& event, log4cxx::helpers::Pool & p);

	/// 文件切换
	void RollOver();

	/// 设置文件名
	void SetFile(const log4cxx::LogString & file)
	{
		m_strFileName = file;
	}
	
	/// 初始化
	void activateOptions();
	
 private:	
	
	/// 当前时间
	apr_time_t m_stNowTime;
	/// 下次切换文件的时间
	apr_time_t m_stCheckTime;

	/// 文件名
	log4cxx::LogString m_strScheduledFileName;
	log4cxx::LogString m_strFileName;

	/// 时间格式
	log4cxx::LogString m_strDatePattern;
	/// 格式化时间工具
	log4cxx::helpers::SimpleDateFormat * m_pDataFormat;
};


/**
 * @brief 日志封装,以log4cxx基础
 *
 *
 */
class MODI_Logger
{
 public:
	MODI_Logger(const log4cxx::LogString & log_name = "Cube");
	
	~MODI_Logger()
	{
		
	}

	/// 增加控制台输出
	void AddConsoleLog();

	/// 删除控制台输出
	void RemoveConsoleLog();

	/// 增加本地文件输出
	void AddLocalFileLog(const log4cxx::LogString & file);

	/// 设置输入等级
	void SetLevel(const std::string & set_level);


	/// 等级输出
	void fatal(const char * pattern, ...);
	void error(const char * pattern, ...);
	void warn(const char * pattern, ...);
	void info(const char * pattern, ...);
	void debug(const char * pattern, ...);
	
 private:

	/// 日志实例
	log4cxx::Logger * m_ptLogger;
	
	/// 日志信息
	char m_cstrMessage[MAX_LOGGERSIZE];

	/// 日志线程安全锁
	MODI_Mutex m_stMsgMutex;
};

#endif
