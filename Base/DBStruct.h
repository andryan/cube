/**
 * @file DBStruct.h
 * @date 2010-01-05 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 表,记录,数据库管理等
 *
 */


#ifndef _MDDBSTRUCT_H
#define _MDDBSTRUCT_H

#include <mysql.h>
#include <map>
#include <ext/hash_map>
#include <vector>
#include <string>
#include <sstream>

#include "Global.h"
#include "VarType.h"
#include "Share.h"
#include "RecordQueue.h"

class MODI_DBClient;

/// 字段压缩格式
enum enFieldMask
{
	/// 无压缩 
	enMaskNone = 0,
	/// zip压缩
	enMaskZip = 1,
};


/**
 * @brief 表结构回调
 *
 */
struct MODI_FieldStructCallBack
{
	virtual bool Exec(const std::string & field_name, const int & field_type, enFieldMask field_mask) = 0;
	virtual ~MODI_FieldStructCallBack(){};
};


/**
 * @brief 表结构封装
 *
 * 对字段封装
 *
 */
class MODI_TableStruct
{
 public:
 	typedef	__gnu_cxx::hash_map<std::string, std::pair<int, enFieldMask >, str_hash > defFieldMap;
	typedef defFieldMap::iterator defMapIter;
	typedef defFieldMap::const_iterator defMapConstIter;
	typedef defFieldMap::value_type defMapValue;

	MODI_TableStruct(const std::string & table_name): m_strName(table_name)
	{

	}

	~MODI_TableStruct()
	{
		
	}

	/**
	 * @brief 设置表名
	 *
	 * @param table_name 设置表名
	 *
	 */
	void SetName(const std::string & table_name)
	{
		m_strName = table_name;
	}

	/**
	 * @brief 获取名字
	 *
	 */ 
	const std::string & GetName() const
	{
		return m_strName;
	}

	/**
	 * @brief 查询字段个数 
	 *
	 * @return 字段个数
	 *
	 */ 
	const int Size()
	{
		return m_FieldMap.size();
	}

	/**
	 * @brief 增加一个字段
	 * @param field_name 字段名
	 * @field_type 字段的类型
	 * @field_mask 字段的压缩格式
	 * @return 成功true,失败false
	 *
	 */ 
	bool AddField(const char * field_name, const int field_type, enFieldMask field_mask = enMaskNone);
	
	/**
	 * @brief 字段是否存在
	 *
	 * @param field_name
	 * @return 有则返回true,无则返回false
	 *
	 */
	bool IsHave(const std::string & field_name)
	{
		std::string temp_name = field_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::toupper);

		defMapIter iter = m_FieldMap.find(temp_name);
		if(iter != m_FieldMap.end())
		{
			return true;
		}
		return false;
	}


	/// 查询字段信息
	bool GetInfo(const std::string & field_name, int & field_type, enFieldMask & field_mask);

	/**
	 * @brief 对表结构回调
	 *
	 *
	 */
	void ExecAllField(MODI_FieldStructCallBack & callback)
	{
		std::map<WORD, std::string >::iterator iter = m_IndexMap.begin();
		for(; iter != m_IndexMap.end(); iter++)
		{
			int field_type;
			enFieldMask field_mask;
			if(GetInfo(iter->second, field_type, field_mask))
			{
				callback.Exec(iter->second, field_type, field_mask);
			}
		}
	}
	
 private:
	/// 表名
	std::string m_strName;
	
	/// <字段名,<字段类型,字段压缩格式> >
	defFieldMap m_FieldMap;

	/// 索引查询
	std::map<WORD, std::string> m_IndexMap;
};


/**
 * @brief 记录回调
 *
 *
 */
struct MODI_FieldCallBack
{
	virtual bool Exec(const std::string & field_name, const MODI_VarType & field_value) = 0;
	virtual ~MODI_FieldCallBack(){};
};


/**
 * @brief 表的一条记录
 * 
 * 包含很多字段和字段的值
 *
 */
class MODI_Record
{
 public:
	MODI_Record()
	{
		
	}

	~MODI_Record()
	{
	}

	/// 别名
	typedef std::map<const std::string, MODI_VarType > defValueMap;
	typedef defValueMap::iterator defValueIter;
	typedef defValueMap::const_iterator defValueConstIter;
	typedef defValueMap::value_type defValueValue;

	/**
	 * @brief 增加一个字段的值 
	 *
	 * @param field_name 字段名字
	 * @param field_value 字段值
	 *
	 * @return 增加成功返回true, 失败返回false
	 *
	 */
	template< typename data_type >
	bool Put(const std::string & field_name, const data_type & field_value)
	{
		std::string temp_name = field_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::toupper);

		std::ostringstream temp_value;
		temp_value << field_value;
		
		defValueIter iter = m_ValueMap.find(temp_name);
		if(iter == m_ValueMap.end())
		{
			m_ValueMap.insert(defValueValue(temp_name, MODI_VarType(temp_value.str())));
			return true;
		}
		iter->second = temp_value.str();
		return true;
	}

	/// 增加字段值
	bool Put(const std::string & field_name, const char & field_value);

	/// 增加字段值
	bool Put(const std::string & field_name, const unsigned char & field_value);

	/// 增加字段值
	bool Put(const std::string & field_name, const void * bin_data, unsigned int bin_len);

	/**
	 * @brief 增加字段的名字
	 * 
	 * @return 增加成功true,增加失败返回false
	 *
	 */
	bool Put(const std::string & field_name)
	{
		std::string temp_name = field_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::toupper);

		defValueIter iter = m_ValueMap.find(field_name);
		if(iter == m_ValueMap.end())
		{
			m_ValueMap.insert(defValueValue(temp_name, MODI_VarType()));
			return true;
		}
		return false;
	}

	/**
	 * @brief 获得某个字段的值
	 *
	 * @param field_name 要查找的字段名
	 * @return 成功返回字段值,失败空值
	 *
	 */ 
	const MODI_VarType & GetValue(const std::string & field_name)
	{
		std::string temp_name = field_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::toupper);

		defValueIter iter = m_ValueMap.find(temp_name);
		if(iter == m_ValueMap.end())
		{
#ifdef _DEBUG
			iter = m_ValueMap.begin();
			while( iter != m_ValueMap.end() )
			{
				std::string strDebug = iter->first;
				strDebug.size();
				iter++;
			}
#endif
			static MODI_VarType null_value;
			return null_value;
		}
		return iter->second;
	}
	
	/**
	 * @brief 重载下标
	 *
	 */ 
	const MODI_VarType & operator[](const std::string & field_name)
	{
		return GetValue(field_name);
	}

	/**
	 * @brief 查询此字段是否存在
	 * 
	 * @param field_name 查询字段名字
	 * @return 有返回true,没有返回false
	 *
	 */
	bool IsHave(const std::string & field_name)
	{
		std::string temp_name = field_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::toupper);

		defValueIter iter = m_ValueMap.find(field_name);
		if(iter == m_ValueMap.end())
		{
			return false;
		}
		return true;
	}

	/**
	 * @brief 查询记录字段的个数
	 * @return 记录字段的个数
	 *
	 */
	int Size()
	{
		return m_ValueMap.size();
	}

	/**
	 * @brief 所有记录回调
	 *
	 *
	 */
	void ExecAllRecord(MODI_FieldCallBack & callback)
	{
	   	defValueIter iter = m_ValueMap.begin();
		for(; iter != m_ValueMap.end(); iter++)
		{
			callback.Exec(iter->first, iter->second);
		}
	}

	void Reset()
	{
		m_ValueMap.clear();
	}
	
 private:
	/// <字段名, 值 >
	std::map<const std::string, MODI_VarType > m_ValueMap;
};


/**
 * @brief 记录集合的回调
 *
 *
 */
struct MODI_RecordCallBack
{
	virtual ~MODI_RecordCallBack(){};
	virtual bool Exec(MODI_Record * record) = 0;
};


/**
 * @brief 记录集合
 *
 *
 */
class MODI_RecordContainer
{
 public:
	MODI_RecordContainer()
	{
		m_blClear = false;
		m_blIsNew = false;
	}

	~MODI_RecordContainer()
	{
		if(m_blClear)
		{
			Clear();
		}
	}

	/**
	 * @brief 加入一条记录 
	 *
	 */ 
	void Put(MODI_Record * p_record)
	{
		m_RecordVec.push_back(p_record);
	}
	
	/**
	 * @brief 获取一条记录
	 *
	 */
	MODI_Record * GetRecord(const unsigned int index)
	{
		if(index < m_RecordVec.size())
		{
			return m_RecordVec[index];
		}
		return NULL;
	}

	
	/**
	 * @brief 重载下标
	 *
	 */
	MODI_Record * operator[](const int index)
	{
		return GetRecord(index);
	}

	
	/**
	 * @brief 获取记录个数
	 *
	 * @return 返回记录个数
	 *
	 */
	int Size()
	{
		return m_RecordVec.size();
	}

	/**
	 * @brief  释放记录
	 *
	 *
	 */
	void Clear()
	{
		if(m_RecordVec.size() > 0)
		{
			std::vector<MODI_Record * >::iterator iter = m_RecordVec.begin();
			for(; iter != m_RecordVec.end(); iter++)
			{
				if(*iter)
				{
					if(m_blIsNew)
					{
						delete(*iter);
					}
					else
					{
						(*iter)->Reset();
						m_stRecordQueue->Put(*iter);
					}
				}
				(*iter) = NULL;
			}
			m_RecordVec.clear();
		}
	}


	/** 
	 * @brief 需要手动释放资源
	 * 
	 */
	void SetClear(bool is_new = false)
	{
		m_blIsNew = is_new;
		m_blClear = true;
	}
	
	/**
	 * @brief 对所有记录回调
	 *
	 */
	void ExecAllRecord(MODI_RecordCallBack & callback)
	{
		std::vector<MODI_Record * >::iterator iter = m_RecordVec.begin();
		for(; iter != m_RecordVec.end(); iter++)
		{
			callback.Exec(*iter);
		}
	}

	/// 记录集合
	static MODI_RecordQueue * m_stRecordQueue;
	
 private:
	/// 记录集合
	std::vector<MODI_Record * > m_RecordVec;

	/// 是否需要手动释放资源
	bool m_blClear;
	/// 直接释放
	bool m_blIsNew;
};


/**
 * @brief 登录数据库信息
 *
 */
class MODI_URLInfo
{
 public:

	 MODI_URLInfo():m_wdPort(0) 
	{

	}
	/**
	 * @brief 获取登录名字
	 *
	 * @return 数据库用户名
	 *
	 */
	const std::string & GetLoginName()
	{
		return m_strLoginName;
	}

	/**
	 * @brief 获取登录密码
	 * 
	 * @return 数据库登录的密码
	 *
	 */
	const std::string & GetPassWord()
	{
		return m_strPassWord;
	}

	/**
	 * @brief 获取登录端口
	 *
	 * @return 数据库连接端口
	 *
	 */
	const WORD & GetPort()
	{
		return m_wdPort;
	}

	/**
	 * @brief 数据库服务器地址
	 *
	 * @return 数据库服务器IP地址
	 *
	 */ 
	const std::string & GetLoginIP()
	{
		return m_strLoginIP;
	}

	/**
	 * @brief  获取要操作的数据库名
	 *
	 * @return 数据库名
	 *
	 */
	const std::string & GetDBName()
	{
		return m_strDBName;
	}

	/**
	 * @brief 获取xml里面配置的登录信息
	 *
	 * @url_info 登录的信息
	 *
	 */ 
	bool GetURLInfo(const std::string & url_info);

	
 private:
	/// 登录的用户名
	std::string m_strLoginName;

	/// 登录的密码
	std::string m_strPassWord;

	/// 登录的端口
	WORD m_wdPort;

	/// 登录的主机
	std::string m_strLoginIP;

	/// 数据库名
	std::string m_strDBName;
};


/**
 * @brief 数据库管理
 *
 *
 */
class MODI_DBManager
{
 public:
	/// 最多的连接数
	static const DWORD MAX_DBCLIENT = 64;
	
	/// 表管理
	typedef __gnu_cxx::hash_map<const std::string, MODI_TableStruct * , str_hash> defTableMap;
	typedef defTableMap::iterator defTableMapIter;
	typedef defTableMap::const_iterator defTableMapConstIter;
	typedef defTableMap::value_type defTableMapValue;

	/// 连接管理 
	typedef std::map<DWORD, MODI_DBClient * > defClientMap;
	typedef defClientMap::iterator defClientMapIter;
	typedef defClientMap::value_type defClientMapValue;

	/// 多数据库(......)
	static MODI_DBManager & GetInstance(const char * database_name = "gamedb")
	{
/* 	   	if(!strcmp(database_name, "billdb")) */
/* 		{ */
/* 			if(m_bill_pInstance == NULL) */
/* 			{ */
/* 				m_bill_pInstance = new MODI_DBManager(); */
/* 			} */
/* 			return * m_bill_pInstance; */
/* 		} */
/* 		else if(!strcmp(database_name, "bbsdb")) */
/* 		{ */
/* 			if(m_bbs_pInstance == NULL) */
/* 			{ */
/* 				m_bbs_pInstance = new MODI_DBManager(); */
/* 			} */
/* 			return * m_bbs_pInstance; */
/* 		} */
/* 		else */
/* 		{ */
/* 			if(m_pInstance == NULL) */
/* 			{ */
/* 				m_pInstance = new MODI_DBManager(); */
/* 			} */
/* 			return * m_pInstance; */
/* 		} */
		
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_DBManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
		
		if(m_bbs_pInstance)
		{
			delete m_bbs_pInstance;
		}
		m_bbs_pInstance = NULL;

		if(m_bill_pInstance)
		{
			delete m_bill_pInstance;
		}
		m_bill_pInstance = NULL;
	}

	/// 初始化,连接数据库,加载表
	bool Init(const std::string & url_info, bool is_queue=true);

	/// 加入一个新表
	bool AddNewTable(MYSQL * mysql_conn, const char * table_name);

	/**
	 * @brief 获得某个表
	 *
	 * @param table_name 表名
	 * @return 成功返回表,失败null
	 *
	 */
	MODI_TableStruct * GetTable(const std::string & table_name)
	{
		std::string temp_name = table_name;
		transform(temp_name.begin(), temp_name.end(), temp_name.begin(), ::tolower);

	   	defTableMapIter::iterator iter = m_TableMap.find(temp_name);
		if(iter != m_TableMap.end())
		{
			return iter->second;
		}
		return NULL;
	}

	/// 获得一个连接
	MODI_DBClient * GetHandle();

	/// 回收一个连接
	void PutHandle(MODI_DBClient * p_client);
	
 private:

	MODI_DBManager()
	{
		
	}
	
	~MODI_DBManager()
	{
		
	}
	
	/// 对象指针
	static MODI_DBManager * m_pInstance;
	static MODI_DBManager * m_bbs_pInstance;
	static MODI_DBManager * m_bill_pInstance;

	/// 连接信息
	MODI_URLInfo m_stURLInfo;

	/// 表管理
  	defTableMap  m_TableMap;

	/// 连接管理
	defClientMap m_ClientMap;

	/// 连接锁
	MODI_Mutex m_stMutex;
};


class MODI_ScopeHandlerRelease
{
	public:

	MODI_ScopeHandlerRelease(MODI_DBClient  * pDBClient);
	~MODI_ScopeHandlerRelease();


	private:

		MODI_DBClient * m_pDBClient;
};

#endif
