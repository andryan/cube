/**
 * @file Encrypt.h
 * @date 2010-01-21 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据加密管理
 *
 */

#ifndef _MDENCRYPT_H
#define _MDENCRYPT_H

#include "rc5.h"
#include "rc5_locl.h"

const unsigned char _key[16] = {0x91,0x5f,0x46,0x19,0xbe,0x41,0xb2,0x51,0x63,0x55,0xa5,0x01,0x10,0xa9,0xce,0x91};

/**
 * @brief 加密管理
 *
 */
class MODI_Encrypt
{
public:
	MODI_Encrypt()
	{
		m_stEncryptType = ENCRYPT_TYPE_NONE;
		RC5_32_set_key(&m_stRc5key,16,_key,8);
	}

	/// 加密类型
	enum enEncryptType
	{
		ENCRYPT_TYPE_NONE,
		ENCRYPT_TYPE_RC5,
		ENCRYPT_TYPE_UNKNOW
	};

	/// 设置加密方式
	void SetEncryptType(enEncryptType type)
	{
		m_stEncryptType = type;
	}

	/// 获取加密方式
	enEncryptType GetEncryptType() const
	{
		return m_stEncryptType;
	}

	void RC5SetKey(const unsigned char * _key, int key_len);
	void Encode(unsigned char  * data, unsigned int data_len);
	void Decode(unsigned char  * data, unsigned int data_len);

private:
	/// 加密方式
	enEncryptType m_stEncryptType;

	RC5_32_KEY m_stRc5key;
};

#endif
