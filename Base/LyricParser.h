//////////////////////////////////////////////////////////////////////////
//

#ifndef __LYRIC_PARSER_H__
#define __LYRIC_PARSER_H__

#include "AudioCommon.h"

const static string SENTENCE_START = "<sentence";
const static string SENTENCE_END   = "</sentence>";
const static int    NUM_ATTRI_TYPE = 8;
const static string TYPE = "@type=";
const static string INTERMISSION = "@intermission=";
const static string MULTIPLE = "@multiple=";
const static string ACT = "@act=";
const static string NPC_ACT = "@npc_act=";
const static string CAMERA = "@camera=";
const static string EFFECT = "@effect=";
const static string LIGHT = "@light=";

const static string ATTRI_TYPE[NUM_ATTRI_TYPE] = {TYPE, INTERMISSION, MULTIPLE, ACT, NPC_ACT, CAMERA, EFFECT, LIGHT};

struct SAttri
{
	string  m_strAttributeType;
	string  m_strValue;
};

typedef vector<SAttri> AttriVector;
typedef AttriVector::iterator AttriVectItor;

class MODI_LyricParser
{
public:
	MODI_LyricParser();
	~MODI_LyricParser();

	int Read(const unsigned char *str);
	string GetWord(){ return m_strWord;}
	AttriVector GetAttribute(){return m_AttriVector;}

	bool FindAttribute(const char *str, const char *ch);
    const char *GetLyric(const char *str);
	
private:
	void FindAttribute(const string &str, const string &attributeType);

private:
	string m_strWord;
	AttriVector m_AttriVector;

};

#endif
