/**
 * @file   PollThread.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Feb 23 15:55:23 2010
 * @version Ver0.1
 * @brief  轮询线程
 * 
 * 
 */

#ifndef _MDPOLLTHREAD_H
#define _MDPOLLTHREAD_H

#include <list>
#include <queue>
#include <deque>

#include "Global.h"
#include "Thread.h"
#include "ServiceTask.h"

/**
 * @brief 轮询线程
 * 
 */
class MODI_PollThread: public MODI_Thread
{
 public:
	virtual ~MODI_PollThread(){}

	/** 
	 * @brief 构造
	 * 
	 * @param name 线程名称
	 * 
	 */
	MODI_PollThread(const char * name): MODI_Thread(name)
	{
		m_dwTaskCount = 0;
	}

	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true,失败false
	 */
	virtual bool Init();

	
	/** 
	 * @brief 获取连接数
	 * 
	 * @return 连接数 
	 */
	int Size()
	{
		return m_dwTaskCount;
	}

	/** 
	 * @brief 增加一个连接到队列
	 * 
	 * @param p_task 要增加的连接
	 */
	virtual void AddTask(MODI_ServiceTask * p_task)
	{
		m_stRWLock.wrlock();
		m_TaskQueue.push(p_task);
		m_stRWLock.unlock();
	}

	/** 
	 * @brief 释放资源
	 * 
	 */
	virtual void Final() = 0;

	/** 
	 * @brief 删除某个连接
	 * 
	 * @param p_task 要删除的连接
	 */
	virtual void RemoveTask(MODI_ServiceTask * p_task) = 0;

 protected:
	/** 
	 * @brief 从队列里面获取任务到列表里
	 * 
	 */
	void GetTaskFromQueue();

	/** 
	 * @brief 连接注册到列表
	 * 
	 * @param p_task 要注册的task
	 */
	virtual void AddTaskToList(MODI_ServiceTask * p_task) = 0;
	
	/// epoll 句柄
	int m_iEpollfd;

	/// epoll事件队列
	std::vector<struct epoll_event > m_EpollEventVec;

	/// 事件队列
	std::list< MODI_ServiceTask * > m_TaskList;

	/// 事件缓冲队列
	std::queue<MODI_ServiceTask *, std::deque<MODI_ServiceTask * > > m_TaskQueue;

	DWORD m_dwTaskCount;

	/// 队列锁
	MODI_RWLock m_stRWLock;

	/// 时间
	MODI_RTime m_stCurrentTime;
};

#endif
