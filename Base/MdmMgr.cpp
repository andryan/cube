#include "MdmMgr.h"
#include "MIDILoader.h"
#include "LyricParser.h"
#include "Song.h"
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include "Global.h"
#include "BinFileMgr.h"
#include "ProgressBar.h"
#include "AssertEx.h"

MODI_MdmMgr::MODI_MdmMgr()
{

}

MODI_MdmMgr::~MODI_MdmMgr()
{
	CleanUP();
}

MODI_MdmMgr * 	MODI_MdmMgr::GetInstancePtr()
{
	static MODI_MdmMgr s_Inst;
	return &s_Inst;
}

/// 初始化管理器
bool MODI_MdmMgr::Initial(const char * szMdmFilesDir)
{
	std::string strPath = szMdmFilesDir;
	
	/// 遍历目录，装载*.mdm文件
	//
	DIR * pDir = opendir( szMdmFilesDir );
	if( !pDir )
	{
		Global::logger->fatal("[%s] MDM file folder:<%s>not exist. Fail to initialize MDM file manager." , SYS_INIT , szMdmFilesDir );
		return false;
	}

	int iErrcount = 0;
	struct stat fileState;
	struct dirent * pDirentp = 0;
	while( (pDirentp = readdir(pDir) ) )
	{
		if( !strcmp(".",pDirentp->d_name) || !strcmp("..",pDirentp->d_name) )
		{
			continue ;
		}

		std::string strFileFullPath = szMdmFilesDir;
		strFileFullPath += "/";
		strFileFullPath += pDirentp->d_name;
		stat(strFileFullPath.c_str(),&fileState);
		if( S_ISDIR(fileState.st_mode) )
			continue ;
		
		if( strFileFullPath.find_last_of(".mdm") != strFileFullPath.npos )
		{
			MODI_Song * pSong  = 0;
			MODI_MIDILoader * pLoader = 0;
			try 
			{
				pSong = new MODI_Song( strFileFullPath.c_str() );
				if( !pSong )
				{
					continue ;
				}

				// 是MDM文件，装载
				pLoader = new MODI_MIDILoader();
				MAP_MDM_ITER itor = m_mapMdm.find( pDirentp->d_name );
				if( itor == m_mapMdm.end() )
				{
					pLoader->SetSong( pSong );

					if( !pLoader->LoadFromMidiFileToBuffer( strFileFullPath.c_str() ) )
					{
						Global::logger->fatal("[%s] Fail to load MDM file <%s>." , SYS_INIT , strFileFullPath.c_str() );
						throw "fail to load mdm file.";
						iErrcount += 1;
						continue ;
					}

					if( !pLoader->ProcessMIDIEvent() )
					{
						Global::logger->fatal("[%s] Fail to Analyze MDM file <%s>." , SYS_INIT , strFileFullPath.c_str() );
						throw "fail to Analyze mdm file.";
						iErrcount += 1;
						continue ;
					}

					std::string strBaseName = pDirentp->d_name;
					size_t nPos = strBaseName.find_last_of(".");
					strBaseName.erase( nPos , strBaseName.size() - nPos );
					m_mapMdm.insert( std::make_pair( strBaseName.c_str() , pLoader ) );

					//Global::logger->debug("[%s] load mdm file =%s ." , SVR_TEST ,strBaseName.c_str() );
				}
				else 
				{
					Global::logger->fatal("[%s] Fail to load duplicated MDM file: <%s> .." , SYS_INIT , strFileFullPath.c_str() );
					throw "fail to load duplicated mdm file.";
					iErrcount += 1;
				}
			}
			catch(...)
			{
				delete pSong;
				delete pLoader;
			}
		}
	};
		
	closedir( pDir );
	return iErrcount > 0 ? false : true;
}


/// 检查MDM资源是否完整有效
bool  MODI_MdmMgr::CheckResource( MODI_BinFileMgr * pBinMgr )
{
	size_t nErr = 0;
	size_t nMusicSize =	pBinMgr->GetMusicBinFile().GetSize();
//	MODI_BarGoLink_Logger processBar( nMusicSize );

	for( size_t n = 0; n < nMusicSize; n++ )
	{
		const GameTable::MODI_Musiclist * pTemp = pBinMgr->GetMusicBinFile().GetByIndex( n );
		if( pTemp )
		{
			if(Find(  pTemp->get_ConfigID() ) == false )
			{
				char szSongName[512];
				SNPRINTF( szSongName , sizeof(szSongName) , "%u" , pTemp->get_ConfigID() );
				Global::logger->fatal("[%s] Fail to find matching MDM file <%s> with music ID <%u>.." , SYS_INIT , 
					 szSongName ,   pTemp->get_ConfigID() );
				nErr += 1;
			}
			else 
			{
				
				/* 如果资源存在
				 * 对资源内容的有效性进行检查
				 * 之后逻辑中再用到资源，则不再对有效性进行检查判断
				 * 			by tt
				 * */
				// 歌词句数必须大于0
				if( !GetSencentsSize( pTemp->get_ConfigID() ) )
				{
					Global::logger->fatal("[%s] Invalid MDM file <%s.mdm>, total lyric count equals zero.." , SYS_INIT , pTemp->get_Songname() );
					nErr += 1;
				}

				float fLength = 0.0f;
				// note 长度总和必须大于0
				if( !this->GetMusicTotalNoteLength( pTemp->get_ConfigID() , fLength ) )
				{
					Global::logger->fatal("[%s] Invalid MDM file <%s.mdm>, total note count equals zero." , SYS_INIT , pTemp->get_Songname() );
					nErr += 1;
				}
				else 
				{
					assert( fLength > 0.0f );
					
					/// show time 需要条件
//					float fShowTime = fLength  * SHOWTIME_PERCENT_IN_TOTALNODE;
//					Global::logger->debug("[%s] song <%s>, ShowTime requires <%f> second performance above good level. " , SYS_INIT , pTemp->get_Songname() , 
//							fShowTime );
				}
			}
		}

//		processBar.step();
	}

	return nErr == 0;
}

/// 获得一首音乐的对应的MDM数据流
bool  MODI_MdmMgr::GetMdmBuffer( defMusicID nMusicID , char * pOutBuf , unsigned int &  nOutBufSize )
{
	bool bRet = false;
	MODI_MIDILoader * pMdm = Find( nMusicID );
	if( pMdm )
	{
		unsigned int nCpySize = std::min( nOutBufSize , pMdm->GetRawDataSize() );
		memcpy( pOutBuf , pMdm->GetRawData(), nCpySize );
		nOutBufSize = nCpySize;
		bRet = true;
	}

	return bRet;
}

/// 获得一首音乐的歌词句数
unsigned int MODI_MdmMgr::GetSencentsSize( defMusicID nMusicID )
{
	MODI_Song * pSong = FindSong( nMusicID );
	if( pSong )
	{
		return	pSong->GetNumSentence();
	}

	return 0;
}

/// 获得一首音乐中某句歌词的开始和结束时间
bool  MODI_MdmMgr::GetSencentsRange( defMusicID nMusicID , unsigned int nSen , float & fBegin , float  & fEnd )
{
	MODI_Song * pSong = FindSong( nMusicID );
	if( pSong )
	{
		if( pSong->GetNumSentence() < (int)nSen )
			return false;

		pSong->HeadSentence();
		unsigned int nCur = 1;
		while( nCur < nSen && pSong->GetCurSentence() )
		{
			pSong->NextSentence();
			nCur += 1;	
		}
		
		if( nCur != nSen )
			return false;
	
		if( pSong->GetCurSentence() )
		{
			fBegin = (float)pSong->GetCurSentence()->GetStartTime();
			fEnd = (float)pSong->GetCurSentence()->GetEndTime();
			return true;
		}
	}

	return false;
}

/// 获得一首音乐的长度
bool  MODI_MdmMgr::GetMusicLength( defMusicID nMusicID , float & fLength )
{
	const GameTable::MODI_Musiclist * pTemp =  MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get( nMusicID );
	if( pTemp )
	{
		// 只精确到(ms)
		fLength = (float)pTemp->get_Duration();
		return true;
	}

	return false;
}

/// 获得一首音乐所有NOTE的长度总合
bool  MODI_MdmMgr::GetMusicTotalNoteLength( defMusicID nMusicID , float & fLength )
{
	MODI_Song * pSong = FindSong( nMusicID );
	if( pSong )
	{
		fLength = 0.0f;
		MODI_Note * pHeadNode = pSong->GetHeadNote();
		MODI_Note * pNextNode = 0;
		MODI_Lyric * pHeadLyric = pSong->GetHeadLyric();
		float nextStartTime = 0.0f;

		while( pHeadNode )
		{
			float fStartTime = pHeadNode->GetStartTime();
			float fEndTime = pHeadNode->GetEndTime();
			pNextNode = pHeadNode->GetNext();
			if (pNextNode!=NULL)
			{
				nextStartTime = pNextNode->GetStartTime();
				if( fEndTime > nextStartTime )
				{
					fEndTime = nextStartTime;
				} 
			}

			if (pHeadLyric->GetLastNote()==pHeadNode)
			{
				fEndTime=fStartTime+((fEndTime-fStartTime)*5.0)/6.0;
				pHeadLyric =pHeadLyric->GetNext();
			}

			fLength += (fEndTime - fStartTime); 
			pHeadNode = pNextNode;
		}

		return true;
	}

	return false;
}

MODI_MIDILoader * 	MODI_MdmMgr::Find( defMusicID nMusicID )
{
	MODI_MIDILoader * pRet = 0;
	
	const GameTable::MODI_Musiclist * pMusicItem =  MODI_BinFileMgr::GetInstancePtr()->GetMusicBinFile().Get( nMusicID );
	if( pMusicItem )
	{
//		const char * szSongName = pMusicItem->get_Songname();
		char szSongName[512];
		SNPRINTF( szSongName , sizeof(szSongName) , "%u" , pMusicItem->get_ConfigID() );
		MAP_MDM_ITER itor = m_mapMdm.find( szSongName );
		if( itor != m_mapMdm.end() )
		{
			pRet = itor->second;
		}
	}
	else
	{
		Global::logger->info("[%s] can't find musicitem in musiclist.by configid<%u>" , SVR_TEST ,
			   nMusicID );	
	}

	return pRet;
}

MODI_Song * MODI_MdmMgr::FindSong( defMusicID nMusicID )
{
	MODI_MIDILoader * pLoader = Find( nMusicID );
	if( pLoader )
	{
		MODI_Song * pSong = pLoader->GetSong();
		return pSong;
	}
	return 0;
}

void  MODI_MdmMgr::CleanUP()
{
	MAP_MDM_ITER itor = m_mapMdm.begin();
	while( itor != m_mapMdm.end() )
	{
		delete itor->second;
		itor++;
	}
	m_mapMdm.clear();
}
