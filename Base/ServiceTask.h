/**
 * @file ServiceTask.h
 * @date 2009-12-21 CST
 * @verison $Id $
 * @author hurixin hurixin@modi.com
 * @brief 服务器模型连接
 *
 */

#ifndef _MDSERVICETASK_H
#define _MDSERVICETASK_H

#include "Type.h"
#include "Timer.h"
#include "Socket.h"

class MODI_TaskQueue;

/**
 * @brief 服务器模型连接
 *
 * 客户端连接服务器时,此对象被动产生
 *
 */
class MODI_ServiceTask: public MODI_DisableCopy
{
 public:
	/// 连接状态
	enum enTaskState
	{
		/// 空闲状态
		enNoUseState,
		/// 使用状态
		enNormalState,
		/// 回收状态
		enRecycleState
	};

	/** 
	 * @brief 清除检测错误次数
	 * 
	 */
	void ClearCheckCount()
	{
		m_byCheckCount = 0;
	}
	
	/** 
	 * @brief 获取检测信号次数
	 * @return 检测信号错误次数
	 */
	BYTE GetCheckCount()
	{
		return m_byCheckCount;
	}

	/**
	 * @brief 设置连接的状态
	 *
	 * @param state 设置的状态
	 *
	 */ 
	inline void SetState(enTaskState  state)
	{
		m_enTaskState = state;
	}

	/**
	 * @brief 返回连接的状态
	 *
	 * @return 连接的状态
	 *
	 */ 
	inline const enTaskState & GetState() const
	{
		return m_enTaskState;
	}

	/** 
	 * @brief 获取下一个状态
	 * 
	 */
	void GetNextState();

	/** 
	 * @brief 是从池中拿的
	 * 
	 */
	inline void SetInBuf()
	{
		m_blIsInBuf = true;
	}

	inline bool IsInBuf()
	{
		return m_blIsInBuf;
	}
	
	/**
	 * @brief 置连接结束标志
	 *
	 */ 
	void Terminate()
	{
		m_blTerminate = true;
		DisConnect();
	}

	/**
	 * @brief 连接是否结束
	 * @return 接收true,失败false
	 *
	 */ 
	bool IsTerminate()
	{
		return m_blTerminate;
	}

	/// 置服务器结束标志
	void TerminateFinal()
	{
		m_blTerminateFinal = true;
	}

	/**
	 * @brief 服务器是否结束
	 *
	 * @return 结束true,失败false
	 *
	 */
	bool IsTerminateFinal()
	{
		return m_blTerminateFinal;
	}

	/**
	 * @brief 构造
	 *
	 */
	MODI_ServiceTask(MODI_TaskQueue * p_queue)
	{
		m_pSocket = new MODI_Socket(-1, NULL);
		m_stLiftTime.GetNow();
		m_stCheckConnTime.GetNow();
		m_enTaskState = enNoUseState;
		m_blTerminate = false;
		m_blTerminateFinal = false;
		m_blBuffer = false;
		m_blEnableCheck = false;
		m_blCheckTimeOut = false;
		m_blCheckCmdFlux = false;
		m_blAddRead = false;
		m_byCheckCount = 0;
		m_dwRecvCmdCount = 0;
		m_blIsInBuf = true;
		m_pTaskQueue = p_queue;
	}
	
	/**
	 * @brief 构造
	 * @param sock 连接的句柄
	 * @param addr 连接的地址
	 *
	 */
	MODI_ServiceTask(const int sock, const struct sockaddr_in * addr)
	{
		m_pSocket = new MODI_Socket(sock, addr);
		m_stLiftTime.GetNow();
		m_stCheckConnTime.GetNow();
		m_enTaskState = enNoUseState;
		m_blTerminate = false;
		m_blTerminateFinal = false;
		m_blBuffer = false;
		m_blEnableCheck = false;
		m_blCheckTimeOut = false;
		m_blCheckCmdFlux = false;
		m_blAddRead = false;
		m_byCheckCount = 0;
		m_dwRecvCmdCount = 0;
		m_blIsInBuf = false;
		m_pTaskQueue = NULL;
	}

	virtual void Init(const int s_sock, const struct sockaddr_in * s_addr, MODI_TaskQueue * task_queue);

	virtual void Reset()
	{
		m_pSocket->Reset();
	}
	

	virtual ~MODI_ServiceTask()
	{
		if(m_pSocket)
		{
			delete m_pSocket;
		}
		m_pSocket = NULL;
	}


	/**
	 * @brief 获取连接的IP地址
	 *
	 * @return 字符形式的IP
	 *
	 */
	inline const char * GetIP() const
	{
		if(m_pSocket)
		{
			return m_pSocket->GetIP();
		}
		return 0;
	}

	/**
	 * @brief 获取端口
	 *
	 * @return 本地字节顺序的端口号
	 *
	 */
	inline const WORD GetPort() const
	{
		if(m_pSocket)
		{
			return m_pSocket->GetPort();
		}
		return 0;
	}

	/**
	 * @brief 获取IP地址
	 *
	 * @return 本地的字节顺序的IP地址 
	 *
	 */ 
	inline const DWORD GetIPNum() const
	{
		if(m_pSocket)
		{
			return m_pSocket->GetIPNum();
		}
		return 0;
	}

	/**
	 * @brief 获取服务器端口
	 *
	 * @return 服务器绑定的端口
	 *
	 */ 
	inline const WORD GetServicePort() const
	{
		if(m_pSocket)
		{
			return m_pSocket->GetServicePort();
		}
		return 0;
	}

	/** 
	 * @brief 校验连接
	 * 
	 * @return 成功返回1,未验证0,错误-1
	 */
   	int CheckVerify();

	/** 
	 * @brief 等待同步
	 * 
	 * @return 已经同步返回1,失败返回-1,继续等待0
	 */
	virtual int WaitSync()
	{
		return 1;
	}
	
	/// 回收连接
	virtual int RecycleConn() = 0;

	virtual void DisConnect()
	{
		
	}

	/// 增加到管理器中
	virtual void AddTaskToManager()
	{
		
	}

	/// 从管理器中删除
	virtual void RemoveFromManager()
	{
		
	}
	
	/** 
	 * @brief 检测连接是否验证超时
	 * 
	 * @param real_time 当前时间
	 * 
	 * @return 超时true
	 */
	inline bool CheckTimeOut(const MODI_RTime & real_time, const DWORD delay_mses = 8000)
	{
		if(m_blCheckTimeOut)
		{
			if((delay_mses + m_stLiftTime.GetMSec()) >  real_time.GetMSec())
			{
				return false;
			}
			return true;
		}
		return false;
	}

	inline void EnableCheckFlux()
	{
		m_blCheckCmdFlux = true;
	}

	inline bool IsCheckFlux()
	{
		return m_blCheckCmdFlux;
	}

	/** 
	 * @brief 是否到发送检测的时间
	 * 
	 * @param current_time 当前的时间
	 * 
	 * @return 到true,未到false
	 */
	inline bool CheckConnTime(const MODI_RTime & current_time)
	{
		if(m_stCheckConnTime < current_time)
		{
			m_stCheckConnTime.Delay(8000);
			return true;
		}
		return false;
	}

	/** 
	 * @brief 发送检测命令
	 * 
	 */
	virtual void CheckConn()
	{
		Cmd::stNullCmd rt_cmd;
		if(SendCmd(&rt_cmd, sizeof(rt_cmd)))
		{
			m_byCheckCount++;
		}
	}

	/**
	 * @brief 是否需要检测
	 *
	 * @return 要检测返回true
	 */
	bool IsEnableCheck()
	{
		return m_blEnableCheck;
	}

	void SetEnableCheck()
	{
		m_blEnableCheck = true;
	}

	bool IsAddRead()
	{
		return m_blAddRead;
	}

	void SetAddRead()
	{
		m_blAddRead = true;
	}

	/**
	 * @brief 增加接收命令计数
	 * 
	 */
	void AddRecvCount()
	{
		m_dwRecvCmdCount++;
	}

	/** 
	 * @brief 获取命令计数
	 * 
	 * 
	 * @return 命令计数
	 */
	const DWORD GetRecvCount()
	{
	   	return m_dwRecvCmdCount;
	}

	/** 
	 * @brief 复位
	 * 
	 */
	void ClearRecvCount()
	{
		m_dwRecvCmdCount = 0;
	}
	
	/**
	 * @brief 发送命令
	 *
	 * @param p_send_cmd 发送命令的内容
	 * @param cmd_size 发送命令的大小
	 * @param is_buffer 是否使用队列发送(默认队列发送)
	 * @return 发送成功true,失败false
	 *
	 */ 
	bool SendCmd(const Cmd::stNullCmd * p_send_cmd, const int cmd_size, bool is_zip = false)
	{
		if(m_pSocket)
		{
			return m_pSocket->SendCmd(p_send_cmd, cmd_size,m_blBuffer, is_zip);
		}
		else
		{
			Global::logger->error("[send_cmd] service task socket is null <ip=%s,port=%d>", GetIP(), GetPort());
			return false;
		}
	}

	bool SendCmdNoBuffer(const Cmd::stNullCmd * p_send_cmd, const int cmd_size, bool is_zip = false)
	{
		if(m_pSocket)
		{
			return m_pSocket->SendCmd(p_send_cmd, cmd_size,false, is_zip);
		}
		else
		{
			Global::logger->error("[send_cmd] service task socket is null <ip=%s,port=%d>", GetIP(), GetPort());
			return false;
		}
	}

	/// 接收数据
	virtual bool RecvDataNoPoll();

	/// 发送数据
	bool SendDataNoPoll();

	/**
	 * @brief 注册到epoll事件
	 *
	 * @param epoll_fd epoll句柄
	 * @param evts 要轮轮询事件
	 * @param p_data 要轮询的对象指针
	 *
	 */ 
	void AddEpoll(int epoll_fd, uint32_t evts, void * p_data)
	{ 
		if(m_pSocket)
		{
			m_pSocket->AddEpoll(epoll_fd, evts, p_data);
		}
	}

	/**
	 * @brief 删除epoll事件
	 *
	 * @param epoll_fd epoll句柄
	 * @param evts 轮询的事件
	 *
	 */
	void DelEpoll(int epoll_fd, uint32_t evts)
	{
		if(m_pSocket)
		{
			m_pSocket->DelEpoll(epoll_fd, evts);
		}
	}

	void AddRecycleQueue();
	
	
 protected:
	/// 套接口
	MODI_Socket * m_pSocket;


	/** 
	 * @brief 该连接的命令处理
	 * 
	 * @param pt_null_cmd 该连接发送到服务器的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 */
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size) = 0;
	

   	/** 
	 * @brief 检查校验命令是否正确
	 * 
	 * @param pt_null_cmd 校验的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功1,失败-1,继续0(可以校验多次)
	 */
	virtual int CheckVerifyCmd(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
	{
		return 1;
	}

	/// 生成时间
	MODI_RTime m_stLiftTime;

	/// 检测连接的定时器
	MODI_RTime m_stCheckConnTime;
	
	/// 信号检测失败次数
	BYTE m_byCheckCount;

	//是否需要发送检测信号
	bool m_blEnableCheck;

	/// 是否需要检查超时连接
	bool m_blCheckTimeOut;
	bool m_blCheckCmdFlux;

	/// 状态
	enTaskState m_enTaskState;
	
 private:
	/// 是否结束
	bool m_blTerminate;
	bool m_blTerminateFinal;

	/// 是否buffer发送
	bool m_blBuffer;

	bool m_blAddRead;

	/// 接收到的命令数
	DWORD m_dwRecvCmdCount;

	/// 是否是池里面的
	bool m_blIsInBuf;

	MODI_TaskQueue * m_pTaskQueue;
};

#endif
