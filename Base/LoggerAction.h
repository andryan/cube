/**
 * @file LoggerAction.h
 * @date 2009-12-31 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 日志动作类
 *
 */

#ifndef _LOGGERACTION_H
#define _LOGGERACTION_H

/***************** 中文日志系统动作定义 *******************/
#ifdef _LOGGER_CHINESE
#define SYS_INIT "系统初始化"
#define LOGACT_SYS_INIT "系统初始化"
#define LOGACT_SOCK_DATA "网络数据"
#define LOGACT_MEM "内存异常"
#define LOGACT_DB_OPT "数据库操作"
#define LOGACT_CMD_RECV "命令接收"
#define LOGACT_CMD_SEND "发送命令"
#define LOGACT_DEBUG "调试"
#define LOGACT_TASK_SCHED "任务调度"

#define SOCK_DATA "网络数据"
#define CMD_RECV "命令接收"
#define CMD_SEND "命令发送"
#define CMD_PARSE "命令分析"
#define SVR_TEST "游戏测试"

#define RECORD_OPT "记录操作"
#define LOGACT_URLINFO "URL信息"

#define SERVER_REGISTER "Server Register"

#define GS_ROOMINFO "Room Information"
#define GS_ROOMOPT  "Room_Operate"
#define GS_AVATAROPT  "Avatar_Operate"
#define SVR_RESOURCES_ERR "Game_Resoureces_Error"

#define LOGIN_OPT   "Login Operate"
#define LOGIN_CHANGECHANNEL  "Change Game Channel"
#define LOGIN_AUTH  "Login Auth"
#define ROLEDATA_OPT "RoleData Operate"

#define ERROR_PACKAGE "Error Package"
#define ERROR_CON "Error Connection"
#define ERROR_CGUID "Error CreateGUID"
#define SYSTEM_SIGNAL "System Signal"

#define GAME_SESSION "Game Session"
#define KICK_CLIENT "Kick Client"

#define GAMEDB_OPT  "GameDB Operate"
#define IMPACT_MODULE "Impact Module"
#define EXPRESSION_MODULE "Expression Module"
#define RELATION_MODULE "Relation Module"
#define ITEM_MODULE "Item Module"
#define MAIL_MODULE "Mail Module"
#define SHOP_MODULE "Shop Module"
#define CLIENT_EXCEPTION "Client Exception"

#endif

/***************** 英文日志系统动作定义 *******************/
#ifdef _LOGGER_ENGLISH
#define LOGACT_SYS_INIT "system_init"
#define LOGACT_SOCK_DATA "socket_data"
#define LOGACT_MEM "mem_out"
#define LOGACT_DB_OPT "db_operation"
#define LOGACT_CMD_RECV "cmd_recv"
#define LOGACT_CMD_SEND "cmd_send"
#define LOGACT_DEBUG "debug"
#define LOGACT_TASK_SCHED "task_sched"


#define SYS_INIT "系统初始化"
#define SOCK_DATA "socket_data"
#define CMD_RECV "cmd_recv"
#define CMD_SEND "cmd_send"
#define CMD_PARSE "cmd_parse"
#define SVR_TEST "test"
#define MEM_OUT "buffer out"
#define RECORD_OPT "record opt"
#define LOGACT_URLINFO "URLInfo"

// add by tt
#define SERVER_REGISTER "Server Register"
#define GS_ROOMINFO "Room Information"
#define GS_ROOMOPT  "Room Operate"
#define GS_AVATAROPT  "Avatar Operate"
#define SVR_RESOURCES_ERR "Game Resoureces Error"

#define LOGIN_OPT   "Login Operate"
#define LOGIN_AUTH  "Login Auth"
#define LOGIN_CHANGECHANNEL  "Change Game Channel"
#define ROLEDATA_OPT "RoleData Operate"

#define ERROR_PACKAGE "Error Package"
#define ERROR_CON "Error Connection"
#define ERROR_CGUID "Error CreateGUID"
#define SYSTEM_SIGNAL "System Signal"

#define GAME_SESSION "Game Session"
#define KICK_CLIENT "Kick Client"

#define GAMEDB_OPT  "GameDB Operate"
#define IMPACT_MODULE "Impact Module"
#define EXPRESSION_MODULE "Expression Module"
#define RELATION_MODULE "Relation Module"
#define ITEM_MODULE "Item Module"
#define MAIL_MODULE "Mail Module"
#define SHOP_MODULE "Shop Module"
#define CLIENT_EXCEPTION "Client Exception"

#endif

/***************** 英文日志系统动作定义结束 *******************/

#endif
