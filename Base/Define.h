/**
 * @file Define.h
 * @date 2010-01-21 CST
 * @version $Id $
 * @author hurixin hrixin@modi.com
 * @brief 一些基本的常量定义
 *
 */

#ifndef _MDDEFINE_H
#define _MDDEFINE_H

#include "BaseOS.h"
#include "Type.h"

/// 日志信息最大长度
#define MAX_LOGGERSIZE 4096

/// 名字的最大长度
#define MAX_NAMESIZE 32

/// 账号最大长度
#define MAX_ACCNAMESIZE	48

/// IP地址最大长度
#define MAX_IPSIZE 16

/// 大于此的包才压缩
#define MIN_ZIPSIZE 64

/// 每秒命令个数限制
const DWORD CMD_FLUX_PER_SECOND = 100;

/// GW发送到GS的命令池大小
const DWORD GWTOGS_CMDSIZE = 40960;
const DWORD GSTOGW_CMDSIZE = 40960;
const DWORD ZSTODB_CMDSIZE = 40960;
const DWORD GSTOZS_CMDSIZE = 40960;
const DWORD ZSTOGS_CMDSIZE = 40960;
const DWORD SVR_CMDSIZE = 40960;


/// socket 的一些常量
namespace Skt
{
	/// 一次发送的最大数据;
	const DWORD MAX_SOCKETSIZE = 64 * 1024;

	/// 包头长度
	const BYTE PACKET_HEAD_LEN = sizeof(DWORD);

	/// 最大的数据包;
	const DWORD MAX_PACKETSIZE = MAX_SOCKETSIZE - PACKET_HEAD_LEN;

	/// 最大的用户包数据
	const DWORD MAX_USERDATASIZE = MAX_PACKETSIZE - 128;

};

#define ACCOUNT_DB_URL 	"AccountDBURL"
#define GAME_DB_URL "GameDBURL"


#endif
