#include "Base/DisableStrTable.h"
#include <fstream>

MODI_DisableStrTable::MODI_DisableStrTable()
{

}

MODI_DisableStrTable::~MODI_DisableStrTable()
{

}

bool MODI_DisableStrTable::Init(const char * szFileName )
{
	if( !szFileName || !szFileName[0] )
		return false;


	std::ifstream inFile;
	inFile.open( szFileName );
	if ( !inFile )
	{
		inFile.close();
		return false;
	}

	CleanUP();

	std::string strLine;
	while ( inFile>>strLine )
	{
		m_vecDirtyWord.push_back(strLine);
	}

	inFile.close();

	return true;
}


bool MODI_DisableStrTable::CheckDirtyWord(const char* str) const
{
	if( !str || !str[0] )
		return false;

	std::string strCheck = str;
	MODI_DirtyWordList_C_Iter itor  = m_vecDirtyWord.begin();
	while( itor != m_vecDirtyWord.end() )
	{
		const std::string & strTemp = *itor;
		if ( strCheck.find( strTemp ) != std::string::npos)
		{
			return true;
		}
		itor++;
	}
	return false;
}
	
void MODI_DisableStrTable::CleanUP()
{
	m_vecDirtyWord.clear();
}
