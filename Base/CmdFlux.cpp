/**
 * @file CmdFlux.h
 * @date 2010-04-14 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief command flux parse
 *
 */

#include "CmdFlux.h"
#include "Global.h"

void MODI_CmdFlux::Put(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return; 
	}
	
	if(pt_null_cmd->byCmd >= CMD_COUNT)
	{
		return;
	}
	
	m_stMutex.lock();
	m_stCmdInfo[pt_null_cmd->byCmd][pt_null_cmd->byParam].m_qwCount++;
	m_stCmdInfo[pt_null_cmd->byCmd][pt_null_cmd->byParam].m_qwSize += cmd_size;
	m_stMutex.unlock();
}

void MODI_CmdFlux::ShowInfo(bool bl_info)
{
	m_stMutex.lock();
	CmdInfo cmd_flux[CMD_COUNT][PARA_COUNT];
	memset(cmd_flux, 0, sizeof(cmd_flux));
	memcpy(cmd_flux, m_stCmdInfo, sizeof(cmd_flux));
	m_stMutex.unlock();
	
	int cmd = 0;
	QWORD total_count = 0;
	QWORD total_size = 0;

	for(; cmd < CMD_COUNT; cmd++)
	{
		int param = 0;
		for(; param < PARA_COUNT; param++)
		{
			if(cmd_flux[cmd][param].m_qwCount > 0)
			{
				if(bl_info)
				{
					Global::logger->debug("[cmd_flux] %s command(cmd=%d,param=%d) count=%llu, size=%llu",m_strName.c_str(),
									  cmd, param,
									  cmd_flux[cmd][param].m_qwCount, cmd_flux[cmd][param].m_qwSize);
				}
				total_count += cmd_flux[cmd][param].m_qwCount;
				total_size += cmd_flux[cmd][param].m_qwSize;
			}
		}
	}
	
	if((last_size + (25 * 1024 * 1024)) < total_size )
	{
		Global::logger->warn("[cmd_flux] %s command size more than %lluByte/s", m_strName.c_str(), (total_size - last_size));
	}
	last_size = total_size;

	if((last_count + 3800) < total_count)
	{
		Global::logger->debug("[cmd_flux] %s command count more than %lluCount/s", m_strName.c_str(), (total_count - last_count));
	}
	last_count = total_count;
	
	Global::logger->debug("[cmd_flux] %s command flux total_count=%llu, total_size=%llu", m_strName.c_str(), total_count, total_size);
}
