#include "Base/2DBitArray.h"



MODI_BitArray2D::MODI_BitArray2D(unsigned int dwWidth1,unsigned int dwWidth2):MODI_BitArray(dwWidth1 * dwWidth2)
{
	m_dwWidth = dwWidth2;
}

MODI_BitArray2D::~MODI_BitArray2D()
{

}

MODI_BitArray2D::MODI_BitArray2D(const MODI_BitArray2D & Array):MODI_BitArray(Array)
{
	m_dwWidth = Array.m_dwWidth;
}

