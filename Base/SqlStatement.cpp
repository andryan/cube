#include "SqlStatement.h"
#include "BaseOS.h"
#include "AssertEx.h"
#include "gamestructdef.h"
#include "HelpFuns.h"
#include <mysql.h>
#include "UserOtherData.h"

//'accounts' 
//'characters' 


char 			MODI_SqlStatement::ms_szSqlEscapeBuf[enSqlEscapeBufSize];

char 			MODI_SqlStatement::ms_szSqlStatementBuf[enSqlStatementBufSize];

const char * 	MODI_SqlStatement::ms_SelectMax = "SELECT MAX(%s) FROM %s ;";

const char * 	MODI_SqlStatement::ms_szSelectAccount = "SELECT * FROM %s WHERE `account` = \'%s\' and `password` = \'%s\';";

const char * 	MODI_SqlStatement::ms_szSelectCharslist = "SELECT * FROM %s WHERE `accountid` = %u;";

const char * 	MODI_SqlStatement::ms_szInsertChar = "INSERT INTO %s (`guid` , `accountid`,`accname`,`name`, `sex` ,`city` , `birthday`, \
`blood`,`qq`,`personalsign`,`honeyinfo` ,`defavatar`)VALUES (%u,%u,\'%s\',\'%s\',%u, %u,\'%s\',%u,%u,\'%s\',\'%s\',%u);"; 

const char * 	MODI_SqlStatement::ms_szLoadCharData = "SELECT * , UNIX_TIMESTAMP(`lastlogintime`) , UNIX_TIMESTAMP(`lastidoltime`) , UNIX_TIMESTAMP(`registertime`) FROM %s WHERE `guid` = %u and `accountid` =%u;";
const char * 	MODI_SqlStatement::ms_szLoadCharDataByName = "SELECT * FROM %s WHERE `name` = \'%s\' ;";

const char * 	MODI_SqlStatement::ms_szUpdateCharData = "UPDATE %s SET \
`age`=%u,`city`=%u,`personalsign`=\'%s\',`clan`=%u,`group`=%u,`exp`=%u,`qq`=%u, \
`blood`=%u,`level`=%u, `birthday`=\'%s\' , `votecount`=%u ,\
`wincount`=%u,`losscount`=%u,`tiecount`=%u ,`readhelp`=%d ,\
`normalflags`=\'%s\' ,`chang_count`=%u,`heng_count`=%u,`keyboard_count`=%u,`vote_count`=%u,`total_online_time`=%u, \
`today_online_time`=%u, `highest_score`=%u , `highest_score_musicid`=%u , `highest_precision`=%f , `highest_precision_musicid`=%u, \
`block_count`=%u, `chenghao`=%u WHERE `guid`=%u;";

const char * 	MODI_SqlStatement::ms_szResetOnlineTime = "UPDATE %s SET `today_online_time`=0";

const char * 	MODI_SqlStatement::ms_szIsHasChar = "SELECT name FROM %s WHERE `name` =\'%s\';";

const char * 	MODI_SqlStatement::ms_szUpdateLastLogin = "UPDATE %s SET `lastlogintime`=FROM_UNIXTIME(%llu), `lastloginip`= \'%s\' WHERE `guid`=%u;";

const char * 	MODI_SqlStatement::ms_AccountTable = "accounts";

const char * 	MODI_SqlStatement::ms_CharsTable = "characters";

//const char * 	MODI_SqlStatement::ms_ItemTable = "iteminfos";

//const char * 	MODI_SqlStatement::ms_ItemSerialTable = "itemserial";

const char * 	MODI_SqlStatement::ms_RelationTable = "relations";

const char * 	MODI_SqlStatement::ms_MailTable = "mails";

const char * 	MODI_SqlStatement::ms_ShopHistroyTable = "shophistroy";

const char * 	MODI_SqlStatement::ms_YunYingCDKeyTable = "cdkeytable";

//const char * 	MODI_SqlStatement::ms_szLoadItemlist = "SELECT * , UNIX_TIMESTAMP(`expire_time`) FROM %s WHERE accid = %u";

//const char * 	MODI_SqlStatement::ms_szLoadAvatarID = "SELECT `configid` , `itempos` FROM %s WHERE `charguid` = %u and `isvalid` = 1 and `bagtype` = 1";

//const char * 	MODI_SqlStatement::ms_szSaveItem = "CALL Save_Item(%u,%u,%u,%u,%u,%llu,%u,%u,%u,%d);";

//const char * 	MODI_SqlStatement::ms_szSaveShopHistroy = "INSERT INTO %s (`transid`,`charid`,`itemserial` , `createtype`,`mailid`,`recvstate`) 
//VALUES(%llu,%u,%u,%u,%llu,%u)";

const char * 	MODI_SqlStatement::ms_szGetYunYingCDKey = "SELECT `items` FROM %s WHERE `cdkey`=\'%s\' and `isvalid`=1";

const char * 	MODI_SqlStatement::ms_szSaveYunYingCDKey = "UPDATE %s SET `isvalid`=%d,`charid`=%u WHERE `cdkey`=\'%s\'";

//const char * 	MODI_SqlStatement::ms_szLoadServerItemSerial = "SELECT * FROM %s WHERE `serverid` = %u;";

//const char * 	MODI_SqlStatement::ms_szSaveServerItemSerial= "UPDATE %s SET `serial` = %u WHERE `serverid` = %u;"; 

//const char * 	MODI_SqlStatement::ms_szClearupInvalidItems = "UPDATE %s SET `isvalid` = 2 WHERE `world`=%d and `server`=%d and `serial`>=%u ; ";

const char * 	MODI_SqlStatement::ms_LoadRelation =  "SELECT * FROM %s WHERE `charid` = %u and isvalid = 1;";

const char * 	MODI_SqlStatement::ms_SaveRelation = "CALL Save_Relation(%u,%u,%d,%u,\'%s\',%u);";

const char * 	MODI_SqlStatement::ms_SaveSocialRelData = "UPDATE %s SET `renqi` =%u , `fensigril`=%u , `fensiboy`=%u , \
														   `honeyinfo` = \'%s\'  ,`lastidoltime`=FROM_UNIXTIME(%llu) WHERE `guid`= %u ; ";

const char * 	MODI_SqlStatement::ms_Rank_Renqi = "SELECT guid,name , chenghao,renqi,fensigril,fensiboy,votecount\
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`renqi` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Self_Rank_Renqi = "SELECT guid \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`renqi` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Rank_Level = "SELECT guid,name,renqi,chenghao,level \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`level` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Self_Rank_Level = "SELECT guid\
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`level` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Rank_ConsumeRMB = "SELECT guid,name,chenghao,consume_rmb \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`consume_rmb` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Self_Rank_ConsumeRMB = "SELECT guid \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`consume_rmb` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Rank_HighestScore = "SELECT guid,name,chenghao,highest_score_musicid , highest_score \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`highest_score` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Self_Rank_HighestScore = "SELECT guid \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`highest_score` DESC\
	LIMIT 0 , %u;";


const char * 	MODI_SqlStatement::ms_Rank_HighestPrecision = "SELECT guid,name,chenghao,highest_precision_musicid ,  highest_precision \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`highest_precision` DESC\
	LIMIT 0 , %u;";

const char * 	MODI_SqlStatement::ms_Self_Rank_HighestPrecision = "SELECT guid \
	FROM `characters` where gmlevel=0 \
	ORDER BY `characters`.`highest_precision` DESC\
	LIMIT 0 , %u;";


///  加载邮件列表
const char * 	MODI_SqlStatement::ms_szLoadMaillist = "SELECT * , UNIX_TIMESTAMP(`sendtime`) FROM %s WHERE `recever` = \'%s\' and isvalid = 1 order by sendtime desc;";

/// 保存邮件列表
const char * 	MODI_SqlStatement::m_szSaveMaillist = "CALL Save_Mail(%llu,\'%s\',\'%s\',\'%s\',\'%s\',%d,%d,%llu,\'%s\',%u,\'%s\',%llu);";

const std::string MODI_SqlStatement::ms_strAccountTableField[enAccountTableFieldCount] = { "account_id" , 
	"account" , 
	"password" , 
	"email" , 
	"activeGame" };

const std::string MODI_SqlStatement::ms_strCharsTableField[enCharsTableFieldCount] = {"guid" , 
																					  "accountid", "accname","name","age","city","personalsign","clan","group","exp","qq","sex","blood",
"level" , "birthday" , "votecount" , "renqi" , "fensigril" , "fensiboy" , "honeyinfo" , "defavatar" ,
"lastlogintime" , "lastloginip" , "money" , "moneyRMB" ,"lastidoltime" ,"wincount","losscount","tiecount" ,"gmlevel","readhelp","normalflags" ,
"chang_count","heng_count","keyboard_count","vote_count","total_online_time","today_online_time","consume_rmb" ,
																					  "highest_score" , "highest_score_musicid" , "highest_precision" , "highest_precision_musicid" , "block_count","chenghao", "otherdata"};

//const std::string MODI_SqlStatement::ms_strItemTableField[enItemTableFieldCount] = { "itemid" ,
//	"charguid", "world" , "server" , "serial" , "configid" , "isvalid" , "expiretime" , "count" , "bagtype" , "itempos" }; 

//const std::string MODI_SqlStatement::ms_strItemSerialTableField[enItemSerialTableFieldCount] = { "aid" , "worldid" , 
// "serverid" , "serial" };

const std::string MODI_SqlStatement::ms_strRelationTableField[enRelationTableFieldCount] = { "relid" , 
   "charid" , "relationguid" , "relationname" , "sex" ,  "friendpoint" , "reltype" , "isvalid" };

const std::string MODI_SqlStatement::ms_strMailTableField[enMailTableFieldCount] = { "mailid" , 
	"sender" , "recever" , "caption" , "content" , "type" , "state" , "sendtime" , "extrainfo" , "scriptdata", "isvalid","transid" };

const std::string MODI_SqlStatement::ms_strShopHistroyTableField[enShopHistroyTableFieldCount] = { "sid", "transid", "charid", 
	"itemserial","createtime","createtype","mailid","recvstate" };

const std::string MODI_SqlStatement::ms_strYunYingCDKeyTableField[enYunYingCDKeyCount] = { "cdkey" , "items" , "charid" , "isvalid" };

const char * 	MODI_SqlStatement::CreateStatement_Sel_Account( const char * szAccount , const char * szPassword )
{
	if( !szAccount || !szPassword )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szSelectAccount ,
		   	ms_AccountTable , szAccount , szPassword );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Sel_Charslist( defAccountID nAccountID )
{
	if( nAccountID == INVAILD_ACCOUNT_ID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szSelectCharslist , 
			ms_CharsTable , nAccountID );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Ins_CharacterEx( defAccountID nAccountID, MODI_CHARID charID ,const char * acc_name, const void * pInfo )
{
	if( nAccountID == INVAILD_ACCOUNT_ID || !pInfo  || charID == INVAILD_CHARID )
		return 0;

	const MODI_CreateRoleInfo * pCreate = (const MODI_CreateRoleInfo *)(pInfo);
	if( pCreate->m_AvatarIdx == INVAILD_CONFIGID )
		return 0;

	MODI_HoneyData  honeyData;
	char szTempData[200];
	memset( szTempData , 0 , sizeof(szTempData) );
	MODI_ASSERT( sizeof(szTempData) > sizeof(MODI_HoneyData) );
	Binary2String( (const char *)&honeyData , sizeof(MODI_HoneyData) , szTempData );

	std::string strBirthday = TimeHelpFuns::GetDateString( pCreate->m_nBirthday );
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szInsertChar  , 
			ms_CharsTable , 
			charID,
			nAccountID ,
			  acc_name,  
			pCreate->m_szRoleName , 
			pCreate->m_bySex,
			pCreate->m_nCityID ,
		   	strBirthday.c_str() , 
			pCreate->m_byXuexing ,
		   	pCreate->m_qq , 
			pCreate->m_Sign ,
			szTempData ,
			pCreate->m_AvatarIdx );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Load_CharData( defAccountID nAccountID, MODI_CHARID charID )
{
	if( charID == INVAILD_CHARID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szLoadCharData , 
			ms_CharsTable , charID , nAccountID );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Load_CharData( const char * szName )
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szLoadCharDataByName , 
			ms_CharsTable , szName );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Update_CharData( MODI_CHARID charID , const MODI_DBSaveCharBaseInfo * pSave)
																	//const MODI_RoleInfo & roleInfo , const MODI_RoleExtraData * pExtraData )
{
	if( charID == INVAILD_CHARID || 
		!pSave )
		return 0;

	if(pSave->pRoleData == NULL || pSave->pExtraData == NULL)
	{
		MODI_ASSERT(0);
		return 0;
	}	
	const MODI_RoleInfo & roleInfo = *(pSave->pRoleData);
   	const MODI_RoleExtraData * pExtraData = pSave->pExtraData;

	char szNormalFlags[128];
	memset( szNormalFlags , 0 , sizeof(szNormalFlags) );

	Binary2String( pExtraData->m_szNormalFlags , 
			sizeof(pExtraData->m_szNormalFlags),
			szNormalFlags );

	// char otherdata[pSave->m_dwSize * 2 + 1];
// 	memset(otherdata, 0, sizeof(otherdata));
// 	mysql_escape_string( otherdata, pSave->pOtherData, pSave->m_dwSize);
	
	std::string strBirthday = TimeHelpFuns::GetDateString( roleInfo.m_detailInfo.m_nBirthDay  );
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szUpdateCharData , 
			ms_CharsTable , 
			roleInfo.m_detailInfo.m_byAge , 
			roleInfo.m_detailInfo.m_nCity , 
			roleInfo.m_detailInfo.m_szPersonalSign,
			roleInfo.m_detailInfo.m_nClan,
			roleInfo.m_detailInfo.m_nGroup,
			roleInfo.m_normalInfo.m_nExp,
			roleInfo.m_detailInfo.m_nQQ,
			roleInfo.m_detailInfo.m_byBlood,
			roleInfo.m_baseInfo.m_ucLevel,
			strBirthday.c_str(),
			  roleInfo.m_detailInfo.m_nVoteCount, //roleInfo.m_detailInfo.m_nMoney, 金币之前已经保存过了
			roleInfo.m_detailInfo.m_nWin,
			roleInfo.m_detailInfo.m_nLoss,
			roleInfo.m_detailInfo.m_nTie,
			roleInfo.m_detailInfo.m_byReadHelp,
			szNormalFlags,
			roleInfo.m_detailInfo.m_Chang,
			roleInfo.m_detailInfo.m_Heng,
			roleInfo.m_detailInfo.m_KeyBoard,
			roleInfo.m_detailInfo.m_Toupiao,
			pExtraData->m_nTotalOnlineTime,
			pExtraData->m_nTodayOnlineTime,
			  //pExtraData->consume_rmb,
			roleInfo.m_detailInfo.highest_socre ,
			roleInfo.m_detailInfo.highest_score_musicid ,
			roleInfo.m_detailInfo.highest_precision,
			roleInfo.m_detailInfo.highest_precision_musicid ,
			  pExtraData->m_wdBlockCount,
		  	roleInfo.m_normalInfo.m_wdChenghao,
			  //otherdata,
		   	charID  );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_ResetTodayOnlineTIme()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szResetOnlineTime , 
			ms_CharsTable );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;

}

const char * 	MODI_SqlStatement::CreateStatement_HasChar( const char * szRoleName )
{
	if( !szRoleName || !szRoleName[0] )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szIsHasChar , 
			ms_CharsTable ,  szRoleName );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_UpdateLastLogin( time_t nTime , const char * szIP , MODI_CHARID charid )
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szUpdateLastLogin , 
			ms_CharsTable ,  
			nTime ,
			szIP,
			charid );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Max( int iTable , const char * szMaxWhat )
{
	const char * szTable = 0;
	if( iTable == enTable_Account )
	{
		szTable = ms_AccountTable;
	}
	else if( iTable == enTable_Character )
	{
		szTable = ms_CharsTable;
	}
// 	else if( iTable == enTable_Item )
// 	{
// 		szTable = ms_ItemTable;
// 	}

	if( !szTable )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_SelectMax , 
			szMaxWhat , 
			szTable );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}


#if 0
const char * 	MODI_SqlStatement::CreateStatement_LoadAvatarIDlist( MODI_CHARID charID )
{
	if( charID == INVAILD_CHARID )
		return 0;

//const char * 	MODI_SqlStatement::ms_szLoadAvatarID = "SELECT `configid` FROM %s WHERE `charguid` = %u and `isvalid` = 1 and `bagtype` = 1";

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szLoadAvatarID ,
			ms_ItemTable , 
			charID );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;

}

const char * 	MODI_SqlStatement::CreateStatement_LoadItemlist( defAccountID  acc_id )
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szLoadItemlist ,
			ms_ItemTable , 
			acc_id);

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}



const char * 	MODI_SqlStatement::CreateStatement_SaveItem( MODI_CHARID charID , void * pSave )
{
	if( charID == INVAILD_CHARID )
		return 0;

	const MODI_DBItemInfo * pItemInfo  = (const MODI_DBItemInfo *)(pSave);
	BYTE byWorldID = pItemInfo->m_byWorldID;
	BYTE byServerID = pItemInfo->m_byServerID;
	DWORD nSerial = pItemInfo->m_nItemSerial;

	if( !byWorldID || !byServerID || !nSerial )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szSaveItem ,
			charID,
			byWorldID,
			byServerID,
			nSerial,
			pItemInfo->m_nConfigID,
			pItemInfo->m_ExpireTime,
			pItemInfo->m_nCount,
			pItemInfo->m_nBagType,
			pItemInfo->m_nItemPos,
			(int)pItemInfo->m_byValid );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_LoadItemSerial( BYTE byWorld , BYTE byServerID )
{
	if( !byWorld || !byServerID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , ms_szLoadServerItemSerial ,
			ms_ItemSerialTable ,
			byServerID );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_SaveItemSerial( BYTE byWorld , BYTE byServerID , DWORD nSerial )
{
	if( !byWorld || !byServerID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szSaveServerItemSerial,
			ms_ItemSerialTable ,
			nSerial,
			byServerID );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_CreateItemHistroy( const void * p , BYTE nIdx )
{
	if( !p )
		return 0;

	const MODI_ShopCreateItemHistroy * pHistroy = (const MODI_ShopCreateItemHistroy *)(p);
	if( nIdx >= pHistroy->count )
		return 0;

//const char * 	MODI_SqlStatement::ms_szSaveShopHistroy = "INSERT INTO %s (`transid`,`charid`,`itemserial` , `createtype`,`mailid`,`recvstate`) 

	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szSaveShopHistroy,
			ms_ShopHistroyTable,
			pHistroy->transid ,
			pHistroy->charid,
			pHistroy->itemserial[nIdx],
			pHistroy->createtype ,
			pHistroy->mailid,
			pHistroy->recvstate );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}
#endif

const char * 	MODI_SqlStatement::CreateStatement_GetYunYingCDKey( const char * szKey )
{
	if( !szKey )
		return 0;

	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szGetYunYingCDKey,
			ms_YunYingCDKeyTable ,
			szKey );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_SaveYunYingCDKeyValid( const char * szKey , int iValid , MODI_CHARID charid )
{
	if( !szKey )
		return 0;

	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szSaveYunYingCDKey ,
			ms_YunYingCDKeyTable ,
			iValid ,
			charid ,
			szKey );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_LoadRelationList( MODI_CHARID nCharid )
{
	if( nCharid == INVAILD_CHARID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_LoadRelation ,
			ms_RelationTable ,
			nCharid );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_SaveRelationList( MODI_CHARID nCharid , const void * p )
{
	if( nCharid == INVAILD_CHARID )
		return 0;

	const MODI_DBRelationInfo * pTemp = (const MODI_DBRelationInfo *)(p);
	MODI_CHARID relcharid = GUID_LOPART( pTemp->m_guid );
	if( relcharid == INVAILD_CHARID )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) ,
		   	ms_SaveRelation ,	
			nCharid ,
			relcharid,
			pTemp->m_RelationType,
			pTemp->m_bIsValid ? 1 : 0,
			pTemp->m_szName,
			pTemp->m_bySex );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;

}

const char * 	MODI_SqlStatement::CreateStatement_SaveRelData( MODI_CHARID nCharid , const void * p )
{
	if( nCharid == INVAILD_CHARID )
		return 0;

	const MODI_SocialRelationData * pSocial = (const MODI_SocialRelationData *)(p);
	char szTempData[200];
	memset( szTempData , 0 , sizeof(szTempData) );
	MODI_ASSERT( sizeof(szTempData) > sizeof(MODI_HoneyData));
	Binary2String( (const char *)&(pSocial->m_HoneyData) , 
			sizeof(MODI_HoneyData) , 
			szTempData );

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) ,
		   	ms_SaveSocialRelData ,	
			ms_CharsTable,
			pSocial->m_nRenqi,
			pSocial->m_nFensiGril,
			pSocial->m_nFensiBoy,
			szTempData,
			pSocial->m_nLastAddIdolTime,
			nCharid );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

#if 0
const char * 	MODI_SqlStatement::CreateStatement_ClearupInvalidItems( BYTE byWorld , BYTE byServerID , DWORD nSerial )
{
	if( !byWorld || !byServerID || !nSerial)
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szClearupInvalidItems,
			ms_ItemTable ,
			byWorld,
			byServerID,
			nSerial );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;

}
#endif
const char * 	MODI_SqlStatement::CreateStatement_LoadMaillist( const char * szName )
{
	if( !szName )
		return 0;

	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_szLoadMaillist,
			ms_MailTable ,
			szName );

	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_SaveMaillist( const void * pMail)
{
	const MODI_DBMailInfo * pDBMail =  (const MODI_DBMailInfo * )(pMail);
	if( !pDBMail || pDBMail->m_dbID == INVAILD_DBMAIL_ID )
		return 0;
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	std::ostringstream os;
	for(BYTE i=0; i<pDBMail->m_MailInfo.m_byExtraCount; i++)
	{
		char szTempData[sizeof(MODI_MailExtraInfo)*2 + 1];
		memset( szTempData , 0 , sizeof(szTempData) );
		MODI_ASSERT( sizeof(szTempData) >= sizeof(MODI_MailExtraInfo) );
		/// 处理一下多个附件的情况
		Binary2String( (const char *)&(pDBMail->m_MailInfo.m_ExtraInfo[i]) , 
			sizeof(MODI_MailExtraInfo) , 
			szTempData );
		os<< szTempData;
	}

	char szScriptData[SCRIPTMAIL_DATA_SIZE];
	memset( szScriptData , 0 , sizeof(szScriptData) );
	MODI_ASSERT( sizeof(szScriptData) >= sizeof(MODI_ScriptMailData)  );
	Binary2String( (const char * )(&pDBMail->m_ScriptData) ,
			sizeof(MODI_ScriptMailData),
			szScriptData );

	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			m_szSaveMaillist,
			pDBMail->m_dbID,
			pDBMail->m_MailInfo.m_szSender ,
			pDBMail->m_MailInfo.m_szRecevier ,
			pDBMail->m_MailInfo.m_szCaption ,
			pDBMail->m_MailInfo.m_szContents , 
			pDBMail->m_MailInfo.m_Type,
			pDBMail->m_MailInfo.m_State,
			pDBMail->m_MailInfo.m_SendTime,
			  os.str().c_str(),
			  //szTempData,
			pDBMail->m_bIsValid ,
			szScriptData ,
			pDBMail->m_Transid );


	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );

	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_RankLevel()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Rank_Level ,
			RANK_LEVEL_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Self_RankLevel()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Self_Rank_Level ,
			SELF_RANK_LEVEL_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_RankRenqi()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Rank_Renqi , 
			RANK_RENQI_MAX_TOP);
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Self_RankRenqi()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Self_Rank_Renqi , 
			SELFRANK_RENQI_MAX_TOP);
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_RankConsumeRMB()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Rank_ConsumeRMB , 
			RANK_CONSUMERMB_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Self_RankConsumeRMB()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Self_Rank_ConsumeRMB , 
			SELFRANK_CONSUMERMB_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_RankHighestScore()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Rank_HighestScore, 
			RANK_HIGHESTSCORE_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Self_RankHighestScore()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Self_Rank_HighestScore, 
			SELFRANK_HIGHESTSCORE_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_RankHighestPrecision()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Rank_HighestPrecision, 
			RANK_HIGHESTPRECISION_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::CreateStatement_Self_RankHighestPrecision()
{
	memset( ms_szSqlStatementBuf , 0 , sizeof( ms_szSqlStatementBuf ) );
	SNPRINTF( ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) , 
			ms_Self_Rank_HighestPrecision, 
			SELFRANK_HIGHESTPRECISION_MAX_TOP );
	EscapeString( ms_szSqlEscapeBuf , sizeof( ms_szSqlEscapeBuf ) , 
			ms_szSqlStatementBuf , sizeof( ms_szSqlStatementBuf ) );
	return ms_szSqlEscapeBuf;
}

const char * 	MODI_SqlStatement::GetFieldInAccountTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enAccountTableFieldCount );
	return ms_strAccountTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::GetFieldInCharsTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enCharsTableFieldCount );
	return ms_strCharsTableField[nIdx].c_str();
}

#if 0
const char * 	MODI_SqlStatement::GetFieldInItemTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enItemTableFieldCount );
	return ms_strItemTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::GetFieldInItemSerialTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enItemSerialTableFieldCount );
	return ms_strItemSerialTableField[nIdx].c_str();
}
#endif

const char * 	MODI_SqlStatement::GetFieldInRelationTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enRelationTableFieldCount );
	return ms_strRelationTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::GetFieldInMailTable( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enMailTableFieldCount );
	return ms_strMailTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::GetFieldInShopHistroy( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enMailTableFieldCount );
	return ms_strShopHistroyTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::GetFieldInYunYingCDKey( unsigned int nIdx )
{
	MODI_ASSERT( nIdx < enYunYingCDKeyCount );
	return ms_strYunYingCDKeyTableField[nIdx].c_str();
}

const char * 	MODI_SqlStatement::EscapeString( char * szOut , size_t nOutSize , const char * szIn , size_t nInSize )
{
	if( nOutSize >= nInSize * 2  + 1 )
	{
		memset( szOut , 0 , nOutSize );
		memcpy( szOut , szIn , nInSize );
//		mysql_escape_string( szOut , szIn , nInSize );
		return szOut;
	}

	MODI_ASSERT(0);
	return 0;
}
