/**
 * @file gw2gs_cmddef.h
 * @date 2009-12-28 CST
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief 网关和GS的通信协议命令值定义
 *
 */

#ifndef GW2GS_CMDDEFINE_H_
#define GW2GS_CMDDEFINE_H_

#include "protocol/svrresult.h"

/// SESSION 相关操作
const unsigned char MCMD_GW2GS_SESSION = 0x01;

/// 网关重定向包
const unsigned char MCMD_SVR_REDIRECTIONAL = 0x02;


/// 语言数据传输范围相关操作
const unsigned char MCMD_GS2GW_MUSICTRANS = 0x03;

/// 广播
const unsigned char MCMD_GS2GW_BROADCAST = 0x04;

#endif
