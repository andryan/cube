#include "FPSControl.h"
#include "TimeManager.h"
#include "Global.h"

MODI_FPSControl::MODI_FPSControl()
{
	m_nFps = 0;
	m_nBeginTime = 0;
}

MODI_FPSControl::~MODI_FPSControl()
{

}

void 	MODI_FPSControl::Initial( DWORD nFps )
{
	DWORD n = 1000 / nFps;
	m_nFps = n;
}

void 	MODI_FPSControl::Begin()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	if( ptm )
	{
		m_nBeginTime =  ptm->CurrentTime();
	}
}

void 	MODI_FPSControl::End()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	if( ptm )
	{
		DWORD nNow = ptm->CurrentTime();
		if( nNow > m_nBeginTime )
		{
			DWORD nFps = nNow - m_nBeginTime;
			if( nFps < m_nFps )
			{
				DWORD nMicroS = (m_nFps - nFps) * 1000;
//				Global::logger->info("[%s] usleep = %u" , SVR_TEST ,
//					   nMicroS );	
				usleep( nMicroS ); 
			}
		}
	}
}

