#include "IGMCommandHandler.h"
#include "CmdLine.h"
#include "Base/AssertEx.h"
#include "Base/HelpFuns.h"

#define  GM_CMD_MAX_SIZE 	256

MODI_IGMCommandHandler::MODI_IGMCommandHandler():m_bInit(false)
{

}

MODI_IGMCommandHandler::~MODI_IGMCommandHandler()
{

}

bool MODI_IGMCommandHandler::Initialization()
{
	if( m_bInit )
		return true;

	if( !FillCmdHandlerTable() )
		return false;

	m_bInit = true;

	return true;
}

void MODI_IGMCommandHandler::AddCmdHandler(const char * szCmd , const MODI_GMCommand & h)
{
    MAP_INSERT_RESULT result = m_mapHandlers.insert(std::make_pair( szCmd , h));
    assert(result.second);
    if (result.second == false)
    {
        throw "reduplicate gm cmd handler .";
    }
}

bool MODI_IGMCommandHandler::IsValidFormat( const char * szCmd )
{
	if( !szCmd || szCmd[0] != '-' )
		return false;

	if( strlen( szCmd ) < 2 )
		return false;
	if( isblank( szCmd[1] ) )
		return false;

	return true;
}

int MODI_IGMCommandHandler::SplitCmd( const char * szCmd , char ** szOut , DWORD nOutCount )
{
	const char * szTemp = szCmd;
//	const char * pBegin = szCmd;
	DWORD nCount = 0;
	int iCharNum = 0;

	while( *szTemp != '\0' && nCount < nOutCount )
	{
		if( !isspace( *szTemp ) )
		{
			// 非空白字符，字符数＋1
			iCharNum += 1;
			szTemp += 1;
		}
		else 
		{
			// 空白字符
			if( iCharNum > 0 )
			{
				// 之前有字符，算做一个参数
				safe_strncpy( szOut[nCount] , szTemp - iCharNum , GM_CMD_MAX_SIZE );
				szOut[nCount][iCharNum] = '\0';
				nCount += 1;
				// 重置
				iCharNum = 0;
			}
			szTemp += 1;
		}
	}

	if( iCharNum )
	{
		safe_strncpy( szOut[nCount] , szTemp - iCharNum , GM_CMD_MAX_SIZE );
		szOut[nCount][iCharNum] = '\0';
		nCount += 1;
	}

	return nCount;
}

int MODI_IGMCommandHandler::DoHandleCommand( const char * szCmd )
{
	if( !this->IsValidFormat( szCmd ) )
		return enGMCmdHandler_InvalidParam;

	// 分配内存空间
	const DWORD nArrayCount = 16;
	char 	szTemp[nArrayCount][GM_CMD_MAX_SIZE];
	char *  szTemp2[nArrayCount];
	for( DWORD i = 0; i < nArrayCount; i++ )
	{
		memset( szTemp[i] , 0 , sizeof(szTemp[i]) );
		szTemp2[i] = szTemp[i];
	}
	char ** szArgs = szTemp2;

	// 获得参数
	int nCount = this->SplitCmd( szCmd , szArgs , nArrayCount );
	if( nCount < 1 )
		return enGMCmdHandler_InvalidParam;

	// 查找GM命令处理
    int iRet = enGMCmdHandler_InvalidCmd;
    MAP_HANDLER_ITER itor = m_mapHandlers.find( szArgs[0] );
    if (itor != m_mapHandlers.end())
    {
        MODI_GMCommand & cmd = (itor->second);
        if ( cmd.Func )
        {
			// 将参数扔给GM命令处理函数
            iRet = cmd.Func( nCount , (const char **)szArgs );
        }
    }

    return iRet;
}
