/**
 * @file   MNetService.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Sat Feb 27 16:10:54 2010
 * @version Ver0.1
 * @brief  多监听的服务器
 * 
 */

#ifndef _MDMNETSERVICE_H
#define _MDMNETSERVICE_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "Type.h"
#include "Service.h"
#include "MTCPService.h"


/**
 * @brief 多监听的服务器
 * 
 */
class MODI_MNetService: public MODI_Service
{
 public:
	virtual ~MODI_MNetService()
	{
		Final();
	}

	/**
	 * @brief 获取对象指针
	 *
	 * @return 对象指针
	 *
	 */ 
	static MODI_MNetService * GetMNetService()
	{
		return m_pMNetInstance;
	}

 protected:

	MODI_MNetService(std::vector<WORD> vec, std::vector<std::string> ip_vec, const char * name, const int count);
		
	/** 
	 * @brief 初始化
	 * 
	 * 
	 * @return 成功true,失败false
	 */
   	bool Init();


	/** 
	 * @brief 结束
	 * 
	 */
	void Final();

	/// 创建新的连接
	virtual bool CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port) = 0;

	/** 
	 * @brief 主循环回调
	 * 
	 * 
	 * @return 失败false
	 */
	bool ServiceCallBack()
	{
		m_stAcceptAddrVec.clear();
		m_stAcceptSockVec.clear();
		m_stAcceptPortVec.clear();
		int ret_code = m_stMTCPService.Accept(m_stAcceptAddrVec, m_stAcceptSockVec, m_stAcceptPortVec);
		if(ret_code)
		{
			for(int i = 0; i<ret_code; i++)
			{
				CreateTask(m_stAcceptSockVec[i], &(m_stAcceptAddrVec[i]), m_stAcceptPortVec[i]);
			}
		}
		return true;
	}

 protected:

	/// 服务器指针
	static MODI_MNetService * m_pMNetInstance;
	
	/// 服务器名字
	std::string m_strMNetName;
	
	/// 服务器连接容器
	MODI_MTCPService m_stMTCPService;

	///服务器绑定的端口
	std::vector<WORD > m_PortVec;

	std::vector<struct sockaddr_in > m_stAcceptAddrVec;
	std::vector<int> m_stAcceptSockVec;
	std::vector<WORD> m_stAcceptPortVec;

	/// 服务器监听的端口个数
	const int m_iCount;
};

#endif
