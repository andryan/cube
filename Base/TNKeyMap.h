/** 
 * @file TNKeyMap.h
 * @brief 多个key的MAP 包装
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */

#ifndef NKEYMAP__MGR_H_
#define NKEYMAP__MGR_H_


#include "protocol/gamedefine.h"
#include "Global.h"
#include <string>
#include <map>

class 	MODI_NKeyMap
{
	public:

		typedef std::map<unsigned long long,void *>				TMAPS_KEY1;
		typedef TMAPS_KEY1::iterator 							TMAPS_KEY1_ITER;
		typedef TMAPS_KEY1::const_iterator 						TMAPS_KEY1_C_ITER;
		typedef std::pair<TMAPS_KEY1_ITER,bool>  				TMAPS_KEY1_INSERT_RESULT;

		typedef std::map<std::string,void *>					TMAPS_KEY2;
		typedef TMAPS_KEY2::iterator 							TMAPS_KEY2_ITER;
		typedef TMAPS_KEY2::const_iterator 						TMAPS_KEY2_C_ITER;
		typedef std::pair<TMAPS_KEY2_ITER,bool>  				TMAPS_KEY2_INSERT_RESULT;

		typedef std::map<defAccountID,void *>					TMAPS_KEY3;
		typedef TMAPS_KEY3::iterator 							TMAPS_KEY3_ITER;
		typedef TMAPS_KEY3::const_iterator 						TMAPS_KEY3_C_ITER;
		typedef std::pair<TMAPS_KEY3_ITER,bool>  				TMAPS_KEY3_INSERT_RESULT;


	public:

		MODI_NKeyMap()
		{

		}

		~MODI_NKeyMap()
		{

		}


	public:

		bool Add( const unsigned long long & guid , const std::string & strName , defAccountID accid , void * p )
		{
			TMAPS_KEY1_INSERT_RESULT r1 = this->m_mapKey1.insert( std::make_pair( guid , p ) );
			if( !r1.second )
			{
				Global::logger->debug("[%s] add to nmap faild,key<guid> = %llu" , SVR_TEST , guid );
				return false;
			}
			TMAPS_KEY2_INSERT_RESULT r2 = this->m_mapKey2.insert( std::make_pair( strName , p ) );
			if( !r2.second )
			{
				Global::logger->debug("[%s] add to nmap faild,key<name> = %s" , SVR_TEST , strName.c_str() );
				this->m_mapKey1.erase( guid );
				return false;
			}
			TMAPS_KEY3_INSERT_RESULT r3 = this->m_mapKey3.insert( std::make_pair( accid , p ) );
			if( !r3.second )
			{
				Global::logger->debug("[%s] add to nmap faild,key<accid> = %u" , SVR_TEST ,  accid );
				this->m_mapKey1.erase( guid );
				this->m_mapKey2.erase( strName );
				return false;
			}

#ifdef _DEBUG

			MODI_ASSERT( this->m_mapKey1.size() == this->m_mapKey2.size() && 
					this->m_mapKey1.size() == this->m_mapKey3.size() );

#endif
			return true;
		}

		bool Del( const unsigned long long & guid , const std::string & strName , defAccountID accid )
		{
			TMAPS_KEY1_ITER itor1 = this->m_mapKey1.find( guid );
			TMAPS_KEY2_ITER itor2 = this->m_mapKey2.find( strName );
			TMAPS_KEY3_ITER itor3 = this->m_mapKey3.find( accid );

			bool bRet = false;
			if( itor1 != this->m_mapKey1.end() &&
				itor2 != this->m_mapKey2.end() &&
				itor3 != this->m_mapKey3.end() )
			{
				this->m_mapKey1.erase( itor1 );
				this->m_mapKey2.erase( itor2 );
				this->m_mapKey3.erase( itor3 );
				bRet = true;
			}
			else 
			{
				MODI_ASSERT(0);
				this->m_mapKey1.erase( guid );
				this->m_mapKey2.erase( strName );
				this->m_mapKey3.erase( accid );
			}

#ifdef _DEBUG
			MODI_ASSERT( this->m_mapKey1.size() == this->m_mapKey2.size() && 
					this->m_mapKey1.size() == this->m_mapKey3.size() );
#endif
			return bRet;
		}

		void * FindByGUID( unsigned long long guid  )
		{
			TMAPS_KEY1_ITER itor = this->m_mapKey1.find( guid );
			if( itor != this->m_mapKey1.end() )
			{
				return itor->second;
			}
			return 0;
		}

		void * FindByName( const std::string & strName )
		{
			TMAPS_KEY2_ITER itor = this->m_mapKey2.find( strName );
			if( itor != this->m_mapKey2.end() )
			{
				return  itor->second;
			}
			return 0;
		}

		void * FindByAccid( defAccountID accid )
		{
			TMAPS_KEY3_ITER itor = this->m_mapKey3.find( accid );
			if( itor != this->m_mapKey3.end() )
			{
				return itor->second;
			}
			return 0; 
		} 

		size_t Size()
		{
#ifdef _DEBUG
			MODI_ASSERT( this->m_mapKey1.size() == this->m_mapKey2.size() && 
					this->m_mapKey1.size() == this->m_mapKey3.size() );
#endif
			return this->m_mapKey1.size();
		}

		const TMAPS_KEY1 	& GetGUIDMapContent() { return m_mapKey1; }
		const TMAPS_KEY2 	& GetNameMapContent() { return m_mapKey2; }
		const TMAPS_KEY3 	& GetAccidMapContent() { return m_mapKey3; }

	protected:

		TMAPS_KEY1 	m_mapKey1;
		TMAPS_KEY2 	m_mapKey2;
		TMAPS_KEY3 	m_mapKey3;
};






#endif

