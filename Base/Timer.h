/**
 * @file Timer.h
 * @date 2009-12-14 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 时间封装
 *
 */

#ifndef _MDTIMER_H
#define _MDTIMER_H

#include <string>
#include <time.h>
#include <sys/time.h>

#include "Type.h"
#include "Mutex.h"


/**
 * @brief 时区相关函数
 *
 *
 */
class MODI_ZTime
{
 public:
	/// 获取系统时区
	static void GetSysZone(std::string & zone_name);
	
	/// 保存系统时区
	static void SaveSysZone(std::string & zone_name);

	/// 重加载系统时区
	static void ReloadSysZone(const std::string & zone_name);

	/// 获取本地时间
	static void GetLocalTime(struct tm & tm_time, time_t & time_value);

	/// 系统时区和GMT0的时差
	static long m_SysZoneTime;

};


/**
 * @brief 时间封装
 *
 */
class MODI_RTime
{
 public:
	/**
	 * @brief 构造
	 *
	 * @param msecond 延时的毫秒数
	 *
	 */
	MODI_RTime(const unsigned int msecond = 0)
	{
		m_msec = now();
		m_msec += msecond;
	}

	~MODI_RTime()
	{
		
	}

	/**
	 * @brief 时间设置到当前时间
	 *
	 */
	void GetNow()
	{
		m_msec = now();
	}

	/**
	 * @brief 延时毫秒
	 *
	 * @param msecond 延时毫秒数
	 *
	 */
	void SetDelay(unsigned int msecond)
	{
		m_msec += msecond;
	}

	/**
	 * @brief 从现在开始延时
	 *
	 * @param msecond 延时毫秒数
	 *
	 */
	void Delay(unsigned int msecond)
	{
		m_msec = now();
		m_msec += msecond;
	}

	/**
	 * @brief 重载=
	 *
	 */
	MODI_RTime & operator= (const MODI_RTime & timer)
	{
		m_msec = timer.m_msec;
		return *this;
	}

	/**
	 * @brief 重载=
	 *
	 */
	MODI_RTime & operator= (const unsigned long long msec)
	{
		m_msec = msec;
		return *this;
	}

	/**
	 * @brief 重载 >
	 *
	 */ 
	bool operator > (const MODI_RTime & timer) const
	{
		return m_msec > timer.m_msec;
	}

	/**
	 * @brief 重载 >=
	 *
	 */ 
	bool operator >= (const MODI_RTime & timer) const
	{
		return m_msec >= timer.m_msec;
	}

	/**
	 * @brief 重载 <
	 *
	 */ 
	bool operator < (const MODI_RTime & timer) const
	{
		return m_msec < timer.m_msec;
	}

	/**
	 * @brief 重载 <=
	 *
	 */ 
	bool operator <= (const MODI_RTime & timer) const
	{
		return m_msec <= timer.m_msec;
	}

	/**
	 * @brief 重载 ==
	 *
	 */ 
	bool operator == (const MODI_RTime & timer) const
	{
		return m_msec == timer.m_msec;
	}

	/**
	 * @brief 获取时间的秒
	 * @return 当前时间的秒数
	 */ 
	const unsigned long GetSec() const
	{
		return m_msec / 1000L;
	}

	/**
	 * @brief 获取时间的毫秒
	 * @return 当前时间的毫秒数
	 */ 
	const unsigned long long GetMSec() const
	{
		return m_msec;
	}

 private:
	/**
	 * @brief 获取当前时间		
	 *
	 * @return 当前时间的毫秒数
	 *
	 */ 
	unsigned long long now()
	{
		unsigned long long ret_val = 0LL;
		struct timespec time_spec;
		clock_gettime(CLOCK_REALTIME, &time_spec);
		ret_val = time_spec.tv_sec;
		ret_val *= 1000LL;
		ret_val += (time_spec.tv_nsec / 1000000L);
		return ret_val;
	}
	
	/// 时间毫秒
	unsigned long long m_msec;
};


/**
 * @brief 时间tm封装
 *
 */
class MODI_TTime
{
 public:
	MODI_TTime()
	{
		time(&m_secs);
		MODI_ZTime::GetLocalTime(m_tmtime, m_secs);
	}

	/**
	 * @brief 拷贝构造
	 *
	 */ 
	MODI_TTime(const MODI_TTime & mdtime)
	{
		m_secs = mdtime.m_secs;
		MODI_ZTime::GetLocalTime(m_tmtime, m_secs);
	}

	/**
	 * @brief 赋值
	 *
	 */ 
	MODI_TTime & operator= (const unsigned int & rt)
	{
		m_secs = rt;
		MODI_ZTime::GetLocalTime(m_tmtime, m_secs);
		return *this;
	}
	
	/**
	 * @brief 重载=
	 *
	 */
	MODI_TTime & operator= (const MODI_RTime &rt)
	{
		m_secs = rt.GetSec();
		MODI_ZTime::GetLocalTime(m_tmtime, m_secs);
		return *this;
	}

	/**
	 * @brief 重载 >
	 *
	 */ 
	bool operator > (const MODI_TTime &rt) const
	{
		return m_secs > rt.m_secs;
	}


	/**
	 * @brief 重载 >=
	 *
	 */ 
	bool operator >= (const MODI_TTime &rt) const
	{
		return m_secs >= rt.m_secs;
	}


	/**
	 * @brief 重载 <
	 *
	 */ 
	bool operator < (const MODI_TTime &rt) const
	{
		return m_secs < rt.m_secs;
	}


	/**
	 * @brief 重载 <=
	 *
	 */
	bool operator <= (const MODI_TTime &rt) const
	{
		return m_secs <= rt.m_secs;
	}


	/**
	 * @brief 重载 ==
	 *
	 */ 
	bool operator == (const MODI_TTime &rt) const
	{
		return m_secs == rt.m_secs;
	}

	/**
	 * @brief 获取当前时间
	 *
	 */ 
	void GetNow()
	{
		time(&m_secs);
		MODI_ZTime::GetLocalTime(m_tmtime, m_secs);
	}

	/**
	 * @brief 时间流逝
	 *
	 * @return 时间流逝的毫秒数
	 *
	 */ 
   	const unsigned int Elapse() 
	{
		time_t now_sec;
		time(&now_sec);
		MODI_ZTime::GetLocalTime(m_tmtime, now_sec);
		return now_sec - m_secs;
	}

	/**
	 * @brief 获取当前时间秒
	 *
	 * @return 返回当前时间秒
	 *
	 */
	time_t GetSec() const
	{
		return m_tmtime.tm_sec;
	}

	/**
	 * @brief 获取当前时间分
	 *
	 * @param 返回当前时间分
	 */
	const int GetMin() const 
	{
		return m_tmtime.tm_min;
	}
	

	/**
	 * @brief 获取当前时间小时
	 *
	 * @param 返回当前时间小时
	 *
	 */
	const int GetHour() const 
	{
		return m_tmtime.tm_hour;
	}
	

	/**
	 * @brief 获取当前日期
	 * 
	 * @param 返回当前日期
	 *
	 */
	const int GetMDay() const 
	{
		return m_tmtime.tm_mday;
	}


	/**
	 * @brief 获取当前星期
	 *
	 * @param 返回当前星期
	 *
	 */
	const int GetWDay() const 
	{
		return m_tmtime.tm_wday;
	}

	/**
	 * @brief 获取当前日期月
	 *
	 * @param 返回当前月
	 *
	 */
	const int GetMon() const 
	{
		return m_tmtime.tm_mon+1;
	}
	

	/**
	 * @brief 获取当前年
	 *
	 * @param 返回当前年
	 *
	 */
	const int GetYear() const 
	{
		return m_tmtime.tm_year+1900;
	}	

	const time_t  GetTimet() const { return m_secs; }

	const char * GetStrTime();

 private:
	/// 秒
	time_t m_secs;

	/// 时间tm 
	struct tm m_tmtime;
};


/**
 * @brief 定时器
 *
 */
class MODI_Timer
{
 public:
	/**
	 * @brief 构造
	 *
	 * @param delay_time 延时的秒数
	 * @param cur_rtime 当前的时间
	 *
	 */
	MODI_Timer(const unsigned int delay_time, const MODI_RTime & cur_rtime)
		: m_dwDelay(delay_time), m_stRTime(cur_rtime)
	{
		m_stRTime.SetDelay(m_dwDelay);
	}

	/**
	 * @brief 构造
	 *
	 * @param delay_time 延时的秒数
	 * @param cur_sec 当前的时间
	 *
	 */
	MODI_Timer(const unsigned int delay_time, const unsigned int cur_sec = 0)
		: m_dwDelay(delay_time), m_stRTime(cur_sec)
	{
		m_stRTime.SetDelay(m_dwDelay);
	}
		
	/**
	 * @brief 重加载定时器
	 * @param delay_time 延时的时间秒数
	 * @param cur_rtime 当前的时间
	 *
	 */
	void Reload(const unsigned int delay_time, const MODI_RTime & cur_rtime)
	{
		m_dwDelay = delay_time;
		m_stRTime = cur_rtime;
		m_stRTime.SetDelay(m_dwDelay);
	}

	/**
	 * @brief 定时器检测
	 *
	 * @param cur_rtime 当前时间
	 * @return 定时器已到true,定时器未到false
	 *
	 */
	bool operator() (const MODI_RTime & cur_rtime)
	{
		if(m_stRTime <= cur_rtime)
		{
			m_stRTime = cur_rtime;
			m_stRTime.SetDelay(m_dwDelay);
			return true;
		}
		return false;
	}

	/**
	 * @brief 定时器检测
	 *
	 * @param cur_rtime 当前时间毫秒数
	 * @return 定时器已到true,定时器未到false
	 *
	 */
	bool operator() (const QWORD & cur_rtime)
	{
		if(m_stRTime.GetMSec() <= cur_rtime)
		{
			m_stRTime = cur_rtime;
			m_stRTime.SetDelay(m_dwDelay);
			return true;
		}
		return false;
	}
	
 private:
	/// 延时毫秒数
	unsigned int m_dwDelay;

	/// 对比时间
	MODI_RTime m_stRTime;
};


/**
 * @brief 时间比较
 * 
 */
class MODI_TimerEqu
{
 public:
	MODI_TimerEqu(const int min = 0,  const int hour = 0)
	{
		m_wdMin = min;
		m_wdHour = hour;
		m_blIsEqu = true;
	}

	bool operator() (const MODI_TTime & cur_ttime)
	{
		if((m_wdHour == cur_ttime.GetHour()) && (m_wdMin == cur_ttime.GetMin()) && (m_blIsEqu == true))
		{
			m_blIsEqu = false;
			return true;
		}

		if(m_wdMin == 59)
		{
			if(m_wdHour == 23)
			{
				if((0 == cur_ttime.GetHour()) && (m_blIsEqu == false))
				{
					m_blIsEqu = true;
					return false;
				}
			}
			else
			{
				if((m_wdHour + 1 == cur_ttime.GetHour()) && (m_blIsEqu == false))
				{
					m_blIsEqu = true;
					return false;
				}
			}
		}
		else
		{
			if((m_wdHour == cur_ttime.GetHour()) && (m_wdMin + 1 == cur_ttime.GetMin()) && (m_blIsEqu == false))
			{
				m_blIsEqu = true;
				return false;
			}
		}
		
		return false;
	}
	
 private:
	bool m_blIsEqu;
	int m_wdMin;
	int m_wdHour;
};

#endif
