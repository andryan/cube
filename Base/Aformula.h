/** 
 * @file Aformula.h
 * @brief 游戏中需要的计算公式
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */

#ifndef MODI_GAME_AFORMULA_H_
#define MODI_GAME_AFORMULA_H_


#include "BaseOS.h"

class 	MODI_Aformula
{

	public:


		static DWORD 	ComputeExp( DWORD nTotalScore , float  fExact , float fExpFactor );

		static DWORD 	ComputeMoney( DWORD nExp , float fExact , float fMoneyFactor );

		static DWORD 	ComputeKeyExp( DWORD nTotalScore , float  fExact , float fExpFactor );

		static DWORD 	ComputeKeyMoney( DWORD nExp , float fExact , float fMoneyFactor );

};

#endif
