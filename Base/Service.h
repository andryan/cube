/**
 * @file Service.h
 * @date 2009-12-18 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 服务器基本框架
 *
 */

#ifndef _MDSERVICE_H
#define _MDSERVICE_H

#include <string>
#include "Type.h"
#include "Global.h"

/**
 * @brief 服务器基本框架 
 *
 */
class MODI_Service: public MODI_DisableCopy
{
 public:
	/**
	 * @brief 获取服务器的名字
	 * 
	 * @return 服务器的名字
	 *
	 */
	const char * GetName() const 
	{
		return m_strBaseName.c_str();
	}

	virtual ~MODI_Service()
	{
		m_pBaseInstance = NULL;
	}

	/**
	 * @brief 获取对象指针
	 * 
	 * @return 对象指针
	 *
	 */ 
	static MODI_Service *  GetBaseInstancePtr()
	{
		return  m_pBaseInstance;
	}

	/// 主函数
	void Main();

	/// 置服务器结束标志
	void Terminate()
	{
		m_blTerminate = true;
	}

	/**
	 * @brief 服务器是否结束
	 *
	 * @return 结束true,失败false
	 *
	 */
	bool IsTerminate()
	{
		return m_blTerminate;
	}


	/** 
	 * @brief 信号处理
	 * 
	 */
	void DealSignalUsr1()
	{
		ReloadConfig();
	}

	
	/** 
	 * @brief 信号处理
	 * 
	 */
	virtual void DealSignalUsr2()
	{
		
	}
	
	/** 
	 * @brief 重新加载配置
	 * 
	 */
	virtual void ReloadConfig()
	{
		Global::logger->info("[%s] relaod %s config", LOGACT_SYS_INIT, GetName());
	}

	virtual void OnSignalUsr2()
	{
		Global::logger->info("[%s] call %s usr2", LOGACT_SYS_INIT, GetName());
	}

 protected:
	MODI_Service(const char * base_name): m_strBaseName(base_name)
	{
		m_pBaseInstance = this;
		m_blTerminate = false;
	}
		
	/// 初始化
	virtual bool Init();

	/// 服务器回调
	virtual bool ServiceCallBack() = 0;

	/// 服务器释放资源
	virtual void Final() = 0;
	
 private:
	/// 服务器名字
	std::string m_strBaseName;

	/// 服务器对象指针
	static MODI_Service * m_pBaseInstance;

	/// 服务器是否结束
	bool m_blTerminate;
};

#endif
