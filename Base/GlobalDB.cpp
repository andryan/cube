/**
 * @file   GlobalDB.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 13 11:41:11 2011
 * 
 * @brief  数据库表结构统一管理
 * 
 */

#include "Global.h"
#include "DBClient.h"
#include "GlobalDB.h"

std::string MODI_GlobalDB::zone_user_var_name = "zone_user_var";
std::string MODI_GlobalDB::game_user_var_name = "game_user_var";
std::string MODI_GlobalDB::item_infos_name = "item_infos";
std::string MODI_GlobalDB::characters_name = "characters";
std::string MODI_GlobalDB::buyrecord_name = "buy_records";
std::string MODI_GlobalDB::giverecord_name = "give_records";
std::string MODI_GlobalDB::family_name = "familys";
std::string MODI_GlobalDB::web_item = "trade_goods";
std::string MODI_GlobalDB::char_info = "charinfo";
std::string MODI_GlobalDB::ina_consume = "ina_consume";
std::string MODI_GlobalDB::web_present = "trade_goods_present";

namespace GlobalDB
{
	/// onlineclient用户变量
	MODI_TableStruct * m_pZoneUserVarTbl = NULL;
	/// clientavatar用户变量
	MODI_TableStruct * m_pGameUserVarTbl = NULL;
	/// 道具表
	MODI_TableStruct * m_pItemInfoTbl = NULL;
	/// 角色表
	MODI_TableStruct * m_pCharacterTbl = NULL;
	/// 交易记录表
	MODI_TableStruct * m_pBuyrecordTbl = NULL;
	/// 赠送记录表
	MODI_TableStruct * m_pGiverecordTbl = NULL;
	/// 家族表
	MODI_TableStruct * m_pFamilyTbl = NULL;
	/// 网页道具表
	MODI_TableStruct * m_pWebItemTbl = NULL;
	/// 网页购买道具角色信息表
	MODI_TableStruct * m_pCharInfoTbl = NULL;
	///单曲信息列表
	MODI_TableStruct * m_pSingleMusicTbl = NULL;
	/// INA对账号表
	MODI_TableStruct * m_pINAConsume = NULL;
	/// 网页赠送道具表
	MODI_TableStruct * m_pWebPresentTbl = NULL;
}


/** 
 * @brief 加载所有的表结构
 * 
 * 
 */
bool MODI_GlobalDB::Init()
{
	GlobalDB::m_pZoneUserVarTbl = MODI_DBManager::GetInstance().GetTable(zone_user_var_name.c_str());
	if(! GlobalDB::m_pZoneUserVarTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", zone_user_var_name.c_str());
		return false;
	}

	GlobalDB::m_pGameUserVarTbl = MODI_DBManager::GetInstance().GetTable(game_user_var_name.c_str());
	if(! GlobalDB::m_pGameUserVarTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", game_user_var_name.c_str());
		return false;
	}

	GlobalDB::m_pItemInfoTbl = MODI_DBManager::GetInstance().GetTable(item_infos_name.c_str());
	if(! GlobalDB::m_pItemInfoTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", item_infos_name.c_str());
		return false;
	}

	GlobalDB::m_pCharacterTbl = MODI_DBManager::GetInstance().GetTable(characters_name.c_str());
	if(! GlobalDB::m_pCharacterTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", characters_name.c_str());
		return false;
	}

	GlobalDB::m_pBuyrecordTbl = MODI_DBManager::GetInstance().GetTable(buyrecord_name.c_str());
	if(! GlobalDB::m_pBuyrecordTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", buyrecord_name.c_str());
		return false;
	}

	GlobalDB::m_pGiverecordTbl = MODI_DBManager::GetInstance().GetTable(giverecord_name.c_str());
	if(! GlobalDB::m_pGiverecordTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", giverecord_name.c_str());
		return false;
	}

	GlobalDB::m_pFamilyTbl = MODI_DBManager::GetInstance().GetTable(family_name.c_str());
	if(! GlobalDB::m_pFamilyTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", family_name.c_str());
		return false;
	}

	GlobalDB::m_pSingleMusicTbl = MODI_DBManager::GetInstance().GetTable("music");
	if( !GlobalDB::m_pSingleMusicTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable to get table struct <name=%s>","music");
		return false;
	}

	GlobalDB::m_pWebItemTbl = MODI_DBManager::GetInstance().GetTable(web_item.c_str());
	if(! GlobalDB::m_pWebItemTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", web_item.c_str());
		return false;
	}

	GlobalDB::m_pCharInfoTbl = MODI_DBManager::GetInstance().GetTable(char_info.c_str());
	if(! GlobalDB::m_pCharInfoTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", char_info.c_str());
		return false;
	}

	GlobalDB::m_pINAConsume = MODI_DBManager::GetInstance().GetTable(ina_consume.c_str());
	if(! GlobalDB::m_pINAConsume)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", ina_consume.c_str());
		return false;
	}

	GlobalDB::m_pWebPresentTbl = MODI_DBManager::GetInstance().GetTable(web_present.c_str());
	if(! GlobalDB::m_pWebPresentTbl)
	{
		Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", web_present.c_str());
		return false;
	}
	
	return true;
}
