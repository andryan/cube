/**
 * @file   ClientNormalSched.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Tue Oct 12 14:41:15 2010
 * @version $Id:$
 * @brief  正常调用
 * 
 * 
 */

#ifndef _MDCLIENTNORMALSCHED_H
#define _MDCLIENTNORMALSCHED_H

#include "ThreadGroup.h"
#include "ClientPollThread.h"

class MODI_PClientTaskSched;


/**
 * @brief 正常的连接处理
 * 
 */
class MODI_ClientNormalThread: public MODI_ClientPollThread
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sched 所属调度器
	 * @param name 线程名字
	 *
	 */
	MODI_ClientNormalThread(MODI_PClientTaskSched * sched, const char * name):
		MODI_ClientPollThread(name), m_pSched(sched)
	{
		
	}

		
	/** 
	 * @brief 初始化
	 * 
	 * @return 成功true,失败false
	 */
	bool Init();

		
	/** 
	 * @brief 结束
	 * 
	 */
	void Final();

	
	/** 
	 * @brief 主循环
	 * 
	 */
	void Run();

	/** 
	 * @brief 删除某个连接
	 * 
	 * @param p_task 要删除的连接
	 */
   	void RemoveTask(MODI_PClientTask * p_task);
	
 protected:
	/** 
 	 * @brief 连接注册到列表
 	 * 
 	 * @param p_task 要注册的task
 	 */
	void AddTaskToList(MODI_PClientTask * p_task);
	
 private:
	/// 是那个调度器
	MODI_PClientTaskSched * m_pSched;
};


/**
 * @brief 正常调度管理
 * 
 */
class MODI_ClientNormalSched
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param shced 那个调度器
	 * @param thread_num 线程数量
	 */
	MODI_ClientNormalSched(MODI_PClientTaskSched * sched, const WORD thread_num):
		m_pSched(sched), m_stThreadGroup(thread_num)
	{
		
	}

	/** 
	 * @brief 增加一个新的连接
	 * 
	 * @param p_task 要增加的连接
	 * 
	 */
   	bool AddTask(MODI_PClientTask * p_task)
	{
		WORD i = 0;
		MODI_ClientNormalThread * p_min_thread = (MODI_ClientNormalThread *)m_stThreadGroup[i++];
		MODI_ClientNormalThread * p_thread = NULL;
		for(; i<m_stThreadGroup.GetCount(); i++)
		{
			p_thread = (MODI_ClientNormalThread *)m_stThreadGroup[i];
			if(! p_thread)
			{
				continue;
			}
			if(p_min_thread->Size() > p_thread->Size())
			{
				p_min_thread = p_thread;
				p_thread = NULL;
			}
		}
		p_task->GetNextState();
	  	p_min_thread->AddTask(p_task);
		return true;
	}

	
	/** 
	 * @brief 初始化
	 * 
	 * 
	 * @return 成功true,失败false
	 */
	bool Init();

	
	/** 
	 * @brief 结束
	 * 
	 */
	void Final()
	{
		m_stThreadGroup.DelAllThread();
	}


	/** 
	 * @brief 获取连接数的大小
	 * 
	 * 
	 * @return 连接数目
	 */
	int Size()
	{
		struct MODI_GetSize: public MODI_ThreadCallBack
		{
			MODI_GetSize()
			{
				size = 0;
			}
			
			int size;

			bool Exec(MODI_Thread * p_thread)
			{
				size += ((MODI_ClientNormalThread *)p_thread)->Size();
				return true;
			}
		};

		MODI_GetSize getsize_callback;
		m_stThreadGroup.ExecAllThread(getsize_callback);
		return getsize_callback.size;
	}
	
 private:
	/// 归属那个调度器
	MODI_PClientTaskSched * m_pSched;

	/// 线程组
	MODI_ThreadGroup m_stThreadGroup;
};

#endif

