#include "LyricParser.h"

MODI_LyricParser::MODI_LyricParser()
{

}

MODI_LyricParser::~MODI_LyricParser()
{

}

int MODI_LyricParser::Read(const unsigned char *str)
{
	int result = 0;
	string strMark;
	int length = 0;
	strMark.assign((char*)str);

	length = strMark.find('>');

	if (0 == strMark.compare(0, SENTENCE_START.size(), SENTENCE_START))
	{
		for (int i= 0; i<NUM_ATTRI_TYPE; i++)
		{
			FindAttribute(strMark, ATTRI_TYPE[i]);
		}
		result = 1;
	}
	
	if (0 == strMark.compare(0, SENTENCE_END.size(), SENTENCE_END))
	{
		m_AttriVector.clear();
		result = -1;
	}

	if (-1 != length)
	{
		m_strWord = strMark.substr(length+1);
	}
	else
	{
		m_strWord = strMark;
	}

	return result;
}

bool MODI_LyricParser::FindAttribute(const char *str, const char *ch)
{
    string tempStr=str;

	if (tempStr.npos==tempStr.find(ch))
	{
        return false;
	}

	return true;
}

const char *MODI_LyricParser::GetLyric(const char *str)
{
	static char lyric[128];
    static char ch[128];

	if (NULL==str || strlen(str)<=0)
	{
        return "";
	}

	sprintf(ch, "%s\\0", str); 

    char *token=strtok(ch, ">"); 

    sprintf(lyric, "%s\\0", token); 

    while (token!=NULL) 
	{ 
		token=strtok(NULL, ">"); 

		if (token!=NULL)
		{
		    sprintf(lyric, "%s\\0", token);
		}
	}

	return lyric;
}

void MODI_LyricParser::FindAttribute(const string &str, const string &attributeType)
{
	size_t index = 0;
	int  end, offset;
	if (str.npos != (index = str.find(attributeType)))
	{
		offset = index + attributeType.size();
		end = str.find('@', offset);
		if (end < 0)
			end = str.find('>', offset);
		string strValue = str.substr(offset, end-offset);
		SAttri attribute;
		attribute.m_strAttributeType = attributeType;
		attribute.m_strValue = strValue;
		m_AttriVector.push_back( attribute);
	}
}

