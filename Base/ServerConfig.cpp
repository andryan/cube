#include "ServerConfig.h"
#include "Global.h"
#include "VarType.h"
#include "BaseOS.h"
#include "AssertEx.h"
#include "HelpFuns.h"

const char * GetDBProxyTypeStringFormat( DBProxyType e ) 
{
	static const std::string strDBProxy[] = {
		"ManagerServerProxy",
		"ZoneServerProxy",
		"GameServerProxy",
	};

	return strDBProxy[e].c_str();
}

MODI_SvrGSConfig::~MODI_SvrGSConfig()
{
	for( size_t n = 0; n < vecGameServers.size(); n++ )
	{
		delete vecGameServers[n];
	}
	vecGameServers.clear();
}

const MODI_SvrGSConfig::MODI_Info * 	MODI_SvrGSConfig::GetInfoByID( unsigned int nID ) const
{
	const MODI_Info * pRet = 0;
	for( size_t n = 0; n < vecGameServers.size(); n++ )
	{
		const MODI_Info * pTemp = vecGameServers[n];
		if( pTemp->nChannelID == nID )
		{
			pRet = pTemp;
			break;
		}
	}

	return pRet;
}

const MODI_SvrGSConfig::MODI_Info * 	MODI_SvrGSConfig::GetInfoByName( const char * szName ) const
{
	const MODI_Info * pRet = 0;
	for( size_t n = 0; n < vecGameServers.size(); n++ )
	{	
		const MODI_Info * pTemp = vecGameServers[n];
		if( pTemp->strChannelName == szName )
		{
			pRet = pTemp;
			break;
		}
	}

	return pRet;
}

MODI_DBProxyConfig::~MODI_DBProxyConfig()
{
	for( size_t n = 0; n < vecDBProxy.size(); n++ )
	{
		delete vecDBProxy[n];
	}
	vecDBProxy.clear();
}

const MODI_DBProxyConfig::MODI_Info * 	MODI_DBProxyConfig::GetInfoByID( unsigned int nID ) const
{
	const MODI_Info * pRet = 0;
	for( size_t n = 0; n < vecDBProxy.size(); n++ )
	{
		const MODI_Info * pTemp = vecDBProxy[n];
		if( pTemp->id == nID )
		{
			pRet = pTemp;
			break;
		}
	}
	return pRet;
}

bool MODI_DBProxyConfig::CheckGSID( unsigned int nID )
{
	std::set<unsigned int>::iterator itor = setidcheck.find( nID );
	if( itor == setidcheck.end() )
		return true;
	return false;
}


bool MODI_DBProxyConfig::AddGSID( MODI_Info * pInfo , unsigned int nID )
{
	if( CheckGSID( nID ) )
	{
		std::set<unsigned int>::iterator itor = pInfo->setGameServerID.find( nID );
		if( itor != pInfo->setGameServerID.end() )
			return false;
		pInfo->setGameServerID.insert( nID );
		setidcheck.insert( nID );
		return true;
	}
	return false;
}

MODI_ISvrConfigInfo::~MODI_ISvrConfigInfo()
{

}	

MODI_ServerConfig::MODI_ServerConfig()
{

}

MODI_ServerConfig::~MODI_ServerConfig()
{
	Unload();
}

xmlNodePtr MODI_ServerConfig::GetField( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot , const char * szKey  )
{
	xmlNodePtr pRet = xmlFile.GetChildNode( pRoot , szKey );
	return pRet;
}

bool 	MODI_ServerConfig::LoadSvrBaseInfo(  MODI_XMLParser & xmlFile , xmlNodePtr & pRoot ,
			   	xmlNodePtr & pField , MODI_ISvrConfigInfo  * p )
{
	MODI_SvrConfigBaseInfo * pBaseInfo = (MODI_SvrConfigBaseInfo *)(p);
	const std::string & file_name = xmlFile.GetFilename();

	xmlNodePtr p_addr = xmlFile.GetChildNode(pField, "addr");
	if(! p_addr)
	{
		//获取%s的节点%s失败
		Global::logger->fatal( LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "addr" );
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "port", &pBaseInfo->nPort, sizeof(pBaseInfo->nPort)))
	{
		//获取%s节点%s的%s属性值失败
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "port");
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "ip", pBaseInfo->strIP ))
	{
		//获取%s节点%s的%s属性值失败
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "ip");
		return false;
	}
	
	xmlNodePtr p_path = xmlFile.GetChildNode(pField, "log");
	if(! p_path)
	{
		//获取%s的节点%s失败
		Global::logger->fatal(LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "log");
		return false;
	}

	if(! xmlFile.GetNodeStr(p_path, "path", pBaseInfo->strLogPath ))
	{
		//获取%s节点%s的%s属性值失败
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "log", "path");
		return false;
	}

	return true;
}

bool 	MODI_ServerConfig::Insert( const std::string & strKey , MODI_ISvrConfigInfo * pValue )
{
	MAP_CONFIGS_INSERT_RESULT itor = m_mapSvrConfigs.insert( std::make_pair( strKey , pValue ) );
	if( !itor.second )
	{
		MODI_ASSERT(0);
		return false;
	}
	return true;
}

bool 	MODI_ServerConfig::LoadGameSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	unsigned int nChannelCount = 1;
	MODI_SvrGSConfig * pGSInfo = new MODI_SvrGSConfig();
	while( true )
	{
		xmlNodePtr pRet = xmlFile.GetChildNode( pRoot , SVRCFG_CHANNEL 	, nChannelCount );
		if( !pRet )
			break;

		nChannelCount += 1;

		const std::string & file_name = xmlFile.GetFilename();

		unsigned short nGatePort = 0;
		unsigned short nPort = 0;
		unsigned short nWanGatePort = 0;
		std::string strIP;
		std::string strGateIP;
		std::string strWanGateIP;
		std::string strGateLogPath;
		std::string strLogPath;

		xmlNodePtr p_addr = xmlFile.GetChildNode( pRet , "addr");
		if(! p_addr)
		{
			//获取%s的节点%s失败
			Global::logger->fatal( LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "addr" );
			continue ;
		}

		if(! xmlFile.GetNodeNum(p_addr, "gsport", &nPort, sizeof(nPort)))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gsport");
			continue ;
		}

		if(! xmlFile.GetNodeNum(p_addr, "gwport", &nGatePort, sizeof(nGatePort)))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gwport");
			continue ;
		}

		if(! xmlFile.GetNodeNum(p_addr, "gwport_wan", &nWanGatePort, sizeof(nWanGatePort)))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gwport_wan");
			continue ;
		}

		if(! xmlFile.GetNodeStr(p_addr, "gsip", strIP ))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gsip");
			continue ;
		}

		if(! xmlFile.GetNodeStr(p_addr, "gwip", strGateIP ))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gwip");
			continue ;
		}

		if(! xmlFile.GetNodeStr(p_addr, "gwip_wan", strWanGateIP ))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "addr", "gwip_wan");
			continue ;
		}
		
		xmlNodePtr p_path = xmlFile.GetChildNode( pRet , "log");
		if(! p_path)
		{
			//获取%s的节点%s失败
			Global::logger->fatal(LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "log");
			continue ;
		}

		if(! xmlFile.GetNodeStr(p_path, "gspath", strLogPath ))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "log", "gspath");
			continue ;
		}

		if(! xmlFile.GetNodeStr(p_path, "gwpath", strGateLogPath ))
		{
			//获取%s节点%s的%s属性值失败
			Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "log", "gwpath");
			continue ;
		}

		// 存在
		const char * szChannel = "channel";

        ///	获得该服务器频道信息字段
        xmlNodePtr p_channel = xmlFile.GetChildNode( pRet , szChannel);
        if (!p_channel)
        {
			continue ;
        }

		const char * szChannelName = "name";
		const char * szChannelID = "id";
       //	获得服务器频道名
        std::string strName;
        if (!xmlFile.GetNodeStr(p_channel, szChannelName , strName))
        {
            Global::logger->fatal("[%s] No channel name of service <%s> specified.", SYS_INIT, szChannel);
			continue ;
        }

		if( pGSInfo->GetInfoByName( strName.c_str() ) )
		{
			// 存在重复名字的配置
            Global::logger->fatal("[%s] channel name<%s> is already in service <%s> specified.", SYS_INIT, strName.c_str() , 
					szChannel);
			continue ;
		}

		// 获得频道ID
		unsigned int nChannelID = 0;
		if( !xmlFile.GetNodeNum(p_channel, szChannelID , &nChannelID , sizeof(nChannelID) ) )
		{
            Global::logger->fatal("[%s] No channel id of service <%> specified.", SYS_INIT, szChannel);
			continue ;
		}

		if( pGSInfo->GetInfoByID( nChannelID ) )
		{
			// 存在重复ID的游戏频道
            Global::logger->fatal("[%s] channel id<%u> is already in service <%s> specified.", SYS_INIT, nChannelID , 
					szChannel);
			continue ;
		}

		MODI_SvrGSConfig::MODI_Info * pInfo = new MODI_SvrGSConfig::MODI_Info();
		pInfo->nChannelID = nChannelID;
		pInfo->strChannelName = strName;
		pInfo->strIP = strIP;
		pInfo->strGatewayIP = strGateIP;
		pInfo->nPort = nPort;
		pInfo->nGatewayPort = nGatePort;
		pInfo->strLogPath = strLogPath;
		pInfo->strGatewayLogPath = strGateLogPath;
		pInfo->strGatewayWanIP = strWanGateIP;
		pInfo->nGatewayWanPort = nWanGatePort;
		pGSInfo->vecGameServers.push_back( pInfo );
	}


	if( Insert( SVRCFG_CHANNEL 	, pGSInfo ) == false )
	{
		delete pGSInfo;
		return false;
	}
	
	return  true;
}

//bool 	MODI_ServerConfig::LoadGatewaySvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
//{
//	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_GATESERVER  );
//	if( !pFeild )
//		return false;
//
//	MODI_ISvrConfigInfo * pGateSvrInfo = new MODI_SvrGWConfig();
//	if( !LoadSvrBaseInfo( xmlFile , pRoot , pFeild  , pGateSvrInfo ) )
//	{
//		delete pGateSvrInfo;
//		return false;
//	}
//
//	if( !Insert( SVRCFG_GATESERVER , pGateSvrInfo ) )
//	{
//		delete pGateSvrInfo;
//		return false;
//	}
//
//	return true;
//}
//
bool 	MODI_ServerConfig::LoadRDBSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pRet = xmlFile.GetChildNode( pRoot , "gamedb" );
	if( !pRet )
		return false;
	xmlNodePtr p_dburl = xmlFile.GetChildNode( pRet , "url" );
	if( !p_dburl )
		return false;
	std::string db_url;
	if( !xmlFile.GetNodeStr( p_dburl  , "path" , db_url ) )
	{
		return false;
	}

	Global::g_stURLMap["db_name"] = db_url;
	Global::g_stURLMap["gamedb"] = db_url;
	unsigned int nChannelCount = 1;
	MODI_DBProxyConfig * p_dbproxy_ifno = new MODI_DBProxyConfig();
	p_dbproxy_ifno->set_db_url( db_url.c_str() );

	while( true )
	{
		xmlNodePtr pRet = xmlFile.GetChildNode( pRoot , SVRCFG_DBPROXY 	, nChannelCount );
		if( !pRet )
			break;

		nChannelCount += 1;

		std::string strLogPath;
		std::string strType;
		DBProxyType dbproxy_type;

		xmlNodePtr p_proxy = xmlFile.GetChildNode( pRet , "proxy");
		if(! p_proxy )
		{
			return false;
		}

		if( !xmlFile.GetNodeStr( p_proxy  , "type" , strType ) )
		{
			return false;
		}

		xmlNodePtr p_log = xmlFile.GetChildNode( pRet , "log");
		if(! p_log)
		{
			return false;
		}

		if(! xmlFile.GetNodeStr( p_log , "path", strLogPath ))
		{
			return false;
		}


		const char * szMsType = "managerserver_proxy";
		const char * szZsType = "zoneserver_proxy";
		const char * szGsType = "gameserver_proxy";
		if( strType == szMsType )
			dbproxy_type = kManagerServerProxy;
		else if( strType == szZsType )
			dbproxy_type = kZoneServerProxy;
		else if( strType == szGsType )
			dbproxy_type = kGameServerProxy;
		else 
		{
			MODI_ASSERT(0);
			return false;
		}

		unsigned int nDBProxyID = 0;
		if( !xmlFile.GetNodeNum( p_proxy  , "id" , &nDBProxyID , sizeof(nDBProxyID) ) )
		{
			return false;
		}

		MODI_DBProxyConfig::MODI_Info * pInfo = new MODI_DBProxyConfig::MODI_Info();
		pInfo->id = nDBProxyID;
		pInfo->strLogPath = strLogPath;
		pInfo->type = dbproxy_type;

		if( dbproxy_type == kGameServerProxy )
		{
			xmlNodePtr p_gsproxy = xmlFile.GetChildNode( pRet , "gameserver_proxy");
			if(! p_gsproxy  )
			{
				delete pInfo;
				return false;
			}

			WORD channel_num = 0;
			if( !xmlFile.GetNodeNum( p_gsproxy  , "id" , &channel_num, sizeof(channel_num)) )
			{
				delete pInfo;
				return false;
			}

			
			//// 先直接写进去
			for(WORD i = 1; i<channel_num+1; i++)
			{				
				p_dbproxy_ifno->AddGSID( pInfo , i);
			}

// 			int n = 0;
// 			char szBuf[32];
// 			memset( szBuf , 0 , sizeof(szBuf) );
// 			const char * szTemp = strChannel.c_str();
// 			while( *szTemp != '\0' )
// 			{
// 				if( *szTemp == ';' )
// 				{
// 					if( n > 0 )
// 					{
// 						unsigned int nNum = 0;
// 						if( safe_strtoul( &szBuf[n - 1] , &nNum ) )
// 						{
// 							if( nNum == 0 || 
// 								nNum >= 255 || 
// 								!p_dbproxy_ifno->AddGSID( pInfo , nNum ) )
// 							{
// 								delete pInfo;
// 								return false;
// 							}
// 						}
// 					}
// 					memset( szBuf , 0 , sizeof(szBuf) );
// 					n = 0;
// 				}
// 				else 
// 				{
// 					szBuf[n] = *szTemp;
// 					n++;
// 				}
// 				szTemp++;
// 			}
		}
			
		if( p_dbproxy_ifno->GetInfoByID( pInfo->id ) )
		{
			delete pInfo;
			return false;
		}

		p_dbproxy_ifno->vecDBProxy.push_back( pInfo );
	}

	if( Insert( SVRCFG_DBPROXY  , p_dbproxy_ifno ) == false )
	{
		delete p_dbproxy_ifno;
		return false;
	}
	
	return  true;
}

bool 	MODI_ServerConfig::LoadDBSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_ACCOUNTSERVER   );
	if( !pFeild )
		return false;

	const std::string & file_name = xmlFile.GetFilename();
	MODI_ISvrConfigInfo * pDBSvrInfo = new MODI_SvrADBConfig();
	if( !LoadSvrBaseInfo( xmlFile , pRoot , pFeild  , pDBSvrInfo ) )
	{
		delete pDBSvrInfo;
		return false;
	}

	xmlNodePtr pRet = xmlFile.GetChildNode( pRoot , SVRCFG_ACCOUNTSERVER  );
	if( !pRet )
		return false;

	xmlNodePtr p_db = xmlFile.GetChildNode( pRet , "db");
	if(!p_db)
	{
		Global::logger->fatal( LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "db" );
		return false;
	}

	std::string strName;
	if(!xmlFile.GetNodeStr(p_db , "url" , strName))
	{
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "db", "url");
		return false;
	}

	pDBSvrInfo->ToADBConfigPtr()->m_strURL = strName;

	xmlNodePtr p_platform = xmlFile.GetChildNode(pRet, "platform");
	if(!p_platform)
	{
		Global::logger->fatal( LOGINFO_XMLINFO3, LOGACT_SYS_INIT, file_name.c_str(), "flatform" );
		//return false;
	}
	
	if(!xmlFile.GetNodeStr(p_platform , "ip" , Global::g_strPlatformIP))
	{
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "flatform", "ip");
		//return false;
	}

	if(! xmlFile.GetNodeNum(p_platform, "port", &Global::g_wdPlatformPort, sizeof(Global::g_wdPlatformPort)))
	{
		//获取%s节点%s的%s属性值失败
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "platform", "port");
		//return false;
	}

	if(! xmlFile.GetNodeNum(p_platform, "useflag", &Global::g_wdPlatformFlag, sizeof(Global::g_wdPlatformFlag)))
	{
		//获取%s节点%s的%s属性值失败
		Global::logger->fatal(LOGINFO_XMLINFO4, LOGACT_SYS_INIT, file_name.c_str(), "platform", "flag");
		//return false;
	}

	if( !this->Insert( SVRCFG_ACCOUNTSERVER , pDBSvrInfo ) ) 
	{
		delete pDBSvrInfo;
		return false;
	}

	return true;
}

bool 	MODI_ServerConfig::LoadLoginSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_LOGINSERVER );
	if( !pFeild )
		return false;

	MODI_SvrLSConfig * pLSSvrInfo = new MODI_SvrLSConfig();
	if( !LoadSvrBaseInfo( xmlFile , pRoot , pFeild , pLSSvrInfo ) )
	{
		delete pLSSvrInfo;
		return false;
	}

	xmlNodePtr p_set = xmlFile.GetChildNode( pFeild ,"login_set");
	if( !p_set )
	{
		delete pLSSvrInfo;
		return false;
	}

	if( !xmlFile.GetNodeNum(p_set , "timeout" , &pLSSvrInfo->nLoginTimeOut , sizeof(pLSSvrInfo->nLoginTimeOut)) )
	{
		delete pLSSvrInfo;
		return false;
	}


	if( !this->Insert( SVRCFG_LOGINSERVER , pLSSvrInfo ) ) 
	{
		delete pLSSvrInfo;
		return false;
	}

	return true;
}

bool 	MODI_ServerConfig::LoadSvrMgr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_MANGGERSERVER   );
	if( !pFeild )
		return false;

	MODI_ISvrConfigInfo * pMsinfo = new MODI_SvrMSConfig();
	if( !LoadSvrBaseInfo( xmlFile , pRoot , pFeild  , pMsinfo ) )
	{
		delete pMsinfo;
		return false;
	}

	if( !this->Insert( SVRCFG_MANGGERSERVER , pMsinfo ) ) 
	{
		delete pMsinfo;
		return false;
	}

	return true;
}

bool 	MODI_ServerConfig::LoadZoneSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_ZONESERVER   );
	if( !pFeild )
		return false;

	MODI_SvrZSConfig * pZoneSvrInfo = new MODI_SvrZSConfig();
	if( !LoadSvrBaseInfo( xmlFile , pRoot , pFeild , pZoneSvrInfo ) )
	{
		delete pZoneSvrInfo;
		return false;
	}

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "dbproxy_port", &pZoneSvrInfo->nDBProxyPort, sizeof(pZoneSvrInfo->nDBProxyPort)))
	{
		return false;
	}

	if( !this->Insert( SVRCFG_ZONESERVER , pZoneSvrInfo ) ) 
	{
		delete pZoneSvrInfo;
		return false;
	}

	return true;
}

bool 	MODI_ServerConfig::LoadGameResource( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr p_resoure = this->GetField( xmlFile , pRoot , SVRCFG_GAMERESOURCE );
	if( !p_resoure )
	{
        Global::logger->fatal("[%s] Fail to find node: gameresource in file config.xml.", SYS_INIT);
		return false;
	}

    xmlNodePtr p_binsdir = xmlFile.GetChildNode(p_resoure, "binsdir");
    if (!p_binsdir)
    {
        Global::logger->fatal("[%s] Fail to find node: gameresource->binsdir in file config.xml.", SYS_INIT);
        return false;
    }

	xmlNodePtr p_mdmsdir = xmlFile.GetChildNode(p_resoure, "mdmsdir" );
	if( !p_mdmsdir )
	{
		Global::logger->fatal("[%s] Fail to find node: gameresource->mdmsdir in file config.xml.", SYS_INIT );
		return false;
	}

	MODI_SvrResourceConfig * pInfo  = new MODI_SvrResourceConfig();

    if (!xmlFile.GetNodeStr(p_binsdir, "dir", pInfo->strBinDir ))
    {
        Global::logger->fatal("[%s] No BIN file folder specified.", SYS_INIT);
		delete pInfo;
        return false;
    }

	if( !xmlFile.GetNodeStr( p_mdmsdir , "dir" , pInfo->strMdmDir ) )
	{
        Global::logger->fatal("[%s] No MDM file folder specified.", SYS_INIT);
		delete pInfo;
        return false;
	}

	if( !this->Insert( SVRCFG_GAMERESOURCE  , pInfo ) )
	{
		delete pInfo;
		return false;
	}

	return true;
}


bool 	MODI_ServerConfig::LoadBillSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_BILLSERVICE   );
	if( !pFeild )
		return false;

	MODI_SvrBillConfig * pBillSvrInfo = new MODI_SvrBillConfig();

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "billip", pBillSvrInfo->m_strBillIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneinip", pBillSvrInfo->m_strZoneInIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneoutip", pBillSvrInfo->m_strZoneOutIP))
	{
		return false;
	}

	Global::g_strBillIP = pBillSvrInfo->m_strZoneOutIP;
	if(! xmlFile.GetNodeNum(p_addr, "billport", &pBillSvrInfo->m_dwBillPort, sizeof(pBillSvrInfo->m_dwBillPort)))
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "zoneport", &pBillSvrInfo->m_dwZonePort, sizeof(pBillSvrInfo->m_dwZonePort)))
	{
		return false;
	}
	Global::g_wdBillPort = pBillSvrInfo->m_dwZonePort;

	/// 购买方式
	xmlNodePtr p_item = xmlFile.GetChildNode( pFeild , "item");
	if(p_item)
	{
		if(! xmlFile.GetNodeStr(p_item, "path", pBillSvrInfo->m_strURL))
		{
			Global::logger->fatal("[load_config] load bill item node failed");
			return false;
		}

		if(! xmlFile.GetNodeNum(p_item, "type", &Global::g_byGameShop, sizeof(Global::g_byGameShop)))
		{
			Global::logger->fatal("[load_config] load bill item node failed");
			return false;
		}
	}

	xmlNodePtr p_log = xmlFile.GetChildNode( pFeild , "log");
	if(! p_log)
	{
		return false;
	}
	
	if(! xmlFile.GetNodeStr(p_log, "path", pBillSvrInfo->m_strLogPath))
	{
		return false;
	}

	if( !this->Insert( SVRCFG_BILLSERVICE , pBillSvrInfo ) ) 
	{
		delete pBillSvrInfo;
		return false;
	}

	return true;
}


bool 	MODI_ServerConfig::LoadYYBillSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_YYBILLSERVICE   );
	if( !pFeild )
		return false;

	MODI_SvrYYBillConfig * pBillSvrInfo = new MODI_SvrYYBillConfig();

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "billip", pBillSvrInfo->m_strYYBillIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneip", pBillSvrInfo->m_strYYZoneIP))
	{
		return false;
	}

	//	Global::g_strYYBillIP = pBillSvrInfo->m_strZoneIP;
	if(! xmlFile.GetNodeNum(p_addr, "billport", &pBillSvrInfo->m_wdYYBillPort, sizeof(pBillSvrInfo->m_wdYYBillPort)))
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "zoneport", &pBillSvrInfo->m_wdYYZonePort, sizeof(pBillSvrInfo->m_wdYYZonePort)))
	{
		return false;
	}
	//	Global::g_wdBillPort = pBillSvrInfo->m_dwZonePort;

	xmlNodePtr p_log = xmlFile.GetChildNode( pFeild , "log");
	if(! p_log)
	{
		return false;
	}
	
	if(! xmlFile.GetNodeStr(p_log, "path", pBillSvrInfo->m_strLogPath))
	{
		return false;
	}


	/// load zoneinfo
	xmlNodePtr p_zoneinfo = xmlFile.GetChildNode( pFeild , "zoneinfo");
	if(! p_zoneinfo)
	{
		return false;
	}
	
	xmlNodePtr p_zone = xmlFile.GetChildNode(p_zoneinfo, "zone");
	while(p_zone)
	{
		std::string name;
		std::string zone_ip;
		if(! xmlFile.GetNodeStr(p_zone, "ip", zone_ip))
		{
			return false;
		}

		if(! xmlFile.GetNodeStr(p_zone, "name", name))
		{
			return false;
		}
		
		pBillSvrInfo->m_ZoneAddrMap.insert(std::map<std::string, std::string>::value_type(name, zone_ip));
		p_zone = xmlFile.GetNextNode(p_zone, "zone");
	}
	

	if( !this->Insert( SVRCFG_YYBILLSERVICE , pBillSvrInfo ) ) 
	{
		delete pBillSvrInfo;
		return false;
	}

	return true;
}


/// 加载infoservice配置
bool 	MODI_ServerConfig::LoadInfoSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_INFOSERVICE   );
	if( !pFeild )
		return false;

	MODI_SvrInfoConfig * pInfoSvrInfo = new MODI_SvrInfoConfig();

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "infoip", pInfoSvrInfo->m_strInfoIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneinip", pInfoSvrInfo->m_strZoneInIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneoutip", pInfoSvrInfo->m_strZoneOutIP))
	{
		return false;
	}

	Global::g_strInfoIP = pInfoSvrInfo->m_strZoneOutIP;
	if(! xmlFile.GetNodeNum(p_addr, "infoport", &pInfoSvrInfo->m_wdInfoPort, sizeof(pInfoSvrInfo->m_wdInfoPort)))
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "zoneport", &pInfoSvrInfo->m_wdZonePort, sizeof(pInfoSvrInfo->m_wdZonePort)))
	{
		return false;
	}
	Global::g_wdInfoPort = pInfoSvrInfo->m_wdZonePort;

	xmlNodePtr p_log = xmlFile.GetChildNode( pFeild , "log");
	if(! p_log)
	{
		return false;
	}
	
	if(! xmlFile.GetNodeStr(p_log, "path", pInfoSvrInfo->m_strLogPath))
	{
		return false;
	}

	/// load url
	xmlNodePtr p_url = xmlFile.GetChildNode( pFeild , "url");
	if(! p_url)
	{
		return false;
	}
	if(! xmlFile.GetNodeStr(p_url, "path", pInfoSvrInfo->m_strURL))
	{
		return false;
	}
	
	/// load zoneinfo
	xmlNodePtr p_zoneinfo = xmlFile.GetChildNode( pFeild , "zoneinfo");
	if(! p_zoneinfo)
	{
		return false;
	}
	xmlNodePtr p_zone = xmlFile.GetChildNode(p_zoneinfo, "zone");
	while(p_zone)
	{
		std::string zone_ip;
		WORD zone_id;
		if(! xmlFile.GetNodeNum(p_zone, "id", &zone_id, sizeof(zone_id)))
		{
			return false;
		}
		if(! xmlFile.GetNodeStr(p_zone, "ip", zone_ip))
		{
			return false;
		}
		pInfoSvrInfo->m_ZoneInfoMap.insert(std::map<WORD, std::string>::value_type(zone_id, zone_ip));
		p_zone = xmlFile.GetNextNode(p_zone, "zone");
	}

	if( !this->Insert( SVRCFG_INFOSERVICE , pInfoSvrInfo ) ) 
	{
		delete pInfoSvrInfo;
		return false;
	}

	return true;
}


/// 加载unifyservice配置
bool 	MODI_ServerConfig::LoadUnifySvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_UNIFYSERVICE   );
	if( !pFeild )
		return false;

	MODI_SvrUnifyConfig * pUnifySvrInfo = new MODI_SvrUnifyConfig();

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "unifyip", pUnifySvrInfo->m_strUnifyIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneinip", pUnifySvrInfo->m_strZoneInIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "zoneoutip", pUnifySvrInfo->m_strZoneOutIP))
	{
		return false;
	}

	Global::g_strUnifyIP = pUnifySvrInfo->m_strZoneOutIP;
	if(! xmlFile.GetNodeNum(p_addr, "unifyport", &pUnifySvrInfo->m_wdUnifyPort, sizeof(pUnifySvrInfo->m_wdUnifyPort)))
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "zoneport", &pUnifySvrInfo->m_wdZonePort, sizeof(pUnifySvrInfo->m_wdZonePort)))
	{
		return false;
	}
	Global::g_wdUnifyPort = pUnifySvrInfo->m_wdZonePort;

	xmlNodePtr p_log = xmlFile.GetChildNode( pFeild , "log");
	if(! p_log)
	{
		return false;
	}
	
	if(! xmlFile.GetNodeStr(p_log, "path", pUnifySvrInfo->m_strLogPath))
	{
		return false;
	}

	/// load url
	xmlNodePtr p_url = xmlFile.GetChildNode( pFeild , "url");
	if(! p_url)
	{
		return false;
	}
	if(! xmlFile.GetNodeStr(p_url, "path", pUnifySvrInfo->m_strURL))
	{
		return false;
	}
	
	/// load zoneinfo
	xmlNodePtr p_zoneinfo = xmlFile.GetChildNode( pFeild , "zoneinfo");
	if(! p_zoneinfo)
	{
		return false;
	}
	xmlNodePtr p_zone = xmlFile.GetChildNode(p_zoneinfo, "zone");
	while(p_zone)
	{
		std::string zone_ip;
		std::string zone_name;
		WORD zone_id;
		if(! xmlFile.GetNodeNum(p_zone, "id", &zone_id, sizeof(zone_id)))
		{
			return false;
		}
		if(! xmlFile.GetNodeStr(p_zone, "ip", zone_ip))
		{
			return false;
		}
		if(! xmlFile.GetNodeStr(p_zone, "name", zone_name))
		{
			return false;
		}
		
		pUnifySvrInfo->m_ZoneInfoMap.insert(std::map<WORD, std::string>::value_type(zone_id, zone_ip));
		pUnifySvrInfo->m_ZoneNameMap.insert(std::map<WORD, std::string>::value_type(zone_id, zone_name));
		p_zone = xmlFile.GetNextNode(p_zone, "zone");
	}

	if( !this->Insert( SVRCFG_UNIFYSERVICE , pUnifySvrInfo ) ) 
	{
		delete pUnifySvrInfo;
		return false;
	}

	return true;
}
 


/// 加载yyloginservice配置
bool 	MODI_ServerConfig::LoadYYLoginSvr( MODI_XMLParser & xmlFile , xmlNodePtr & pRoot )
{
	xmlNodePtr pFeild = this->GetField( xmlFile , pRoot , SVRCFG_YYLOGINSERVICE   );
	if( !pFeild )
		return false;

	MODI_SvrYYLoginConfig * pYYLoginSvrInfo = new MODI_SvrYYLoginConfig();

	xmlNodePtr p_addr = xmlFile.GetChildNode( pFeild , "addr");
	if(! p_addr)
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "yyloginip", pYYLoginSvrInfo->m_strYYLoginIP))
	{
		return false;
	}

	if(! xmlFile.GetNodeStr(p_addr, "infoip", pYYLoginSvrInfo->m_strInfoIP))
	{
		return false;
	}

	Global::g_strAccSyncIP = pYYLoginSvrInfo->m_strInfoIP;

	if(! xmlFile.GetNodeNum(p_addr, "yyloginport", &pYYLoginSvrInfo->m_wdYYLoginPort, sizeof(pYYLoginSvrInfo->m_wdYYLoginPort)))
	{
		return false;
	}

	if(! xmlFile.GetNodeNum(p_addr, "infoport", &pYYLoginSvrInfo->m_wdInfoPort, sizeof(pYYLoginSvrInfo->m_wdInfoPort)))
	{
		return false;
	}
	Global::g_wdAccSyncPort = pYYLoginSvrInfo->m_wdInfoPort;

	xmlNodePtr p_log = xmlFile.GetChildNode( pFeild , "log");
	if(! p_log)
	{
		return false;
	}
	
	if(! xmlFile.GetNodeStr(p_log, "path", pYYLoginSvrInfo->m_strLogPath))
	{
		return false;
	}

	
	if( !this->Insert( SVRCFG_YYLOGINSERVICE , pYYLoginSvrInfo ) ) 
	{
		delete pYYLoginSvrInfo;
		return false;
	}

	return true;
}


bool 	MODI_ServerConfig::Load( const char * szConfigFile )
{
	MODI_XMLParser xml;

	if(! xml.InitFile( szConfigFile ))
	{
		Global::logger->fatal( LOGINFO_XMLINFO1, LOGACT_SYS_INIT, szConfigFile );
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("config");
	if(! p_root)
	{
		//获取%s的根节点%s失败
		Global::logger->fatal(LOGINFO_XMLINFO2, LOGACT_SYS_INIT, szConfigFile , szConfigFile , "config");
		return false;
	}

	if(!LoadGameSvr( xml , p_root ) )
	{
		return false;
	}
	
//	if( !LoadGatewaySvr( xml , p_root ) )
//	{
//		return false;
//	}
//
	if( !LoadDBSvr( xml , p_root ) )
	{
		return false;
	}

	if( !LoadRDBSvr( xml , p_root ) )
	{
		return false;
	}

	if( !LoadLoginSvr( xml , p_root ) )
	{
		return false;
	}

	if( !LoadSvrMgr( xml , p_root ) )
	{
		return false;
	}

	if( !LoadZoneSvr( xml , p_root ) )
	{
		return false;
	}

	if( !LoadGameResource( xml , p_root ) )
	{
		return false;
	}

	if(! LoadBillSvr(xml, p_root))
	{
		return false;
	}

	if(! LoadYYBillSvr(xml, p_root))
	{
		return false;
	}

	if(! LoadInfoSvr(xml, p_root))
	{
		return false;
	}

	if(! LoadUnifySvr(xml, p_root))
	{
		return false;
	}

	if(!LoadYYLoginSvr(xml, p_root))
	{
		
	}

	return true;
}

void 	MODI_ServerConfig::Unload()
{
	MAP_CONFIGS_ITER itor = m_mapSvrConfigs.begin();
	while( itor != m_mapSvrConfigs.end() )
	{
		delete itor->second;
		itor++;
	}

	m_mapSvrConfigs.clear();
}

const MODI_ISvrConfigInfo * 	MODI_ServerConfig::GetConfig( const char * szKey )
{
	MAP_CONFIGS_ITER itor = m_mapSvrConfigs.find( szKey );
	if( itor != m_mapSvrConfigs.end() )
	{
		return itor->second;

	}
	return 0;
}

MODI_ServerConfig & 	MODI_ServerConfig::GetInstance()
{
	static MODI_ServerConfig ms_Inst;
	return ms_Inst;
}
