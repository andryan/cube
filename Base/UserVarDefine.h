/**
 * @file   UserVarDefine.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jun 13 17:29:28 2011
 * 
 * @brief  变量定义
 * 
 * 
 */

/**
 * @brief 变量介绍
 *
 * 变量属性有三个字段type,del,time
 *
 * type有normal,dayclear,dayinc,time trigger
 *
 * 比如type=normal_del=20110506;
 *
 * 表示改变量为普通类型，删除时间20110506零点
 *
 * type=time_time=3600
 * 3600秒的刷新周期
 *
 */


#ifndef _MD_USERVARDEFINE_H
#define _MD_USERVARDEFINE_H

/** 游戏服务器 **/


/** 区服务器 **/

/// 称号下降保留15天
const std::string CHENGHAO_XIAJIAN_KEEPTIME = "chenghao_xiajian_keeptime";
const std::string CHENGHAO_XIAJIAN_KEEPTIME_ATTRIBUTE = "type=trigger_time=1296000";

/// 称号下降24通知一次
const std::string CHENGHAO_XIAJIAN_NOTIFY = "chenghao_xiaojian_notify";
const std::string CHENGHAO_XIAJIAN_NOTIFY_ATTRIBUTE = "type=time_time=86400";

#endif
