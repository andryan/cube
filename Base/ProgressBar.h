/** 
 * @file ProgressBar.h
 * @brief 用于服务器的进度条
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-02
 */

#ifndef TERM_PROGRESSBAR_H
#define TERM_PROGRESSBAR_H

#include <stdio.h>

class MODI_BarGoLink
{
    char const * empty;
    char const * full;

    int rec_no;
    int rec_pos;
    int num_rec;
    int indic_len;


	protected:
	
		void Format( const char * szFormat , ... );
		virtual void PutOut(const char * szOut);
		virtual void Flush();

	public:

		void step( void );
		MODI_BarGoLink( int );
		virtual	~MODI_BarGoLink();
};


/** 
 * @brief 使用日志系统输出的终端进度条
 */
class MODI_BarGoLink_Logger : public MODI_BarGoLink
{
	protected:
	
		virtual void PutOut(const char * szOut);
		virtual void Flush();

	public:

		MODI_BarGoLink_Logger( int );
		virtual ~MODI_BarGoLink_Logger();

	private:

		char m_szTempOut[1024];
};
#endif
