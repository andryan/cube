/** 
 * @file DisbaleStrTable.h
 * @brief 脏字屏蔽表
 * @author Tang Teng
 * @version v0.1
 * @date 2010-09-08
 */

#ifndef MODI_DISABLE_STR_TABLE_H_
#define MODI_DISABLE_STR_TABLE_H_


#include <string>
#include <vector>
#include "Base/SingleObject.h"



class 	MODI_DisableStrTable : public CSingleObject<MODI_DisableStrTable>
{
	public:
	MODI_DisableStrTable();
	virtual ~MODI_DisableStrTable();

	bool Init( const char * szFileName );
	bool CheckDirtyWord(const char* str) const;
	void CleanUP();

	private:


	typedef std::vector<std::string> MODI_DirtyWordList;
	typedef MODI_DirtyWordList::iterator MODI_DirtyWordList_Iter;
	typedef MODI_DirtyWordList::const_iterator MODI_DirtyWordList_C_Iter;

	MODI_DirtyWordList m_vecDirtyWord;
};


#endif
