#include "BinFileMgr.h"
#include "Global.h"
#include "Base/AssertEx.h"
#include "protocol/gamedefine2.h"

MODI_BinFileMgr::MODI_BinFileMgr()
{

}

MODI_BinFileMgr::~MODI_BinFileMgr()
{

}

MODI_BinFileMgr * MODI_BinFileMgr::GetInstancePtr()
{
	static MODI_BinFileMgr s_Inst;
	return &s_Inst;
}

template <typename T>
bool LoadBinFile( T & binFile , const char * szBinFilesDir , const char * szBaseName ) {
	std::string strFullPath; 
	strFullPath = szBinFilesDir;
	strFullPath += "/";
	strFullPath += szBaseName;
	if( !binFile.Load( strFullPath.c_str() ) )
	{
		Global::logger->fatal("[%s] Fail to open sheet <%s>. Loading failure <Path = %s>.",
				SYS_INIT, szBaseName , strFullPath.c_str() );
		return false;
	}
	return true;
}

bool MODI_BinFileMgr::Initial(const char * szBinFilesDir , int iNoloadMask )
{
	const char * szMusicListBinFileBaseName = "MODI_Musiclist.bin";
	const char * szLevelListBinFileBaseName = "MODI_Level.bin";
	const char * szAvatarListBinFileBaseName = "MODI_Avatar.bin";
	const char * szDefaultAvatarListBinFileBaseName = "MODI_DefaultAvatar.bin";
	const char * szItemListBinFileBaseName = "MODI_Itemlist.bin";
	const char * szShopListBinFileBaseName = "MODI_Shop.bin";
	const char * szSuitListBinFileBaseName = "MODI_Suit.bin";
	const char * szSceneListBinFileBaseName = "MODI_Scenelist.bin";
	const char * szImpactListBinFileBaseName = "MODI_Impactlist.bin";
	const char * szGoodsPackageBinFileBaseName = "MODI_GoodsPackage.bin";
	const char * szChenghaoBinFileBaseName = "MODI_Chenghao.bin";
	const char * szAllResourceBinFileBaseName = "MODI_AllResource.bin";
	const char * szQuickBuyBinFileBaseName = "MODI_Quickbuy.bin";
	const char * szRandomdrawBinFileBaseName = "MODI_Randomdraw.bin";
	const char * szWaitRoomBinFileBaseName = "MODI_WaitRoom.bin";

	std::string strFullPath; 


	if( (iNoloadMask & enBinFT_Music) == 0 )
	{
		if( !LoadBinFile( m_MusicBinFile , szBinFilesDir , szMusicListBinFileBaseName ) )
			return false;
#ifdef _DEBUG
		for( size_t n = 0; n < m_MusicBinFile.GetSize(); n++ )
		{
			const GameTable::MODI_Musiclist * pItem = m_MusicBinFile.GetByIndex( n );
			if( pItem )
			{
				// Global::logger->debug("[%s] musicitem<configid=%u,name=%s> ", SVR_TEST ,
// 						pItem->get_ConfigID(),
// 						pItem->get_Songname() );
				;
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
#endif
	}

	if( (iNoloadMask & enBinFT_AllResource) == 0)
	{
		if( !LoadBinFile( m_AllResourceBinFile,szBinFilesDir,szAllResourceBinFileBaseName))
		{
			return false;
		}
	}

	if( (iNoloadMask & enBinFT_Scene) == 0 )
	{
		if( !LoadBinFile( m_SceneBinFile , szBinFilesDir ,  szSceneListBinFileBaseName) )
			return false;
	}

	if( (iNoloadMask & enBinFT_Level) == 0 )
	{
		if( !LoadBinFile( m_LevelBinFile , szBinFilesDir , szLevelListBinFileBaseName  ) )
			return false;		
		bool bCheckSuccessful = true;
		/// 检查所有等级是否都有配置
		for( int i = 1; i < MAX_LEVEL; i++ )
		{
			const GameTable::MODI_Level * pCheck =  m_LevelBinFile.Get( i );
			if( !pCheck )
			{
				Global::logger->fatal("[%s] Missing level data: <%d> in level sheet." , 
					SYS_INIT , i );	
				bCheckSuccessful = false;
			}
		}

		if( !bCheckSuccessful ) 
		{
			Global::logger->fatal("[%s] Fail to check sheet <level table>." , SYS_INIT );
			return false;
		}
	}

	if( (iNoloadMask & enBinFT_Avatar) == 0 )
	{
		if( !LoadBinFile( m_AvatarBinFile , szBinFilesDir ,  szAvatarListBinFileBaseName ) )
			return false;
	}

	if( (iNoloadMask & enBinFT_DefaultAvatar) == 0 )
	{
		if( !LoadBinFile( m_DefaultAvatarBinFile , szBinFilesDir ,  szDefaultAvatarListBinFileBaseName  ) )
			return false;
		// check default avatar table 
		if( !CheckDeafaultAvatarTable() )
		{
			Global::logger->fatal("[%s] Fail to check sheet <default avatar table>." , SYS_INIT );
			return false;
		}
	}

	if( (iNoloadMask & enBinFT_Item) == 0 )
	{
		if( !LoadBinFile( m_ItemBinFile , szBinFilesDir , szItemListBinFileBaseName  ) )
			return false;
	}

	if( (iNoloadMask & enBinFT_Shop) == 0 )
	{
		if( !LoadBinFile( m_ShopBinFile , szBinFilesDir , szShopListBinFileBaseName ) )
			return false;
		if( !CheckShopTable() )
		{
			Global::logger->fatal("[%s] check shop table faild.", SYS_INIT );
			return false;
		}
		if( (iNoloadMask & enBinFT_Item) == 0 )
		{
			if( !CheckShopGoods() )
			{
				Global::logger->fatal("[%s] check shop goods faild.", SYS_INIT );
				return false;
			}
		}
		else 
		{
			Global::logger->info("[%s] not load item/avatar tables. don't check shop goods .", SYS_INIT );
		}
	}

	if( (iNoloadMask & enBinFT_Suit) == 0 )
	{
		if( !LoadBinFile( m_SuitBinFile , szBinFilesDir ,szSuitListBinFileBaseName  ) )
			return false;
	}

	if( (iNoloadMask & enBinFT_Impact) == 0 )
	{
		if( !LoadBinFile( m_ImpactFile , szBinFilesDir ,szImpactListBinFileBaseName ) )
			return false;
	}

	if( (iNoloadMask & enBinFT_GoodsPackage ) == 0 )
	{
		if( !LoadBinFile( m_GoodsPackageBinFile , szBinFilesDir , szGoodsPackageBinFileBaseName ) )
			return false;

		if( (iNoloadMask & enBinFT_GoodsPackage ) == 0 )
		{
			if( !CheckGoodsPackage() )
			{
				Global::logger->fatal("[%s] check goods package faild.", SYS_INIT );
				return false;
			}
		}
		else 
		{
			Global::logger->info("[%s] not load item/avatar tables. don't check  goods package.", SYS_INIT );
		}
	}

	/// 加载称号表数据到内存
	if( (iNoloadMask & enBinFT_Chenghao) == 0 )
	{
		if( !LoadBinFile( m_ChenghaoBinFile , szBinFilesDir , szChenghaoBinFileBaseName ) )
		{
			Global::logger->fatal("[load_table] load table failed <name=MODI_Chenghao.csv>");
			return false;
		}
		
// #ifdef _HRX_DEBUG
// 		for(unsigned int i=0; i<m_ChenghaoBinFile.GetSize(); i++)
// 		{
// 			const GameTable::MODI_Chenghao * p_chenghao = m_ChenghaoBinFile.Get(i+1);
// 			if(p_chenghao)
// 			{
// 				Global::logger->debug("[load_table] show MODI_Chenghao.csv <record=%u,Name=%s,Content=%u, id=%u>", i+1, p_chenghao->get_Name(), p_chenghao->get_Contion()
// 									  ,p_chenghao->get_ConfigID());
// 			}
// 			else
// 			{
// 				MODI_ASSERT(0);
// 			}
// 		}
// #endif
	}

	if( (iNoloadMask & enBinFT_Quickbuy) == 0)
	{
		if( !LoadBinFile( m_QuickbuyBinFile,szBinFilesDir,szQuickBuyBinFileBaseName))
		{
			Global::logger->fatal("[load_table] load table failed <name=MODI_Quickbuy.csv>");
			return false;
		}
	}

	if( (iNoloadMask & enBinFT_Randomdraw) == 0)
	{
		if( !LoadBinFile( m_RandomdrawBinFile,szBinFilesDir,szRandomdrawBinFileBaseName))
		{
			Global::logger->fatal("[load_table] load table failed <name=MODI_Randomdrawbin>");
			return false;
		}
	}
	

	if( (iNoloadMask & enBinFT_WaitRoom) == 0)
	{
		if( ! LoadBinFile (m_WaitRoomBinFile, szBinFilesDir, szWaitRoomBinFileBaseName))
		{
			Global::logger->fatal("[load_bin] load wait room bin failed <path=%s,name=%s>", szBinFilesDir, szWaitRoomBinFileBaseName);
			return false;
		}
	}
	return true;
}

/// 获得某个等级所需的经验
bool MODI_BinFileMgr::GetNeedExpByLevel( unsigned char byLevel , unsigned int & nNeedExp )
{
	const GameTable::MODI_Level * pTemp = m_LevelBinFile.Get( byLevel );
	if( !pTemp )
	{
		return false;
	}

	nNeedExp = pTemp->get_Exp();
//	nNeedExp = ( 5 + pTemp->get_factor() ) * ((byLevel * byLevel) + (5 * byLevel));
	return true;
}
	
bool MODI_BinFileMgr::IsValidConfigInTables( WORD nConfigID , int iMask )
{
	if( nConfigID == INVAILD_CONFIGID )
		return false;

	if( iMask & enBinFT_Avatar )
	{
		if( this->GetAvatarBinFile().Get( nConfigID ) )
			return true;
	}

	if( iMask & enBinFT_Item )
	{
		if( this->GetItemBinFile().Get( nConfigID ) )
			return true;
	}

	if( iMask & enBinFT_Shop )
	{
		if( this->GetShopBinFile().Get( nConfigID ) )
			return true;
	}

	if( iMask & enBinFT_DefaultAvatar )
	{
		if( this->GetDefaultAvatarBinFile().Get( nConfigID ) )
			return true;
	}

	if( iMask & enBinFT_Suit )
	{
		if( this->GetSuitBinFile().Get( nConfigID ) )
			return true;
	}
	if( iMask & enBinFT_Quickbuy)
	{
		if( m_QuickbuyBinFile.Get(nConfigID))
		{
			return true;
		}
	}

	return false;
}

bool MODI_BinFileMgr::CheckDeafaultAvatarTable()
{
	return true;

	size_t nSize = m_DefaultAvatarBinFile.GetSize();
	if( !nSize )
		return false;

	for( size_t n = 0; n < nSize; n++ )
	{
		// 获得对应的config id
		const GameTable::MODI_DefaultAvatar * pDefault = m_DefaultAvatarBinFile.GetByIndex( n );
		if( !pDefault )
			return false;

		// 必须存在物品的部位是否有物品
		if( pDefault->get_Head() == INVAILD_CONFIGID ||
			pDefault->get_Hair() == INVAILD_CONFIGID ||
			pDefault->get_Upper() == INVAILD_CONFIGID ||
			pDefault->get_Lower() == INVAILD_CONFIGID ||
			pDefault->get_Hand() == INVAILD_CONFIGID ||
			pDefault->get_Foot() == INVAILD_CONFIGID )
		{
			Global::logger->debug("[%s] " , SVR_TEST );
			return false;
		}

		// 物品性别必须一样
	}

	return true;
}
	
bool MODI_BinFileMgr::CheckShopTable()
{
	for( size_t n = 0; n < GetShopBinFile().GetSize(); n++ )
	{
		const GameTable::MODI_Shop * pElement = GetShopBinFile().GetByIndex( n );
		if( !pElement )
		{
			Global::logger->fatal("[%s] check shop table faild.can't find element by index=%u." , 
					SYS_INIT ,
					n );

			return false;
		}
		if( pElement->get_ConfigID() == INVAILD_CONFIGID )
		{
			Global::logger->fatal("[%s] check shop table faild. element configid is invalid.",
					SYS_INIT );
			return false;
		}

		if( pElement->get_BuyStrategy() != kGameMoney &&
			pElement->get_BuyStrategy() != kRMBMoney )
		{
			Global::logger->fatal("[%s] check shop table faild.element<configid=%u>'s buy strategy<%u> invalid.",
					SYS_INIT ,
					pElement->get_ConfigID() ,
					pElement->get_BuyStrategy() );
			return false;
		}
		if( pElement->get_WhereSell() != kNormalShop &&
			pElement->get_WhereSell() != kRareShop )
		{
			Global::logger->fatal("[%s] check shop table faild.element<configid=%u>'s whereSell <%u> invalid.",
					SYS_INIT ,
					pElement->get_ConfigID() ,
					pElement->get_WhereSell() );
			return false;
		}
	}
	return true;
}

bool MODI_BinFileMgr::CheckShopGoods()
{
	for( size_t n = 0; n < GetShopBinFile().GetSize(); n++ )
	{
		const GameTable::MODI_Shop * pElement = GetShopBinFile().GetByIndex( n );
		if( !pElement )
		{
			Global::logger->fatal("[%s] checkshopgoods faild.can't find by index=%u." , SYS_INIT ,
					n );
			return false;
		}
		if( IsValidConfigInTables( pElement->get_SellItemID() , enBinFT_Item | enBinFT_Avatar ) == false )
		{
			Global::logger->fatal("[%s] checkshopgoods faild.is invalid config in table.configid=%u" , SYS_INIT ,
					pElement->get_ConfigID() );
			return false;
		}
	}
	return true;
}

bool MODI_BinFileMgr::CheckGoodsPackage()
{

#define GOODSPACKAGE_CHECK_HELP( no ) 	if( pElement->get_Goods_##no() != INVAILD_CONFIGID ) { \
	if( IsValidConfigInTables( pElement->get_Goods_##no() , enBinFT_Item | enBinFT_Avatar ) == false ) \
	{\
		Global::logger->fatal("[%s] check goods package faild.invalid config in table.configid=%u,goodid=%u" , SYS_INIT ,\
				pElement->get_ConfigID() , pElement->get_Goods_##no() );\
		return false;\
	}\
	if( pElement->get_Count_##no() == 0 )\
	{\
		Global::logger->fatal("[%s] check goods package faild.count is invalid<%u>.configid=%u" , SYS_INIT ,\
				pElement->get_Count_##no() , pElement->get_ConfigID() );\
		return false;\
	}\
	if( pElement->get_TimeLimit_##no() != kShopGoodT_7Day && pElement->get_TimeLimit_##no() != kShopGoodT_30Day && pElement->get_TimeLimit_##no() != kShopGoodT_Forver )\
	{\
		Global::logger->fatal("[%s] check goods package faild.time is invalid<%u>.configid=%u" , SYS_INIT ,\
				(int)pElement->get_TimeLimit_##no() , pElement->get_ConfigID() );\
		return false;\
	}\
	}

	for( size_t n = 0; n < GetGoodsPackageBinFile().GetSize(); n++ )
	{
		const GameTable::MODI_GoodsPackage * pElement = GetGoodsPackageBinFile().GetByIndex( n );
		if( !pElement )
		{
			Global::logger->fatal("[%s] check goods package faild.can't find by index=%u." ,SYS_INIT , n );
			return false;
		}

		GOODSPACKAGE_CHECK_HELP(1)
		GOODSPACKAGE_CHECK_HELP(2)
		GOODSPACKAGE_CHECK_HELP(3)
		GOODSPACKAGE_CHECK_HELP(4)
		GOODSPACKAGE_CHECK_HELP(5)
		GOODSPACKAGE_CHECK_HELP(6)
		GOODSPACKAGE_CHECK_HELP(7)
		GOODSPACKAGE_CHECK_HELP(8)
		GOODSPACKAGE_CHECK_HELP(9)
		GOODSPACKAGE_CHECK_HELP(10)
	}

	return true;
}

bool MODI_BinFileMgr::GetGoodsPackage( WORD nConfigID , MODI_GoodsPackageArray * pOut )
{
	if( nConfigID == INVAILD_CONFIGID || !pOut )
		return false;

	const GameTable::MODI_GoodsPackage * pElement = GetGoodsPackageBinFile().Get( nConfigID );
	if( !pElement )
		return false;

	WORD nIdx = 0;
#define GOODSPACKAGE_GET_HELP( no ) 	if( pElement->get_Goods_##no() != INVAILD_CONFIGID && pElement->get_Count_##no() ) { \
if( pElement->get_TimeLimit_##no() == kShopGoodT_7Day || pElement->get_TimeLimit_##no() == kShopGoodT_30Day || pElement->get_TimeLimit_##no() == kShopGoodT_Forver )\
{\
	pOut->m_array[nIdx].nGoodID = pElement->get_Goods_##no();\
	pOut->m_array[nIdx].nCount = pElement->get_Count_##no();\
	pOut->m_array[nIdx].time = (enShopGoodTimeType)pElement->get_TimeLimit_##no();\
	nIdx += 1;\
}\
}
	GOODSPACKAGE_GET_HELP(1)
	GOODSPACKAGE_GET_HELP(2)
	GOODSPACKAGE_GET_HELP(3)
	GOODSPACKAGE_GET_HELP(4)
	GOODSPACKAGE_GET_HELP(5)
	GOODSPACKAGE_GET_HELP(6)
	GOODSPACKAGE_GET_HELP(7)
	GOODSPACKAGE_GET_HELP(8)
	GOODSPACKAGE_GET_HELP(9)
	GOODSPACKAGE_GET_HELP(10)

	if( !nIdx )
		return false;

	pOut->m_count = nIdx;

	return true;
}
