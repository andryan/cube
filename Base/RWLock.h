/**
 * @file RWLock.h
 * @date 2009-12-15 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 系统读写锁
 *
 */

#ifndef _MDRWLOCK_H
#define _MDRWLOCK_H

#include <pthread.h>
#include "Type.h"


/**
 * @brief 系统读写锁
 *
 */
class MODI_RWLock : private MODI_DisableCopy
{
 public:
	/**
	 * @brief 系统锁初始化
	 *
	 */
	MODI_RWLock()
	{
		::pthread_rwlock_init(&m_rwlock, NULL);
	}
	

	/**
	 * @brief 释放
	 *
	 */
	~MODI_RWLock()
	{
		::pthread_rwlock_destroy(&m_rwlock);
	}
	

	/**
	 * @brief 读加锁
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	inline int rdlock()
	{
		int ret_code = 0;
		ret_code = ::pthread_rwlock_rdlock(&m_rwlock);
		return ret_code;
	}
	

	/**
	 * @brief 写加锁
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	inline int wrlock()
	{
		int ret_code = 0;
		ret_code = ::pthread_rwlock_wrlock(&m_rwlock);
		return ret_code;
	}
	

	/**
	 * @brief 解读写锁
	 *
	 * @return 成功返回0,失败返回错误代码
	 *
	 */
	inline int unlock()
	{
		int ret_code = 0;
		ret_code = ::pthread_rwlock_unlock(&m_rwlock);
		return ret_code;
	}

 private:
	/// 系统读写锁
	pthread_rwlock_t m_rwlock;
	
};


/**
 * @brief 自动对读锁加锁和解锁
 *
 */
class MODI_AutoRDLock: public MODI_DisableCopy
{
 public:
	/**
	 * @brief 自动加锁
	 *
	 */
	MODI_AutoRDLock(MODI_RWLock & lock): m_stRWLock(lock)
	{
		m_stRWLock.rdlock();
	}
		
	/**
	 * @brief 自动解锁
	 *
	 */
	~MODI_AutoRDLock()
	{
		m_stRWLock.unlock();
	}
	
 private:
	/// 读写锁引用
	MODI_RWLock & m_stRWLock;
};


/**
 * @brief 自动对写锁进行加锁和解锁
 *
 */
class MODI_AutoWRLock: public MODI_DisableCopy
{
 public:
	/**
	 * @brief 加锁
	 *
	 */
	MODI_AutoWRLock(MODI_RWLock & lock): m_stRWLock(lock)
	{
		m_stRWLock.wrlock();
	}

	/**
	 * @brief 解锁
	 *
	 */
	~MODI_AutoWRLock()
	{
		m_stRWLock.unlock();
	}
	
 private:
	/// 读写锁引用
	MODI_RWLock & m_stRWLock;
};

#endif

