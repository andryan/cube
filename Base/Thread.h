/**
 * @file Thread.h
 * @date 2009-12-14 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 线程封装
 *
 */

#ifndef _MDTHREAD_H
#define _MDTHREAD_H

#include <pthread.h>
#include "Type.h"
#include "Mutex.h"
#include "Cond.h"

/**
 * @brief 线程封装
 *
 *
 */
class MODI_Thread
{
 public:
	MODI_Thread(const char * name = "Thread"): m_strThreadName(name), m_blALive(false), m_blTerminate(false), m_stThread(0)
	{
		
	};

	/// 释放
	virtual ~MODI_Thread()
	{
		
	}

	/**
	 * @brief 是否在run
	 *
	 * @return 运行中true
	 *
	 */ 
	const bool IsAlive() const
	{
		return m_blALive;
	}

	/// 创建线程
	static void * InitThread(void * new_thread);

	/// 线程开始
	bool Start();

	/**
	 * @brief 置线程结束标志
	 *
	 */ 
	void TTerminate()
	{
		m_blTerminate = true;
	}

	/**
	 * @brief 线程是否结束
	 *
	 * @return 结束true
	 */ 
	const bool IsTTerminate() const 
	{
		return m_blTerminate;
	}

	/**
	 * @brief 重载此函数，为线程主循环
	 *
	 */
	virtual void Run() = 0;

	/**
	 * @brief 获取线程名字
	 *
	 * @return 返回线程名字
	 *
	 */
	const char * GetThreadName() const
	{
		return m_strThreadName.c_str();
	}

	/**
	 * @brief 等待结
	 *
	 */ 
	int Join()
	{
		int ret_code = 0;
		if(m_stThread)
		{
			ret_code = ::pthread_join(m_stThread, NULL);
		}
		m_stThread = 0;
		return ret_code;
	}
	
 private:
	/// 线程名字
	std::string m_strThreadName;
	
	/// 锁
	MODI_Mutex m_stLock;

	/// 条件变量
	MODI_Cond m_stCond;

	/// 是否在run
	volatile bool m_blALive;

	/// 是否要结束
	volatile bool m_blTerminate;

	/// 线程号
	pthread_t m_stThread;			
};

#endif
