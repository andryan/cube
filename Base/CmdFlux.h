/**
 * @file CmdFlux.h
 * @date 2010-04-14 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief command flux parse
 *
 */

#ifndef _MDCMDFLUX_H
#define _MDCMDFLUX_H

#include "Logger.h"

const int CMD_COUNT = 10;
const int PARA_COUNT = 256;

struct CmdInfo
{
	CmdInfo()
	{
		m_qwCount = 0;
		m_qwSize = 0;
	}
	QWORD m_qwCount;
	QWORD m_qwSize;
};

class MODI_CmdFlux
{
 public:
	MODI_CmdFlux(const char * flux_name): m_strName(flux_name)
	{
		memset(m_stCmdInfo, 0, sizeof(m_stCmdInfo));
		last_size = 0;
		last_count = 0;
	}
	
	void Put(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	void ShowInfo(bool bl_info = false);
	
	void Reset()
	{
		memset(m_stCmdInfo, 0, sizeof(m_stCmdInfo));
	}
 private:
	MODI_Mutex m_stMutex;
	CmdInfo m_stCmdInfo[10][256];
	std::string m_strName;

	QWORD last_size;
	QWORD last_count;
};

#endif
