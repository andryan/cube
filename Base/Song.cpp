
#include "Song.h"

/*****************************************************************************/
MODI_Note::MODI_Note()
{
	pitch = 0;
	startTime = 0.0f;
	endTime = 0.0f;
	prev = next = NULL;
}

/*****************************************************************************/
MODI_Note::~MODI_Note()
{
	if (NULL!=next)
	{
		delete next;
	    next=NULL;
	}
}

/*****************************************************************************/
MODI_Lyric::MODI_Lyric()
{
	//character = NULL;
	m_startTime = 0.0f;
	m_prev = m_next = NULL;
	m_numLinkNote = 0;
	m_last = false;
}

/*****************************************************************************/
MODI_Lyric::~MODI_Lyric()
{
	if (NULL!=m_next)
	{
		delete m_next;
	    m_next=NULL;
	}
}

/*****************************************************************************/
MODI_Sentence::MODI_Sentence()
{
	m_startLyric=NULL;
	m_endLyric=NULL;

	m_next=NULL;
	m_prev=NULL;
	m_bIsEffected=false;
}

/*****************************************************************************/
MODI_Sentence::~MODI_Sentence()
{
	if (NULL!=m_next)
	{
		delete m_next;
	    m_next = NULL;
	}
}

/*****************************************************************************/
MODI_Tempo::MODI_Tempo()
{
	m_dwTempo = 0;
	m_startTicks = 0;
	next = NULL;
	prev = NULL;
}

/*****************************************************************************/
MODI_Tempo::~MODI_Tempo()
{
	if (NULL!=next)
	{
	    delete next;
        next=NULL;
	}
}

/*****************************************************************************/
double MODI_Sentence::GetDurationOfAllLyric()
{
	MODI_Lyric* pStartLyric = GetStartLyric();
	MODI_Lyric* pCurrentLyric = pStartLyric;
	double  dDuration = 0.0f;
	int count = GetNumLyric();
	for(int i=0;i<count;i++)
	{
		dDuration += pCurrentLyric->GetDuration();
		pCurrentLyric = pCurrentLyric->GetNext();
	}
	return dDuration;
}
/*****************************************************************************/
MODI_Song::MODI_Song()
{
	m_notesListHead = m_curNote = NULL;
	m_lyricsListHead = m_curLyric = NULL;
	m_sentenceListHead = m_curSentence = NULL;
	m_tempoListHead = NULL;

	m_numNotes = m_numLyrics = m_numTempos = 0;

	//time1 = time2 = 0;
	m_noteOn = m_oldNoteOn = false;

	m_maxPitch = 0;
	m_minPitch = 128;
	m_maxDuration = 0.0f;
	m_minDuration = 4.0f;
	m_dDivision = 0;
}

/*****************************************************************************/
MODI_Song::~MODI_Song()
{
	if (m_notesListHead)
		delete m_notesListHead;
	m_notesListHead = NULL;

	if (m_lyricsListHead)
		delete m_lyricsListHead;
	m_lyricsListHead = NULL;

	if (m_sentenceListHead)
		delete m_sentenceListHead;
	m_sentenceListHead = NULL;

	if (NULL!=m_tempoListHead)
	{
	    delete m_tempoListHead;
        m_tempoListHead=NULL;
	}

}

/*****************************************************************************/
MODI_Song::MODI_Song( const string& fileName )
{
	m_notesListHead = m_curNote = NULL;
	m_lyricsListHead = m_curLyric = NULL;
	m_sentenceListHead = m_curSentence = NULL;
	m_tempoListHead = NULL;

	m_numNotes = m_numLyrics = m_numTempos = 0;

	m_name = fileName;
	//time1 = time2 = 0;
	m_noteOn = m_oldNoteOn = false;

	m_maxPitch = 0;
	m_minPitch = 128;
	m_maxDuration = 0.0f;
	m_minDuration = 4.0f;
}

/*****************************************************************************/
void MODI_Song::AddNote( MODI_Note * note )
{
	if ( NULL == m_notesListHead )
	{
		m_notesListHead = note;
	}
	else
	{
		m_curNote->next = note;
		note->prev = m_curNote;
	}
	m_curNote = note;
}

/*****************************************************************************/
void MODI_Song::AddLyric( MODI_Lyric * lyric)
{
	if ( NULL == m_lyricsListHead )
	{
		m_lyricsListHead = lyric;
	}
	else
	{
		m_curLyric->m_next = lyric;
		lyric->m_prev = m_curLyric;
	}
	m_curLyric = lyric;
}

/*****************************************************************************/
void MODI_Song::AddSentence( MODI_Sentence * sentence )
{
	if ( NULL == m_sentenceListHead )
	{
		m_sentenceListHead = sentence;
	}
	else
	{
		m_curSentence->m_next = sentence;
		sentence->m_prev = m_curSentence;
	}
	m_curSentence = sentence;
}

/*****************************************************************************/
void MODI_Song::AddTempo(MODI_Tempo *tempo)
{
	if ( NULL == m_tempoListHead )
	{	// ????????
		tempo->m_startTime = 0.0f;
		m_tempoListHead = tempo;
	}
	else
	{
		MODI_Tempo *pRear = m_tempoListHead;
		while(pRear->GetNext()!=NULL)
		{
			pRear = pRear->GetNext();
		}
		pRear->next = tempo;
		tempo->prev = pRear;
		tempo->m_startTime = pRear->m_startTime + 
			((long long)pRear->GetTempo())*((long long)(tempo->GetStartTicks() - pRear->GetStartTicks()))/m_dDivision/1000000.0;
	}
}

/*****************************************************************************/
void MODI_Song::PrevNote()
{
	m_curNote = m_curNote->prev;
}

/*****************************************************************************/
void MODI_Song::PrevLyric()
{
	m_curLyric = m_curLyric->m_prev;
}

/*****************************************************************************/
void MODI_Song::PrevSentence()
{
	m_curSentence = m_curSentence->m_prev;
}

/*****************************************************************************/
void MODI_Song::NextNote()
{
	m_curNote = m_curNote->next;
}

/*****************************************************************************/
void MODI_Song::NextLyric()
{
	m_curLyric = m_curLyric->m_next;
}

/*****************************************************************************/
void MODI_Song::NextSentence()
{
	m_curSentence = m_curSentence->m_next;
}

/*****************************************************************************/
void MODI_Song::HeadNote()
{
	m_curNote = m_notesListHead;
}

/*****************************************************************************/
void MODI_Song::HeadLyric()
{
	m_curLyric = m_lyricsListHead;
}

/*****************************************************************************/
void MODI_Song::HeadSentence()
{
	m_curSentence = m_sentenceListHead;
}

/*****************************************************************************/
bool MODI_Song::IsNoteRear()
{
	if (m_curNote) 
		return false;
	else 
		return true;
}

/*****************************************************************************/
bool MODI_Song::IsLyricRear()
{
	if (m_curLyric) 
		return false;
	else 
		return true;
}

/*****************************************************************************/
bool MODI_Song::IsSentenceRear()
{
	if (m_curSentence) 
		return false;
	else 
		return true;
}

/*****************************************************************************/
MODI_Note * MODI_Song::GetNoteByTime( double time ) const
{
	MODI_Note * pNote = m_curNote;

	while ( pNote->GetNext() && time > pNote->GetEndTime() )
	{
		if ( pNote->GetStartTime() < time && time < pNote->GetEndTime() )
			return pNote;

		if ( pNote->GetNext() && pNote->GetEndTime() < time && time < pNote->GetNext()->GetStartTime() )
			return pNote->GetNext();

		pNote = pNote->GetNext();
	}

	while ( pNote->GetPrev() && time < pNote->GetPrev()->GetEndTime() )
	{
		if ( pNote->GetStartTime() < time && time < pNote->GetEndTime() )
			return pNote;

		if ( pNote->GetNext() && pNote->GetEndTime() < time && time < pNote->GetNext()->GetStartTime() )
			return pNote->GetNext();

		pNote = pNote->GetPrev();
	}

	if (pNote->GetStartTime() < time && time < pNote->GetEndTime())
	{
		return pNote;
	}
	else
	{
		return NULL;
	}
}

// ???????????????????
double MODI_Song::GetBeatNumByTime(double dTime)
{
	MODI_Tempo *pTempo = GetTempoByTime(dTime);
	if(pTempo==NULL)
	{
		return 0.0;
	}
	double dTickNum = pTempo->GetStartTicks() + 
						(dTime - pTempo->GetStartTime()) * m_dDivision * 1000000.0 / pTempo->GetTempo();
	return dTickNum/m_dDivision/pTempo->m_dQuaNotePerBeat;
}
/*****************************************************************************/
MODI_Lyric * MODI_Song::GetLyricByTime( double time ) const
{
	MODI_Lyric * pLyric = m_curLyric;

	while ( pLyric->GetNext() && time >= pLyric->GetNext()->GetStartTime() )
	{

		pLyric = pLyric->GetNext();
	}

	while ( pLyric->GetPrev() && time < pLyric->GetStartTime() )
	{

		pLyric = pLyric->GetPrev();
	}

	if (pLyric->GetStartTime() < time && time < pLyric->GetEndTime())
	{
		return pLyric;
	}
	else
	{
		return NULL;
	}
}

/*****************************************************************************/
MODI_Sentence * MODI_Song::GetSentenceByTime( double time ) const
{
	MODI_Sentence * pSentence = m_curSentence;

	if (pSentence->GetStartTime() < time && time < pSentence->GetEndTime())
	{
		return pSentence;
	}

	while ( pSentence->GetNext())
	{
		pSentence = pSentence->GetNext();
		if (pSentence->GetStartTime() < time && time < pSentence->GetEndTime())
		{
			return pSentence;
		}
	}

	while ( pSentence->GetPrev())
	{
		pSentence = pSentence->GetPrev();
		if (pSentence->GetStartTime() < time && time < pSentence->GetEndTime())
		{
			return pSentence;
		}
	}

	return NULL;
}

/*****************************************************************************/
MODI_Tempo * MODI_Song::GetTempoByTicks(unsigned long ticks)
{
	MODI_Tempo *pTempo=m_tempoListHead;

	while (pTempo!=NULL)
	{
		if(ticks<pTempo->GetStartTicks())
		{
			pTempo=pTempo->GetPrev();
			break;
		}
		if (pTempo->GetNext()==NULL)
		{	// is the rear node
			break;
		}

		pTempo=pTempo->GetNext();
	}

	return pTempo;
}

double MODI_Song::GetAbsTimeByTicks(unsigned nTicks)
{
	MODI_Tempo *pTempo = GetTempoByTicks(nTicks);

	if(pTempo)
	{
		return pTempo->GetStartTime() +
			((long long)pTempo->GetTempo())*((long long)(nTicks - pTempo->GetStartTicks()))/m_dDivision/1000000.0;
	}
	
	return 0.0;
}

/*****************************************************************************/
void MODI_Song::Update( float time )
{
	while ( m_curNote->GetNext() && time > m_curNote->GetEndTime() )
	{
		if ( m_curNote->GetStartTime() < time && time < m_curNote->GetEndTime() )
			return;

		if ( m_curNote->GetNext() && m_curNote->GetEndTime() < time && time < m_curNote->GetNext()->GetStartTime() )
			return;

		NextNote();
	}

	while ( m_curNote->GetPrev() && time < m_curNote->GetPrev()->GetEndTime() )
	{
		if ( m_curNote->GetStartTime() < time && time < m_curNote->GetEndTime() )
			return;

		if ( m_curNote->GetNext() && m_curNote->GetEndTime() < time && time < m_curNote->GetNext()->GetStartTime() )
			return;

		PrevNote();
	}

	while ( m_curLyric->GetNext() && time > m_curLyric->GetStartTime() )
	{
		NextLyric();
	}

	while ( m_curLyric->GetPrev() && time < m_curLyric->GetPrev()->GetStartTime() )
	{
		PrevLyric();
	}

	while ( m_curSentence->GetNext() && time > m_curSentence->GetEndTime() )
	{
		if ( m_curSentence->GetStartTime() < time && time < m_curSentence->GetEndTime() )
			return;

		if ( m_curSentence->GetNext() && m_curSentence->GetEndTime() < time && time < m_curSentence->GetNext()->GetStartTime() )
			return;

		NextSentence();
	}

	while ( m_curSentence->GetPrev() && time < m_curSentence->GetPrev()->GetEndTime() )
	{
		if ( m_curSentence->GetStartTime() < time && time < m_curSentence->GetEndTime() )
			return;

		if ( m_curSentence->GetNext() && m_curSentence->GetEndTime() < time && time < m_curSentence->GetNext()->GetStartTime() )
			return;

		PrevSentence();
	}
}

int MODI_Sentence::GetType()
{
	for (AttriVectItor itor=m_attri.begin(); itor != m_attri.end(); ++itor)
	{
		if (TYPE == (*itor).m_strAttributeType)
		{
			return atoi((*itor).m_strValue.data());
		}
	}
	return -1;
}

float MODI_Sentence::GetMultiple()
{
	float result;
	for (AttriVectItor itor=m_attri.begin(); itor != m_attri.end(); ++itor)
	{
		if (TYPE == (*itor).m_strAttributeType)
		{
			result = (float)atoi((*itor).m_strValue.data())/100.0f;
			return result;
		}
	}
	return 1.0f;
}

bool MODI_Sentence::HasTag(const char* pcTag)
{
	for (AttriVectItor itor=m_attri.begin(); itor != m_attri.end(); ++itor)
	{
		if ((*itor).m_strAttributeType.compare(pcTag)==0)
		{
			return true;
		}
	}
	return false;
}

bool MODI_Sentence::isRap()
{
    return m_bRap;
}

bool MODI_Sentence::isInterMission()
{
    return m_bInterMission;
}

void MODI_Song::CutTime(float fHeadTime, float fEndTime)
{
	HeadLyric();
	while (!IsLyricRear())
	{
		if ((fHeadTime+fEndTime)*2 > GetCurLyric()->GetDuration())
		{// double 'cut time's larger than duration
			fHeadTime = 0.0f;
			fEndTime = GetCurLyric()->GetDuration()*0.4;
		}
		GetCurLyric()->m_startTime += fHeadTime;
		GetCurLyric()->m_endTime   -= fEndTime;

		GetCurLyric()->GetLinkNote()->startTime	+= fHeadTime;
		GetCurLyric()->GetLastNote()->endTime	-= fEndTime;

		NextLyric();
	}
	HeadLyric();

	HeadSentence();
}

// ?????????????Midi??Tempo
MODI_Tempo * MODI_Song::GetTempoByTime( double time )
{
	MODI_Tempo *pTempo = m_tempoListHead;

	while (pTempo!=NULL)
	{
		if( time < pTempo->GetStartTime() )
		{
			pTempo = pTempo->GetPrev();
			break;
		}
		if(pTempo->GetNext()==NULL)
		{	// is the rear node
			break;
		}

		pTempo = pTempo->GetNext();
	}

	return pTempo;
}

void MODI_Song::GetRefrainTime(double &timeBegin, double &timeEnd)
{
	MODI_Sentence *pSen = m_sentenceListHead;
	
	timeBegin = timeEnd = -1.0;

	// ??? type?2??4(????)?????
	while(pSen)
	{
		if(2 == pSen->GetType() || 4 == pSen->GetType())
		{
			timeBegin = pSen->GetStartTime();
			pSen = pSen->GetNext();
			break;
		}
		pSen = pSen->GetNext();
	}

	while(pSen)
	{
		if( pSen->GetType()==1 ||
			pSen->GetType()==4 ||
			pSen->GetType()==5)
		{	// ??????�Q??
			timeEnd = pSen->GetStartTime();
			break;
		}
		else if(pSen->HasTag("@intermission="))
		{	// ?t????????????
			timeEnd = pSen->GetEndTime();
			break;
		}

		pSen = pSen->GetNext();
	}


	return;
}

// ??????????????????????
void MODI_Song::MarkClimax()
{
	MODI_Sentence *pSen = m_sentenceListHead;
	// ?????????
	while(pSen->GetNext())
	{
		pSen = pSen->GetNext();
	}

	// ??????? type?2(????)?????
	while(pSen)
	{
		if(2 == pSen->GetType())
		{
			AttriVector &vAttri = pSen->m_attri;
			for (AttriVectItor itor=vAttri.begin(); itor != vAttri.end(); ++itor)
			{
				if (TYPE == (*itor).m_strAttributeType)
				{
					(*itor).m_strValue = "4";
					break;
				}
			}
			break;
		}
		pSen = pSen->GetPrev();
	}
}
/*****************************************************************************/
// Song.cpp
/*****************************************************************************/
