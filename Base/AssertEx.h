/** 
 * @file AssertEx.h
 * @brief 自定义的ASSERT,EXCEPTIONS系统
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-21
 */

#ifndef MODI_ASSERTEX_H_
#define MODI_ASSERTEX_H_

#include <assert.h>

/// MODI_ASSERT 	: 	普通断言
/// MODI_ASSERT_EXCEPTIONS : 异常输出＋断言
/// MODI_EXCEPTIONS : 异常输出

namespace MODIExceptions
{
	extern void AssertOut( const char * szFile , unsigned int nLine , const char * szAssertion );
	extern void AssertExceptions( const char * szFile , unsigned int nLine , const char * szFunction );
	extern void Exceptions( const char * szFile , unsigned int nLine , const char * szFunction , const char * szExceptionDes );
};

#define DISABLE_UNUSED_WARNING( exp ) ((void *)(&exp))

#ifdef _DEBUG

/*-----------------------------------------------------------------------------
 *  调试版本的相关定义
 *-----------------------------------------------------------------------------*/

#ifdef _WIN32

#define MODI_Assert( assertion ) { if( !(assertion) ) { MODIExceptions::AssertOut( __FILE__ , __LINE__ , #assertion ); assert(0); } }
#define MODI_AssertExceptions( assertion ) { if( !(assertion) ) { MODIExceptions::AssertExceptions( __FILE__ , __LINE__ , __FUNCTION__  ); assert(0); } } 
#define MODI_Exceptions( des ) {  MODIExceptions::Exceptions( __FILE__ , __LINE__ , __FUNCTION__ ,  des ); }

#else 

#define MODI_Assert( assertion ) { if( !(assertion) ) { MODIExceptions::AssertOut( __FILE__ , __LINE__ , #assertion ); assert(0); } }
#define MODI_AssertExceptions( assertion ){ if( !(assertion) ) { MODIExceptions::AssertExceptions( __FILE__ , __LINE__ ,__PRETTY_FUNCTION__ ); assert(0); } } 
#define MODI_Exceptions( des ) {  MODIExceptions::Exceptions( __FILE__ , __LINE__ , __PRETTY_FUNCTION__  ,  des ); }

#endif

#define MODI_ASSERT MODI_Assert
#define MODI_ASSERT_EXCEPTIONS MODI_AssertExceptions
#define MODI_EXCEPTIONS 	MODI_Exceptions

#else 

/*-----------------------------------------------------------------------------
 *  发行版本的定义
 *-----------------------------------------------------------------------------*/

#define MODI_Assert( assertion ) ((void)0)
#define MODI_AssertExceptional ((void)0)
#define MODI_Exceptions ((void)0)

#define MODI_ASSERT MODI_Assert
#define MODI_ASSERT_EXCEPTIONS MODI_AssertExceptions
#define MODI_EXCEPTIONS 	MODI_Exceptions

#endif // end 

// 异常捕捉
#ifdef MODI_CATCH_EXCEPTIONS
	#define MODI_BEGIN_TRYCATCH {try{
#ifdef MODI_WHEN_CATCH_EXCEPTIONS_ASSERT
	#define MODI_END_TRYCATCH }catch(const char * s) { MODI_ASSERT_EXCEPTIONS( s ); } catch(...){MODI_ASSERT_EXCEPTIONS( "" );}}
#else 
	#define MODI_END_TRYCATCH }catch(const char * s) { MODI_EXCEPTIONS( s ); } catch(...){MODI_EXCEPTIONS( "" );}}
#endif
#else 
	#define MODI_BEGIN_TRYCATCH 
	#define MODI_END_TRYCATCH 
#endif

#define MODI_ENTER_FUNCTION MODI_BEGIN_TRYCATCH 
#define MODI_LEAVE_FUNCTION MODI_END_TRYCATCH


#endif
