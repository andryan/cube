/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   14:20
	filename: 	functionPtr
	author	:	tt
	module	:	模板库
	purpose	:	函数指针(类方法指针)模板
	
	
	log		:
	07.06.14			加入函数指针模板,成员函数指针模板.但无法模板化部分void类型方法(函数)	
	08.09.10			修改部分代码,对模板进行特列化.使之可以模板化任意类型的单个参数的函数和类方法.
	
	Copyright (c) 汤腾
	
***********************************************************************/
	
#ifndef FUNCTION_PTR_H_
#define FUNCTION_PTR_H_

#ifdef _MSC_VER
#pragma once
#endif


//  [6/14/2007 qztt]
//	所有函数指针,成员函数指针等的基类
//	typename Param1			指向函数的参数列表
//	typename FunReturnType = bool		返回类型
template <typename Param1,typename FunReturnType>
class	CFunctionPtrBase 
{

public:

	virtual ~CFunctionPtrBase() {}

	virtual bool 	IsVaild() { return false; } 

public:

	virtual FunReturnType operator()( Param1  args ) = 0;

};

template <typename Param1,typename Param2,typename FunReturnType>
class	CFunctionPtrBase_2 
{

public:

	virtual ~CFunctionPtrBase_2() {}

public:

	virtual FunReturnType operator()( Param1  args , Param2 args2 ) = 0;

};

template <typename Param1,typename Param2,typename Param3,typename FunReturnType>
class	CFunctionPtrBase_3 
{

public:

	virtual ~CFunctionPtrBase_3() {}

public:

	virtual FunReturnType operator()( Param1  args , Param2 args2 , Param3 args3 ) = 0;

};



template <typename Param1>
class	CFunctionPtrBase<Param1,void>
{
public:

	virtual ~CFunctionPtrBase() {}
public:

	virtual void operator()( Param1  args ) = 0;
};

template <typename FunReturnType>
class	CFunctionPtrBase<void,FunReturnType>
{
public:

	virtual ~CFunctionPtrBase() {}
public:
	
	virtual FunReturnType operator()() = 0;
};

template<>
class	CFunctionPtrBase<void,void>
{
public:

	virtual ~CFunctionPtrBase() {}
public:
	
	virtual void operator()() = 0;
};


/*
	c 函数指针模板
*/

template <typename Param1,typename FunReturnType>
class	CFreeFunctionPtr : public CFunctionPtrBase<Param1,FunReturnType>
{

public:

	virtual ~CFreeFunctionPtr() {}
public:

	typedef FunReturnType ( *FunctionPtr)( Param1 );

	CFreeFunctionPtr( FunctionPtr  fun ):m_FreePtr( fun )
	{

	}

	virtual bool 	IsVaild() 
	{
		if( m_FreePtr )
			return true;
		return false;
	}

	virtual FunReturnType operator()(Param1 args)
	{
		return m_FreePtr(args);
	}

	void Set( FunctionPtr  fun )
	{
		m_FreePtr =  fun; 
	}

private:

	FunctionPtr   m_FreePtr;
};



template <typename Param1,typename Param2 , typename Param3, typename FunReturnType>
class	CFreeFunctionPtr_3 : public CFunctionPtrBase_3<Param1,Param2,Param3,FunReturnType>
{

public:

	virtual ~CFreeFunctionPtr_3() {}
public:

	typedef FunReturnType ( *FunctionPtr)( Param1 , Param2 , Param3 );

	CFreeFunctionPtr_3( FunctionPtr  fun ):m_FreePtr( fun )
	{

	}

	virtual FunReturnType operator()( Param1  args , Param2 args2 , Param3 args3 )
	{
		return m_FreePtr(args , args2 , args3 );
	}

	void Set( FunctionPtr  fun )
	{
		m_FreePtr =  fun; 
	}

	bool IsVaild() const
	{
		return m_FreePtr ? true : false;
	}

	inline operator bool() const
	{
		return this->IsVaild();
	}

	inline operator bool() 
	{
		return m_FreePtr ? true : false;
	}

private:

	FunctionPtr   m_FreePtr;
};


//	特例化 void 返回类型
template <typename Param1> 
class  CFreeFunctionPtr<Param1,void> : public CFunctionPtrBase<Param1,void>
{	

public:

	virtual ~CFreeFunctionPtr() {}
public:

	typedef void ( *FunctionPtr)( Param1 );

	CFreeFunctionPtr( FunctionPtr  fun ):m_FreePtr( fun )
	{

	}

	virtual void operator()(Param1 args)
	{
		m_FreePtr(args);
	}

	void Set( FunctionPtr  fun )
	{
		m_FreePtr =  fun; 
	}

private:

	FunctionPtr   m_FreePtr;
};


//	特例化 void 形参
template<typename FunReturnType> 
class  CFreeFunctionPtr<void , FunReturnType> : public CFunctionPtrBase< void , FunReturnType >
{	

public:

	virtual ~CFreeFunctionPtr() {}
public:

	typedef FunReturnType ( *FunctionPtr)();

	CFreeFunctionPtr( FunctionPtr  fun ):m_FreePtr( fun )
	{

	}

	virtual FunReturnType operator()()
	{
		return m_FreePtr();
	}

	void Set( FunctionPtr  fun )
	{
		m_FreePtr =  fun; 
	}

private:

	FunctionPtr   m_FreePtr;
};

//	特列化 void 返回值, void 形参
template<>
class CFreeFunctionPtr<void,void> : public CFunctionPtrBase<void,void>
{
public:

	virtual ~CFreeFunctionPtr() {}
public:

	typedef void ( *FunctionPtr)();

	CFreeFunctionPtr( FunctionPtr  fun ):m_FreePtr( fun )
	{

	}

	virtual void operator()()
	{
		return m_FreePtr();
	}

	void Set( FunctionPtr  fun )
	{
		m_FreePtr =  fun; 
	}

private:

	FunctionPtr   m_FreePtr;
};

/*
	c++ 成员函数指针模板
*/

//	类成员函数指针模版
//	typename ObjectType						类型
//	typename Param1						参数表
//	typename FunReturnType = bool			返回类型
template <typename ObjectType , typename Param1 , typename FunReturnType>
class	CMemberFunctionPtr : public CFunctionPtrBase<Param1,FunReturnType>
{

public:

	virtual ~CMemberFunctionPtr() {}
public:

	typedef	FunReturnType (ObjectType::*FunctionPtr)(Param1);

	CMemberFunctionPtr( FunctionPtr  fun , ObjectType * obj ) :
	m_MemberPtr( fun ),m_pObject( obj )
	{

	}

	virtual FunReturnType operator()( Param1  args )
	{
		return (m_pObject->*m_MemberPtr)(args);
	}

private:

	FunctionPtr 	m_MemberPtr;
	ObjectType *	m_pObject;

};


//	特例化 void 返回类型
template <typename ObjectType , typename Param1>
class	CMemberFunctionPtr<ObjectType,Param1,void> : public CFunctionPtrBase<Param1,void>
{

public:

	virtual ~CMemberFunctionPtr() {}
public:

	typedef	void (ObjectType::*FunctionPtr)(Param1);

	CMemberFunctionPtr( FunctionPtr  fun , ObjectType * obj ) :
	m_MemberPtr( fun ),m_pObject( obj )
	{

	}

	virtual void operator()( Param1  args )
	{
		(m_pObject->*m_MemberPtr)(args);
	}

private:

	FunctionPtr 	m_MemberPtr;
	ObjectType *	m_pObject;
};

//	特列化 void 形参
template <typename ObjectType , typename FunReturnType>
class	CMemberFunctionPtr<ObjectType,void,FunReturnType> : public CFunctionPtrBase<void,FunReturnType>
{

public:

	virtual ~CMemberFunctionPtr() {}
public:

	typedef	FunReturnType (ObjectType::*FunctionPtr)();

	CMemberFunctionPtr( FunctionPtr  fun , ObjectType * obj ) :
	m_MemberPtr( fun ),m_pObject( obj )
	{

	}

	virtual FunReturnType operator()()
	{
		return (m_pObject->*m_MemberPtr)();
	}

private:

	FunctionPtr 	m_MemberPtr;
	ObjectType *	m_pObject;
};


//	特列化 void 返回值 , void 形参 
template <typename ObjectType>
class	CMemberFunctionPtr<ObjectType,void,void> : public CFunctionPtrBase<void,void>
{

public:

	virtual ~CMemberFunctionPtr() {}
public:

	typedef	void (ObjectType::*FunctionPtr)();

	CMemberFunctionPtr( FunctionPtr  fun , ObjectType * obj ) :
	m_MemberPtr( fun ),m_pObject( obj )
	{

	}

	virtual void operator()()
	{
		(m_pObject->*m_MemberPtr)();
	}

private:

	FunctionPtr 	m_MemberPtr;
	ObjectType *	m_pObject;
};


#endif



