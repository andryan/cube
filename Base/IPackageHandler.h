/** 
 * @file IPackageHandler.h
 * @brief 消息包处理基础类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */
#ifndef IPACKAGE_HANDLER_H_
#define IPACKAGE_HANDLER_H_

#include <map>
#include "Type.h"
#include <assert.h>
#include "functionPtr.h"


/// 消息包处理返回值
enum
{
    enPHandler_OK, 
	enPHandler_Warning, 
	enPHandler_Kick,
   	enPHandler_Ban,
};

typedef CFreeFunctionPtr_3<void * ,const Cmd::stNullCmd * , const unsigned int ,int> defPackageHandler;
typedef std::map<unsigned short int, defPackageHandler> MAP_PACKAGEHANDLER;
typedef MAP_PACKAGEHANDLER::iterator MAP_PACKAGEHANDLER_ITER;
typedef std::pair<MAP_PACKAGEHANDLER_ITER, bool> MAP_PACKAGEHANDLER_INSERT_RESULT;


/**
 * @brief 游戏逻辑消息的派发处理
 *
 */
class MODI_IPackageHandler
{
public:

    MODI_IPackageHandler();

    virtual ~MODI_IPackageHandler();


	/** 
	 * @brief 初始化消息派发处理器
	 * 
	 * @return 成功返回TRUE 	
	 */
	bool 	Initialization();
	

    ///	处理一个消息包
    int DoHandlePackage(unsigned short int nPackage, void * pObj,
            const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	

    int DoHandlePackage(unsigned char ucCmd, unsigned char ucSubCmd, void * pObj,
            const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	

	const char * GetCmdName( unsigned char ucCmd , unsigned char ucSubCmd );

protected:

    void AddPackageHandler(unsigned char ucCmd, unsigned char ucSubCmd, defPackageHandler h,const char * szCmdName = 0);
	

    void AddPackageHandler(unsigned short int nCmd, defPackageHandler h,const char * szCmdName = 0);
	

	/** 
	 * @brief 初始化消息派发回调注册
	 */
    virtual bool FillPackageHandlerTable() = 0;

private:

	typedef std::map<WORD,std::string> 	MAP_CMDNAME;
	typedef MAP_CMDNAME::iterator 		MAP_CMDNAME_ITER;
	typedef MAP_CMDNAME::const_iterator MAP_CMDNAME_C_ITER;
	typedef std::pair<MAP_CMDNAME_ITER, bool> MAP_CMDNAME_INSERT_RESULT;

	MAP_CMDNAME 		m_mapCmdName;
    MAP_PACKAGEHANDLER 	m_mapHandlers;
	bool 				m_bInit;
};


#endif

