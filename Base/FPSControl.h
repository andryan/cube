/** 
 * @file FPSControl.h
 * @brief FPS控制器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-09-17
 */
#ifndef MODI_FPSCONTROL_H_
#define MODI_FPSCONTROL_H_


#include "TimeManager.h"

class 	MODI_FPSControl
{

	public:
		MODI_FPSControl();
		~MODI_FPSControl();

		void 	Initial( DWORD nFps );

		void 	Begin();

		void 	End();


	private:


		DWORD  	m_nFps;
		DWORD 	m_nBeginTime;
};



#endif
