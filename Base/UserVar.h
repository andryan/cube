/**
 * @file   UserVar.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:57:57 2011
 * 
 * @brief  用户身上的变量
 * 
 * 
 */

#ifndef _MD_BUSERVAR_H
#define _MD_BUSERVAR_H

#include "Global.h"
#include "Timer.h"
#include "s2rdb_cmd.h"
#include "DBClient.h"
#include "UserVarDefine.h"

/** 
 * @brief 用户变量
 * 
 * 
 */
template<typename user_type >
class MODI_UserVar
{
 public:
	/** 
	 * @param var_name 用户变量，变量不带任何信息，变量的信息都在
	 * Init里面的属性字段里面
	 *
	 * @param p_table 变量保存到那个数据库里面
	 *
	 */
	MODI_UserVar(const char * var_name, MODI_TableStruct * p_table)
	{
		strncpy(m_stVarData.m_szName, var_name, sizeof(m_stVarData.m_szName) - 1);
		m_pTable = p_table;
		m_dwAccountID = 0;
		m_blNeedUpdate = false;
	}

	virtual ~MODI_UserVar()
	{
			
	}

	/** 
	 * @brief  设置accid
	 * 
	 * @param id 
	 */
	void SetAccID(const defAccountID & id)
	{
		m_dwAccountID = id;
	}

	const defAccountID & GetAccountID() const 
	{
		return m_dwAccountID;
	}

	/** 
	 * @brief 获取变量名
	 * 
	 */
	const char * GetName() const 
	{
		return m_stVarData.m_szName;
	}

	/** 
	 * @brief 获取变量值
	 * 
	 */
	const WORD & GetValue() const 
	{
		return m_stVarData.m_wdValue;
	}

	/** 
	 * @brief 设置变量值
	 * 
	 */
	void SetValue(const WORD & _value)
	{
		m_stVarData.m_wdValue = _value;
		SetNeedUpdate();
		//UpDateToDB();
	}

	/** 
	 * @brief 增加变量值
	 * 
	 */
	void AddValue(const WORD & _value)
	{
		m_stVarData.m_wdValue += _value;
		//UpDateToDB();
		SetNeedUpdate();
	}

	/** 
	 * @brief 设置需要更新
	 * 
	 */
	void SetNeedUpdate()
	{
		m_blNeedUpdate = true;
	}

	void ClearNeedUpdate()
	{
		m_blNeedUpdate = false;
	}
	
	/** 
	 * @brief 获取变量类型
	 * 
	 */
	const BYTE & GetType() const 
	{
		return m_stVarData.m_byType;
	}


	/** 
	 * @brief 获取变量更新的时间
	 * 
	 */
	const DWORD & GetUpdateTime() const 
	{
		return m_stVarData.m_dwUpdateTime;
	}

	/** 
	 * @brief 获取变量删除的时间
	 * 
	 */
	const DWORD & GetDelTime() const 
	{
		return m_stVarData.m_dwDelTime;
	}


	/** 
	 * @brief 初始化
	 * 
	 * @param p_client 用户类型
	 * @param attribute 变量属性
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool Init(user_type * p_client, const char * attribute);



	/** 
	 * @brief 复位
	 * 
	 */
	void Reset()
	{
		/// m_stVarData.m_dwUpdateTime = cur_rtime.GetSec() + m_stVarData.m_dwAddTime;
		/// 先用global::的，后期整理好修改
		m_stVarData.m_dwUpdateTime = Global::m_stLogicRTime.GetSec() + m_stVarData.m_dwAddTime;
		m_stVarData.m_wdValue = 0;
		SetNeedUpdate();
		//UpDateToDB();
	}

	
	/** 
	 * @brief 变量更新
	 * 
	 */
	virtual void UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);


	/** 
	 * @brief 变量保存到数据库
	 * 
	 */
	virtual void SaveToDB();


	/** 
	 * @brief 从数据库中删除
	 * 
	 */
	virtual void DelFromDB();

	/** 
	 * @brief 更新数据库中的变量
	 * 
	 */
	virtual void UpDateToDB();


	/** 
	 * @brief  执行语句到数据库
	 * 
	 */
	virtual void ExecSqlToDB(const char * sql, const WORD sql_size) = 0;

	/// 数据
	MODI_VarData m_stVarData;
	
 private:
	
	/// 账号id变量标识
	defAccountID m_dwAccountID;
	MODI_TableStruct * m_pTable;
	bool m_blNeedUpdate;
};


/**
 * @brief 变量管理
 * 
 */
template<typename var_type >
class MODI_UserVarManager
{
 public:
	typedef	__gnu_cxx::hash_map<std::string, var_type *, str_hash > defUserVarMap;
	typedef	typename defUserVarMap::iterator defUserVarMapIter;
	typedef	typename defUserVarMap::value_type defUserVarMapValue;

	MODI_UserVarManager(): m_timerVarSave(1000 * 60 * 10)
	{
			
	}

	~MODI_UserVarManager()
	{
		Final();
	}
	
	/** 
	 * @brief 任务初始化
	 * 
	 */
	template<typename obj_type>
	bool Init(obj_type * p_client, const char * data, const WORD data_size)
	{
		if(!p_client || !data || data_size == 0)
			return true;
	
		/// 从数据库加载该用户的所有的变量
		MODI_VarData * p_startaddr = (MODI_VarData *)(data);
		for(WORD i = 0; i<data_size; i++)
		{
			var_type * p_new_var = new var_type(p_startaddr->m_szName);
			p_new_var->m_stVarData = *p_startaddr;
			p_new_var->Init(p_client, NULL);
			AddVar(p_new_var);
		
#ifdef _VAR_DEBUG
			Global::logger->debug("[load_user_var] add a new var from db <name=%s,value=%u,type=%d,time=%ul>",
							  p_new_var->GetName(), p_new_var->GetValue(), p_new_var->GetType(), p_new_var->GetUpdateTime());
#endif		
			p_startaddr++;
		}
		return true;
	}

	
	/** 
	 * @brief 增加一个新变量
	 * 
	 * @param p_var 新变量
	 *
	 */
	bool AddVar(var_type * p_var);


	/** 
	 * @brief 获取某个变量
	 * 
	 * @param var_name 变量名字
	 * 
	 * @return 失败返回null
	 *
	 */
	var_type * GetVar(const char * var_name);

	/** 
	 * @brief 获取某个变量如果没有存在的话，那么就自动生成一个
	 * 
	 * @param var_name 变量名字
	 * 
	 * @return 失败返回null
	 *
	 */

//	var_type  * GetVarOrNew(const char * var_name);

	/** 
	 * @brief 用户是否有该变量
	 * 
	 * @param var_name 变量名字
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool IsHaveVar(const char * var_name);


	/** 
	 * @brief 更新时间变量
	 * 
	 */
	void UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);



	/** 
	 * @brief 更新保存
	 * 
	 */
	void SaveUserVar();
	
	defUserVarMap & GetVarMap()
	{
		return m_stUserVarMap;
	}

 private:

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final()
	{
		m_stRWLock.wrlock();
		var_type * p_var = NULL;
		defUserVarMapIter iter = m_stUserVarMap.begin();
		for(; iter != m_stUserVarMap.end(); iter++)
		{
			p_var = iter->second;
			delete p_var;
			p_var = NULL;
		}
	
		m_stUserVarMap.clear();
		m_stRWLock.unlock();
	}
	
	/// 变量集合
	defUserVarMap m_stUserVarMap;

	MODI_RWLock m_stRWLock;

	MODI_TimerEqu m_stZeroEqu;

	/// 保存间隔
	MODI_Timer m_timerVarSave;
};


/** 
 * @brief 增加一个新变量
 * 
 * @param p_state 新任务
 *
 */
template <typename var_type>
bool MODI_UserVarManager<var_type>::AddVar(var_type * p_var)
{
	if(p_var)
	{
		std::pair<defUserVarMapIter, bool> ret_code;
		m_stRWLock.wrlock();
		ret_code = m_stUserVarMap.insert(defUserVarMapValue(p_var->GetName(), p_var));
		m_stRWLock.unlock();
		return ret_code.second;
	}
	return false;
}


/** 
 * @brief 获取某个变量
 * 
 * @param var_name 变量名字
 * 
 * @return 失败返回null
 *
 */
template <typename var_type>
var_type * MODI_UserVarManager<var_type>::GetVar(const char * var_name)
{
	var_type * p_var = NULL;
	m_stRWLock.wrlock();
	defUserVarMapIter iter = m_stUserVarMap.find(var_name);
	if(iter != m_stUserVarMap.end())
	{
		p_var = iter->second;
	}
	m_stRWLock.unlock();
	return p_var;
}


/** 
 * @brief 用户是否有该变量
 * 
 * @param var_name 变量名字
 * 
 * @return 成功true,失败false
 *
 */
template <typename var_type>
bool MODI_UserVarManager<var_type>::IsHaveVar(const char * var_name)
{
	bool ret_code = false;
	m_stRWLock.wrlock();
	defUserVarMapIter iter = m_stUserVarMap.find(var_name);
	if(iter != m_stUserVarMap.end())
	{
		ret_code = true;
	}
	m_stRWLock.unlock();
	return ret_code;
}


/** 
 * @brief 变量更新
 * 
 */
template <typename user_type>
void MODI_UserVar<user_type>::UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	if(m_stVarData.m_byType == (BYTE)MODI_VarState::enTimeClearType)
	{
		if(cur_rtime.GetSec() > m_stVarData.m_dwUpdateTime)
		{
			m_stVarData.m_dwUpdateTime = cur_rtime.GetSec() + m_stVarData.m_dwAddTime;
			
#ifdef _VAR_DEBUG
			Global::logger->debug("[set_var] reset var <name=%s>", GetName());
#endif			
			m_stVarData.m_wdValue = 0;
			//UpDateToDB();
		}
	}
}


/** 
 * @brief 变量保存到数据库
 * 
 */
template <typename user_type>
void MODI_UserVar<user_type>::SaveToDB()
{
	if(m_pTable == NULL)
		return;
	
	MODI_Record record_insert;
	record_insert.Put("accountid", m_dwAccountID);
	record_insert.Put("name", m_stVarData.m_szName);
	record_insert.Put("value", m_stVarData.m_wdValue);
	record_insert.Put("type", m_stVarData.m_byType);
	record_insert.Put("update_time", m_stVarData.m_dwUpdateTime);
	record_insert.Put("add_time", m_stVarData.m_dwAddTime);
	record_insert.Put("del_time", m_stVarData.m_dwDelTime);
	
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);
	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeInsertSql(result, m_pTable, &record_container))
	{
		Global::logger->fatal("[save_uservar_todb] make save user_var sql failed <accid=%u,var_name=%s>", m_dwAccountID, m_stVarData.m_szName);
		return;
	}
	
	ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 更新数据库中的变量
 * 
 */
template <typename user_type>
void MODI_UserVar<user_type>::UpDateToDB()
{
	if(!m_pTable)
	{	
		return;
	}
	if(! m_blNeedUpdate)
	{
		return;
	}
	
	ClearNeedUpdate();
	
	MODI_Record record_update;
	std::ostringstream where;
	where << "name=\'" << m_stVarData.m_szName << "\' and accountid=" << m_dwAccountID;
	record_update.Put("value", m_stVarData.m_wdValue);
	record_update.Put("update_time", m_stVarData.m_dwUpdateTime);
	record_update.Put("where", where.str());

	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeUpdateSql(result, m_pTable, &record_update))
	{
		Global::logger->fatal("[update_uservar] update uservar failed <accid=%u,var_name=%s>", m_dwAccountID, m_stVarData.m_szName);
		return;
	}
	
#ifdef _VAR_DEBUG
	Global::logger->fatal("[update_uservar] update uservar successful <accid=%u,var_name=%s>", m_dwAccountID, m_stVarData.m_szName);
#endif
	
	ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 从数据库中删除
 * 
 */
template <typename user_type>
void MODI_UserVar<user_type>::DelFromDB()
{
	if(! m_pTable)
		return;
	std::ostringstream os;
	std::string table_name = m_pTable->GetName();
	os<< "delete from " <<table_name << " where name=\'" << m_stVarData.m_szName << "\' and accountid=" << m_dwAccountID;
	ExecSqlToDB(os.str().c_str(), os.str().size());
}


/** 
 * @brief 初始化变量
 * 
 * 
 */
template <typename user_type>
bool MODI_UserVar<user_type>::Init(user_type * p_client, const char * attribute)
{
	if(attribute != NULL)
	{
		std::string var_attribute = attribute;

		/// type=normal_del=20110506_time=3600;

		MODI_SplitString split;
		split.Init(var_attribute.c_str(),"_");

		std::string var_type = split["type"];
		std::string var_del = split["del"];
		std::string var_time = split["time"];

#ifdef _VAR_DEBUG
		Global::logger->debug("[init_var] <name=%s,attribute=%s,type=%s,del=%s,time=%s>", GetName(),
							  var_attribute.c_str(), var_type.c_str(), var_del.c_str(), var_time.c_str());
#endif				

		/// 普通的变量，程序不管更新
		if(var_type == "normal")
		{
			m_stVarData.m_byType = MODI_VarState::enNormalType;
			m_stVarData.m_dwDelTime = MODI_TimeFun::DayToSec(var_del.c_str());
		}
		
		/// 0点更新
		else if(var_type == "dayclear")
		{
			m_stVarData.m_byType = MODI_VarState::enDayClearType;
			m_stVarData.m_dwDelTime = MODI_TimeFun::DayToSec(var_del.c_str());
		}

		/// 0点增加
		else if(var_type == "dayinc")
		{
			m_stVarData.m_byType = MODI_VarState::enDayIncType;
			m_stVarData.m_dwDelTime = MODI_TimeFun::DayToSec(var_del.c_str());
		}

		/// 时间更新
		else if(var_type == "time")
		{
			m_stVarData.m_byType = MODI_VarState::enTimeClearType;
			m_stVarData.m_dwDelTime = MODI_TimeFun::DayToSec(var_del.c_str());
			m_stVarData.m_dwAddTime = atoi(var_time.c_str());
			m_stVarData.m_dwUpdateTime = Global::m_stLogicRTime.GetSec() + m_stVarData.m_dwAddTime;
		}

		else if(var_type == "trigger")
		{
			m_stVarData.m_byType = MODI_VarState::enTriggerType;
			m_stVarData.m_dwDelTime = MODI_TimeFun::DayToSec(var_del.c_str());
			m_stVarData.m_dwAddTime = atoi(var_time.c_str());
			m_stVarData.m_dwUpdateTime = Global::m_stLogicRTime.GetSec() + m_stVarData.m_dwAddTime;
		}

		else
		{
			Global::logger->error("[var_init] init var <type=%s>", var_type.c_str());
			MODI_ASSERT(0);
			return false;
		}
		
	}
	
	m_dwAccountID = p_client->GetAccountID();
	return true;
}



/** 
 * @brief 所有变量更新
 * 
 */
template <typename var_type>
void MODI_UserVarManager<var_type>::UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	m_stRWLock.wrlock();
	defUserVarMapIter iter = m_stUserVarMap.begin();
	for(; iter != m_stUserVarMap.end(); iter++)
	{
		iter->second->UpDate(cur_rtime, cur_ttime);
	}
	m_stRWLock.unlock();

	/// 0点到了
	if(m_stZeroEqu(cur_ttime))
	{
		var_type * p_var = NULL;
		m_stRWLock.wrlock();
		defUserVarMapIter iter,next;
		for(next=m_stUserVarMap.begin(),iter=next; iter != m_stUserVarMap.end(); iter=next)
		{
			if(next != m_stUserVarMap.end())
			{
				next++;
			}
			
			p_var = iter->second;
			if(p_var)
			{
				if(p_var->GetType() == MODI_VarState::enDayClearType)
				{
					p_var->SetValue(0);
				}
				else if(p_var->GetType() == MODI_VarState::enDayIncType)
				{
					p_var->AddValue(1);
				}
				if((cur_rtime.GetSec() > p_var->GetDelTime()) && (p_var->GetDelTime() != 0))
				{
					p_var->DelFromDB();
					delete p_var;
					p_var = NULL;
					iter->second = NULL;
					m_stUserVarMap.erase(iter);
				}
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
		m_stRWLock.unlock();
	}
}



/** 
 * @brief 更新保存
 * 
 */
template <typename var_type>
void MODI_UserVarManager<var_type>::SaveUserVar()
{
	m_stRWLock.wrlock();
	defUserVarMapIter iter = m_stUserVarMap.begin();
	for(; iter != m_stUserVarMap.end(); iter++)
	{
		iter->second->UpDateToDB();
	}
	m_stRWLock.unlock();
}

#endif



