#include "PackageHandler_zs2ms.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "s2zs_cmd.h"
#include "s2unify_cmd.h"
#include "Base/HelpFuns.h"
#include "Base/AssertEx.h"
#include "OnlineClientMgr.h"
#include "GameSvrClientTask.h"
#include "ManangerClient.h"
#include "World.h"
#include "ZoneServerAPP.h"
#include "AccUserManager.h"
#include "protocol/c2gs_chatcmd.h"
#include "PackageHandler_s2zs.h"
#include "MailCore.h"

MODI_PackageHandler_zs2ms::MODI_PackageHandler_zs2ms()
{

}

MODI_PackageHandler_zs2ms::~MODI_PackageHandler_zs2ms()
{

}

bool MODI_PackageHandler_zs2ms::FillPackageHandlerTable()
{
	AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_LoginAuthResult::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::DirectionToGameServer );

	AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_ReMakePPResult::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::DirectionToGameServer );

	/// 告知防沉迷
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_Unify2S_ChenMiResult_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::DirectionToGameServer );
	
	/// gmtool访问游戏区的状态
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_QueryChannelStatus_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::QueryChannelStatus );

	/// gmtool发系统公告
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_Unify2S_SystemChat_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::GMSystemChat_Handler);

	/// 设置频道人数
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_Unify2S_SetChannelNum_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::SetChannelNum_Handler);

	/// gmtool 发送邮件
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_Unify2S_SendMail_Cmd::ms_SubCmd,
			&MODI_PackageHandler_zs2ms::GMSendMail_Handler);
	

	/// gmtool都某个玩家的操作
	AddPackageHandler( MAINCMD_S2UNIFY , MODI_Unify2S_GMOperateUser_Cmd::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::GMOperateUser_Handler );
	
	AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_ClientLogin::ms_SubCmd ,
			&MODI_PackageHandler_zs2ms::ClientLogin_Handler );

	return true;
}
	
int MODI_PackageHandler_zs2ms::DirectionToGameServer( void * pObj , 
		const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	if( pt_null_cmd->byCmd == MAINCMD_S2ADB ) 
	{
		if( pt_null_cmd->byParam == MODI_ADB2S_Notify_LoginAuthResult::ms_SubCmd )
		{
			// 验证结果的处理
			const MODI_ADB2S_Notify_LoginAuthResult * pMsg = (const MODI_ADB2S_Notify_LoginAuthResult *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pMsg->m_nServerID );
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
				//// 增加一个acc_user
				MODI_AccUserManager::GetInstance().AddAccUser(pMsg->m_nAccountID, pMsg->m_bFangChengMi);
			}
		}
		else if( pt_null_cmd->byParam == MODI_ADB2S_Notify_ReMakePPResult::ms_SubCmd )
		{
			// remake pp 的结果
			const MODI_ADB2S_Notify_ReMakePPResult * pMsg = (const MODI_ADB2S_Notify_ReMakePPResult *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pMsg->m_nServerID );
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
		}
	}
	
	/// 从unifyservice过来的命令
	else if(pt_null_cmd->byCmd == MAINCMD_S2UNIFY)
	{
		if(pt_null_cmd->byParam == MODI_Unify2S_ChenMiResult_Cmd::ms_SubCmd)
		{
			const MODI_Unify2S_ChenMiResult_Cmd * recv_cmd = (const MODI_Unify2S_ChenMiResult_Cmd *)pt_null_cmd;
			
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_OnlineClient * pOnline = pMgr->FindByAccid(recv_cmd->m_dwAccID);
			
			if( pOnline )
			{
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnline->GetServerID() );
				if( pServer )
				{
					MODI_ZS2S_Notify_ChenMi_Cmd send_cmd;
					send_cmd.m_charid = pOnline->GetCharID();
					send_cmd.m_enResult = recv_cmd->m_enResult;
					
					pServer->SendCmd(&send_cmd, sizeof(send_cmd));
					
#ifdef _HRX_DEBUG
					Global::logger->debug("[notify_chenmi] notify gameservice chenmi <accid=%u,chenmi=%d>", recv_cmd->m_dwAccID, recv_cmd->m_enResult);
#endif
					
				}
			}
		}			
	}
	
	return enPHandler_OK;
}


int MODI_PackageHandler_zs2ms::ClientLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_ClientLogin * pMsg = (const MODI_S2ZS_Request_ClientLogin *)(pt_null_cmd);

	Global::logger->info("[%s] Recv client<accid=%u,session=%s> login in package." , 
			LOGIN_OPT ,
			pMsg->m_accid ,
			pMsg->m_sessionID.ToString());

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByAccid( pMsg->m_accid );
	if( !pClient  )
	{
		Global::logger->info("[%s] client<accid=%u,session=%s> not on line , try load character's list from db." , 
				LOGIN_OPT ,
				pMsg->m_accid ,
				pMsg->m_sessionID.ToString());

		TryLoadCharacterListFromDB( pMsg->m_accid , pMsg->m_sessionID );
		return enPHandler_OK;
	}

	MODI_ASSERT( pClient->GetAccid() == pMsg->m_accid );

	Global::logger->info("[%s] anther client<accid=%u,charid=%u,gameserver_sessionid<%s>,loginserver_sessionid<%s>,serverid<%u>> login ." , 
			KICK_CLIENT ,
			pMsg->m_accid,
			pClient->GetCharID(),
			pClient->GetSessionID().ToString(),
			pMsg->m_sessionID.ToString() ,
			pClient->GetServerID() );


	if( pClient->GetServerID() > 0 )
	{
		// 客户端所在服务器
		MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pClient->GetServerID() );
		if( !pServer )
		{
			Global::logger->warn("[%s] client<%s> was online , but can't find his server<id=%u,name=%s>." , 
					SVR_TEST ,
					pClient->GetRoleName() , 
					pClient->GetServerID(),
					pClient->GetServerName() );

			// 该玩家所在服务器已经不存在
			// 删除该用户后，向db请求数据
			MODI_KickClientOp::Kick( pClient , false , 0 , kServerNotExist );

			TryLoadCharacterListFromDB( pMsg->m_accid , pMsg->m_sessionID );
			return enPHandler_OK;
		}

		// 在线，需要踢人下线
		MODI_ZS2S_Notify_KickSb kickmsg;
		kickmsg.m_byReason = kAntherLogin;
		kickmsg.m_guid = pClient->GetGUID();
		kickmsg.m_sessionID = pClient->GetSessionID();
		kickmsg.m_LoginServerSessionID = pMsg->m_sessionID;
		pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
	}
	else 
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s>'s server id is invalid<%u>. " , 
				SVR_TEST ,
				pClient->GetCharID() ,
				pClient->GetRoleName() , 
				pClient->GetServerID() );

		if( pClient->IsInWaitReEnterChannelState() )
		{

		}

		// 不在任何服务器上,直接顶掉
		pClient->SetGameState( kOutServer );
		pClient->SetServerID( 0 );
		pClient->SetServerName("unknow server");

		// 发送角色列表
		MODI_CharInfo charinfo;
		charinfo.m_byLevel = pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
		charinfo.m_bySex = pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
		charinfo.m_nCharID = pClient->GetCharID();
		safe_strncpy( charinfo.m_szRoleName , pClient->GetRoleName() , sizeof(charinfo.m_szRoleName) );
		charinfo.m_AvatarInfo = pClient->GetFullData().m_roleInfo.m_normalInfo.m_avatarData;
		charinfo.m_byReadHelp = pClient->GetFullData().m_roleInfo.m_detailInfo.m_byReadHelp;
		charinfo.m_byVip = pClient->GetFullData().m_roleInfo.m_baseInfo.m_bVip;
		MODI_PackageHandler_zs2ms::SendCharlist( pClient->GetAccid() , pMsg->m_sessionID , 1 , &charinfo , 1 );

		// 等待另外一个客户端进入
		pClient->SwtichToWaitAnotherLoginState();

		MODI_ASSERT(0);

		MODI_KickClientOp::Kick( pClient , false , 0 ,  kServerNotExist );
	}

	return enPHandler_OK;
}

void  MODI_PackageHandler_zs2ms::SendCharlist( defAccountID accid , const MODI_SessionID & sessionid , 
			BYTE byCharNum , const MODI_CharInfo * pCharinfo , BYTE byResult )
{
	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if( !pMc )
		return ;

	MODI_ASSERT( byCharNum <= 1 );
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_ZS2S_Notify_Charlist * pMsg = (MODI_ZS2S_Notify_Charlist *)(szPackageBuf);
	AutoConstruct( pMsg );
	pMsg->m_accid = accid;
	pMsg->m_nRoleCount = byCharNum;
	pMsg->m_sessionID = sessionid;
	pMsg->m_bySuccessful = byResult;
	if( byCharNum > 0 )
	{
		memcpy( pMsg->m_pChars , pCharinfo , sizeof(MODI_CharInfo) * byCharNum );
	}
	int iSendSize = sizeof(MODI_ZS2S_Notify_Charlist) + sizeof(MODI_CharInfo) * byCharNum;
	pMc->SendCmd( (const Cmd::stNullCmd *)szPackageBuf , iSendSize );
}
	
void  MODI_PackageHandler_zs2ms::TryLoadCharacterListFromDB( defAccountID accid , const MODI_SessionID & sessionid )
{
	const BYTE kOk = 0;
	const BYTE kNotExistDBProxy = 1;
	const BYTE kSendFaild = 2;

	BYTE byResult = kNotExistDBProxy;
	MODI_DBProxyClientTask * pDBProxy  = GetApp()->GetDBInterface(0);
	if( pDBProxy )
	{
		byResult = kSendFaild;

		MODI_S2RDB_Request_Charlist reqCharlist;
		reqCharlist.m_nAccountID = accid;
		reqCharlist.m_nSessionID = sessionid;
		if( pDBProxy->SendCmd( &reqCharlist , sizeof(reqCharlist) ) )
		{
			byResult = kOk;
		}
	}

	if( byResult != kOk )
	{
		Global::logger->info("[%s] Try load<accid=%u,session=%s> 's character list from DB faild<result=%u>.",
				LOGIN_OPT,
				accid,
				sessionid.ToString() ,
				byResult);

		// 返回失败的结果给ms
		MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
		if( pMc )
		{

		}
	}
}


/// 查询区频道信息
int MODI_PackageHandler_zs2ms::QueryChannelStatus( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_QueryChannelStatus_Cmd * p_recv_cmd = (const MODI_QueryChannelStatus_Cmd * )pt_null_cmd;
	
	char buf[1024];
	memset(buf, 0, sizeof(buf));
	MODI_ReturnChannelStatus_Cmd * p_send_cmd = (MODI_ReturnChannelStatus_Cmd * )buf;
	AutoConstruct(p_send_cmd);

	p_send_cmd->m_stSessionID = p_recv_cmd->m_stSessionID;
	
	int send_size = MODI_GameSvrClientTask::CreateChannelStatusToGMTool(p_send_cmd);

	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if( pMc )
	{
		pMc->SendCmd(p_send_cmd, send_size);
	}
	
	return enPHandler_OK;	
}


/// gm都某个玩家的操作
int MODI_PackageHandler_zs2ms::GMOperateUser_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_Unify2S_GMOperateUser_Cmd * p_recv_cmd = (const MODI_Unify2S_GMOperateUser_Cmd * )pt_null_cmd;
	MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
	send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
	send_cmd.operate_type = p_recv_cmd->operate_type;
	strncpy(send_cmd.m_strName, p_recv_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);

	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if(!pMc )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("[family_init] family manager unable get dbclient");
		send_cmd.m_byValue = 2;
		return enPHandler_OK;
	}
	
	MODI_ScopeHandlerRelease lock( p_dbclient );
	
	BYTE op = p_recv_cmd->operate_type;

	if(op == enGMDisableChatByGuid) /// 根据 角色id 对某个用户 禁言
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "guid=" << (QWORD)p_recv_cmd->m_stGuid;
		record_update.Put("gmlevel", 100);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%llu>",(QWORD)p_recv_cmd->m_stGuid);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByGUID(p_recv_cmd->m_stGuid);
				if(p_client)
				{
					p_client->SetGmLevel(100);
				}
			}
		}
	}
	else if(op == enGMDisableChatByAccid) ///  根据 账号id 对某个用户 禁言
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "accountid=" << p_recv_cmd->m_dwAccId;
		record_update.Put("gmlevel", 100);
		record_update.Put("where", where.str());
		
		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%u>",(QWORD)p_recv_cmd->m_dwAccId);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByAccid(p_recv_cmd->m_dwAccId);
				if(p_client)
				{
					p_client->SetGmLevel(100);
				}
			}
		}
	}
	else if(op == enGMDisableChatByAccName) /// 根据 账号名 对某个用户 禁言
	{
	}
	else if(op == enGMDisableChatByName) /// 根据 角色名 对某个用户 禁言
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "name=\'" << p_recv_cmd->m_strName << "\'";
		record_update.Put("gmlevel", 100);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <name=%s>",p_recv_cmd->m_strName);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByName(p_recv_cmd->m_strName);
				if(p_client)
				{
					p_client->SetGmLevel(100);
				}
			}
		}
	}
	else if(op == enGMEnableChatByGuid) ///根据 角色id 对某个用户 开放聊天
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "guid=" << (QWORD)p_recv_cmd->m_stGuid;
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%llu>",(QWORD)p_recv_cmd->m_stGuid);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByGUID(p_recv_cmd->m_stGuid);
				if(p_client)
				{
					p_client->SetGmLevel(0);
				}
			}
		}
	}
	else if(op == enGMEnableChatByAccid)///根据 账号id 对某个用户 开放聊天
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "accountid=" << p_recv_cmd->m_dwAccId;
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%u>",(QWORD)p_recv_cmd->m_dwAccId);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByAccid(p_recv_cmd->m_dwAccId);
				if(p_client)
				{
					p_client->SetGmLevel(0);
				}
			}
		}
	}
	else if(op == enGMEnableChatByAccName) ///根据 账号名 对某个用户 开放聊天
	{
		
	}
	else if(op == enGMEnableChatByName) ///根据 角色名 对某个用户 开放聊天
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "name=\'" << p_recv_cmd->m_strName << "\'";
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <name=%s>",p_recv_cmd->m_strName);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 不下线直接处理
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * p_client = pMgr->FindByName(p_recv_cmd->m_strName);
				if(p_client)
				{
					p_client->SetGmLevel(0);
				}
			}
		}
	}

	else if(op == enKickUserByGuid) ///根据 角色id 对某个用户 踢下线
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		if( pMgr )
		{
			MODI_OnlineClient * wait_be_kick = pMgr->FindByGUID(p_recv_cmd->m_stGuid);
			if( wait_be_kick )
			{
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
				if( pServer )
				{
					MODI_ZS2S_Notify_KickSb kickmsg;
					kickmsg.m_byReason = kAdminKick;
					kickmsg.m_guid = wait_be_kick->GetGUID();
					kickmsg.m_sessionID = wait_be_kick->GetSessionID();
					pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				}
			}
			else
			{
				send_cmd.m_byValue = 2;
			}
		}
	}
	else if(op == enKickUserByAccid) /// 根据 账号id 对某个用户 踢下线
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		if( pMgr )
		{
			MODI_OnlineClient * wait_be_kick = pMgr->FindByAccid(p_recv_cmd->m_dwAccId);
			if( wait_be_kick )
			{
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
				if( pServer )
				{
					MODI_ZS2S_Notify_KickSb kickmsg;
					kickmsg.m_byReason = kAdminKick;
					kickmsg.m_guid = wait_be_kick->GetGUID();
					kickmsg.m_sessionID = wait_be_kick->GetSessionID();
					pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				}
			}
			else
			{
				send_cmd.m_byValue = 2;
			}
		}
	}
	else if(op == enKickUserByAccName) ///根据 账号名 对某个用户 踢下线
	{
		
	}
	else if(op == enKickUserByName) /// 根据 角色名 对某个用户 踢下线
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		if( pMgr )
		{
			MODI_OnlineClient * wait_be_kick = pMgr->FindByName(p_recv_cmd->m_strName);
			if( wait_be_kick )
			{
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
				if( pServer )
				{
					MODI_ZS2S_Notify_KickSb kickmsg;
					kickmsg.m_byReason = kAdminKick;
					kickmsg.m_guid = wait_be_kick->GetGUID();
					kickmsg.m_sessionID = wait_be_kick->GetSessionID();
					pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				}
			}
			else
			{
				send_cmd.m_byValue = 2;
			}
		}
	}

	else if(op == enFrostUserByGuid) /// 根据 角色id 封号
	{
		/// 封号
		MODI_Record record_update;
		std::ostringstream where;
		where << "guid=" << (QWORD)p_recv_cmd->m_stGuid;
		record_update.Put("gmlevel", 200);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%llu>",(QWORD)p_recv_cmd->m_stGuid);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 踢人
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * wait_be_kick = pMgr->FindByGUID(p_recv_cmd->m_stGuid);
				if( wait_be_kick )
				{
					MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
					if( pServer )
					{
						MODI_ZS2S_Notify_KickSb kickmsg;
						kickmsg.m_byReason = kAdminKick;
						kickmsg.m_guid = wait_be_kick->GetGUID();
						kickmsg.m_sessionID = wait_be_kick->GetSessionID();
						pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
					}
				}
			}
		}
	}
	else if(op == enFrostUserByAccid) /// 根据 账号id 封号
	{
		/// 封号
		MODI_Record record_update;
		std::ostringstream where;
		where << "accountid=" << p_recv_cmd->m_dwAccId;
		record_update.Put("gmlevel", 200);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%u>",(QWORD)p_recv_cmd->m_dwAccId);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 踢人
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * wait_be_kick = pMgr->FindByAccid(p_recv_cmd->m_dwAccId);
				if( wait_be_kick )
				{
					MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
					if( pServer )
					{
						MODI_ZS2S_Notify_KickSb kickmsg;
						kickmsg.m_byReason = kAdminKick;
						kickmsg.m_guid = wait_be_kick->GetGUID();
						kickmsg.m_sessionID = wait_be_kick->GetSessionID();
						pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
					}
				}
			}
		}
		
	}
	else if(op == enFrostUserByAccName) /// 根据 账号名 封号
	{
		return enPHandler_OK;
	}
	else if(op == enFrostUserByName) ///  根据 角色名 封号
	{
		/// 封号
		MODI_Record record_update;
		std::ostringstream where;
		where << "name=\'" << p_recv_cmd->m_strName << "\'";
		record_update.Put("gmlevel", 200);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <name=%s>",p_recv_cmd->m_strName);
			send_cmd.m_byValue = 2;
		}
		else
		{
			/// 踢人
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			if( pMgr )
			{
				MODI_OnlineClient * wait_be_kick = pMgr->FindByName(p_recv_cmd->m_strName);
				if( wait_be_kick )
				{
					MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
					if( pServer )
					{
						MODI_ZS2S_Notify_KickSb kickmsg;
						kickmsg.m_byReason = kAdminKick;
						kickmsg.m_guid = wait_be_kick->GetGUID();
						kickmsg.m_sessionID = wait_be_kick->GetSessionID();
						pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
					}
				}
			}
		}
	}

	else if(op == enThawUserByGuid) /// 根据 角色id 解封
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "guid=" << (QWORD)p_recv_cmd->m_stGuid;
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%llu>",(QWORD)p_recv_cmd->m_stGuid);
			send_cmd.m_byValue = 2;
		}
	}
	else if(op == enThawUserByAccid) /// 根据 账号id 解封
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "accountid=" << p_recv_cmd->m_dwAccId;
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <guid=%u>",(QWORD)p_recv_cmd->m_dwAccId);
			send_cmd.m_byValue = 2;
		}
	}
	else if(op == enThawUserByAccName) /// 根据 账号名 解封
	{
		
	}
	else if(op == enThawUserByName) ///  根据 角色名 解封
	{
		MODI_Record record_update;
		std::ostringstream where;
		where << "name=\'" << p_recv_cmd->m_strName << "\'";
		record_update.Put("gmlevel", 0);
		record_update.Put("where", where.str());

		if(p_dbclient->ExecUpdate(MODI_ZoneServerAPP::m_pCharactersTbl, &record_update) != 1)
		{
			Global::logger->fatal("[gm_forsk_guid] forsk failed <name=%s>",p_recv_cmd->m_strName);
			send_cmd.m_byValue = 2;
		}
	}
	else
	{
		send_cmd.m_byValue = 2;
	}

	pMc->SendCmd(&send_cmd, sizeof(send_cmd));
	return enPHandler_OK;	
}

/// gm发布公告
int MODI_PackageHandler_zs2ms::GMSystemChat_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_Unify2S_SystemChat_Cmd * p_recv_cmd = (const MODI_Unify2S_SystemChat_Cmd *)pt_null_cmd;
	char buf[1024];
	memset(buf, 0, sizeof(buf));
    MODI_GS2C_Notify_SystemBroast * p_notify = (MODI_GS2C_Notify_SystemBroast *)(buf);
    AutoConstruct(p_notify);
	std::string notify_content = p_recv_cmd->m_cstrContent;
	p_notify->m_nSize = notify_content.size();
	strncpy(p_notify->m_pContents, notify_content.c_str(), notify_content.size());
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(pMgr)
	{
		pMgr->Broadcast(p_notify, sizeof(MODI_GS2C_Notify_SystemBroast) + p_notify->m_nSize);
	}

	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
	send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
	if(pMc)
	{
		pMc->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	
	return enPHandler_OK;
}


/// gm修改频道人数设置
int MODI_PackageHandler_zs2ms::SetChannelNum_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_Unify2S_SetChannelNum_Cmd * p_recv_cmd = (const MODI_Unify2S_SetChannelNum_Cmd *)pt_null_cmd;

	DWORD nClientNum = 0;
	if( !safe_strtoul( p_recv_cmd->m_cstrContent , &nClientNum ) )
	{
		return enPHandler_OK;
	}

	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
	send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
	if(!pMc)
	{
		return enPHandler_OK;
	}

	
	if(nClientNum < MODI_GameSvrClientTask::m_wdChannelMaxClient)
	{
		send_cmd.m_byValue = 0;
		pMc->SendCmd(&send_cmd, sizeof(send_cmd));
		return enPHandler_OK;
	}
	
	pMc->SendCmd(&send_cmd, sizeof(send_cmd));
	MODI_GameSvrClientTask::m_wdChannelMaxClient = nClientNum;
	return enPHandler_OK;
	
	
	{
		pMc->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	
	return enPHandler_OK;
}

/// gmtool 发送邮件
int MODI_PackageHandler_zs2ms::GMSendMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if(!pMc )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	const MODI_Unify2S_SendMail_Cmd * p_recv_cmd = (const MODI_Unify2S_SendMail_Cmd *)pt_null_cmd;
	MODI_Unify2S_SendMail_Cmd * p_change_cmd = const_cast<MODI_Unify2S_SendMail_Cmd *>(p_recv_cmd);

	/// 条件发送，直接发送命令到role
	if(p_recv_cmd->m_byType == 3)
	{
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
		if( pDB )
		{
			/// 发送命令
			MODI_SendMailByCondition_Cmd send_cmd;
			send_cmd.m_stMailCmd = *p_recv_cmd;
			pDB->SendCmd(&send_cmd, sizeof(MODI_SendMailByCondition_Cmd));
			return enPHandler_OK;
		}
		else
		{
			MODI_ASSERT(0);
			MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
			send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
			send_cmd.m_byValue = 2;
			strncpy(send_cmd.m_strName, p_recv_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);
			if(pMc)
			{
				pMc->SendCmd(&send_cmd, sizeof(send_cmd));
			}
		}
		
		return enPHandler_OK;
	}
	
	MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
	MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();

	MODI_OfflineUser * p_offclient = NULL;
	MODI_OnlineClient * p_onlineclient = NULL;
	
	if(p_recv_cmd->m_byType == 1)
	{
		std::string acc_name = p_recv_cmd->m_strName;
		p_offclient = pOfflineMgr->FindByName(p_recv_cmd->m_strName);
		p_onlineclient = pOnlineMgr->FindByName(acc_name);
	}
	else if(p_recv_cmd->m_byType == 2)
	{
		std::string acc_name = p_recv_cmd->m_strName;
		p_offclient = pOfflineMgr->FindByAccName(p_recv_cmd->m_strName);
		p_onlineclient = pOnlineMgr->FindByAccName(acc_name);
	}

	if(p_onlineclient || p_offclient)
	{
		if(p_offclient)
		{
			strncpy(p_change_cmd->m_strName, p_offclient->GetName(), sizeof(p_change_cmd->m_strName) - 1);
		}
		if(p_onlineclient)
		{
			strncpy(p_change_cmd->m_strName, p_onlineclient->GetRoleName(), sizeof(p_change_cmd->m_strName) - 1);
		}
		
		return CallGMSendMail_Handler(pObj, pt_null_cmd, cmd_size);
	}
	
	///  从来没有登录过或者根本不存在
	else
	{
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
		if( pDB )
		{
			char buf[Skt::MAX_USERDATASIZE];
			memset(buf, 0, sizeof(buf));
			MODI_S2RDB_Request_RoleDetailInfo * reqRoleInfo = (MODI_S2RDB_Request_RoleDetailInfo *)buf;
			AutoConstruct(reqRoleInfo);

			///  by name
			if(p_recv_cmd->m_byType == 1)
			{
				strncpy(reqRoleInfo->m_szName, p_recv_cmd->m_strName, sizeof(reqRoleInfo->m_szName) -1);
			}
			else if(p_recv_cmd->m_byType == 2)
			{
				strncpy(reqRoleInfo->m_szAccName, p_recv_cmd->m_strName, sizeof(reqRoleInfo->m_szAccName) -1);
			}
		
			/// 发送离线邮件
			reqRoleInfo->m_opt = kSendGMMails;
			reqRoleInfo->m_RelationType = p_recv_cmd->m_byType;
		
			/// 带上准备邮件的信息
			reqRoleInfo->m_nExtraSize = cmd_size;
			memcpy((char *)(&(reqRoleInfo->m_szExtra)), (const char *)(pt_null_cmd), cmd_size);
		
			pDB->SendCmd(reqRoleInfo , (sizeof(MODI_S2RDB_Request_RoleDetailInfo) + reqRoleInfo->m_nExtraSize));
		}
		else
		{
			MODI_ASSERT(0);
			MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
			send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
			send_cmd.m_byValue = 2;
			strncpy(send_cmd.m_strName, p_recv_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);
			if(pMc)
			{
				pMc->SendCmd(&send_cmd, sizeof(send_cmd));
			}
		}
	}
	return enPHandler_OK;
}

/// 已经确认有此用户或者找到用户的名字
int MODI_PackageHandler_zs2ms::CallGMSendMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if(!pMc )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
		
	const MODI_Unify2S_SendMail_Cmd * p_recv_cmd = (const MODI_Unify2S_SendMail_Cmd *)pt_null_cmd;
	MODI_S2ZS_Request_SendGiveItemMail msg;
	msg.m_nCount = 0;
	msg.m_shopmail = false;
	strncpy( msg.m_szSenderName  , SYS_MAIL_SENDER,  sizeof(msg.m_szSenderName) -1 );
	strncpy( msg.m_szReceverName , p_recv_cmd->m_strName, sizeof(msg.m_szReceverName) - 1);
	strncpy( msg.m_szContent ,  p_recv_cmd->m_szContents, sizeof(msg.m_szContent) - 1);
	strncpy( msg.m_szCaption ,  p_recv_cmd->m_szCaption, sizeof(msg.m_szCaption) -1);
	
	if(p_recv_cmd->m_szAttach[0])
	{
		/// 获取附件
		MODI_GoodsPackageArray goods;
		if(! PublicFun::StrConvToGoodsPackage(goods, p_recv_cmd->m_szAttach, strlen(p_recv_cmd->m_szAttach)))
		{
			MODI_ASSERT(0);
			return false;
		}

		if(goods.m_count > MAX_MAIL_ATTACH_NUM)
		{
			MODI_ASSERT(0);
			return false;
		}

		msg.m_nCount = goods.m_count;
			
		for(BYTE i=0; i<goods.m_count; i++)
		{
			MODI_S2ZS_Request_SendGiveItemMail::tagGoods * tmp = &msg.m_Goods[i];
			tmp->m_nCount = goods.m_array[i].nCount;
			tmp->m_nItemConfigID = goods.m_array[i].nGoodID;
			tmp->m_nLimitTime = goods.m_array[i].time;
		}
		MODI_PackageHandler_s2zs::SendAddItemMail_Handler(NULL, &msg, sizeof(msg));
	}
	else
	{
		MODI_SendMailInfo  sendmail;
		sendmail.Reset();
		strncpy( sendmail.m_szCaption , p_recv_cmd->m_szCaption , sizeof(sendmail.m_szCaption) - 1);
		strncpy( sendmail.m_szContents , p_recv_cmd->m_szContents , sizeof(sendmail.m_szContents) - 1);
		strncpy( sendmail.m_szRecevier , p_recv_cmd->m_strName, sizeof(sendmail.m_szRecevier) - 1);
		sendmail.m_Type = kTextMail;
		MODI_MailCore::SendMail( sendmail );
	}
	
	MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
	send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
	strncpy(send_cmd.m_strName, p_recv_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);
	if(pMc)
	{
		pMc->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	return enPHandler_OK;
}
