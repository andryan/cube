#include "ZoneService.h"
#include "Global.h"
#include "ServerConfig.h"
#include "AssertEx.h"
#include "GameSvrClientTask.h"
#include "DBProxyClientTask.h"
#include "Channellist.h"
#include "OnlineClientMgr.h"

MODI_ZoneService::MODI_ZoneService(std::vector<WORD > &  vec, const std::vector<std::string> & vecip , const char * name , const int count): 
	MODI_MNetService(vec, vecip , name, count)
{

}

/// 创建新的连接
bool MODI_ZoneService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	if(sock == -1 || addr == NULL)
	{
		return false;
	}
	
	if( this->m_PortVec.size() !=  ZSSVR_PORTIDX_COUNT )
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if( this->m_PortVec[ ZSSVR_PORTIDX_SVRS ] == port )
	{
		Global::logger->info("[%s] add a gameserver task(%s,%d)", 
				SVR_TEST,
				inet_ntoa(addr->sin_addr), 
				ntohs(addr->sin_port));

		MODI_GameSvrClientTask * pTask = new MODI_GameSvrClientTask( sock , addr );
		MODI_GameSvrClientTask::ms_ProcessBase.OnAcceptConnection( pTask );
		m_pTaskSched[ZSSVR_PORTIDX_SVRS].AddNormalSched(pTask);
		return true;
	}
	else if( m_PortVec[ ZSSVR_PORTIDX_DB ] == port )
	{
		Global::logger->info("[%s] add a dbproxy task(%s,%d)", 
				SVR_TEST,
				inet_ntoa(addr->sin_addr), 
				ntohs(addr->sin_port));

		MODI_DBProxyClientTask * pTask = new MODI_DBProxyClientTask( sock ,addr );
		MODI_DBProxyClientTask::ms_ProcessBase.OnAcceptConnection( pTask );
		m_pTaskSched[ZSSVR_PORTIDX_DB].AddNormalSched(pTask);
		return true;
	}

	TEMP_FAILURE_RETRY(::close(sock));
	MODI_ASSERT(0);
	return false;
}

void MODI_ZoneService::Final()
{
	for( int i = 0 ; i < ZSSVR_PORTIDX_COUNT ; i++ )
	{
		m_pTaskSched[i].Final();
	}
}

bool MODI_ZoneService::Init()
{
	if( MODI_MNetService::Init() )
	{
		for( int i = 0 ; i < ZSSVR_PORTIDX_COUNT ; i++ )
		{
			m_pTaskSched[i].Init(); 
		}
			
		return true;
	}

	return false;
}


void MODI_ZoneService::ReloadConfig()
{
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(! plist)
	{
		Global::net_logger->fatal("[reload_config] reload config failed <%s>", GetName());
		return;
	}

	if(!plist->Init("./Config/config.xml"))
	{
		Global::net_logger->fatal("[reload_config] reload config failed <%s>", GetName());
		return;
	}

	Global::net_logger->info("[reload_config] reload config successful <%s>", GetName());
}



void MODI_ZoneService::DealSignalUsr2()
{
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	pMgr->KickAllClients(200);

	Global::logger->info("[kick_allclient] kick all clients successful");
}


