/** 
 * @file ManangerClient.h
 * @brief 连接ms的客户端模型
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MANANGER_CLIENT_H_
#define MANANGER_CLIENT_H_

#include "ClientTask.h"
#include "SingleObject.h"
#include "CommandQueue.h"
#include "RecurisveMutex.h"

class MODI_ManangerSvrClient : public MODI_ClientTask  , 
	public CSingleObject<MODI_ManangerSvrClient> ,
	public MODI_CmdParse
{
public:

	MODI_ManangerSvrClient(const char * name, const char * server_ip,const WORD & port);

	~MODI_ManangerSvrClient();

	/// 发送命令
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

	/// 初始化
	virtual	bool Init();

 protected:

	virtual void Final();

};


#endif
