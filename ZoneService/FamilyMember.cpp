/**
 * @file   FamilyMember.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Apr 25 15:14:42 2011
 * 
 * @brief  家族成员
 * 
 * 
 */

#include "FamilyMember.h"
#include "DBClient.h"
#include "DBStruct.h"
#include "OnlineClientMgr.h"
#include "OnlineClient.h"
#include "MailCore.h"
#include "protocol/c2gs_family.h"
#include "OfflineUserMgr.h"
#include "ZoneServerAPP.h"
#include "config.h"

MODI_FamilyMember::MODI_FamilyMember(const MODI_FamilyMemBaseInfo & base_info)
{

	m_stBaseInfo = base_info;
	//m_qdJoinTime = current_time;
	m_qdJoinTime = 0;

}

MODI_FamilyMember:: MODI_FamilyMember(const MODI_FamilyBaseInfo & base_info, MODI_OnlineClient * p_client)
{
	 m_stBaseInfo.m_dwFamilyID = base_info.m_dwFamilyID;
    if(p_client)
	 {
		 m_stBaseInfo.m_stGuid = p_client->GetGUID();
		 strncpy(m_stBaseInfo.m_cstrRoleName, p_client->GetRoleName(), sizeof(m_stBaseInfo.m_cstrRoleName) -1);
		 m_stBaseInfo.m_dwAccountID = p_client->GetAccid();
		 m_stBaseInfo.m_bySex = p_client->GetSexType();
		 m_stBaseInfo.m_enState = enMemStateT_Free;
		 m_stBaseInfo.m_dwRenqi = p_client->GetFullData().m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi;
		 m_stBaseInfo.m_wdChenghao = p_client->GetChenghao();
			 m_stBaseInfo.m_wLevel = base_info.m_wdLevel;
		 
      	 //m_stBaseInfo.m_qdLastlogin = p_cliet->GetLogintime();
		 //m_qdJoinTime = current_time;
		 m_qdJoinTime = 0;
	 }
	 else
	 {
		 m_stBaseInfo.m_dwFamilyID = INVAILD_FAMILY_ID;
	 }
}


/** 
 * @brief 保存到家族成员表中
 * 
 */
void MODI_FamilyMember::SaveToDB()
{
	MODI_Record record_insert;
	record_insert.Put("accountid", m_stBaseInfo.m_dwAccountID);
	record_insert.Put("familyid", m_stBaseInfo.m_dwFamilyID);
	record_insert.Put("position", (BYTE)m_stBaseInfo.m_enPosition);
	record_insert.Put("contribute", m_stBaseInfo.m_dwContribute);
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);

	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeInsertSql(result, MODI_ZoneServerAPP::m_pFamilyMemTbl, &record_container))
	{
		Global::logger->fatal("[add_member] insert to family member failed <familyid=%u,mem=%s>",
							  GetFamilyID(), GetRoleName());
		return ;
	}

	MODI_ZoneServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 保存到家族成员表中
 * 
 */
void MODI_FamilyMember::UpDateToDB()
{
	std::ostringstream where;
	where << "accountid=" << GetAccid();
	
	MODI_Record record_update;
	record_update.Put("accountid", m_stBaseInfo.m_dwAccountID);
	record_update.Put("familyid", m_stBaseInfo.m_dwFamilyID);
	record_update.Put("position", (BYTE)m_stBaseInfo.m_enPosition);
	record_update.Put("contribute", m_stBaseInfo.m_dwContribute);
	record_update.Put("where", where.str());

	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeUpdateSql(result, MODI_ZoneServerAPP::m_pFamilyMemTbl, &record_update))
	{
		Global::logger->fatal("[update_family] update family member failed <familyid=%u,name=%s>",
							  GetFamilyID(), GetRoleName());
		return;
	}

	MODI_ZoneServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 从数据库中删除成员
 * 
 */
void MODI_FamilyMember::DelFromDB()
{
	std::ostringstream os;
	os<< "delete from family_members where accountid=" << m_stBaseInfo.m_dwAccountID << ";";
	MODI_ZoneServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());
}


/** 
 * @brief 把自己加入到家族
 * 
 */
bool MODI_FamilyMember::AddMeToFamily()
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(GetFamilyID());
	if(! p_family)
	{
		Global::logger->debug("[addme_tofamily] not find family <familyid=%,guid=%u>", GetFamilyID(), (QWORD)GetGuid());
		MODI_ASSERT(0);
		return false;
	}
	
	p_family->AddFamilyMember(this);
	SetFamilyName(p_family->GetFamilyName());
	if(IsMaster())
	{
		p_family->SetMaster(this);
	}
	return true;
 }


/** 
 * @brief 把我的信息发给所有家族成员
 * 
 */
void MODI_FamilyMember::SendMeToFamily(MODI_OnlineClient * p_ex_client)
{
	MODI_GS2C_Notify_UpdateFamilyMem send_cmd;
	send_cmd.m_stInfo.m_dwFamilyID = m_stBaseInfo.m_dwFamilyID;
	send_cmd.m_stInfo.m_stGuid = m_stBaseInfo.m_stGuid;		
	strncpy(send_cmd.m_stInfo.m_szName, m_stBaseInfo.m_cstrRoleName, sizeof(send_cmd.m_stInfo.m_szName) - 1);
	send_cmd.m_stInfo.m_bySex = m_stBaseInfo.m_bySex;
	send_cmd.m_stInfo.m_enPosition = m_stBaseInfo.m_enPosition;
	send_cmd.m_stInfo.m_enState = m_stBaseInfo.m_enState;
	send_cmd.m_stInfo.m_dwContribute = m_stBaseInfo.m_dwContribute;
	send_cmd.m_stInfo.m_dwRenqi = m_stBaseInfo.m_dwRenqi;
	send_cmd.m_stInfo.m_wdChenghao = m_stBaseInfo.m_wdChenghao;
    send_cmd.m_stInfo.m_stLastlogin = m_stBaseInfo.m_qdLastlogin;

	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(m_stBaseInfo.m_dwFamilyID);
	if(p_family)
	{
		p_family->Broadcast(&send_cmd, sizeof(send_cmd), p_ex_client);
	}
	
#ifdef _FAMILY_DEBUG	
	else
	{
		MODI_ASSERT(0);
	}
#endif
	
}


/** 
 * @brief 把家族信息发给我
 * 
 */
void MODI_FamilyMember::SendFamilyToMe(enSendFullInfoReason reason)
{
	char buf[Skt::MAX_USERDATASIZE];
	
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(m_stBaseInfo.m_dwFamilyID);
 	if(! p_family)
 	{
 		MODI_ASSERT(0);
 		return;
 	}

	DWORD send_size = p_family->GetFamilyFullData(buf, reason);
 
	MODI_CHARID charid = GUID_LOPART(GetGuid());
	if( charid == INVAILD_CHARID )
	{
		MODI_ASSERT(0);
		return;
	}
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pTargetClient = pMgr->FindByGUID(charid);
	if(pTargetClient)
	{
		pTargetClient->SendPackage(buf, send_size);
	}
}



/** 
 * @brief 创建家族的成员
 * 
 * 
 */
bool MODI_FamilyMember::OnCreate()
{
	SetMaster();
	if(! AddMeToFamily())
	{
		Global::logger->error("[create_family] add me to new family faild <familyid=%u,name=%u>",
							  GetFamilyID(), GetRoleName());
		MODI_ASSERT(0);
		return false;
	}
	SaveToDB();
	return true;
}


/** 
 * @brief 请求进入家族
 * 
 */
bool MODI_FamilyMember::OnRequest()
{
	SetRequest();
	if(! AddMeToFamily())
	{
		Global::logger->error("[join_family] add me to family faild <familyid=%u,name=%u>",
							  GetFamilyID(), GetRoleName());

		MODI_ASSERT(0);
		return false;
	}
	
	SaveToDB();
	return true;
}


/** 
 * @brief 被批准了
 * 
 */
bool MODI_FamilyMember::OnAccept()
{
	SetNormal();
	UpDateToDB();
	return true;
}


/** 
 * @brief 请求离开家族
 * 
 */
bool MODI_FamilyMember::OnLeave()
{
	DelFromDB();
	SetLeave();
	return true;
}


/** 
 * @brief 发送离线邮件
 * 
 * @param text 
 */
void MODI_FamilyMember::SendMailToMe(const char * text)
{
	const char * szTestCaption1 = CONFIG_FAMILY_INFO;
	
	MODI_SendMailInfo  sendmail;
	sendmail.Reset();
	strncpy( sendmail.m_szCaption , szTestCaption1 , sizeof(sendmail.m_szCaption) - 1);
	strncpy( sendmail.m_szContents , text , sizeof(sendmail.m_szContents) - 1);
	strncpy( sendmail.m_szRecevier , GetRoleName(), sizeof(sendmail.m_szRecevier) - 1);
	sendmail.m_Type = kTextMail;
	
	MODI_MailCore::SendMail(sendmail);
}


/** 
 * @brief 设置人气
 * 
 */
void MODI_FamilyMember::SetRenqi(MODI_OnlineClient * p_client)
{
	if(! p_client)
	{
		return;
	}
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(GetFamilyID());
	if(! p_family)
	{
		return;
	}
	
	const DWORD new_renqi = p_client->GetRenqi();
	m_stBaseInfo.m_dwRenqi = new_renqi;
	p_family->SendFamilyToAll(enReason_ModiBase);
}


/** 
 * @brief 超链接名字
 * 
 */
const char * MODI_FamilyMember::GetRoleLinkName()
{
	std::string name = GetRoleName();
	std::string link_name = "{@" + name + "}";
	return link_name.c_str();
}


/** 
 * @brief 登出
 * 
 */
void MODI_FamilyMember::Logout()
{
	m_stBaseInfo.m_enState = enMemStateT_Offline;
	/// 初始化离线名片
	MODI_OfflineUserMgr * pMgr2 = MODI_OfflineUserMgr::GetInstancePtr();
	if( pMgr2 )
	{
		MODI_OfflineUser * p_client = pMgr2->FindByName(GetRoleName());
		if(! p_client)
		{
			MODI_ASSERT(0);
			return;
		}
		MODI_FamilyCard card;
		card.m_stGuid = GetGuid();
		card.m_stPosition = GetPosition();
		memset(card.m_cstrFamilyName, 0 ,sizeof(card.m_cstrFamilyName));
		strncpy(card.m_cstrFamilyName, GetFamilyName(), sizeof(card.m_cstrFamilyName) - 1);
		p_client->InitFamilyCard(card);
	}
}
