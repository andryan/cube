/**
 * @file   ZoneUserVar.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jun  7 10:25:08 2011
 * 
 * @brief  zoneservice用户变量
 * 
 */

#include "ZoneUserVar.h"
#include "ZoneServerAPP.h"
#include "OnlineClient.h"
#include "UserVarDefine.h"

/** 
 * @brief onlineclient用户变量
 * 
 * @param sql 要执行的sql
 * @param sql_size sql的大小
 *
 */
void MODI_ZoneUserVar::ExecSqlToDB(const char * sql, const WORD sql_size)
{
	if(!sql || sql_size == 0)
		return;
	MODI_ZoneServerAPP::ExecSqlToDB(sql, sql_size);
}



/** 
 * @brief 更新
 * 
 */
void MODI_ZoneUserVar::UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(!pMgr)
	{
		return;
	}
	
	MODI_OnlineClient * pClient = pMgr->FindByAccid(GetAccountID());
	if(!pClient)
	{
		return;
	}
	
	if(m_stVarData.m_byType == (BYTE)MODI_VarState::enTriggerType)
	{
		if(cur_rtime.GetSec() > m_stVarData.m_dwUpdateTime)
		{
			/// do something
			/// 称号下降了15天
			if((strcmp(GetName(), CHENGHAO_XIAJIAN_KEEPTIME.c_str()) == 0) && m_stVarData.m_wdValue > 0)
			{
				pClient->ChangeChenghao(1);
			}
			
			m_stVarData.m_dwUpdateTime = cur_rtime.GetSec() + m_stVarData.m_dwAddTime;
			
#ifdef _VAR_DEBUG
			Global::logger->debug("[set_var] reset var <name=%s>", GetName());
#endif			
			m_stVarData.m_wdValue = 0;
			UpDateToDB();
		}
	}
	
	MODI_UserVar<MODI_OnlineClient>::UpDate(cur_rtime, cur_ttime);
}

