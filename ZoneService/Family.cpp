/**
 * @file   Family.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Apr 23 17:31:48 2011
 * 
 * @brief 家族管理
 * 
 * 
 */

#include "DBStruct.h"
#include "DBClient.h"
#include "Family.h"
#include "OnlineClientMgr.h"
#include "OnlineClient.h"
#include "protocol/c2gs_family.h"
#include "ZoneServerAPP.h"
#include "MailCore.h"
#include "config.h"
#include "BillClient.h"

#define MAX_PACKETSIZE 	1024
MODI_Family::MODI_Family(const MODI_FamilyBaseInfo & base_info)
{
  m_stBaseInfo = base_info;
  m_stMasterID = INVAILD_GUID;
}

/** 
 * @brief 创建的时候
 * 
 */
MODI_Family::MODI_Family(const MODI_FamilyBaseInfo & base_info, MODI_OnlineClient * p_client)
{
  m_stBaseInfo = base_info;
  if(p_client)
  {
  	m_stMasterID = p_client->GetGUID();
	m_strMasterName = p_client->GetRoleName();
  }
  
#ifdef _FAMILY_DEBUG  
  else
   {
     MODI_ASSERT(0);
   }
#endif  
}


/** 
 * @brief 保存到数据库
 * 
 */
void MODI_Family::SaveToDB()
{
	
	MODI_Record record_insert;
	record_insert.Put("familyid", m_stBaseInfo.m_dwFamilyID);
	record_insert.Put("familyname", m_stBaseInfo.m_cstrFamilyName);
	record_insert.Put("level", (BYTE)m_stBaseInfo.m_wdLevel);
	record_insert.Put("totem", m_stBaseInfo.m_byTotem);
	record_insert.Put("money", m_stBaseInfo.m_dwMoney);
	record_insert.Put("renqi", m_stBaseInfo.m_dwRenqi);
	record_insert.Put("xuanyan", m_stBaseInfo.m_cstrXuanyan);
	record_insert.Put("publics", m_stBaseInfo.m_cstrPublic);
	
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);
	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeInsertSql(result, MODI_ZoneServerAPP::m_pFamilyTbl, &record_container))
	{
		Global::logger->debug("[save_family_todb] make save family sql famild <%u>", m_stBaseInfo.m_dwFamilyID);
		return;
	}
	
	MODI_ZoneServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 更新到数据库
 * 
 */
void MODI_Family::UpDateToDB()
{
	MODI_Record record_update;
	std::ostringstream where;
	where << "familyid=" << GetFamilyID();
	record_update.Put("familyname", m_stBaseInfo.m_cstrFamilyName);
	record_update.Put("level", (BYTE)m_stBaseInfo.m_wdLevel);
	record_update.Put("totem", m_stBaseInfo.m_byTotem);
	record_update.Put("money", m_stBaseInfo.m_dwMoney);
	record_update.Put("renqi", m_stBaseInfo.m_dwRenqi);
	record_update.Put("xuanyan", m_stBaseInfo.m_cstrXuanyan);
	record_update.Put("publics", m_stBaseInfo.m_cstrPublic);
	record_update.Put("where", where.str());

	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeUpdateSql(result, MODI_ZoneServerAPP::m_pFamilyTbl, &record_update))
	{
		Global::logger->fatal("[update_family] updatefamily failed <familyid=%u,name=%s>",
							  GetFamilyID(), GetFamilyName());
		return;
	}

	MODI_ZoneServerAPP::ExecSqlToDB((const char *)result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 有家族创建
 * 
 * @param p_client 创建家族的人
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_Family::OnCreate(MODI_OnlineClient * p_client)
{
	MODI_FamilyMember * p_member = new MODI_FamilyMember(m_stBaseInfo, p_client);
	if(! p_member->OnCreate())
	{
		MODI_ASSERT(0);
		return false;
	}
	
	SaveToDB();

	p_client->UpDateFamilyInfo(p_member, enCreateFamily_Reason);

	/// 把家族完整信息发给自己
	p_member->SendFamilyToMe(enReason_CreateFamily);
	return true;
}


/** 
 * @brief 某人请求加入家族
 * 
 */
enFamilyOptResult MODI_Family::OnRequest(MODI_OnlineClient * p_client)
{
	if(IsFull())
	{
#ifdef _FAMILY_DEBUG
		Global::logger->debug("[request_family] request family number full <name=%s,familyid=%u>",
							  p_client->GetRoleName(), GetFamilyID());
#endif
		return enFamilyRetT_Request_Num;
	}

	MODI_FamilyMember * p_member = new MODI_FamilyMember(m_stBaseInfo, p_client);
	if(! p_member->OnRequest())
	{
		delete p_member;
		p_member = NULL;
		Global::logger->fatal("[request_family] request family failed <familyid=%u,name=%s>", GetFamilyID(), p_client->GetRoleName());
		return enFamilyOptResult_Unknow;
	}

	/// 更新自己的信息
	UpDateFamilyCard(p_member, enRequest_Reason);
	
	/// 把家族信息发给自己
	p_member->SendFamilyToMe(enReason_RequestFamily);
	
	/// 添加自己到家族所有的成员
	p_member->SendMeToFamily();
	
	char tmp[MAX_PACKETSIZE];
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,CONFIG_REQUEST_FAMILY,p_member->GetRoleLinkName());
	SendSysChatToMaster(tmp, strlen(tmp));
	SendSysChatToSlaver(tmp, strlen(tmp));
	
	return enFamilyRequestRetT_Ok;
}


/** 
 * @brief 某人请求取消某家族申请
 * 
 */
bool MODI_Family::OnCancel(MODI_OnlineClient * p_client)
{
	MODI_FamilyMember * p_member = FindMember(p_client->GetGUID());
	if(! p_member)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(! p_member->OnLeave())
	{
		Global::logger->fatal("[request_family] request family failed <familyid=%u,name=%s>", GetFamilyID(), p_client->GetRoleName());
		return false;
	}

	/// 更新此人的家族名片
	UpDateFamilyCard(p_member, enLeaveFamily_Reason);

	/// 广播家族成员某人退出家族
	NotifyDelMember(p_member, enDMReason_Cancel);
	RemoveFamilyMember(p_member);
	char tmp[MAX_PACKETSIZE];
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,CONFIG_CANCEL_REQUEST,p_member->GetRoleLinkName() );
	SendSysChatToMaster(tmp, strlen(tmp));
	SendSysChatToSlaver(tmp, strlen(tmp));
	
	return true;
}


/** 
 * @brief 某人请求取消某家族申请
 * 
 */
enFamilyOptResult MODI_Family::OnLeave(MODI_OnlineClient * p_client)
{
	MODI_FamilyMember * p_member = FindMember(p_client->GetGUID());
	if(! p_member)
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	/// 族长离开
	if(p_member->IsMaster())
	{
		if(NormalMemNum() > 1)
		{
			MODI_FamilyMember * p_max_member = NULL;
			if(SlaverMemNum() > 0)
			{
				/// 找一个贡献度最高的人
				p_max_member = GetMaxContributeInSlaver(p_member);
				if(p_max_member)
				{
					ChangeMaster(p_max_member);
				}
				else
				{
					MODI_ASSERT(0);
					return enFamilyOptResult_Unknow;
				}
			}
			else
			{
				/// 找一个贡献度最高的人
				p_max_member = GetMaxContribute(p_member);
				if(p_max_member)
				{
					ChangeMaster(p_max_member);
				}
				else
				{
					MODI_ASSERT(0);
					return enFamilyOptResult_Unknow;
				}
			}
			if(! p_member->OnLeave())
			{
				Global::logger->fatal("[leave_family] leave family failed <familyid=%u,name=%s>", GetFamilyID(), p_member->GetRoleName());
				return enFamilyOptResult_Unknow;
			}

			char tmp[MAX_PACKETSIZE];
			memset(tmp,'\0',sizeof(tmp));
			sprintf(tmp,CONFIG_LEAVE_FAMILY,p_member->GetRoleLinkName());
			BroadcastSysChatExRequest(tmp, strlen(tmp));

			/// 更新二个人的家族名片
			UpDateFamilyCard(p_member, enLeaveFamily_Reason);
			UpDateFamilyCard(p_max_member, enNone_Reason);
			
			/// 告知大家有人离开
			NotifyDelMember(p_member, enDMReason_Leave);
			RemoveFamilyMember(p_member);
			
			/// 告知人数和人气变更了
			SendFamilyToAll(enReason_ModiBase);
			return enFamilyRetT_Level_Succ;
		}
		else if(NormalMemNum() == 1)
		{
			/// 解散吧
			return MODI_FamilyManager::GetInstance().FreeFamily(p_client);
		}
		else
		{
			MODI_ASSERT(0);
			return enFamilyOptResult_Unknow;
		}
	}
	
	/// 普通成员离开
	else
	{
		if(! p_member->OnLeave())
		{
			Global::logger->fatal("[leave_family] leave family failed <familyid=%u,name=%s>", GetFamilyID(), p_member->GetRoleName());
			return enFamilyOptResult_Unknow;
		}

		UpDateFamilyCard(p_member, enLeaveFamily_Reason);

		/// 告知大家有人离开
		NotifyDelMember(p_member, enDMReason_Leave);
		RemoveFamilyMember(p_member);
		/// 告知人数好人气变更了
		SendFamilyToAll(enReason_ModiBase);
		char tmp[MAX_PACKETSIZE];
		memset(tmp,'\0',sizeof(tmp));
		sprintf(tmp,CONFIG_LEAVE_FAMILY,p_member->GetRoleLinkName());
		BroadcastSysChatExRequest(tmp, strlen(tmp));
		
		return enFamilyRetT_Level_Succ;
	}
	return enFamilyRetT_Level_Succ;
}


/** 
 * @brief 族长拒绝某人加入
 * 
 */
bool MODI_Family::OnRefuse(const MODI_GUID & refuse_id)
{
	MODI_FamilyMember * p_member = FindMember(refuse_id);
	if(! p_member)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(! p_member->OnLeave())
	{
		Global::logger->fatal("[request_family] request family failed <familyid=%u,name=%s>", GetFamilyID(), p_member->GetRoleName());
		return false;
	}
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_CHARID charid = GUID_LOPART(refuse_id);
	MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
	if(p_client)
	{
		p_client->UpDateFamilyInfo(p_member, enRefuseFamily_Reason);
		/// 告知他被拒绝
		p_client->RetFamilyResult(enFamilyRetT_Request_Refuse);
	}
	else
	{
		char tmp[MAX_PACKETSIZE];
		memset(tmp,'\0',sizeof(tmp));
		sprintf(tmp,CONFIG_REFUSE_FIRST,GetFamilyName());
		p_member->SendMailToMe(tmp);
	}
	
	NotifyDelMember(p_member, enDMReason_Refuse);
	RemoveFamilyMember(p_member);
	
	return true;
}


/** 
 * @brief 接受某个成员加入
 * 
 * @param accept_id 接收的成员
 * 
 * @return 
 */
bool MODI_Family::OnAccept(const MODI_GUID & accept_id)
{
	MODI_FamilyMember * p_member = FindMember(accept_id);
	if(! p_member)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(! p_member->OnAccept())
	{
		Global::logger->fatal("[request_family] request family failed <familyid=%u,name=%s>", GetFamilyID(), p_member->GetRoleName());
		return false;
	}

	/// 更新家族名片
	UpDateFamilyCard(p_member, enAccept_Reason);

	char tmp[MAX_PACKETSIZE];
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,CONFIG_JOINFAMILY,p_member->GetRoleLinkName());
	BroadcastSysChatExRequest(tmp, strlen(tmp));
	
	p_member->SendMeToFamily();
	p_member->SendFamilyToMe(enReason_JoinFamily);
	/// 基本信息变更 
	SendFamilyToAll(enReason_ModiBase);
	
	return true;
}


/** 
 * @brief 开除某人出家族
 * 
 */
bool MODI_Family::OnFire(const MODI_GUID & fire_id)
{
	MODI_FamilyMember * p_member = FindMember(fire_id);
	if(! p_member)
	{
		MODI_ASSERT(0);
		return false;
	}

	char tmp[MAX_PACKETSIZE];
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,CONFIG_BE_FIRED,p_member->GetRoleLinkName());
	BroadcastSysChatExRequest(tmp, strlen(tmp));
	
	if(! p_member->OnLeave())
	{
		Global::logger->fatal("[request_family] request family failed <familyid=%u,name=%s>", GetFamilyID(), p_member->GetRoleName());
		return false;
	}

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_CHARID charid = GUID_LOPART(fire_id);
	MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
	if(p_client)
	{		
		p_client->UpDateFamilyInfo(p_member, enLeaveFamily_Reason);
		/// 告知被开除了
		p_client->RetFamilyResult(enFamilyRetT_OnFire);
	}
	else
	{
		std::string os =  CONFIG_YOU_ARE_FIRED;
		p_member->SendMailToMe(os.c_str());
	}
	
	NotifyDelMember(p_member, enDMReason_Fire);
	RemoveFamilyMember(p_member);
	SendFamilyToAll(enReason_ModiBase);
	return true;
}


/** 
 * @brief 发送系统公告到副族长
 * 
 * @param chat_content 公告内容
 * @param chat_size 公告大小
 *
 */
void MODI_Family::SendSysChatToSlaver(const char * chat_content, const DWORD chat_size)
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_OnlineClient * p_client = NULL;
	MODI_FamilyMember * p_member = NULL;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			if(! p_member->IsSlaver())
			{
				continue;
			}
			else
			{
				MODI_CHARID charid = GUID_LOPART(iter->first);
				if( charid == INVAILD_CHARID )
				{
					MODI_ASSERT(0);
					continue;
				}
	
				MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
				p_client = pMgr->FindByGUID(charid);
		
				if(p_client)
				{
					p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
				}
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
}


/** 
 * @brief 发送公告到族长
 * 
 * @param chat_content 发送内容
 * @param chat_size 内容大小
 *
 */
void MODI_Family::SendSysChatToMaster(const char * chat_content, const DWORD chat_size)
{
	
	MODI_CHARID charid = GUID_LOPART(m_stMasterID);
	if( charid == INVAILD_CHARID )
	{
		MODI_ASSERT(0);
		return;
	}
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
		
	if(p_client)
	{
		p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
	}
}


/** 
 * @brief 增加家族成员
 * 
 * @param p_member 
 * 
 * @return 
 */
bool MODI_Family::AddFamilyMember(MODI_FamilyMember * p_member)
{
	std::pair<defFamilyMemMapIter, bool > ret_code;
	ret_code = m_stMemberMap.insert(defFamilyMemMapValue(p_member->GetGuid(), p_member));
	if(! ret_code.second)
	{
		Global::logger->fatal("[add_member] add member failed <familyid=%u, mem_guid=%u>",
							  GetFamilyID(), (QWORD)p_member->GetGuid());
		MODI_ASSERT(0);
    }
	return ret_code.second;
}


/** 
 * @brief 删除某个成员
 * 
 * @param guid 
 */
void MODI_Family::RemoveFamilyMember(MODI_FamilyMember * p_member)
{
	if(! p_member)
	{
		return;
	}
	defFamilyMemMapIter iter = m_stMemberMap.find(p_member->GetGuid());
	if(iter != m_stMemberMap.end())
	{
		if(iter->second)
		{
			delete iter->second;
			iter->second = NULL;
		}
		m_stMemberMap.erase(iter);
	}
	else
	{
		MODI_ASSERT(0);
	}
}


/** 
 * @brief 是否是家族成员
 * 
 * @param guid 成员id
 * 
 */
bool MODI_Family::IsFamilyMember(const MODI_GUID & guid)
{
	bool ret_code = false;
	defFamilyMemMapIter iter = m_stMemberMap.find(guid);
	if(iter != m_stMemberMap.end())
	{
		ret_code = true;
	}
	return ret_code;
}


/** 
 * @brief 是否是家族成员
 * 
 * @param guid 成员id
 * 
 */
MODI_FamilyMember *  MODI_Family::FindFamilyMember(const char * name)
{
	MODI_FamilyMember * p_member = NULL;
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(!p_member)
			continue;
		
		if(strcmp(name, p_member->GetRoleName()) == 0)
		{
			return p_member;
		}
	}
	return p_member;
}


/** 
 * @brief 获取某个家族成员
 * 
 */
MODI_FamilyMember * MODI_Family::FindMember(const MODI_GUID & guid)
{
	MODI_FamilyMember * p_member = NULL;
	defFamilyMemMapIter iter = m_stMemberMap.find(guid);
	if(iter != m_stMemberMap.end())
	{
		p_member = iter->second;
	}
	return p_member;
}

/** 
 * @brief 家族系统广播
 * 
 */
void MODI_Family::BroadcastSysChat(const char * chat_content, const WORD chat_size, MODI_OnlineClient * p_ex_client)
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_OnlineClient * p_client = NULL;
	if(p_ex_client)
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{
			if(iter->first == p_ex_client->GetGUID())
			{
				continue;
			}

			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);
			
			if(p_client)
			{
				p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
			}
		}
	}
	else
	{
	   	for(; iter != m_stMemberMap.end(); iter++)
		{
			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);

			if(p_client)
			{
				p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
			}
		}
	}
}

/** 
 * @brief 家族广播
 * 
 */
void MODI_Family::Broadcast(const stNullCmd * pt_null_cmd, const unsigned int cmd_size, MODI_OnlineClient * p_ex_client)
{
	MODI_CHARID   character[2000];
	int n=0;
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_OnlineClient * p_client = NULL;
	if(p_ex_client)
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{

			if(iter->first == p_ex_client->GetGUID())
			{
				continue;
			}

			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);

			if(p_client)
			{
				character[n++] = p_client->GetCharID();
			}
		}
	}
	else
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{
			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
			
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);
			
			if(p_client)
			{
				character[n++] = p_client->GetCharID();
			}
		}
	}

	MODI_OnlineClientMgr::GetInstancePtr()->BroadcastRange(pt_null_cmd, cmd_size, character, n);
}



/** 
 * @brief 家族系统广播
 * 
 */
void MODI_Family::BroadcastSysChatExRequest(const char * chat_content, const WORD chat_size, MODI_OnlineClient * p_ex_client)
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_OnlineClient * p_client = NULL;
	if(p_ex_client)
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{
			if(iter->first == p_ex_client->GetGUID())
			{
				continue;
			}
			if(iter->second->IsRequest())
			{
				continue;
			}

			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);
			
			if(p_client)
			{
				p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
			}
		}
	}
	else
	{
	   	for(; iter != m_stMemberMap.end(); iter++)
		{
			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}

			if(iter->second->IsRequest())
			{
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);

			if(p_client)
			{
				p_client->SendSystemChat(chat_content, chat_size, enSystemNotice_Family);
			}
		}
	}
}

/** 
 * @brief 家族广播
 * 
 */
void MODI_Family::BroadcastExRequest(const stNullCmd * pt_null_cmd, const unsigned int cmd_size, MODI_OnlineClient * p_ex_client)
{
	MODI_CHARID character[2000];
	memset(character, 0, sizeof(character));
	int n=0;
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_OnlineClient * p_client = NULL;
	if(p_ex_client)
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{

			if(iter->first == p_ex_client->GetGUID())
			{
				continue;
			}

			if(iter->second->IsRequest())
			{
				continue;
			}

			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}
	
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);

			if(p_client)
			{
				character[n++] = p_client->GetCharID();
			}
		}
	}
	else
	{
		for(; iter != m_stMemberMap.end(); iter++)
		{
			MODI_CHARID charid = GUID_LOPART(iter->first);
			if( charid == INVAILD_CHARID )
			{
				MODI_ASSERT(0);
				continue;
			}

			if(iter->second->IsRequest())
			{
				continue;
			}
			
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			p_client = pMgr->FindByGUID(charid);
			
			if(p_client)
			{
				character[n++] = p_client->GetCharID();
			}
		}
	}
	MODI_OnlineClientMgr::GetInstancePtr()->BroadcastRange(pt_null_cmd, cmd_size,(MODI_CHARID *)character, n);
}



/** 
 * @brief 所有成员执行
 * 
 * @param call_back 执行体
 *
 */
void MODI_Family::AllMemberExec(MODI_FamilyMemberCallback & call_back)
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			call_back.Exec(p_member);
		}
	}
}


/** 
 * @brief 家族更新
 * 
 */
void MODI_Family::UpDate()
{
//   defFamilyMemMapIter iter = m_stMemberMap.begin();
//   MODI_FamilyMember * p_member = NULL;
//   for(; iter != m_stMemberMap.end(); iter++)
//   {
//     p_member = iter->second;
//     if(p_member)
//      {
//        p_member->UpDate();
//      }
//     else
//      {   
// 	Global::logger->error("[family_update]  family update not find member <familyid=%u,guid=%u>",
// 			      GetFamilyID(), (DWORD)iter->first);
// 	MODI_ASSERT(0);
//      }
//   }
  /// do other 
}


/** 
 * @brief 解散家族
 * 
 * @param p_master 族长 
 * 
 */
bool MODI_Family::OnFree(MODI_OnlineClient * p_master)
{
	if(NormalMemNum() > 1)
	{
		p_master->RetFamilyResult(enFamilyRetT_Free_Member);
		return false;
	}

	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			p_member->OnLeave();
			/// 通知删除此成员
			NotifyDelMember(p_member, enDMReason_Free);
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_CHARID charid = GUID_LOPART(p_member->GetGuid());
			MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
			if(p_client)
			{
				p_client->UpDateFamilyInfo(p_member, enLeaveFamily_Reason);
				/// 告知他家族解散成功
				if(p_client->GetGUID() == p_master->GetGUID())
				{
					p_client->RetFamilyResult(enFamilyFreeRetT_Ok);
				}
				else
				{
					p_client->RetFamilyResult(enFamilyFreeT_Request);
				}
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	DelFromDB();
	return true;
}


/** 
 * @brief 家族升级
 * 
 * @param p_master 族长 
 * 
 */
bool MODI_Family::OnLevelup(MODI_OnlineClient * p_master)
{
	if(! p_master)
	{
		return false;
	}
	
	MODI_S2RDB_FamilyLevel_Cmd send_cmd;
	send_cmd.m_dwFamilyID = GetFamilyID();
	send_cmd.m_byLevel = m_stBaseInfo.m_wdLevel;
	send_cmd.m_dwAccountID = p_master->GetAccountID();
	strncpy(send_cmd.m_cstrAccName, p_master->GetAccName(), sizeof(send_cmd.m_cstrAccName));

	if(Global::g_byGameShop > 0)
	{
		MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
		if( pBill && pBill->IsConnected())
		{
			pBill->SendCmd(&send_cmd, sizeof(send_cmd));
		}
	}
	else
	{
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
		if( !pDB )
		{
			Global::logger->info("[request_exec_sql] can't get db interface");
			return false;
		}
		pDB->SendCmd(&send_cmd, sizeof(send_cmd));
	}


	SendFamilyToAll(enReason_Levelup);

	
	return true;
	
};

	
	



/** 
 * @brief 家族升级
 * 
 */
bool MODI_Family::Levelup()
{
	if(m_stBaseInfo.m_wdLevel >= nsFamilyLevel::MAX_FAMILY_LEVEL)
	{
		MODI_ASSERT(0);
		return false;
	}
	m_stBaseInfo.m_wdLevel++;

	/// 告知所有家族成员家族升级了
	SendFamilyToAll(enReason_Levelup);

	MODI_FamilyMember * p_member;
	defFamilyMemMapIter  iter = m_stMemberMap.begin();
///	通知所有的家族人更新家族名片
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			p_member->SetLevel(m_stBaseInfo.m_wdLevel);
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_CHARID charid = GUID_LOPART(p_member->GetGuid());
			MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
			if(p_client)
			{
				p_client->UpDateFamilyInfo(p_member, enLogin_Reason);
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	/// 告知服务器
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(!pMgr)
	{
		return false;
	}
	
	std::ostringstream os;

	/// SGP
	if(Global::g_byGameShop == 2)
	{
		/// 这里固定写了，就10个等级省去配置
		if(m_stBaseInfo.m_wdLevel == 2)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP2;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT1;
					std::string caption = CONFIG_BROADCAST_FCAPTION1;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 3)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP3;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT2;
					std::string caption = CONFIG_BROADCAST_FCAPTION2;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 4)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP4;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT3;
					std::string caption = CONFIG_BROADCAST_FCAPTION3;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 5)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP5;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT4;
					std::string caption = CONFIG_BROADCAST_FCAPTION4;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 6)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP6;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT5;
					std::string caption = CONFIG_BROADCAST_FCAPTION5;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 7)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP7;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT6;
					std::string caption = CONFIG_BROADCAST_FCAPTION6;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 8)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP8;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);
			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT7;
					std::string caption = CONFIG_BROADCAST_FCAPTION7;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 9)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP9;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT8;
					std::string caption = CONFIG_BROADCAST_FCAPTION8;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 10)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP10;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT9;
					std::string caption = CONFIG_BROADCAST_FCAPTION9;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54059, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
	}
 	/// INA
 	else if(Global::g_byGameShop == 1)
 	{
		/// 这里固定写了，就10个等级省去配置
		if(m_stBaseInfo.m_wdLevel == 2)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP2;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT1;
					std::string caption = CONFIG_BROADCAST_FCAPTION1;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 1000 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT1;
					caption = CONFIG_BROADCAST_FCAPTION1;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(52001, 10, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 3)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP3;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT2;
					std::string caption = CONFIG_BROADCAST_FCAPTION2;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 1250 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT2;
					caption = CONFIG_BROADCAST_FCAPTION2;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53002, 10, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 4)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP4;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT3;
					std::string caption = CONFIG_BROADCAST_FCAPTION3;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 1500 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT3;
					caption = CONFIG_BROADCAST_FCAPTION3;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(52002, 10, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 5)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP5;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT4;
					std::string caption = CONFIG_BROADCAST_FCAPTION4;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 1750 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT4;
					caption = CONFIG_BROADCAST_FCAPTION4;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(52003, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(53003, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 2;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 6)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP6;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT5;
					std::string caption = CONFIG_BROADCAST_FCAPTION5;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 2000 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT5;
					caption = CONFIG_BROADCAST_FCAPTION5;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(50002, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(50003, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[2].SetAttachment_ItemInfo(50004, 5, enForever);
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 7)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP7;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT6;
					std::string caption = CONFIG_BROADCAST_FCAPTION6;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 2250 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT6;
					caption = CONFIG_BROADCAST_FCAPTION6;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(52004, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(53008, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 2;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 8)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP8;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);
			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT7;
					std::string caption = CONFIG_BROADCAST_FCAPTION7;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 2500 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					if(p_member->GetSexType() == 1)
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(14068, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}
					else
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(14075, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}

					content = CONFIG_BROADCAST_FCONTENT7;
					caption = CONFIG_BROADCAST_FCAPTION7;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 9)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP9;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT8;
					std::string caption = CONFIG_BROADCAST_FCAPTION8;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 2750 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					if(p_member->GetSexType() == 1)
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(28042, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}
					else
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(28053, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}

					content = CONFIG_BROADCAST_FCONTENT8;
					caption = CONFIG_BROADCAST_FCAPTION8;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 10)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP10;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT9;
					std::string caption = CONFIG_BROADCAST_FCAPTION9;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 3000 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

					if(p_member->GetSexType() == 1)
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(34018, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}
					else
					{
						send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(34020, 1, enForever);
						send_familylevel_mail.m_byExtraCount = 1;
					}

					content = CONFIG_BROADCAST_FCONTENT9;
					caption = CONFIG_BROADCAST_FCAPTION9;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);

				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
 	}
	/// 大陆版本
	else
	{
		/// 这里固定写了，就10个等级省去配置
		if(m_stBaseInfo.m_wdLevel == 2)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP2;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT1;
					std::string caption = CONFIG_BROADCAST_FCAPTION1;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 500 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 3)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP3;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT2;
					std::string caption = CONFIG_BROADCAST_FCAPTION2;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53002, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 4)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP4;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT3;
					std::string caption = CONFIG_BROADCAST_FCAPTION3;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(52002, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 5)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP5;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT4;
					std::string caption = CONFIG_BROADCAST_FCAPTION4;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(50001, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 6)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP6;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT5;
					std::string caption = CONFIG_BROADCAST_FCAPTION5;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53003, 5, enForever);
					send_familylevel_mail.m_byExtraCount = 2;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(52002, 5, enForever);
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 7)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP7;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT6;
					std::string caption = CONFIG_BROADCAST_FCAPTION6;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53002, 10, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(53001, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[2].SetAttachment_ItemInfo(50002, 1, enForever);
					send_familylevel_mail.m_byExtraCount = 3;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 8)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP8;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);
			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT7;
					std::string caption = CONFIG_BROADCAST_FCAPTION7;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 2000 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_MailCore::SendMail(send_familylevel_mail);


					send_familylevel_mail.Reset();
					content = "恭喜您，您所在的家族从Lv7升级至Lv8,这是给您的奖励，请确认，请继续打造家族美好的明天！";
					caption = "家族升级(Lv7-Lv8)奖励";
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53001, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(53007, 5, enForever);
					send_familylevel_mail.m_ExtraInfo[2].SetAttachment_ItemInfo(50002, 2, enForever);
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					send_familylevel_mail.m_byExtraCount = 3;
					MODI_MailCore::SendMail(send_familylevel_mail);
					
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 9)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP9;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT8;
					std::string caption = CONFIG_BROADCAST_FCAPTION8;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(55001, 3, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(50001, 10, enForever);
					send_familylevel_mail.m_ExtraInfo[2].SetAttachment_ItemInfo(51001, 1, enForever);
					send_familylevel_mail.m_ExtraInfo[3].SetAttachment_ItemInfo(50002, 3, enForever);
					send_familylevel_mail.m_byExtraCount = 4;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
		else if(m_stBaseInfo.m_wdLevel == 10)
		{
			/// 广播
			os<< CONFIG_CONGRATULATE << GetFamilyName() << CONFIG_BROADCAST_FLEVELUP10;
			pMgr->BroadcastSysChat(os.str().c_str(), os.str().size(), enSystemNotice_FamilyLevelup);

			struct send_level_mail: public MODI_FamilyMemberCallback
			{
				virtual ~send_level_mail(){}
				void Exec(MODI_FamilyMember * p_member)
				{
					/// 发奖品
					MODI_SendMailInfo  send_familylevel_mail;
					send_familylevel_mail.Reset();
					std::string content = CONFIG_BROADCAST_FCONTENT9;
					std::string caption = CONFIG_BROADCAST_FCAPTION9;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_Money( 5000 );
					send_familylevel_mail.m_byExtraCount = 1;
					send_familylevel_mail.m_Type = kAttachmentMoneyMail;
					MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
					MODI_CHARID charid = GUID_LOPART(p_member->GetGuid());
					MODI_OnlineClient * p_client = NULL;
					p_client = pMgr->FindByGUID(charid);
					MODI_MailCore::SendMail(send_familylevel_mail);

					content = CONFIG_BROADCAST_FCONTENT9;
					caption = CONFIG_BROADCAST_FCAPTION9;
					strncpy( send_familylevel_mail.m_szCaption, caption.c_str(), sizeof(send_familylevel_mail.m_szCaption) - 1);
					strncpy( send_familylevel_mail.m_szContents , content.c_str(), sizeof(send_familylevel_mail.m_szContents) - 1);
					strncpy( send_familylevel_mail.m_szRecevier , p_member->GetRoleName(), sizeof(send_familylevel_mail.m_szRecevier) - 1);
					send_familylevel_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(53002, 10, enForever);
					send_familylevel_mail.m_ExtraInfo[1].SetAttachment_ItemInfo(52002, 10, enForever);
					send_familylevel_mail.m_ExtraInfo[2].SetAttachment_ItemInfo(50002, 10, enForever);
					send_familylevel_mail.m_byExtraCount = 3;
					send_familylevel_mail.m_Type = kAttachmentItemMail;
					MODI_MailCore::SendMail(send_familylevel_mail);
				}
			};

			send_level_mail send_money;
			AllMemberExec(send_money);
			return true;
		}
	}	
	return false;
}




/** 
 * @brief 释放资源
 * 
 */
void MODI_Family::Final()
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			delete p_member;
			p_member = NULL;
		}
		else
		{
			Global::logger->error("[family_onfree]  family on free not find member <familyid=%u,guid=%u>",
								  GetFamilyID(), (QWORD)iter->first);
			MODI_ASSERT(0);
		}
	}
	m_stMemberMap.clear();
}



/** 
 * @brief 找贡献度最高的人
 * 
 * @param p_ex_member 此人除外
 * 
 */
MODI_FamilyMember * MODI_Family::GetMaxContribute(MODI_FamilyMember * p_ex_member)
{
	if(GetMemNum() == 0)
	{
		MODI_ASSERT(0);
	}
	
	if(p_ex_member)
	{
		MODI_FamilyMember * p_member = NULL;
		MODI_FamilyMember * max_member = NULL;
		DWORD max_contribute = 0;
		DWORD cur_contribute = 0;
		defFamilyMemMapIter iter = m_stMemberMap.begin();
		for(; iter != m_stMemberMap.end(); iter++)
		{
			p_member = iter->second;
			if(p_member->GetGuid() == p_ex_member->GetGuid())
			{
				continue;
			}

			if(p_member)
			{
				cur_contribute = p_member->GetContribute();
				if(cur_contribute >= max_contribute)
				{
					max_contribute = cur_contribute;
					max_member = p_member;
				}
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
		return max_member;
	}
	
	else
	{
		MODI_FamilyMember * p_member = NULL;
		MODI_FamilyMember * max_member = NULL;
		DWORD max_contribute = 0;
		DWORD cur_contribute = 0;
		defFamilyMemMapIter iter = m_stMemberMap.begin();
		for(; iter != m_stMemberMap.end(); iter++)
		{
			p_member = iter->second;
			cur_contribute = p_member->GetContribute();
			if(cur_contribute > max_contribute)
			{
				max_contribute = cur_contribute;
				max_member = p_member;
			}
		}
		return max_member;
	}
}



/** 
 * @brief 找贡献度最高的人
 * 
 * @param p_ex_member 此人除外
 * 
 */
MODI_FamilyMember * MODI_Family::GetMaxContributeInSlaver(MODI_FamilyMember * p_ex_member)
{
	if(GetMemNum() == 0)
	{
		MODI_ASSERT(0);
	}
	
	if(p_ex_member)
	{
		MODI_FamilyMember * p_member = NULL;
		MODI_FamilyMember * max_member = NULL;
		DWORD max_contribute = 0;
		DWORD cur_contribute = 0;
		defFamilyMemMapIter iter = m_stMemberMap.begin();
		for(; iter != m_stMemberMap.end(); iter++)
		{
			p_member = iter->second;
			if(p_member->GetGuid() == p_ex_member->GetGuid())
			{
				continue;
			}
			if(!p_member->IsSlaver())
			{
				continue;
			}

			if(p_member)
			{
				cur_contribute = p_member->GetContribute();
				if(cur_contribute >= max_contribute)
				{
					max_contribute = cur_contribute;
					max_member = p_member;
				}
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
		return max_member;
	}
	
	else
	{
		MODI_FamilyMember * p_member = NULL;
		MODI_FamilyMember * max_member = NULL;
		DWORD max_contribute = 0;
		DWORD cur_contribute = 0;
		defFamilyMemMapIter iter = m_stMemberMap.begin();
		for(; iter != m_stMemberMap.end(); iter++)
		{
			p_member = iter->second;
			cur_contribute = p_member->GetContribute();
			if(cur_contribute > max_contribute)
			{
				max_contribute = cur_contribute;
				max_member = p_member;
			}
		}
		return max_member;
	}
}



/** 
 * @brief 更换族长
 * 
 * @param p_member 新族长
 *
 */
void  MODI_Family::ChangeMaster(MODI_FamilyMember * p_member)
{
	p_member->SetMaster();
	p_member->UpDateToDB();
	SetMaster(p_member);
	
	/// 更新新家族族长
	p_member->SendMeToFamily();
	SendFamilyToAll(enReason_ChangeMaster);

	std::ostringstream os;
	os<< "Congratulations:" << p_member->GetRoleLinkName()<< "to be the new head master";
	BroadcastSysChatExRequest(os.str().c_str(), os.str().size());

	os.str("");
	os<< "Congratulations to be " << GetFamilyName() << "head master!";
	p_member->SendMailToMe(os.str().c_str());
}


/** 
 * @brief 从数据库中删除家族
 * 
 */
void MODI_Family::DelFromDB()
{
	std::ostringstream os;
	os<< "delete from familys where familyid=" << GetFamilyID() << ";";
	MODI_ZoneServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());
}


/** 
 * @brief 正式成员的数量
 * 
 * 
 */
const WORD MODI_Family::NormalMemNum()
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	WORD normal_size = 0;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			if(p_member->IsNormal())
			{
				normal_size++;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return normal_size;
}


/** 
 * @brief 副族长成员的数量
 * 
 * 
 */
const WORD MODI_Family::SlaverMemNum()
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	WORD normal_size = 0;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			if(p_member->IsSlaver())
			{
				normal_size++;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return normal_size;
}


/** 
 * @brief 请求成员的数量
 * 
 * 
 */
const WORD MODI_Family::RequestMemNum()
{
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	MODI_FamilyMember * p_member = NULL;
	WORD normal_size = 0;
	for(; iter != m_stMemberMap.end(); iter++)
	{
		p_member = iter->second;
		if(p_member)
		{
			if(p_member->IsRequest())
			{
				normal_size++;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return normal_size;
}



/** 
 * @brief 通知删除某个成员
 * 
 */
void MODI_Family::NotifyDelMember(MODI_FamilyMember * p_member, enDelMemInfoReason reason)
{
	/// 广播家族成员某人退出家族 
	MODI_GS2C_Notify_DelFamilyMem send_cmd;
	send_cmd.m_guid = p_member->GetGuid();
	send_cmd.m_enReason = reason;
	Broadcast(&send_cmd, sizeof(send_cmd));
}



/** 
 * @brief 访问家族
 * 
 * @param p_client 访问家族的人 
 * 
 */
enFamilyOptResult MODI_Family::OnVisit(MODI_OnlineClient * p_client)
{
	if(p_client)
	{
		SendFamilyToClient(p_client, enReason_VisitFamily);
	}
	return enFamilyOptResult_None;
}


/** 
 * @brief 把家族信息发给客户端
 * 
 */
void MODI_Family::SendFamilyToClient(MODI_OnlineClient * p_client, enSendFullInfoReason reason)
{
	char buf[Skt::MAX_USERDATASIZE];
	DWORD send_size = GetFamilyFullData(buf, reason);
	p_client->SendPackage(buf, send_size);
	
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[send_full_family_info] send a full info to client <reason=%u,name=%s,membernum=%u>",
						  reason, p_client->GetRoleName(), send_size);
#endif
}



/** 
 * @brief 把家族信息发给所有的客户端
 * 
 */
void MODI_Family::SendFamilyToAll(enSendFullInfoReason reason)
{
	char buf[Skt::MAX_USERDATASIZE];
	DWORD send_size = GetFamilyFullData(buf, reason);
	Broadcast((stNullCmd *)buf, send_size);
}


/** 
 * @brief 更换族长
 * 
 * @param p_master 老族长
 * @param change_id 新族长
 */
enFamilyOptResult MODI_Family::OnChangeMaster(MODI_OnlineClient * p_master, const MODI_GUID & change_id)
{
	MODI_FamilyMember * p_old_master = FindMember(p_master->GetGUID());
	MODI_FamilyMember * p_new_master = FindMember(change_id);

	if(!p_old_master || !p_new_master)
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(! p_new_master->IsSlaver())
	{
		return enFamilyOptResult_Unknow;
	}
	
	p_old_master->OnAccept();
	p_old_master->SendMeToFamily();
	ChangeMaster(p_new_master);

	/// 更新二个玩家的家族名片
	p_master->UpDateFamilyInfo(p_old_master, enNone_Reason);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_CHARID charid = GUID_LOPART(change_id);
	MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
	if(p_client)
	{
		p_client->UpDateFamilyInfo(p_new_master, enNone_Reason);
	}

	return enFamilyPositionRetT_Ok;
}


/** 
 * @brief 设置副族长
 * 
 */
enFamilyOptResult MODI_Family::OnSetSlaver(MODI_OnlineClient * p_master, const MODI_GUID & set_id)
{
	MODI_FamilyMember * p_member = FindMember(set_id);
	if(!p_member)
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(p_member->IsSlaver())
	{
		return enFamilyOptResult_None;
	}

	if(IsSlaverFull())
	{
#ifdef _FAMILY_DEBUG		
		Global::logger->info("[set_slaver] slaver full <familyid=%u>", GetFamilyID());
#endif
		return enFamilyRetT_Position_Num;
	}
	
	p_member->SetSlaver();
	p_member->UpDateToDB();
	
	/// 告知家族所有的人
	p_member->SendMeToFamily();
	UpDateFamilyCard(p_member, enNone_Reason);

	/// 系统公告
	
	char tmp[MAX_PACKETSIZE];
	memset(tmp,'\0',sizeof(tmp));
	sprintf(tmp,CONFIG_VICE_LEAD,p_member->GetRoleLinkName());
	BroadcastSysChatExRequest(tmp, strlen(tmp));

	return enFamilyPositionRetT_Ok;
}


/** 
 * @brief 罢免副族长
 * 
 */
enFamilyOptResult MODI_Family::OnSetNormal(MODI_OnlineClient * p_master, const MODI_GUID & set_id)
{
	MODI_FamilyMember * p_member = FindMember(set_id);
	if(! p_member)
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	if(! p_member->IsSlaver())
	{
		Global::logger->warn("[free_slaver] this member is not slaver <familyid=%u,memberid=%u>", GetFamilyID(), (QWORD)set_id);
		return enFamilyOptResult_None;
	}

	p_member->SetNormal();
	p_member->UpDateToDB();
	p_member->SendMeToFamily();
	UpDateFamilyCard(p_member, enNone_Reason);
	
	return enFamilyPositionRetT_Ok;
}


/** 
 * @brief 家族成员数上限
 * 
 * 
 */
bool MODI_Family::IsFull()
{
	if(Global::g_byGameShop == 1)
	{
		if(GetMemNum() >= ((m_stBaseInfo.m_wdLevel - 1 )* 25 + 100))
		{
			return true;
		}
	}
	else
	{
		if(GetMemNum() >= m_stBaseInfo.m_wdLevel * 100)
		{
			return true;
		}
	}
	
	return false;
}


/** 
 * @brief 副族长是否满了
 * 
 */
bool MODI_Family::IsSlaverFull()
{
	if(SlaverMemNum() >= m_stBaseInfo.m_wdLevel)
	{
		return true;
	}
	
	return false;
}


/** 
 * @brief 更新对应的家族名片
 * 
 */
void MODI_Family::UpDateFamilyCard(MODI_FamilyMember * p_member, enUpDateCFReason reason)
{
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_CHARID charid = GUID_LOPART(p_member->GetGuid());
	MODI_OnlineClient * p_client = pMgr->FindByGUID(charid);
	if(p_client)
	{
		p_client->UpDateFamilyInfo(p_member, reason);
	}
}




/** 
 * @brief 获取家族全部信息
 * 
 * @param reason 理由
 */
const DWORD  MODI_Family::GetFamilyFullData(char * buf, enSendFullInfoReason reason)
{
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_FamilyFullInfo * p_send_cmd = (MODI_GS2C_Notify_FamilyFullInfo *)buf;
	AutoConstruct(p_send_cmd);
	MODI_Family * p_family = this;
	
	const MODI_FamilyBaseInfo & base_info = p_family->GetFamilyBaseInfo();
	
	p_send_cmd->m_stListInfo.m_dwFamilyID = base_info.m_dwFamilyID;
	memset(p_send_cmd->m_stListInfo.m_szName, 0, sizeof(p_send_cmd->m_stListInfo.m_szName));
	strncpy(p_send_cmd->m_stListInfo.m_szName, base_info.m_cstrFamilyName, sizeof(p_send_cmd->m_stListInfo.m_szName) - 1);
	p_send_cmd->m_stListInfo.m_wdLevel = base_info.m_wdLevel;
	p_send_cmd->m_stListInfo.m_dwRenqi = GetFamilyRenqi();
	p_send_cmd->m_stListInfo.m_dwMoney = base_info.m_dwMoney;
	p_send_cmd->m_stListInfo.m_wdMemnum = GetMemNum();
	strncpy(p_send_cmd->m_cstrXuanyan, base_info.m_cstrXuanyan, sizeof(p_send_cmd->m_cstrXuanyan) - 1);
	strncpy(p_send_cmd->m_cstrPublic, base_info.m_cstrPublic, sizeof(p_send_cmd->m_cstrPublic) - 1);
	p_send_cmd->m_enReason = reason;
	if(reason == enReason_ModiBase ||
	   reason == enReason_RequestFamily ||
	   reason == enReason_ModiPublic ||
	   reason == enReason_ModiXuanyan ||
	   reason == enReason_ChangeMaster ||
	   reason == enReason_Levelup)
	{
		p_send_cmd->m_dwMemberSize = 0;
	}
	else
	{
		struct MODI_GetMemInfo: public MODI_FamilyMemberCallback
		{
			MODI_GetMemInfo(MODI_FamilyMemInfo * info, enSendFullInfoReason &  s_reason): p_info(info), send_reason(s_reason)
			{
				m_dwSize = 0;
			}
			
			void Exec(MODI_FamilyMember * p_member)
			{
				if(send_reason == enReason_VisitFamily)
				{
					if(p_member->IsRequest())
					{
						return;
					}
				}
				
				const MODI_FamilyMemBaseInfo & base_info = p_member->GetMemBaseInfo();
				p_info->m_dwFamilyID = base_info.m_dwFamilyID;
				p_info->m_stGuid = base_info.m_stGuid;
				memset(p_info->m_szName, 0, sizeof(p_info->m_szName));
				strncpy(p_info->m_szName, base_info.m_cstrRoleName, sizeof(p_info->m_szName) - 1);
				p_info->m_bySex = base_info.m_bySex;
				p_info->m_enPosition = base_info.m_enPosition;
				p_info->m_enState = base_info.m_enState;
				p_info->m_dwContribute = base_info.m_dwContribute;
				p_info->m_dwRenqi = base_info.m_dwRenqi;
				p_info->m_wdChenghao = base_info.m_wdChenghao;
				p_info->m_stLastlogin = base_info.m_qdLastlogin;
#ifdef _FAMILY_DEBUG
				Global::logger->debug("[meminfo] send meminfo <name=%s,online=%d>", p_info->m_szName, p_info->m_enState);
#endif				
				p_info++;
				m_dwSize++;
			}
			DWORD m_dwSize;
			MODI_FamilyMemInfo * p_info;
			enSendFullInfoReason send_reason;
		};

		MODI_FamilyMemInfo * p_info = &(p_send_cmd->m_stMemInfo[0]);
		MODI_GetMemInfo get_info_call_back(p_info, reason);
		p_family->AllMemberExec(get_info_call_back);
		p_send_cmd->m_dwMemberSize = get_info_call_back.m_dwSize;
	}

	return (sizeof(MODI_GS2C_Notify_FamilyFullInfo) + sizeof(MODI_FamilyMemInfo) * p_send_cmd->m_dwMemberSize);
}




/** 
 * @brief 修改公告
 * 
 */
void MODI_Family::OnModiPublic(MODI_OnlineClient * p_master, const char * modi_public)
{
	strncpy(m_stBaseInfo.m_cstrPublic, modi_public, sizeof(m_stBaseInfo.m_cstrPublic) - 1);
	UpDateToDB();
	SendFamilyToAll(enReason_ModiPublic);
}


/** 
 * @brief 修改宣言
 * 
 */
void MODI_Family::OnModiXuanyan(MODI_OnlineClient * p_master, const char * modi_xuanyan)
{
	strncpy(m_stBaseInfo.m_cstrXuanyan, modi_xuanyan, sizeof(m_stBaseInfo.m_cstrXuanyan) - 1);
	UpDateToDB();
	SendFamilyToAll(enReason_ModiXuanyan);
}


/** 
 * @brief 获取家族基本信息
 * 
 * 
 */
void MODI_Family::GetFamilyListInfo(MODI_FamilyListInfo & base_info)
{
	base_info.m_dwFamilyID = m_stBaseInfo.m_dwFamilyID;
	strncpy(base_info.m_szName, m_stBaseInfo.m_cstrFamilyName, sizeof(base_info.m_szName) - 1);
	base_info.m_wdLevel = m_stBaseInfo.m_wdLevel;
	base_info.m_dwRenqi = m_stBaseInfo.m_dwRenqi;
	base_info.m_dwMoney = m_stBaseInfo.m_dwMoney;
	base_info.m_wdMemnum = GetMemNum();
}


/** 
 * @brief 获取成员数量,临时用
 * 
 */
const WORD  MODI_Family::GetMemNum()
{
	WORD num = 0;
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	for(; iter != m_stMemberMap.end(); iter++)
	{
		if(iter->second)
		{
			if(! (iter->second->IsRequest()))
			{
				num++;
			}
		}
	}
	return num;
}


/** 
 * @brief 获取家族人气
 * 
 * 
 */
const DWORD MODI_Family::GetFamilyRenqi()
{

	WORD num = 0;
	DWORD renqi = 0;
	defFamilyMemMapIter iter = m_stMemberMap.begin();
	for(; iter != m_stMemberMap.end(); iter++)
	{
		if(iter->second)
		{
			if(! (iter->second->IsRequest()))
			{
				num++;
			}
		}
	}

	renqi = num * 10;
	iter = m_stMemberMap.begin();
	for(; iter != m_stMemberMap.end(); iter++)
	{
		if(iter->second)
		{
			if(! (iter->second->IsRequest()))
			{
				renqi += iter->second->GetRenqi();
			}
		}
	}
	
	return renqi;
}


/** 
 * @brief 是否可以升级
 * 
 * 
 * @return 可以true
 */
enFamilyOptResult MODI_Family::IsCanLevelup()
{
	DWORD ren_qi = GetFamilyRenqi();
	BYTE level = m_stBaseInfo.m_wdLevel;
	
	if(level == 1 && ren_qi >= nsFamilyLevel::LEVEL_1_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 2 && ren_qi >= nsFamilyLevel::LEVEL_2_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 3 && ren_qi >= nsFamilyLevel::LEVEL_3_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 4 && ren_qi >= nsFamilyLevel::LEVEL_4_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 5 && ren_qi >= nsFamilyLevel::LEVEL_5_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 6 && ren_qi >= nsFamilyLevel::LEVEL_6_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 7 && ren_qi >= nsFamilyLevel::LEVEL_7_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 8 && ren_qi >= nsFamilyLevel::LEVEL_8_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 9 && ren_qi >= nsFamilyLevel::LEVEL_9_RENQI)
	{
		return enLevelFamily_Succ;
	}
	else if(level == 10)
	{
		return enLevelFamily_Level;
	}
	return enLevelFamily_NoRenqi;
}
