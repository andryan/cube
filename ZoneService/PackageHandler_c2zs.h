/** 
 * @file PackageHandler_c2zs.h
 * @brief C2ZS 数据包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef PACKAGE_HANDLER_H_C2ZS_
#define PACKAGE_HANDLER_H_C2ZS_

#include "protocol/gamedefine.h"
#include "IPackageHandler.h"
#include "SingleObject.h"



class MODI_PackageHandler_c2zs : public MODI_IPackageHandler , 
	public CSingleObject<MODI_PackageHandler_c2zs>
{
public:

    MODI_PackageHandler_c2zs();
    ~MODI_PackageHandler_c2zs();

	static int AddRelation_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int ModifyRelation_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int CanYouMarryMe_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int AnserMarry_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int ReqRoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int PrivateChatByName_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int SendMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int DelMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int SetMailRead_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

    static int GMCommand_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int GetAttachment_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int SystemBroast_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int WorldChat_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 客户端请求充值
	static int RequestPayCmd_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int RequestRank_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int RequestSpecificByRankNo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int RequestSelfRank_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoginFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int CreateFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int FamilyOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
	
	static int RequestMemberOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int FModifyPosition_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int VisitFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int FamilyNameCheck_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int ModiFamilyXuanyan_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );
	
	static int ModiFamilyPublic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int FamilyInvite_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int FamilyInviteResponse_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

	static int ReqBalance_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size );

private:

    virtual bool FillPackageHandlerTable();

};

#endif

