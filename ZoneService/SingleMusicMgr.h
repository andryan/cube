
#ifndef _SINGLE_MUSIC_INFO_H__
#define _SINGLE_MUSIC_INFO_H__

#include "AssertEx.h"
#include "Global.h"
#include "DBStruct.h"
#include "protocol/gamedefine.h"
#include "gamestructdef.h"
#include "Timer.h"
#define	DB_TIMER	5*60*1000
#define	GAME_TIMER	1*60*1000

/**
 * @brief 单曲音乐信息管理
 *
 */


class	MODI_SingMusicMgr
{
public:
	typedef std::map<WORD, SingleMusic *> defSingleMap;
	typedef std::map<WORD, SingleMusic *>::value_type  defSingleMapValue;
	typedef std::map<WORD, SingleMusic * >::iterator  defSingleMapIter;

	static  MODI_SingMusicMgr  & GetInstance();

	bool 	Init();

	bool 	OnRecvMusicInfo( SingleMusicInfo * p_music, enGameMode mode_t);

	bool	SyncToGame();

	bool	SyncToDataBase();
	
	WORD	GenPackage(SingleMusicInfo * buf,enGameMode mode_t);

	void	Update();

protected:
	
	SingleMusic  * GetMusic( WORD id);

	bool	InsertMusic(SingleMusic *p_music);

	void	NewMusicToDB( SingleMusic * p_music);
private:
	MODI_SingMusicMgr():m_DbTime(DB_TIMER),m_GameTime(GAME_TIMER)
	{
	}
	~MODI_SingMusicMgr()
	{
		delete m_pInstance;
		m_pInstance= NULL;
	}

	MODI_Timer 	m_DbTime;

	MODI_Timer	m_GameTime;

	static MODI_SingMusicMgr  * m_pInstance;
	
	defSingleMap	m_mMusicMap;

};


#endif

