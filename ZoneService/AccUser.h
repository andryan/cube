/**
 * @file   AccUser.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 18:14:08 2011
 * 
 * @brief  账号用户
 * 
 * 
 */

#ifndef _MD_ACCUSER_H
#define _MD_ACCUSER_H

#include "Global.h"
#include "protocol/gamedefine.h"


/**
 * @brief 账号用户
 * 
 */
class MODI_AccUser
{
 public:
	MODI_AccUser(const defAccountID & acc_id, bool chen_mi): m_dwAccID(acc_id),m_blChenMi(chen_mi) 
	{
		
	}

	bool IsChenMi()
	{
		return m_blChenMi;
	}

 private:
	/// 此用户账号 
	defAccountID m_dwAccID;

	/// 是否防沉迷
	bool m_blChenMi;
};

#endif
