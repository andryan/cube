#ifndef GM_MONMAND_ON_ZS_H_
#define GM_MONMAND_ON_ZS_H_


#include "IGMCommandHandler.h"
#include "SingleObject.h"


class MODI_OnlineClient;

class MODI_GMCmdHandler_zs : public MODI_IGMCommandHandler , public CSingleObject<MODI_GMCmdHandler_zs> 
{
	public:

	MODI_GMCmdHandler_zs();
	virtual ~MODI_GMCmdHandler_zs();

	private:

    virtual bool FillCmdHandlerTable();

	MODI_OnlineClient * 	m_pClient;
	public:


	void 	SetClient( MODI_OnlineClient * pClient );

	MODI_OnlineClient * 	GetClient();

	/** 
	 * @brief 显示GM命令帮助
	 * 
	 * @param nArgc   	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * @return 	成功返回 enGMCmdHandler_OK 
	 */
	static int ShowHelp( int nArgc ,const char ** szArgs );

	/** 
	 * @brief 获得自己邮件列表中的所有邮件的附件
	 * 
	 * @param nArgc 
	 * @param szArgs
	 * 
	 * @return 	
	 */
	static int TestGetMailAttach( int nArgc ,const char ** szArgs );


	/** 
	 * @brief 显示服务器信息
	 * 
	 * @param nArgc 	参数个数
	 * @param szArgs 	参数内容
	 * 
	 * @return 	返回 enGMCmdHandler_SendToZone 
	 */
	static int ShowZoneInfo( int nArgc ,const char ** szArgs);

	static int KickAllClients( int nArgc ,const char ** szArgs );

	static int KickClient( int nArgc ,const char ** szArgs );
	static int FreezeClient( int nArgc ,const char ** szArgs );
	static int SetChannelClientNum( int nArgc ,const char ** szArgs );
	static int SetAttr( int nArgc ,const char ** szArgs );
	static int SetConstantValue( int nArgc , const char ** szArgs );
	static int AddFensi( int nArgc , const char ** szArgs );
	static int AddBlack( int nArgc , const char ** szArgs );
	static int DecBlack( int nArgc , const char ** szArgs );
	static int RefreshPaim( int nArgc , const char ** szArgs );
};



#endif
