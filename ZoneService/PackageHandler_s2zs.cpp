#include "PackageHandler_s2zs.h"
#include "PackageHandler_zs2ms.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "protocol/c2gs.h"
#include "Base/HelpFuns.h"
#include "Base/AssertEx.h"
#include "GameSvrClientTask.h"
#include "s2zs_cmd.h"
#include "OnlineClientMgr.h"
#include "ManangerClient.h"
#include "World.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"
#include "Mail.h"
#include "MailCore.h"
#include "Base/TransCommand.h"
#include "BillClient.h"
#include "config.h"
#include "SystemUser.h"
#include "SingleMusicMgr.h"

MODI_PackageHandler_s2zs::MODI_PackageHandler_s2zs()
{

}

MODI_PackageHandler_s2zs::~MODI_PackageHandler_s2zs()
{

}

int MODI_PackageHandler_s2zs::DirectionToManangerServer( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_ManangerSvrClient * pMs = MODI_ManangerSvrClient::GetInstancePtr();
	if( pMs )
	{
		pMs->SendCmd( pt_null_cmd , cmd_size );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::DirectionToDBProxy( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);

	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( pServer->GetServerID() );
	if( pDB )
	{
		pDB->SendCmd( pt_null_cmd , cmd_size );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::DirectionToZoneDBProxy( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
	if( pDB )
	{
		pDB->SendCmd( pt_null_cmd , cmd_size );
	}
	return enPHandler_OK;
}

bool MODI_PackageHandler_s2zs::FillPackageHandlerTable()
{
	// 游戏服务器注册
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_RegSelf::ms_SubCmd , 
		&MODI_PackageHandler_s2zs::ServerReg_Handler );

	// 游戏服务器通知有客户端请求进入频道
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_EnterChannel::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::ClientReqEnterChannel_Handler );

	// 游戏服务器通知有客户端请求切换频道
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_RechangeChannel::ms_SubCmd , 
		&MODI_PackageHandler_s2zs::ClientRechangeChannel_Handler );

	// 游戏服务器通知客户端选定频道
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_SelectChannel::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::ClientSelectChannel_Handler );

	// 游戏服务器通知有客户端登出
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_ClientLoginOut::ms_SubCmd , 
		&MODI_PackageHandler_s2zs::ClientLoginOut_Handler );

	// 游戏服务器返回踢人结果
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_KickSbResult::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::KickSbResult_Handler );

	// gs-adb 请求验证登入的帐号
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_LoginAuth::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::DirectionToManangerServer );

	// gs-adb 请求重新生成pp
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_ReMakePP::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::DirectionToManangerServer );

	// gs-rdb 
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ItemSerial::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToZoneDBProxy );

	// gs-rdb 
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveSerial::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToZoneDBProxy);

	// gs-rb
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveItem::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// 增加音乐点击率
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Notify_MusicCount::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// 加载包裹信息
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ItemInfo::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// 申请获取网页道具
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Req_WebItem::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// gs数据库操作，临时zoneservic转一下，后期改成直接和数据库代理通信
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ExecSql_Cmd::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ExecSql_Transaction_Cmd::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ExchangeByYunyingKey::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_SaveYunYingKey::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// 减钱
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_DecMoney_Cmd::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	/// 购买物品
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_BuyItem_Cmd::ms_SubCmd,
		&MODI_PackageHandler_s2zs::DirectionToDBProxy );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_AddMoenyRes::ms_SubCmd,
		&MODI_PackageHandler_s2zs::AddMoneyRes_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_AddItemRes::ms_SubCmd,
		&MODI_PackageHandler_s2zs::AddItemRes_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Notify_UpdateObj::ms_SubCmd,
		&MODI_PackageHandler_s2zs::UpdateObj_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_SendGiveItemMail::ms_SubCmd,
		&MODI_PackageHandler_s2zs::SendAddItemMail_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_QueryHasChar_Shop::ms_SubCmd,
		&MODI_PackageHandler_s2zs::QueryHasCharShop_Handler );

	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_CreateItemHistroy::ms_SubCmd,
		&MODI_PackageHandler_s2zs::CreateItemHistroy_Handler );

	this->AddPackageHandler( MODI_ConsumeMoneyCmd::m_bySCmd , MODI_ConsumeMoneyCmd::m_bySParam ,
		&MODI_PackageHandler_s2zs::BillTransaction_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_SbEnterRare::ms_SubCmd ,
		&MODI_PackageHandler_s2zs::SBEnterRare_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_ChangeVar::ms_SubCmd,
		&MODI_PackageHandler_s2zs::ChangeVar_Handler );

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_SendSysMail::ms_SubCmd,
		&MODI_PackageHandler_s2zs::SendSysmail_Handler);

	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_MusicEnd ::ms_SubCmd,
		&MODI_PackageHandler_s2zs::MusicEnd_Handler );

	this->AddPackageHandler(MODI_INAItemConsume::m_bySCmd, MODI_INAItemConsume::m_bySParam,
							&MODI_PackageHandler_s2zs::INAItemConsume_Handler);

	this->AddPackageHandler(MODI_INAGiveItem::m_bySCmd, MODI_INAGiveItem::m_bySParam,
							&MODI_PackageHandler_s2zs::INAGiveItem_Handler);


	AddPackageHandler(MAINCMD_S2ZS, MODI_S2ZS_Request_ChangeVip::ms_SubCmd,
			&MODI_PackageHandler_s2zs::ChangeVip_Handler);

	AddPackageHandler(MAINCMD_S2ZS,MODI_S2ZS_Request_DelFrindandIdole::ms_SubCmd,
			&MODI_PackageHandler_s2zs::DelFriendIdol_Handler);
	AddPackageHandler(MAINCMD_S2ZS,MODI_S2ZS_Modify_Renqi::ms_SubCmd,
			&MODI_PackageHandler_s2zs::Request_ModifyRenqi);

	AddPackageHandler(MAINCMD_S2ZS,MODI_S2ZS_Broadcast_SystemChat::ms_SubCmd,
			&MODI_PackageHandler_s2zs::Request_BroadcastSysChat);	
	return true;
}
	
// 游戏服务器注册的处理
int MODI_PackageHandler_s2zs::ServerReg_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_RegSelf * pMsg = (const MODI_S2ZS_Request_RegSelf *)(pt_null_cmd);
	MODI_GameSvrClientTask * pTask = (MODI_GameSvrClientTask *)(pObj);
	MODI_ASSERT( pMsg->m_ServerID != 0 );
	if( !pMsg->m_ServerID )
		return enPHandler_Kick;

	if( MODI_GameSvrClientTask::RegServer( pMsg->m_ServerID , pMsg->m_szServerName , 
			pMsg->m_nGatewayIP , pMsg->m_nGatewayPort , pTask ) )
	{
		// 向对应DB请求last item
		MODI_S2RDB_Request_ItemSerial load_itemserial_msg;
		load_itemserial_msg.m_byServerID = pMsg->m_ServerID;
		load_itemserial_msg.m_byWorldID = pMsg->m_nWorldID;
		DirectionToZoneDBProxy( pObj , (const Cmd::stNullCmd *)(&load_itemserial_msg) ,
				sizeof(load_itemserial_msg) );
	}

	return enPHandler_OK;
}
	
// 客户端请求进入频道
int MODI_PackageHandler_s2zs::ClientReqEnterChannel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_EnterChannel * pReq = (const MODI_S2ZS_Request_EnterChannel *)(pt_null_cmd);
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);

	MODI_ZS2S_Notify_EnterChannelRet msg;
	msg.m_sessionID = pReq->m_nSessionID;
	msg.m_accid = pReq->m_accid;

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pOnlineOne = pMgr->FindByAccid( pReq->m_accid );
	if( !pOnlineOne )
	{
		// 客户端不在线,需要从db读取
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( pServer->GetServerID() );
		if( !pDB )
		{
			msg.m_bySuccessful = 0;
			pServer->SendCmd( &msg , sizeof(msg) );
			return enPHandler_Kick;
		}

		MODI_ASSERT( pServer->GetServerID() !=  0 );

		MODI_S2RDB_Request_LoadCharData 	msg;
		msg.m_nAccountID = pReq->m_accid;
		msg.m_nSessionID = pReq->m_nSessionID;
		msg.m_nCharID = pReq->m_charid;
		msg.m_nServerID = pServer->GetServerID();
		pDB->SendCmd( &msg , sizeof(msg) );
			
		return enPHandler_OK;
	}

	// 该客户端已经在线

	bool bFaild = false;
	if( pReq->m_charid != pOnlineOne->GetCharID() )
	{
		bFaild = true;
	}

	if( pOnlineOne->IsInWaitReEnterChannelState() == false &&
		pOnlineOne->IsInWaitAnotherLoginState() == false )
	{
		bFaild = true;
	}

	if( pOnlineOne->IsInWaitReEnterChannelState() )
	{
		if( pServer->GetServerID() != pOnlineOne->GetNewServerID() )
		{
			bFaild = true;
		}
	}

	MODI_ASSERT( pReq->m_accid == pOnlineOne->GetAccid() );

	BYTE old_server_id = pOnlineOne->GetServerID();

	pOnlineOne->SetNewServerID( 0 );
	pOnlineOne->SetServerID( pServer->GetServerID() );
	pOnlineOne->SetServerName( pServer->GetServerName() );
	pOnlineOne->SetSessionID( pReq->m_nSessionID );


	if( bFaild )
	{
		msg.m_bySuccessful = 0;
		pServer->SendCmd( &msg , sizeof(msg) );
	}
	else 
	{
		msg.m_bySuccessful = 1;
		memcpy( &msg.m_fullData , &pOnlineOne->GetFullData() , sizeof(MODI_CharactarFullData) );
		pServer->SendCmd( &msg , sizeof(msg) );

//		pOnlineOne->OnLoginIn();

		pOnlineOne->OnEnterChannel();

		MODI_GameSvrClientTask * old_server = MODI_GameSvrClientTask::FindServer( old_server_id );
		if( old_server )
		{
			old_server->DecClientNum();
		}

		pServer->IncClientNum();

		/// 用户登录游戏世界(可能是切换频道)
		
		
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( pServer->GetServerID() );
		if( !pDB )
		{
			return enPHandler_OK;
		}
		
		/// 用户可能是切换频道，登陆到游戏频道
		pOnlineOne->OnLoginGameService();

		// 进入游戏世界，处于游戏状态
		pOnlineOne->SwitchToInWorldPlayingState();
 		pOnlineOne->UpdateLastLoginInfo();
  	}

	return enPHandler_OK;
}

// 客户端请求切换频道
int MODI_PackageHandler_s2zs::ClientRechangeChannel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_RechangeChannel * pMsg = (const MODI_S2ZS_Request_RechangeChannel *)(pt_null_cmd);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( !pMgr )
		return enPHandler_OK;
	MODI_OnlineClient * pClient = pMgr->FindByGUID( GUID_LOPART(pMsg->m_guid) );
	if( !pClient )
		return enPHandler_OK;

	if( !pClient->IsInWorldPlayingState() )
		return enPHandler_OK;

	pClient->SetLoginKey( pMsg->m_key );
	pClient->OnRecvFullDataFromGameServer( pMsg->m_fullData );
	pClient->SetGameState( kOutServer );
	pClient->OnLeaveChannel();

	// 下发频道列表
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	int iSendSize = MODI_GameSvrClientTask::CreateChannelListPackage( szPackageBuf , Skt::MAX_USERDATASIZE ,
																	  MODI_LS2C_Notify_Channellist::kRechangeChannel, pClient->GetAccid());
	pClient->SendPackage( szPackageBuf , iSendSize );

	// 等待选择频道
	pClient->SwitchToWaitRechangeChannel();

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::ClientSelectChannel_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);
	const MODI_S2ZS_Request_SelectChannel * pReq = (const MODI_S2ZS_Request_SelectChannel *)(pt_null_cmd);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByGUID( GUID_LOPART( pReq->m_guid ) );
	if( !pClient )
		return enPHandler_OK;
	if( !pReq->m_byServer )
		return enPHandler_OK;

	bool bSendChannellist = false;
	MODI_LS2C_Notify_Channellist::Reason res = MODI_LS2C_Notify_Channellist::kBegin;
	// 所选择的频道是否还存在
	MODI_GameSvrClientTask * pSelected = MODI_GameSvrClientTask::FindServer( pReq->m_byServer );
	if( !pSelected )
	{
		bSendChannellist = true;
		res = MODI_LS2C_Notify_Channellist::kChannelNotExist;
	}

	//if( pSelected && pSelected->GetClientNum() >= GAME_CHANNEL_MAX_CLIENT )
	if( pSelected && pSelected->GetClientNum() >= MODI_GameSvrClientTask::m_wdChannelMaxClient )
	{
		bSendChannellist = true;
		res = MODI_LS2C_Notify_Channellist::kChannelFull;
	}	

	if( bSendChannellist )
	{
		// 重新下发频道列表
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		int iSendSize = MODI_GameSvrClientTask::CreateChannelListPackage( szPackageBuf , 
				Skt::MAX_USERDATASIZE ,
																		  res , pClient->GetAccid());
		pClient->SendPackage( szPackageBuf , iSendSize );
		return enPHandler_OK;
	}

	// 等待重新进入频道
	pClient->SwitchToWaitReEnterChannelState();
	pClient->SetNewServerID( pReq->m_byServer );

	MODI_LS2C_Notify_GatewayInfo msg;
	
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(! plist || !(pReq->m_szNetType))
		return enPHandler_OK;
	
	MODI_ChannelAddr get_addr;
	if(! plist->GetChannelAddr(pReq->m_byServer, pReq->m_szNetType, get_addr))
	{
		Global::logger->fatal("[get_channel_info] get channel <%d,%s> failed", pReq->m_byServer, pReq->m_szNetType);
		return enPHandler_OK;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[channel_info] select info <id=%d,nettype=%s,ip=%s,port=%u>", pReq->m_byServer, pReq->m_szNetType,
						  get_addr.m_strChannelIP.c_str(), get_addr.m_wdChannelPort);
#endif
	
	msg.m_nIP = PublicFun::StrIP2NumIP(get_addr.m_strChannelIP.c_str());
	msg.m_nPort = get_addr.m_wdChannelPort;
	
	// 下发客户端所选的频道的信息
	//msg.m_nIP = pSelected->GetGateIP();
	//msg.m_nPort = pSelected->GetGatePort();
	msg.m_Key = pClient->GetLoginKey();
	msg.m_nAccountID = pClient->GetAccid();
	pClient->SendPackage( &msg , sizeof(msg) );

	// 让原来的服务器踢掉之前的客户端
	MODI_ZS2S_Notify_KickSb kick;
	kick.m_byReason = kRechangeChannel;
	kick.m_guid = pClient->GetGUID();
	kick.m_sessionID = pClient->GetSessionID();
	kick.m_bySelectSverver = pReq->m_byServer;
	pServer->SendCmd( &kick , sizeof(kick) );

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::ClientLoginOut_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_ClientLoginOut * pMsg = (const MODI_S2ZS_Request_ClientLoginOut *)(pt_null_cmd);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( !pMgr )
		return enPHandler_OK;

	MODI_CHARID charid = GUID_LOPART( pMsg->m_guid );
	MODI_OnlineClient * pClient = pMgr->FindByGUID( charid );
	if( !pClient )
	{
		Global::logger->error("[%s] recv client loginout package , but client<charid=%u> not online." ,
				SVR_TEST ,
				charid );
		return enPHandler_OK;
	}
	pClient->OnRecvFullDataFromGameServer( pMsg->m_fullData );
	MODI_World * pWorld = MODI_World::GetInstancePtr();
	pWorld->OnClientLoginout( pClient );
	return enPHandler_OK;
}


int MODI_PackageHandler_s2zs::KickSbResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);
	const MODI_S2ZS_Request_KickSbResult * pResult  = (const MODI_S2ZS_Request_KickSbResult *)(pt_null_cmd);
	MODI_CHARID charid = GUID_LOPART(pResult->m_guid);

	Global::logger->info("[%s] recv kick client<charid=%u> result form server<serverid=%u,result=%u,reason=%s>." ,
			KICK_CLIENT ,
			charid,
			pServer->GetServerID() ,
			pResult->m_byResult,
			GetKickReasonStringFormat( pResult->m_byReason ) );

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByGUID( charid );
	if( !pClient )
	{
		Global::logger->info("[%s] gs kicksb result<%u> , reason<%u> , but not found client<charid=%u> in zone." , 
					SVR_TEST ,
					pResult->m_byResult,
					pResult->m_byReason,
					charid );
		return enPHandler_OK;
	}

	if( pResult->m_byReason == kAntherLogin )
	{
		if( pResult->m_byResult == MODI_S2ZS_Request_KickSbResult::enFound )
		{
			pClient->OnRecvFullDataFromGameServer( pResult->m_fullData );
		}

		pClient->ResetTempData();
		// 发送角色列表
		MODI_CharInfo charinfo;
		charinfo.m_byLevel = pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
		charinfo.m_bySex = pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
		charinfo.m_nCharID = pClient->GetCharID();
		safe_strncpy( charinfo.m_szRoleName , pClient->GetRoleName() , sizeof(charinfo.m_szRoleName) );
		charinfo.m_AvatarInfo = pClient->GetFullData().m_roleInfo.m_normalInfo.m_avatarData;
		charinfo.m_nDefaultAvatarIdx = pClient->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
		charinfo.m_byReadHelp = pClient->GetFullData().m_roleInfo.m_detailInfo.m_byReadHelp;
		charinfo.m_byVip = pClient->GetFullData().m_roleInfo.m_baseInfo.m_bVip;
		MODI_PackageHandler_zs2ms::SendCharlist( pClient->GetAccid() , pResult->m_LoginServerSessionID , 1 , &charinfo , 1 );

		// 等待另外一个客户端进入
		pClient->SwtichToWaitAnotherLoginState();
	}
	else if( pResult->m_byReason == kRechangeChannel )
	{
		
	}
	else 
	{
		if( pResult->m_byResult == MODI_S2ZS_Request_KickSbResult::enNotFound )
		{
			if( pServer->GetServerID() == pClient->GetServerID() )
			{
				Global::logger->info("[%s] gs kicksb result<not found> client<charid=%u>. find it in zone. free it." , 
						SVR_TEST ,
						charid );
				MODI_World * pWorld = MODI_World::GetInstancePtr();
				pWorld->OnClientLoginout( pClient );
			}
			else 
			{
				Global::logger->error("[%s] gs kicksb result<not found client>,find client<charid=%u> in zone. \
						but serverid not equal.<cs=%u,ss=%u>" , 
					SVR_TEST ,
					charid ,
					pClient->GetServerID() ,
					pServer->GetServerID() );
				MODI_ASSERT(0);
			}
		}
	}

	return enPHandler_OK;
}


int MODI_PackageHandler_s2zs::AddMoneyRes_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_AddMoenyRes * pResult  = (const MODI_ZS2S_Notify_AddMoenyRes *)(pt_null_cmd);

	MODI_CHARID charid = GUID_LOPART(pResult->m_guid);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByGUID( charid );
	if( !pClient )
		return enPHandler_OK;

	if( pResult->m_reason == kGetMailAttachment )
	{
		MODI_GS2C_Notify_GetAttachmentRes res;
		DecodeZS2SOpt_GetAttachment( pResult->m_szData , res.m_mailid );

		MODI_Mail * pMail = pClient->GetUserMail(res.m_mailid);
		if( pMail )
		{
			pMail->SetLockAttachment( false );
			Global::logger->debug("[%s] set mail lock to false." , SVR_TEST );
		}

		if( pResult->m_result == kZS2S_Successful )
		{
			res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kSuccessful;
			if( pMail )
			{
				pMail->SetAlreadyGetAttachment();
				pMail->ResetExtraInfo();
			}
		}
		else 
		{
			res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kGetAttachFaild;
		}

		pClient->SendPackage( &res , sizeof(res) );
	}


	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::AddItemRes_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ZS2S_Notify_AddItemRes * pResult  = (const MODI_ZS2S_Notify_AddItemRes *)(pt_null_cmd);

	MODI_CHARID charid = GUID_LOPART(pResult->m_guid);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByGUID( charid );
	if( !pClient )
		return enPHandler_OK;

	if( pResult->m_reason == kGetMailAttachment )
	{
		MODI_GS2C_Notify_GetAttachmentRes res;
		DecodeZS2SOpt_GetAttachment( pResult->m_szData , res.m_mailid );

		MODI_Mail * pMail = pClient->GetUserMail(res.m_mailid);
		if( pMail )
		{
			pMail->SetLockAttachment( false );
			Global::logger->debug("[%s] set mail lock to false." , SVR_TEST );
		}

		if( pResult->m_result == kZS2S_Successful )
		{
			res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kSuccessful;

			if( pMail )
			{
				pMail->SetAlreadyGetAttachment();
				pMail->ResetExtraInfo();
			}
		}
		else if( pResult->m_result == kZS2S_BagFull )
		{
			res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kGetItemFaild_BagFull;
		}
		else 
		{
			res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kGetAttachFaild;
		}
		pClient->SendPackage( &res , sizeof(res) );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::UpdateObj_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Notify_UpdateObj * pUpdate  = (const MODI_S2ZS_Notify_UpdateObj *)(pt_null_cmd);

	// 判断下是哪种具体类型
	// to do ..

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_CHARID charid = GUID_LOPART(pUpdate->m_guid);
	MODI_OnlineClient * pClient = pMgr->FindByGUID( charid );
	if( !pClient )
		return enPHandler_OK;

	const MODI_ObjUpdateMask & upm = pUpdate->m_UpdateMask;
	MODI_UpdateDataRead r(pUpdate->m_pData,pUpdate->m_nSize);
	pClient->OnRecvUpdateData( r , upm );
	return enPHandler_OK;
}
	
int MODI_PackageHandler_s2zs::SendAddItemMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	//MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);
	//	if( !pServer )
	//		return enPHandler_OK;
	
// 	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
// 	if( !pDB )
// 		return enPHandler_OK;

	const MODI_S2ZS_Request_SendGiveItemMail * pReq  = (const MODI_S2ZS_Request_SendGiveItemMail *)(pt_null_cmd);
	
	//const char * szCaption_Buy = CONFIG_ITEM_YOU_BUY;
	//const char * szContent_Buy = CONFIG_ITEM_CONTENT;
	const char * szCaption_Give = CONFIG_ITEM_PRESENT;


// 	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
// 	MODI_ASSERT(ptm);
// 	if( !ptm )
// 		return enPHandler_OK;

	MODI_SendMailInfo sendm;
	sendm.m_Type = kAttachmentItemMail;

	/// 是花钱赠送的,另外的算系统邮件了
	if( pReq->m_shopmail )
	{
		// MODI_S2RDB_Request_CreateItemHistroy cih;
// 		cih.m_Histroy.charid = pReq->m_sender_charid;
// 		cih.m_Histroy.createtype = pReq->m_type;
// 		cih.m_Histroy.mailid = INVAILD_DBMAIL_ID;
// 		cih.m_Histroy.transid = pReq->m_TransID;
// 		cih.m_Histroy.recvstate = kShopGoodRecvState_Mailing;

		/// 包裹不够不发邮件了，直接不让卖了。等下个版本支持此功能吧
	// 	if( pReq->m_type == kTransaction_Buy )
// 		{
// 			safe_strncpy( sendm.m_szCaption  , szCaption_Buy , sizeof(sendm.m_szCaption) );
// 			safe_strncpy( sendm.m_szRecevier , pReq->m_szReceverName , sizeof(sendm.m_szRecevier) );
// 			if( pReq->m_szContent && pReq->m_szContent[0] )
// 			{
// 				safe_strncpy( sendm.m_szContents , pReq->m_szContent , sizeof(sendm.m_szContents) );
// 			}
// 			else 
// 			{
// 				safe_strncpy( sendm.m_szContents , szContent_Buy , sizeof(sendm.m_szContents) );
// 			}

// 			MODI_OnlineClient * pClient = pMgr->FindByName( pReq->m_szSenderName );
// 			for( BYTE n = 0; n < pReq->m_nCount; n++ )
// 			{
// 				const MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = pReq->m_Goods[n];
// 				sendm.m_ExtraInfo.SetAttachment_ItemInfo( tmp.m_nItemConfigID ,  tmp.m_nCount , tmp.m_nLimitTime );
// 				if( MODI_MailCore::ServerSendGiveMailToClient( sendm , pClient  , SYS_MAIL_SENDER , 
// 					   cih.m_Histroy.transid , &cih.m_Histroy.mailid	) )
// 				{
// 					cih.m_Histroy.count =  1;
// 					//pDB->SendCmd( &cih, sizeof(cih) );
// 				}
// 				else 
// 				{
// 					Global::logger->error("[%s] client<name=%s> buy goods<trandid=%llu>,but send givemail faild." , 
// 							SHOP_MODULE ,
// 							pReq->m_szSenderName,
// 							cih.m_Histroy.transid );
// 				}
// 			}
// 		}
		//else if( pReq->m_type == kTransaction_ToGive )
		if( pReq->m_type == kTransaction_ToGive )
		{
			/// 赠送
			safe_strncpy( sendm.m_szRecevier , pReq->m_szReceverName , sizeof(sendm.m_szRecevier) );
			safe_strncpy( sendm.m_szCaption  , szCaption_Give , sizeof(sendm.m_szCaption) );
			safe_strncpy( sendm.m_szContents , pReq->m_szContent , sizeof(sendm.m_szContents) );


			if(pReq->m_nCount > 1)
			{
				MODI_ASSERT(0);
				Global::logger->warn("[give_mails] client max give one <%u>", pReq->m_nCount);
				return enPHandler_OK;
			}

			sendm.m_byExtraCount = pReq->m_nCount;
			for( BYTE n = 0; n < pReq->m_nCount; n++ )
			{
				const MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = pReq->m_Goods[n];
				sendm.m_ExtraInfo[n].SetAttachment_ItemInfo( tmp.m_nItemConfigID ,  tmp.m_nCount , tmp.m_nLimitTime );
			}
				
			const char * szSender = 0;
			if( !pReq->m_szSenderName[0] )
			{
				MODI_ASSERT(0);
				return enPHandler_OK;
			}

			MODI_DBMail_ID  mail_id = 0;
			szSender = pReq->m_szSenderName;
			if( MODI_MailCore::SendMail(sendm, szSender, &mail_id, pReq->m_TransID) )
			{
				Global::logger->error("[send_give_mail] mails successful <mailid=%llu, send=%s,recever=%s,trandid=%llu>" , 
									  mail_id, szSender, sendm.m_szRecevier, pReq->m_TransID);
				return enPHandler_OK;
			}
			else 
			{
				Global::logger->error("[send_give_mail] mails failed <send=%s,recever=%s,trandid=%llu>" , 
									  szSender, sendm.m_szRecevier, pReq->m_TransID);
				return enPHandler_OK;
			}
		}
		else if( pReq->m_type == kTransaction_AskFor )
		{

		}
	}
	else 
	{
		/// 系统赠送
		safe_strncpy( sendm.m_szRecevier , pReq->m_szReceverName , sizeof(sendm.m_szRecevier) );
		std::string m_strCaption = pReq->m_szCaption;
		if(m_strCaption != "")
		{
			safe_strncpy( sendm.m_szCaption  , m_strCaption.c_str(), sizeof(sendm.m_szCaption) );
		}
		else
		{
			safe_strncpy( sendm.m_szCaption  , szCaption_Give, sizeof(sendm.m_szCaption) );
		}
		safe_strncpy( sendm.m_szContents , pReq->m_szContent , sizeof(sendm.m_szContents) );

		sendm.m_byExtraCount = pReq->m_nCount;
		for( BYTE n = 0; n < pReq->m_nCount; n++ )
		{
			const MODI_S2ZS_Request_SendGiveItemMail::tagGoods & tmp = pReq->m_Goods[n];
			sendm.m_ExtraInfo[n].SetAttachment_ItemInfo( tmp.m_nItemConfigID ,  tmp.m_nCount , tmp.m_nLimitTime );
		}
		
		const char * szSender = 0;
		if( !pReq->m_szSenderName[0] )
		{
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		szSender = pReq->m_szSenderName;
		MODI_DBMail_ID  mail_id = 0;
		if( MODI_MailCore::SendMail(sendm, szSender, &mail_id))
		{
			Global::logger->debug("[send_system_mail] mails successful <mailid=%llu, name=%s>" ,mail_id, sendm.m_szRecevier);
			return enPHandler_OK;
		}
		else 
		{
			Global::logger->debug("[send_system_mail] mails failed <name=%s>" , sendm.m_szRecevier);
			return enPHandler_OK;
		}
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::QueryHasCharShop_Handler(  void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_QueryHasChar_Shop * pReq  = (const MODI_S2ZS_Request_QueryHasChar_Shop *)(pt_null_cmd);
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);
	if( pReq->m_nServerID != pServer->GetServerID() )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pOnline = pMgr->FindByName( pReq->m_szRoleName );
	MODI_OfflineUser * pOffline = 0;
	if( !pOnline )
	{
		MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
		pOffline = pOfflineMgr->FindByName( pReq->m_szRoleName );
		if( !pOffline )
		{
			MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( pServer->GetServerID() );
			if( pDB )
			{
				char szPackageBuf[Skt::MAX_USERDATASIZE];
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				MODI_S2RDB_Request_RoleDetailInfo * pTemp = (MODI_S2RDB_Request_RoleDetailInfo *)(szPackageBuf);
				AutoConstruct( pTemp );

				MODI_S2RDB_Request_RoleDetailInfo & reqRoleDetail = *pTemp;
				reqRoleDetail.m_reqCharid = pReq->m_queryClient;
				safe_strncpy(reqRoleDetail.m_szName , pReq->m_szRoleName , sizeof(reqRoleDetail.m_szName) );
				reqRoleDetail.m_nExtraSize = pReq->m_nSize;
				memcpy( &reqRoleDetail.m_szExtra , pReq->m_pExtraData , reqRoleDetail.m_nExtraSize );
				if( pReq->m_type == kQueryChar_Shop_Give )
				{
					reqRoleDetail.m_opt = kReqShopGiveGood;
				}
				else if( pReq->m_type == kQueryChar_Shop_AskFor )
				{
					MODI_ASSERT(0);
					return enPHandler_OK;
				}

				int iSendSize = sizeof(MODI_S2RDB_Request_RoleDetailInfo) + sizeof(char) * reqRoleDetail.m_nExtraSize;
				pDB->SendCmd( &reqRoleDetail , iSendSize );
				return enPHandler_OK;
			}
		}
	}

	if( pOnline || pOffline )
	{
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_ZS2S_Notify_QueryHasCharResult_Shop * res = (MODI_ZS2S_Notify_QueryHasCharResult_Shop *)(szPackageBuf);
		AutoConstruct(res);
		res->m_bHashChar = true;
		res->m_nServerID = pReq->m_nServerID;
		res->m_nSize = pReq->m_nSize;
		res->m_queryClient = pReq->m_queryClient;
		res->m_type = pReq->m_type;

		if( pOnline )
		{
			res->m_sex = pOnline->GetSexType();
		}
		else 
		{
			res->m_sex = pOffline->GetSex();
		}

		memcpy( res->m_pExtraData  , pReq->m_pExtraData , res->m_nSize );
		safe_strncpy( res->m_szRoleName , pReq->m_szRoleName , sizeof(res->m_szRoleName) );
		int iSendSize = sizeof(MODI_ZS2S_Notify_QueryHasCharResult_Shop) + res->m_nSize * sizeof(char);
		pServer->SendCmd( res , iSendSize );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_s2zs::CreateItemHistroy_Handler(  void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_S2RDB_Request_CreateItemHistroy * pReq  = (MODI_S2RDB_Request_CreateItemHistroy *)(pt_null_cmd);
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
	if( pDB )
	{
		if( pDB->SendCmd( pt_null_cmd , cmd_size ) )
		{
			Global::logger->info("[%s] send create item<charid=%u,tranid=%llu> histroy to db successful." , 
					SHOP_MODULE ,
					pReq->m_Histroy.charid , 
					pReq->m_Histroy.transid);
		}
		else 
		{
			Global::logger->error("[%s] send create item<charid=%u,tranid=%llu> histroy to db faild." , 
					SHOP_MODULE ,
					pReq->m_Histroy.charid , 
					pReq->m_Histroy.transid);
		}
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::BillTransaction_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_ConsumeMoneyCmd * pReq = ( const MODI_ConsumeMoneyCmd *)(pt_null_cmd);
	MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);

	MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
	if( !pBill || pBill->IsConnected() == false )
	{
		MODI_RetConsumeMoneyCmd res;
		res.m_enResult = enTransBillErr;
		res.m_qdTransID = pReq->m_stTransData.m_qdTransID;
		res.m_client = pReq->m_stTransData.m_stCharID;
		res.m_iTransType = pReq->m_iTransType;
		memcpy( res.m_szExtraData , pReq->m_szExtraData , sizeof(pReq->m_szExtraData) );
		pServer->SendCmd( &res , sizeof(res) );

		Global::logger->warn("[%s] not connection with bill server.client<charid=%u> can't buy rmb good. " , SVR_TEST ,
				pReq->m_stTransData.m_stCharID );
	}
	else 
	{
		pBill->SendCmd( pt_null_cmd , cmd_size );
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_s2zs::SBEnterRare_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_SbEnterRare * pReq = (const MODI_S2ZS_Request_SbEnterRare *)(pt_null_cmd);
	if( pReq )
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		if( pMgr )
		{
			MODI_GS2C_Notify_SbEnterRareShop msg;
			safe_strncpy( msg.m_szName  , pReq->m_szName , sizeof(msg.m_szName) );
			pMgr->Broadcast( &msg ,sizeof(msg) );
		}
	} 
	return enPHandler_OK;
}
int MODI_PackageHandler_s2zs::ChangeVar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{

	MODI_S2ZS_Request_ChangeVar * change = (MODI_S2ZS_Request_ChangeVar *) pt_null_cmd;
	
	MODI_OnlineClient * m_pClient = MODI_SystemUser::GetInstance()->GetClient();	
	MODI_ZoneUserVar * p_var = m_pClient->m_stZoneUserVarMgr.GetVar(change->m_stGlobalVar.m_csName);
	if( !p_var)
	{
		p_var = new MODI_ZoneUserVar(change->m_stGlobalVar.m_csName);
		std::string var_attribute = "type=normal";
		if( p_var->Init(m_pClient,var_attribute.c_str()))
		{
			if( !m_pClient->m_stZoneUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}
	p_var->SetValue( change->m_stGlobalVar.m_wValue);
	MODI_ZS2S_Notify_VarChanged notify;
	notify.m_stGlobalVar = change->m_stGlobalVar;
	MODI_GameSvrClientTask::BroadcastAllServer(&notify,sizeof(MODI_ZS2S_Notify_VarChanged));

	return enPHandler_OK;
}

int MODI_PackageHandler_s2zs::MusicEnd_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
				const unsigned int cmd_size )
{
	MODI_S2ZS_Request_MusicEnd * req = (MODI_S2ZS_Request_MusicEnd *)pt_null_cmd;
	MODI_SingMusicMgr::GetInstance().OnRecvMusicInfo( &(req->m_stMusic), req->m_eMode);
	return enPHandler_OK;
}
 
/// INA购买道具申请扣款
int  MODI_PackageHandler_s2zs::INAItemConsume_Handler( void *pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	const MODI_INAItemConsume * p_recv_cmd = ( const MODI_INAItemConsume *)(pt_null_cmd);
	//MODI_GameSvrClientTask * pServer = (MODI_GameSvrClientTask *)(pObj);
	MODI_INAItemConsume * p_change_cmd = const_cast<MODI_INAItemConsume *>(p_recv_cmd);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(!pMgr)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
		
	MODI_OnlineClient * pOnlineOne = pMgr->FindByAccid(p_recv_cmd->m_dwAccountId);
	if(!pOnlineOne)
	{
		return enPHandler_OK;
	}

	/// INA是用账号名
	if(Global::g_byGameShop == 1)
	{
		strncpy(p_change_cmd->m_cstrAccName, pOnlineOne->GetAccName(), sizeof(p_change_cmd->m_cstrAccName) - 1);
	}
	else if(Global::g_byGameShop == 2)
	{
		strncpy(p_change_cmd->m_cstrAccName, pOnlineOne->GetSessionID().GetIPString(), sizeof(p_change_cmd->m_cstrAccName) - 1);
	}

	MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
	if( !pBill || pBill->IsConnected() == false )
	{
		MODI_GS2C_Notify_TransactionResult res;
		res.m_actionType = kTransaction_Buy;
		res.m_result = kTransaction_UnknowError;
		
		if(pOnlineOne)
		{
			pOnlineOne->SendPackage(&res, sizeof(res));
			return enPHandler_OK;
		}
	}
	else 
	{
		pBill->SendCmd(pt_null_cmd , cmd_size);
	}

	return enPHandler_OK;
}



/// INA赠送道具申请扣款
int  MODI_PackageHandler_s2zs::INAGiveItem_Handler( void *pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	const MODI_INAGiveItem * p_recv_cmd = ( const MODI_INAGiveItem *)(pt_null_cmd);
	MODI_INAGiveItem * p_change_cmd = const_cast<MODI_INAGiveItem *>(p_recv_cmd);


	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(!pMgr)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
		
	MODI_OnlineClient * pOnlineOne = pMgr->FindByAccid(p_recv_cmd->m_dwAccountId);
	if(!pOnlineOne)
	{
		return enPHandler_OK;
	}

	strncpy(p_change_cmd->m_cstrAccName, pOnlineOne->GetAccName(), sizeof(p_change_cmd->m_cstrAccName) - 1);

	/// 赠送加上被赠送人的accname
	std::string rece_acc_name;
	MODI_OfflineUser * pOfflineUser = MODI_OfflineUserMgr::GetInstancePtr()->FindByName(p_recv_cmd->m_cstrGiftRoleName);
	if(pOfflineUser)
	{
		rece_acc_name = pOfflineUser->GetAccName();
	}

	MODI_OnlineClient * pReceUser = pMgr->FindByName(p_recv_cmd->m_cstrGiftRoleName);
	if(pReceUser)
	{
		rece_acc_name = pReceUser->GetAccName();
	}

	if(rece_acc_name == "")
	{
		return enPHandler_OK;
	}
	
	strncpy(p_change_cmd->m_cstrGiftAccName, rece_acc_name.c_str(), sizeof(p_change_cmd->m_cstrGiftAccName) - 1);

	MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
	if( !pBill || pBill->IsConnected() == false )
	{
		MODI_GS2C_Notify_TransactionResult res;
		res.m_actionType = kTransaction_Buy;
		res.m_result = kTransaction_UnknowError;
		
		if(pOnlineOne)
		{
			pOnlineOne->SendPackage(&res, sizeof(res));
			return enPHandler_OK;
		}
	}
	else 
	{
		pBill->SendCmd(pt_null_cmd , cmd_size);
	}

	return enPHandler_OK;
}
int	MODI_PackageHandler_s2zs::ChangeVip_Handler( void * pObj, const Cmd::stNullCmd * pt_null_cmd,  const unsigned int cmd_size)
{
	MODI_S2ZS_Request_ChangeVip  * vip = (MODI_S2ZS_Request_ChangeVip *)pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();	

	MODI_OnlineClient * pclient = pMgr->FindByGUID( vip->m_dwCharid);
	if( !pclient)
	{
		Global::logger->debug("[change_vip_handler] can't find the onlineclient of the change vip one <charid=%u>",vip->m_dwCharid);
		return enPositionT_None;
	}
	pclient->GetFullDataModify().m_roleInfo.m_baseInfo.m_bVip = vip->m_bVip;

	return enPHandler_OK;
}



int	MODI_PackageHandler_s2zs::DelFriendIdol_Handler( void * pObj, const Cmd::stNullCmd * pt_null_cmd,  const unsigned int cmd_size)
{
	MODI_S2ZS_Request_DelFrindandIdole * del = (MODI_S2ZS_Request_DelFrindandIdole *)pt_null_cmd;

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient  * client = pMgr->FindByGUID(del->m_dwCharid);

	if( !client)
	{
		Global::logger->debug("[del_friend_idole] can't find the online client %d",del->m_dwCharid);	
		return enPositionT_None;
	}

	client->GetRelationList().DeleteFriendTo( del->m_nFriend);
	client->GetRelationList().DeleteIdoleTo(del->m_nIdole);
	client->SyncRelationList();
	return enPHandler_OK;
}




int  MODI_PackageHandler_s2zs::SendSysmail_Handler( void *pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_S2ZS_Request_SendSysMail  * pinfo =  (MODI_S2ZS_Request_SendSysMail *) pt_null_cmd;	
	MODI_SendMailInfo  sendmail;
	sendmail.Reset();
	strncpy( sendmail.m_szCaption , pinfo->m_szCaption , sizeof(sendmail.m_szCaption) - 1);
	strncpy( sendmail.m_szContents , pinfo->m_szContent, sizeof(sendmail.m_szContents) - 1);
	strncpy( sendmail.m_szRecevier , pinfo->m_szReceverName, sizeof(sendmail.m_szRecevier) - 1);
	sendmail.m_Type = kTextMail;
	MODI_MailCore::SendMail( sendmail );
	return enPHandler_OK;
}


int  MODI_PackageHandler_s2zs::Request_ModifyRenqi( void *pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_S2ZS_Modify_Renqi  * req = (MODI_S2ZS_Modify_Renqi * )pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient *pclient;
	MODI_ASSERT(pMgr);
	if( pMgr )
	{
		pclient = pMgr->FindByGUID( req->m_dwCharid);
		if( pclient)
		{
			pclient->AddRenqi( req->m_nRenqi);
		}
	}
	return enPHandler_OK;

}


int  MODI_PackageHandler_s2zs::Request_BroadcastSysChat( void *pObj , const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_S2ZS_Broadcast_SystemChat  * req = (MODI_S2ZS_Broadcast_SystemChat * )pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if(pMgr)
		pMgr->BroadcastSysChat(req->m_pContent, req->m_wdSize);
	return enPHandler_OK;
}









