#include "GMCommand_ZS.h"
#include "AssertEx.h"
#include "BinFileMgr.h"
#include "HelpFuns.h"
#include "protocol/c2gs.h"
#include "OnlineClient.h"
#include "MailCore.h"
#include "GameHelpFun.h"
#include "OnlineClientMgr.h"
#include "GameSvrClientTask.h"
#include "Base/s2zs_cmd.h"
#include "GameConstant.h"
#include "RankCore.h"

MODI_GMCmdHandler_zs::MODI_GMCmdHandler_zs():m_pClient(0)
{

}

MODI_GMCmdHandler_zs::~MODI_GMCmdHandler_zs()
{


}

void 	MODI_GMCmdHandler_zs::SetClient( MODI_OnlineClient * pClient )
{
	m_pClient = pClient;
}

MODI_OnlineClient * 	MODI_GMCmdHandler_zs::GetClient()
{
	return m_pClient;
}

bool MODI_GMCmdHandler_zs::FillCmdHandlerTable()
{
	static const MODI_GMCommand Commands[]=
	{
		{"-help", &(MODI_GMCmdHandler_zs::ShowHelp) ,
			"display help message" , 
			"usage: -help" },

		{"-getmailattach", &(MODI_GMCmdHandler_zs::TestGetMailAttach),
			"get all mail's attachs by self ." ,
		   	"usage: -getmailattach" },

		{"-zoneinfo", &(MODI_GMCmdHandler_zs::ShowZoneInfo) ,
			"display zone infomations " , "usage: -zoneinfo" },

		{"-kickallclients",&(MODI_GMCmdHandler_zs::KickAllClients) ,
			"kick all online clients" , "usage:-kickallclients" },

		{"-kick",&(MODI_GMCmdHandler_zs::KickClient),
			"kick one online client" , "usage:-kick rolename" },

		{"-setccnum", &(MODI_GMCmdHandler_zs::SetChannelClientNum) ,
			"set channel client num" ,
		 "usage: -setccnum maxnum " },

		{"-setattr", &(MODI_GMCmdHandler_zs::SetAttr) ,
			"set client attr" ,
		 "usage: -setattr attr_name attr_value " },
		
		{"-setcv",&(MODI_GMCmdHandler_zs::SetConstantValue) ,
			"set game constant vlaue.",
			"usage: -setcv VALUENAME NEWVALUES" },

		{"-freeze",&(MODI_GMCmdHandler_zs::FreezeClient),
			"freeze one online client" , "usage:-freeze rolename" },
		{"-addfensi",&(MODI_GMCmdHandler_zs::AddFensi),
			"add fensigirl count.",
			"usage: -addfensi  USERNAME "},
		{"-addblack",&(MODI_GMCmdHandler_zs::AddBlack),
			"add add black .",
			"usage: -addblack  USERNAME "},
		{"-decblack",&(MODI_GMCmdHandler_zs::DecBlack),
			"dec black .",
			"usage: -decblack  USERNAME "},
		{"-refreshpaim", &(MODI_GMCmdHandler_zs::RefreshPaim),
		 "refresh paim", "usage: -refreshpaim"
		},
	};
	int iCount = sizeof(Commands) / sizeof(MODI_GMCommand);

	for( int i = 0; i < iCount; i++ )
	{
		this->AddCmdHandler( Commands[i].cmd , Commands[i] );
	}

	return true;
}

// 显示帮助明
int MODI_GMCmdHandler_zs::ShowHelp( int nArgc ,const char ** szArgs )
{
	if( !nArgc )
		return enGMCmdHandler_InvalidParam;

	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );
	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}

	MODI_GS2C_Notify_GMCmdResult result;
	MAP_HANDLER_ITER itor = MODI_GMCmdHandler_zs::GetInstancePtr()->m_mapHandlers.begin();
	while( itor != MODI_GMCmdHandler_zs::GetInstancePtr()->m_mapHandlers.end() )
	{
		const MODI_GMCommand & cmd = itor->second;
		SNPRINTF( result.m_szResult ,
				sizeof(result.m_szResult) , 
				"Command: %s , Des: %s , Usage: %s ." ,
				cmd.cmd , cmd.description , cmd.usage );
		pClient->SendPackage( &result , sizeof(result) );

		itor++;
	}
	
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_zs::TestGetMailAttach( int nArgc ,const char ** szArgs )
{
#if 0	
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );


	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
//	 format: -testgetmailattach 
	if( nArgc < 1 )
		return enGMCmdHandler_InvalidParam;

	MODI_Maillist * pList = pClient->GetMaillist();
	if( !pList )
		return enGMCmdHandler_InvalidParam;

	MODI_Mail * pIter = 0;
	SLIST_FOREACH( pIter , pList->GetMailsHeader() , MailNode )
	{
		if( pIter->IsAttachmentItemMail() || 
			pIter->IsAttachmentMoneyMail()  )
		{
			// 
			MODI_MailCore::GetAttachment( pClient , pIter->GetGUID() );
		}
	}
#endif
	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_zs::ShowZoneInfo( int nArgc ,const char ** szArgs)
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	const unsigned int zone_player_num = pMgr->Size();

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult , sizeof(result.m_szResult) ,  "this zone have players(%u)",zone_player_num);
	pClient->SendPackage( &result , sizeof(result) );
	Global::logger->debug("[zone_info] this zone have players(%u)", zone_player_num);
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_zs::KickAllClients( int nArgc ,const char ** szArgs )
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}

	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	WORD channel_id = atoi(szArgs[1]);
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	
	pMgr->KickAllClients(channel_id);

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult , sizeof(result.m_szResult) ,  "kickallclients <id=%u> successful",channel_id);
	pClient->SendPackage( &result , sizeof(result) );

	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_zs::SetChannelClientNum( int nArgc ,const char ** szArgs)
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	DWORD nClientNum = 0;
	if( !safe_strtoul( szArgs[1] , &nClientNum ) )
		return enGMCmdHandler_InvalidParam;

	if(nClientNum < MODI_GameSvrClientTask::m_wdChannelMaxClient)
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult , 
				sizeof(result.m_szResult) ,  
				"set channel max client (%u-->%u) failed",
				MODI_GameSvrClientTask::m_wdChannelMaxClient, 
				nClientNum);
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_InvalidParam;
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult , 
			sizeof(result.m_szResult) ,  
			"set channel max client (%u-->%u) success",MODI_GameSvrClientTask::m_wdChannelMaxClient, nClientNum);
	pClient->SendPackage( &result , sizeof(result) );
	Global::logger->info("set channel max client (%u-->%u) success",
			MODI_GameSvrClientTask::m_wdChannelMaxClient, 
			nClientNum);
	MODI_GameSvrClientTask::m_wdChannelMaxClient = nClientNum;
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_zs::SetAttr( int nArgc ,const char ** szArgs)
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 3 )
	{
		return enGMCmdHandler_InvalidParam;
	}
	std::string attr_name = szArgs[1];
	
	const MODI_CharactarFullData & old_value = pClient->GetFullData();
	
	MODI_GS2C_Notify_GMCmdResult old_result;
	SNPRINTF(old_result.m_szResult, sizeof(old_result.m_szResult), "gm<%s> before <renqi=%u,fensiboy=%u,fensigril=%u,blockc=%u>",attr_name.c_str(),
			 old_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi,
			 old_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy,
			 old_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril,
			 pClient->GetBlockCount());
	pClient->SendPackage( &old_result , sizeof(old_result) );

	
	DWORD cmd_value = 0;
	if(attr_name == "idoldel")
	{
		if( !safe_strtoul( szArgs[2] , &cmd_value) )
		{
			return enGMCmdHandler_InvalidParam;
		}
		
		pClient->OnBeDelIdolBySB(cmd_value);// 1=box 0=gril
	}
	
	if(attr_name == "idoladd")
	{
		if( !safe_strtoul( szArgs[2] , &cmd_value) )
		{
			return enGMCmdHandler_InvalidParam;
		}
		
		pClient->OnBeAddIdolBySB(cmd_value);// 1=box 0=gril
	}
	
	else if(attr_name == "blockadd")
	{
		pClient->BlockOptDeal(enRelOpt_AddB);
	}
	else if(attr_name == "blocksub")
	{
		pClient->BlockOptDeal(enRelOpt_DelB);
	}
	else if(attr_name == "renqiadd")
	{
		MODI_CharactarFullData & m_FullData = pClient->GetFullDataModify();
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi += 1;
		pClient->SyncFensiRenqi();
	}
	else if(attr_name == "renqisub")
	{
		MODI_CharactarFullData & m_FullData = pClient->GetFullDataModify();
		if(m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi > 0)
		{
			m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi -= 1;
			pClient->SyncFensiRenqi();
		}
	}

	const MODI_CharactarFullData & new_value = pClient->GetFullData();
	
	MODI_GS2C_Notify_GMCmdResult new_result;
	SNPRINTF(new_result.m_szResult, sizeof(new_result.m_szResult), "gm<%s> after <renqi=%u,fensiboy=%u,fensigril=%u,blockc=%u>",attr_name.c_str(),
			 new_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi,
			 new_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy,
			 new_value.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril,
			 pClient->GetBlockCount());
	pClient->SendPackage( &new_result , sizeof(new_result) );
	return enGMCmdHandler_OK;
}

int MODI_GMCmdHandler_zs::KickClient( int nArgc ,const char ** szArgs )
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( pMgr )
	{
		const char * role_name = szArgs[1];
		MODI_OnlineClient * wait_be_kick = pMgr->FindByName( role_name );
		if( wait_be_kick )
		{
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
			if( pServer )
			{
				MODI_ZS2S_Notify_KickSb kickmsg;
				kickmsg.m_byReason = kAdminKick;
				kickmsg.m_guid = wait_be_kick->GetGUID();
				kickmsg.m_sessionID = wait_be_kick->GetSessionID();
				pServer->SendCmd( &kickmsg , sizeof(kickmsg) );

				MODI_GS2C_Notify_GMCmdResult result;
				SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"kick client successful <%s> outline", wait_be_kick->GetRoleName());
				pClient->SendPackage( &result , sizeof(result) );
				return enGMCmdHandler_OK;
			}
		}
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"kick client not online");
		pClient->SendPackage( &result , sizeof(result) );
	}

	return enGMCmdHandler_OK;
}



int MODI_GMCmdHandler_zs::FreezeClient( int nArgc ,const char ** szArgs )
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( pMgr )
	{
		const char * role_name = szArgs[1];
		MODI_OnlineClient * wait_be_kick = pMgr->FindByName( role_name );
		if( wait_be_kick )
		{
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( wait_be_kick->GetServerID() );
			if( pServer )
			{
				MODI_ZS2S_Notify_KickSb kickmsg;
				kickmsg.m_byReason = kAdminKick;
				kickmsg.m_guid = wait_be_kick->GetGUID();
				kickmsg.m_sessionID = wait_be_kick->GetSessionID();
				pServer->SendCmd( &kickmsg , sizeof(kickmsg) );

				MODI_GS2C_Notify_GMCmdResult result;
				SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"freeze client and kick <%s> successful", wait_be_kick->GetRoleName());
				pClient->SendPackage( &result , sizeof(result) );
	
			}
		}
	}

	return enGMCmdHandler_OK;
}


int MODI_GMCmdHandler_zs::SetConstantValue( int nArgc , const char ** szArgs )
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgc < 2 )
		return enGMCmdHandler_InvalidParam;

	using namespace MODI_GameConstant;
	const char * szValueName = szArgs[1];
	const char * szNewValue = szArgs[2];
	enConstantValueType vt;
	
	if( get_ValueType_ByName( szValueName , vt ) == false )
	{
		Global::logger->info("[%s] gm<charid=%u,name=%s> try SetConstantValue by <valuename=%s,newvalue=%s>" , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				szValueName,
				szNewValue );

		return enGMCmdHandler_InvalidParam;
	}

	if( vt == kCVT_DWORD )
	{
		DWORD n = 0;
		if( safe_strtoul(  szNewValue , &n ) )
		{
			set_DWORDValue_ByName( szValueName ,  n );
		}
	}
	else if( vt == kCVT_INT )
	{
		long i = 0;
		if( safe_strtol(  szNewValue , &i ) )
		{
			set_IntValue_ByName( szValueName ,  i );
		}
	}
	else if( vt == kCVT_FLOAT )
	{
		float f = atof( szNewValue );
		set_FloatValue_ByName( szValueName , f );
	}
	else 
	{
		MODI_ASSERT(0);
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_success] deal zone gm command successful.");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;
}
int MODI_GMCmdHandler_zs::AddFensi( int nArgs,const char **szArgs)
{

	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgs < 2 )
		return enGMCmdHandler_InvalidParam;

	std::string name = szArgs[1];
	

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pclient2 = pMgr->FindByName(name);
	if( !pclient2)
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_failed] can't find the client.");
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;

	}

	int n = 1;
	int i=0; 
	if( nArgs > 2)
	{
		n = atoi( szArgs[2]);
	}	
	for( i=0; i< n ;  ++i)
	{

		MODI_GS2C_Notify_RelationCU notify;
		notify.m_byType = enRelOpt_AddIdol;
		safe_strncpy( notify.m_szName  , pClient->GetRoleName() , sizeof(notify.m_szName) );
		pclient2->SendPackage( &notify , sizeof(notify) );

		pclient2->OnBeAddIdolBySB( pClient->GetSexType() );
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_successful] Add fen si ok");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;

}



int MODI_GMCmdHandler_zs::AddBlack( int nArgs,const char **szArgs)
{

	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgs < 2 )
		return enGMCmdHandler_InvalidParam;
	int n = 1;
	int i=0; 
	if( nArgs > 2)
	{
		n = atoi( szArgs[2]);
	}	

	std::string name = szArgs[1];
	

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pclient2 = pMgr->FindByName(name);
	if( !pclient2)
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_failed] can't find the client.");
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;

	}
	
	for( i=0; i< n; ++i )
	{
	
		MODI_GS2C_Notify_RelationCU notify;
		notify.m_byType = enRelOpt_AddB;
		safe_strncpy( notify.m_szName  , pClient->GetRoleName() , sizeof(notify.m_szName) );
		pclient2->SendPackage( &notify , sizeof(notify) );

		pclient2->BlockOptDeal( enRelOpt_AddB );
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_successful] Add black ok");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;

}


int MODI_GMCmdHandler_zs::DecBlack( int nArgs,const char **szArgs)
{

	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}
	
	if( nArgs < 2 )
		return enGMCmdHandler_InvalidParam;

	std::string name = szArgs[1];
	int n = 1;
	int i=0;
	if( nArgs > 2)
	{
		n = atoi( szArgs[2]);
	}	

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pclient2 = pMgr->FindByName(name);
	if( !pclient2)
	{
		MODI_GS2C_Notify_GMCmdResult result;
		SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_failed] can't find the client.");
		pClient->SendPackage( &result , sizeof(result) );
		return enGMCmdHandler_OK;

	}

	for( i=0; i< n ; ++i)
	{
		MODI_GS2C_Notify_RelationCU notify;
		notify.m_byType = enRelOpt_DelB;
		safe_strncpy( notify.m_szName  , pClient->GetRoleName() , sizeof(notify.m_szName) );
		pclient2->SendPackage( &notify , sizeof(notify) );

		pclient2->BlockOptDeal( enRelOpt_DelB );
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_successful] Dec black ok");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;
}


/// 刷新排名
int MODI_GMCmdHandler_zs::RefreshPaim( int nArgs,const char **szArgs)
{
	MODI_OnlineClient * pClient = MODI_GMCmdHandler_zs::GetInstancePtr()->GetClient();
	MODI_ASSERT( pClient );

	if(!( pClient->GetFullData().m_roleExtraData.m_iGMLevel > 1 ))
	{
		Global::logger->debug("[gm_level] gm level < 2 <rolename=%s>", pClient->GetRoleName());
		return enGMCmdHandler_InvalidParam;
	}

	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
	if(rank_core)
	{
		rank_core->LoadRanksFromDB();
	}

	MODI_GS2C_Notify_GMCmdResult result;
	SNPRINTF( result.m_szResult ,sizeof(result.m_szResult),"%s", "[gm_cmd_successful] refresh ok");
	pClient->SendPackage( &result , sizeof(result) );
	return enGMCmdHandler_OK;
}

























