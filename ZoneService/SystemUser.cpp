#include "SystemUser.h"
#include "DBClient.h"
#include "s2zs_cmd.h"
#include "s2rdb_cmd.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"


MODI_SystemUser * MODI_SystemUser::m_pInstance = NULL;


void MODI_SystemUser::Init()
{
	m_pClient = MODI_OnlineClientPool::New();
	if( !m_pClient	)
	{
		MODI_ASSERT(0);
	}
	else
	{
		MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();
		m_pClient->Init(0,MAKE_GUID(0,0),"system");
		m_pClient->SetAccid(0);
		m_pClient->SetAccName("system");
		m_pClient->SetGUID(MAKE_GUID(0,0));
		pOnlineMgr->OnClientLogin( m_pClient,true,false);
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
		if( !pDB )
		{
			return;
		}
		
		MODI_S2RDB_Request_ZoneUserVar_Cmd send_cmd;
		send_cmd.m_dwAccountID = m_pClient->GetAccid(); 
		pDB->SendCmd(&send_cmd, sizeof(send_cmd));
	}

}







