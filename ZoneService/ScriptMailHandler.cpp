#include "ScriptMailHandler.h"
#include <assert.h>
#include "Mail.h"
#include "MailCore.h"
#include "AssertEx.h"
#include "OnlineClient.h"

MODI_ScriptMailHandler::MODI_ScriptMailHandler()
{

}

MODI_ScriptMailHandler::~MODI_ScriptMailHandler()
{

}

void MODI_ScriptMailHandler::AddHandler(enScriptMailID type , const MODI_ScriptMail & h)
{
    MAP_INSERT_RESULT result = m_mapHandlers.insert(std::make_pair( type , h));
    assert(result.second);
    if (result.second == false)
    {
        throw "reduplicate srcipt mail handler .";
    }
}


bool 	MODI_ScriptMailHandler::Initialization()
{
	static const MODI_ScriptMail 	ScriptMails[]=
	{
		{kScriptMail_AddMail, &(MODI_ScriptMailHandler::AddMail) },
		{kScriptMail_DecIdolCount, &(MODI_ScriptMailHandler::DecIdolCount) },
		{kScriptMail_IncIdolCount, &(MODI_ScriptMailHandler::IncIdolCount) },
		{kScriptMail_GiveMail, &(MODI_ScriptMailHandler::GiveShopGoodsMail) },
		{kScriptMail_BlockOpt, &(MODI_ScriptMailHandler::BlockOptMail) },
	};
	int iCount = sizeof(ScriptMails) / sizeof(MODI_ScriptMail);

	for( int i = 0; i < iCount; i++ )
	{
		this->AddHandler( ScriptMails[i].type , ScriptMails[i] );
	}

	return true;
}

int  	MODI_ScriptMailHandler::ExcuteScript( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
	MODI_ASSERT( pClient );
    int iRet = kScriptMailHandler_NotHandler_Del;
	if( mi.IsScriptMail() && pClient )
	{
		MAP_HANDLER_ITER itor = m_mapHandlers.find( (enScriptMailID)mi.GetScriptMailID() );
		if (itor != m_mapHandlers.end())
		{
			MODI_ScriptMail & sp = (itor->second);
			if ( sp.exe_func )
			{
				iRet = sp.exe_func( pClient , mi );
			}
		}
	}

	return iRet;
}

int 	MODI_ScriptMailHandler::AddMail( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
#if 0
	if( !pClient )
		return kScriptMailHandler_NotHandler_Del;

	MODI_Maillist * pList = pClient->GetMaillist();
	if( !pList )
		return kScriptMailHandler_NotHandler;

	// 目标邮箱没有满，则发送邮件给目标
	if( pList->IsFull() == false )
	{
		MODI_SendMailInfo sm;
		MODI_MailCore::ConvertSendMailInfo( mi.GetMailInfo() , sm );
		sm.m_Type = mi.GetDBMailInfo().m_ScriptData.GetArg_AddMail().m_AddMailType;
		if( MODI_MailCore::ServerSendMailToClient( sm , pClient ) )
		{
			return kScriptMailHandler_Del;
		}
	}
	else 
	{
		pClient->SetPendingMailCount( pClient->GetPendingMailCount() + 1 );
	}
	
	return kScriptMailHandler_NotHandler;
#endif

	return kScriptMailHandler_Del;
}

int MODI_ScriptMailHandler::GiveShopGoodsMail( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
#if 0	
	if( !pClient )
		return kScriptMailHandler_NotHandler_Del;

	MODI_Maillist * pList = pClient->GetMaillist();
	if( !pList )
		return kScriptMailHandler_NotHandler;

	// 目标邮箱没有满，则发送邮件给目标
	if( pList->IsFull() == false )
	{
		MODI_SendMailInfo sm;
		MODI_MailCore::ConvertSendMailInfo( mi.GetMailInfo() , sm );
		sm.m_Type = kAttachmentItemMail;

		if( MODI_MailCore::ServerSendGiveMailToClient( sm , pClient , 
					mi.GetDBMailInfo().m_ScriptData.GetArg_ShopGiveMail().szSender ,
					mi.GetDBMailInfo().m_ScriptData.GetArg_ShopGiveMail().transid ) )
		{
			return kScriptMailHandler_Del;
		}
		return kScriptMailHandler_Del;
	}
	else 
	{
		pClient->SetPendingMailCount( pClient->GetPendingMailCount() + 1 );
	}
	
	return kScriptMailHandler_NotHandler;
#endif
	return kScriptMailHandler_Del;
}


int 	MODI_ScriptMailHandler::DecIdolCount( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
	if( !pClient )
		return kScriptMailHandler_NotHandler_Del;

	// 减少偶像个数
	pClient->OnBeDelIdolBySB( mi.GetDBMailInfo().m_ScriptData.GetArg_DecIdolCount().bySex );

	return kScriptMailHandler_Del;
}

int 	MODI_ScriptMailHandler::IncIdolCount( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
	if( !pClient )
		return kScriptMailHandler_NotHandler_Del;

	pClient->OnBeAddIdolBySB( mi.GetDBMailInfo().m_ScriptData.GetArg_IncIdolCount().bySex );

	return kScriptMailHandler_Del;
}

/** 
 * @brief 被拉黑邮件处理
 * 
 * @param pClient 被拉黑对象
 * @param mi 信息
 * 
 */
int 	MODI_ScriptMailHandler::BlockOptMail( MODI_OnlineClient * pClient , const MODI_Mail & mi )
{
	if( !pClient )
		return kScriptMailHandler_NotHandler_Del;

	pClient->BlockOptDeal((enRelOptType)(mi.GetDBMailInfo().m_ScriptData.GetArg_BlockOpt().m_byType));

	return kScriptMailHandler_Del;
}
