/**
 * @file   FamilyInvite.h
 * @author  <hurixin@localhost.localdomain>
 * @date   Thu May  5 10:42:31 2011
 * 
 * @brief  家族邀请
 * 
 */


#ifndef _MD_FAMILYINVITE_H
#define _MD_FAMILYINVITE_H

#include "InviteManager.h"
class MODI_OnlineClient;

/**
 * @brief 家族邀请
 * 
 */
class MODI_FamilyInviteData: public MODI_InviteData
{
 public:
	virtual ~MODI_FamilyInviteData(){}

	/** 
	 * @brief 初始化
	 * 
	 */
	void Init(MODI_OnlineClient * p_invite_client, MODI_OnlineClient * p_res_client);

	
	/** 
	 * @brief 发送邀请
	 * 
	 */
	virtual void Invite();

	/** 
	 * @brief 接收邀请
	 * 
	 */
	virtual void Responsion(const enOptRequestResult & result);

 private:
	/// 邀请人id
	MODI_GUID m_stInviteID;

	/// 被邀请人id
	MODI_GUID m_stResponsionID;
};


#endif
