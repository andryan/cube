/**
 * @file   FamilyInvite.cpp
 * @author  <hurixin@localhost.localdomain>
 * @date   Thu May  5 10:47:05 2011
 * 
 * @brief  家族邀请
 * 
 * 
 */


#include "FamilyInvite.h"
#include "FamilyManager.h"
#include "OnlineClientMgr.h"

/** 
 * @brief 初始化
 * 
 */
void MODI_FamilyInviteData::Init(MODI_OnlineClient * p_invite_client, MODI_OnlineClient * p_res_client)
{
	if(p_invite_client == NULL || p_res_client == NULL)
	{
		return;
	}

	SetInviteID(MODI_InviteManager::GetInstance().GenInviteID());
	SetTimeout(Global::m_stLogicRTime.GetMSec() + 1000 * 60);/// one min
	m_stInviteID = p_invite_client->GetGUID();
	m_stResponsionID = p_res_client->GetGUID();
	
	/// 加入到管理器
	if(! MODI_InviteManager::GetInstance().AddInvite(this))
	{
		return;
	}

	
	/// 直接做了
	MODI_GS2C_Notify_FamilyInvition send_cmd;
	send_cmd.m_dwInvitionID = GetInviteID();
	strncpy(send_cmd.m_szName, p_invite_client->GetRoleName(), sizeof(send_cmd.m_szName) - 1);

	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_invite_client->GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[invite_family] on free family failed,not find family <client=%s,familyid=%u>",
							  p_invite_client->GetRoleName(), p_invite_client->GetFamilyID());
		MODI_ASSERT(0);
		return;
	}
	
   	p_family->GetFamilyListInfo(send_cmd.m_stListInfo);
	
	p_res_client->SendPackage(&send_cmd, sizeof(send_cmd));
}


/** 
 * @brief 发出邀请
 * 
 */
void MODI_FamilyInviteData::Invite()
{
	
}


/** 
 * @brief 答复了
 * 
 */
void MODI_FamilyInviteData::Responsion(const enOptRequestResult & result)
{
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	
	MODI_CHARID charid_invite = GUID_LOPART(m_stInviteID);
	MODI_OnlineClient * p_invite_client = pMgr->FindByGUID(charid_invite);
	
	MODI_CHARID charid_res = GUID_LOPART(m_stResponsionID);
	MODI_OnlineClient * p_res_client = pMgr->FindByGUID(charid_res);

	if(p_invite_client == NULL || p_res_client == NULL)
	{
		return;
	}

	if(!(p_invite_client->IsFamilyMaster() || p_invite_client->IsFamilySlaver()))
	{
		return;
	}
// 	if(p_res_client->GetFamilyID() == p_invite_client->GetFamilyID())
// 	{
// 		return;
// 	}


	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_invite_client->GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[invite_family] on free family failed,not find family <client=%s,familyid=%u>",
						  p_invite_client->GetRoleName(), p_invite_client->GetFamilyID());
		MODI_ASSERT(0);
		return;
	}
		
	if(p_family->IsFull())
	{
		return;
	}
	
	if(p_res_client->IsHaveFamily())
	{
		
		MODI_FamilyManager::GetInstance().LeaveFamily(p_res_client);
	}

	MODI_GS2C_Notify_FamilyInvitionResponse send_cmd;
	strncpy(send_cmd.m_szName, p_res_client->GetRoleName(), sizeof(send_cmd.m_szName) - 1);
	send_cmd.m_enResult = result;
	p_invite_client->SendPackage(&send_cmd, sizeof(send_cmd));

	/// 直接加入家族成员
	if(result == enOptRequestAccept)
	{
		MODI_FamilyMember * p_member = new MODI_FamilyMember(p_family->GetFamilyBaseInfo(), p_res_client);
		if(! p_member->OnRequest())
		{
			delete p_member;
			p_member = NULL;
			MODI_ASSERT(0);
			return;
		}
		if(! p_member->OnAccept())
		{
			MODI_ASSERT(0);
			return;
		}

		/// 更新家族名片
		p_family->UpDateFamilyCard(p_member, enAccept_Reason);

		std::ostringstream os;
		os<<"Welcome" << p_member->GetRoleName() << "join our family";
		p_family->BroadcastSysChatExRequest(os.str().c_str(), os.str().size());
	
		p_member->SendMeToFamily();
		p_member->SendFamilyToMe(enReason_JoinFamily);
	}
}
