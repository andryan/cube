#include "PackageHandler_rdb2zs.h"
#include "PackageHandler_c2zs.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "s2zs_cmd.h"
#include "protocol/c2gs.h"
#include "Base/HelpFuns.h"
#include "Base/AssertEx.h"
#include "OfflineUserMgr.h"
#include "OnlineClientMgr.h"
#include "Global.h"
#include "GameSvrClientTask.h"
#include "DBProxyClientTask.h"
#include "PackageHandler_zs2ms.h"
#include "World.h"
#include "MailCore.h"
#include "RankCore.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"


MODI_PackageHandler_rdb2zs::MODI_PackageHandler_rdb2zs()
{

}

MODI_PackageHandler_rdb2zs::~MODI_PackageHandler_rdb2zs()
{

}

bool MODI_PackageHandler_rdb2zs::FillPackageHandlerTable()
{
	AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_RegDBProxy::ms_SubCmd ,
		&MODI_PackageHandler_rdb2zs::Register_Handler );

	// rdb 返回角色的详细信息
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_RoleDetailInfo::ms_SubCmd ,
		&MODI_PackageHandler_rdb2zs::RoleDetailInfo_Handler );

	// rdb 返回角色的所有数据
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_LoadCharData::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::LoadCharData_Handler);

	// rdb 返回角色列表
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_Charlist::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::LoadCharlist_Handler);

	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_ItemSerial::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);

	/// 直接把变量交给gameservice
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Return_LoadUserVar_Cmd::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);

	/// 直接把包裹信息返回gameservice
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_ItemInfo::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);

	/// 网页道具直接返回给gameservice
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Ret_WebItem::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);

	/// 网页赠送道具
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Ret_WebPresent::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);
	
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_ExchangeByYunyingKeyRes::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DirectionToGameServer);

	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_LoadMaillist::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::LoadMaillist_Handler);

	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_LoadRanksRes::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::LoadRankResult_Handler);

	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_LoadSelfRanksRes::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::LoadSelfRankResult_Handler );

	AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_ClearZero::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::SongMoney_Handler );

	/// 加载zone_user_var
	AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Return_ZoneUserVar_Cmd::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::ZoneUserVar_Handler);

	/// 减少钱
	AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_DecMoney_Cmd::ms_SubCmd,
		&MODI_PackageHandler_rdb2zs::DecMoney_Handler);

	/// 家族升级成功
	AddPackageHandler( MAINCMD_S2RDB, MODI_RDB2S_Notify_FamilyLevel::ms_SubCmd,
					   &MODI_PackageHandler_rdb2zs::FamilyLevelResult);

	///加载社会关系数据
	AddPackageHandler( MAINCMD_S2RDB, MODI_RDB2S_RelationLoad::ms_SubCmd,
					   &MODI_PackageHandler_rdb2zs::RelationList_Handler);


	/// gm条件发送邮件
	AddPackageHandler(MAINCMD_S2RDB, MODI_SendMailByCondition_Cmd::ms_SubCmd, &SendMailByCondition_Handler);
		
	return true;
}

int MODI_PackageHandler_rdb2zs::Register_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2ZS_Request_RegDBProxy * pMsg = (const MODI_S2ZS_Request_RegDBProxy *)(pt_null_cmd);
	MODI_DBProxyClientTask * pDBProxy = (MODI_DBProxyClientTask *)(pObj);
	pDBProxy->SetDBProxyType( pMsg->m_type );

	Global::logger->info("[%s] dbproxy server request to register , type<%s>" , 
			SERVER_REGISTER ,
			GetDBProxyTypeStringFormat( pMsg->m_type ) );

	if( pMsg->m_type == kGameServerProxy )
	{
		for( BYTE i = 1; i < 255; i++ )
		{
			if( pMsg->m_servers[i] > 0 )
			{
				if( !MODI_DBProxyClientTask::RegServer( i , pDBProxy ) )
				{
					Global::logger->error("[%s] dbproxy<type=%s,gameserver_id<%u>> register faild. disconnect this connection." , 
						SERVER_REGISTER ,
						GetDBProxyTypeStringFormat( pDBProxy->GetDBProxyType() ),
					   	i);

					pDBProxy->Terminate();
					return enPHandler_Kick;
				}
			}
		}
	}
	else if( pMsg->m_type == kZoneServerProxy )
	{
		if( !MODI_DBProxyClientTask::RegServer( 0 , pDBProxy ) )
		{
			Global::logger->error("[%s] dbproxy<type=%s> register faild. disconnect this connection." , 
					SERVER_REGISTER ,
					GetDBProxyTypeStringFormat( pDBProxy->GetDBProxyType() ) );

			pDBProxy->Terminate();
		}
		else 
		{
			if( MODI_MailCore::GetLastMailID() == INVAILD_DBMAIL_ID )
			{
				MODI_MailCore::SetLastMailID( pMsg->m_lastMailID );
			}

			MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
			if( rank_core  )
			{
				rank_core->LoadRanksFromDB();
			}
		}
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_rdb2zs::RoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_RDB2S_Notify_RoleDetailInfo * pMsg = (MODI_RDB2S_Notify_RoleDetailInfo *)(pt_null_cmd);
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( pMsg->m_bCharExist == false)
	{
		// 添加的联系人不存在
		if( pMsg->m_opt == kReqRelationOpt )
		{
			MODI_GS2C_Notify_AddRelationRes res;
			res.m_byResult = enRelationRetT_Add_NotExist;
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				pUser->SendPackage( &res , sizeof(res) );
			}
		}
		else if( pMsg->m_opt == kReqRoleDetail )
		{
			// 不存在角色
			MODI_GS2C_Notify_RoleDetailInfo notify;
			notify.m_byResult = MODI_GS2C_Notify_RoleDetailInfo::kNotExist;
			notify.m_namecard = pMsg->m_namecard;
			notify.m_bVip = pMsg->m_bVip;
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				pUser->SendPackage( &notify , sizeof(notify) );
			}
		}
		else if( pMsg->m_opt == kReqShopGiveGood )
		{
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(! pUser)
			{
				return enPHandler_OK;
			}
			
			MODI_ZS2S_Notify_QueryHasCharResult_Shop res;
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pUser->GetServerID() );
			if( pServer )
			{ 
				res.m_nSize = 0;
				res.m_bHashChar = false;
				res.m_queryClient = pUser->GetCharID();
				res.m_type = kQueryChar_Shop_Give;
				res.m_nServerID = pUser->GetServerID();
				safe_strncpy( res.m_szRoleName  , pMsg->m_namecard.m_szName , sizeof(res.m_szRoleName) );
				pServer->SendCmd( &res , sizeof(res) );
			}
		}
		else if( pMsg->m_opt == kReqRankSpecific )
		{
			MODI_ReqRoleDetail_RankSpecific_ExtraData * extra_data = (MODI_ReqRoleDetail_RankSpecific_ExtraData *)(pMsg->m_szExtra);
			MODI_GS2C_Notify_SpecificRank res;
			res.result = kRankSpecificFaild;
			res.rank_no = extra_data->rank_no;
			res.rank_type = extra_data->rank_type;
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				pUser->SendPackage( &res , sizeof(res) );
			}
		}
		else if(pMsg->m_opt == kReqPayAction)
		{
			DWORD * p_startaddr = (DWORD *)(&(pMsg->m_szExtra[0]));
			DWORD m_enReason = *p_startaddr;
			p_startaddr++;
			DWORD m_dwPayMoney = *p_startaddr;
			
			Global::logger->debug("[pay_active_not_user] not find user in db <accid=%u,reason=%u,money=%u>", pMsg->m_dwAccId,
								  m_enReason, m_dwPayMoney);
		}
		else if(pMsg->m_opt == kSendOfflineMails)
		{
			MODI_GS2C_Notify_SendMailRes resSend;
			resSend.m_result = MODI_GS2C_Notify_SendMailRes::kNoChar;
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				pUser->SendPackage( &resSend , sizeof(resSend) );
			}
		}
		else if(pMsg->m_opt == kSendGMMails)
		{
			/// 告知用户不存在
			const MODI_Unify2S_SendMail_Cmd * p_recv_cmd = (const MODI_Unify2S_SendMail_Cmd *)pMsg->m_szExtra;
			MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
			strncpy(send_cmd.m_strName, p_recv_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);
			
			send_cmd.m_stSessionID = p_recv_cmd->m_stSessionID;
			send_cmd.m_byValue = 2;
			
			MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
			if(pMc)
			{
				pMc->SendCmd(&send_cmd, sizeof(send_cmd));
			}
		}

		return enPHandler_OK;
	}
	else 
	{
		// 加入到离线数据管理器
		MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
		MODI_OfflineUser * pNode = pOfflineMgr->AddNode( pMsg->m_namecard.m_szName );
		if(! pNode )
		{
			Global::logger->warn("[load_data] load data failed <reason=%u,accid=%u>",pMsg->m_opt, pMsg->m_dwAccId);
			MODI_ASSERT(0);
			return enPHandler_OK;
		}

		pNode->InitNameCard( pMsg->m_charid , pMsg->m_dwAccId, pMsg->m_namecard );


		/// 家族名片
		MODI_FamilyMember * p_member = MODI_FamilyManager::GetInstance().FindFamilyMember(pNode->GetName());
		if(p_member)
		{
			MODI_Family  * p_family = MODI_FamilyManager::GetInstance().FindFamily( p_member->GetFamilyID());
			if( p_family)
			{

			MODI_FamilyCard card;
			card.m_stGuid = p_member->GetGuid();
			card.m_stPosition = p_member->GetPosition();
			card.m_wLevel = p_family->GetFamilyLevel();
			memset(card.m_cstrFamilyName, 0 ,sizeof(card.m_cstrFamilyName));
			strncpy(card.m_cstrFamilyName, p_member->GetFamilyName(), sizeof(card.m_cstrFamilyName) - 1);
			pNode->InitFamilyCard(card);	
			}
			else
			{
				Global::logger->fatal("[family_card_info] Can't find the familys <char=%s,family=%s>",pNode->GetName(),p_member->GetFamilyName());
			}
		}
		
		if( pMsg->m_opt == kReqRoleDetail )
		{
			// 请求详细信息的处理
			MODI_GS2C_Notify_RoleDetailInfo notify;
			MODI_OfflineUser * p2 = pNode;
			notify.m_namecard.m_avatar = p2->GetAvatarData();
			notify.m_namecard.m_byLevel = p2->GetLevel();
			notify.m_namecard.m_bySex = p2->GetSex();
			safe_strncpy( notify.m_namecard.m_szName , p2->GetName() , sizeof(notify.m_namecard.m_szName) );
			notify.m_namecard.m_RoleDetail = p2->GetDetailData();
			notify.m_namecard.m_nDefaultAvatarIdx = p2->GetDefaultAvatar();
			notify.m_namecard.m_wdChenghao = p2->GetChenghao();
			notify.m_namecard.m_stFamilyCard = p2->GetFamilyCard();
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				pUser->SendPackage( &notify , sizeof(notify) );
			}
		}
		else if( pMsg->m_opt == kReqRelationOpt )
		{
			// 离线添加联系人的处理

			// to do .. 暂时那么做

			// 构造假数据包
			MODI_C2GS_Request_AddRelation  fakeMsg;
			safe_strncpy( fakeMsg.m_szTargetName , pMsg->m_namecard.m_szName, sizeof(fakeMsg.m_szTargetName) );
			fakeMsg.m_type = pMsg->m_RelationType;

			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(pUser)
			{
				// 处理离线添加
				MODI_PackageHandler_c2zs::AddRelation_Handler( pUser , &fakeMsg , sizeof(fakeMsg) );
			}
		}
		else if( pMsg->m_opt == kReqShopGiveGood )
		{
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(! pUser)
			{
				return enPHandler_OK;
			}
			
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pUser->GetServerID() );
			if( pServer )
			{
				char szPackageBuf[Skt::MAX_USERDATASIZE];
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				MODI_ZS2S_Notify_QueryHasCharResult_Shop * res = (MODI_ZS2S_Notify_QueryHasCharResult_Shop *)(szPackageBuf);
				AutoConstruct(res);
				res->m_bHashChar = true;
				res->m_nServerID = pUser->GetServerID();
				res->m_queryClient = pUser->GetCharID();
				res->m_type = kQueryChar_Shop_Give;
				res->m_sex = pMsg->m_namecard.m_bySex;
				res->m_nSize = pMsg->m_nExtraSize;
				memcpy( res->m_pExtraData , pMsg->m_szExtra , pMsg->m_nExtraSize );
				safe_strncpy( res->m_szRoleName , pMsg->m_namecard.m_szName , sizeof(res->m_szRoleName) );
				int iSendSize = sizeof(MODI_ZS2S_Notify_QueryHasCharResult_Shop) + res->m_nSize * sizeof(char);
				pServer->SendCmd( res , iSendSize );
			}
		}
		else if( pMsg->m_opt == kReqRankSpecific )
		{
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(! pUser)
			{
				return enPHandler_OK;
			}
			
			MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
			MODI_ReqRoleDetail_RankSpecific_ExtraData * extra_data = (MODI_ReqRoleDetail_RankSpecific_ExtraData *)(pMsg->m_szExtra);
			rank_core->SyncOfflineClientSpecificRank( pUser , extra_data->rank_type , extra_data->rank_no , pNode );
		}
		/// 充值活动发送邮件
		else if(pMsg->m_opt == kReqPayAction)
		{
			BYTE client_sex = pMsg->m_namecard.m_bySex;
			std::string client_name = pMsg->m_namecard.m_szName;
			DWORD * p_startaddr = (DWORD *)(&(pMsg->m_szExtra[0]));
			DWORD m_enReason = *p_startaddr;
			p_startaddr++;
			DWORD m_dwPayMoney = *p_startaddr;
			DWORD accid = pMsg->m_dwAccId;

			if(m_enReason == enMRefreshMoney_None)
			{
				Global::logger->debug("[mrefresh_pay_return_direct] pay client online or offline, but not can get gift, so return <accid=%llu>", accid);
				return enPHandler_OK;
			}
		
			MODI_SendMailInfo  send_pay_mail;
			send_pay_mail.Reset();
			std::string content = "恭喜您获取充值活动礼包";
			std::string caption = "甜蜜盛典";
			strncpy( send_pay_mail.m_szCaption, caption.c_str(), sizeof(send_pay_mail.m_szCaption) - 1);
			strncpy( send_pay_mail.m_szContents , content.c_str(), sizeof(send_pay_mail.m_szContents) - 1);
			strncpy( send_pay_mail.m_szRecevier , client_name.c_str(), sizeof(send_pay_mail.m_szRecevier) - 1);
			send_pay_mail.m_Type = kAttachmentItemMail;

			if(m_enReason == enMRefreshMoney_Pay20)
			{
				if(client_sex == 1)
				{	
					send_pay_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54132, 1, enForever);
					send_pay_mail.m_byExtraCount = 1;
				}
				else
				{
					send_pay_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54133, 1, enForever);
					send_pay_mail.m_byExtraCount = 1;
				}
				
				Global::logger->debug("[have_get_pay] is pay20 <accid=%u,paymoney=%u>",
									  accid, m_dwPayMoney);
			}
			else if(m_enReason == enMRefreshMoney_Pay30)
			{
				
				Global::logger->debug("[have_get_pay] is pay30 <accid=%u,paymoney=%u>",
									  accid, m_dwPayMoney);
			}
			else if(m_enReason == enMRefreshMoney_Pay60)
			{
			}
			else if(m_enReason == enMRefreshMoney_Pay100)
			{
				Global::logger->debug("[have_get_pay] is pay100 <accid=%u,paymoney=%u>",
									  accid, m_dwPayMoney);
			}
			else if(m_enReason == enMRefreshMoney_Pay200)
			{
				Global::logger->debug("[have_get_pay] is pay200 <accid=%u,paymoney=%u>",
									  accid, m_dwPayMoney);
			}
			else if(m_enReason == enMRefreshMoney_Pay500)
			{
				if(client_sex == 1)
				{	
					send_pay_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(4282, 1, 3);
				}
				else
				{
					send_pay_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(54126, 1, enForever);
					send_pay_mail.m_byExtraCount = 1;
					MODI_MailCore::SendMail(send_pay_mail);
					Global::logger->debug("[have_get_pay] is  pay than 10000 <accid=%u,paymoney=%u>",
									  accid, m_dwPayMoney);
				}
			}
			else
			{
				Global::logger->error("[pay_get_gitf_error] get a gitf error <reason=%u,accid=%u>", m_enReason, accid);
				MODI_ASSERT(0);
				return enPHandler_OK;
			}
		}
		
		else if(pMsg->m_opt == kSendOfflineMails)
		{
			MODI_OnlineClient * pUser = pMgr->FindByGUID( pMsg->m_reqCharid );
			if(! pUser)
			{
				return enPHandler_OK;
			}
			
			MODI_MailCore::ClientSendMailToClient(pUser, (const MODI_SendMailInfo & )(pMsg->m_szExtra[0]) );
		}
		else if(pMsg->m_opt == kSendGMMails)
		{
			const MODI_Unify2S_SendMail_Cmd * p_recv_cmd = (const MODI_Unify2S_SendMail_Cmd *)pMsg->m_szExtra;
			MODI_Unify2S_SendMail_Cmd * p_change_cmd = const_cast<MODI_Unify2S_SendMail_Cmd *>(p_recv_cmd);
			MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
			MODI_OfflineUser * p_offclient = NULL;
			if(p_recv_cmd->m_byType == 1)
			{
				p_offclient = pOfflineMgr->FindByName(p_recv_cmd->m_strName);
			}
			else if(p_recv_cmd->m_byType == 2)
			{
				p_offclient = pOfflineMgr->FindByAccName(p_recv_cmd->m_strName);
			}
			else if(p_recv_cmd->m_byType == 3)
			{
				DWORD accid = atoi(p_recv_cmd->m_strName);
				p_offclient = pOfflineMgr->FindByAccId(accid);
			}

			strncpy(p_change_cmd->m_strName, p_offclient->GetName(), sizeof(p_change_cmd->m_strName) - 1);
				
			MODI_PackageHandler_zs2ms::CallGMSendMail_Handler(NULL,p_recv_cmd, pMsg->m_nExtraSize );
		}
	}
		
	return enPHandler_OK;
}



/** 
 * @brief 数据库把角色数据发送给zone,zone加载用户用户
 * 
 */
int MODI_PackageHandler_rdb2zs::LoadCharData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_LoadCharData * pMsg = (const MODI_RDB2S_Notify_LoadCharData *)(pt_null_cmd);
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pMsg->m_nServerID );
	if( !pServer )
	{
		Global::logger->warn("[%s] client <accid=%u,charid=%u> load data from DB<result=%u> , but gs<serverid=%u> not exist. " , 
							ROLEDATA_OPT,
							pMsg->m_nAccountID,
							pMsg->m_nCharID ,
							pMsg->m_bySuccessful ,
							pMsg->m_nServerID );
		return enPHandler_OK;
	}

	MODI_ZS2S_Notify_EnterChannelRet msg;
	msg.m_sessionID = pMsg->m_nSessionID;
	msg.m_bySuccessful = pMsg->m_bySuccessful;
	msg.m_accid = pMsg->m_nAccountID;
	if( !pMsg->m_bySuccessful )
	{
		Global::logger->warn("[%s] client <accid=%u,charid=%u> load data from DB faild. " , 
							ROLEDATA_OPT,
							pMsg->m_nAccountID,
							pMsg->m_nCharID );
		pServer->SendCmd( &msg , sizeof(msg) );
	}
	else 
	{
		MODI_OnlineClient * pClient = MODI_OnlineClientPool::New();
		if( !pClient )
		{
			MODI_ASSERT(0);
			msg.m_bySuccessful = 0;
			pServer->SendCmd( &msg , sizeof(msg) );
		}
		else 
		{
			MODI_World * pWorld = MODI_World::GetInstancePtr();
			MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_OnlineClient * pAlreadyOn = pOnlineMgr->FindByGUID( pMsg->m_nCharID );
			if( pAlreadyOn )
			{
				// 在线，需要踢人下线
				MODI_ZS2S_Notify_KickSb kickmsg;
				kickmsg.m_byReason = kAntherLoginImmediatelyKick;
				kickmsg.m_guid = pAlreadyOn->GetGUID();
				kickmsg.m_sessionID = pAlreadyOn->GetSessionID();
				pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				pWorld->OnClientLoginout( pAlreadyOn );
			}

			MODI_GUID clientguid = MAKE_GUID( pMsg->m_nCharID , enObjType_Player );
			MODI_CharactarFullData * pModifyFullData = (MODI_CharactarFullData *)(&(pMsg->m_CharFullData));
			pModifyFullData->m_roleInfo.m_baseInfo.m_roleID = clientguid;

			pClient->Init( pMsg->m_nAccountID ,  clientguid , 
					pMsg->m_CharFullData.m_roleInfo.m_baseInfo.m_cstrRoleName );
			pClient->OnLoadFromDB( pMsg->m_CharFullData );
			if(pMsg->m_dwSize > 0)
			{
				pClient->BinLoadFromDB(pMsg->m_Data, pMsg->m_dwSize);
			}
			/// 把accname赋给此用户
			pClient->SetAccName(pMsg->m_cstrAccName);
			pClient->SetServerID( pServer->GetServerID() );
			pClient->SetServerName( pServer->GetServerName() );
			pClient->SetGameState( kInLobby );
			pClient->SetSessionID( pMsg->m_nSessionID );

			msg.m_fullData  = pClient->GetFullData();
//			msg.m_fullData.m_roleInfo.m_baseInfo.m_roleID = clientguid;
			pServer->SendCmd( &msg , sizeof(msg) );
			

			pWorld->OnClientLogin( pClient );
			pClient->OnEnterChannel();

			/// 用户登陆的游戏频道
			pClient->OnLoginGameService();
			pClient->SwitchToInWorldPlayingState();
			pClient->UpdateLastLoginInfo();
		}
	}
	return enPHandler_OK;
}

#if 0
int MODI_PackageHandler_rdb2zs::LoadCharData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_LoadCharData * pMsg = (const MODI_RDB2S_Notify_LoadCharData *)(pt_null_cmd);
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pMsg->m_nServerID );
	if( !pServer )
	{
		Global::logger->warn("[%s] client <accid=%u,charid=%u> load data from DB<result=%u> , but gs<serverid=%u> not exist. " , 
							ROLEDATA_OPT,
							pMsg->m_nAccountID,
							pMsg->m_nCharID ,
							pMsg->m_bySuccessful ,
							pMsg->m_nServerID );
		return enPHandler_OK;
	}

	MODI_ZS2S_Notify_EnterChannelRet msg;
	msg.m_sessionID = pMsg->m_nSessionID;
	msg.m_bySuccessful = pMsg->m_bySuccessful;
	msg.m_accid = pMsg->m_nAccountID;
	if( !pMsg->m_bySuccessful )
	{
		Global::logger->warn("[%s] client <accid=%u,charid=%u> load data from DB faild. " , 
							ROLEDATA_OPT,
							pMsg->m_nAccountID,
							pMsg->m_nCharID );
		pServer->SendCmd( &msg , sizeof(msg) );
	}
	else 
	{
		MODI_OnlineClient * pClient = MODI_OnlineClientPool::New();
		if( !pClient )
		{
			MODI_ASSERT(0);
			msg.m_bySuccessful = 0;
			pServer->SendCmd( &msg , sizeof(msg) );
		}
		else 
		{
			MODI_World * pWorld = MODI_World::GetInstancePtr();
			MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_OnlineClient * pAlreadyOn = pOnlineMgr->FindByGUID( pMsg->m_nCharID );
			if( pAlreadyOn )
			{
				// 在线，需要踢人下线
				MODI_ZS2S_Notify_KickSb kickmsg;
				kickmsg.m_byReason = kAntherLoginImmediatelyKick;
				kickmsg.m_guid = pAlreadyOn->GetGUID();
				kickmsg.m_sessionID = pAlreadyOn->GetSessionID();
				pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				pWorld->OnClientLoginout( pAlreadyOn );
			}

			MODI_GUID clientguid = MAKE_GUID( pMsg->m_nCharID , enObjType_Player );
			MODI_CharactarFullData * pModifyFullData = (MODI_CharactarFullData *)(&(pMsg->m_CharFullData));
			pModifyFullData->m_roleInfo.m_baseInfo.m_roleID = clientguid;

			pClient->Init( pMsg->m_nAccountID ,  clientguid , 
					pMsg->m_CharFullData.m_roleInfo.m_baseInfo.m_cstrRoleName );
			pClient->OnLoadFromDB( pMsg->m_CharFullData );
			if(pMsg->m_dwSize > 0)
			{
				pClient->BinLoadFromDB(pMsg->m_Data, pMsg->m_dwSize);
			}
						
			/// 把accname赋给此用户
			pClient->SetAccName(pMsg->m_cstrAccName);
			pClient->SetServerID( pServer->GetServerID() );
			pClient->SetServerName( pServer->GetServerName() );
			pClient->SetGameState( kInLobby );
			pClient->SetSessionID( pMsg->m_nSessionID );

			msg.m_fullData  = pClient->GetFullData();
//			msg.m_fullData.m_roleInfo.m_baseInfo.m_roleID = clientguid;
			pServer->SendCmd( &msg , sizeof(msg) );
			

			pWorld->OnClientLogin( pClient );
			pClient->OnEnterChannel();
			pClient->SwitchToInWorldPlayingState();

			/// 用户登陆的游戏频道
			pClient->OnLoginGameService();
			pClient->UpdateLastLoginInfo();
		}
	}
	return enPHandler_OK;
}
#endif
	
int MODI_PackageHandler_rdb2zs::LoadCharlist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_Charlist * pMsg = (const MODI_RDB2S_Notify_Charlist *)(pt_null_cmd);

	Global::logger->info("[%s] recv client<session=%s,accid=%u,successful=%u,rolecount=%u>'s characters list from db." , 
			LOGIN_OPT ,
			pMsg->m_nSessionID.ToString(),
			pMsg->m_nAccountID,
			pMsg->m_bySuccessful,
			pMsg->m_nRoleCount );

	if( !pMsg->m_bySuccessful )
	{
		MODI_PackageHandler_zs2ms::SendCharlist( pMsg->m_nAccountID , pMsg->m_nSessionID , 
				0 , 0 , pMsg->m_bySuccessful );
		return enPHandler_OK;
	}

//	if( pMsg->m_nRoleCount > 0 )
//	{
//		// 有角色，登入游戏
//		MODI_World * pWorld = MODI_World::GetInstancePtr();
//		pWorld->OnClientLogin( pMsg->m_nAccountID , pMsg->m_pData[0].m_nCharID , pMsg->m_pData[0].m_szRoleName );
//	}

	// 下发角色列表
	MODI_PackageHandler_zs2ms::SendCharlist( pMsg->m_nAccountID , pMsg->m_nSessionID , 
											 pMsg->m_nRoleCount , &(pMsg->m_pData) , pMsg->m_bySuccessful );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_rdb2zs::DirectionToGameServer( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
#ifdef _HRX_DEBUG
	Global::logger->debug("[roledb_to_game] recv a command from dbproxy_gameservice to gameservice <%d,%d>", pt_null_cmd->byCmd, pt_null_cmd->byParam);
#endif	
	if( pt_null_cmd->byCmd == MAINCMD_S2RDB ) 
	{
		if( pt_null_cmd->byParam == MODI_RDB2S_Notify_ItemSerial::ms_SubCmd )
		{
			const MODI_RDB2S_Notify_ItemSerial * pMsg = (const MODI_RDB2S_Notify_ItemSerial *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pMsg->m_byServerID );
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction itemserial to gameserver<%u> faild." , 
						SVR_TEST ,
						pMsg->m_byServerID );
			}
		}
		else if( pt_null_cmd->byParam == MODI_RDB2S_Notify_ExchangeByYunyingKeyRes::ms_SubCmd )
		{
			const MODI_RDB2S_Notify_ExchangeByYunyingKeyRes * pRes = (const MODI_RDB2S_Notify_ExchangeByYunyingKeyRes *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_serverid );
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction exchange by yy key  to gameserver<%u> faild." , 
						SVR_TEST ,
						pRes->m_serverid );
			}
		}
		/// 用户变量
		else if( pt_null_cmd->byParam == MODI_RDB2S_Return_LoadUserVar_Cmd::ms_SubCmd )
		{
			const MODI_RDB2S_Return_LoadUserVar_Cmd * pRes = (const MODI_RDB2S_Return_LoadUserVar_Cmd *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_byServerID);
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction load user var  to gameserver<%u> faild" , SVR_TEST ,pRes->m_byServerID);
				MODI_ASSERT(0);
			}
		}
		/// 包裹信息
		else if(pt_null_cmd->byParam == MODI_RDB2S_Notify_ItemInfo::ms_SubCmd)
		{
			const MODI_RDB2S_Notify_ItemInfo * pRes = (const MODI_RDB2S_Notify_ItemInfo *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_byServerID);
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction notify item info  to gameserver<%u> faild" , SVR_TEST ,pRes->m_byServerID);
				MODI_ASSERT(0);
			}
		}
		/// 网页道具
		else if(pt_null_cmd->byParam == MODI_RDB2S_Ret_WebItem::ms_SubCmd)
		{
			const MODI_RDB2S_Ret_WebItem * pRes = (const MODI_RDB2S_Ret_WebItem *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_byServerId);
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction notify web item to gameserver<%u> faild" , SVR_TEST ,pRes->m_byServerId);
				MODI_ASSERT(0);
			}
		}

		/// 网页赠送道具
		else if(pt_null_cmd->byParam == MODI_RDB2S_Ret_WebPresent::ms_SubCmd)
		{
			const MODI_RDB2S_Ret_WebPresent * pRes = (const MODI_RDB2S_Ret_WebPresent *)(pt_null_cmd);
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_byServerId);
			if( pServer )
			{
				pServer->SendCmd( pt_null_cmd , cmd_size );
			}
			else
			{
				Global::logger->info("[%s] direction notify web present to gameserver<%u> faild" , SVR_TEST ,pRes->m_byServerId);
				MODI_ASSERT(0);
			}
		}
		
		// /// 角色家族信息
// 		else if( pt_null_cmd->byParam == MODI_RDB2S_Return_LoadCFamilyInfo_Cmd::ms_SubCmd )
// 		{
// 			const MODI_RDB2S_Return_LoadCFamilyInfo_Cmd * pRes = (const MODI_RDB2S_Return_LoadCFamilyInfo_Cmd *)(pt_null_cmd);
// 			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pRes->m_byServerID);
// 			/// onlineclient也要一份
// 			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
// 			MODI_OnlineClient * pOnline = pMgr->FindByAccid(pRes->m_dwAccountID);
// 			if(pOnline)
// 			{
// 				/// 成员表信息更新
// 				m_stFMInfo = pRes->m_stFMInfo;
// 				m_stFMInfo.m_Guid = GetGUID();
// 				strcpy(m_stFMInfo.m_szName, GetRoleName(), sizeof(m_stFMInfo.m_szName) -1 );
// 				m_bySex = GetSexType();
// 				m_enState = enMemStateT_Free;
// 				m_stLastlogin = GetFullData().m_roleExtraData.m_nLastLoginTime;

// 			}
// 			else
// 			{
// 				MODI_ASSERT(0);
// 			}
			
// 			if( pServer )
// 			{
// 				pServer->SendCmd( pt_null_cmd , cmd_size );
// 			}
// 			else
// 			{
// 				Global::logger->info("[%s] direction load character family  to gameserver<%u> faild" , SVR_TEST ,pRes->m_byServerID);
// 				MODI_ASSERT(0);
// 			}
// 		}
		
	}
	return enPHandler_OK;
}

/** 
 * @brief 数据库返回邮件列表,只登陆才会走这个流程，其他走其他的流程
 * 
 */
int MODI_PackageHandler_rdb2zs::LoadMaillist_Handler( void * pObj, const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	const MODI_RDB2S_Notify_LoadMaillist * pMsg = (const MODI_RDB2S_Notify_LoadMaillist *)(pt_null_cmd);

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByName( pMsg->m_szName );
	if(! pClient)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}

	/// 初始化邮件并通知客户端
	pClient->InitMail(pMsg->m_mails);
	return enPHandler_OK;
	
#if 0
	
	MODI_Maillist * plist = 0;
	if( pMsg->m_result == MODI_RDB2S_Notify_LoadMaillist::kSuccessful )
	{
		MODI_MailCore::OnLoadMaillistFromDB( pMsg->m_szName , pMsg->m_mails, &plist);
	}
	else
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	MODI_MailCore::LoadMailsFromDB(pClient, plist);
	return enPHandler_OK;
#endif	
	

}

int MODI_PackageHandler_rdb2zs::LoadRankResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_LoadRanksRes * pRes = (const MODI_RDB2S_Notify_LoadRanksRes *)(pt_null_cmd);
	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
	if( pRes->type == kRank_Level )
	{
		const MODI_DBRank_Level_List * list = (const MODI_DBRank_Level_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_Level , pRes->version );
		rank_core->SetRankDataValid( kRank_Level , true );
		rank_core->UpdateLevelRank( *list );
	}
	else if( pRes->type == kRank_Renqi )
	{
		const MODI_DBRank_Renqi_List * list = (const MODI_DBRank_Renqi_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_Renqi , pRes->version );
		rank_core->SetRankDataValid( kRank_Renqi , true );
		rank_core->UpdateRenqiRank( *list );
	}
	else if( pRes->type == kRank_Consume )
	{
		const MODI_DBRank_ConsumeRMB_List * list = (const MODI_DBRank_ConsumeRMB_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_Consume , pRes->version );
		rank_core->SetRankDataValid( kRank_Consume , true );
		rank_core->UpdateConsumeRMBRank( *list );
	}
	else if( pRes->type == kRank_HighestScore )
	{
		const MODI_DBRank_HighestScore_List * list = (const MODI_DBRank_HighestScore_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_HighestScore , pRes->version );
		rank_core->SetRankDataValid( kRank_HighestScore , true );
		rank_core->UpdateHighestScoreRank( *list );
	}
	else if( pRes->type == kRank_HighestPrecision )
	{
		const MODI_DBRank_HighestPrecision_List * list = (const MODI_DBRank_HighestPrecision_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_HighestPrecision , pRes->version );
		rank_core->SetRankDataValid( kRank_HighestPrecision , true );
		rank_core->UpdateHighestPrecisionRank( *list );
	}

	else if( pRes->type == kRank_KeyScore)
	{
		const MODI_DBRank_HighestScore_List * list = (const MODI_DBRank_HighestScore_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_KeyScore, pRes->version );
		rank_core->SetRankDataValid(kRank_KeyScore, true );
		rank_core->UpdateKeyHighestScoreRank( *list );
	}
	else if( pRes->type == kRank_KeyPrecision)
	{
		const MODI_DBRank_HighestPrecision_List * list = (const MODI_DBRank_HighestPrecision_List *)(pRes->data);
		rank_core->SetRankVersion( kRank_KeyPrecision, pRes->version );
		rank_core->SetRankDataValid( kRank_KeyPrecision, true );
		rank_core->UpdateKeyHighestPrecisionRank( *list );
	}
	return enPHandler_OK;
}

int MODI_PackageHandler_rdb2zs::LoadSelfRankResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Notify_LoadSelfRanksRes * pRes = (const MODI_RDB2S_Notify_LoadSelfRanksRes *)(pt_null_cmd);
	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();

	MODI_DBSelfRank self_rank;
	self_rank.size = pRes->size;
	self_rank.ranks = (MODI_CHARID *)pRes->data;
	rank_core->UpdateSelfRanks( pRes->type , self_rank );
	return enPHandler_OK;
}


int MODI_PackageHandler_rdb2zs::SongMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_S2RDB_Request_ClearZero * p_recv_cmd = (const MODI_S2RDB_Request_ClearZero *)(pt_null_cmd);
	/// 告知roledb加钱(临时性的...是一个无解的后遗症....)
	if(p_recv_cmd->m_byState == 3)
	{
		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
		if( !pDB )
		{
			Global::logger->info("[%s] save faild. can't get db interface." , SVR_TEST );
		}
		else
		{
			const_cast<MODI_S2RDB_Request_ClearZero *>(p_recv_cmd)->m_byState = 4;
			pDB->SendCmd(pt_null_cmd, sizeof(MODI_S2RDB_Request_ClearZero));
		}
	}
	/// 钱加好了
	else if(p_recv_cmd->m_byState == 5)
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
		if( pOnline )
		{
			MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnline->GetServerID() );
			if( pServer )
			{
				MODI_ZS2S_Notify_RefreshMoney msg;
				msg.m_charid = pOnline->GetCharID();
				msg.m_RMBMoney = p_recv_cmd->m_dwMoney;
				pServer->SendCmd(&msg,sizeof(msg));
			}
		}
		
		/// 告知他本人一个系统公告
		std::string send_content = "你今天第一次登陆，系统送了100点券给你";
		pOnline->SendSystemChat(send_content.c_str(), send_content.size());
	}
	return enPHandler_OK;
}

int MODI_PackageHandler_rdb2zs::ZoneUserVar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	const MODI_RDB2S_Return_ZoneUserVar_Cmd * p_recv_cmd = (const MODI_RDB2S_Return_ZoneUserVar_Cmd *)pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
	if( pClient )
	{
#ifdef _HRX_DEBUG
		Global::logger->debug("[load_user_var] load all user var from db <client=%s,size=%u>", pClient->GetRoleName(),p_recv_cmd->m_wdSize);
#endif			
#ifdef _DEBUG
		if( p_recv_cmd->m_dwAccountID == 0)
		{
			Global::logger->debug("[[[[[[[[[[[[[[[[[[[[receive system wide variable]]]]]]]]]]]]]]");
		}
#endif
		pClient->m_stZoneUserVarMgr.Init(pClient, (const char * )(&(p_recv_cmd->m_pData[0])), p_recv_cmd->m_wdSize);
	}
	else if( p_recv_cmd->m_dwAccountID == 0)
	{
		///   收到系统玩家的zone user的变量列表
		
		MODI_ASSERT(0);
	}
	else
	{
		MODI_ASSERT(0);
	}

#ifdef _DEBUG	
	std::string first_login_name = "zone_first_login_flag";
	MODI_ZoneUserVar * p_var = pClient->m_stZoneUserVarMgr.GetVar(first_login_name.c_str());
	if(! p_var)
	{
		p_var = new MODI_ZoneUserVar(first_login_name.c_str());
		std::string var_attribute = "type=normal";
		if(p_var->Init(pClient, var_attribute.c_str()))
		{
			if(! pClient->m_stZoneUserVarMgr.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}
	
	if(p_var->GetValue() == 0)
	{
		p_var->SetValue(1);
		const char * szTestCaption1 = "测试登陆";
 		const char * szTestContents1 = "测试的，正式版本不会有的";
		MODI_SendMailInfo  sendmail;
		sendmail.Reset();
		strncpy( sendmail.m_szCaption , szTestCaption1 , strlen(szTestCaption1) );
		strncpy( sendmail.m_szContents , szTestContents1 , strlen(szTestContents1) );
		strncpy( sendmail.m_szRecevier , pClient->GetRoleName() , pClient->GetNameLen() );
		sendmail.m_Type = kTextMail;
		MODI_MailCore::SendMail( sendmail);
	}
#endif	
	return enPHandler_OK;
}

/** 
 * @brief 刷新钱
 * 
 * @return 
 */
int MODI_PackageHandler_rdb2zs::DecMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_S2RDB_DecMoney_Cmd * p_recv_cmd = (MODI_S2RDB_DecMoney_Cmd *)pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
	if( pOnline )
	{
		MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnline->GetServerID());
		if( pServer )
		{
			MODI_ZS2S_Notify_RefreshMoney msg;
			msg.m_charid = pOnline->GetCharID();
			msg.m_RMBMoney = p_recv_cmd->m_dwMoney;
			msg.m_byMoneyType = p_recv_cmd->m_byMoneyType;
			pServer->SendCmd(&msg,sizeof(msg));
			Global::logger->info("[%s] send refresh money cmd to server<%u> , client<accid=%u,charid=%u,name=%s>" , 
					SVR_TEST ,
					pOnline->GetServerID() , 
					pOnline->GetAccid(),
					pOnline->GetCharID(),
					pOnline->GetRoleName() );

		}
	}
	else
	{
		Global::logger->debug("[dec_money] not find money <%u>", p_recv_cmd->m_dwAccountID);
		//MODI_ASSERT(0);
	}
	
	return enPHandler_OK;
}



/** 
 * @brief 家族升级结果
 * 
 * @return 
 */
int MODI_PackageHandler_rdb2zs::FamilyLevelResult(void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	const MODI_RDB2S_Notify_FamilyLevel * p_recv_cmd = (const MODI_RDB2S_Notify_FamilyLevel *)pt_null_cmd;
	/// 失败
	if(p_recv_cmd->m_byResult == 0)
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
		if( pOnline )
		{
			pOnline->RetFamilyResult(enLevelFamily_Error);
		}
	}
	else if(p_recv_cmd->m_byResult == 1)
	{

		/// 成功处理
		MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_recv_cmd->m_dwFamilyID);
		if(! p_family)
		{
			Global::logger->fatal("[onlevel_family] on level family failed,not find family <familyid=%u>", p_recv_cmd->m_dwFamilyID);
			MODI_ASSERT(0);
			return enPHandler_OK;
		}

		if(p_family->Levelup())
		{
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
			if( pOnline )
			{
				pOnline->RetFamilyResult(enLevelFamily_Succ);
			}
		}
	}
	return enPHandler_OK;
}


int MODI_PackageHandler_rdb2zs::RelationList_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	MODI_RDB2S_RelationLoad * relation = (MODI_RDB2S_RelationLoad *) pt_null_cmd;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * client = pMgr->FindByGUID(relation->m_id);

	if( !client)
	{
		Global::logger->debug("[relation_list] relation list load from db can't find characters");
	return enPHandler_OK;
	}
#ifdef _XXP_DEBUG
	Global::logger->debug("[relation_list] relation list load from db get %u relations ", relation->m_wNum);
#endif

	client->GetRelationList().LoadFromDB(client,relation->m_info,relation->m_wNum, relation->m_byIsLoadEnd);
	//if( relation->m_wNum != 500)
	if(relation->m_byIsLoadEnd == 1)
	{
		pMgr->OnClientLogin(client,false,true);
		/// 已经加载完毕	
		client->SyncRelationList();
	}	


	return enPHandler_OK;
}



/** 
 * @brief gm条件发送邮件
 * 
 */
int MODI_PackageHandler_rdb2zs::SendMailByCondition_Handler(void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size)
{
	MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
	if(!pMc )
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
	
	const MODI_SendMailByCondition_Cmd * p_recv_cmd = (MODI_SendMailByCondition_Cmd *)pt_null_cmd;
	MODI_Unify2S_SendMail_Cmd * p_change_cmd = const_cast<MODI_Unify2S_SendMail_Cmd *>(&(p_recv_cmd->m_stMailCmd));
	
	
	if(p_recv_cmd->m_wdSize == 0)
	{
		MODI_S2Unify_ReturnOpteate_Cmd send_cmd;
		send_cmd.m_stSessionID = p_change_cmd->m_stSessionID;
		strncpy(send_cmd.m_strName, p_change_cmd->m_strName, sizeof(send_cmd.m_strName) - 1);
		send_cmd.m_byValue = 2;
		pMc->SendCmd(&send_cmd, sizeof(send_cmd));
		return enPHandler_OK;
	}
	else
	{
		
		MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo * p_startaddr = (MODI_SendMailByCondition_Cmd::MODI_GetRoleInfo *)&(p_recv_cmd->m_pData[0]);
		for(WORD i=0; i<p_recv_cmd->m_wdSize; i++)
		{
			strncpy(p_change_cmd->m_strName, p_startaddr->m_cstrName, sizeof(p_change_cmd->m_strName) - 1);
			MODI_PackageHandler_zs2ms::CallGMSendMail_Handler(NULL,p_change_cmd, sizeof(MODI_Unify2S_SendMail_Cmd));
			p_startaddr++;
		}
	}
	return enPHandler_OK;
}
