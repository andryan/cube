#include "OfflineUserMgr.h"
#include "Base/HelpFuns.h"

MODI_OfflineUserMgr::MODI_OfflineUserMgr()
{

}

MODI_OfflineUserMgr::~MODI_OfflineUserMgr()
{

}

bool 	MODI_OfflineUserMgr::OnClientLogin( MODI_OnlineClient * pClient )
{
	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( pClient->GetRoleName() , pClient->GetNameLen() , role_name );
	// 从离线用户管理器中删除
	HASHMAP_ITER it = m_map.find( role_name );
	if( it != m_map.end() )
	{
		MODI_OfflineUser * pNode = it->second;
		m_Pool.Free( pNode );
		m_map.erase( it );
		return true;
	}
	return false;
}

bool 	MODI_OfflineUserMgr::OnClientLoginout(  MODI_OnlineClient * pClient )
{
	//有玩家登出，加入到离线管理器中 
	MODI_OfflineUser * pNode = m_Pool.Allocate();
	if( !pNode )
		return false;

	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( pClient->GetRoleName() , pClient->GetNameLen() , role_name );

	HASHMAP_INSERT_RESULT res = m_map.insert( std::make_pair( role_name , pNode ) );
	if( !res.second )
	{
		m_Pool.Free( pNode );
		return false;
	}

	// 加入成功后再初始化
	pNode->Init(pClient );
	
	return true;
}

MODI_OfflineUser * MODI_OfflineUserMgr::FindByName( const std::string & strName )
{
	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( strName.c_str() , strName.size(), role_name );

	HASHMAP_ITER it = m_map.find( role_name );
	if( it != m_map.end() )
	{
		MODI_OfflineUser * pNode = it->second;
		return pNode;
	}
	return 0;
}

MODI_OfflineUser * 	MODI_OfflineUserMgr::AddNode(const char * szName)
{
	if( !szName || !szName[0] )
		return 0;

	MODI_OfflineUser * pNode = m_Pool.Allocate();
	if( !pNode )
		return 0;

	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( szName , strlen(szName) , role_name );

	HASHMAP_INSERT_RESULT res = m_map.insert( std::make_pair( role_name , pNode ) );
	if( !res.second )
	{
		m_Pool.Free( pNode );
		return 0;
	}

	return pNode;
}


/// 历史问题，只能遍历
MODI_OfflineUser * MODI_OfflineUserMgr::FindByAccName(const char * acc_name)
{
	HASHMAP_ITER iter = m_map.begin();
	for(; iter != m_map.end(); iter++)
	{
		MODI_OfflineUser * p_user = iter->second;
		if(p_user)
		{
			if(strcasecmp(acc_name, p_user->GetAccName()) == 0)
			{
				return p_user;
			}
		}
	}
	return NULL;
}


/// 历史问题，只能遍历
MODI_OfflineUser * MODI_OfflineUserMgr::FindByAccId(const defAccountID acc_id)
{
	HASHMAP_ITER iter = m_map.begin();
	for(; iter != m_map.end(); iter++)
	{
		MODI_OfflineUser * p_user = iter->second;
		if(p_user)
		{
			if(p_user->GetAccId() == acc_id)
			{
				return p_user;
			}
		}
	}
	return NULL;
}
