/**
 * @file   Mail.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jan 30 11:12:38 2012
 * 
 * @brief  邮件类,整理
 * 
 * 
 */

#ifndef MODI_MAIL_H_
#define MODI_MAIL_H_

#include "Base/gamestructdef.h"
#include "ScriptMailDef.h"

/**
 * @brief 邮件类
 * 
 */
class MODI_Mail
{
 public:

	MODI_Mail();
	
	~MODI_Mail();

	/** 
	 * @brief 每个邮件有唯一性id
	 * 
	 */
	const MODI_GUID & GetGUID() const 
	{ 
		return m_dbInfo.m_MailInfo.m_mailGUID;
	}

	
	void Init(const MODI_DBMailInfo & info);

	void InitFromSendMail(const char * szSender, size_t nSenderLen , const MODI_SendMailInfo & info);


	/** 
	 * @brief 是否是文本邮件
	 * 
	 */
	bool IsTextMail() const
	{
		return m_dbInfo.m_MailInfo.m_Type == kTextMail;
	}


	/** 
	 * @brief 是否是带附件的邮件
	 * 
	 */
	bool IsAttachmentItemMail() const
	{
		return m_dbInfo.m_MailInfo.m_Type == kAttachmentItemMail;
	}

	/** 
	 * 是否是带金币的邮件
	 * 
	 * 
	 */
	bool IsAttachmentMoneyMail() const
	{
		return m_dbInfo.m_MailInfo.m_Type == kAttachmentMoneyMail;
	}

	/** 
	 * @brief 是否是系统邮件
	 * 
	 * 
	 */
	bool IsServerMail() const
	{
		return m_dbInfo.m_MailInfo.m_Type == kServerMail;
	}

	/** 
	 * @brief 邮件的状态
	 * 
	 * 
	 */
	char GetState() const 
	{
		return m_dbInfo.m_MailInfo.m_State;
	}

	/** 
	 * @brief 设置读状态
	 * 
	 */
	void SetReadState();
	

	bool IsRead() const 
	{ 
		return m_dbInfo.m_MailInfo.IsAlreadyRead();
	}

	void SetSender( const char * szName );
	void SetRecevier( const char * szName );
	void SetCaption( const char * szCaption );
	void SetContents( const char * szContents );
	void SetTimeStamp();

	const char * GetSender() const
	{
		return m_dbInfo.m_MailInfo.m_szSender;
	}

	const char * GetRecevier() const
	{
		return m_dbInfo.m_MailInfo.m_szRecevier;
	}

	const char * GetCaption() const
	{
		return m_dbInfo.m_MailInfo.m_szCaption;
	}

	const char * GetContents() const
	{
		return m_dbInfo.m_MailInfo.m_szContents;
	}

	time_t GetTimeStamp() const
	{
		return m_dbInfo.m_MailInfo.m_SendTime;
	}

	const MODI_DBMailInfo & GetDBMailInfo() const { return m_dbInfo; }

	const MODI_MailInfo & GetMailInfo() const { return m_dbInfo.m_MailInfo; }

	void  DeleteFromDB();
	void  SaveToDB();
	void  SetNewMail();
	void  SetTransid(QWORD transid)
	{
		m_dbInfo.m_Transid = transid;
	}

	void SetLockAttachment(bool bSet)
	{
		m_bLockGetAttach = bSet;
	}

	bool IsLockAttachment() const
	{
		return m_bLockGetAttach;
	}

	void ResetExtraInfo();

	bool IsAlreadyGetAttachment() const;
	void SetAlreadyGetAttachment();

	bool IsScriptMail() const;
	enScriptMailID GetScriptMailID() const;

 private:

	MODI_DBMailInfo 	m_dbInfo;
	bool 				m_bNeedSave;
	bool 				m_bLockGetAttach;
};

#endif
