#include "RankCore.h"
#include "Base/TimeManager.h"
#include "DBProxyClientTask.h"
#include "OnlineClient.h"
#include "OfflineUser.h"
#include "OnlineClientMgr.h"
#include "protocol/c2gs.h"
#include "Base/HelpFuns.h"

#ifdef _DEBUG
#define TIMER_RANKS_LOAD 	1000 * 60 * 1  // one mintue
#else
#define TIMER_RANKS_LOAD 	1000 * 60*60  // one hour
#endif

MODI_RankCore::MODI_RankCore():timer_rank_load_(TIMER_RANKS_LOAD)
{
	rank_level_.size = 0;
	rank_renqi_.size = 0;
	memset( version_ , 0 , sizeof(version_));
	memset( data_valid_ , 0 , sizeof(data_valid_));
	rank_space_[kRank_Begin].b = 0;
	rank_space_[kRank_Begin].e = 0;

	rank_space_[kRank_Level].b = 1;
	rank_space_[kRank_Level].e = RANK_LEVEL_MAX_TOP;

	rank_space_[kRank_Renqi].b = 1;
	rank_space_[kRank_Renqi].e = RANK_RENQI_MAX_TOP;

	rank_space_[kRank_Consume].b = 1;
	rank_space_[kRank_Consume].e = RANK_CONSUMERMB_MAX_TOP;

	rank_space_[kRank_HighestScore].b = 1;
	rank_space_[kRank_HighestScore].e = RANK_HIGHESTSCORE_MAX_TOP;

	rank_space_[kRank_HighestPrecision].b = 1;
	rank_space_[kRank_HighestPrecision].e = RANK_HIGHESTPRECISION_MAX_TOP;

	rank_space_[kRank_KeyPrecision].b = 1;
	rank_space_[kRank_KeyPrecision].e = RANK_HIGHESTPRECISION_MAX_TOP;


	rank_space_[kRank_KeyScore].b = 1;
	rank_space_[kRank_KeyScore].e = RANK_HIGHESTSCORE_MAX_TOP;

	rank_space_[kRank_End].b = 0;
	rank_space_[kRank_End].e = 0;
}

MODI_RankCore::~MODI_RankCore()
{

}

bool 	MODI_RankCore::IsValidRankSapce( enRankType type , WORD begin , WORD end  )
{
	if( end >= begin && begin >= rank_space_[type].b && end <= rank_space_[type].e )
		return true;
	return false;
}

bool 	MODI_RankCore::GetLevelRank( WORD begin , WORD end , MODI_Rank_Level * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_Level ) == false )
		return false;

	if( IsValidRankSapce( kRank_Level , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_Level * temp = (const MODI_Rank_Level *)(&rank_level_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}

bool 	MODI_RankCore::GetRenqiRank( WORD begin , WORD end , MODI_Rank_Renqi * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_Renqi ) == false )
		return false;

	if( IsValidRankSapce( kRank_Renqi , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_Renqi * temp = (const MODI_Rank_Renqi *)(&rank_renqi_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}

bool 	MODI_RankCore::GetConsumeRank( WORD begin , WORD end , MODI_Rank_ConsumeRMB * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_Consume ) == false )
		return false;

	if( IsValidRankSapce( kRank_Consume , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_ConsumeRMB * temp = (const MODI_Rank_ConsumeRMB *)(&rank_consume_rmb_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}

bool 	MODI_RankCore::GetHighestScoreRank( WORD begin , WORD end , MODI_Rank_HighestScore * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_HighestScore ) == false )
		return false;

	if( IsValidRankSapce( kRank_HighestScore , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_HighestScore * temp = (const MODI_Rank_HighestScore *)(&rank_highest_score_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}
bool 	MODI_RankCore::GetKeyHighestScoreRank( WORD begin , WORD end , MODI_Rank_HighestScore * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_KeyScore) == false )
		return false;

	if( IsValidRankSapce( kRank_KeyScore, begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_HighestScore * temp = (const MODI_Rank_HighestScore *)(&rank_key_highest_score_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}

bool 	MODI_RankCore::GetHighestPrecisionRank( WORD begin , WORD end , MODI_Rank_HighestPrecision * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_HighestPrecision ) == false )
		return false;

	if( IsValidRankSapce( kRank_HighestPrecision , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_HighestPrecision * temp = (const MODI_Rank_HighestPrecision *)(&rank_highest_precision_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}

bool 	MODI_RankCore::GetKeyHighestPrecisionRank( WORD begin , WORD end , MODI_Rank_HighestPrecision * out )
{
	if( !out )
		return false;

	if( IsValidData( kRank_KeyPrecision) == false )
		return false;

	if( IsValidRankSapce( kRank_KeyPrecision , begin , end ) == false )
		return false;

	size_t idx = 0;
	for( size_t n = begin - 1; n < end; n++ )
	{
		const MODI_Rank_HighestPrecision * temp = (const MODI_Rank_HighestPrecision *)(&rank_key_highest_precision_.ranks[n]);
		out[idx++] =  *temp;
	}
	return true;
}
void 	MODI_RankCore::UpdateLevelRank( const MODI_DBRank_Level_List & rank )
{
	rank_level_ = rank; 
}

void 	MODI_RankCore::UpdateRenqiRank( const MODI_DBRank_Renqi_List & rank )
{
	rank_renqi_ = rank; 
}

void 	MODI_RankCore::UpdateConsumeRMBRank( const MODI_DBRank_ConsumeRMB_List & rank )
{
	rank_consume_rmb_ = rank;
}

void 	MODI_RankCore::UpdateHighestScoreRank( const MODI_DBRank_HighestScore_List & rank )
{
	rank_highest_score_ = rank;
}

void 	MODI_RankCore::UpdateKeyHighestScoreRank( const MODI_DBRank_HighestScore_List & rank )
{
	rank_key_highest_score_ = rank;
}
void 	MODI_RankCore::UpdateHighestPrecisionRank( const MODI_DBRank_HighestPrecision_List & rank )
{
	rank_highest_precision_ = rank;
}

void 	MODI_RankCore::UpdateKeyHighestPrecisionRank( const MODI_DBRank_HighestPrecision_List & rank )
{
	rank_key_highest_precision_ = rank;
}
void 	MODI_RankCore::UpdateSelfRanks( enRankType type , const MODI_DBSelfRank & rank  )
{
	if( type > kRank_Begin && type < kRank_End )
	{
		self_ranks_[type].clear();
		for( size_t n = 0; n < rank.size; n++ )
		{
			MAP_SELF_RANKS_RESULT result = self_ranks_[type].insert( std::make_pair( rank.ranks[n] , n + 1 ) );
			if( result.second != true )
			{
				MODI_ASSERT(0);
			}
		}

		// 更新在线玩家的我的排名信息
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		pMgr->UpdateSelfRank( type );
	}
}

bool 	MODI_RankCore::LoadRanksFromDB()
{
	MODI_TimeManager * tm = MODI_TimeManager::GetInstancePtr();
	MODI_DBProxyClientTask * db_proxy = MODI_DBProxyClientTask::FindServer( 0 );
	bool bRet = false;
	if( tm && db_proxy )
	{
		MODI_S2RDB_Request_LoadRanks msg;
		msg.version = (QWORD)tm->GetSavedANSITime();
		bRet = db_proxy->SendCmd( &msg ,sizeof(msg) );
	}
	return bRet;
}

void 	MODI_RankCore::Update()
{
	if( timer_rank_load_( Global::m_stLogicRTime ) )
	{
		LoadRanksFromDB();
	}
}

bool 	MODI_RankCore::GetCharIDAndNameByRankNo( enRankType type , WORD no , MODI_CHARID & charid , char * role_name  )
{
	if( IsValidData( type ) == false )
		return false;

	if( IsValidRankSapce( type , no , no ) == false )
		return false;

	switch( type )
	{
	case kRank_Level:
		{
			charid = rank_level_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_level_.ranks[no - 1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;

	case kRank_Renqi:
		{
			charid = rank_renqi_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_renqi_.ranks[no - 1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;

	case kRank_Consume:
		{
			charid = rank_consume_rmb_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_consume_rmb_.ranks[no - 1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;
	case kRank_HighestScore:
		{
			charid = rank_highest_score_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_highest_score_.ranks[no -1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;
	case kRank_KeyScore:
		{
			charid = rank_key_highest_score_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_key_highest_score_.ranks[no -1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;
	case kRank_HighestPrecision:
		{
			charid = rank_highest_precision_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_highest_precision_.ranks[no -1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;

	case kRank_KeyPrecision:
		{
			charid = rank_key_highest_precision_.ranks[no - 1].charid;
			safe_strncpy( role_name , rank_key_highest_precision_.ranks[no -1].role_name , ROLE_NAME_MAX_LEN + 1 );
		}
	break;
	default:
		{
			MODI_ASSERT(0);
			return false;
		}
		break;
	}

	return true;
}

size_t 	MODI_RankCore::GetPerSpecificRankSize( enRankType type )
{
	static const DWORD specific_size[kRank_End - kRank_Begin + 1] = { 0, 
		sizeof(MODI_Rank_Level_Specific) , 
		sizeof(MODI_Rank_Renqi_Specific) , 
		sizeof(MODI_Rank_Consume_Specific) , 
		sizeof(MODI_Rank_HighestScore_Specific) , 
		sizeof(MODI_Rank_HighestPrecision_Specific),
		sizeof(MODI_Rank_HighestScore_Specific) , 
		sizeof(MODI_Rank_HighestPrecision_Specific),
		0 };
	if( type > kRank_Begin && type < kRank_End )
	{
		return specific_size[type];
	}
	return 0;
}

DWORD 	MODI_RankCore::GetSelfRank( MODI_CHARID charid , enRankType type )
{
	DWORD ret = 0;
	if( type > kRank_Begin && type < kRank_End )
	{
		MAP_SELF_RANKS_C_ITER citor = self_ranks_[type].find( charid );
		if( citor != self_ranks_[type].end() )
		{
			ret = citor->second;
		}
	}
	return ret;
}


void 	MODI_RankCore::FillSpecificInfoByOnlineClient( enRankType type , char * fill , MODI_OnlineClient * online_client )
{
	switch( type )
	{
	case kRank_Level:
		{
			MODI_Rank_Level_Specific * lvl_spec = (MODI_Rank_Level_Specific *)(fill);
			lvl_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			lvl_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData;
			lvl_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			lvl_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( lvl_spec->name , online_client->GetRoleName() , sizeof(lvl_spec->name) );
		}
	break;

	case kRank_Renqi:
		{
			MODI_Rank_Renqi_Specific * renqi_spec = (MODI_Rank_Renqi_Specific *)(fill);
			renqi_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			renqi_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData;
			renqi_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			renqi_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( renqi_spec->name , online_client->GetRoleName() , sizeof(renqi_spec->name) );
		}
	break;

	case kRank_Consume:
		{
			MODI_Rank_Consume_Specific * consume_spec = (MODI_Rank_Consume_Specific *)(fill);
			consume_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			consume_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData; 
			consume_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			consume_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( consume_spec->name , online_client->GetRoleName() , sizeof(consume_spec->name) );
		}
	break;

	case kRank_HighestScore:
		{
			MODI_Rank_HighestScore_Specific * highest_score_spec = (MODI_Rank_HighestScore_Specific *)(fill);
			highest_score_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			highest_score_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData; 
			highest_score_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			highest_score_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( highest_score_spec->name , online_client->GetRoleName() , sizeof(highest_score_spec->name) );

		}
	break;

	case kRank_KeyScore:
		{
			MODI_Rank_HighestScore_Specific * highest_score_spec = (MODI_Rank_HighestScore_Specific *)(fill);
			highest_score_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			highest_score_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData; 
			highest_score_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			highest_score_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( highest_score_spec->name , online_client->GetRoleName() , sizeof(highest_score_spec->name) );

		}
	break;
	case kRank_HighestPrecision:
		{
			MODI_Rank_HighestPrecision_Specific * highest_pre_spec = (MODI_Rank_HighestPrecision_Specific *)(fill);
			highest_pre_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			highest_pre_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData; 
			highest_pre_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			highest_pre_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( highest_pre_spec->name , online_client->GetRoleName() , sizeof(highest_pre_spec->name) );

		}
	break;

	case kRank_KeyPrecision:
		{
			MODI_Rank_HighestPrecision_Specific * highest_pre_spec = (MODI_Rank_HighestPrecision_Specific *)(fill);
			highest_pre_spec->default_avatar_idx = online_client->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
			highest_pre_spec->avatars = online_client->GetFullData().m_roleInfo.m_normalInfo.m_avatarData; 
			highest_pre_spec->level = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
			highest_pre_spec->sex = online_client->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
			safe_strncpy( highest_pre_spec->name , online_client->GetRoleName() , sizeof(highest_pre_spec->name) );

		}
	break;
	default:
		{
			MODI_ASSERT(0);
		}
		break;
	}
}

void 	MODI_RankCore::FillSpecificInfoByOfflineClient( enRankType type , char * fill , MODI_OfflineUser * offline_client )
{
	switch( type )
	{
	case kRank_Level:
		{
			MODI_Rank_Level_Specific * lvl_spec = (MODI_Rank_Level_Specific *)(fill);
			lvl_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			lvl_spec->avatars = offline_client->GetAvatarData();
			lvl_spec->level = offline_client->GetLevel();
			lvl_spec->sex = offline_client->GetSex();
			safe_strncpy( lvl_spec->name , offline_client->GetName() , sizeof(lvl_spec->name) );
		}
	break;

	case kRank_Renqi:
		{
			MODI_Rank_Renqi_Specific * renqi_spec = (MODI_Rank_Renqi_Specific *)(fill);
			renqi_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			renqi_spec->avatars = offline_client->GetAvatarData();
			renqi_spec->level = offline_client->GetLevel();
			renqi_spec->sex = offline_client->GetSex();
			safe_strncpy( renqi_spec->name , offline_client->GetName() , sizeof(renqi_spec->name) );
		}
	break;

	case kRank_Consume:
		{
			MODI_Rank_Consume_Specific * consume_spec = (MODI_Rank_Consume_Specific *)(fill);
			consume_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			consume_spec->avatars = offline_client->GetAvatarData();
			consume_spec->level = offline_client->GetLevel();
			consume_spec->sex = offline_client->GetSex();
			safe_strncpy( consume_spec->name , offline_client->GetName() , sizeof(consume_spec->name) );
		}
	break;
	case kRank_HighestScore:
		{
			MODI_Rank_HighestScore_Specific * highest_score_spec = (MODI_Rank_HighestScore_Specific *)(fill);
			highest_score_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			highest_score_spec->avatars = offline_client->GetAvatarData();
			highest_score_spec->level = offline_client->GetLevel();
			highest_score_spec->sex = offline_client->GetSex();
			safe_strncpy( highest_score_spec->name , offline_client->GetName() , sizeof(highest_score_spec->name) );
		}
	break;
	case kRank_HighestPrecision:
		{
			MODI_Rank_HighestPrecision_Specific * highest_pre_spec = (MODI_Rank_HighestPrecision_Specific *)(fill);
			highest_pre_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			highest_pre_spec->avatars = offline_client->GetAvatarData();
			highest_pre_spec->level = offline_client->GetLevel();
			highest_pre_spec->sex = offline_client->GetSex();
			safe_strncpy( highest_pre_spec->name , offline_client->GetName() , sizeof(highest_pre_spec->name) );
		}
	break;

	case kRank_KeyScore:
		{
			MODI_Rank_HighestScore_Specific * highest_score_spec = (MODI_Rank_HighestScore_Specific *)(fill);
			highest_score_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			highest_score_spec->avatars = offline_client->GetAvatarData();
			highest_score_spec->level = offline_client->GetLevel();
			highest_score_spec->sex = offline_client->GetSex();
			safe_strncpy( highest_score_spec->name , offline_client->GetName() , sizeof(highest_score_spec->name) );
		}
	break;
	case kRank_KeyPrecision:
		{
			MODI_Rank_HighestPrecision_Specific * highest_pre_spec = (MODI_Rank_HighestPrecision_Specific *)(fill);
			highest_pre_spec->default_avatar_idx = offline_client->GetDefaultAvatar();
			highest_pre_spec->avatars = offline_client->GetAvatarData();
			highest_pre_spec->level = offline_client->GetLevel();
			highest_pre_spec->sex = offline_client->GetSex();
			safe_strncpy( highest_pre_spec->name , offline_client->GetName() , sizeof(highest_pre_spec->name) );
		}
	break;
	default:
		{
			MODI_ASSERT(0);
		}
		break;
	}
}

void 	MODI_RankCore::SyncOnlineClientSpecificRank( MODI_OnlineClient * src_client , enRankType type , 
		WORD rank_no , MODI_OnlineClient * rank_client )
{
	if( src_client && rank_client && type > kRank_Begin && type < kRank_End )
	{
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_GS2C_Notify_SpecificRank * res = (MODI_GS2C_Notify_SpecificRank *)(szPackageBuf);
		AutoConstruct( res );
		res->result = kRankSpecificOk;
		res->rank_no = rank_no;
		res->rank_type = type;
		FillSpecificInfoByOnlineClient( type , res->specific_data , rank_client );
		size_t per_element_size = GetPerSpecificRankSize(  type );
		int send_size = sizeof(MODI_GS2C_Notify_SpecificRank) + per_element_size * 1;
		src_client->SendPackage( res , send_size );
	}
}
		
void 	MODI_RankCore::SyncOfflineClientSpecificRank( MODI_OnlineClient * src_client , enRankType type , 
				WORD rank_no , MODI_OfflineUser * rank_client )
{
	if( src_client && rank_client && type > kRank_Begin && type < kRank_End )
	{
		char szPackageBuf[Skt::MAX_USERDATASIZE];
		memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
		MODI_GS2C_Notify_SpecificRank * res = (MODI_GS2C_Notify_SpecificRank *)(szPackageBuf);
		AutoConstruct( res );
		res->result = kRankSpecificOk;
		res->rank_no = rank_no;
		res->rank_type = type;
		FillSpecificInfoByOfflineClient( type , res->specific_data , rank_client );
		size_t per_element_size = GetPerSpecificRankSize(  type );
		int send_size = sizeof(MODI_GS2C_Notify_SpecificRank) + per_element_size * 1;
		src_client->SendPackage( res , send_size );
	}
}
