/**
 * @file   ZoneUserVar.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jun  7 10:21:20 2011
 * 
 * @brief  zoneservice变量
 * 
 */

#ifndef _MD_ZONEUSERVAR_H
#define _MD_ZONEUSERVAR_H

class MODI_OnlineClient;

#include "UserVar.h"
#include "GlobalDB.h"


/**
 * @brief onlineclient用户变量
 * 
 */
class MODI_ZoneUserVar: public MODI_UserVar<MODI_OnlineClient>
{
 public:
	MODI_ZoneUserVar(const char * var_name, MODI_TableStruct * p_table = GlobalDB::m_pZoneUserVarTbl):
		MODI_UserVar<MODI_OnlineClient>(var_name,p_table)
	{
			
	}

		
	virtual ~MODI_ZoneUserVar(){}
	
	/** 
	 * @brief  执行数据库sql语句
	 * 
	 * @param sql 
	 * @param sql_size 
	 */
	virtual void ExecSqlToDB(const char * sql, const WORD sql_size);

	/** 
	 * @brief  更新
	 * 
	 */
	virtual void UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);
};

typedef MODI_UserVarManager<MODI_ZoneUserVar> MODI_ZoneUserVarMgr;

#endif
