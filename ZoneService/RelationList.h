/** 
 * @file RelationList.h
 * @brief 联系人表
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-13
 */

#ifndef MODI_RELATION_LIST_H_
#define MODI_RELATION_LIST_H_

#include "Relation.h"
#include <map>


/// 解除关系的联系人缓存上限
#define RELATIONS_RELIEVE_MAX 	10

class MODI_OnlineClient ;

class 	MODI_RelationList
{
		typedef std::map<MODI_CHARID , MODI_Relation *>  LIST_RELATIONS;
		typedef LIST_RELATIONS::iterator 		LIST_RELATIONS_ITER;
		typedef LIST_RELATIONS::const_iterator 	LIST_RELATIONS_C_ITER;
		typedef std::pair<LIST_RELATIONS_ITER,bool> LIST_RELATIONS_INSERT_RESULT;
		
	public:

	public:

		MODI_RelationList();
		~MODI_RelationList();

	public:

		/** 
		 * @brief 初始化联系人列表
		 * 
		 * @return 	
		 */
		bool Init( MODI_OnlineClient * pClient , const void * p );

		/**
		 * @brief  data base 加载的数据
		 *
		 */
		bool	LoadFromDB( MODI_OnlineClient * pClient,MODI_DBRelationInfo * array,WORD num, bool is_load);

		/**
		 * @brief 删除好友到某一个数量
		 * @return 
		 */
		WORD	DeleteFriendTo(WORD	num);

		/**
		 *@breif 删除偶像到某一个数量
		 *
		 */
		WORD	DeleteIdoleTo( WORD	num);	


		/** 
		 * @brief 添加一个联系人到列表中
		 * 
		 * @param pRel 联系人信息
		 * 
		 * @return 	成功返回TRUE
		 */
		bool AddRelation( MODI_Relation * pRel , enRelOptType opt , enRelationRetType & ret );

		/** 
		 * @brief 从联系人列表中删除一个联系人
		 * 
		 * @param charid 联系人id
		 */
		void DelRelation( MODI_CHARID  charid );

		/** 
		 * @brief 某人是否是自己的联系人
		 * 
		 * @param szName 某人名
		 * 
		 * @return 	是则返回true
		 */
		bool 	IsRelation( const char * szName , size_t nNameLen );

		
		/** 
		 * @brief 根据ID查找一个联系人
		 * 
		 * @param charid 联系人的ID
		 * 
		 * @return 	
		 */
		MODI_Relation * FindRelation( MODI_CHARID charid );

		/** 
		 * @brief 根据名字查找一个联系人
		 * 
		 * @param szName 联系人的名字
		 * @param nNameLen 名字长度
		 * 
		 * @return 	
		 */
		MODI_Relation * FindRelation( const char * szName , size_t nNameLen );

		/** 
		 * @brief 获得当前好友数
		 * 
		 * @return 	
		 */
		size_t GetFriendSize() const;

		/** 
		 * @brief 黑名单数
		 * 
		 * @return 	
		 */
		size_t GetBlackSize() const;

		/** 
		 * @brief 偶像数
		 * 
		 * @return 	
		 */
		size_t GetIdolsSize() const;

		size_t GetRelationSize() const;

		/** 
		 * @brief 好友是否已经满
		 * 
		 * @return 	
		 */
		bool IsFriendFull() const;

		/** 
		 * @brief 黑名单是否已经满
		 * 
		 * @return 	
		 */
		bool IsBlackFull() const;

		/** 
		 * @brief 偶像是否已经满
		 * 
		 * @return 	
		 */
		bool IsIdolsFull() const;

		
		/** 
		 * @brief 清空联系人列表
		 */
		void CleanUp();

		/** 
		 * @brief 保存到数据库
		 */
		void SaveToDB();


		//void BuildRelationListNotifyPackage( void * p );
		void SyncRelationList(MODI_OnlineClient * p_client);

		void OnListChanged();

		/** 
		 * @brief 修改某个联系人的关系类型
		 * 
		 * @param id
		 * @param opt
		 * 
		 * @return 	
		 */
		MODI_Relation * ModfiyRel( MODI_CHARID charid , enRelOptType opt , enRelationRetType & ret );

		void FlushToFullData( MODI_CharactarFullData & fulldata );

	private:

		bool IsInList( LIST_RELATIONS & l , MODI_CHARID id );
		bool IsInList( LIST_RELATIONS & l , const char * szName , size_t nSize );

		bool AddToList( LIST_RELATIONS & l , MODI_CHARID id , MODI_Relation * rel );
		MODI_Relation * DelFromList( LIST_RELATIONS & l , MODI_CHARID id );
		MODI_Relation * FindInList( LIST_RELATIONS & l , MODI_CHARID id );
		MODI_Relation * FindInList( LIST_RELATIONS & l , const char * szName , size_t nSize );

		void CleanUpList( LIST_RELATIONS & l );

		void SaveListToDB( LIST_RELATIONS & l , void * p );


		void OnRelieve( MODI_Relation * pRel );


		enRelationRetType ImplAddFriend( MODI_Relation * pRel );
		enRelationRetType ImplDelFriend( MODI_Relation * pRel );
		enRelationRetType ImplAddIdol( MODI_Relation * pRel );
		enRelationRetType ImplDelIdol( MODI_Relation * pRel );
		enRelationRetType ImplAddBlack( MODI_Relation * pRel );
		enRelationRetType ImplDelBlack( MODI_Relation * pRel );
		
	public:
		bool	HaveInit()
		{
			return m_bInit;
		}

	private:
		WORD	DelRelationTo( WORD num ,enRelationType  type);

		LIST_RELATIONS m_Rels;

		size_t 		m_nIdolSize;
		size_t 		m_nBlackSize;
		size_t 		m_nFriendSize;

		MODI_Relation * m_pLover;
		MODI_Relation * m_pMarried;

		MODI_DBRelationInfo 	m_RelieveRelations[RELATIONS_RELIEVE_MAX];
		
		MODI_OnlineClient * m_pClient;
		bool	m_bInit;
};









#endif
