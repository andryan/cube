#include "TodayOnlineTimeClean.h"
#include "GameSvrClientTask.h"
#include "DBProxyClientTask.h"
#include "Base/s2zs_cmd.h"
#include "Base/s2rdb_cmd.h"

MODI_TodayOnlineTimeClean::MODI_TodayOnlineTimeClean()
{

}

MODI_TodayOnlineTimeClean::~MODI_TodayOnlineTimeClean()
{

}

void 	MODI_TodayOnlineTimeClean::Initial()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	m_start = ptm->Time2DWORD();
}

void 	MODI_TodayOnlineTimeClean::Update()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	if( ptm )
	{
		DWORD now = ptm->Time2DWORD();

		DWORD nowday = now / 10000;
		// 比较日期
		DWORD startday = m_start / 10000;
		if( nowday > startday )
		{
			Global::logger->info("[clean today online time] startday=%u,nowday=%u.",
					startday ,
					nowday );

			OnCleanUPOnlineTime();
			m_start = now;
		}
	}
}

void 	MODI_TodayOnlineTimeClean::CleanUP()
{

}

void 	MODI_TodayOnlineTimeClean::OnCleanUPOnlineTime()
{
	MODI_ZS2S_Notify_ResetOnlineTime msg;
	msg.m_todayOnlineTime = 0;
	for( BYTE  n = 0; n < 100; n++ )
	{
		MODI_GameSvrClientTask * pServer  = MODI_GameSvrClientTask::FindServer( n );
		if( pServer )
		{
			pServer->SendCmd(&msg,sizeof(msg));
		}
	}

	MODI_S2RDB_Request_ResetTodayOnlineTime reset;
	MODI_DBProxyClientTask * pDBProxy = MODI_DBProxyClientTask::FindServer(0);
	if( pDBProxy )
	{
		pDBProxy->SendCmd( &reset , sizeof(reset) );
	}
}
