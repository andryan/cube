#include "SingleObject.h"
#include "Global.h"
#include "BinFileMgr.h"
#include <vector>
#include "OnlineClient.h"
using namespace std;

//class MODI_OnlineClient;

struct ChengHaoItem
{
	WORD 	m_wConfigId;
	DWORD	m_dwValue;
	ChengHaoItem()
	{
		m_wConfigId = 0;
		m_dwValue = 0;
	}

};


class MODI_ChengHaoMgr 
{

public:
	void Init();
	static MODI_ChengHaoMgr * GetInstance();
	WORD	GetChengHao(MODI_OnlineClient * pclient);				

private:
	MODI_ChengHaoMgr()
	{
	}
	
	~MODI_ChengHaoMgr()
	{
	}
	static MODI_ChengHaoMgr * m_pInstance;


	vector<ChengHaoItem> m_mPositive;

	vector<ChengHaoItem> m_mNegative;

	WORD		m_wGm;

};



