#include "ZoneServerAPP.h"

/**
 * @brief 主程序
 *
 *
 */
int main(int argc, char **argv)
{
	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	MODI_ZoneServerAPP app("Zone Server");

	int iRet = app.Init();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	app.Shutdown();

	::sleep(1);

	return iRet;
}

