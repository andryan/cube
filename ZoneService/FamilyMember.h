/**
 * @file   FamilyMember.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Apr 23 17:20:59 2011
 * 
 * @brief  家族成员
 * 
 * 
 */

#ifndef _MD_FAMILYMEMBER_H
#define _MD_FAMILYMEMBER_H

#include "Global.h"
#include "AssertEx.h"
#include "gamestructdef.h"

class MODI_OnlineClient;

/** 
 * @brief 家族成员类
 * 
 */
class MODI_FamilyMember
{
 public:

	MODI_FamilyMember(const MODI_FamilyMemBaseInfo & base_info);
	
 	MODI_FamilyMember(const MODI_FamilyBaseInfo & base_info, MODI_OnlineClient * p_client);

	 /** 
	  * @brief 把自己加入到管理器
	  * 
	  */
	 bool AddMeToFamily();

	 /** 
	  * @brief 设置家族名字
	  * 
	  * @param name 
	  */
	 void SetFamilyName(const char * name)
	 {
		 strncpy(m_stBaseInfo.m_cstrFamilyName, name, sizeof(m_stBaseInfo.m_cstrFamilyName) - 1);
	 }

	 /** 
	  * @brief 获取家族名字
	  * 
	  * 
	  */
	 const char * GetFamilyName()
	 {
		 return m_stBaseInfo.m_cstrFamilyName;
	 }

 	 const MODI_GUID & GetGuid()
	 {
		 return m_stBaseInfo.m_stGuid;
	 }

	 const defFamilyID & GetFamilyID()
	 {
	 	return m_stBaseInfo.m_dwFamilyID;
	 }

	 enFamilyPositionType GetPosition()
	 {
		 return m_stBaseInfo.m_enPosition;
	 }

	 const char * GetRoleName()
	 {
		 return m_stBaseInfo.m_cstrRoleName;
	 }

	 const BYTE GetSexType()
	 {
		 return m_stBaseInfo.m_bySex;
	 }
	 
	 const char * GetRoleLinkName();

	 void SetMaster()
	 {
		 m_stBaseInfo.m_enPosition = enPositionT_Lead;
	 }

	 void SetSlaver()
	 {
		 m_stBaseInfo.m_enPosition = enPositionT_ViceLead;
	 }


	 bool IsMaster()
	 {
		 if(m_stBaseInfo.m_enPosition == enPositionT_Lead)
			 return true;
		 return false;
	 }

	 bool IsSlaver()
	 {
		 if(m_stBaseInfo.m_enPosition == enPositionT_ViceLead)
			 return true;
		 return false;
	 }

	 bool IsNormal()
	 {
		 if(m_stBaseInfo.m_enPosition == enPositionT_Normal ||
			m_stBaseInfo.m_enPosition == enPositionT_Lead ||
			m_stBaseInfo.m_enPosition == enPositionT_ViceLead)
			 return true;
		 return false;
	 }

	 void Login()
	 {
		 m_stBaseInfo.m_enState = enMemStateT_Free;
	 }

	 void Logout();

	 bool IsRequest()
	 {
		 if(m_stBaseInfo.m_enPosition == enPositionT_Request)
			 return true;
		 return false;
	 }

	 const DWORD GetContribute()
	 {
		 return m_stBaseInfo.m_dwContribute;
	 }

	 const defAccountID & GetAccid()
	 {
		 return m_stBaseInfo.m_dwAccountID;
	 }

	 /** 
	  * @brief 创建者
	  * 
	  */
	 bool OnCreate();
	 

	 /** 
	  * @brief 申请加入家族
	  * 
	  * 
	  */
	 bool OnRequest();


	 /** 
	  * @brief 离开了
	  * 
	  */
	 bool OnLeave();

	 
	 bool OnAccept();
	 

	 void SetNormal()
	 {
		 m_stBaseInfo.m_enPosition = enPositionT_Normal;
	 }

	 void SetRequest()
	 {
		 m_stBaseInfo.m_enPosition = enPositionT_Request;
	 }

	 void SetLeave()
	 {
		 m_stBaseInfo.m_enPosition = enPositionT_None;
		 m_stBaseInfo.Clear();
	 }

	 void  SetLevel( WORD level)
	 {
		 m_stBaseInfo.m_wLevel = level;
	 }


	 /** 
	  * @brief 人气设置
	  * 
	  */
	 void SetRenqi(MODI_OnlineClient * p_client);


	 /** 
	  * @brief 获取人气
	  * 
	  * 
	  */
	 const DWORD GetRenqi()
	 {
		 return m_stBaseInfo.m_dwRenqi;
	 }
	 

	/** 
	 * @brief 保存到数据库
	 * 
	 */
	void SaveToDB();

	/** 
	 * @brief 更新到数据库
	 * 
	 */
	void UpDateToDB();

	/** 
	 * @brief 从数据库中删除
	 * 
	 */
	void DelFromDB();


	/** 
	 * @brief 获取成员属性
	 * 
	 * 
	 */
	const MODI_FamilyMemBaseInfo & GetMemBaseInfo()
	{
		return m_stBaseInfo;
	}

	/** 
	 * @brief 把自己的信息发送给家族所有成员
	 * 
	 */
	void SendMeToFamily(MODI_OnlineClient * p_ex_client = NULL);


	/** 
	 * @brief 把家族信息发给自己
	 * 
	 * @param reason 
	 */
	void SendFamilyToMe(enSendFullInfoReason reason);


	/** 
	 * @brief 发送邮件
	 * 
	 * @param text 邮件内容
	 *
	 */
	void SendMailToMe(const char * text);

 private:
	/// 成员信息
	MODI_FamilyMemBaseInfo m_stBaseInfo;
	
	/// 加入时间
  	QWORD m_qdJoinTime;

	
};

#endif
