/**
 * @file   BillClient.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Fri Oct 15 11:45:46 2010
 * @version $Id:$
 * @brief  交易连接
 * 
 */

#ifndef _MD_BILLTASK_H
#define _MD_BILLTASK_H

#include "Global.h"
#include "RClientTask.h"
#include "CommandQueue.h"
#include "TransCommand.h"

/**
 * @brief bill连接
 * 
 */
class MODI_BillClient: public MODI_RClientTask, public MODI_CmdParse
{
public:
	MODI_BillClient(const char * name, const char * server_ip,const WORD & port): MODI_RClientTask(name, server_ip, port), m_strClientName(name)
	{
		Resize(GWTOGS_CMDSIZE);
	}

	static MODI_BillClient * GetInstancePtr()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_BillClient("billclient", Global::g_strBillIP.c_str(), Global::g_wdBillPort);
		}
		return m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	/// 初始化
	bool Init();

	const bool IsConnected();

	/** 
	 * @brief 发送命令
	 * 
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 队列内的命令
	 * 
	 * @param pt_null_cmd 获取的命令
	 * @param dwCmdLen 命令长度
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);


	/** 
	 * @brief 主动刷新显示
	 * 
	 * @param accid 要刷新的id
	 * @param money 此账号所拥有的钱
	 * 
	 */
	void MRefreshMoney(const MODI_MRefreshMoney * p_recv_cmd);

	/** 
	 * @brief 请求刷新显示
	 * 
	 * @param accid 要刷新的id
	 *
	 */
	void SRefreshMoney(const char * acc_name, const DWORD & accid);


	
	/** 
	 * @brief 交易返回
	 * 
	 * @param trans_id 交易唯一ID
	 * @param result 结果
	 */
	void RetConsumeResult(QWORD trans_id, enTransResult result);

 private:
	static MODI_BillClient * m_pInstance;
	virtual ~MODI_BillClient()
	{
		ClientFinal();
	}
	
	void ClientFinal();

	std::string m_strClientName;

};


#endif
