/** 
 * @file ScriptMailHandler.h
 * @brief 脚本邮件处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-09-26
 */

#ifndef MODI_SCRIPTMAIL_HANDLER_H_
#define MODI_SCRIPTMAIL_HANDLER_H_

#include "ScriptMailDef.h"
#include "SingleObject.h"
#include <map>

class 	MODI_OnlineClient;
class 	MODI_Mail;

enum
{
	kScriptMailHandler_Ok = 0, // 处理但是不删除
	kScriptMailHandler_Del, // 处理后删除
	kScriptMailHandler_NotHandler, // 没有处理
	kScriptMailHandler_NotHandler_Del , // 没有处理，但是删除
};

typedef int(* pScriptMailHandler)(MODI_OnlineClient * , const MODI_Mail & );

class 	MODI_ScriptMailHandler : public CSingleObject<MODI_ScriptMailHandler>
{
	struct MODI_ScriptMail
	{
		enScriptMailID 	type;
		pScriptMailHandler 	exe_func; 			
	};


	public:


	MODI_ScriptMailHandler();
	virtual ~MODI_ScriptMailHandler();

	bool 	Initialization();
    int  	ExcuteScript( MODI_OnlineClient * pClient , const MODI_Mail & mi );

	private:

	void AddHandler(enScriptMailID type , const MODI_ScriptMail & h);

	static int AddMail( MODI_OnlineClient * pClient , const MODI_Mail & mi );
	static int DecIdolCount( MODI_OnlineClient * pClient , const MODI_Mail & mi );
	static int IncIdolCount( MODI_OnlineClient * pClient , const MODI_Mail & mi );
	static int BlockOptMail( MODI_OnlineClient * pClient , const MODI_Mail & mi );
	static int GiveShopGoodsMail( MODI_OnlineClient * pClient , const MODI_Mail & mi );

	private:

	typedef std::map<enScriptMailID, MODI_ScriptMail> 		MAP_HANDLER;
	typedef MAP_HANDLER::iterator 						MAP_HANDLER_ITER;
	typedef std::pair<MAP_HANDLER_ITER,bool> 			MAP_INSERT_RESULT;
	MAP_HANDLER 		m_mapHandlers;
};

#endif

