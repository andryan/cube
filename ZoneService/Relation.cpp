#include "Relation.h"
#include "FreeList.h"

static gems::FreeList< MODI_Relation,
	gems::PlacementNewLinkedList<MODI_Relation>,
	gems::ConstantGrowthPolicy<10000,1000,1000000> > 	gs_RelationPoolInst;

// 测试用
//static gems::FreeList< MODI_Relation,
//	gems::PlacementNewLinkedList<MODI_Relation>,
//	gems::ConstantGrowthPolicy<10,50,1000000> > 	gs_RelationPoolInst;

MODI_Relation::MODI_Relation()
{
	m_blNeedSave = false;
}


MODI_Relation::~MODI_Relation()
{

}
		
void MODI_Relation::Init( const MODI_DBRelationInfo & info )
{
	m_info = info;
	m_info.m_bIsValid = true;
	m_blNeedSave = false;
}

bool MODI_Relation::IsHasRelation() const
{
	return IsFriend() || IsBlack() || IsLover() || IsMarried() || IsIdol();
}

void MODI_Relation::SetServerName( const char * szServerName )
{
	memset( m_info.m_TempData.m_szServerName , 0 , sizeof(m_info.m_TempData.m_szServerName ) );
	strcpy( m_info.m_TempData.m_szServerName , szServerName );
}

void MODI_Relation::SetGameState( kGameState state )
{
	m_info.m_TempData.m_nGameState = state;
}

kGameState MODI_Relation::GetGameState() const
{
	return m_info.m_TempData.m_nGameState; 
}

void MODI_Relation::SetRoomID( defRoomID id )
{
	m_info.m_TempData.m_roomID = id;
}

defRoomID MODI_Relation::GetRoomID() const
{
	return m_info.m_TempData.m_roomID; 
}

const char * MODI_Relation::GetServerName() const
{
	return m_info.m_TempData.m_szServerName;
}

MODI_Relation * 	MODI_RelationPool::New()
{
	return gs_RelationPoolInst.Allocate();
}
		
void 				MODI_RelationPool::Delete( MODI_Relation * p )
{
	gs_RelationPoolInst.Free( p );
}
