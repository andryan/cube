#include "OnlineClient.h"
#include "protocol/c2gs.h"
#include "FreeList.h"
#include "GameSvrClientTask.h"
#include "s2zs_cmd.h"
#include "s2rdb_cmd.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"
#include "MailCore.h"
#include "Base/HelpFuns.h"
#include "BinFileMgr.h"
#include "FamilyManager.h"
#include "DBClient.h"
#include "UserVarDefine.h"
#include "ChengHaoMgr.h"
#include "BillClient.h"

static gems::FreeList< MODI_OnlineClient,
	gems::PlacementNewLinkedList<MODI_OnlineClient>,
	gems::ConstantGrowthPolicy<4000,500,1000000> > 	gs_OnlineClientPool;

// 测试用
//static gems::FreeList< MODI_OnlineClient,
//	gems::PlacementNewLinkedList<MODI_OnlineClient>,
//	gems::ConstantGrowthPolicy<10,50,1000000> > 	gs_OnlineClientPool;

MODI_OnlineClient * 	MODI_OnlineClientPool::New()
{
	return gs_OnlineClientPool.Allocate();
}

void 	MODI_OnlineClientPool::Delete( MODI_OnlineClient * p )
{
	gs_OnlineClientPool.Free( p );
}

#define NOTONSERVER_TIMEOUT 	60 * 1000
#define CLIENT_DB_SAVETIMER 	1000 * 10  // 保存数据到数据库的时间
//#define CLIENT_DB_SAVETIMER 	30 * 1000 * 1 // 保存数据到数据库的时间
#define REQ_LOVE_TIMEOUT 		30 * 1000 // 求婚，请爱超时

MODI_OnlineClient::MODI_OnlineClient():
	m_byServerID(0),
	m_byGameState(kOutServer),
	m_timerDBSave( CLIENT_DB_SAVETIMER ),
	m_ReqLoveTimer( 30 * 1000 ),
	m_timerNotOnServer( NOTONSERVER_TIMEOUT ),
	m_timerUpRelGameState( 1 * 1000 ),
	m_nReqLoveID(INVAILD_CHARID),
	m_bReqLover(false),m_nNameLen(0)
{
	m_st = enWaitDataFromDB;
	m_pMaillist = 0;
	m_bLoadingMails = false;
//	m_bMailTest = false;
	m_nPendingMailCount = 0;

	memset( self_ranks , SELF_RANK_OUT , sizeof(self_ranks) );
	self_ranks_version = RANK_VER_FOR_INITIAL;
}
		

MODI_OnlineClient::~MODI_OnlineClient()
{
}

void MODI_OnlineClient::Init( defAccountID accid , const MODI_GUID & guid , const char * szName )
{
	m_nAccid = accid;
	m_guid = guid;
	m_nNameLen = strlen(szName);
	safe_strncpy( m_FullData.m_roleInfo.m_baseInfo.m_cstrRoleName , 
			szName , 
			sizeof(m_FullData.m_roleInfo.m_baseInfo.m_cstrRoleName) );
}

void MODI_OnlineClient::SetAccid( defAccountID accid )
{
	m_nAccid = accid;
}

void MODI_OnlineClient::SetGUID( const MODI_GUID & guid )
{
	m_guid = guid;
}

void MODI_OnlineClient::SetSessionID( const MODI_SessionID & sessionid )
{
	m_sessionID = sessionid;
}

void MODI_OnlineClient::SetServerID( BYTE byServerID )
{
	m_byServerID = byServerID;
}

void MODI_OnlineClient::SetNewServerID( BYTE byNewServerID )
{
	m_byNewServerID = byNewServerID;
}

void MODI_OnlineClient::SetServerName( const char * szServerName )
{
	strcpy( m_szServerName , szServerName );
}

void MODI_OnlineClient::SetGameState( kGameState st )
{
	m_byGameState = st;
}

//bool 	MODI_OnlineClient::Init( defAccountID accid , const MODI_GUID & guid , const MODI_SessionID & sessionID ,
//	   	BYTE byServerID , const char * szServerName )
//{
//	m_accid = accid;
//	m_guid = guid;
//	m_byServerID = byServerID;
//	m_sessionID = sessionID;
//	strcpy( m_szServerName , szServerName );
//
////	MODI_CharactarFullData * pFullData = (MODI_CharactarFullData *)(p);
////	
////
////	memcpy( &m_FullData , pFullData , sizeof(MODI_CharactarFullData) );
////	m_guid = m_FullData.m_roleInfo.m_baseInfo.m_roleID;
////	m_sessionID = sessionID;
////	m_byServerID = byServerID;
////	strcpy( m_szServerName , szServerName );
////
////	m_relations.Init( this , pFullData );
//
//	return true;
//}

void MODI_OnlineClient::Update(const MODI_RTime &  cur_rtime, const MODI_TTime &  cur_ttime)
{
	// 保存数据
 	if( m_timerDBSave(cur_rtime) )
 	{
		m_stUserMailsMgr.Update();
		m_stSvrMailsMgr.Update();
 	}
	
	// 是否正在求爱
	if( IsReqingHoney() )
	{
		// 求爱超时
		if( m_ReqLoveTimer(cur_rtime) )
		{
			m_nReqLoveID = INVAILD_CHARID;
		}
	}

	m_stZoneUserVarMgr.UpDate(cur_rtime,cur_ttime);
}

const MODI_GUID & MODI_OnlineClient::GetGUID() const
{
	return m_guid;
}

MODI_CHARID  MODI_OnlineClient::GetCharID() const
{
	MODI_CHARID charid = GUID_LOPART( m_guid );
	return charid;
}

const MODI_SessionID & MODI_OnlineClient::GetSessionID() const
{
	return m_sessionID;
}

const char * MODI_OnlineClient::GetRoleName() const
{
	return m_FullData.m_roleInfo.m_baseInfo.m_cstrRoleName;
}

const char * MODI_OnlineClient::GetServerName() const
{
	return m_szServerName;
}

BYTE 	MODI_OnlineClient::GetServerID() const
{
	return m_byServerID;
}

BYTE 	MODI_OnlineClient::GetNewServerID() const
{
	return m_byNewServerID;
}

BYTE 	MODI_OnlineClient::GetSexType() const
{
	return m_FullData.m_roleInfo.m_baseInfo.m_ucSex;
}

bool 	MODI_OnlineClient::IsNeedNotifyWithSbLogin( MODI_CHARID guid )
{
	MODI_Relation * pRel = m_relations.FindRelation( guid );
	if( pRel )
	{
		if( pRel->IsFriend() || pRel->IsLover() || pRel->IsMarried() || pRel->IsIdol() )
			return true;
	}
	return false;
}

bool 	MODI_OnlineClient::IsNeedNotifyWithSbLoginOut( MODI_CHARID guid )
{
	return this->IsNeedNotifyWithSbLogin( guid );
}

void 	MODI_OnlineClient::OnRelationLogin( MODI_OnlineClient * pRel )
{
	MODI_Relation * pRelData = GetRelationList().FindRelation( pRel->GetCharID() );
	if( !pRelData )
		return ;
	if( pRelData->IsBlack() )
		return ;

	pRelData->SetOnline( true );
	// 通知该客户端，有联系人上线了
	MODI_GS2C_Notify_RelationStateChanged 	msg;
	msg.m_guid = pRel->GetGUID();
	msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enOnlineState;
	msg.m_newState.m_onlineState.m_bOnline = true;
	memset( msg.m_newState.m_onlineState.m_szServerName , 
			0 , 
			sizeof(msg.m_newState.m_onlineState.m_szServerName) );
	safe_strncpy( msg.m_newState.m_onlineState.m_szServerName , 
			pRel->GetServerName() , 
			sizeof(msg.m_newState.m_onlineState.m_szServerName) );

	this->SendPackage( &msg , sizeof(msg) );

	pRelData->SetGameState( pRel->GetFullData().m_roleInfo.m_baseInfo.m_ucGameState );
	pRelData->SetRoomID( pRel->GetFullData().m_roleInfo.m_baseInfo.m_roomID );
	{
		MODI_GS2C_Notify_RelationStateChanged 	msg;
		msg.m_guid = pRel->GetGUID();
		msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enGameState;
		msg.m_newState.m_gameState.m_nRoomID = pRelData->GetRoomID();
		msg.m_newState.m_gameState.m_nGameState = pRelData->GetGameState();
		this->SendPackage( &msg , sizeof(msg) );
	}

	AddReverseRelation(pRel->GetCharID());
}
		
void 	MODI_OnlineClient::OnRelationLoginOut( MODI_OnlineClient * pRel )
{
	MODI_Relation * pRelData = GetRelationList().FindRelation( pRel->GetCharID() );
	if( !pRelData )
		return ;
	if( pRelData->IsBlack() )
		return ;

 
	pRelData->SetOnline( false );
	// 通知该客户端，有联系人下线了
	MODI_GS2C_Notify_RelationStateChanged 	msg;
	msg.m_guid = pRel->GetGUID();
	msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enOnlineState;
	msg.m_newState.m_onlineState.m_bOnline = false;
	this->SendPackage( &msg , sizeof(msg) );

	RemoveReverseRelation(pRel->GetCharID());
}

void 	MODI_OnlineClient::OnRelationGameStateChange( MODI_OnlineClient * pRel )
{
	MODI_Relation * pRelData = GetRelationList().FindRelation( pRel->GetCharID() );
	if( !pRelData )
		return ;
	if( pRelData->IsBlack() )
		return ;

	pRelData->SetGameState( pRel->GetFullData().m_roleInfo.m_baseInfo.m_ucGameState );
	pRelData->SetRoomID( pRel->GetFullData().m_roleInfo.m_baseInfo.m_roomID );

	MODI_GS2C_Notify_RelationStateChanged 	msg;
	msg.m_guid = pRel->GetGUID();
	msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enGameState;
	msg.m_newState.m_gameState.m_nRoomID = pRelData->GetRoomID();
	msg.m_newState.m_gameState.m_nGameState = pRelData->GetGameState();
	this->SendPackage( &msg , sizeof(msg) );
}

bool 	MODI_OnlineClient::SendPackage(const void * pt_null_cmd, int cmd_size)
{
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( GetServerID() );
	if( !pServer || 
		GetCharID() == INVAILD_CHARID ||
		GetSessionID().IsInvaild() )
	{
		Global::logger->error("[%s] can't send package to online client<name=%s,charid=%u,onserver_id=%u,serverptr=%p,\
				sessionid<%s>>"  ,
				SVR_TEST,
				GetRoleName(),
				GetCharID(),
				GetServerID(),
				pServer,
				GetSessionID().ToString() );
		return false;
	}

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_ZS2GS_Redirectional * p_redirectionalPackage = (MODI_ZS2GS_Redirectional *) (szPackageBuf);
	AutoConstruct( p_redirectionalPackage );
	p_redirectionalPackage->m_guid = MAKE_GUID( GetCharID() , 0 );
	p_redirectionalPackage->m_sessionID = GetSessionID();
	p_redirectionalPackage->m_nSize = cmd_size;
	memcpy(p_redirectionalPackage->m_pData, (char *) pt_null_cmd, cmd_size);
	int iSendSize = cmd_size + sizeof( MODI_ZS2GS_Redirectional );

	return pServer->SendCmd( (const Cmd::stNullCmd *)p_redirectionalPackage , iSendSize );
}
		
void 	MODI_OnlineClient::SyncRelationList()
{
	GetRelationList().SyncRelationList(this);
	return;
#if 0	
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2C_Notify_RelationList * pMsg = (MODI_GS2C_Notify_RelationList *)(szPackageBuf);
	AutoConstruct( pMsg );
	GetRelationList().BuildRelationListNotifyPackage( pMsg );
	int iSendSize = sizeof(MODI_GS2C_Notify_RelationList) + pMsg->m_nSize * sizeof(MODI_RelationInfo);
	SendPackage( pMsg , iSendSize );
#endif	
}

void 	MODI_OnlineClient::SyncFensiRenqi()
{
	MODI_GS2C_Notify_UpdateFensiRenqi msg;
	msg.m_nFensiBoy = m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy;
	msg.m_nFensiGril = m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril;
	msg.m_nRenqi = m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi;
	SendPackage( &msg , sizeof(msg) );
	
	/// 人气改变了，看称号是否需要改变
	ChangeChenghao();
	/// 改变家族成员人气
	ChangeFamilyMemRenqi();
}

void 	MODI_OnlineClient::SyncSelfRank()
{
	MODI_GS2C_Notify_SelfRank msg;
	msg.self_rank_consume = self_ranks[kRank_Consume];
	msg.self_rank_highestprecision = self_ranks[kRank_HighestPrecision];
	msg.self_rank_highestscore = self_ranks[kRank_HighestScore];
	msg.self_rank_level = self_ranks[kRank_Level];
	msg.self_rank_renqi = self_ranks[kRank_Renqi];
	msg.self_rank_keyprecise = self_ranks[kRank_KeyPrecision];
	msg.self_rank_keyscore = self_ranks[kRank_KeyScore];
	SendPackage( &msg ,sizeof(msg) );
}

void 	MODI_OnlineClient::OnBeAddIdolBySB( BYTE bySex )
{
	if( bySex == enSexAll )
		return ;

	if( bySex == enSexBoy )
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy += 1;
	else 
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril += 1;
	
	m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi += 30;

	SyncFensiRenqi();
}

void	MODI_OnlineClient::AddRenqi( WORD	n)
{
	m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi += n;
	SyncFensiRenqi();
}
		
void 	MODI_OnlineClient::OnBeDelIdolBySB( BYTE bySex )
{
	if( bySex == enSexAll )
		return ;

	if( bySex == enSexBoy )
	{
		if( m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy > 0 )
			m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiBoy -= 1;
	}
	else 
	{
		if( m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril > 0 )
			m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nFensiGril -= 1;
	}
	
	if( m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi  < 30 )
	{
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi = 0;
	}
	else 
	{
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi -= 30;
	}

	SyncFensiRenqi();
}

/** 
 * @brief 被拉黑操作处理
 * 
 */
void	MODI_OnlineClient::BlockOptDeal(const enRelOptType by_type)
{
	WORD block_count = GetBlockCount();
	
	if(by_type == enRelOpt_AddB) //被拉黑
	{
		if(block_count < ((WORD)-1))
		{
			block_count++;
		}
	}
	else if(by_type == enRelOpt_DelB) //被解黑
  	{
		if(block_count > 0)
		{
			block_count--;
		}
		else
		{
			//MODI_ASSERT(0);
		}
	}
	else
	{
		MODI_ASSERT(0);
	}
	
	SetBlockCount(block_count);

	/// 是否需要更改称号
	ChangeChenghao();
}

/** 
 * @brief 检查次称号24小时内是否重复
 * 
 * @param chenghao_id 要检查的称号
 */
void	MODI_OnlineClient::CheckChanghaoTime(DWORD chenghao_id)
{
	/// 查看是否相隔24小时,没过24小时不要更换负面称号
	/// modi zone var
	NotifyChangeChenghao(chenghao_id, true,true);
}

/** 
 * @brief 根据属性获得称号
 * 
 * @return 称号ID
 *
 */
const WORD	MODI_OnlineClient::GetChenghao() 
{
#ifdef _XXP_DEBUG
	WORD	chenghao = MODI_ChengHaoMgr::GetInstance()->GetChengHao(this);

	Global::logger->debug("[Chenghao_debug] I got a chenghao <name=%s,block=%u,renqi=%u,chenghao=%u>",GetRoleName(),GetBlockCount(),GetRenqi(),chenghao);
	return chenghao;
#else
	return MODI_ChengHaoMgr::GetInstance()->GetChengHao(this);
;
#endif
}


/** 
 * @brief 通知称号改变
 * 
 * @param is_notify_zone 是否需要广播
 *
 */
void 	MODI_OnlineClient::NotifyChangeChenghao(const WORD chenghao_id,  bool is_notify_zone, bool is_notify_client)
{
	Global::logger->info("[change_chenghao] change chenghao <name=%s,chenghao_old=%u,chenghao_new=%u>",
						 GetRoleName(),m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao,chenghao_id);
	m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = chenghao_id;

	if(is_notify_client)
	{
		MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( GetServerID() );
		if( !pServer)
		{
			MODI_ASSERT(0);
			return ;
		}
	
		MODI_ZS2S_Notify_ChangChenghao send_cmd;
		send_cmd.m_wdChenghao = m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao;
		send_cmd.m_charid = GetCharID();
		pServer->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	
}


/** 
 * @brief 角色称号改变
 * 
 * @param by_flag 0考虑15天限制，1不要考虑称号下降15天限制
 *
 */
void 	MODI_OnlineClient::ChangeChenghao(BYTE by_flag)
{
	WORD cheng_hao_old = m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao;
	WORD cheng_hao_new = GetChenghao();

#ifdef _HRX_DEBUG	
	Global::logger->info("[change_chenghao] change chenghao <name=%s,old=%u,new=%u>", GetRoleName(), cheng_hao_old, cheng_hao_new);
#endif	
	
	if(cheng_hao_old == cheng_hao_new)
	{
		if(by_flag)/// 15天过去了，直接恢复低称号
		{
			m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = cheng_hao_new;
		}
		return;
	}
	if((cheng_hao_new >= TESHU_CHENGHAO_ID) || (cheng_hao_old >= TESHU_CHENGHAO_ID))//大于特殊称号
   	{
		/// 负面称号
		CheckChanghaoTime(cheng_hao_new);
		m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = cheng_hao_new;
		NotifyChangeChenghao(m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao, false,false);
		return;
	}

	/// 正常称号
	if(cheng_hao_new > cheng_hao_old)/// 称号上涨
	{
		/// 复位称号下降15天变量
		/// ......modi zone var
		MODI_ZoneUserVar * p_var = m_stZoneUserVarMgr.GetVar(CHENGHAO_XIAJIAN_KEEPTIME.c_str());
		if(p_var)
		{
			p_var->SetValue(0);
		}
		
		m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = cheng_hao_new;
	}
	else/// 称号下降了
	{
		if(by_flag)/// 15天过去了，直接恢复低称号
		{
			m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = cheng_hao_new;
			NotifyChangeChenghao(m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao, false,false);
			return;
		}
		else
		{
			/// 保留称号15后再下降,
			MODI_ZoneUserVar * p_var = m_stZoneUserVarMgr.GetVar(CHENGHAO_XIAJIAN_KEEPTIME.c_str());
			if(! p_var)
			{
				CreateUserVar(CHENGHAO_XIAJIAN_KEEPTIME.c_str(), CHENGHAO_XIAJIAN_KEEPTIME_ATTRIBUTE.c_str());
			}

			/// 24小时邮件通知一次
			p_var = m_stZoneUserVarMgr.GetVar(CHENGHAO_XIAJIAN_NOTIFY.c_str());
			if(! p_var)
			{
				CreateUserVar(CHENGHAO_XIAJIAN_NOTIFY.c_str(), CHENGHAO_XIAJIAN_NOTIFY_ATTRIBUTE.c_str());
			}
			p_var = m_stZoneUserVarMgr.GetVar(CHENGHAO_XIAJIAN_NOTIFY.c_str());
			if(p_var)
			{
				if(p_var->GetValue() == 0)
				{
					p_var->SetValue(1);
					const char * szTestCaption1 = "Kamu akan kehilangan title";
					const char * szTestContents1 = "Popularitas kamu telah berkurang, title kamu sekarang akan bertahan hingga 15 hari kedepan. Jika popularitas kamu tidak memenuhi kriteria title-mu sekarang, maka title-mu akan kembali ke level terakhir, segera tingkatkan nilai popularitasmu.";
					MODI_SendMailInfo  sendmail;
					sendmail.Reset();
					strncpy( sendmail.m_szCaption , szTestCaption1 , strlen(szTestCaption1) );
					strncpy( sendmail.m_szContents , szTestContents1 , strlen(szTestContents1) );
					strncpy( sendmail.m_szRecevier , GetRoleName() , GetNameLen() );
					sendmail.m_Type = kTextMail;
					MODI_MailCore::SendMail( sendmail);
				}
			}
		}
	}

	/// 称号更改通知客户端
	if(m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao != cheng_hao_old)
	{
		///第一个白色称号
		if(((m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao % 3) == 1) &&
		   (m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao > 1))
		{
			NotifyChangeChenghao(m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao, true, true);
		}
		else
		{
			NotifyChangeChenghao(m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao, false, true);
		}
	}
}
		
bool 	MODI_OnlineClient::IsAddIdolTimeOver() const
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	time_t diff = ptm->DiffTimeT( GetLastIdolTime() , ptm->GetSavedANSITime() );
	if( diff >= 60 * 15)
		//	enOneHour * 2 )
		return true;
	return false;
}

time_t 	MODI_OnlineClient::GetLastIdolTime() const
{
	return m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nLastAddIdolTime;
}

void 	MODI_OnlineClient::SetLastIdolTime( time_t t )
{
	if( t != GetLastIdolTime() )
	{
		m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nLastAddIdolTime = t;
	}
}


void 	MODI_OnlineClient::UpdateLastLoginInfo()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	safe_strncpy( m_FullData.m_roleExtraData.m_szLastLoginIP , 
			GetSessionID().GetIPString(),
			sizeof(m_FullData.m_roleExtraData.m_szLastLoginIP));
	m_FullData.m_roleExtraData.m_nLastLoginTime = ptm->GetSavedANSITime();

	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
	if( pDB )
	{
		MODI_S2RDB_Request_UpdateLastLogin msg;
		msg.m_charid = GetCharID();
		msg.m_Time = m_FullData.m_roleExtraData.m_nLastLoginTime;
		safe_strncpy( msg.m_szIP , 
					m_FullData.m_roleExtraData.m_szLastLoginIP , 
					sizeof(msg.m_szIP));
		pDB->SendCmd(&msg,sizeof(msg) );
	}
	else 
	{
		Global::logger->warn("[%s] save last login information faild. can't get db interface." , SVR_TEST );
	}
}

void 	MODI_OnlineClient::OnLoginIn()
{
	// 加载联系人数据
	//GetRelationList().Init( this , &m_FullData );

	// 加载邮件数据
	MODI_MailCore::OnClientLogin( this );

	// 加载我的排行数据
	LoadSelfRank();

	/// INA和SGP版本同步钱
	if(Global::g_byGameShop > 0)
	{
		MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
		if(pBill && pBill->IsConnected())
		{
			pBill->SRefreshMoney(GetAccName(), GetAccid());
		}
		
	}
	Global::logger->debug("[client_login] online client login <accid=%llu,accname=%s,guid=%llu,rolename=%s,%s>",
						  GetAccid(), GetAccName(), GetCharID(), GetRoleName(), m_sessionID.ToString());
}

void 	MODI_OnlineClient::LoadSelfRank()
{
	// 加载排行数据
	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
	if( rank_core )
	{
		for( int i = kRank_Begin + 1; i < kRank_End; i++ )
		{
			DWORD rank = rank_core->GetSelfRank( GetCharID() , (enRankType)i );
			SetSelfRank( (enRankType)i , rank );
		}
	}
}

void 	MODI_OnlineClient::OnLoginOut()
{
	Global::logger->debug("[client_logout] online client logout <accid=%llu,accname=%s,guid=%llu,rolename=%s,%s>",
						  GetAccid(), GetAccName(), GetCharID(), GetRoleName(), m_sessionID.ToString());
	SaveToDB();
	
	//SaveMoneyRmb();
}

void 	MODI_OnlineClient::OnEnterChannel()
{
	ChangeChenghao(false);
	// 是否重新进入频道
	if( IsInWaitReEnterChannelState() )
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		pMgr->OnClientReEnterChannel( this );
	}

	// 同步联系人数据
	if(GetRelationList().HaveInit())
		SyncRelationList();

	// 同步邮件信息
	if( IsInWaitAnotherLoginState() || 
		IsInWaitReEnterChannelState() )
	{
		m_stUserMailsMgr.SendUserMailsToClient();
		
		if( IsInWaitAnotherLoginState() )
		{
			LoadSelfRank();
		}
	}
}

void 	MODI_OnlineClient::OnLeaveChannel()
{
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	pMgr->OnClientLeaveChannel( this );
	// fulsh relationlist to fulldata
	GetRelationList().FlushToFullData( m_FullData );
}

enRelationRetType 	MODI_OnlineClient::IsCanMarryMe( MODI_OnlineClient * pClient )
{
	const BYTE byLevelReq = 2;

	// 性别是否一样
	if( this->GetSexType() == pClient->GetSexType() )
		return enRelationRetT_CanMarry_SexSame;

	// 是否都为单身关系
	if( !this->GetFullData().m_roleInfo.m_detailInfo.IsSingle() )
		return enRelationRetT_CanMarry_SelfNotSingle;
	if( !pClient->GetFullData().m_roleInfo.m_detailInfo.IsSingle() )
		return enRelationRetT_CanMarry_TargetNotSingle;

	// 是否互为联系人
	MODI_Relation * pRel1 = this->GetRelationList().FindRelation( pClient->GetCharID() );
	MODI_Relation * pRel2 = pClient->GetRelationList().FindRelation(  this->GetCharID() );
	if( !pRel1 || !pRel2 )
		return enRelationRetT_CanMarry_NotF;

	// 是否互为好友
	if( !pRel1->IsFriend() || !pRel2->IsFriend() )
		return enRelationRetT_CanMarry_NotF;

	// 是否满足等级需求
	if( this->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel < byLevelReq ||
		pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel < byLevelReq )
		return enRelationRetT_CanMarry_LevelReq;

	// 满足其他需求
			
	return enRelationRetT_Ok;
}

enRelationRetType 	MODI_OnlineClient::IsCanDoMyGF( MODI_OnlineClient * pClient )
{
	const BYTE byLevelReq = 2;

	// 性别是否一样
	if( this->GetSexType() == pClient->GetSexType() )
		return enRelationRetT_DoMyGF_SexSame;

	// 是否都为单身关系
	if( !this->GetFullData().m_roleInfo.m_detailInfo.IsSingle() )
		return enRelationRetT_DoMyGF_SelfNotSingle; 
	if( !pClient->GetFullData().m_roleInfo.m_detailInfo.IsSingle() )
		return enRelationRetT_DoMyGF_TargetNotSingle;

	// 是否互为联系人
	MODI_Relation * pRel1 = this->GetRelationList().FindRelation( pClient->GetCharID() );
	MODI_Relation * pRel2 = pClient->GetRelationList().FindRelation(  this->GetCharID() );
	if( !pRel1 || !pRel2 )
		return enRelationRetT_DoMyGF_NotF;

	// 是否互为好友
	if( !pRel1->IsFriend() || !pRel2->IsFriend() )
		return enRelationRetT_DoMyGF_NotF;

	// 是否满足等级需求
	if( this->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel < byLevelReq ||
		pClient->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel < byLevelReq )
		return enRelationRetT_DoMyGF_LevelReq;

	return enRelationRetT_Ok;
}

void 	MODI_OnlineClient::OnBeginReqCanDoMyHoney( MODI_CHARID charid , bool bLover )
{
	if( charid == INVAILD_CHARID )
		return ;

	m_nReqLoveID = charid;
	m_ReqLoveTimer.Reload( REQ_LOVE_TIMEOUT , Global::m_stLogicRTime );
	m_bReqLover = bLover;
}
		
bool 	MODI_OnlineClient::IsMyReqHoney( MODI_CHARID charid )
{
	if( m_nReqLoveID == charid && charid != INVAILD_CHARID )
		return true;

	return false;
}
		
bool 	MODI_OnlineClient::IsReqingHoney() const
{
	if( m_nReqLoveID == INVAILD_CHARID )
		return false;

	return true;
}

void 	MODI_OnlineClient::OnEndReqCanDoMyHoney()
{
	m_nReqLoveID = INVAILD_CHARID;
	m_bReqLover = false;
}
		
bool 	MODI_OnlineClient::IsReqMarry() const
{
	return m_bReqLover == false;
}

size_t 	MODI_OnlineClient::GetNameLen() const
{
	return m_nNameLen;
}

void 	MODI_OnlineClient::SetSelfRank( enRankType type , DWORD rank )
{
	if( type > kRank_Begin && type < kRank_End )
	{
		self_ranks[type] = rank;
	}
}

void 	MODI_OnlineClient::OnLoadFromDB( const MODI_CharactarFullData & fullData )
{
	MODI_GUID guid = GetGUID();
	memcpy( &m_FullData , &fullData , sizeof(MODI_CharactarFullData) );
	m_FullData.m_roleInfo.m_baseInfo.m_roleID = guid;

	/// 加载zone_user_var
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(GetServerID());
	if( !pDB )
	{
		return;
	}
	
	MODI_S2RDB_Request_ZoneUserVar_Cmd send_cmd;
	send_cmd.m_dwAccountID = GetAccid(); 
	pDB->SendCmd(&send_cmd, sizeof(send_cmd));
}

void 	MODI_OnlineClient::BinLoadFromDB(const char * p_bin_data, const DWORD data_size)
{
	
}

void 	MODI_OnlineClient::SaveToDB()
{
	Global::logger->info("[%s] save client<rolename=%s,charid=%u,serverid=%u,state=%s> to db." , 
			SVR_TEST ,
			GetRoleName() ,
			GetCharID(),
			GetServerID(),
			GetStringFromatState() );

	if( GetAccid() == 0)
	{
		return ;
	}
	bool bSave = true;
	if( GetServerID() == 0 )
	{
		bool bKick = false;
		if( IsInWaitAnotherLoginState() || IsInWaitReEnterChannelState() )
		{
			if( m_timerNotOnServer( Global::m_stLogicRTime ) )
			{
				bKick = true;
				Global::logger->info("[%s] client not on the gameserver timeout . save and kick." , 
						SVR_TEST );
			}
		}
		else 
		{
			bKick = true;
			Global::logger->error("[%s] client not on the gameserver<serverid=%u> , and state is error." , 
					SVR_TEST );
			MODI_ASSERT(0);
		}

		if( bKick )
		{
			MODI_KickClientOp::Kick( this , false , 0 , kNotOnServerTimeout );
		}
	}
	
	if( !bSave )
		return ;

	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( GetServerID() );
	if( !pDB )
	{
		Global::logger->info("[%s] save faild. can't get db interface." , SVR_TEST );
		return ;
	}

	char cmd_buf[Skt::MAX_USERDATASIZE];
	memset(cmd_buf, 0, sizeof(cmd_buf));
 	MODI_S2RDB_Request_SaveCharData  * msg2db_savechardata = (MODI_S2RDB_Request_SaveCharData *)(cmd_buf);
	AutoConstruct(msg2db_savechardata);

	msg2db_savechardata->m_nChannelID = GetServerID();
	msg2db_savechardata->m_nCharID = GetCharID();
	msg2db_savechardata->m_nSessionID = GetSessionID();
	msg2db_savechardata->m_roleInfo = GetFullData().m_roleInfo;
	msg2db_savechardata->m_roleExtraData = GetFullData().m_roleExtraData;


	/// 处理一下零散的数据
// 	m_stSerialUserOtherData.Clear();
// 	m_stSerialUserOtherData.SetValue(m_stOtherData);
// 	m_stSerialUserOtherData.SetValue(m_stCountManager);
// 	m_stSerialUserOtherData.SerialStruct(msg2db_savechardata->m_Data, msg2db_savechardata->m_dwSize);

	msg2db_savechardata->m_dwSize = 0;
	
	pDB->SendCmd(msg2db_savechardata , sizeof(MODI_S2RDB_Request_SaveCharData) + msg2db_savechardata->m_dwSize);

	// 社会关系信息
	GetRelationList().SaveToDB();

	MODI_S2RDB_Request_SaveSocialData msg;
	msg.m_charid = this->GetCharID();
	msg.m_data = this->GetFullData().m_roleInfo.m_detailInfo.m_SocialRelData;
	pDB->SendCmd( &msg , sizeof(msg) );
}
		
void 	MODI_OnlineClient::ReqFullDataFromGameServer()
{
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( GetServerID() );
	if( !pServer || 
		GetCharID() == INVAILD_CHARID ||
		GetSessionID().IsInvaild() )
	{
		return ;
	}
}

void 	MODI_OnlineClient::OnRecvFullDataFromGameServer( const MODI_CharactarFullData & fullData )
{
	MODI_ASSERT( strcmp( m_FullData.m_roleInfo.m_baseInfo.m_cstrRoleName , fullData.m_roleInfo.m_baseInfo.m_cstrRoleName ) == 0 );
	MODI_ASSERT( m_FullData.m_roleInfo.m_baseInfo.m_ucSex == fullData.m_roleInfo.m_baseInfo.m_ucSex );

	// 1
	m_FullData.m_roleInfo.m_baseInfo.m_roomID = INVAILD_ROOM_ID;
	if( m_FullData.m_roleInfo.m_baseInfo.m_ucGameState !=  fullData.m_roleInfo.m_baseInfo.m_ucGameState )
	{
	}
	if( m_FullData.m_roleInfo.m_baseInfo.m_ucLevel != fullData.m_roleInfo.m_baseInfo.m_ucLevel )
	{

	}
	m_FullData.m_roleInfo.m_baseInfo = fullData.m_roleInfo.m_baseInfo;

	// 2
	WORD chenghao = m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao;
	m_FullData.m_roleInfo.m_normalInfo = fullData.m_roleInfo.m_normalInfo;
	m_FullData.m_roleInfo.m_normalInfo.m_wdChenghao = chenghao;

	// 3
	MODI_SocialRelationData backup_srd = m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData;
	
	m_FullData.m_roleInfo.m_detailInfo = fullData.m_roleInfo.m_detailInfo;

	m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData = backup_srd;

	


	// 4
	//m_FullData.m_avatarBag = fullData.m_avatarBag;

	// 5
	//m_FullData.m_playerBag = fullData.m_playerBag;

	WORD block_count = m_FullData.m_roleExtraData.m_wdBlockCount;
	m_FullData.m_roleExtraData = fullData.m_roleExtraData;
	m_FullData.m_roleExtraData.m_wdBlockCount = block_count;
}	

void 	MODI_OnlineClient::ResetTempData()
{
	m_FullData.m_roleInfo.m_baseInfo.m_roomID = INVAILD_ROOM_ID;
	m_FullData.m_roleInfo.m_baseInfo.m_ucGameState = kInLobby;
	m_FullData.m_roleInfo.m_normalInfo.m_SingerID = INVAILD_GUID;
	m_FullData.m_roleInfo.m_normalInfo.m_roomSolt = INVAILD_POSSOLT;
}

const char * MODI_OnlineClient::GetStringFromatState() const
{
	static const std::string strState[] = {
	"WaitDataFromDB",
	"InWorldPlaying",
	"WaitRechangeChannel", 
	"WaitReEnterChannel", 
	"WaitAnotherLogin" };

	return strState[m_st].c_str();
}

void 	MODI_OnlineClient::SwtichToWaitAnotherLoginState()
{
	m_st = enWaitAnotherLogin;
	m_timerNotOnServer.Reload( NOTONSERVER_TIMEOUT 	, Global::m_stLogicRTime );
}

void 	MODI_OnlineClient::SwitchToWaitReEnterChannelState()
{
	m_st = enWaitReEnterChannel;
	m_timerNotOnServer.Reload( NOTONSERVER_TIMEOUT , Global::m_stLogicRTime );
}

void 	MODI_OnlineClient::OnRecvUpdateData( MODI_UpdateDataRead & r , const MODI_ObjUpdateMask & upm )
{
	bool bNotifyGameState = false;
	MODI_RoleInfo roleinfo;
	if(upm.isSetBit(MODI_RoleUPMaskDefine::kRenqi))
	{
		if( r.Read(&roleinfo.m_detailInfo.m_SocialRelData.m_nRenqi,sizeof(roleinfo.m_detailInfo.m_SocialRelData.m_nRenqi)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi = roleinfo.m_detailInfo.m_SocialRelData.m_nRenqi;
			SyncFensiRenqi();
		}
	}
	
	if( upm.isSetBit( MODI_RoleUPMaskDefine::kGameState ) )
	{
		if( r.Read(&(roleinfo.m_baseInfo.m_ucGameState),sizeof(roleinfo.m_baseInfo.m_ucGameState)) )
//				&&
//			m_FullData.m_roleInfo.m_baseInfo.m_ucGameState != roleinfo.m_baseInfo.m_ucGameState )
		{
			bNotifyGameState  = true;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kLevel ) )
	{
		if( r.Read(&roleinfo.m_baseInfo.m_ucLevel,sizeof(roleinfo.m_baseInfo.m_ucLevel)) )
		{
			m_FullData.m_roleInfo.m_baseInfo.m_ucLevel = roleinfo.m_baseInfo.m_ucLevel;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kAvatar ) )
	{
		if( r.Read(&roleinfo.m_normalInfo.m_avatarData,sizeof(roleinfo.m_normalInfo.m_avatarData) ) )
		{
			m_FullData.m_roleInfo.m_normalInfo.m_avatarData = roleinfo.m_normalInfo.m_avatarData;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kExp ) )
	{
		if( r.Read(&roleinfo.m_normalInfo.m_nExp,sizeof(roleinfo.m_normalInfo.m_nExp) ) )
		{
			m_FullData.m_roleInfo.m_normalInfo.m_nExp = roleinfo.m_normalInfo.m_nExp;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kAge ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_byAge,sizeof(roleinfo.m_detailInfo.m_byAge)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_byAge = roleinfo.m_detailInfo.m_byAge;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kCity ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nCity,sizeof(roleinfo.m_detailInfo.m_nCity)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nCity = roleinfo.m_detailInfo.m_nCity;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kClan ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nClan,sizeof(roleinfo.m_detailInfo.m_nClan)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nClan = roleinfo.m_detailInfo.m_nClan;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kGroup ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nGroup,sizeof(roleinfo.m_detailInfo.m_nGroup)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nGroup = roleinfo.m_detailInfo.m_nGroup;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kQQ ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nQQ,sizeof(roleinfo.m_detailInfo.m_nQQ)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nQQ = roleinfo.m_detailInfo.m_nQQ;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kBlood ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_byBlood,sizeof(roleinfo.m_detailInfo.m_byBlood)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_byBlood = roleinfo.m_detailInfo.m_byBlood;
		}
	}
	
	if( upm.isSetBit( MODI_RoleUPMaskDefine::kBirthDay ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nBirthDay,sizeof(roleinfo.m_detailInfo.m_nBirthDay)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nBirthDay = roleinfo.m_detailInfo.m_nBirthDay;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kVoteCount ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nVoteCount,sizeof(roleinfo.m_detailInfo.m_nVoteCount)) )
		{
			if( roleinfo.m_detailInfo.m_nVoteCount > m_FullData.m_roleInfo.m_detailInfo.m_nVoteCount )
			{
				DWORD nVote = roleinfo.m_detailInfo.m_nVoteCount - m_FullData.m_roleInfo.m_detailInfo.m_nVoteCount;
				// 更新人气值
				m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi += nVote * 1;
				SyncFensiRenqi();

				m_FullData.m_roleInfo.m_detailInfo.m_nVoteCount = roleinfo.m_detailInfo.m_nVoteCount;
			}
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kPersonalSign ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_szPersonalSign,sizeof(roleinfo.m_detailInfo.m_szPersonalSign)) )
		{

		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kWin ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nWin,sizeof(roleinfo.m_detailInfo.m_nWin)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nWin = roleinfo.m_detailInfo.m_nWin;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kLoss ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nLoss,sizeof(roleinfo.m_detailInfo.m_nLoss)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nLoss = roleinfo.m_detailInfo.m_nLoss;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kTie ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nTie,sizeof(roleinfo.m_detailInfo.m_nTie)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nTie = roleinfo.m_detailInfo.m_nTie;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kPefect ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nPefect,sizeof(roleinfo.m_detailInfo.m_nPefect));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kCool ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nCool,sizeof(roleinfo.m_detailInfo.m_nCool));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kGood ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nGood,sizeof(roleinfo.m_detailInfo.m_nGood));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kBad ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nBad,sizeof(roleinfo.m_detailInfo.m_nBad));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kMiss ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nMiss,sizeof(roleinfo.m_detailInfo.m_nMiss));
	}
	
	if( upm.isSetBit( MODI_RoleUPMaskDefine::kCombo ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_nCombo,sizeof(roleinfo.m_detailInfo.m_nCombo));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kExact ) )
	{
		r.Read(&roleinfo.m_detailInfo.m_fExact,sizeof(roleinfo.m_detailInfo.m_fExact));
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kMoney ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nMoney,sizeof(roleinfo.m_detailInfo.m_nMoney)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nMoney = roleinfo.m_detailInfo.m_nMoney;
		}

	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kRMBMoney ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_nRMBMoney,sizeof(roleinfo.m_detailInfo.m_nRMBMoney)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_nRMBMoney = roleinfo.m_detailInfo.m_nRMBMoney;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kChangCount ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_Chang , sizeof(roleinfo.m_detailInfo.m_Chang)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_Chang = roleinfo.m_detailInfo.m_Chang;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kHengCount ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_Heng , sizeof(roleinfo.m_detailInfo.m_Heng)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_Heng = roleinfo.m_detailInfo.m_Heng;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kKeyBoardCount ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_KeyBoard , sizeof(roleinfo.m_detailInfo.m_KeyBoard)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_KeyBoard = roleinfo.m_detailInfo.m_KeyBoard;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kToupiaoCount ) )
	{
		if( r.Read(&roleinfo.m_detailInfo.m_Toupiao , sizeof(roleinfo.m_detailInfo.m_Toupiao)) )
		{
			m_FullData.m_roleInfo.m_detailInfo.m_Toupiao = roleinfo.m_detailInfo.m_Toupiao;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kRoomID ) )
	{
		if( r.Read(&roleinfo.m_baseInfo.m_roomID , sizeof(roleinfo.m_baseInfo.m_roomID)) )
//			&&
//			m_FullData.m_roleInfo.m_baseInfo.m_roomID != roleinfo.m_baseInfo.m_roomID )
		{
			m_FullData.m_roleInfo.m_baseInfo.m_roomID = roleinfo.m_baseInfo.m_roomID;
			bNotifyGameState  = true;
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kConsumeRMB ) )
	{
		DWORD consume_rmb = 0;
		if( r.Read(&consume_rmb,sizeof(consume_rmb)) )
		{
			m_FullData.m_roleExtraData.consume_rmb = consume_rmb;
		}
	}

	if( (upm.isSetBit(MODI_RoleUPMaskDefine::kRoomID)) || (upm.isSetBit(MODI_RoleUPMaskDefine::kGameState)) )
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		if( bNotifyGameState && pMgr && m_timerUpRelGameState( Global::m_stLogicRTime ) )
		{
			struct MODI_NotifyReverse: public MODI_ReverseRelationCallBack
			{
				virtual ~MODI_NotifyReverse(){}
				MODI_NotifyReverse(MODI_OnlineClient * p_client, MODI_NKeyMap::TMAPS_KEY1 mgr_map): p_src(p_client), m_mgr_map(mgr_map){}
		
				bool Exec(MODI_CHARID guid)
				{
					MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr_map.find(guid);
					if(itor != m_mgr_map.end())
					{
						MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
						if( pOnlineClient->IsNeedNotifyWithSbLoginOut( p_src->GetCharID() ) )
						{
							pOnlineClient->OnRelationGameStateChange(p_src);
						}
					}
					return true;
				}
		
				MODI_OnlineClient * p_src;
				MODI_NKeyMap::TMAPS_KEY1 m_mgr_map;

			};

			MODI_NotifyReverse notify_mem(this, pMgr->m_mgr.GetGUIDMapContent());
			this->AllReverseExec(notify_mem);
		}
	}

	if( upm.isSetBit( MODI_RoleUPMaskDefine::kMoney ) )
	{
		/// 金币，单独先保存
		std::ostringstream os;
		os<< "update characters set money=" <<m_FullData.m_roleInfo.m_detailInfo.m_nMoney << " where accountid=" << GetAccid();
		MODI_ZoneServerAPP::ExecSqlToDB(os.str().c_str(), os.str().size());
	}
	
#if 0
	// 不加时间限制很恐怖
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	if( bNotifyGameState && pMgr && m_timerUpRelGameState( Global::m_stLogicRTime ) )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = pMgr->m_mgr.GetGUIDMapContent().begin();
		while( itor != pMgr->m_mgr.GetGUIDMapContent().end() )
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
			inext++;

			MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
			if( pOnlineClient != this )
			{
				pOnlineClient->OnRelationGameStateChange( this );
			}

			itor = inext;
		}
	}
#endif	
}

bool MODI_KickClientOp::Kick( MODI_OnlineClient * pClient , bool bCmdToGS , BYTE byServer , enKickReason Reason )
{
	MODI_World * pWorld = MODI_World::GetInstancePtr();
	pWorld->OnClientLoginout( pClient );
	return true;
}

/** 
 * @brief 系统公告
 * 
 */
bool 	MODI_OnlineClient::SendSystemChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type)
{
	char buf[sizeof(MODI_GS2C_SystemChat_Cmd) + chat_size];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_SystemChat_Cmd * send_cmd = (MODI_GS2C_SystemChat_Cmd *)buf;
	AutoConstruct(send_cmd);
	send_cmd->m_wdSize = chat_size;
	send_cmd->m_eType = notify_type;
	strncpy(send_cmd->m_stContent, chat_content, send_cmd->m_wdSize);
	return SendPackage(send_cmd, sizeof(buf));
}



/** 
 * @brief 用户登录游戏世界所要加载的数据
 * 
 */
void MODI_OnlineClient::OnLoginGameService()
{
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(GetServerID());
	if( !pDB )
	{
		return;
	}
		
	/// 获取用户变量的命令
	MODI_S2RDB_Request_LoadUserVar_Cmd send_cmd;
	send_cmd.m_dwAccountID = GetAccid(); 
	send_cmd.m_byServerID = GetServerID();
	pDB->SendCmd(&send_cmd, sizeof(send_cmd));

	/// 家族成员登陆
	MODI_FamilyManager::GetInstance().FamilyMemLogin(this, IsInWaitReEnterChannelState());

	/// 获取zone_user_var变量
}

/** 
 * @brief 更新家族信息
 * 
 * @param enUpDateCFReason 更新的理由
 * 
 * @return 
 */
void MODI_OnlineClient::UpDateFamilyInfo(MODI_FamilyMember * p_member, enUpDateCFReason reason)
{
	if(! p_member)
	{
		MODI_ASSERT(0);
		return;
	}

	/// 把属性赋给onlineclient
	m_stCFamilyInfo = p_member->GetMemBaseInfo();

	/// 同步到gs
	MODI_ZS2S_Notify_FamilyInfo_Cmd send_cmd;
	send_cmd.m_stCFamilyInfo = m_stCFamilyInfo;
	send_cmd.m_dwAccountID = GetAccid();
	
	/// 以下情况必须通知给所有频道的人
// 	if(reason == enCreateFamily_Reason ||
// 	   reason == enLogin_Reason ||
// 	   reason == enRequest_Reason ||
// 	   reason == enLeaveFamily_Reason ||
// 	   reason == enFireFamily_Reason ||
// 	   reason == enAccept_Reason)
// 	{
// 		send_cmd.m_byNeedCardName = 1;
// 	}
	send_cmd.m_enReason = reason;
	
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( GetServerID());
	if( pServer )
	{
		pServer->SendCmd(&send_cmd, sizeof(send_cmd));
	}
	else
	{
		MODI_ASSERT(0);
	}
}



/** 
 * @brief 是否有家族
 * 
 */
bool MODI_OnlineClient::IsHaveFamily()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是族长
 *
 */
bool MODI_OnlineClient::IsFamilyMaster()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Lead)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是副族长
 * 
 */
bool MODI_OnlineClient::IsFamilySlaver()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_ViceLead)
	{
		return true;
	}
	return false;
}


/** 
 * @brief 是否是正式成员
 * 
 */
bool MODI_OnlineClient::IsFamilyNormal()
{
	if((m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Normal)) ||
	   (m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Lead)) ||
	   (m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_ViceLead)))
	{
		return true;
	}
	return false;
}

/** 
 * @brief 是否是申请成员
 * 
 */
bool MODI_OnlineClient::IsFamilyRequest()
{
	if(m_stCFamilyInfo.m_dwFamilyID != INVAILD_FAMILY_ID && (m_stCFamilyInfo.m_enPosition == (BYTE)enPositionT_Request))
	{
		return true;
	}
	return false;
}


/** 
 * @brief 获取家族id
 * 
 */
const DWORD MODI_OnlineClient::GetFamilyID()
{
	return m_stCFamilyInfo.m_dwFamilyID;
}

/** 
 * @brief 获取家族名字
 * 
 */
const char * MODI_OnlineClient::GetFamilyName()
{
	return m_stCFamilyInfo.m_cstrFamilyName;
}


void MODI_OnlineClient::ChangeFamilyMemRenqi()
{
	if(!IsHaveFamily())
	{
		return;
	}
	
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[change_renqi] on free family failed,not find family <client=%s,familyid=%u>",
							  GetRoleName(), GetFamilyID());
		MODI_ASSERT(0);
		return;
	}

	MODI_FamilyMember * p_member = p_family->FindMember(GetGUID());
	if(! p_member)
	{
		Global::logger->fatal("[change_renqi] not find family member <familyid=%u,name=%s>", GetFamilyID(), GetRoleName());
		return;
	}
	
	p_member->SetRenqi(this);
}


/** 
 * @brief 给用户加金币
 * 
 */
void MODI_OnlineClient::IncMoney(const DWORD money, MODI_ZS2S_Notify_OptMoney::enOptReason reason)
{
	MODI_ZS2S_Notify_OptMoney send_cmd;
	send_cmd.m_charid = GetCharID();
	send_cmd.m_enOpt = MODI_ZS2S_Notify_OptMoney::enOptAddMoney;
	send_cmd.m_dwMoney = money;
	send_cmd.m_enReason = reason;
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer(GetServerID());
	if( pServer )
	{
		pServer->SendCmd(&send_cmd, sizeof(send_cmd));
	}
}


/** 
 * @brief 给用户减金币
 * 
 */
void MODI_OnlineClient::DecMoney(const DWORD money, MODI_ZS2S_Notify_OptMoney::enOptReason reason)
{
	MODI_ZS2S_Notify_OptMoney send_cmd;
	send_cmd.m_charid = GetCharID();
	send_cmd.m_enOpt = MODI_ZS2S_Notify_OptMoney::enOptSubMoney;
	send_cmd.m_dwMoney = money;
	send_cmd.m_enReason = reason;
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer(GetServerID());
	if( pServer )
	{
		pServer->SendCmd(&send_cmd, sizeof(send_cmd));
	}
}


/** 
 * @brief 超链接名字
 * 
 */
const char * MODI_OnlineClient::GetRoleLinkName()
{
	std::string name = GetRoleName();
	std::string link_name = "{@" + name + "}";
	return link_name.c_str();
}


/** 
 * @brief 直接写数据库
 * 
 */
void MODI_OnlineClient::SaveMoneyRmb()
{
	DWORD en_size = 0;
	char buf[255];
	memset(buf, 0, sizeof(buf));
	PublicFun::EncryptMoney(buf, en_size, GetMoneyRmb(), GetAccName());

	MODI_Record record_update;
	std::ostringstream where;
	where << "accname=\'" <<GetAccName() << "\'";
	record_update.Put("moneyRMB", GetMoneyRmb());
	record_update.Put("otherdata", buf, en_size);
	record_update.Put("where", where.str());
	MODI_ByteBuffer result(1024);
	if(! MODI_DBClient::MakeUpdateSql(result, MODI_ZoneServerAPP::m_pCharactersTbl, &record_update))
	{
		Global::logger->error("[save_moneyrmb] make sql failed");
		MODI_ASSERT(0);
		return;
	}

	ExecSqlToDB((const char * )result.ReadBuf(), result.ReadSize());
}


/** 
 * @brief 请求执行sql
 * 
 * @param sql sql语句
 * @param sql_len 语句长度
 *
 */
void MODI_OnlineClient::ExecSqlToDB(const char * sql, const WORD sql_len)
{
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( GetServerID() );
	if( !pDB )
	{
		Global::logger->info("[request_exec_sql] can't get db interface");
		return;
	}

	char cmd_buf[sql_len + sizeof(MODI_S2RDB_Request_ExecSql_Cmd) + 2];
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_S2RDB_Request_ExecSql_Cmd * p_send_cmd = (MODI_S2RDB_Request_ExecSql_Cmd *)cmd_buf;
	AutoConstruct(p_send_cmd);
	memcpy(p_send_cmd->m_Sql, sql, sql_len);
	p_send_cmd->m_wdSize = sql_len;
	pDB->SendCmd(p_send_cmd, p_send_cmd->GetSize());
}


/** 
 * @brief 创建变量
 * 
 * @param var_name 变量名
 * @param var_attribute 变量属性
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_OnlineClient::CreateUserVar(const char * var_name, const char * var_attribute)
{
	MODI_ZoneUserVar * p_var = new MODI_ZoneUserVar(var_name);
	if(p_var->Init(this, var_attribute))
	{
		if(! (this->m_stZoneUserVarMgr.AddVar(p_var)))
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
			return false;
		}
		else
		{
			p_var->SaveToDB();
		}
	}
	else
	{
		delete p_var;
		p_var = NULL;
		MODI_ASSERT(0);
		return true;
	}
	return true;
}


/** 
 * @brief 修改gmlevel
 * 
 * @param gm_level 
 */
void MODI_OnlineClient::SetGmLevel(const BYTE gm_level)
{
	m_FullData.m_roleExtraData.m_iGMLevel = gm_level;
	MODI_ZS2S_Notify_Modi_GMLevel send_cmd;
	send_cmd.m_charid = GetCharID();
	send_cmd.m_byLevel = gm_level;
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer(GetServerID());
	if( pServer )
	{
		pServer->SendCmd(&send_cmd, sizeof(send_cmd));
	}
}


/** 
 * @brief 增加一个反向社会关系
 * 
 */
void MODI_OnlineClient::AddReverseRelation(MODI_CHARID guid)
{
	m_ReverseRelationSet.insert(guid);
}


/** 
 * @brief 移除一个反向社会关系
 * 
 */
void MODI_OnlineClient::RemoveReverseRelation(MODI_CHARID guid)
{
	std::set<MODI_CHARID>::iterator iter = m_ReverseRelationSet.find(guid);
	if(iter != m_ReverseRelationSet.end())
	{
		m_ReverseRelationSet.erase(iter);
	}
}


/** 
 * @brief 通知反向的社会关系
 * 
 */
void MODI_OnlineClient::AllReverseExec(MODI_ReverseRelationCallBack & call_back)
{
	if(m_ReverseRelationSet.size() == 0)
	{
		return;
	}
	
	std::set<MODI_CHARID>::iterator iter = m_ReverseRelationSet.begin();
	for(; iter != m_ReverseRelationSet.end(); iter++)
	{
		call_back.Exec(*iter);
	}
}


bool MODI_OnlineClient::InitMail(const MODI_DBMaillist & dbmails)
{
	m_stUserMailsMgr.Init(this, &(dbmails.m_Mails[0]));
	m_stSvrMailsMgr.Init(this, &(dbmails.m_ServerMail[0]));

	/// 发送到客户端
	m_stUserMailsMgr.SendUserMailsToClient();

	/// 执行系统邮件脚本
	m_stSvrMailsMgr.ExecuteSvrMails();

	return true;
}


bool MODI_OnlineClient::MailIsFull()
{
	return m_stUserMailsMgr.IsFull();
}

bool MODI_OnlineClient::AddUserMail(MODI_Mail * pMail)
{
	if(pMail)
	{
		return m_stUserMailsMgr.AddMail(pMail);
	}
	return false;
}


bool MODI_OnlineClient::AddServerMail(MODI_Mail * pMail)
{
	if(pMail)
	{
		return m_stSvrMailsMgr.AddMail(pMail);
	}
	return false;
}

MODI_Mail * MODI_OnlineClient::GetUserMail(const MODI_GUID & id)
{
	return m_stUserMailsMgr.GetMails(id);
}


bool MODI_OnlineClient::DelUserMail(const MODI_GUID & id)
{
	return m_stUserMailsMgr.DelMail(id);
}
