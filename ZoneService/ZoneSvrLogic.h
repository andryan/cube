/** 
 * @file ZoneSvrLogic.h
 * @brief zs服务器逻辑线程
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef ZONESVR_LOGIC_THREAD_H_
#define ZONESVR_LOGIC_THREAD_H_

#include "Type.h"
#include "Timer.h"
#include "Thread.h"
#include "SingleObject.h"
#include "RecurisveMutex.h"


class MODI_ZoneSvrLogic: public MODI_Thread , 
	public CSingleObject<MODI_ZoneSvrLogic>
{

public:

    /// 主循环
    virtual void Run();

	void Final();

    /// 构造
    MODI_ZoneSvrLogic();

    /// 析构
    virtual ~MODI_ZoneSvrLogic();


private:

	MODI_Timer 	m_timerSyncCl;
	MODI_Timer  m_timerItemSerialSave;
	MODI_TTime m_stTTime;
};

#endif
