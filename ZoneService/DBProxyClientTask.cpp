/** 
 * @file DBProxyClientTask.cpp
 * @brief dxproxy客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#include "DBProxyClientTask.h"
#include "PackageHandler_rdb2zs.h"
#include "s2rdb_cmd.h"



MODI_SvrPackageProcessBase 	 MODI_DBProxyClientTask::ms_ProcessBase;
MODI_DBProxyClientTask * 	MODI_DBProxyClientTask::ms_pDBProxy[255];

void 	MODI_DBProxyClientTask::Init()
{
	memset( ms_pDBProxy , 0 , sizeof(ms_pDBProxy) );
}

bool 	MODI_DBProxyClientTask::RegServer( BYTE byServer ,  MODI_DBProxyClientTask * p )
{
	if( ms_pDBProxy[byServer] )
	{
		Global::logger->warn("[%s] dbproxy<id=%u> dup register. " , SVR_TEST ,
				byServer );

//		MODI_DBProxyClientTask * pOld = ms_pDBProxy[byServer];
//		pOld->Terminate();
		return false;
	}

	Global::logger->debug("-->>>>>>>>>register server=%u", byServer);
	ms_pDBProxy[byServer] = p;
	return true;
}

MODI_DBProxyClientTask * MODI_DBProxyClientTask::FindServer(BYTE byServer)
{
	return ms_pDBProxy[byServer];
}

// 连接上的回调,可以安全的在逻辑线程调用
void MODI_DBProxyClientTask::OnConnection()
{
}

// 断开的回调，可以安全的在逻辑线程调用
void MODI_DBProxyClientTask::OnDisconnection()
{
	if( this == ms_pDBProxy[0] )
	{
		// zoneserver db proxy
		ms_pDBProxy[0] = 0;
		if( m_Type != kZoneServerProxy )
		{
			Global::logger->error("[%s] db proxy disconnection.but type is error.index<%u> must be ZoneServerProxy " , 
					SVR_TEST , 0 );
			MODI_ASSERT( 0 );
		}
		return ;
	}

	if( m_Type != kGameServerProxy )
	{
		Global::logger->error("[%s] db proxy disconnection.but type is error.index not eq 0 must be GameServerProxy " , 
					SVR_TEST );
		MODI_ASSERT(0);

	}

	for( int i = 1; i < 255; i++ )
	{
		if( ms_pDBProxy[i] == this )
		{
			// 相关频道服务器不能继续提供服务，断开
			ms_pDBProxy[i] = 0;
		}
	}
}

void 	MODI_DBProxyClientTask::ProcessAllPackage()
{
	ms_ProcessBase.ProcessAllPackage();
}

MODI_DBProxyClientTask::MODI_DBProxyClientTask( const int sock, const struct sockaddr_in * addr):
	MODI_IServerClientTask( sock , addr )
{
	Resize(ZSTOGS_CMDSIZE);
}

MODI_DBProxyClientTask::~MODI_DBProxyClientTask()
{

}


bool MODI_DBProxyClientTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
		return false;

	this->Put( pt_null_cmd , cmd_size );
	return true;
}

bool MODI_DBProxyClientTask::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	///	游戏命令的处理
	int iRet = MODI_PackageHandler_rdb2zs::GetInstancePtr()->DoHandlePackage(
					ptNullCmd->byCmd, 
					ptNullCmd->byParam, 
					this,
					ptNullCmd,
					dwCmdLen);

	if( iRet == enPHandler_Ban )
	{

	}
	else  if ( iRet == enPHandler_Kick )
	{

	}
	else if( iRet == enPHandler_Warning )
	{

	}

	return true;

}

int MODI_DBProxyClientTask::RecycleConn()
{
	if( this->IsCanDel() )
		return 1;
	return 0;
}

void MODI_DBProxyClientTask::DisConnect()
{
	Global::logger->info("[%s] remote client<ip=%s,port=%u> disconnectioned. " , 
			ERROR_CON ,
			this->GetIP() , 
			this->GetPort() );
}
