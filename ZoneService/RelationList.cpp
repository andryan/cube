#include "RelationList.h"
#include "Base/HelpFuns.h"
#include "Base/AssertEx.h"
#include "OnlineClientMgr.h"
#include "s2rdb_cmd.h"
#include "Global.h"
#include "protocol/c2gs_relation.h"
#include "ZoneServerAPP.h"

MODI_RelationList::MODI_RelationList():m_pClient(0)
{
	m_nIdolSize = 0;
	m_nBlackSize = 0;
	m_nFriendSize = 0;
	m_pLover = 0;
	m_pMarried = 0;
	m_bInit = false;
}
	
MODI_RelationList::~MODI_RelationList()
{


}
bool	MODI_RelationList::LoadFromDB( MODI_OnlineClient * pClient ,MODI_DBRelationInfo * array,WORD num, bool is_load)
{
// 	if( m_pClient )
// 	{
// 		MODI_ASSERT( 0 );
// 		return false;
// 	}

	m_pClient = pClient;
//	const MODI_CharactarFullData * pFullData = (MODI_CharactarFullData *)(p);


	// 加载联系人数据
	for( int i = 0; i < num; i++ )
	{
		if(array[i].m_guid.IsInvalid() )
			continue ;

		MODI_Relation * pRel = MODI_RelationPool::New();
		if( !pRel )
		{
			MODI_ASSERT(0);
			continue ;
		}

		pRel->Init( array[i]);
		pRel->SetOnline( false );
		
		if( AddToList( m_Rels , pRel->GetCharID() , pRel ) == false )
		{
			int iRelType = (int)pRel->GetInfo().m_RelationType;
			Global::logger->error("[%s] character<%s>'s relation list has dup item.<name=%s,charid=%u,relid=%u,reltype=%d>." ,
				SVR_TEST ,
				m_pClient->GetRoleName() ,
				pRel->GetName() ,
				m_pClient->GetCharID(),
				pRel->GetCharID() ,
			   	iRelType );

			MODI_ASSERT(0);

			MODI_RelationPool::Delete( pRel );
		}
	}

	if(!is_load)
		return true;

	LIST_RELATIONS_ITER itor = m_Rels.begin();
	while( itor != m_Rels.end() )
	{
		MODI_Relation * pTemp = itor->second;
		if( pTemp->IsBlack() )
		{
			m_nBlackSize += 1;
			
			MODI_ASSERT( pTemp->IsFriend() == false );
			MODI_ASSERT( pTemp->IsLover() == false );
			MODI_ASSERT( pTemp->IsMarried() == false );
			MODI_ASSERT( pTemp->IsIdol() == false );

		}
		else if( pTemp->IsFriend() )
		{
			m_nFriendSize += 1;
		}
		else if( pTemp->IsLover() )
		{
			MODI_ASSERT( m_pLover == 0 );
			m_pLover = pTemp;
			m_nFriendSize += 1;
		}
		else if( pTemp->IsMarried() )
		{
			MODI_ASSERT( m_pMarried == 0 );
			m_pMarried = pTemp;
			m_nFriendSize += 1;
		}

		if( pTemp->IsIdol() )
		{
			m_nIdolSize += 1;
		}

		itor++;
	}

	m_bInit = true;
	return true;
}

bool MODI_RelationList::Init( MODI_OnlineClient * pClient , const void * p )
{
	if( m_pClient )
	{
		MODI_ASSERT( 0 );
		return false;
	}


	return true;
}

bool MODI_RelationList::AddRelation( MODI_Relation * pRel ,enRelOptType opt , enRelationRetType & ret )
{
	if( AddToList( m_Rels , pRel->GetCharID() , pRel ) == false )
		return false;

	if( opt == enRelOpt_AddF ) // 添加好友操作
	{
		ret = ImplAddFriend( pRel );
	}
	else if( opt == enRelOpt_AddB ) // 添加黑名单操作
	{
		ret = ImplAddBlack( pRel );
	}
	else if( opt == enRelOpt_AddIdol )
	{
		ret = ImplAddIdol( pRel );
	}

	if( ret != enRelationRetT_Ok )
	{
		DelFromList( m_Rels , pRel->GetCharID() );
		return false;
	}

	return true;
}

bool MODI_RelationList::IsInList( LIST_RELATIONS & l , MODI_CHARID id )
{
	LIST_RELATIONS_ITER it = l.find( id );
	if( it != l.end() )
	{
		return true;
	}

	return false;
}

bool MODI_RelationList::IsInList( LIST_RELATIONS & l , const char * szName , size_t nSize )
{
	LIST_RELATIONS_ITER it = l.begin();
	while( it != l.end() )
	{
		MODI_Relation * pTemp = it->second;
		if( pTemp && strncmp( pTemp->GetName() , szName , nSize ) == 0 )
		{
			return  true;
		}
		it++;
	}

	return false;
}

bool MODI_RelationList::AddToList( LIST_RELATIONS & l , MODI_CHARID id , MODI_Relation * rel )
{
	if( id == INVAILD_CHARID || !rel )
		return false;
	LIST_RELATIONS_INSERT_RESULT res = m_Rels.insert(std::make_pair( id , rel ));
	return res.second;
}

MODI_Relation * MODI_RelationList::DelFromList( LIST_RELATIONS & l , MODI_CHARID id )
{
	LIST_RELATIONS_ITER it = l.find(id);
	if( it != l.end() )
	{
		MODI_Relation * pTemp = it->second;
		l.erase( it );
		return pTemp;
	}
	return 0;
}

MODI_Relation * MODI_RelationList::FindInList( LIST_RELATIONS & l , MODI_CHARID id )
{
	LIST_RELATIONS_ITER it = l.find( id );
	if( it != l.end() )
	{
		return it->second;
	}

	return 0;
}

MODI_Relation * MODI_RelationList::FindInList( LIST_RELATIONS & l , const char * szName , size_t nSize )
{
	LIST_RELATIONS_ITER it = l.begin();
	while( it != l.end() )
	{
		MODI_Relation * pTemp = it->second;
		if( pTemp && strncmp ( pTemp->GetName() , szName , nSize ) == 0 )
			return pTemp;
		it++;
	}

	return 0;
}

void MODI_RelationList::CleanUpList( LIST_RELATIONS & l )
{
	LIST_RELATIONS_ITER it = l.begin();
	while( it != l.end() )
	{
		MODI_Relation * pTemp = it->second;
		if( pTemp )
		{
			MODI_RelationPool::Delete( pTemp );
#ifdef _XXP_DEBUG
#endif
		}
		it++;
	}
	l.clear();
}

void MODI_RelationList::DelRelation( MODI_CHARID  charid )
{
	MODI_Relation * pTemp = DelFromList( m_Rels , charid );
	if( pTemp )
	{
		if( pTemp->IsBlack() && m_nBlackSize > 0 )
		{
			m_nBlackSize -= 1;
		}
		else if( pTemp->IsFriend() && m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
		}
		else if( pTemp->IsLover() && m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
			m_pLover = 0;
		}
		else if( pTemp->IsMarried() && m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
			m_pMarried = 0;
		}

		if( pTemp->IsIdol() && m_nIdolSize > 0 )
		{
			m_nIdolSize -= 1;
		}
		OnRelieve( pTemp );
		MODI_RelationPool::Delete( pTemp );
	}
}

bool MODI_RelationList::IsRelation( const char * szName , size_t nNameLen )
{
	if( FindRelation( szName , nNameLen ) )
		return true;
	return false;
}

MODI_Relation * MODI_RelationList::FindRelation( MODI_CHARID charid )
{
	MODI_Relation * pRet = this->FindInList( m_Rels , charid );
	return pRet;
}

MODI_Relation * MODI_RelationList::FindRelation( const char * szName , size_t nNameLen )
{
	MODI_Relation * pRet = this->FindInList( m_Rels , szName , nNameLen );
	return pRet;
}

size_t MODI_RelationList::GetFriendSize() const
{
	return m_nFriendSize;
}

size_t MODI_RelationList::GetBlackSize() const
{
	return m_nBlackSize;
}

size_t MODI_RelationList::GetIdolsSize() const
{
	return m_nIdolSize;
}

bool MODI_RelationList::IsFriendFull() const
{
	if(!m_pClient)
	{
		MODI_ASSERT(0);
		return true;
	}
	if( m_pClient->GetFullData().m_roleInfo.m_baseInfo.m_bVip > 0)
	{
		return GetFriendSize() >= MAX_VIPFRIENDSCOUNT ;
	}
	return GetFriendSize() >= MAX_FRIENDCOUNT;
}

bool MODI_RelationList::IsBlackFull() const
{
	return GetBlackSize() >= MAX_BLACKCOUNT;
}

bool MODI_RelationList::IsIdolsFull() const
{
	if(!m_pClient)
	{
		MODI_ASSERT(0);
		return true;
	}
	
	if( m_pClient->GetFullData().m_roleInfo.m_baseInfo.m_bVip > 0)
	{
		return GetIdolsSize() >= MAX_VIPIDOLCOUNT;
	}
	return GetIdolsSize() >= MAX_IDOLCOUNT;
}

size_t MODI_RelationList::GetRelationSize() const
{
	return m_Rels.size();
}

void MODI_RelationList::CleanUp()
{
#ifdef _XXP_DEBUG
#endif
	Global::logger->debug("[relation_list] clean up <%d> relaition list \n",m_Rels.size());
	CleanUpList( m_Rels );
}
		
void MODI_RelationList::SaveListToDB( LIST_RELATIONS & l , void * p )
{
	MODI_S2RDB_Request_SaveRelation * pMsg = (MODI_S2RDB_Request_SaveRelation *)(p);

	LIST_RELATIONS_ITER it = l.begin(); 
	while( it != l.end() )
	{
		MODI_Relation * pRel = it->second;
		if( pRel && pRel->IsNeedSave())
		{
#ifdef _DEBUG
			if( pRel->IsBlack() )
			{
				MODI_ASSERT( pRel->IsFriend() == false &&
					pRel->IsIdol() == false &&
					pRel->IsLover() == false &&
					pRel->IsMarried() == false );
			}

#endif

			pRel->ClearNeedSave();
			MODI_DBRelationInfo * pTemp = pMsg->m_pData + pMsg->m_nSize;
			*pTemp = pRel->GetInfo();
			pMsg->m_nSize += 1;
		}
		it++;
	}
}

void MODI_RelationList::SaveToDB()
{
	if( !m_pClient )
		return ;

	MODI_DBProxyClientTask * pRDB = GetApp()->GetDBInterface( m_pClient->GetServerID() );
	if( !pRDB )
		return ;

	int i = 0;
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );

	MODI_S2RDB_Request_SaveRelation * pMsg = (MODI_S2RDB_Request_SaveRelation *)(szPackageBuf);
	AutoConstruct( pMsg );
	pMsg->m_Charid = m_pClient->GetCharID();

	for( i = 0; i < RELATIONS_RELIEVE_MAX; i++ )
	{
		if( GUID_LOPART(m_RelieveRelations[i].m_guid) != INVAILD_CHARID )
		{
			if( IsInList( m_Rels , GUID_LOPART(m_RelieveRelations[i].m_guid) ) )
				continue;

			MODI_DBRelationInfo * pTemp = pMsg->m_pData + pMsg->m_nSize;
			*pTemp = m_RelieveRelations[i];
			pTemp->m_bIsValid = 0;
			pMsg->m_nSize += 1;
		}
	}

	// 重置
	memset( m_RelieveRelations , INVAILD_CHARID , sizeof(m_RelieveRelations) );

	// 保存现在联系人的信息
	SaveListToDB( m_Rels , pMsg );

	// send to DB
	if( pRDB && pMsg->m_nSize > 0 )
	{
		int iSendSize = pMsg->m_nSize * sizeof(MODI_DBRelationInfo) + 
			sizeof(MODI_S2RDB_Request_SaveRelation);
		pRDB->SendCmd( pMsg , iSendSize );
	}
}

void MODI_RelationList::OnRelieve( MODI_Relation * pRel )
{
	if( pRel->GetCharID() == INVAILD_CHARID )
		return ;

	int iFree = -1;
	for( int i = 0; i < RELATIONS_RELIEVE_MAX; i++ )
	{
		if( GUID_LOPART(m_RelieveRelations[i].m_guid) == pRel->GetCharID() )
		{
			return ;
		}
		else if( iFree == -1 && GUID_LOPART(m_RelieveRelations[i].m_guid) == INVAILD_CHARID )
		{
			iFree = i;
		}
	}

	if( iFree == -1 )
	{
		// 缓存满，刷到DB
		SaveToDB();
		iFree = 0;
	}

	// 取头保存
	m_RelieveRelations[iFree].m_guid = pRel->GetGUID();
	m_RelieveRelations[iFree].m_bIsValid = 0;
	m_RelieveRelations[iFree].m_bySex = pRel->GetSex();
	safe_strncpy( m_RelieveRelations[iFree].m_szName , pRel->GetName() , sizeof(m_RelieveRelations[iFree].m_szName) );
	m_RelieveRelations[iFree].m_RelationType = pRel->GetInfo().m_RelationType;
}
		
void MODI_RelationList::FlushToFullData( MODI_CharactarFullData & fulldata )
{
/*	size_t n = 0;
	LIST_RELATIONS_ITER itor = m_Rels.begin();
	while( itor != m_Rels.end() )
	{
		MODI_Relation * pTemp = itor->second;
	//	fulldata.m_Relations[n] = pTemp->GetInfo();
		n++;
		itor++;
	}
	*/
}

void MODI_RelationList::SyncRelationList(MODI_OnlineClient * p_client)
{
	if(!p_client)
		return;
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_GS2C_Notify_RelationList * pMsg = (MODI_GS2C_Notify_RelationList *)(szPackageBuf);
	AutoConstruct( pMsg );
	//	GetRelationList().BuildRelationListNotifyPackage( pMsg );
	pMsg->m_nSize = 0;
	MODI_RelationInfo * p_startaddr = &(pMsg->m_pData[0]);
	
	LIST_RELATIONS_ITER itor = m_Rels.begin();
	for(; itor != m_Rels.end(); itor++)
	{
		MODI_Relation * pTemp = itor->second;
		*p_startaddr = pTemp->GetInfo();
		pMsg->m_nSize++;
		p_startaddr++;

		unsigned int check_size = sizeof(MODI_GS2C_Notify_RelationList) + pMsg->m_nSize * sizeof(MODI_RelationInfo);
		if(check_size >= Skt::MAX_USERDATASIZE - 256)
		{
			p_client->SendPackage( pMsg , check_size );
			memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
			AutoConstruct( pMsg );
			pMsg->m_nSize = 0;
			p_startaddr = &(pMsg->m_pData[0]);
		}
		
	}

	int iSendSize = sizeof(MODI_GS2C_Notify_RelationList) + pMsg->m_nSize * sizeof(MODI_RelationInfo);
	p_client->SendPackage( pMsg , iSendSize );
}

void MODI_RelationList::OnListChanged()
{
}

enRelationRetType MODI_RelationList::ImplAddFriend( MODI_Relation * pRel )
{
	if( pRel->IsFriend() )
		return enRelationRetT_Add_AlreadyIsF;

	// 在黑名单中,无法添加好友
	if( pRel->IsBlack() )
		return  enRelationRetT_AddF_IsInBlack;

	// 好友列表已经满
	if( IsFriendFull() )
		return enRelationRetT_AddF_Full;

	pRel->SetFriend();

	m_nFriendSize += 1;
//	MODI_ASSERT( m_nFriendSize <= MAX_FRIENDCOUNT );

	return enRelationRetT_Ok;
}

enRelationRetType MODI_RelationList::ImplDelFriend( MODI_Relation * pRel )
{
	// 无法删除好友，不存在于好友列表中
	if( !pRel->IsFriend() )
		return enRelationRetT_DelF_NotExist;

	if( pRel->IsLover() )
	{
		MODI_ASSERT( pRel->IsMarried() == false );
		m_pLover = 0;
	}
	else if( pRel->IsMarried() )
	{
		MODI_ASSERT( pRel->IsLover() == false );
		m_pMarried = 0;
	}

	pRel->ClearFBLM();

	MODI_ASSERT( m_nFriendSize > 0 );
	if( m_nFriendSize > 0 )
	{
		m_nFriendSize -= 1;
	}
//	MODI_ASSERT( m_nFriendSize <= MAX_FRIENDCOUNT );

	return enRelationRetT_Ok;
}

enRelationRetType MODI_RelationList::ImplAddIdol( MODI_Relation * pRel )
{
	// 已经是偶像
	if( pRel->IsIdol() )
		return enRelationRetT_Add_AlreadyIsI;

	// 存在于黑名单中，无法添加偶像
	if( pRel->IsBlack() )
		return enRelationRetT_AddI_IsInBlack;

	// 偶像列表满
	if( IsIdolsFull() )
		return enRelationRetT_AddI_Full;

	// 偶像添加间隔时间还没到
	if( m_pClient->IsAddIdolTimeOver() == false )
		return enRelationRetT_AddI_ArriveTime;

	if(m_pClient->GetFullDataModify().m_roleInfo.m_baseInfo.m_ucLevel < 10)
	{
		std::string m_send_cmd = "Add idol failed,the role can't add idol when it's level less than 10";
		m_pClient->SendSystemChat(m_send_cmd.c_str(), m_send_cmd.size());
		return enRelationRetT_AddI_ArriveTime;
		//return enRelationRetT_AddI_Level;
	}

	pRel->SetIdol();

	m_nIdolSize += 1;

	//MODI_ASSERT( m_nIdolSize <= MAX_IDOLCOUNT );
	// 更新最后次添加偶像的时间

	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	time_t t = ptm->GetSavedANSITime();
	m_pClient->SetLastIdolTime( t );

	return enRelationRetT_Ok;
}

enRelationRetType MODI_RelationList::ImplDelIdol( MODI_Relation * pRel )
{
	// 不存在于偶像名单中
	if( !pRel->IsIdol() )
		return enRelationRetT_DelI_NotExist;

	// 取消偶像关系
	pRel->ClearIdol();

	MODI_ASSERT( m_nIdolSize > 0 );
	if( m_nIdolSize > 0 )
		m_nIdolSize -= 1;
//	MODI_ASSERT( m_nIdolSize <= MAX_IDOLCOUNT );

	return enRelationRetT_Ok;
}

enRelationRetType MODI_RelationList::ImplAddBlack( MODI_Relation * pRel )
{
	// 已经存在于黑名单中
	if( pRel->IsBlack() )
		return enRelationRetT_Add_AlreadyIsB;

	// 黑名单满
	if( IsBlackFull() )
		return enRelationRetT_AddB_Full;

	if( pRel->IsIdol() )
	{
		if( m_nIdolSize > 0 )
		{
			m_nIdolSize -= 1;
		}
		else 
		{
			MODI_ASSERT(0);
		}
	}
	if( pRel->IsFriend() )
	{
		if( m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
		}
		else 
		{
			MODI_ASSERT(0);
		}
	}
	else if( pRel->IsLover() )
	{
		m_pLover = 0;
		if( m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
		}
		else 
		{
			MODI_ASSERT(0);
		}
	}
	else if( pRel->IsMarried() )
	{
		m_pMarried = 0;
		if( m_nFriendSize > 0 )
		{
			m_nFriendSize -= 1;
		}
		else 
		{
			MODI_ASSERT(0);
		}
	}

	pRel->SetBlack();
	m_nBlackSize += 1;
	MODI_ASSERT( m_nBlackSize <= MAX_BLACKCOUNT );

	return enRelationRetT_Ok;
}

enRelationRetType MODI_RelationList::ImplDelBlack( MODI_Relation * pRel )
{
	if( !pRel->IsBlack() )
		return enRelationRetT_DelB_NotExist;

	pRel->ClearAllRelationType();

	MODI_ASSERT( m_nBlackSize > 0 );
	if( m_nBlackSize  > 0 )
		m_nBlackSize -= 1;

	return enRelationRetT_Ok;
}


MODI_Relation * MODI_RelationList::ModfiyRel( MODI_CHARID charid , enRelOptType opt , enRelationRetType & ret )
{
	// 是否存存在该联系人
	MODI_Relation * pRel = FindRelation( charid );
	if( !pRel )
	{
		ret = enRelationRetT_Mod_NotExist;
		return pRel;
	}
		
	ret = enRelationRetT_Ok;

	if( opt == enRelOpt_AddF ) // 添加好友操作
	{
		ret = ImplAddFriend( pRel );
	}
	else if( opt == enRelOpt_DelF ) // 删除好友操作
	{
		ret = ImplDelFriend( pRel );
	}
	else if( opt == enRelOpt_AddB ) // 添加黑名单操作
	{
		ret = ImplAddBlack( pRel );
	}
	else if( opt == enRelOpt_DelB )
	{
		ret = ImplDelBlack( pRel );
	}
	else if( opt == enRelOpt_AddIdol )
	{
		ret = ImplAddIdol( pRel );
	}
	else if( opt == enRelOpt_DelIdol )
	{
		ret = ImplDelIdol( pRel );
	}

	return pRel;
}

WORD	MODI_RelationList::DeleteFriendTo( WORD num)
{
	return DelRelationTo( num, enRelationT_Friend);
}

WORD	MODI_RelationList::DeleteIdoleTo( WORD num)
{
	return DelRelationTo( num , enRelationT_Idol);
}



WORD	MODI_RelationList::DelRelationTo( WORD num, enRelationType type)
{

	WORD	current_num =  m_nFriendSize;
	enRelOptType  revertype = enRelOpt_DelF;


	if( type == enRelationT_Idol)
	{
		current_num = m_nIdolSize;
		revertype = enRelOpt_DelIdol;
	}

	if( num >= current_num)
		return 0;
	
	WORD	i=0;
	WORD	numdel=0;

	LIST_RELATIONS   dellist;
	LIST_RELATIONS_ITER  iter = m_Rels.begin();

	for( ; iter != m_Rels.end(); ++iter)
	{
		
		MODI_Relation * pTemp = iter->second;
		bool	del=false;

		if( type == enRelationT_Friend&&  pTemp->IsFriend())
		{
			del =true;
		}
		if( type == enRelationT_Idol && pTemp->IsIdol())
		{
			del = true;
		}

		if( del )
		{
			dellist.insert(std::map<MODI_CHARID,MODI_Relation *>::value_type(pTemp->m_info.m_dwRelid,pTemp));
		}
	}
	
	for( iter = dellist.begin(); iter != dellist.end(); ++iter)
	{
		i++;		
		if( i > num )
		{
			enRelationRetType  ret;
			MODI_Relation * pTemp = iter->second;
			MODI_CHARID  charid = pTemp->GetCharID();
			/// 删除这个好友	
			MODI_Relation * relation=ModfiyRel( pTemp->GetCharID(),revertype, ret);
			if( relation  && ret == enRelationRetT_Ok)
			{

				if( relation->IsHasRelation() == false)
				{
					DelRelation( charid);
					/// 删除反向关系
					MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
					MODI_OnlineClient * pTargetClient = pMgr->FindByGUID( charid );
					if( pTargetClient )
					{
						pTargetClient->RemoveReverseRelation(m_pClient->GetCharID());
					}

				}	
				relation->SetNeedSave();
			}
			++numdel;
		}
	}

	OnListChanged();

	return numdel;
}


















