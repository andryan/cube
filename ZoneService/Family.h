/**
 * @file   Family.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Apr 23 17:28:21 2011
 * 
 * @brief  家族类
 * 
 * 
 */

#ifndef _MD_FAMILY_H
#define _MD_FAMILY_H

#include "FamilyMember.h"

/** 
 * @brief 家族成员回调
 * 
 * 
 */
struct MODI_FamilyMemberCallback
{
	virtual ~MODI_FamilyMemberCallback(){}
	virtual void Exec(MODI_FamilyMember * p_member) = 0;
};


/**
 * @brief 家族类
 * 
 */
class MODI_Family
{
 public:
	typedef std::map<MODI_GUID, MODI_FamilyMember * > defFamilyMemMap;
	typedef std::map<MODI_GUID, MODI_FamilyMember * >::value_type defFamilyMemMapValue;
	typedef std::map<MODI_GUID, MODI_FamilyMember * >::iterator defFamilyMemMapIter;
	
	MODI_Family(const MODI_FamilyBaseInfo & base_info);
	MODI_Family(const MODI_FamilyBaseInfo & base_info, MODI_OnlineClient * p_client);

	~MODI_Family()
	{
		Final();
	}

	defFamilyID & GetFamilyID()
	{
		return m_stBaseInfo.m_dwFamilyID;
	}

	const char * GetFamilyName()
	{
		return m_stBaseInfo.m_cstrFamilyName;
	}

	void SetMaster(MODI_FamilyMember * p_member)
	{
	  m_stMasterID = p_member->GetGuid();
	  m_strMasterName = p_member->GetRoleName();
	}

	const MODI_GUID & GetMasterID()
	{
	  return m_stMasterID;
	}

	const char * GetMasterName()
	{
	  return m_strMasterName.c_str();
	}


	/** 
	 * @brief 创建一个家族
	 * 
	 * @param p_client 创建的人
	 * 
	 * @return 成功与失败
	 *
	 */
	bool OnCreate(MODI_OnlineClient * p_client);


	/** 
	 * @brief 某人请求申请家族
	 * 
	 * @param p_client 申请人
	 * 
	 * @return 成功true,失败false
	 *
	 */
	enFamilyOptResult OnRequest(MODI_OnlineClient * p_client);


	/** 
	 * @brief 某人取消家族申请
	 * 
	 * 
	 */
	bool OnCancel(MODI_OnlineClient * p_client);


	/** 
	 * @brief 离开家族
	 * 
	 * @param p_client 离开的人 
	 * 
	 */
	enFamilyOptResult OnLeave(MODI_OnlineClient * p_client);


	/** 
	 * @brief 拒绝某个人加入家族
	 * 
	 * @param refuse_id 拒绝的id
	 * 
	 * @return 成功与失败
	 *
	 */
	bool OnRefuse(const MODI_GUID & refuse_id);


	/** 
	 * @brief 同意某人加入
	 * 
	 * @param accept_id 加入的id
	 * 
	 */
	bool OnAccept(const MODI_GUID & accept_id);
	


	/** 
	 * @brief 开除某人
	 * 
	 */
	bool OnFire(const MODI_GUID & fire_id);


	/** 
	 * @brief 家族升级
	 * 
	 * 
	 */
	bool OnLevelup(MODI_OnlineClient * p_client);


	/** 
	 * @brief 家族升级
	 * 
	 */
	bool Levelup();

	
	/** 
	 * @brief 是否可以升级
	 * 
	 * 
	 * @return 可以true
	 */
	enFamilyOptResult IsCanLevelup();
		
	/** 
	 * @brief 增加一个成员
	 * 
	 */
	bool AddFamilyMember(MODI_FamilyMember * p_member);

	/** 
	 * @brief 删除一个成员
	 * 
	 * @param guid 成员guid
	 *
	 */
	void RemoveFamilyMember(MODI_FamilyMember * p_member);


	/** 
	 * @brief 查找一个成员
	 * 
	 * @param guid 
	 * 
	 * @return 
	 */
	MODI_FamilyMember * FindMember(const MODI_GUID & guid);
	

	/** 
	 * @brief 是否是该家族成员
	 * 
	 * @param guid 成员id
	 * 
	 * @return 是true,不是false
	 *
	 */
	bool IsFamilyMember(const MODI_GUID & guid);


	/** 
	 * @brief 是否是该家族成员
	 * 
	 * @param name 成员名
	 * 
	 * @return 是true,不是false
	 *
	 */
	MODI_FamilyMember *  FindFamilyMember(const char * name);


	/** 
	 * @brief 发送公告给族长
	 * 
	 * @param chat_content 内容
	 * @param chat_size 内容大小
	 *
	 */
	void SendSysChatToMaster(const char * chat_content, const DWORD chat_size);


	/** 
	 * @brief 发送公告给副族长
	 * 
	 * @param chat_content 公告内容
	 * @param chat_size 内容大小
	 *
	 */
	void SendSysChatToSlaver(const char * chat_content, const DWORD chat_size);


	/** 
	 * @brief 发送系统公告
	 * 
	 * @param chat_content 
	 * @param chat_size 
	 * @param p_ex_client 
	 */
	void BroadcastSysChat(const char * chat_content, const WORD chat_size, MODI_OnlineClient * p_ex_client = NULL);


	/** 
	 * @brief 广播命令
	 * 
	 * @param pt_null_cmd 
	 * @param cmd_size 
	 * @param p_ex_client 
	 */
	void Broadcast(const stNullCmd * pt_null_cmd, const unsigned int cmd_size, MODI_OnlineClient * p_ex_client = NULL);


	/** 
	 * @brief 发送系统公告
	 * 
	 * @param chat_content 
	 * @param chat_size 
	 * @param p_ex_client 
	 */
	void BroadcastSysChatExRequest(const char * chat_content, const WORD chat_size, MODI_OnlineClient * p_ex_client = NULL);


	/** 
	 * @brief 广播命令
	 * 
	 * @param pt_null_cmd 
	 * @param cmd_size 
	 * @param p_ex_client 
	 */
	void BroadcastExRequest(const stNullCmd * pt_null_cmd, const unsigned int cmd_size, MODI_OnlineClient * p_ex_client = NULL);

	
	/** 
	 * @brief 所有成员都执行
	 * 
	 * @param call_back 执行体
	 *
	 */
	void AllMemberExec(MODI_FamilyMemberCallback & call_back);
	
	void UpDate();
	void SaveToDB();
	void UpDateToDB();


	/** 
	 * @brief 正式成员数量
	 * 
	 * 
	 */
	const WORD NormalMemNum();


	/** 
	 * @brief 副族长的个数
	 * 
	 * 
	 */
	const WORD SlaverMemNum();


	/** 
	 * @brief 申请个数
	 * 
	 * 
	 */
	const WORD RequestMemNum();
	
	
	/** 
	 * @brief 家族成员数上限
	 * 
	 * 
	 */
	bool IsFull();
	

	/** 
	 * @brief 副族长是否满了
	 * 
	 */
	bool IsSlaverFull();


	/** 
	 * @brief 更新家族名片
	 * 
	 */
	void UpDateFamilyCard(MODI_FamilyMember * p_member, enUpDateCFReason reason);
	
	
	const MODI_FamilyBaseInfo & GetFamilyBaseInfo()
	{
		return m_stBaseInfo;
	}

	const WORD GetFamilyLevel()
	{
		return m_stBaseInfo.m_wdLevel;
	}

	
	/* const DWORD GetRenqi() */
/* 	{ */
/* 		return m_stBaseInfo.m_dwRenqi; */
/* 	} */

	
	const DWORD GetMoney()
	{
		return m_stBaseInfo.m_dwMoney;
	}
	

	/** 
	 * @brief 找一个贡献度最高的
	 * 
	 * @param p_ex_member 除此人
	 * 
	 */
	MODI_FamilyMember * GetMaxContribute(MODI_FamilyMember * p_ex_member = NULL);


	/** 
	 * @brief 再副族长里面找贡献度最高的人
	 * 

	 */
	MODI_FamilyMember * GetMaxContributeInSlaver(MODI_FamilyMember * p_ex_member);


	/** 
	 * @brief 更换族长
	 * 
	 * @param p_member 新族长
	 *
	 */
	void  ChangeMaster(MODI_FamilyMember * p_member);


	/** 
	 * @brief 主动更换族长
	 * 
	 * @param p_master 老族长
	 * @param change_id 新的族长
	 *
	 */
	enFamilyOptResult OnChangeMaster(MODI_OnlineClient * p_master, const MODI_GUID & change_id);


	/** 
	 * @brief 解散
	 * 
	 */
	bool OnFree(MODI_OnlineClient * p_client);


	/** 
	 * @brief 有人访问家族
	 * 
	 * @param p_client 访问家族的人
	 * 
	 */
	enFamilyOptResult OnVisit(MODI_OnlineClient * p_client);


	/** 
	 * @brief 修改公告
	 * 
	 */
	void OnModiPublic(MODI_OnlineClient * p_master, const char * modi_public);


	/** 
	 * @brief 修改宣言
	 * 
	 */
	void OnModiXuanyan(MODI_OnlineClient * p_master, const char * modi_xuanyan);


	/** 
	 * @brief 设置副族长
	 * 
	 */
	enFamilyOptResult OnSetSlaver(MODI_OnlineClient * p_master, const MODI_GUID & set_id);


	/** 
	 * @brief 罢免副族长
	 * 
	 */
	enFamilyOptResult OnSetNormal(MODI_OnlineClient * p_master, const MODI_GUID & set_id);
	

	/** 
	 * @brief 通知删除成员
	 * 
	 * @param p_member 成员
	 * @param reason 理由
	 *
	 */
	void NotifyDelMember(MODI_FamilyMember * p_member, enDelMemInfoReason reason);


	/** 
	 * @brief 把家族信息发给客户端
	 * 
	 */
	void SendFamilyToClient(MODI_OnlineClient * p_client, enSendFullInfoReason reason);


	/** 
	 * @brief 广播家族信息
	 * 
	 * @param reason 理由
	 */
	void SendFamilyToAll(enSendFullInfoReason reason);


	/** 
	 * @brief 获取家族全部信息
	 * 
	 * @param reason 理由
	 */
	const DWORD GetFamilyFullData(char * buf, enSendFullInfoReason reason);


	/** 
	 * @brief 获取家族基本信息
	 * 
	 * 
	 */
	void GetFamilyListInfo(MODI_FamilyListInfo & base_info);

	/** 
	 * @biref 成员数
	 * 
	 * 
	 */
	const WORD  GetMemNum();

	/** 
	 * @brief 计算家族人气
	 * 
	 * 
	 */
	const DWORD GetFamilyRenqi();
	
 private:
	void Final();
	void DelFromDB();
	
	MODI_GUID m_stMasterID;
	std::string m_strMasterName;
	
	MODI_FamilyBaseInfo m_stBaseInfo;
	
	defFamilyMemMap m_stMemberMap;
};

#endif
