/** 
 * @file RankCore.h
 * @brief 排行榜模块
 * @author Tang Teng
 * @version v0.1
 * @date 2010-12-13
 */
#ifndef MODI_RANK_CORE_H_
#define MODI_RANK_CORE_H_

#include "gamestructdef.h"
#include "Base/s2rdb_cmd.h"
#include "Base/SingleObject.h"
#include "Base/Timer.h"

class 	MODI_OfflineUser; 
class 	MODI_OnlineClient;

class 	MODI_RankCore : public CSingleObject<MODI_RankCore>
{

	public:
		MODI_RankCore();
		~MODI_RankCore();

		bool 	LoadRanksFromDB();

		bool 	GetLevelRank( WORD begin , WORD end , MODI_Rank_Level * out );
		bool 	GetRenqiRank( WORD begin , WORD end , MODI_Rank_Renqi * out );
		bool 	GetConsumeRank( WORD begin , WORD end , MODI_Rank_ConsumeRMB * out );
		bool 	GetHighestScoreRank( WORD begin , WORD end , MODI_Rank_HighestScore * out );
		bool 	GetHighestPrecisionRank( WORD begin , WORD end , MODI_Rank_HighestPrecision * out );
		bool 	GetKeyHighestScoreRank( WORD begin , WORD end , MODI_Rank_HighestScore * out );
		bool 	GetKeyHighestPrecisionRank( WORD begin , WORD end , MODI_Rank_HighestPrecision * out );



		QWORD 	GetRankVersion( enRankType type ) { return version_[type]; }
		void 	SetRankVersion( enRankType type , QWORD ver ) { version_[type] = ver; }

		bool 	IsValidRankSapce( enRankType type , WORD begin , WORD end  );

		void 	UpdateLevelRank( const MODI_DBRank_Level_List & rank );
		void 	UpdateRenqiRank( const MODI_DBRank_Renqi_List & rank );
		void 	UpdateConsumeRMBRank( const MODI_DBRank_ConsumeRMB_List & rank );
		void 	UpdateHighestScoreRank( const MODI_DBRank_HighestScore_List & rank );
		void 	UpdateHighestPrecisionRank( const MODI_DBRank_HighestPrecision_List & rank );
		void 	UpdateKeyHighestScoreRank( const MODI_DBRank_HighestScore_List & rank );
		void 	UpdateKeyHighestPrecisionRank( const MODI_DBRank_HighestPrecision_List & rank );

		void 	UpdateSelfRanks( enRankType type , const MODI_DBSelfRank & rank  );


		bool 	IsValidData( enRankType type ) const { return data_valid_[type]; }
		void 	SetRankDataValid( enRankType type ,bool valid ) { data_valid_[type] = valid; }

		bool 	GetCharIDAndNameByRankNo( enRankType type , WORD no , MODI_CHARID & charid , char * role_name  );

		void 	Update();

		void 	SyncOnlineClientSpecificRank( MODI_OnlineClient * src_client , enRankType type , 
				WORD rank_no , MODI_OnlineClient * rank_client );

		void 	SyncOfflineClientSpecificRank( MODI_OnlineClient * src_client , enRankType type , 
				WORD rank_no , MODI_OfflineUser * rank_client );

		size_t 	GetPerSpecificRankSize( enRankType type );

		DWORD 	GetSelfRank( MODI_CHARID charid , enRankType type );


	private:

		void 	FillSpecificInfoByOnlineClient( enRankType type , char * fill , MODI_OnlineClient * online_client );

		void 	FillSpecificInfoByOfflineClient( enRankType type , char * fill , MODI_OfflineUser * offline_client );

	private:

		struct Rank_Space
		{
			WORD 	b;
			WORD 	e;
		};

		typedef std::map<MODI_CHARID,DWORD> 	MAP_SELF_RANKS;
		typedef MAP_SELF_RANKS::iterator 		MAP_SELF_RANKS_ITER;
		typedef MAP_SELF_RANKS::const_iterator 	MAP_SELF_RANKS_C_ITER;
		typedef std::pair<MAP_SELF_RANKS_ITER,bool> MAP_SELF_RANKS_RESULT;

		MODI_DBRank_Level_List 	rank_level_;
		MODI_DBRank_Renqi_List 	rank_renqi_;
		MODI_DBRank_ConsumeRMB_List rank_consume_rmb_;
		MODI_DBRank_HighestScore_List rank_highest_score_;
		MODI_DBRank_HighestPrecision_List rank_highest_precision_;
		MODI_DBRank_HighestScore_List rank_key_highest_score_;
		MODI_DBRank_HighestPrecision_List rank_key_highest_precision_;

		MAP_SELF_RANKS 	self_ranks_[kRank_End - kRank_Begin + 1];

		QWORD 	version_[kRank_End - kRank_Begin + 1];
		bool 	data_valid_[kRank_End - kRank_Begin + 1];
		Rank_Space rank_space_[kRank_End - kRank_Begin + 1];
		MODI_Timer 	timer_rank_load_;
};

#endif
