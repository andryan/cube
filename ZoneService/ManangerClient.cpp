#include "ManangerClient.h"
#include "ZoneService.h"
#include "PackageHandler_zs2ms.h"



MODI_ManangerSvrClient::MODI_ManangerSvrClient(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(SVR_CMDSIZE);
}

MODI_ManangerSvrClient::~MODI_ManangerSvrClient()
{

}

bool MODI_ManangerSvrClient::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

void MODI_ManangerSvrClient::Final()
{
	MODI_ClientTask::Final();

	Global::logger->info("[%s] Disconnection with Manager Server. ZoneServer will Terminate. " , 
			ERROR_CON );

	MODI_ZoneService * pService = MODI_ZoneService::GetInstancePtr();
	// MANANGER SERVER 断开，则终止服务器
	if( pService )
	{
		pService->Terminate();
	}
}

bool MODI_ManangerSvrClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }

    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_ManangerSvrClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

	return false;
}

bool MODI_ManangerSvrClient::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	// 数据包处理
	if( MODI_ManangerSvrClient::GetInstancePtr() )
	{
            ///	游戏命令的处理
		int iRet = MODI_PackageHandler_zs2ms::GetInstancePtr()->DoHandlePackage(
						ptNullCmd->byCmd, ptNullCmd->byParam, 
						MODI_ManangerSvrClient::GetInstancePtr() , 
						ptNullCmd,	dwCmdLen);

		if( iRet == enPHandler_Ban )
		{

		}
		else  if ( iRet == enPHandler_Kick )
		{

		}
		else if( iRet == enPHandler_Warning )
		{

		}

		return true;
	}

	return true;
}

