/** 
 * @file GameSvrClientTask.h 
 * @brief ZONE SERVER 上的游戏服务器客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-13
 */
#ifndef GAMESVRCLIENT_ONZS_TASK_H_
#define GAMESVRCLIENT_ONZS_TASK_H_

#include "protocol/gamedefine.h"
#include "ServiceTask.h"
#include "RecurisveMutex.h"
#include "CommandQueue.h"
#include <set>
#include "SvrPackageProcessBase.h"
#include "s2unify_cmd.h"

class  MODI_GameSvrClientTask : public MODI_IServerClientTask
{
	public:

		MODI_GameSvrClientTask(const int sock, const struct sockaddr_in * addr);
		virtual ~MODI_GameSvrClientTask();

	public:

		virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

		virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

		virtual int RecycleConn();
		virtual void DisConnect();

		// 连接上的回调,可以安全的在逻辑线程调用
		virtual void OnConnection(); 

		// 断开的回调，可以安全的在逻辑线程调用
		virtual void OnDisconnection(); 

		BYTE 	GetServerID() const { return m_byServerID; }
		const char * GetServerName() const { return m_szServerName; }
		DWORD 	GetGateIP() const { return m_nGateIP; }
		WORD 	GetGatePort() const { return m_nGatePort; }
		DWORD 	GetClientNum() const { return m_nClientNum; }

		void 	IncClientNum();
		void 	DecClientNum();

		static void BroadcastAllServer(const stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	private:

		void 	OnHandlerClientCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);
		void 	OnHandlerGameServerCmd(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen);

	public:


		/** 
		 * @brief 处理所有客户端的数据包
		 */
		static void 	ProcessAllPackage();

		static MODI_SvrPackageProcessBase 	ms_ProcessBase;

		static bool 	RegServer( BYTE byServer , const char * szName , DWORD nGateIP , WORD nGatePort , MODI_GameSvrClientTask * p );

		static MODI_GameSvrClientTask * FindServer(BYTE byServer);

		static void 	Init();

		static int 		CreateChannelListPackage( char * szBuf , size_t nBufSize , int iReason , const defAccountID & acc_id = 0);

		/// 发送频道信息给gmtool
		static int      CreateChannelStatusToGMTool(MODI_ReturnChannelStatus_Cmd * p_send_cmd);

		static void 	SyncChannelListToLoginServer();

		static WORD m_wdChannelMaxClient;

	private:

		
		BYTE 	m_byServerID;
		char 	m_szServerName[MAX_SERVERNAME_LEN + 1];
		DWORD 	m_nGateIP;
		WORD 	m_nGatePort;
		DWORD 	m_nClientNum;

		static  MODI_GameSvrClientTask * 	ms_pGameServers[255];
};


#endif
