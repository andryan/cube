/**
 * @file   MailCore.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Aug  3 16:36:31 2011
 * 
 * @brief  邮件相关操作,hrx整理
 * 
 */


#include "MailCore.h"
#include "OnlineClient.h"
#include "AssertEx.h"
#include "Mail.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"
#include "protocol/c2gs_mail.h"
#include "s2zs_cmd.h"
#include "Base/BinFileMgr.h"
#include "ScriptMailDef.h"
#include "ScriptMailHandler.h"
#include "Base/HelpFuns.h"


MODI_DBMail_ID 		MODI_MailCore::ms_LastMailID = INVAILD_DBMAIL_ID;


/** 
 * @brief 邮件附件检查
 * 
 * @param mailType 邮件类型
 * @param extraInfo 附件信息
 * 
 * @return 合法true
 *
 */
bool MODI_MailCore::CheckAttachment( enMailType mailType , const MODI_MailExtraInfo &  extraInfo )
{
	/// 这个已经不用了
	if( mailType == kAttachmentMoneyMail )
	{
		/// 检查货币数量
		if( extraInfo.GetAttachment_Money() <= MAIL_ATTACHMENT_MAX_MONEY )
		{
			return true;
		}
	}
	
	/// 检查附件的有效性
	else if( mailType == kAttachmentItemMail )
	{
		/// config id 
		MODI_BinFileMgr * pBinMgr =  MODI_BinFileMgr::GetInstancePtr();
		if( !pBinMgr->IsValidConfigInTables( extraInfo.GetAttachment_ItemInfo_ConfigID() , 
					enBinFT_Avatar | enBinFT_Item ) )
		{
			Global::logger->error("[check_attachment] check attachment configid failed <configid=%u>", extraInfo.GetAttachment_ItemInfo_ConfigID());
			MODI_ASSERT(0);
			return false;
		}

		WORD nMaxLapCount = 1;
		defItemlistBinFile & itemBin = pBinMgr->GetItemBinFile();
		const GameTable::MODI_Itemlist * pItem = itemBin.Get( extraInfo.GetAttachment_ItemInfo_ConfigID() );
		if( pItem )
		{
			nMaxLapCount = pItem->get_MaxOverLap();
		}

		/// count 
		if( extraInfo.GetAttachment_ItemInfo_Count() > nMaxLapCount )
		{
			Global::logger->error("[check_attachment] check max lap failed <maxlap=%u,count=%u>", nMaxLapCount, extraInfo.GetAttachment_ItemInfo_Count());
			MODI_ASSERT(0);
			return false;
		}

		/// time
		if( extraInfo.GetAttachment_ItemInfo_TimeExpretime() != kShopGoodT_Forver &&
			extraInfo.GetAttachment_ItemInfo_TimeExpretime() != kShopGoodT_7Day &&
			extraInfo.GetAttachment_ItemInfo_TimeExpretime() != kShopGoodT_30Day )
		{
			MODI_MailExtraInfo  &  extraInfo_new = const_cast<MODI_MailExtraInfo&>(extraInfo);

#ifdef _DEBUG
			Global::logger->debug("[check_attachment] check attachment old");
#endif				
			/// 先兼容一下老的
			if(extraInfo.GetAttachment_ItemInfo_TimeExpretime() == enForever)
			{
				DWORD config_id = extraInfo.GetAttachment_ItemInfo_ConfigID();
				WORD count = extraInfo.GetAttachment_ItemInfo_Count();
				extraInfo_new.SetAttachment_ItemInfo(config_id, count, kShopGoodT_Forver);
				return true;
			}
			else if(extraInfo.GetAttachment_ItemInfo_TimeExpretime() == enOneWeak)
			{
				DWORD config_id = extraInfo.GetAttachment_ItemInfo_ConfigID();
				WORD count = extraInfo.GetAttachment_ItemInfo_Count();
				extraInfo_new.SetAttachment_ItemInfo(config_id, count, kShopGoodT_7Day);
				return true;
			}
			else if(extraInfo.GetAttachment_ItemInfo_TimeExpretime() == enOneMonth)
			{
				DWORD config_id = extraInfo.GetAttachment_ItemInfo_ConfigID();
				WORD count = extraInfo.GetAttachment_ItemInfo_Count();
				extraInfo_new.SetAttachment_ItemInfo(config_id, count, kShopGoodT_30Day);
				return true;
			}
			
			Global::logger->error("[check_attachment] check time failed <timeexpretime=%u>", extraInfo.GetAttachment_ItemInfo_TimeExpretime());
			MODI_ASSERT(0);
			return false;
		}

		return true;
	}
	return false;
}


/** 
 * @brief 玩家发送邮件给玩家(没有附件的), 但玩家不一定存在
 * 
 * @param pSender 发送者
 * @param mail 邮件信息
 * 
 * @return 成功true
 *
 */
bool MODI_MailCore::ClientSendMailToClient( MODI_OnlineClient * pSender , const MODI_SendMailInfo & mail)
{
	/// Disable Text Mail due to unresolved bug may cause crashes
	return false;

	if( !pSender )
	{
		return false;
	}

	MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pRecever = pOnlineMgr->FindByName(mail.m_szRecevier);
	
	if(! pRecever)
	{
		/// 玩家离线
		MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
		MODI_OfflineUser * pOfflineUser = pOfflineMgr->FindByName(mail.m_szRecevier);
		if( pOfflineUser )
		{
			/// 存在此用户，直接写入数据库
			MODI_Mail * pMail = new MODI_Mail();
			pMail->InitFromSendMail( pSender->GetRoleName() , pSender->GetNameLen() , mail );
			pMail->SetTimeStamp();
			pMail->SetNewMail();
			pMail->SaveToDB();
			
			MODI_GS2C_Notify_SendMailRes resSend;
			safe_strncpy( resSend.m_szTargetName, pMail->GetRecevier(), sizeof(resSend.m_szTargetName));
			resSend.m_result = MODI_GS2C_Notify_SendMailRes::kSuccessful;
			pSender->SendPackage( &resSend , sizeof(resSend) );

			//delete pMail;
			pMail = NULL;
			
		}
		else 
		{
			/// 向db请求后，db返回加入离线管理器，最后还是走这个流程
			MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
			if( pDB )
			{

				char buf[Skt::MAX_USERDATASIZE];
				memset(buf, 0, sizeof(buf));
				MODI_S2RDB_Request_RoleDetailInfo * reqRoleInfo = (MODI_S2RDB_Request_RoleDetailInfo *)buf;
				AutoConstruct(reqRoleInfo);

				reqRoleInfo->m_reqCharid = pSender->GetCharID();

				strncpy(reqRoleInfo->m_szName, mail.m_szRecevier, sizeof(reqRoleInfo->m_szName) -1);
				
				/// 发送离线邮件
				reqRoleInfo->m_opt = kSendOfflineMails;
				
				/// 带上准备邮件的信息
				reqRoleInfo->m_nExtraSize = sizeof(mail);
				memcpy((char *)(&(reqRoleInfo->m_szExtra)), (const char *)(&mail), sizeof(mail));
				
				pDB->SendCmd(reqRoleInfo , (sizeof(MODI_S2RDB_Request_RoleDetailInfo) + reqRoleInfo->m_nExtraSize));
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
	}
	
	/// 玩家在线
	else 
	{
		MODI_Mail * pMail = new MODI_Mail();
		pMail->InitFromSendMail( pSender->GetRoleName() , pSender->GetNameLen() , mail );
		if( SendTo( pSender , pRecever , pMail ) == false )
		{
			//delete pMail;
			pMail = NULL;
			return false;
		}
	}

	return MODI_MailCore::kOk;
}


/** 
 * @brief 发送邮件，调用此函数前必须确认接收玩家已经存在,可以不带附件发送文本信息
 * 
 * @param mail 邮件内容
 * @param szSender 发送者
 * @param pMailDBID 邮件id
 * @param transid 交易序号
 * 
 * @return 发送成功
 *
 */
bool MODI_MailCore::SendMail(const MODI_SendMailInfo & mail, const char * szSender, MODI_DBMail_ID * pMailDBID, QWORD transid)
{
	/// 发送人和接收人是否存在
	if(!mail.m_szRecevier[0] || !szSender)
	{
		Global::logger->error("send a mails failed, recever or sender null");
		MODI_ASSERT(0);
		return false;
	}

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pRecever = pMgr->FindByName(mail.m_szRecevier);

	/// 是否有附件
	if(mail.m_Type != kTextMail && mail.m_Type != kServerMail)
	{
		for(BYTE i=0; i<mail.m_byExtraCount; i++)
		{
			if(! CheckAttachment(mail.m_Type , mail.m_ExtraInfo[i]))
			{
				Global::logger->error("[send_mail] server send mail to client check attachment failed <name=%s,sender=%s>", mail.m_szRecevier, szSender);
				MODI_ASSERT(0);
				return false;
			}
		}
	}

	bool is_online = true;
	MODI_Mail * pMail = new MODI_Mail;
	pMail->InitFromSendMail(szSender , strlen(szSender) , mail);
	pMail->SetTimeStamp();
	pMail->SetTransid(transid);
			
	/// 接收玩家在线
	if(pRecever)
	{
		if(pRecever->MailIsFull())
		{
			/// 满后当离线处理
			/// 可以告诉他邮箱满了,让其删除邮件后下线再上线领取
			is_online = false;
			
		}
		else
		{

			if(pRecever->AddUserMail(pMail) == false)
			{
				Global::logger->error("[add_mail_to_list] add failed <transid=%llu, sender=%s,recever=%s>", 
									  transid, szSender, mail.m_szRecevier);
				MODI_ASSERT(0);
				return false;
			}

			/// 下一次更新保存到数据库
			pMail->SetNewMail();
			
			/// 通知接受者，有新的邮件
			SendAddMailPackage(pRecever , *pMail  , MODI_GS2C_Notify_AddMail::kNew);
		}
	}
	else
	{
		is_online = false;
	}

	if(pMailDBID)
	{
		*pMailDBID = pMail->GetDBMailInfo().m_dbID;
	}
	
	/// 玩家离线或者邮箱满了
	if(is_online == false)
	{
		pMail->SetNewMail();
		pMail->SaveToDB();
		//delete pMail;
	}

	Global::logger->debug("[send_give_mails] send a give successful <sender=%s,recever=%s,mailid=%llu>",
						  szSender, mail.m_szRecevier,pMailDBID);
	
	return true;
}


/** 
 * @brief 系统邮件
 * 
 * @param mail 邮件
 * @param scriptdata 脚本数据
 * @param pMailDBID 返回邮件id
 * 
 * @return 成功ture
 *
 */
bool MODI_MailCore::SendScriptMail( const MODI_MailInfo & mail , const MODI_ScriptMailData & scriptdata , MODI_DBMail_ID * pMailDBID)
{
	if( mail.m_Type != kServerMail )
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_DBMailInfo dbmail;
	dbmail.m_MailInfo = mail;
	dbmail.m_bIsValid = true;
	dbmail.m_dbID = GetNextMailID();
	dbmail.m_ScriptData = scriptdata;
	MODI_Mail * pMail = new MODI_Mail();
	pMail->Init( dbmail );
	pMail->SetTimeStamp();

	MODI_DBMail_ID mdid = pMail->GetDBMailInfo().m_dbID;

	MODI_OnlineClientMgr * pOnlineMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pRecever = pOnlineMgr->FindByName(mail.m_szRecevier);
	
	if(! pRecever)
	{
		/// 直接保存了
		pMail->SetNewMail();
		pMail->SaveToDB();
		//delete pMail;
		pMail = NULL;
	}
	else 
	{
		if( pRecever->AddServerMail(pMail) == false )
		{
			//delete pMail;
			pMail = NULL;
			return false;
		}
		
		pMail->SetNewMail();
		pMail->SaveToDB();
	}

	if( pMailDBID )
	{
		*pMailDBID = mdid;
	}

	return true;
}


/** 
 * @brief 发送邮件给某玩家，私有的函数
 * 
 * @param pSender 发送者
 * @param pList 邮件列表
 * @param pMail 邮件
 * 
 */
bool MODI_MailCore::SendTo(MODI_OnlineClient * pSender , MODI_OnlineClient * pRecever, MODI_Mail * pMail)
{
	if( !pSender || !pRecever || !pMail )
	{
		return false;
	}

	MODI_GS2C_Notify_SendMailRes resSend;
	safe_strncpy( resSend.m_szTargetName, pMail->GetRecevier(), sizeof(resSend.m_szTargetName));

	resSend.m_result = MODI_GS2C_Notify_SendMailRes::kUnknowError;

	if(pRecever->MailIsFull())
	{
		/// 通知发送者，对方邮箱已经满
		resSend.m_result = MODI_GS2C_Notify_SendMailRes::kMailBoxFull;
		pSender->SendPackage( &resSend , sizeof(resSend) );
		return false;
	}

	pMail->SetTimeStamp();
	if(pRecever->AddUserMail(pMail) == false)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	pMail->SetNewMail();

	// 通知接受者，有新的邮件
	SendAddMailPackage( pRecever , *pMail  , MODI_GS2C_Notify_AddMail::kNew );

	resSend.m_result = MODI_GS2C_Notify_SendMailRes::kSuccessful;
	pSender->SendPackage( &resSend , sizeof(resSend) );
	return true;
}


/** 
 * @brief 把某个玩家的邮件发送给客户端,是一封一封的发
 * 
 * @param pRecever 此玩家
 * @param mail 此邮件
 * @param iType 理由
 *
 */
void MODI_MailCore::SendAddMailPackage( MODI_OnlineClient * pRecever , const MODI_Mail & mail , int iType )
{
	if( !pRecever )
	{
		return ;
	}

    char szPackageBuf[Skt::MAX_USERDATASIZE];
    memset(szPackageBuf, 0, sizeof(szPackageBuf));
    MODI_GS2C_Notify_AddMail * p_notify = (MODI_GS2C_Notify_AddMail *) (szPackageBuf);
    AutoConstruct(p_notify);

	p_notify->m_type = (MODI_GS2C_Notify_AddMail::enType)iType;
	p_notify->Set( mail.GetMailInfo() );
	int iSendSize = sizeof(MODI_GS2C_Notify_AddMail) + p_notify->m_nContentsize;
	pRecever->SendPackage( p_notify , iSendSize );
}


/** 
 * @brief 获取某个邮件的附件
 * 
 * @param pClient 客户端
 * @param id 邮件id
 *
 */
void MODI_MailCore::GetAttachment( MODI_OnlineClient * pClient , const MODI_GUID & id )
{
	if( !pClient )
	{
		MODI_ASSERT(0);
		return ;
	}

	MODI_GS2C_Notify_GetAttachmentRes res;
	res.m_mailid = id;

	MODI_Mail * pMail = pClient->GetUserMail(id);
	if( !pMail )
	{
		res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kMailNotExist;
		pClient->SendPackage( &res , sizeof(res) );
		return ;
	}

	/// 是否有附件
	if( !pMail->IsAttachmentItemMail() && !pMail->IsAttachmentMoneyMail() )
	{
		res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kNotExistAttachment;
		pClient->SendPackage( &res , sizeof(res) );
		return ;
	}

	if( pMail->IsAlreadyGetAttachment() )
	{
		Global::logger->info("[%s] client<charid=%u,name=%s> request get attachment.but mail is already get attachment." , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName());
		MODI_ASSERT(0);
		return ;
	}

	if( pMail->IsLockAttachment() )
	{
		Global::logger->info("[%s] client<charid=%u,name=%s> request get attachment.but mail is lock." , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName());
		MODI_ASSERT(0);
		return ;
	}

	for(BYTE i=0; i<pMail->GetMailInfo().m_byExtraCount; i++)
	{
		if( !CheckAttachment( pMail->GetMailInfo().m_Type , pMail->GetMailInfo().m_ExtraInfo[i] ) )
		{
			Global::logger->info("[%s] client<charid=%u,name=%s,%u> request get attachment.but attachment invaild." , 
								 MAIL_MODULE ,
								 pClient->GetCharID(),
								 pClient->GetRoleName(), (WORD)pMail->GetMailInfo().m_byExtraCount);
			MODI_ASSERT(0);
			return ;
		}
	}

	// 客户端所在gs
	MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pClient->GetServerID() );
	if( !pServer )
	{
		Global::logger->info("[%s] client<charid=%u,name=%s> request get attachment.but gameserver<id=%u> not exist." , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				pClient->GetServerID() );
		res.m_ressult = MODI_GS2C_Notify_GetAttachmentRes::kUnknowError;
		pClient->SendPackage( &res , sizeof(res) );
		return ;
	}

	if( pMail->IsAttachmentMoneyMail() )
	{
		// 让gs给某个客户端加钱
		MODI_ZS2S_Request_AddMoeny reqAddMoney;
		reqAddMoney.m_guid = pClient->GetGUID();
		reqAddMoney.m_nMoeny = pMail->GetMailInfo().m_ExtraInfo[0].GetAttachment_Money();
		reqAddMoney.m_reason = kGetMailAttachment;
		EncodeZS2SOpt_GetAttachment( reqAddMoney.m_szData , pMail->GetGUID() );
		pServer->SendCmd( &reqAddMoney , sizeof(reqAddMoney) );

		pMail->SetLockAttachment( true );
	}
	
	else  if ( pMail->IsAttachmentItemMail() )
	{
		for(BYTE i=0; i<pMail->GetMailInfo().m_byExtraCount; i++)
		{
			// 让gs给某个客户端加物品
			MODI_ZS2S_Request_AddItem reqAddItem;
			reqAddItem.m_guid = pClient->GetGUID();
			reqAddItem.m_nConfigID = pMail->GetMailInfo().m_ExtraInfo[i].GetAttachment_ItemInfo_ConfigID();
			reqAddItem.m_nCount = pMail->GetMailInfo().m_ExtraInfo[i].GetAttachment_ItemInfo_Count();
			reqAddItem.m_ExpireTime = pMail->GetMailInfo().m_ExtraInfo[i].GetAttachment_ItemInfo_TimeExpretime();
			reqAddItem.m_reason = kGetMailAttachment;
			EncodeZS2SOpt_GetAttachment( reqAddItem.m_szData , pMail->GetGUID() );
			reqAddItem.m_transid = pMail->GetDBMailInfo().m_Transid;
			pServer->SendCmd( &reqAddItem , sizeof(reqAddItem) );
		}

		pMail->SetLockAttachment( true );
	}
}




/** 
 * @brief 客户端第一次登陆,要加载邮件列表,也仅加载一次
 * 
 * @param pClient 此客户端
 * 
 */
MODI_MailCore::enResult MODI_MailCore::OnClientLogin( MODI_OnlineClient * pClient )
{
	MODI_MailCore::enResult res = kOk;
	if(!pClient)
	{
		MODI_ASSERT(0);
		return kLoadingFaildFromDB;
	}
	
	MODI_DBProxyClientTask * pDBProxy  = GetApp()->GetDBInterface(0);
	if( !pDBProxy )
	{
		Global::logger->info("[%s] client login. try request db load client<charid=%u,name=%s> 's mails faild." , 
							 MAIL_MODULE ,
							 pClient->GetCharID(),
							 pClient->GetRoleName() );
		return kLoadingFaildFromDB;
	}

	MODI_S2RDB_Request_LoadMaillist  reqLoadMaillist;
	safe_strncpy( reqLoadMaillist.m_szName , pClient->GetRoleName() , sizeof(reqLoadMaillist.m_szName) );
	pDBProxy->SendCmd( &reqLoadMaillist ,  sizeof(reqLoadMaillist) );
	
	return res;
}
