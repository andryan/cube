/** 
 * @file OfflineUserMgr.h
 * @brief 离线用户管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef OFFLINE_CLIENT_MGR_H_
#define OFFLINE_CLIENT_MGR_H_

#include "OnlineClient.h"
#include "OfflineUser.h"
#include "FreeList.h"
#include "SingleObject.h"


class 	MODI_OfflineUserMgr : public CSingleObject<MODI_OfflineUserMgr> 
{
	public:

		MODI_OfflineUserMgr();
		virtual ~MODI_OfflineUserMgr();

		bool 	OnClientLogin( MODI_OnlineClient * pClient );
		bool 	OnClientLoginout(  MODI_OnlineClient * pClient );

		MODI_OfflineUser * 	AddNode(const char *szName);

		MODI_OfflineUser * FindByName( const std::string & strName );
		MODI_OfflineUser * FindByAccName(const char * acc_name);
		MODI_OfflineUser * FindByAccId(const defAccountID acc_id);

//		void Update(DWORD nTime);

	private:
	
		gems::FreeList< MODI_OfflineUser,
		gems::PlacementNewLinkedList<MODI_OfflineUser>,
		gems::ConstantGrowthPolicy<10000,10000,1000000> > 	m_Pool;

		// 测试用
//		gems::FreeList< MODI_OfflineUser,
//		gems::PlacementNewLinkedList<MODI_OfflineUser>,
//		gems::ConstantGrowthPolicy<10,50,1000000> > 	m_Pool;

		typedef __gnu_cxx::hash_map<std::string, MODI_OfflineUser *, str_hash> HASHMAP;
		typedef HASHMAP::iterator 	HASHMAP_ITER;
		typedef std::pair<HASHMAP_ITER,bool> 	HASHMAP_INSERT_RESULT;

		HASHMAP 	m_map;
};





#endif
