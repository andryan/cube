#include "ChengHaoMgr.h"
#include "gamestructdef.h"
#include "GameTableDefine/Chenghao.h"

using namespace GameTable;
MODI_ChengHaoMgr * MODI_ChengHaoMgr::m_pInstance=NULL;

MODI_ChengHaoMgr * MODI_ChengHaoMgr::GetInstance()
{
	if( !m_pInstance)
	{
		m_pInstance = new MODI_ChengHaoMgr();
	}
	return m_pInstance;

}

void	MODI_ChengHaoMgr::Init()
{

	Global::logger->debug("[chenghao_mgr] Initital the Cheng hao manager...");
	defChenghaoBinFile & cheng_hao_bin = MODI_BinFileMgr::GetInstancePtr()->GetChenghaoBinFile();

	size_t i =0;
	for( i=0;  i< cheng_hao_bin.GetSize(); ++i )
	{
		ChengHaoItem _tmp;
		const MODI_Chenghao * _chenghao= cheng_hao_bin.GetByIndex( i);
		_tmp.m_wConfigId = _chenghao->get_ConfigID();
		_tmp.m_dwValue = _chenghao->get_Condition();
		if(  _tmp.m_wConfigId < 1000 )
		{
			m_mPositive.push_back(_tmp);
		}
		else  if( _tmp.m_wConfigId < 2000 )
		{
			m_mNegative.push_back(_tmp);
		}
		else
		{
			m_wGm = _tmp.m_wConfigId;
		}
	}

	Global::logger->debug("[chenghao_mgr]  Cheng hao manager inited ok...");
}

WORD	MODI_ChengHaoMgr::GetChengHao(MODI_OnlineClient * pclient)
{
	
	MODI_CharactarFullData _fulldata  = pclient->GetFullData();	
	int _gmlevel = _fulldata.m_roleExtraData.m_iGMLevel;	
	const WORD _block_count = pclient->GetBlockCount();
	const DWORD _Renqi = _fulldata.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi;
	size_t i=0;
	WORD	_wRet=0;

	if( _gmlevel == 3)
	{
		return m_wGm;	 
	}

	///	获得负面称号
	if(_wRet ==0 && m_mNegative.size() && _block_count >= m_mNegative[0].m_dwValue)
	{
		while( i < m_mNegative.size())
		{
			if( _block_count >= m_mNegative[i].m_dwValue)
				_wRet = m_mNegative[i].m_wConfigId;		
			else
				break;
			i++;
		}
		return _wRet;
	}

	///	获得正面称号
	if(  m_mPositive.size() &&  _Renqi >= m_mPositive[0].m_dwValue)
	{
		i=0;
		while( i < m_mPositive.size())
		{
			if( _Renqi >= m_mPositive[i].m_dwValue)
				_wRet = m_mPositive[i].m_wConfigId;
			i++;
		}
	}

	return _wRet;
}






