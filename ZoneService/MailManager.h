/**
 * @file   MailManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Feb 10 17:12:10 2012
 * 
 * @brief  邮件管理，每个人物身上都有一个
 * 
 * 
 */

#ifndef _MD_MAILMANAGER_H_
#define _MD_MAILMANAGER_H_

#include "Global.h"
#include "Mail.h"


class MODI_OnlineClient;

/**
 * @brief 邮件系统
 * 
 */
class MODI_MailManager
{
 public:
	typedef std::map<MODI_GUID, MODI_Mail * > defMailMap;
	typedef std::map<MODI_GUID, MODI_Mail * >::iterator defMailMapIter;
	typedef std::map<MODI_GUID, MODI_Mail * >::value_type defMailMapValue;

	MODI_MailManager();
	~MODI_MailManager();

	bool Init(MODI_OnlineClient * p_client, const MODI_DBMailInfo * mails);

	bool AddMail(MODI_Mail * p_mail);

	bool DelMail(MODI_Mail * p_mail);

	bool DelMail(const MODI_GUID & mail_id);

	MODI_Mail * GetMails(const MODI_GUID & mail_id);

	void SendUserMailsToClient();
	
	void ExecuteSvrMails();

	bool IsFull()
	{
		return (m_stMailMap.size() >= MAILLIST_MAX_COUNT);
	}

	void Update();
	
 private:
	defMailMap m_stMailMap;
	MODI_OnlineClient * m_pOwnClient;
};

#endif
