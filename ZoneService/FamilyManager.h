/**
 * @file   FamilyManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Apr 23 17:12:49 2011
 * 
 * @brief  家族全局管理器
 * 
 */

#ifndef _MD_FAMILYMANAGER_H
#define _MD_FAMILYMANAGER_H

#include "AssertEx.h"
#include "Global.h"
#include "DBStruct.h"
#include "Family.h"
#include "protocol/family_def.h"



  

/**
 * @brief 家族管理
 * 
 */
class MODI_FamilyManager
{
 public:
	typedef std::map<defFamilyID, MODI_Family * > defFamilyMap;
	typedef std::map<defFamilyID, MODI_Family * >::value_type  defFamilyMapValue;
	typedef std::map<defFamilyID, MODI_Family * >::iterator defFamilyMapIter;

	MODI_FamilyManager()
	{
		m_dwGenFamilyID = 0;
	}
	
	static MODI_FamilyManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_FamilyManager;
		}
		return *m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
	    {
	      delete m_pInstance;
	    }
		m_pInstance = NULL;
	}

	/** 
	 * @brief 家族个数
	 * 
	 * 
	 */
	const DWORD Size()
	{
		return m_stFamilyMap.size();
	}
	
	/** 
	 * @brief 从数据库加载数据
	 * 
	 */
	bool Init();

	
	/** 
	 * @brief 增加一个家族,只负责加入到管理器中
	 * 
	 * @param p_family 要增加的家族
	 *
	 */
	bool AddFamily(MODI_Family * p_family);


	/** 
	 * @brief 删除一个家族，负责释放内存
	 * 
	 * @param family_id 家族id
	 *
	 */
	void RemoveFamily(MODI_Family * p_family);


	/** 
	 * @brief 查找一个家族
	 * 
	 * @param family_id 家族id
	 * 
	 */
	MODI_Family * FindFamily(const defFamilyID & family_id);

	/** 
	 * @brief 请求创建家族，所有条件都满足了
	 * 
	 * @param create_info 创建家族的信息
	 * @param p_client 此人创建
	 * 
	 * @return 成功或者失败
	 *
	 */
	bool CreateFamily(MODI_FamilyBaseInfo & create_info, MODI_OnlineClient * p_client);

	
	/** 
	 * @brief 请求某个家族操作
	 * 
	 * @param p_client 请求人
	 * @param family_id 请求的家族id
	 * 
	 */
	enFamilyOptResult RequestFamily(MODI_OnlineClient * p_client, const defFamilyID & family_id);


	/** 
	 * @brief 取消申请
	 * 
	 * @param p_client 取消的人
	 * 
	 */
	enFamilyOptResult CancelFamily(MODI_OnlineClient * p_client);
	

	/** 
	 * @brief 离开家族
	 * 
	 * @param p_client 离开的人
	 * 
	 */
	enFamilyOptResult LeaveFamily(MODI_OnlineClient * p_client);


	/** 
	 * @brief 解散家族
	 * 
	 */
	enFamilyOptResult FreeFamily(MODI_OnlineClient * p_client);


	/** 
	 * @brief 访问家族
	 * 
	 */
	enFamilyOptResult VisitFamily(MODI_OnlineClient * p_client, const defFamilyID & visit_id);


	/** 
	 * @brief 家族升级
	 * 
	 * @param p_client 此人
	 * 
	 * @return 成功返回状态
	 *
	 */
	enFamilyOptResult FamilyLevel(MODI_OnlineClient * p_client);


	/** 
	 * @brief 修改公告
	 * 
	 */
	void ModiPublic(MODI_OnlineClient * p_master, const char * public_str);


	/** 
	 * @brief 修改宣言
	 * 
	 */
	void ModiXuanyan(MODI_OnlineClient * p_master, const char * xuanyan);


	/** 
	 * @brief 拒绝某人加入家族
	 * 
	 * @param p_master 家族族长
	 * @param refuse_id 拒绝id
	 * 
	 *
	 */
	enFamilyOptResult RefuseMember(MODI_OnlineClient * p_master, const MODI_GUID refuse_id);


	/** 
	 * @brief 开除家族成员
	 * 
	 * @param p_master 族长
	 * @param refuse_id 被开除的人
	 * 
	 */
	bool FireMember(MODI_OnlineClient * p_master, const MODI_GUID refuse_id);


	/** 
	 * @brief 接受某成员加入
	 * 
	 * @param p_master 族长
	 * @param accept_guid 被接受的人
	 * 
	 */
	enFamilyOptResult AcceptMember(MODI_OnlineClient * p_master, const MODI_GUID & accept_guid);


	/** 
	 * @brief 更换家族
	 * 
	 * @param p_master 族长 
	 * @param change_id 新族长
	 * 
	 */
	enFamilyOptResult ChangeMaster(MODI_OnlineClient * p_master, const MODI_GUID & change_id);

	
	/** 
	 * @brief 设置副族长
	 * 
	 * @param p_master 族长
	 * @param set_id 被设置的副族长
	 * 
	 */
	enFamilyOptResult SetSlaver(MODI_OnlineClient * p_master, const MODI_GUID & set_id);


	/** 
	 * @brief 罢免副族长
	 * 
	 */
	enFamilyOptResult SetNormal(MODI_OnlineClient * p_master, const MODI_GUID & normal_id);
	

	/** 
	 * @brief 所有家族执行
	 * 
	 * @param call_back 
	 */
	//void AllFamilyExec(MODI_FamilyCallback & call_back);

	/** 
	 * @brief 家族有人登陆,是否需要告知家族所有的人
	 * 
	 */
	void FamilyMemLogin(MODI_OnlineClient * p_client, bool is_notify);


	/** 
	 * @brief 是否是某家族成员
	 * 
	 * @param name 成员名
	 * 
	 */
	MODI_FamilyMember * FindFamilyMember(const char * name);


	/** 
	 * @brief 家族有人退出
	 * 
	 */
	void FamilyMemLogout(MODI_OnlineClient * p_client);


	/** 
	 * @brief 发送家族列表
	 *
	 */
	void NotifyFamilyList(MODI_OnlineClient * p_client);


	/** 
	 * @brief 发送家族列表
	 *
	 */
	void NotifyFamilyList();
	
	
	/** 
	 * @brief 通知增加家族列表
	 * 
	 * @param p_family 家族信息
	 *
	 */
	void NotifyAddFList(MODI_Family * p_family);


	/** 
	 * @brief 通知减少一个家族列表
	 * 
	 * @param family_id 
	 */
	void NotifyDelFList(const defFamilyID & family_id);

	
	/** 
	 * @brief 家族id流水线
	 * 
	 */
	const DWORD GenFamilyID()
	{
		m_dwGenFamilyID++;
		return m_dwGenFamilyID;
	}

	/** 
	 * @brief 家族名字检查
	 * 
	 */
	bool CheckFamilyName(const char * check_name);

	
 private:
	static MODI_FamilyManager * m_pInstance;
	~MODI_FamilyManager()
  	{
	    OnFree();
  	}


	/** 
	 * @brief 释放资源
	 * 
	 */
	void OnFree();
	
	defFamilyMap m_stFamilyMap;

	/// 家族id分配
	DWORD m_dwGenFamilyID;
};

#endif
