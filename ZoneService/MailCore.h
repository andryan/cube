/**
 * @file   MailCore.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Aug  3 17:25:58 2011
 * 
 * @brief  邮箱相关操作，hrx整理
 * 
 * 
 */


#ifndef MODI_MAILCORE_H_
#define MODI_MAILCORE_H_

#include "config.h"
#include "gamestructdef.h"
#include "MailManager.h"
#include "Base/s2rdb_cmd.h"
#include "ScriptMailDef.h"

class MODI_OnlineClient;


/**
 * @brief 邮件的相关操作
 * 
 */
class MODI_MailCore
{
 public:
	/// 邮件操作结果
	enum enResult
	{
		kOk = 0,
		kAsyncLoading,
		kLoadingFaild,
		kAlreadyInPool,
		kLoadingFaildFromDB,
		kParamInVaild,
		kUnknowErr,
	};

	/** 
	 * @brief 获取最后一个邮件id
	 * 
	 */
	static MODI_DBMail_ID GetLastMailID()
	{
		return ms_LastMailID;
	}

	/** 
	 * @brief 设置邮件id
	 * 
	 */
	static void SetLastMailID(MODI_DBMail_ID id)
	{
		ms_LastMailID = id;
	} 

	/** 
	 * @brief 获取下一个邮件id
	 * 
	 */
	static MODI_DBMail_ID  GetNextMailID() 
	{ 
		++ms_LastMailID;
		return ms_LastMailID;
	}


	
	/** 
	 * @brief 邮件附件检查
	 * 
	 * @param mailType 邮件类型
	 * @param extraInfo 附件信息
	 * 
	 * @return 合法true
	 *
	 */
	static bool CheckAttachment( enMailType mailType , const MODI_MailExtraInfo &  extraInfo );


	/** 
	 * @brief 玩家发送邮件给玩家(没有附件的)
	 * 
	 * @param pSender 发送者
	 * @param mail 邮件信息
	 * 
	 * @return 成功true
	 *
	 */
	static bool ClientSendMailToClient( MODI_OnlineClient * pSender , const MODI_SendMailInfo & mail);


	/** 
	 * @brief 玩家赠送玩家的物品, 主要是带有transid号
	 * 
	 * @param mail 邮件信息
	 * @param pRecever 接收者
	 * @param szSender 发送者
	 * @param transid 交易id
	 * @param pMailDBID 返回邮件id
	 * 
	 */
	static bool SendMail(const MODI_SendMailInfo & mail, const char * szSender = CONFIG_SYSTEM_MAIL, MODI_DBMail_ID * pMailDBID = NULL, QWORD transid = 0);


	/** 
	 * @brief 发送系统邮件
	 * 
	 */
	static bool SendScriptMail( const MODI_MailInfo & mail , const MODI_ScriptMailData & scriptdata , MODI_DBMail_ID * pMailDBID = NULL);


	/** 
	 * @brief 发送邮件给某玩家，私有的函数
	 * 
	 * @param pSender 发送者
	 * @param pList 邮件列表
	 * @param pMail 邮件
	 * 
	 */
	static bool SendTo(MODI_OnlineClient * pSender , MODI_OnlineClient * pRecever , MODI_Mail * pMail);


	/** 
	 * @brief 把某个玩家的邮件发送给客户端,是一封一封的发
	 * 
	 * @param pRecever 此玩家
	 * @param mail 此邮件
	 * @param iType 理由
	 *
	 */
	static void SendAddMailPackage( MODI_OnlineClient * pRecever , const MODI_Mail & mail , int iType);


	/** 
	 * @brief 获取某个邮件的附件
	 * 
	 * @param pClient 客户端
	 * @param id 邮件id
	 *
	 */
	static void GetAttachment( MODI_OnlineClient * pClient , const MODI_GUID & id );


	static enResult OnClientLogin( MODI_OnlineClient * pClient );

		
 private:
   	/// 全局的邮件id
	static MODI_DBMail_ID 		ms_LastMailID;
};

#endif

