#include "SingleMusicMgr.h"

#include "DBStruct.h"
#include "DBClient.h"
#include "OnlineClient.h"
#include "protocol/c2gs_Activity.h"
#include "GameSvrClientTask.h"
#include "OnlineClientMgr.h"
#include "ZoneServerAPP.h"
#include "config.h"
#include "Global.h"

MODI_SingMusicMgr * MODI_SingMusicMgr::m_pInstance = NULL;

/**
 * @brief 从数据库直接加载单曲列表信息
 *
 */
MODI_SingMusicMgr  & MODI_SingMusicMgr::GetInstance()
{
		if( ! m_pInstance)
		{
			m_pInstance = new MODI_SingMusicMgr();
		}
		return * m_pInstance;
}

bool	MODI_SingMusicMgr::Init()
{

	int	_nMusiccount;
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("[single_music_init] single music unable to get dbclient");
		return false;
	}

	MODI_RecordContainer  record_select;
	MODI_Record   select;
	select.Put("musicid");
	select.Put("singcount");
	select.Put("highestscore");	
	select.Put("scoreperson");
	select.Put("highestprecise");
	select.Put("preciseperson");
	select.Put("unix_timestamp(scoretime)");
	select.Put("unix_timestamp(precisetime)");
	///  Key Mode
	select.Put("keyscore");	
	select.Put("keyscoreperson");
	select.Put("keyprecise");
	select.Put("keypreciseperson");
	select.Put("unix_timestamp(keyscoretime)");
	select.Put("unix_timestamp(keyprecisetime)");
	
	_nMusiccount = p_dbclient->ExecSelect( record_select,MODI_ZoneServerAPP::m_pSingleMusicTbl,NULL,&select,true);

#ifdef _XXP_DEBUG
	Global::logger->debug("[single_music_init] get single music record <num=%u>",_nMusiccount);
#endif

	if( _nMusiccount == 0)
	{
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	}	

	for( int i=0;  i< _nMusiccount; ++i)
	{
		MODI_Record  * p_select_record = record_select.GetRecord(i);
		if( p_select_record == NULL)
		{
			continue;
		}
		MODI_VarType v_musicid = p_select_record->GetValue("musicid");
		MODI_VarType v_singcount = p_select_record->GetValue("singcount");
		MODI_VarType v_highestscore = p_select_record->GetValue("highestscore");
		MODI_VarType v_scoreperson = p_select_record->GetValue("scoreperson");
		MODI_VarType v_highestprecise = p_select_record->GetValue("highestprecise");
		MODI_VarType v_preciseperson = p_select_record->GetValue("preciseperson");
		MODI_VarType v_scoretime = p_select_record->GetValue("unix_timestamp(scoretime)");
		MODI_VarType v_precisetime = p_select_record->GetValue("unix_timestamp(precisetime)");
		/// Key mode
		MODI_VarType v_Keyscore = p_select_record->GetValue("keyscore");
		MODI_VarType v_Keyscoreperson = p_select_record->GetValue("keyscoreperson");
		MODI_VarType v_Keyprecise = p_select_record->GetValue("keyprecise");
		MODI_VarType v_Keypreciseperson = p_select_record->GetValue("keypreciseperson");
		MODI_VarType v_Keyscoretime = p_select_record->GetValue("unix_timestamp(keyscoretime)");
		MODI_VarType v_Keyprecisetime = p_select_record->GetValue("unix_timestamp(keyprecisetime)");

		if( v_musicid.Empty() || v_singcount.Empty() || v_highestscore.Empty() )
		{
			MODI_ASSERT(0);
			continue;
		}


		struct SingleMusic * p_infos = new SingleMusic();
		MODI_ASSERT(p_infos != NULL);
		p_infos->m_wMusicid = (WORD) v_musicid;
		p_infos->m_dwCount = (DWORD) v_singcount;
		p_infos->m_dwScore = (DWORD) v_highestscore;
		p_infos->m_wPrecise = (WORD) v_highestprecise;
		p_infos->m_tScoreFresh = (long) v_scoretime;
		p_infos->m_tPreciseFresh = (long) v_precisetime;
		strncpy( p_infos->m_szScorePerson,(const char *) v_scoreperson,sizeof(p_infos->m_szScorePerson));
		strncpy( p_infos->m_szPrecisePerson,(const char*) v_preciseperson,sizeof(p_infos->m_szPrecisePerson));
		/// Keymode
		p_infos->m_dwKeyScore = (DWORD) v_Keyscore;
		p_infos->m_wKeyPrecise = (WORD) v_Keyprecise;
		p_infos->m_tKeyScoreFresh = (long) v_Keyscoretime;
		p_infos->m_tKeyPreciseFresh = (long) v_Keyprecisetime;
		strncpy( p_infos->m_szKeyScorePerson,(const char *) v_Keyscoreperson,sizeof(p_infos->m_szScorePerson));
		strncpy( p_infos->m_szKeyPrecisePerson,(const char*) v_Keypreciseperson,sizeof(p_infos->m_szPrecisePerson));
		//strncpy( p_infos->m_szScoreFresh,(const char *)v_scoretime,sizeof(p_infos->m_szScoreFresh));
		//strncpy( p_infos->m_szPreciseFresh,(const char *)v_precisetime,sizeof(p_infos->m_szPreciseFresh));

		if(! InsertMusic( p_infos))
		{
			Global::logger->debug("<single_music_insert> errorr Got music   <id=%u,count=%u,score=%u,precise=%u,scorefresh=%u,precifresh=%u>",p_infos->m_wMusicid,p_infos->m_dwCount,p_infos->m_dwScore,p_infos->m_wPrecise,p_infos->m_tScoreFresh,p_infos->m_tPreciseFresh);
		}

	}

	return true;

}

bool	MODI_SingMusicMgr::InsertMusic( SingleMusic * p_music)
{
	if( !p_music)
		return false;

	defSingleMapIter  itor = m_mMusicMap.find(p_music->m_wMusicid);
	if( itor != m_mMusicMap.end())
	{
		return false;
	}	

	m_mMusicMap.insert(defSingleMapValue(p_music->m_wMusicid,p_music));	
	return true;
}

SingleMusic  * MODI_SingMusicMgr::GetMusic(WORD id)
{
	defSingleMapIter  itor = m_mMusicMap.find(id);
	if( itor != m_mMusicMap.end())
	{
		return itor->second;
	}
	return NULL;
}

WORD 	MODI_SingMusicMgr::GenPackage( SingleMusicInfo* buf, enGameMode mode_t)
{
	SingleMusicInfo  * pInfo=(SingleMusicInfo * ) buf;
	WORD	i=0;
	defSingleMapIter   iter = m_mMusicMap.begin();
	if( mode_t == enGameMode_Sing)
	{

		for( ; iter != m_mMusicMap.end(); ++iter)
		{
//Global::logger->info("SING %u %u %u %u %s %s",iter->second->m_wMusicid,iter->second->m_dwCount,iter->second->m_dwScore,iter->second->m_wPrecise,iter->second->m_szScorePerson,iter->second->m_szPrecisePerson);
			pInfo[i].m_wMusicid = iter->second->m_wMusicid;
			pInfo[i].m_dwCount = iter->second->m_dwCount;
			pInfo[i].m_dwScore = iter->second->m_dwScore;
			pInfo[i].m_wPrecise = iter->second->m_wPrecise;
			strncpy( pInfo[i].m_szScorePerson,iter->second->m_szScorePerson,sizeof(pInfo[i].m_szScorePerson));
			strncpy( pInfo[i].m_szPrecisePerson,iter->second->m_szPrecisePerson,sizeof(pInfo[i].m_szScorePerson));
			pInfo[i].m_tScoreFresh = iter->second->m_tScoreFresh;
			pInfo[i].m_tPreciseFresh  = iter->second->m_tPreciseFresh;

			++i;
if(i==1022) break; // without this, this part crashes server because the packet buffer for music highscores is only 65536 when music is too many (over >1000)
		}
	}
	else
	{
		i=0;
		for( ; iter != m_mMusicMap.end(); ++iter)
		{
//Global::logger->info("KEY %u %u %u %u %s %s",iter->second->m_wMusicid,iter->second->m_dwCount,iter->second->m_dwKeyScore,iter->second->m_wPrecise,iter->second->m_szKeyScorePerson,iter->second->m_szKeyPrecisePerson);
			pInfo[i].m_wMusicid = iter->second->m_wMusicid;
			pInfo[i].m_dwCount = iter->second->m_dwCount;
			pInfo[i].m_dwScore = iter->second->m_dwKeyScore;
			pInfo[i].m_wPrecise = iter->second->m_wKeyPrecise;
			strncpy( pInfo[i].m_szScorePerson,iter->second->m_szKeyScorePerson,sizeof(pInfo[i].m_szScorePerson));
			strncpy( pInfo[i].m_szPrecisePerson,iter->second->m_szKeyPrecisePerson,sizeof(pInfo[i].m_szScorePerson));
			pInfo[i].m_tScoreFresh = iter->second->m_tKeyScoreFresh;
			pInfo[i].m_tPreciseFresh  = iter->second->m_tKeyPreciseFresh;

			++i;
if(i==1022) break;
		}

	}
	return i;
}	

bool	MODI_SingMusicMgr::OnRecvMusicInfo(SingleMusicInfo * p_music ,enGameMode mode_t)
{
	if( !p_music)
		return false;
	SingleMusic * music=GetMusic(p_music->m_wMusicid);
	if( music == NULL)
	{
		SingleMusic * newmusic = new SingleMusic();
		MODI_ASSERT(newmusic );
//		*newmusic = *p_music;
		newmusic->m_dwCount=1;
		newmusic->m_wMusicid = p_music->m_wMusicid;

		if( mode_t == enGameMode_Sing)
		{
			newmusic->m_dwScore = p_music->m_dwScore;
			newmusic->m_wPrecise = p_music->m_wPrecise;
			strncpy( newmusic->m_szScorePerson,p_music->m_szScorePerson,sizeof(newmusic->m_szScorePerson));
			strncpy( newmusic->m_szPrecisePerson,p_music->m_szPrecisePerson,sizeof( newmusic->m_szPrecisePerson));
			newmusic->m_tPreciseFresh =Global::m_stLogicRTime.GetSec();
			newmusic->m_tScoreFresh = Global::m_stLogicRTime.GetSec();	
		}
		else
		{
			newmusic->m_dwKeyScore = p_music->m_dwScore;
			newmusic->m_wKeyPrecise = p_music->m_wPrecise;
			strncpy( newmusic->m_szKeyScorePerson,p_music->m_szScorePerson,sizeof(newmusic->m_szScorePerson));
			strncpy( newmusic->m_szKeyPrecisePerson,p_music->m_szPrecisePerson,sizeof( newmusic->m_szPrecisePerson));
			newmusic->m_tKeyPreciseFresh =Global::m_stLogicRTime.GetSec();
			newmusic->m_tKeyScoreFresh = Global::m_stLogicRTime.GetSec();	
			
		}

		InsertMusic(newmusic);	
		/// 建立档案
		NewMusicToDB(newmusic);
	}	
	else
	{
		/// 	修改档案
		music->m_dwCount++;
		if( mode_t == enGameMode_Sing)
		{
			if( p_music->m_dwScore > music->m_dwScore)
			{
				music->m_dwScore = p_music->m_dwScore;
				strncpy(music->m_szScorePerson,p_music->m_szScorePerson,sizeof(p_music->m_szScorePerson));
				music->m_tScoreFresh = Global::m_stLogicRTime.GetSec();
			}	
			if( p_music->m_wPrecise > music->m_wPrecise)
			{
				music->m_wPrecise = p_music->m_wPrecise;
				strncpy(music->m_szPrecisePerson,p_music->m_szPrecisePerson,sizeof(p_music->m_szPrecisePerson));
				music->m_tPreciseFresh = Global::m_stLogicRTime.GetSec();
			}
		}
		else
		{
			if( p_music->m_dwScore > music->m_dwKeyScore)
			{
				music->m_dwKeyScore = p_music->m_dwScore;
				strncpy(music->m_szKeyScorePerson,p_music->m_szScorePerson,sizeof(p_music->m_szScorePerson));
				music->m_tKeyScoreFresh = Global::m_stLogicRTime.GetSec();
			}	
			if( p_music->m_wPrecise > music->m_wKeyPrecise)
			{
				music->m_wKeyPrecise = p_music->m_wPrecise;
				strncpy(music->m_szKeyPrecisePerson,p_music->m_szPrecisePerson,sizeof(p_music->m_szPrecisePerson));
				music->m_tKeyPreciseFresh = Global::m_stLogicRTime.GetSec();
			}
			

		}
	}
	
	return true;
}

bool	MODI_SingMusicMgr::SyncToGame()
{
	char	m_szPacket[Skt::MAX_USERDATASIZE];
	MODI_ZS2S_Notify_SingleMusicList  *plist=(MODI_ZS2S_Notify_SingleMusicList* )m_szPacket;
	AutoConstruct(plist);
	static   BYTE perk = 0;
	++perk;
	perk %= 2;
	size_t size=0;
	if( perk  == 0)
	{
//Global::logger->info("SING %s\n", plist->m_stMusic);
		plist->m_wNum = GenPackage( plist->m_stMusic,enGameMode_Sing);

		size= sizeof( MODI_ZS2S_Notify_SingleMusicList)+sizeof(SingleMusicInfo)*plist->m_wNum;
		plist->m_eMode = enGameMode_Sing;
	}
	else
	{
//Global::logger->info("KEY %s\n", plist->m_stMusic);
		plist->m_wNum = GenPackage( plist->m_stMusic,enGameMode_Key);

		size= sizeof( MODI_ZS2S_Notify_SingleMusicList)+sizeof(SingleMusicInfo)*plist->m_wNum;
		plist->m_eMode = enGameMode_Key;
	}
	MODI_GameSvrClientTask::BroadcastAllServer( plist,size);
	return true;
}

bool	MODI_SingMusicMgr::SyncToDataBase()
{
	///  暂时定义为分成5批进行 数据库更新
	static  BYTE nCount = 0;
	++nCount;
	nCount %= 3;
	char	m_szPacket[Skt::MAX_USERDATASIZE];
	MODI_S2RDB_FlushSingleMusicList * list = (MODI_S2RDB_FlushSingleMusicList *)m_szPacket;
	AutoConstruct(list);
	
	defSingleMapIter  iter = m_mMusicMap.begin();
	WORD i=0;
	for( ; iter != m_mMusicMap.end(); ++iter)
	{
		if( iter->second->m_wMusicid %3 == nCount )
			list->m_stMusic[i++] = *(iter->second);
	}	
	
	list->m_wNum = i;	
	MODI_DBProxyClientTask   *pDB = GetApp()->GetDBInterface(1);
	if( !pDB)
	{
		Global::logger->debug("[flush_music_to_db] error can't get DBhandle");
		return  false;
	}
	pDB->SendCmd( list,(sizeof(MODI_S2RDB_FlushSingleMusicList)+sizeof(SingleMusic)*list->m_wNum));
	
	
	return true;
}

void	MODI_SingMusicMgr::NewMusicToDB( SingleMusic * newmusic)
{
	MODI_S2RDB_NewSingleMusic save;
	save.m_stMusic = * newmusic;
	MODI_DBProxyClientTask   *pDB = GetApp()->GetDBInterface(1);
	if( !pDB)
	{
		Global::logger->debug("[new_music_to_db] error can't get DBhandle");
		return ;
	}
	pDB->SendCmd(&save,sizeof(MODI_S2RDB_NewSingleMusic));
}

void	MODI_SingMusicMgr::Update()
{
	if( m_DbTime (Global::m_stLogicRTime))
	{
		SyncToDataBase();
	}

	if( m_GameTime( Global::m_stLogicRTime))
	{
		SyncToGame();
	}
}



