/** 
 * @file World.h
 * @brief 游戏世界
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MODI_ZONEWORLD_H_
#define MODI_ZONEWORLD_H_

#include "OnlineClientMgr.h"
#include "OfflineUserMgr.h"


class 	MODI_World : public CSingleObject<MODI_World>
{
	public:

	bool 	OnClientLogin( MODI_OnlineClient * pClient );

	bool 	OnClientLoginout( MODI_OnlineClient * pClient );

	void 	OnServerDisconnection( BYTE byServer );
};

#endif
