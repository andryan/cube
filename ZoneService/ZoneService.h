/** 
 * @file ZoneService.h
 * @brief ZONE SERVER 上的网络服务对象
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-13
 */

#ifndef ZONESVR_SERVICE_H_
#define ZONESVR_SERVICE_H_

#include <vector>
#include <iostream>
#include "MNetService.h"
#include "ServiceTaskSched.h"
#include "SingleObject.h"
#include "DBStruct.h"

enum
{
	ZSSVR_PORTIDX_SVRS = 0,
	ZSSVR_PORTIDX_DB = 1,
	ZSSVR_PORTIDX_COUNT,
};

/**
 * @brief 多监听的服务器
 * 
 */
class MODI_ZoneService : public MODI_MNetService , 
	public CSingleObject<MODI_ZoneService>
{
 public:

	MODI_ZoneService(std::vector<WORD > &  vec, 
			const std::vector<std::string> & vecip , 
			const char * name = "ZoneService", 
			const int count = ZSSVR_PORTIDX_COUNT );

	bool CreateTask(const int & sock, 
			const struct sockaddr_in * addr , 
			const WORD & port);
		
	virtual bool Init();

	virtual void Final();

	void DealSignalUsr2();

   	void ReloadConfig();
	
 private:

	MODI_ServiceTaskSched m_pTaskSched[ZSSVR_PORTIDX_COUNT];
};


#endif
