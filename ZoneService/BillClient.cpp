/**
 * @file   BillClient.cpp
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Fri Oct 15 12:02:10 2010
 * @version $Id:$
 * @brief  交易连接
 * 
 *
 */

#include "AssertEx.h"
#include "BillClient.h"
#include "GameSvrClientTask.h"
#include "OnlineClient.h"
#include "OnlineClientMgr.h"
#include "Base/s2zs_cmd.h"
#include "protocol/c2gs_pay.h"
#include "FamilyManager.h"
#include "MailCore.h"
#include "OfflineUserMgr.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"

MODI_BillClient * MODI_BillClient::m_pInstance = NULL;
 
/** 
 * @brief 初始化
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BillClient::Init()
{
	if(! MODI_RClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 释放
 * 
 */
void MODI_BillClient::ClientFinal()
{
	Terminate(true);
	TTerminate();
	Join();
}


/** 
 * @brief 发送命令
 * 
 * @param pt_null_cmd 要发送的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BillClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}
	return MODI_RClientTask::SendCmdInConnect(pt_null_cmd, cmd_size);
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 
 */
bool MODI_BillClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd))))
    {
        return false;
    }
	this->Put( pt_null_cmd , cmd_size );
    return true;
}

/** 
 * @brief 队列内的命令
 * 
 * @param pt_null_cmd 获取的命令
 * @param dwCmdLen 命令长度
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BillClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	if(pt_null_cmd == NULL || dwCmdLen < 2)
	{
		return false;
	}

#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] billclient recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, dwCmdLen);
#endif
	
	if(pt_null_cmd->byCmd == MODI_TransCommand::m_bySCmd)
	{
		switch(pt_null_cmd->byParam)
		{
			/// 刷新显示
			case MODI_MRefreshMoney::m_bySParam :
			{
				MODI_MRefreshMoney * recv_cmd = (MODI_MRefreshMoney *)pt_null_cmd;
				MRefreshMoney(recv_cmd);
			}
			break;

			/// 消费扣款返回
			case MODI_INAItemReturn::m_bySParam :
			{
				const MODI_INAItemReturn * recv_cmd = (const MODI_INAItemReturn *)pt_null_cmd;
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( recv_cmd->m_byServerId);
				if( pServer )
				{
					pServer->SendCmd( recv_cmd , dwCmdLen );
				}
			}
			break;

			case MODI_INAGiveReturn::m_bySParam :
			{
				const MODI_INAGiveReturn * recv_cmd = (const MODI_INAGiveReturn *)pt_null_cmd;
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( recv_cmd->m_byServerId);
				if( pServer )
				{
					pServer->SendCmd( recv_cmd , dwCmdLen );
				}
			}
			break;

			/// 充值返回
			case MODI_GS2C_Notify_PayResult::ms_SubCmd:
			{
				const MODI_GS2C_Notify_PayResult * recv_cmd = (const MODI_GS2C_Notify_PayResult *)pt_null_cmd;
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( recv_cmd->m_byServerid);
				if( pServer )
				{
					pServer->SendCmd( recv_cmd , dwCmdLen );
				}
				return true;
			}
			break;

			default:
			{
				Global::logger->fatal("[recv_cmd] billclient recv a invalid param %d", pt_null_cmd->byParam);
			}
			break;
		}
		
		return true;
	}
	
	/// 兑换充值命令
	else if(pt_null_cmd->byCmd == MODI_PayCmd::m_bySCmd)
	{
		switch(pt_null_cmd->byParam)
		{
			/// 充值返回
			case MODI_GS2C_Notify_PayResult::ms_SubCmd:
			{
				const MODI_GS2C_Notify_PayResult * recv_cmd = (const MODI_GS2C_Notify_PayResult *)pt_null_cmd;
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( recv_cmd->m_byServerid);
				if( pServer )
				{
					pServer->SendCmd( recv_cmd , dwCmdLen );
				}
				return true;
			}
			break;

			default:
			{
				Global::logger->fatal("[recv_cmd] billclient recv a invalid pay cmd param %d", pt_null_cmd->byParam);
				return false;
			}
			break;
		}
		return true;
	}

	/// 家族升级结果
	else if(pt_null_cmd->byCmd == MAINCMD_S2RDB)
	{
		if(pt_null_cmd->byParam == MODI_RDB2S_Notify_FamilyLevel::ms_SubCmd)
		{
			const MODI_RDB2S_Notify_FamilyLevel * p_recv_cmd = (const MODI_RDB2S_Notify_FamilyLevel *)pt_null_cmd;
			/// 失败
			if(p_recv_cmd->m_byResult == 0)
			{
				MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
				MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
				if( pOnline )
				{
					pOnline->RetFamilyResult(enLevelFamily_Error);
				}
			}
			else if(p_recv_cmd->m_byResult == 1)
			{
				/// 成功处理
				MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_recv_cmd->m_dwFamilyID);
				if(! p_family)
				{
					Global::logger->fatal("[onlevel_family] on level family failed,not find family <familyid=%u>", p_recv_cmd->m_dwFamilyID);
					MODI_ASSERT(0);
					return false;
				}

				if(p_family->Levelup())
				{
					MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
					MODI_OnlineClient * pOnline = pMgr->FindByAccid(p_recv_cmd->m_dwAccountID);
					if( pOnline )
					{
						pOnline->RetFamilyResult(enLevelFamily_Succ);
					}
				}
			}
			return true;
		}
		return false;
	}
	else
	{
		Global::logger->fatal("[recv_cmd] billclient recv a invalid bycmd  %d", pt_null_cmd->byCmd);
		return false;
	}
	
	return true;	
}

/** 
 * @brief 请求刷新显示
 * 
 * @param accid 要刷新的id
 *
 */
void MODI_BillClient::SRefreshMoney(const char * acc_name, const DWORD  & accid)
{
	MODI_SRefreshMoney send_cmd;
	send_cmd.m_dwAccID = accid;
	strncpy(send_cmd.m_cstrAccName, acc_name, sizeof(send_cmd.m_cstrAccName));
	SendCmd(&send_cmd, sizeof(send_cmd));
}




/** 
 * @brief 主动刷新显示
 * 
 * @param accid 要刷新的id
 * @param money 此账号所拥有的钱
 * 
 */
void MODI_BillClient::MRefreshMoney(const MODI_MRefreshMoney * p_recv_cmd)
{
	defAccountID accid = p_recv_cmd->m_stAccID;
	DWORD money = p_recv_cmd->m_dwMoney;

	BYTE client_sex = 0;
	std::string client_name;
	
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pOnline = pMgr->FindByAccid( accid );
	if( pOnline )
	{
		client_name = pOnline->GetRoleName();
		client_sex = pOnline->GetSexType();
		
		MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnline->GetServerID() );
		if( pServer )
		{
			MODI_ZS2S_Notify_RefreshMoney msg;
			msg.m_charid = pOnline->GetCharID();
			msg.m_RMBMoney = money;
			msg.m_enReason = p_recv_cmd->m_enReason;
			pServer->SendCmd(&msg,sizeof(msg));

			Global::logger->info("[%s] send refresh money cmd to server<%u> , client<accid=%u,charid=%u,name=%s,reason=%u>" , 
					SVR_TEST ,
					pOnline->GetServerID() , 
					pOnline->GetAccid(),
					pOnline->GetCharID(),
								 pOnline->GetRoleName(),
								 p_recv_cmd->m_enReason);
			
		}
	}
	else
	{
		if(p_recv_cmd->m_enReason == enMRefreshMoney_None)
		{
			Global::logger->debug("[mrefresh_pay_return_direct] pay client not online, but not can get gift, so return direct <accid=%llu>", accid);
			return;
		}
		
		/// 玩家离线
		MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
		MODI_OfflineUser * pOfflineUser = pOfflineMgr->FindByAccId(accid);
		if( pOfflineUser )
		{
			client_sex = pOfflineUser->GetSex();
			client_name = pOfflineUser->GetName();
		}
		else 
		{
			/// 向db请求后，db返回加入离线管理器，最后还是走这个流程
			MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
			if( pDB )
			{
				char buf[Skt::MAX_USERDATASIZE];
				memset(buf, 0, sizeof(buf));
				MODI_S2RDB_Request_RoleDetailInfo * reqRoleInfo = (MODI_S2RDB_Request_RoleDetailInfo *)buf;
				AutoConstruct(reqRoleInfo);
				
				reqRoleInfo->m_opt = kReqPayAction;
				reqRoleInfo->m_dwAccId = accid;
				/// 带上准备邮件的信息
				reqRoleInfo->m_nExtraSize = 0;
				DWORD * p_startaddr = (DWORD *)(&(reqRoleInfo->m_szExtra[0]));
				*p_startaddr = (DWORD)p_recv_cmd->m_enReason;
				reqRoleInfo->m_nExtraSize += sizeof(DWORD);
				p_startaddr++;
				*p_startaddr = (DWORD)p_recv_cmd->m_dwPayMoney;
				reqRoleInfo->m_nExtraSize += sizeof(DWORD);
				
				pDB->SendCmd(reqRoleInfo , sizeof(MODI_S2RDB_Request_RoleDetailInfo) + reqRoleInfo->m_nExtraSize);
				Global::logger->debug("[pay_active] client pay but never login <accid=%u,paymoney=%u,reason=%u>",
									  accid, p_recv_cmd->m_dwPayMoney, p_recv_cmd->m_enReason);
			}
			else 
			{
				Global::logger->debug("[pay_active_failed] client pay but never login get failed <accid=%u,paymoney=%u,reason=%u>",
									  accid, p_recv_cmd->m_dwPayMoney, p_recv_cmd->m_enReason);
			}
		}
	}

	if(client_name != "")
	{
		if(p_recv_cmd->m_enReason == enMRefreshMoney_None)
		{
			Global::logger->debug("[mrefresh_pay_return_direct] pay client online or offline, but not can get gift, so return <accid=%llu>", accid);
			return;
		}
		
		/// 活动,发邮件
		/// 发奖品
		MODI_SendMailInfo  send_pay_mail;
		send_pay_mail.Reset();
		std::string content = "恭喜您获取充值活动礼包";
		std::string caption = "甜蜜盛典";
		strncpy( send_pay_mail.m_szCaption, caption.c_str(), sizeof(send_pay_mail.m_szCaption) - 1);
		strncpy( send_pay_mail.m_szContents , content.c_str(), sizeof(send_pay_mail.m_szContents) - 1);
		strncpy( send_pay_mail.m_szRecevier , client_name.c_str(), sizeof(send_pay_mail.m_szRecevier) - 1);
		send_pay_mail.m_Type = kAttachmentItemMail;

		if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay20)
		{
			if(client_sex == 1)
			{	
			}
			else
			{

			}
			
			Global::logger->debug("[have_get_pay] is pay20 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay30)
		{
			
			Global::logger->debug("[have_get_pay] is pay30 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay60)
		{
			send_pay_mail.m_ExtraInfo[0].SetAttachment_ItemInfo(56007, 8, 3);
			MODI_MailCore::SendMail(send_pay_mail);
			Global::logger->debug("[have_get_pay] is pay60 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay100)
		{
			Global::logger->debug("[have_get_pay] is pay100 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay200)
		{
			Global::logger->debug("[have_get_pay] is pay200 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else if(p_recv_cmd->m_enReason == enMRefreshMoney_Pay500)
		{
			if(client_sex == 1)
			{	
			}
			else
			{
			}
			
			Global::logger->debug("[have_get_pay] is pay500 <accid=%u,paymoney=%u>",
								  accid, p_recv_cmd->m_dwPayMoney);
		}
		else
		{
			Global::logger->error("[pay_get_gitf_error] get a gitf error <reason=%u,accid=%u>", p_recv_cmd->m_enReason, accid);
			MODI_ASSERT(0);
			return;
		}
	}
}

const bool MODI_BillClient::IsConnected()
{
	bool is_connect = false;
	is_connect = MODI_RClientTask::IsConnected();
	return is_connect;
}
