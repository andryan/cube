/**
 * @file   MailManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Feb 10 17:33:20 2012
 * 
 * @brief  邮件系统管理
 * 
 * 
 */

#include "MailManager.h"
#include "OnlineClient.h"
#include "MailCore.h"
#include "protocol/c2gs_mail.h"
#include "ScriptMailHandler.h"

MODI_MailManager::MODI_MailManager()
{
	m_pOwnClient = NULL;
}


MODI_MailManager::~MODI_MailManager()
{
	if(m_stMailMap.size())
	{
		defMailMapIter iter = m_stMailMap.begin();
		for(; iter != m_stMailMap.end(); iter++)
		{
			MODI_Mail * pMail = iter->second;
			if(pMail)
			{
				pMail->SaveToDB();
#ifdef _DEBUG				
				Global::logger->debug("[free_mail] <id=%llu>",(QWORD)pMail->GetGUID());
#endif				
				//delete pMail;
				pMail = NULL;
			}
		}
	}
	m_pOwnClient = NULL;
}


bool MODI_MailManager::Init(MODI_OnlineClient * p_client, const MODI_DBMailInfo * mails)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	if(m_pOwnClient)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	m_pOwnClient = p_client;
	
	for( int i = 0; i < MAILLIST_MAX_COUNT; i++ )
	{
		const MODI_DBMailInfo & info = mails[i];
		if( info.m_dbID != INVAILD_DBMAIL_ID && info.m_bIsValid )
		{
			MODI_Mail * pMail = new MODI_Mail();
			pMail->Init(info);
			
#ifdef _DEBUG			
			Global::logger->debug("[add_mail] <name=%s,id=%llu>", m_pOwnClient->GetAccName(), (QWORD)pMail->GetGUID());
#endif			
			if(AddMail(pMail) == false)
			{
				//delete pMail;
				pMail = NULL;
				MODI_ASSERT(0);
			}
		}
		else
		{
			/// 没有那么多邮件
			//MODI_ASSERT(0);
		}
	}
	return true;
}




bool MODI_MailManager::AddMail(MODI_Mail * p_mail)
{
	if(!p_mail)
		return false;
	std::pair<defMailMapIter, bool > ret_code = m_stMailMap.insert(defMailMapValue(p_mail->GetGUID(), p_mail));
	return ret_code.second;
}


bool MODI_MailManager::DelMail(MODI_Mail * p_mail)
{
	if(!p_mail)
		return false;
	defMailMapIter iter = m_stMailMap.find(p_mail->GetGUID());
	if(iter != m_stMailMap.end())
	{
		/// 只需设置is_valid=0即可
		p_mail->DeleteFromDB();
		return true;
	}
	return false;
}



bool MODI_MailManager::DelMail(const MODI_GUID & mail_id)
{
	defMailMapIter iter = m_stMailMap.find(mail_id);
	if(iter != m_stMailMap.end())
	{
		MODI_Mail * p_mail = iter->second;
		if(!p_mail)
			return false;
		p_mail->DeleteFromDB();
		//delete p_mail;
		p_mail = NULL;
		m_stMailMap.erase(iter);
		return true;
	}
	return false;
}



MODI_Mail * MODI_MailManager::GetMails(const MODI_GUID & mail_id)
{
	defMailMapIter iter = m_stMailMap.find(mail_id);
	if(iter != m_stMailMap.end())
	{
		return iter->second;
	}
	return NULL;
}


void MODI_MailManager::SendUserMailsToClient()
{
	if(m_stMailMap.size())
	{
		defMailMapIter iter = m_stMailMap.begin();
		for(; iter != m_stMailMap.end(); iter++)
		{
			MODI_Mail * pMail = iter->second;
			if(pMail && (!pMail->IsServerMail()))
			{
				MODI_MailCore::SendAddMailPackage(m_pOwnClient, *pMail, MODI_GS2C_Notify_AddMail::kLogin);
			}
		}
	}	
}


void MODI_MailManager::ExecuteSvrMails()
{
	MODI_ScriptMailHandler * pScriptMailHandler = MODI_ScriptMailHandler::GetInstancePtr();
	if( !pScriptMailHandler  )
	{
		MODI_ASSERT(0);
		return;
	}
	if(! m_pOwnClient)
	{
		MODI_ASSERT(0);
		return;
	}
		
	if(m_stMailMap.size())
	{
		defMailMapIter iter = m_stMailMap.begin();
		defMailMapIter next_iter = iter;
		for(next_iter++; iter != m_stMailMap.end(); iter=next_iter)
		{
			MODI_Mail * pMail = iter->second;
			if(pMail && (pMail->IsServerMail()))
			{
				int iRet = pScriptMailHandler->ExcuteScript(m_pOwnClient, *pMail);
	
				if( iRet == kScriptMailHandler_Ok || iRet == kScriptMailHandler_NotHandler)
				{
					/// 处理但是不删除
				}
				else if( iRet == kScriptMailHandler_Del || iRet == kScriptMailHandler_NotHandler_Del)
				{
					/// 处理后删除
					pMail->DeleteFromDB();
					//				delete pMail;
					pMail = NULL;
					iter->second = NULL;
					m_stMailMap.erase(iter);
				}
				else
				{
					pMail->DeleteFromDB();
					//					delete pMail;
					pMail = NULL;
					iter->second = NULL;
					m_stMailMap.erase(iter);
				}
			}

			if(next_iter != m_stMailMap.end())
			{
				next_iter++;
			}
		}
	}	
}


void MODI_MailManager::Update()
{
	if(m_stMailMap.size())
	{
		defMailMapIter iter = m_stMailMap.begin();
		for(; iter != m_stMailMap.end(); iter++)
		{
			MODI_Mail * pMail = iter->second;
			if(pMail)
			{
				pMail->SaveToDB();
			}
		}
	}	
}
