/**
 * @file   FamilyManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sat Apr 23 17:17:11 2011
 * 
 * @brief  家族管理
 * 
 */

#include "FamilyManager.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "OnlineClient.h"
#include "protocol/family_def.h"
#include "protocol/c2gs_family.h"
#include "GameSvrClientTask.h"
#include "OnlineClientMgr.h"
#include "ZoneServerAPP.h"
#include "config.h"

#define MAX_PACKETSIZE 	1024
MODI_FamilyManager * MODI_FamilyManager::m_pInstance = NULL;

/** 
 * @brief 从数据库加载家族信息
 * 
 */
bool MODI_FamilyManager::Init()
{
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("[family_init] family manager unable get dbclient");
		return false;
	}

	/// 有个bug，先这样用一下，，
	std::string del_sql = "delete from family_members where familyid not in (select familyid from familys);";
	p_dbclient->ExecSql(del_sql.c_str(), del_sql.size());
	
	MODI_RecordContainer record_select_family;
	MODI_Record select_field_family;
	select_field_family.Put("familyid");
	select_field_family.Put("familyname");
	select_field_family.Put("createtime");
	select_field_family.Put("level");
	select_field_family.Put("totem");
	select_field_family.Put("money");
	select_field_family.Put("renqi");
	select_field_family.Put("xuanyan");
	select_field_family.Put("publics");
		
	int family_num = p_dbclient->ExecSelect(record_select_family, MODI_ZoneServerAPP::m_pFamilyTbl, NULL, &select_field_family, true);
	
#ifdef _FAMILY_DEBUG	
	Global::logger->debug("[family_init] get family record from familys <count=%u>", family_num);
#endif

	if(family_num == 0)
	{
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
		return true;
	}
	
	for(int i=0; i<family_num; i++)
	{
		MODI_Record * p_select_record = record_select_family.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		MODI_VarType v_familyid = p_select_record->GetValue("familyid");
		MODI_VarType v_familyname = p_select_record->GetValue("familyname");
		MODI_VarType v_createtime = p_select_record->GetValue("createtime");
		MODI_VarType v_level = p_select_record->GetValue("level");
		MODI_VarType v_totem = p_select_record->GetValue("totem");
		MODI_VarType v_money = p_select_record->GetValue("money");
		MODI_VarType v_renqi = p_select_record->GetValue("renqi");
		MODI_VarType v_xuanyan = p_select_record->GetValue("xuanyan");
		MODI_VarType v_publics = p_select_record->GetValue("publics");

		if(v_familyid.Empty() ||v_familyname.Empty() || v_createtime.Empty() ||
		   v_level.Empty() || v_totem.Empty() || v_renqi.Empty())
		{
			MODI_ASSERT(0);
			continue;
		}
		
		MODI_FamilyBaseInfo base_info;
		base_info.m_dwFamilyID = (defFamilyID)v_familyid;
		strncpy(base_info.m_cstrFamilyName, (const char *)v_familyname, sizeof(base_info.m_cstrFamilyName) - 1);
		/// 时间先不要
		base_info.m_wdLevel = (WORD)v_level;
		base_info.m_byTotem = (BYTE)v_totem;
		base_info.m_dwMoney = (DWORD)v_money;
		base_info.m_dwRenqi = (DWORD)v_renqi;
		strncpy(base_info.m_cstrXuanyan, (const char *)v_xuanyan, sizeof(base_info.m_cstrXuanyan) -1 );
		strncpy(base_info.m_cstrPublic, (const char *)v_publics, sizeof(base_info.m_cstrPublic) -1 );
		
		MODI_Family * p_family = new MODI_Family(base_info);
		AddFamily(p_family);
	}

	/// 家族成员列表加载
	MODI_RecordContainer record_select_member;
	MODI_Record select_field_member;
	select_field_member.Put("accountid");
	select_field_member.Put("familyid");
	select_field_member.Put("position");
	select_field_member.Put("contribute");
	select_field_member.Put("jointime");
		
	int member_num = p_dbclient->ExecSelect(record_select_member, MODI_ZoneServerAPP::m_pFamilyMemTbl, NULL, &select_field_member, true);
	
#ifdef _FAMILY_DEBUG	
	Global::logger->debug("[family_init] get member record from family_member <count=%u>", member_num);
#endif

	if(member_num == 0)
	{
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
		MODI_ASSERT(0);
		return true;
	}
	
	for(int i=0; i<member_num; i++)
	{
		MODI_FamilyMemBaseInfo base_info;
		
		MODI_Record * p_select_record = record_select_member.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		MODI_VarType v_accountid = p_select_record->GetValue("accountid");
		MODI_VarType v_familyid = p_select_record->GetValue("familyid");
		MODI_VarType v_position = p_select_record->GetValue("position");
		MODI_VarType v_contribute = p_select_record->GetValue("contribute");
		MODI_VarType v_jointime = p_select_record->GetValue("jointime");

		if(v_accountid.Empty() ||v_familyid.Empty() || v_position.Empty() || v_jointime.Empty())
		{
			MODI_ASSERT(0);
			continue;
		}

		base_info.m_dwFamilyID = (defAccountID)v_familyid;
		base_info.m_dwAccountID = (defAccountID)v_accountid;
		base_info.m_enPosition = (enFamilyPositionType)((BYTE)v_position);
		base_info.m_dwContribute = (DWORD)v_contribute;
		/// 时间不要了

		/// 读取角色表
		MODI_RecordContainer record_select_character;
		std::ostringstream where;
   		where << "accountid=" << base_info.m_dwAccountID;
		MODI_Record select_field_character;
		select_field_character.Put("guid");
		select_field_character.Put("name");
		select_field_character.Put("sex");
		select_field_character.Put("chenghao");
		select_field_character.Put("renqi");
		select_field_character.Put("UNIX_TIMESTAMP(`lastlogintime`)");

		if((p_dbclient->ExecSelect(record_select_character, MODI_ZoneServerAPP::m_pCharactersTbl, where.str().c_str(), &select_field_character)) != 1)
		{
			MODI_ASSERT(0);
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			return false;
		}

		MODI_Record * p_select_character_record = record_select_character.GetRecord(0);
		if(p_select_character_record == NULL)
		{
			MODI_ASSERT(0);
			continue;
		}

		MODI_VarType v_guid = p_select_character_record->GetValue("guid");
		MODI_VarType v_name = p_select_character_record->GetValue("name");
		MODI_VarType v_sex = p_select_character_record->GetValue("sex");
		MODI_VarType v_chenghao = p_select_character_record->GetValue("chenghao");
		MODI_VarType v_renqi = p_select_character_record->GetValue("renqi");
		MODI_VarType v_lastlogintime = p_select_character_record->GetValue("UNIX_TIMESTAMP(`lastlogintime`)");

		if(v_guid.Empty() ||v_name.Empty() || v_sex.Empty() || v_lastlogintime.Empty() || v_chenghao.Empty() || v_renqi.Empty())
		{
			MODI_ASSERT(0);
			continue;
		}
		
		base_info.m_stGuid = (QWORD)v_guid;
		strncpy(base_info.m_cstrRoleName, (const char *)v_name, sizeof(base_info.m_cstrRoleName) - 1);
		base_info.m_bySex = (BYTE)v_sex;
		base_info.m_dwRenqi = (DWORD)v_renqi;
		base_info.m_wdChenghao = (WORD)v_chenghao;
		base_info.m_qdLastlogin = (QWORD)v_lastlogintime;

		MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(base_info.m_dwFamilyID);
		if(! p_family)
		{
			Global::logger->debug("[addme_tofamily] not find family <familyid=%>", base_info.m_dwFamilyID);
			MODI_ASSERT(0);
			return false;
		}
		base_info.m_wLevel = p_family->GetFamilyLevel();
		MODI_FamilyMember * p_member = new MODI_FamilyMember(base_info);
		if(! p_member->AddMeToFamily())
		{
			MODI_ASSERT(0);
			return false;
		}
	}

	/// 把最大的familyid拿出来
	std::string get_sql = "select MAX(`familyid`) from familys;";
	MODI_RecordContainer record_select_family_id;
	int family_id_num = p_dbclient->ExecSelect(record_select_family_id, get_sql.c_str(), get_sql.size(), false);
	if(family_id_num != 1)
	{
		Global::logger->fatal("[max_family_id] select record num <%u>", family_id_num);
		MODI_ASSERT(0);
		return false;
	}

	MODI_Record * pRecord = record_select_family_id.GetRecord(0); 
	if( !pRecord )
	{
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
		MODI_ASSERT(0);
		return false;
	}

	const char * szField = "MAX(`familyid`)";
	const MODI_VarType & v_serial = pRecord->GetValue(szField);
	m_dwGenFamilyID = (DWORD)v_serial;
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[max_family_id] <%u>", m_dwGenFamilyID);
#endif

	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	Global::logger->info("[family_init] init family manager successful");
	return true;
}


/** 
 * @brief 增加家族列表
 * 
 */
void MODI_FamilyManager::NotifyAddFList(MODI_Family * p_family)
{
	if(! p_family)
	{
		MODI_ASSERT(0);
		return;
	}
	
	MODI_GS2C_Notify_AddFamilyList send_cmd;
	send_cmd.m_stListInfo.m_dwFamilyID = p_family->GetFamilyID();
	strncpy(send_cmd.m_stListInfo.m_szName, p_family->GetFamilyName(), sizeof(send_cmd.m_stListInfo.m_szName) - 1);
	send_cmd.m_stListInfo.m_wdLevel = p_family->GetFamilyLevel();
	send_cmd.m_stListInfo.m_wdMemnum = p_family->GetMemNum();
	send_cmd.m_stListInfo.m_dwRenqi = p_family->GetFamilyRenqi();
	send_cmd.m_stListInfo.m_dwMoney = p_family->GetMoney();
	MODI_GameSvrClientTask::BroadcastAllServer(&send_cmd, sizeof(send_cmd));
	
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[create_family] add a family list <name=%s,id=%u>", p_family->GetFamilyName(), p_family->GetFamilyID());
#endif
}


/** 
 * @brief 通知减少家族列表
 * 
 * @param family_id 
 */
void MODI_FamilyManager::NotifyDelFList(const defFamilyID & family_id)
{
	MODI_GS2C_Notify_DelFamilyList send_cmd;
	send_cmd.m_dwFamilyID = family_id;
	MODI_GameSvrClientTask::BroadcastAllServer(&send_cmd, sizeof(send_cmd));
}


/** 
 * @brief 通知刷新家族列表给所有的人
 * 
 */
void MODI_FamilyManager::NotifyFamilyList()
{
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_FamilyList * p_send_cmd = (MODI_GS2C_Notify_FamilyList *)buf;
	AutoConstruct(p_send_cmd);
	MODI_FamilyListInfo * p_info = &(p_send_cmd->m_pListInfo[0]);
	if(Size() > 0)
	{
		MODI_Family * p_family = NULL;
		defFamilyMapIter iter = m_stFamilyMap.begin();
		for(; iter != m_stFamilyMap.end(); iter++)
		{
			p_family = iter->second;
			
			if( p_family->GetFamilyLevel() < 2 )
				continue;
			p_info->m_dwFamilyID = p_family->GetFamilyID();
			strncpy(p_info->m_szName, p_family->GetFamilyName(), sizeof(p_info->m_szName) - 1);
			p_info->m_wdLevel = p_family->GetFamilyLevel();
			p_info->m_wdMemnum = p_family->GetMemNum();
			p_info->m_dwRenqi = p_family->GetFamilyRenqi();
			p_info->m_dwMoney = p_family->GetMoney();
			
			p_info++;
			p_send_cmd->m_dwSize++;

			if( (char*)p_info - (char*)buf > (int)(sizeof(buf) - sizeof(MODI_FamilyListInfo)) )
				break;
		}
	}
	MODI_GameSvrClientTask::BroadcastAllServer(p_send_cmd, sizeof(MODI_GS2C_Notify_FamilyList) + sizeof(MODI_FamilyListInfo) * p_send_cmd->m_dwSize);
	
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[send_family_list] send a family list to all client <size=%u>", p_send_cmd->m_dwSize);
#endif
}


/** 
 * @brief 删除所有信息
 * 
 */
void MODI_FamilyManager::OnFree()
{
	/// 先不实现
}



/** 
 * @brief 请求创建家族，所有条件都满足了
 * 
 * @param create_info 创建家族的信息
 * @param p_client 此人创建
 * 
 * @return 成功或者失败
 *
 */
bool MODI_FamilyManager::CreateFamily(MODI_FamilyBaseInfo & create_info, MODI_OnlineClient * p_client)
{
	if(p_client == NULL)
	{
		MODI_ASSERT(0);
		return false;
	}

	defFamilyID family_id = GenFamilyID();
	if(family_id == INVAILD_FAMILY_ID)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	create_info.m_dwFamilyID = family_id;
	
	MODI_Family * p_family = new MODI_Family(create_info, p_client);
	
	if( ! AddFamily(p_family))
	{
		delete p_family;
		p_family = NULL;
		MODI_ASSERT(0);
		return false;
	}
	
	if(! p_family->OnCreate(p_client))
  	{
		RemoveFamily(p_family);
	    return false;
	}

#ifdef _FAMILY_DEBUG
	Global::logger->debug("[create_family] create family succeful <name=%s,familyid=%u,familyname=%s>",
						  p_client->GetRoleName(), p_family->GetFamilyID(), p_family->GetFamilyName());
#endif
	
	/// 通知在家族里面的人增加家族列表
	NotifyAddFList(p_family);

	/// 发系统公告
	std::ostringstream os;
	os<< p_client->GetRoleLinkName() << "create family" << p_family->GetFamilyName() << "successful";
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	pMgr->BroadcastSysChat(os.str().c_str(), os.str().size());
	
	return true;
}


/** 
 * @brief 解散家族
 * 
 *
 */
enFamilyOptResult MODI_FamilyManager::FreeFamily(MODI_OnlineClient * p_client)
{
	MODI_Family * p_family = FindFamily(p_client->GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[onfree_family] on free family failed,not find family <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(p_client->GetGUID() != p_family->GetMasterID())
	{
		Global::logger->fatal("[onfree_family] on free family failed, client not master <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(! p_family->OnFree(p_client))
	{
		return enFamilyRetT_Free_Member;
	}

	NotifyDelFList(p_family->GetFamilyID());
	RemoveFamily(p_family);
	
	return enFamilyFreeRetT_Ok;
}


/** 
 * @brief 家族升级
 * 
 *
 */
enFamilyOptResult MODI_FamilyManager::FamilyLevel(MODI_OnlineClient * p_client)
{
	enFamilyOptResult result = enFamilyOptResult_None;
	MODI_Family * p_family = FindFamily(p_client->GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[onlevel_family] on free family failed,not find family <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}


	if(! (p_client->IsFamilyMaster() || p_client->IsFamilySlaver()))
	{
		Global::logger->fatal("[onlevel_family] on free family failed, client not master <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enLevelFamily_NoAuth;
	}

	if(Global::g_byGameShop == 2)
	{
		if(p_client->GetMoneyRmb() < 1000)
		{
			MODI_ASSERT(0);
			return enLevelFamily_NoMoney;
		}
	}
	else if(Global::g_byGameShop == 1)
	{
		if(p_client->GetMoneyRmb() < 4000)
		{
			MODI_ASSERT(0);
			return enLevelFamily_NoMoney;
		}
	}
	else
	{
		if(p_client->GetMoneyRmb() < nsFamilyLevel::LEVEL_MONEY)
		{
			MODI_ASSERT(0);
			return enLevelFamily_NoMoney;
		}
	}

	result = p_family->IsCanLevelup();
	if(result != enLevelFamily_Succ)
	{
		return result;
	}
	
	if(! p_family->OnLevelup(p_client))
	{
		return enLevelFamily_Error;
	}
	
	return enFamilyOptResult_None;
}


/** 
 * @brief 某个玩家请求家族
 * 
 * @param p_client 改玩家请求
 * @param family_id 请求家族的id
 * 
 */
enFamilyOptResult MODI_FamilyManager::RequestFamily(MODI_OnlineClient * p_client, const defFamilyID & family_id)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(family_id);
	if(!p_family)
	{
		Global::logger->warn("[request_family] unable find family when client request family <name=%s,familyid=%u>",
							 p_client->GetRoleName(), family_id);
		MODI_ASSERT(0);
		return enFamilyRetT_Request_No;
	}

	if(! p_client->IsHaveFamily())
	{		
		return p_family->OnRequest(p_client);
	}
	else if(p_client->IsFamilyRequest())
	{
		MODI_Family * p_old_family = MODI_FamilyManager::GetInstance().FindFamily(p_client->GetFamilyID());
		if(!p_old_family)
		{
			Global::logger->warn("[find_family] unable find old family when client request family <name=%s,familyid=%u>",
								 p_client->GetRoleName(), family_id);
			MODI_ASSERT(0);
			return enFamilyRetT_Request_No;
		}
		if(p_old_family->IsFamilyMember(p_client->GetGUID()))
		{
			p_old_family->OnCancel(p_client);
			return p_family->OnRequest(p_client);
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	else
	{
		MODI_ASSERT(0);
	}
	
	return enFamilyOptResult_Unknow;
}


/** 
 * @brief 某人取消加入家族
 * 
 * @param p_client 取消的人
 * 
 *
 */
enFamilyOptResult MODI_FamilyManager::CancelFamily(MODI_OnlineClient * p_client)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_client->GetFamilyID());
	if(!p_family)
	{
		Global::logger->warn("[find_family] unable find family when client cancel family <name=%s,familyid=%u>",
							 p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	if(! p_family->IsFamilyMember(p_client->GetGUID()))
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	if(! p_family->OnCancel(p_client))
	{
		return enFamilyOptResult_Unknow;
	}
	
	return enFamilyRetT_Cancel_Succ;
}


/** 
 * @brief 离开家族
 * 
 * @param p_client 谁离开家族
 * 
 */
enFamilyOptResult MODI_FamilyManager::LeaveFamily(MODI_OnlineClient * p_client)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_client->GetFamilyID());
	if(!p_family)
	{
		Global::logger->warn("[find_family] unable find family when client cancel family <name=%s,familyid=%u>",
							 p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	if(! p_family->IsFamilyMember(p_client->GetGUID()))
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	return p_family->OnLeave(p_client);
}


/** 
 * @brief 拒绝某个人加入
 * 
 * @param p_master 族长
 * @param refuse_id 拒绝的人
 * 
 * @return 成功与失败
 *
 */
enFamilyOptResult  MODI_FamilyManager::RefuseMember(MODI_OnlineClient * p_master, const MODI_GUID refuse_id)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_master->GetFamilyID());
	if(!p_family)
	{
		Global::logger->warn("[find_family] unable find family when client refuse member <name=%s,familyid=%u>",
							 p_master->GetRoleName(), p_master->GetFamilyID());
		return enFamilyOptResult_Unknow;
	}

	if(! p_family->IsFamilyMember(refuse_id))
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	/// 是否有权限
	if(!(p_master->IsFamilyMaster() || p_master->IsFamilySlaver()))
	{
		return enFamilyRetT_Position_Auth;
	}
	
	if(! p_family->OnRefuse(refuse_id))
	{
		return enFamilyOptResult_Unknow;
	}

	return enFamilyPositionRetT_Ok;
}


/** 
 * @brief 开除某人
 * 
 * @param p_master 族长
 * @param refuse_id 被开的人
 * 
 * @return 
 */
bool MODI_FamilyManager::FireMember(MODI_OnlineClient * p_master, const MODI_GUID refuse_id)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_master->GetFamilyID());
	if(!p_family)
	{
		Global::logger->warn("[find_family] unable find family when client fire member <name=%s,familyid=%u>",
							 p_master->GetRoleName(), p_master->GetFamilyID());
		return false;
	}

	if(! p_family->IsFamilyMember(refuse_id))
	{
		MODI_ASSERT(0);
		return false;
	}

	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(! p_family->OnFire(refuse_id))
	{
		return false;
	}
	return true;
}


/** 
 * @brief 接收某个成员加入
 * 
 * @param p_master 族长
 * @param accept_guid 被接受的成员 
 * 
 */
enFamilyOptResult MODI_FamilyManager::AcceptMember(MODI_OnlineClient * p_master, const MODI_GUID & accept_guid)
{
	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p_master->GetFamilyID());
	if(!p_family)
	{
		Global::logger->warn("[find_family] unable find family when client accept member <name=%s,familyid=%u>",
							 p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	/// 是否有权限
	if(!(p_master->IsFamilyMaster() || p_master->IsFamilySlaver()))
	{
		return enFamilyRetT_Position_Auth;
	}
	
	if(! p_family->IsFamilyMember(accept_guid))
	{
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(p_family->IsFull())
	{
		return enFamilyRetT_Position_Num;
	}
	
	if(! p_family->OnAccept(accept_guid))
	{
		return enFamilyOptResult_Unknow;
	}

	return enFamilyPositionRetT_Ok;

}



/** 
 * @brief 增加一个家族
 * 
 * @param p_family 要增加的家族
 */
bool MODI_FamilyManager::AddFamily(MODI_Family * p_family)
{
	std::pair<defFamilyMapIter, bool> ret_code;
	ret_code = m_stFamilyMap.insert(defFamilyMapValue(p_family->GetFamilyID(), p_family));
	if(!ret_code.second)
	{
		Global::logger->error("[add_family] add family to manager failed<id=%u>", p_family->GetFamilyID());
		MODI_ASSERT(0);
	}
	return true;
}


/** 
 * @brief 删除一个家族
 * 
 * @param family_id 要删除的家族id
 *
 */
void MODI_FamilyManager::RemoveFamily(MODI_Family * p_family)
{
	defFamilyMapIter iter = m_stFamilyMap.find(p_family->GetFamilyID());
	if(iter != m_stFamilyMap.end())
	{
		delete iter->second;
		iter->second = NULL;
		m_stFamilyMap.erase(iter);
	}
	else
	{
		Global::logger->error("[remove_family] remove a family from manager failed <id=%u>", p_family->GetFamilyID());
		MODI_ASSERT(0);
	}
}


/** 
 * @brief 查找一个家族
 * 
 */
MODI_Family *  MODI_FamilyManager::FindFamily(const defFamilyID & family_id)
{
	MODI_Family * p_family = NULL;
	defFamilyMapIter iter = m_stFamilyMap.find(family_id);
	if(iter != m_stFamilyMap.end())
	{
		p_family = iter->second;
	}
	return p_family;
}


/** 
 * @brief 所有家族都执行
 * 
 * @param call_back 执行体
 *
 */
// void MODI_FamilyManager::AllFamilyExec(MODI_FamilyCallback & call_back)
// {
	
// }


/** 
 * @brief 有家族成员登陆
 * 
 * @param p_client 
 */
void MODI_FamilyManager::FamilyMemLogin(MODI_OnlineClient * p_client, bool is_notify)
{
	bool is_have_family = false;
	MODI_Family * p_family = NULL;
	MODI_FamilyMember * p_member = NULL;
	defFamilyMapIter iter = m_stFamilyMap.begin();
	for(; iter != m_stFamilyMap.end(); iter++)
	{
		p_family = iter->second;
		if(p_family)
		{
			if(p_family->IsFamilyMember(p_client->GetGUID()))
			{
				is_have_family = true;
				p_member = p_family->FindMember(p_client->GetGUID());
				break;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	
	if(is_have_family)
	{
		p_client->UpDateFamilyInfo(p_member, enLogin_Reason);
	}
	
	if(is_have_family)
	{
		/// 家族有人登陆
		p_member->Login();
		
		p_member->SendMeToFamily(p_client);
		p_member->SendFamilyToMe(enReason_Login);
		if(p_member->IsNormal() && (is_notify == false))
		{
			char tmp[MAX_PACKETSIZE];
			memset(tmp,'\0',sizeof(tmp));
			sprintf(tmp,CONFIG_LOGIN,p_client->GetRoleLinkName());
			/// 家族有人登陆
			p_family->BroadcastSysChatExRequest(tmp,strlen(tmp), p_client);
		}
	}
}



/** 
 * @brief 有家族成员退出登录
 *
 */
void MODI_FamilyManager::FamilyMemLogout(MODI_OnlineClient * p_client)
{
	if(! p_client->IsHaveFamily())
	{
		return;
	}

	MODI_Family * p_family = FindFamily(p_client->GetFamilyID());
	if(! p_family)
	{
		Global::logger->fatal("[logout] on free family failed,not find family <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		return;
	}

	if(p_family)
	{
		MODI_FamilyMember * p_member = p_family->FindMember(p_client->GetGUID());
		if(! p_member)
		{
			return;
		}

		p_member->Logout();
		p_member->SendMeToFamily(p_client);

		if(p_member->IsNormal())
		{
			char tmp[MAX_PACKETSIZE];
			memset(tmp,'\0',sizeof(tmp));
			sprintf(tmp,CONFIG_LOGOUT,p_client->GetRoleLinkName());
			p_family->BroadcastSysChatExRequest(tmp,strlen(tmp), p_client);
		}
	}
}


/** 
 * @brief 通知家族列表给某个人
 * 
 * @param p_client 被通知的客户端
 *
 */
void MODI_FamilyManager::NotifyFamilyList(MODI_OnlineClient * p_client)
{
	if( !p_client )
		return;
	char buf[Skt::MAX_USERDATASIZE];
	memset(buf, 0, sizeof(buf));
	MODI_GS2C_Notify_FamilyList * p_send_cmd = (MODI_GS2C_Notify_FamilyList *)buf;
	AutoConstruct(p_send_cmd);
	MODI_FamilyListInfo * p_info = &(p_send_cmd->m_pListInfo[0]);

	MODI_Family * p_family = NULL;
	defFamilyMapIter iter = m_stFamilyMap.begin();
	for(; iter != m_stFamilyMap.end(); iter++)
	{
		p_family = iter->second;
		
		if( p_family->GetFamilyLevel() < 2 )
			continue;
		p_info->m_dwFamilyID = p_family->GetFamilyID();
		strncpy(p_info->m_szName, p_family->GetFamilyName(), sizeof(p_info->m_szName) - 1);
		p_info->m_wdLevel = p_family->GetFamilyLevel();
		p_info->m_wdMemnum = p_family->GetMemNum();
		p_info->m_dwRenqi = p_family->GetFamilyRenqi();
		p_info->m_dwMoney = p_family->GetMoney();
		
		p_info++;
		p_send_cmd->m_dwSize++;

		if( (char*)p_info - (char*)buf > (int)(sizeof(buf) - sizeof(MODI_FamilyListInfo)) )
			break;
	 }

	p_client->SendPackage(p_send_cmd, sizeof(MODI_GS2C_Notify_FamilyList) + sizeof(MODI_FamilyListInfo) * p_send_cmd->m_dwSize);
	
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[send_family_list] send a family list to client <size=%u,name=%s>", p_send_cmd->m_dwSize, p_client->GetRoleName());
#endif
	
}



/** 
 * @brief 家族名字检查
 * 
 */
bool MODI_FamilyManager::CheckFamilyName(const char * check_name)
{
	MODI_Family * p_family = NULL;
	defFamilyMapIter iter = m_stFamilyMap.begin();
	for(; iter != m_stFamilyMap.end(); iter++)
	{
		p_family = iter->second;
		if(strcmp(p_family->GetFamilyName(), check_name) == 0)
		{
			return false;
		}
	}
	return true;
}


/** 
 * @brief 访问家族信息
 * 
 * @return 
 */
enFamilyOptResult  MODI_FamilyManager::VisitFamily(MODI_OnlineClient * p_client, const defFamilyID & visit_id)
{
	MODI_Family * p_family = FindFamily(visit_id);
	if(! p_family)
	{
		Global::logger->fatal("[visit_family] visit family failed,not find family <client=%s,familyid=%u>",
							  p_client->GetRoleName(), p_client->GetFamilyID());
		MODI_ASSERT(0);
		p_client->RetFamilyResult(enVisitFamily_NoExit);
	}

	return p_family->OnVisit(p_client);
}



/** 
 * @brief 更换家族
 * 
 * @param p_master 族长 
 * @param change_id 新族长
 * 
 */
enFamilyOptResult MODI_FamilyManager::ChangeMaster(MODI_OnlineClient * p_master, const MODI_GUID & change_id)
{
	MODI_Family * p_family = FindFamily(p_master->GetFamilyID());
	if(! p_family)
	{
		Global::logger->warn("[change_master] change master family failed,not find family <client=%s,familyid=%u>",
							  p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		Global::logger->warn("[change_master] change master family,id not equ <mid=%u,id=%u>", (QWORD)(p_family->GetMasterID()),(QWORD)(p_master->GetGUID()) );
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	enFamilyOptResult result = p_family->OnChangeMaster(p_master, change_id);

	return result;
}



/** 
 * @brief 设置副族长
 * 
 * @param p_master 族长
 * @param set_id 被设置的副族长
 * 
 */
enFamilyOptResult MODI_FamilyManager::SetSlaver(MODI_OnlineClient * p_master, const MODI_GUID & set_id)
{
	MODI_Family * p_family = FindFamily(p_master->GetFamilyID());
	if(! p_family)
	{
		Global::logger->warn("[change_master] change master family failed,not find family <client=%s,familyid=%u>",
							  p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		Global::logger->warn("[change_master] change master family,id not equ <mid=%u,id=%u>", (QWORD)(p_family->GetMasterID()),(QWORD)(p_master->GetGUID()) );
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}

	return p_family->OnSetSlaver(p_master, set_id);
}


/** 
 * @brief 罢免副族长
 * 
 */
enFamilyOptResult MODI_FamilyManager::SetNormal(MODI_OnlineClient * p_master, const MODI_GUID & normal_id)
{
	MODI_Family * p_family = FindFamily(p_master->GetFamilyID());
	if(! p_family)
	{
		Global::logger->warn("[change_master] change master family failed,not find family <client=%s,familyid=%u>",
							  p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return enFamilyOptResult_Unknow;
	}
	
	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		Global::logger->warn("[change_master] change master family,id not equ <mid=%u,id=%u>", (QWORD)(p_family->GetMasterID()),(QWORD)(p_master->GetGUID()) );
		MODI_ASSERT(0);
		return enFamilyOptResult_None;
	}

	return p_family->OnSetNormal(p_master, normal_id);
}



/** 
 * @brief 修改公告
 * 
 */
void MODI_FamilyManager::ModiPublic(MODI_OnlineClient * p_master, const char * public_str)
{
	MODI_Family * p_family = FindFamily(p_master->GetFamilyID());
	if(! p_family)
	{
		Global::logger->warn("[modi_public] change master family failed,not find family <client=%s,familyid=%u>",
							  p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return;
	}
	
	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		Global::logger->warn("[modi_public] change master family,id not equ <mid=%u,id=%u>", (QWORD)(p_family->GetMasterID()),(QWORD)(p_master->GetGUID()) );
		MODI_ASSERT(0);
		return;
	}

	p_family->OnModiPublic(p_master, public_str);
}


/** 
 * @brief 修改宣言
 * 
 */
void MODI_FamilyManager::ModiXuanyan(MODI_OnlineClient * p_master, const char * xuanyan)
{
	MODI_Family * p_family = FindFamily(p_master->GetFamilyID());
	if(! p_family)
	{
		Global::logger->warn("[modi_public] change master family failed,not find family <client=%s,familyid=%u>",
							  p_master->GetRoleName(), p_master->GetFamilyID());
		MODI_ASSERT(0);
		return;
	}
	
	if(p_family->GetMasterID() != p_master->GetGUID())
	{
		Global::logger->warn("[modi_public] change master family,id not equ <mid=%u,id=%u>", (QWORD)(p_family->GetMasterID()),(QWORD)(p_master->GetGUID()) );
		MODI_ASSERT(0);
		return;
	}

	p_family->OnModiXuanyan(p_master, xuanyan);
}



/** 
 * @brief 是否是某家族成员
 * 
 * @param name 角色名
 * 
 */
MODI_FamilyMember * MODI_FamilyManager::FindFamilyMember(const char * name)
{
	MODI_Family * p_family = NULL;
	MODI_FamilyMember * p_member = NULL;
	defFamilyMapIter iter = m_stFamilyMap.begin();
	for(; iter != m_stFamilyMap.end(); iter++)
	{
		p_family = iter->second;
		if(p_family)
		{
			p_member = p_family->FindFamilyMember(name);
			if(p_member)
			{
				return p_member;
			}
		}
		else
		{
			MODI_ASSERT(0);
		}
	}
	return p_member;
}
