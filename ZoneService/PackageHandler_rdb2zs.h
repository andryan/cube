/** 
 * @file PackageHandler_rdb2zs.h
 * @brief DBPROXY和ZS的数据包处理类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef PACKAGE_HANDLER_H_RDB2ZS_
#define PACKAGE_HANDLER_H_RDB2ZS_

#include "protocol/gamedefine.h"
#include "IPackageHandler.h"
#include "SingleObject.h"



class MODI_PackageHandler_rdb2zs : public MODI_IPackageHandler , 
	public CSingleObject<MODI_PackageHandler_rdb2zs>
{
public:

    MODI_PackageHandler_rdb2zs();
    ~MODI_PackageHandler_rdb2zs();

	static int Register_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int RoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadCharData_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadCharlist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadMaillist_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int DirectionToGameServer( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadRankResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int LoadSelfRankResult_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int SongMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int ZoneUserVar_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	
	static int FamilyLevelResult( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int DecMoney_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static 	int RelationList_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static 	int SendMailByCondition_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );


private:

    virtual bool FillPackageHandlerTable();

};

#endif

