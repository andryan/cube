#include "World.h"
#include "GameSvrClientTask.h"
#include "s2unify_cmd.h"
#include "ManangerClient.h"
#include "AccUserManager.h"
#include "s2rdb_cmd.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"
#include "GameConstant.h"
#include "FamilyManager.h"

bool 	MODI_World::OnClientLogin( MODI_OnlineClient * pClient )
{
	// 必须先填充游戏服务器ID和服务器名字
	MODI_ASSERT( pClient->GetServerID() );
	MODI_ASSERT( strlen( pClient->GetServerName() )  > 0 );

	MODI_OnlineClientMgr * pMgr1 = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OfflineUserMgr * pMgr2 = MODI_OfflineUserMgr::GetInstancePtr();
	if( pMgr1 )
	{
		pMgr1->OnClientLogin( pClient , true , true );
	}

	if( pMgr2 )
	{
		pMgr2->OnClientLogin( pClient );
	}

	MODI_GameSvrClientTask * cur_server = MODI_GameSvrClientTask::FindServer( pClient->GetServerID() );
	if( cur_server )
	{
		cur_server->IncClientNum();
	}

	if(MODI_GameConstant::get_OneLogin())
	{
#ifdef _HRX_DEBUG
		Global::logger->debug("[one_login] set one login flag");
#endif		
// 		//压力测试加钱
// 		MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
// 		if( !pDB )
// 		{
// 			Global::logger->info("[%s] save faild. can't get db interface." , SVR_TEST );
// 		}
// 		else
// 		{
// 			MODI_S2RDB_Request_ClearZero send_cmd;
// 			send_cmd.m_byState = 2;
// 			send_cmd.m_dwAccountID = pClient->GetAccid();
// 			pDB->SendCmd(&send_cmd, sizeof(send_cmd));
// 		}
	}
	else
	{
#ifdef _HRX_DEBUG
		Global::logger->debug("[one_login] clear on login falg");
#endif
		;
	}
			
	if(MODI_AccUserManager::GetInstance().IsChenMi(pClient->GetAccid()))
	{
		//// 通知防沉迷服务器上线
		MODI_S2Unify_Onlogin_Cmd send_cmd;
		send_cmd.m_dwAccID = pClient->GetAccid();
		MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
		if( pMc )
		{
#ifdef _HRX_DEBUG
			Global::logger->debug("[chenmi_login] a chenmi user login <accid=%u>", pClient->GetAccid());
#endif			
			pMc->SendCmd(&send_cmd, sizeof(send_cmd));
		}
	}

	return true;
}

bool 	MODI_World::OnClientLoginout( MODI_OnlineClient * pClient )
{
	MODI_OnlineClientMgr * pMgr1 = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OfflineUserMgr * pMgr2 = MODI_OfflineUserMgr::GetInstancePtr();
	if( pMgr1 )
	{
		pMgr1->OnClientLoginout( pClient );
	}

	if( pMgr2 )
	{
		pMgr2->OnClientLoginout( pClient );
	}

	MODI_FamilyManager::GetInstance().FamilyMemLogout(pClient);

	MODI_GameSvrClientTask * cur_server = MODI_GameSvrClientTask::FindServer( pClient->GetServerID() );
	if( cur_server )
	{
		cur_server->DecClientNum();
	}

	if(MODI_AccUserManager::GetInstance().IsChenMi(pClient->GetAccid()))
	{
		//// 通知防沉迷服务器下线
		MODI_S2Unify_Onlogout_Cmd send_cmd;
		send_cmd.m_dwAccID = pClient->GetAccid();
		MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
		if( pMc )
		{
#ifdef _HRX_DEBUG
			Global::logger->debug("[chenmi_login] a chenmi user logout <accid=%u>", pClient->GetAccid());
#endif			
			pMc->SendCmd(&send_cmd, sizeof(send_cmd));
		}
	}
	// 新添加的 删除联系人列表
#ifdef _XXP_DEBUG
#endif
	pClient->GetRelationList().CleanUp();	
	MODI_OnlineClientPool::Delete( pClient );
	
	return true;
}

void 	MODI_World::OnServerDisconnection( BYTE byServer )
{
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = pMgr->m_mgr.GetGUIDMapContent().begin();
	while( itor != pMgr->m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient->GetServerID() == byServer )
		{
			this->OnClientLoginout( pOnlineClient );
		}

		itor = inext;
	}
}
