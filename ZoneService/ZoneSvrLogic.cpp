#include "ZoneSvrLogic.h"
#include "ZoneService.h"
#include "AssertEx.h"
#include "GameSvrClientTask.h"
#include "OnlineClientMgr.h"
#include "ManangerClient.h"
#include "s2rdb_cmd.h"
#include "DBProxyClientTask.h"
#include "TimeManager.h"
#include "MailCore.h"
#include "FunctionTime.h"
#include "BillClient.h"
#include "Base/FPSControl.h"
#include "TodayOnlineTimeClean.h"
#include "RankCore.h"
#include "ZoneServerAPP.h"
#include "FamilyManager.h"
#include "InviteManager.h"
#include "SingleMusicMgr.h"


MODI_ZoneSvrLogic::MODI_ZoneSvrLogic():m_timerSyncCl(5 * 1000),m_timerItemSerialSave( 3 * 60 * 1000 )
{

}


MODI_ZoneSvrLogic::~MODI_ZoneSvrLogic()
{

}

/**
 * @brief 主循环
 *
 */
void MODI_ZoneSvrLogic::Run()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	ptm->Init();
	MODI_TodayOnlineTimeClean * ptotc = MODI_TodayOnlineTimeClean::GetInstancePtr();
	ptotc->Initial();
	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();

	Global::m_stLogicRTime.GetNow();
	unsigned long nBegin = GetTickCount( true );
	unsigned long one_min_delay = 0;
	unsigned long one_sec_delay = 0;

	MODI_FPSControl fps;
	bool bl_is_run_zero = true;
	
	fps.Initial( 30 );
	
    while (!IsTTerminate())
    {
        /// 获取当前时间
        Global::m_stLogicRTime.GetNow();
		m_stTTime = Global::m_stLogicRTime;
		ptm->GetANSITime();

		//MODI_FunctionTime functiontime(50 * 1000, "zoneservice");//100ms
		
		fps.Begin();

		if( MODI_ManangerSvrClient::GetInstancePtr() )
		{
			const unsigned int nMaxCmdMS = 1000;
			MODI_ManangerSvrClient::GetInstancePtr()->Get(nMaxCmdMS );
		}

		// 处理来自RDB的数据包
		MODI_DBProxyClientTask::ProcessAllPackage();

		// 处理来自其他服务器的数据包
		MODI_GameSvrClientTask::ProcessAllPackage();

		if( m_timerSyncCl( Global::m_stLogicRTime ) )
		{
			MODI_GameSvrClientTask::SyncChannelListToLoginServer();
		}

		//unsigned long nTime = Global::m_stLogicRTime.GetMSec() - nBegin;
		MODI_OnlineClientMgr::GetInstancePtr()->Update( Global::m_stLogicRTime, m_stTTime );
		//MODI_MailCore::Update( nTime );
		rank_core->Update();

		nBegin = Global::m_stLogicRTime.GetMSec();

		if(nBegin > one_sec_delay)
		{
			one_sec_delay = nBegin + 1000;
			MODI_InviteManager::GetInstance().UpDate(Global::m_stLogicRTime);
			MODI_SingMusicMgr::GetInstance().Update();
		}

		if(nBegin > one_min_delay)
		{
			one_min_delay = nBegin + 60000;
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			const unsigned int zone_player_num = pMgr->Size();
			Global::logger->info("[zone_info] this zone have players(%u)", zone_player_num);
			Global::logger->info("[zone_online] this zone have players <online=%u>", zone_player_num);

			/// 1分钟更新一次家族列表
			MODI_FamilyManager::GetInstance().NotifyFamilyList();
		}

		/// deal billclient command
		const DWORD GET_BILLCMD_COUNT = 1000;
		MODI_BillClient::GetInstancePtr()->Get(GET_BILLCMD_COUNT);
		
		/// 每天0点要做的事情
		if(m_stTTime.GetHour() == 0 && bl_is_run_zero)
		{
			bl_is_run_zero = false;
			/// send a command to dbproxy 清理标志
			MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
			if( !pDB )
			{
				Global::logger->info("[%s] save faild. can't get db interface." , SVR_TEST );
			}
			else
			{
				MODI_S2RDB_Request_ClearZero send_cmd;
				send_cmd.m_byState = 1;
				pDB->SendCmd(&send_cmd, sizeof(send_cmd));
			}

			/// 今天在线时间清0
			MODI_TodayOnlineTimeClean::GetInstancePtr()->OnCleanUPOnlineTime();
		}
		if(m_stTTime.GetHour() > 0 )
		{
			bl_is_run_zero = true;
		}
		
		fps.End();
    }

    Final();
}

void MODI_ZoneSvrLogic::Final()
{
	if( MODI_ZoneService::GetInstancePtr() )
	{
		// 逻辑线程终止，则服务器终止
		MODI_ZoneService::GetInstancePtr()->Terminate();
		Global::logger->info("[%s] Zone Server Logic Shutdown." , SVR_TEST );
	}
}
