/** 
 * @file DBProxyClientTask.h
 * @brief 连接ZoneServer的DB代理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-24
 */
#ifndef DBPROXYCLIENT_ONZS_TASK_H_
#define DBPROXYCLIENT_ONZS_TASK_H_

#include "protocol/gamedefine.h"
#include "ServiceTask.h"
#include "RecurisveMutex.h"
#include "CommandQueue.h"
#include <set>
#include "SvrPackageProcessBase.h"
#include "ServerConfig.h"


class  MODI_DBProxyClientTask : public MODI_IServerClientTask
{
	public:

		MODI_DBProxyClientTask(const int sock, const struct sockaddr_in * addr);
		virtual ~MODI_DBProxyClientTask();

	public:

		virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

		virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

		virtual int RecycleConn();
		virtual void DisConnect();

		// 连接上的回调,可以安全的在逻辑线程调用
		virtual void OnConnection(); 

		// 断开的回调，可以安全的在逻辑线程调用
		virtual void OnDisconnection(); 

		void 	SetDBProxyType( DBProxyType type ) { m_Type = type; }
		DBProxyType GetDBProxyType() const { return m_Type; }

	public:


		/** 
		 * @brief 处理所有客户端的数据包
		 */
		static void 	ProcessAllPackage();

		static MODI_SvrPackageProcessBase 	ms_ProcessBase;

		static bool 	RegServer( BYTE byServer ,  MODI_DBProxyClientTask * p );

		static MODI_DBProxyClientTask * FindServer(BYTE byServer);

		static void 	Init();


	private:
		
		DBProxyType 	m_Type;
		static  MODI_DBProxyClientTask * 	ms_pDBProxy[255];
};


#endif
