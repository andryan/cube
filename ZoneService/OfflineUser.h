/** 
 * @file OfflineUser.h
 * @brief 离线用户
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-22
 */

#ifndef MODI_OFFLINEUSER_H_
#define MODI_OFFLINEUSER_H_

#include "session_id.h"
#include "gamestructdef.h"
#include "protocol/gamedefine.h"
#include "Timer.h"
class MODI_OnlineClient;

class 	MODI_OfflineUser
{
	public:

		MODI_OfflineUser();
		virtual ~MODI_OfflineUser();


	public:

		// 初始化离线用户
		void 	Init(MODI_OnlineClient * p_client);
		void 	InitNameCard( MODI_CHARID charid , const defAccountID acc_id, const MODI_NameCard & card  );
		
		/// 初始化家族名片
		void InitFamilyCard(MODI_FamilyCard & card);
		MODI_FamilyCard & GetFamilyCard()
		{
			return m_namecard.m_stFamilyCard;
		}

		MODI_CHARID GetCharID() const { return m_charID; }
		const char * GetName() const { return m_namecard.m_szName; }
		const char * GetAccName() const { return m_namecard.m_szAccName; }
		const defAccountID GetAccId() const {return m_dwAccId; }
		BYTE 	GetSex() const { return m_namecard.m_bySex; }
		BYTE 	GetLevel() const { return m_namecard.m_byLevel; }
		BYTE 	GetAge() const { return m_namecard.m_RoleDetail.m_byAge; }
		DWORD 	GetCity() const { return m_namecard.m_RoleDetail.m_nCity; }
		DWORD 	GetClan() const { return m_namecard.m_RoleDetail.m_nClan; }
		DWORD 	GetGroup() const { return m_namecard.m_RoleDetail.m_nGroup; }
		DWORD 	GetQQ() const { return m_namecard.m_RoleDetail.m_nQQ; }
		BYTE 	GetBlood() const { return m_namecard.m_RoleDetail.m_byBlood; }
		DWORD 	GetBirthDay() const { return m_namecard.m_RoleDetail.m_nBirthDay; }
		WORD 	GetDefaultAvatar() const { return m_namecard.m_nDefaultAvatarIdx; }
		WORD 	GetChenghao() const { return m_namecard.m_wdChenghao; }
		const char * GetPerSign() const { return m_namecard.m_RoleDetail.m_szPersonalSign; }
		const MODI_SocialRelationData & GetSocialRelData() const { return m_namecard.m_RoleDetail.m_SocialRelData; }
		const MODI_ClientAvatarData & GetAvatarData() const { return m_namecard.m_avatar; }	
		const MODI_RoleInfo_Detail & GetDetailData() const { return m_namecard.m_RoleDetail; }

	private:

		// 离线用户需要保存的数据

		MODI_CHARID m_charID;
		defAccountID m_dwAccId;
		MODI_NameCard 	m_namecard;
};

#endif
