/** 
 * @file TodayOnlineTimeClean.h
 * @brief 清理当天在线时间
 * @author Tang Teng
 * @version v0.1
 * @date 2010-11-23
 */


#ifndef MODI_TODAYONLINETIMECLEAN_H_
#define MODI_TODAYONLINETIMECLEAN_H_


#include "Base/TimeManager.h"
#include "Base/SingleObject.h"

class 	MODI_TodayOnlineTimeClean : public CSingleObject<MODI_TodayOnlineTimeClean>
{
	public:
		MODI_TodayOnlineTimeClean();
		~MODI_TodayOnlineTimeClean();


	public:

		void 	Initial();

		void 	Update();

		void 	CleanUP();

		//	private:

		void 	OnCleanUPOnlineTime();

	private:

		DWORD 	m_start;
};


#endif

