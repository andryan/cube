/**
 * @file   AccUserManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 18:15:44 2011
 * 
 * @brief  账号用户管理
 * 
 * 
 */

#ifndef _MD_ACCUSERMANAGER_H
#define _MD_ACCUSERMANAGER_H

#include "AssertEx.h"
#include "Global.h"
#include "AccUser.h"
#include "protocol/gamedefine.h"

/**
 * @brief zoneservice的accuser manager
 * 
 */
class MODI_AccUserManager
{
 public:
	typedef std::map<defAccountID, MODI_AccUser> defAccUserMap;
	typedef std::map<defAccountID, MODI_AccUser>::value_type  defAccUserMapValue;
	typedef std::map<defAccountID, MODI_AccUser>::iterator defAccUserMapIter;

	static MODI_AccUserManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_AccUserManager;
		}
		return *m_pInstance;
	}

	bool IsChenMi(const defAccountID & acc_id)
	{
		defAccUserMapIter iter = m_AccUserMap.find(acc_id);
		if(iter == m_AccUserMap.end())
		{
			MODI_ASSERT(0);
			return false;
		}
		return iter->second.IsChenMi();
	}

	/** 
	 * @brief 注册一个用户
	 * 
	 * @param acc_id 用户id
	 */
	void AddAccUser(const defAccountID & acc_id, bool chen_mi);

	/** 
	 * @brief 删除一个用户
	 * 
	 * @param acc_id 用户id
	 */
	void RemoveAccUser(const defAccountID & acc_id);
	
 private:
	static MODI_AccUserManager * m_pInstance;
	
	/// 用户管理
	defAccUserMap m_AccUserMap;
};

#endif
