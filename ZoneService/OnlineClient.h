/** 
 * @file OnlineClient.h
 * @brief 在线客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-17
 */

#ifndef MODI_ONLINE_CLIENT_H_
#define MODI_ONLINE_CLIENT_H_

#include "session_id.h"
#include "gamestructdef.h"
#include "RelationList.h"
#include "Timer.h"
#include "FamilyManager.h"
#include "protocol/c2gs_family.h"
#include "s2zs_cmd.h"
#include "ZoneUserVar.h"
#include "MailManager.h"

struct MODI_ReverseRelationCallBack
{
	virtual ~MODI_ReverseRelationCallBack(){}
	virtual bool Exec(MODI_CHARID guid) = 0;
};

class 	MODI_Maillist;

class 	MODI_OnlineClient
{
	enum enState
	{
		enWaitDataFromDB, // 等待从数据库中获取数据
		enInWorldPlaying,// 在游戏世界中
		enWaitRechangeChannel, // 处于切换频道状态
		enWaitReEnterChannel, // 等待重新进入频道
		enWaitAnotherLogin, // 等待另外一个客户端登入
	};

	public:

		MODI_OnlineClient();
		~MODI_OnlineClient();

	public:

		void Init( defAccountID accid , const MODI_GUID & guid , const char * szName );

		void SetAccid( defAccountID accid );

		void SetGUID( const MODI_GUID & guid );

		void SetSessionID( const MODI_SessionID & sessionid );

		void SetServerID( BYTE byServerID );

		void SetNewServerID( BYTE byNewServerID );

		void SetServerName( const char * szServerName );

		void SetGameState( kGameState st );

		void Update(const MODI_RTime &  cur_rtime, const MODI_TTime &  cur_ttime);

		const MODI_GUID & GetGUID() const;

		MODI_CHARID  GetCharID() const;

		const MODI_SessionID & GetSessionID() const;

		const char * GetRoleName() const;

		const char * GetRoleLinkName();

		const char * GetServerName() const;

		BYTE 	GetServerID() const;

		BYTE 	GetNewServerID() const;

		BYTE 	GetSexType() const;

		/** 
		 * @brief 某个客户端登入时是否需要通知
		 * 
		 * @param guid 某个客户端
		 * 
		 * @return 	如需要返回true
		 */
		bool 	IsNeedNotifyWithSbLogin( MODI_CHARID guid );

		/** 
		 * @brief 某个客户端登出时是否需要通知
		 * 
		 * @param guid 某个客户端
		 * 
		 * @return 	如果需要返回true
		 */
		bool 	IsNeedNotifyWithSbLoginOut( MODI_CHARID guid );

		/** 
		 * @brief 自己的某个联系人上线时调用
		 * 
		 * @param pRel 自己的联系人
		 */
		void 	OnRelationLogin( MODI_OnlineClient * pRel );

		/** 
		 * @brief 自己的某个联系人下线时调用
		 * 
		 * @param pRel 自己的联系人
		 */
		void 	OnRelationLoginOut( MODI_OnlineClient * pRel );

		void 	OnRelationGameStateChange( MODI_OnlineClient * pRel );

		MODI_RelationList 	& 	GetRelationList() { return m_relations; }

		const 	MODI_CharactarFullData & GetFullData() const { return m_FullData; }
		MODI_CharactarFullData & GetFullDataModify(){ return m_FullData; }

		
		void SetGmLevel(const BYTE gm_level);

		void AddReverseRelation(MODI_CHARID guid);
		void RemoveReverseRelation(MODI_CHARID  guid);
		void AllReverseExec(MODI_ReverseRelationCallBack & call_back);
		
		
		/** 
		 * @brief 获取被拉黑的次数
		 * 
		 * @return 被拉黑的次数
		 *
		 */
		const WORD GetBlockCount() const 
		{
			return m_FullData.m_roleExtraData.m_wdBlockCount;
		}

		/** 
		 * @brief 保存拉黑的次数
		 * 
		 */
		void SetBlockCount(const WORD block_count)
		{
			m_FullData.m_roleExtraData.m_wdBlockCount = block_count;
		}

		const DWORD GetRenqi()
		{
			return m_FullData.m_roleInfo.m_detailInfo.m_SocialRelData.m_nRenqi;
		}

		void 	OnBeAddIdolBySB( BYTE bySex );

		void 	OnBeDelIdolBySB( BYTE bySex );
		
		void	BlockOptDeal(const enRelOptType by_type);

		void 	ChangeChenghao(BYTE by_flag = 0);

		void 	NotifyChangeChenghao(const WORD chenghao_id,  bool is_notify_zone, bool is_notify_client);

		void	CheckChanghaoTime(DWORD chenghao_id);
		
		const 	WORD GetChenghao();

		bool 	IsAddIdolTimeOver() const;

		time_t 	GetLastIdolTime() const;

		void 	SetLastIdolTime( time_t t );

		void 	OnLoginIn();

		void 	OnLoginGameService();

		void 	OnLoginOut();

		void 	OnEnterChannel();

		void 	OnLeaveChannel();

		enRelationRetType 	IsCanMarryMe( MODI_OnlineClient * pClient );

		enRelationRetType 	IsCanDoMyGF( MODI_OnlineClient * pClient );

		void 	OnBeginReqCanDoMyHoney( MODI_CHARID charid , bool bLover );

		bool 	IsMyReqHoney( MODI_CHARID charid );

		bool 	IsReqingHoney() const;

		bool 	IsReqMarry() const;

		void 	OnEndReqCanDoMyHoney();

		void 	SetLoginKey( const MODI_LoginKey & key )
		{
			m_loginKey = key;
		}

		const MODI_LoginKey & GetLoginKey() const
		{
			return m_loginKey;
		}

		defAccountID GetAccid() const { return m_nAccid; }
		defAccountID GetAccountID() const { return m_nAccid; }


		/** 
		 * @brief 获取账号名字
		 * 
		 * 
		 * @return 账号名字
		 *
		 */
		const char * GetAccName()
		{
			return m_strAccName.c_str();
		}

		/** 
		 * @brief 设置账号名字
		 * 
		 * @param acc_name 账号名字
		 *
		 */
		void SetAccName(const char * acc_name)
		{
			if(acc_name)
			{
				m_strAccName = acc_name;
				strncpy(m_FullData.m_roleExtraData.m_cstrAccName, m_strAccName.c_str(), sizeof(m_FullData.m_roleExtraData.m_cstrAccName));
			}
		}

		size_t 	GetNameLen() const;

		//void 	OnLoadMails( MODI_Maillist * pMails );

		//MODI_Maillist * GetMaillist();

		void 	SetSelfRank( enRankType type , DWORD rank );


		void 	LoadSelfRank();

	public:

		// db operator 

		void 	OnLoadFromDB( const MODI_CharactarFullData & fullData );

		void 	BinLoadFromDB(const char * p_bin_data, const DWORD data_size);
		
		void 	SaveToDB();


		/** 
		 * @brief 请求执行sql
		 * 
		 * @param sql sql语句
		 * @param sql_len 语句的长度
		 *
		 */
		void ExecSqlToDB(const char * sql, const WORD sql_len);
		

		/** 
		 * @brief 获取点券
		 * 
		 */
		const DWORD GetMoneyRmb() const
		{
			return m_FullData.m_roleInfo.m_detailInfo.m_nRMBMoney;
		}
		

		/** 
		 * @brief 保存点券
		 * 
		 */
		void SaveMoneyRmb();

		void 	UpdateLastLoginInfo();

	public:

		// 于游戏客户端的同步相关
		bool 	SendPackage(const void * pt_null_cmd, int cmd_size);

		bool 	SendSystemChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type = enSystemNotice_Default);

		void 	SyncRelationList();

		void 	SyncFensiRenqi();

		void 	SyncSelfRank();

		void	AddRenqi( WORD	n);

	public:

		void 	ReqFullDataFromGameServer();

		void 	OnRecvFullDataFromGameServer( const MODI_CharactarFullData & fullData );

		void 	ResetTempData();

		void 	OnRecvUpdateData( MODI_UpdateDataRead & r , const MODI_ObjUpdateMask & upm );

	public:

		const char * GetStringFromatState() const;

		// 状态相关
		void 	SwitchToWaitDataFromDB() 
		{
			m_st = enWaitDataFromDB;
		}

		bool 	IsInWaitDataFromDBState() const
		{
			return m_st == enWaitDataFromDB;
		}

		void 	SwitchToInWorldPlayingState()
		{
			m_st = enInWorldPlaying;
		}

		bool 	IsInWorldPlayingState() const
		{
			return m_st == enInWorldPlaying; 
		}

		void 	SwitchToWaitRechangeChannel() 
		{
			m_st = enWaitRechangeChannel;
		}

		bool 	IsInWaitRechangeChannelState() const
		{
			return m_st == enWaitRechangeChannel;
		}

		void 	SwtichToWaitAnotherLoginState();

		bool 	IsInWaitAnotherLoginState() const
		{
			return m_st == enWaitAnotherLogin;
		}

		void 	SwitchToWaitReEnterChannelState();

		bool 	IsInWaitReEnterChannelState() const
		{
			return m_st == enWaitReEnterChannel;
		}

		/*
		void 	SetLoadingMails( bool bSet )
		{
			m_bLoadingMails = bSet;
		}

		bool 	IsLoadingMails() const 
		{
			return m_bLoadingMails;
			}*/

		bool InitMail(const MODI_DBMaillist & dbmails);

		bool MailIsFull();

		bool AddUserMail(MODI_Mail * pMail);
		bool AddServerMail(MODI_Mail * pMail);
		MODI_Mail * GetUserMail(const MODI_GUID & id);
		bool DelUserMail(const MODI_GUID & id);

		size_t 	GetPendingMailCount() const { return m_nPendingMailCount; }
		void 	SetPendingMailCount( size_t n) { m_nPendingMailCount += n; }

		/** 
		 * @brief 给用户加金币
		 * 
		 */
		void IncMoney(const DWORD money, MODI_ZS2S_Notify_OptMoney::enOptReason reason = MODI_ZS2S_Notify_OptMoney::enReasonBegin);


		/** 
		 * @brief 给用户减金币
		 * 
		 */
		void DecMoney(const DWORD money, MODI_ZS2S_Notify_OptMoney::enOptReason reason = MODI_ZS2S_Notify_OptMoney::enReasonBegin);
		

		/// 用户家族信息
		MODI_FamilyMemBaseInfo m_stCFamilyInfo;

		/** 
		 * @brief 是否有家族
		 * 
		 */
		bool IsHaveFamily();

		/** 
		 * @brief 是不是族长
		 * 
		 */
		bool IsFamilyMaster();

		/** 
		 * @brief 是不是福族长
		 * 
		 */
		bool IsFamilySlaver();

		/** 
		 * @brief 是不是申请成员
		 * 
		 */
		bool IsFamilyRequest();


		/** 
		 * @brief 是不是普通成员
		 * 
		 */
		bool IsFamilyNormal();

		/** 
		 * @brief 获取家族名字
		 * 
		 */
		const char * GetFamilyName();

		/** 
		 * @brief 获取家族id
		 * 
		 */
		const DWORD GetFamilyID();


		/** 
		 * @brief 更新个人家族信息
		 *
		 * @param reason 是否需要把名片信息更新到客户端
		 *
		 */
		void UpDateFamilyInfo(MODI_FamilyMember * p_member, enUpDateCFReason reason);
		

		/** 
		 * @brief 返回家族结果
		 * 
		 * @param result 返回的结果
		 *
		 */
		void RetFamilyResult(enFamilyOptResult result)
		{
			MODI_GS2C_Return_FamilyOpt_Result send_cmd;
			send_cmd.m_enResult = result;
			SendPackage(&send_cmd, sizeof(send_cmd));
		}

		/** 
		 * @brief  更新家族成员人气
		 * 
		 */
		void ChangeFamilyMemRenqi();


		/** 
		 * @brief 创建变量
		 * 
		 * @param var_name 变量名
		 * @param var_attribute 变量属性
		 * 
		 * @return 成功true,失败false
		 *
		 */
		bool CreateUserVar(const char * var_name, const char * var_attribute);

		/// 用户身上的变量
		MODI_ZoneUserVarMgr m_stZoneUserVarMgr;
		
	private:

		/// 联系人
		MODI_RelationList 	m_relations;

		/// 反向联系人
		std::set<MODI_CHARID > m_ReverseRelationSet;

		/// 所在服务器
		BYTE 		m_byServerID;

		/// 将要转移的服务器
		BYTE 		m_byNewServerID;

		/// 所在服务器名字
		char 		m_szServerName[MAX_SERVERNAME_LEN + 1]; 

		/// 游戏状态
		kGameState  m_byGameState;

		/// 角色所有数据
		MODI_CharactarFullData 	m_FullData;

		MODI_SessionID m_sessionID;

		MODI_GUID 	m_guid;

		MODI_Timer 	m_timerDBSave;

		MODI_Timer 	m_ReqLoveTimer;

		MODI_Timer 	m_timerNotOnServer;

		MODI_Timer 	m_timerUpRelGameState;

		MODI_CHARID m_nReqLoveID;

		bool 		m_bReqLover;

		enState 	m_st;

		MODI_LoginKey m_loginKey;

		defAccountID 	m_nAccid;

		size_t 		m_nNameLen;
		
		MODI_Maillist * m_pMaillist;

		bool 		m_bLoadingMails;

//		bool 		m_bMailTest;

		size_t 		m_nPendingMailCount;

		DWORD 		self_ranks[kRank_End - kRank_Begin + 1];

		DWORD 		self_ranks_version;
		
		/// 账号名字
		std::string m_strAccName;

		/// 邮件管理
		MODI_MailManager m_stUserMailsMgr;
		MODI_MailManager m_stSvrMailsMgr;
};

/** 
 * @brief 联系人结构池
 */
class 	MODI_OnlineClientPool
{
	public:

		static MODI_OnlineClient * 	New();
		static void 			Delete( MODI_OnlineClient * p );

};

class 	MODI_KickClientOp
{
	private:
		MODI_KickClientOp() {}

	public:

		static bool Kick( MODI_OnlineClient * pClient , bool bCmdToGS , BYTE byServer , enKickReason Reason );

};

#endif
