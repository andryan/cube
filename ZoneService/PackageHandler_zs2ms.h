/** 
 * @file PackageHandler_zs2ms.h
 * @brief ms和zs 数据包处理类
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef PACKAGE_HANDLER_H_ZS2MS_
#define PACKAGE_HANDLER_H_ZS2MS_

#include "protocol/gamedefine.h"
#include "IPackageHandler.h"
#include "SingleObject.h"


class MODI_PackageHandler_zs2ms : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_zs2ms>
{
public:

    MODI_PackageHandler_zs2ms();
    ~MODI_PackageHandler_zs2ms();

	static int ClientLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int DirectionToGameServer( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static void  SendCharlist( defAccountID accid , const MODI_SessionID & sessionid , 
			BYTE byCharNum , const MODI_CharInfo * pCharinfo , BYTE byResult );


	static int QueryChannelStatus( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int GMOperateUser_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int GMSystemChat_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int GMSendMail_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	static int CallGMSendMail_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );


	static int SetChannelNum_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );
private:

//	static int 	TryLoadCharacterDataFromDB();
	static void  TryLoadCharacterListFromDB( defAccountID accid , const MODI_SessionID & sessionid );

private:

    virtual bool FillPackageHandlerTable();

};

#endif

