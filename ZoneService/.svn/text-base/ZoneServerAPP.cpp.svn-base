#include "ZoneServerAPP.h"
#include "ZoneService.h"
#include "ZoneSvrLogic.h"
#include "s2ms_cmd.h"
#include "DBProxyClientTask.h"
#include "Base/BinFileMgr.h"
#include "FamilyManager.h"
#include "BillClient.h"
#include "GlobalDB.h"
#include "SystemUser.h"
#include "ChengHaoMgr.h"
#include "SingleMusicMgr.h"

class 	MODI_OnlineClientPool;
class MODI_OnlineClient;
class MODI_OnlineClientMgr;
MODI_TableStruct * MODI_ZoneServerAPP::m_pFamilyTbl = NULL;
MODI_TableStruct * MODI_ZoneServerAPP::m_pFamilyMemTbl = NULL;
MODI_TableStruct * MODI_ZoneServerAPP::m_pCharactersTbl = NULL;
MODI_TableStruct * MODI_ZoneServerAPP::m_pSingleMusicTbl = NULL;

MODI_ZoneServerAPP::MODI_ZoneServerAPP( const char * szAppName ):
	MODI_IAppFrame( szAppName ),
	m_pManangerClient(0)
{
	m_nTransCount = 0;

}


MODI_ZoneServerAPP::~MODI_ZoneServerAPP()
{


}

int MODI_ZoneServerAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	MODI_GameSvrClientTask::Init();
	MODI_DBProxyClientTask::Init();

	// 获取相关配置信息
	const MODI_SvrZSConfig * pZoneSvrInfo = (const MODI_SvrZSConfig * )(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ZONESERVER ) );
	if( !pZoneSvrInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrADBConfig * pADBInfo = (const MODI_SvrADBConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ACCOUNTSERVER ) );
	if( !pADBInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrMSConfig * pMsInfo = (const MODI_SvrMSConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_MANGGERSERVER ) );
	if( !pMsInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
	
    Global::logger->AddLocalFileLog(pZoneSvrInfo->strLogPath);
	std::string net_log_path = pZoneSvrInfo->strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();

	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(!plist)
		return MODI_IAppFrame::enConfigInvaild;
	
	if(! plist->Init("./Config/config.xml"))
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	if( !m_packageHandler_rdb2zs.Initialization() ||
		!m_packageHandler_s2zs.Initialization() ||
		!m_packageHandler_c2zs.Initialization() ||
		!m_packageHandler_zs2ms.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}

    // 初始化bin mgr
	const MODI_SvrResourceConfig * pResInfo = (const MODI_SvrResourceConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_GAMERESOURCE ) );
	if( !pResInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}
	Global::logger->info("[%s]  Loading Bin Files ...", SYS_INIT);
    if (!MODI_BinFileMgr::GetInstancePtr()->Initial(pResInfo->strBinDir.c_str() , 
				enBinFT_DefaultAvatar || enBinFT_Music || enBinFT_GoodsPackage || enBinFT_Scene || enBinFT_Impact))
    {
        Global::logger->fatal("[%s] Fail to initialize BIN file manager.", SYS_INIT);
		return MODI_IAppFrame::enLoadResourceFaild;
    }

	    MODI_ChengHaoMgr::GetInstance()->Init();
	if( !m_gmCmdHandler.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}

	if( !m_ScriptMailHandler.Initialization() )
	{
		return MODI_IAppFrame::enLoadResourceFaild;
	}

	// 连接游戏数据库
	if(! MODI_DBManager::GetInstance().Init(Global::g_stURLMap["gamedb"]))
	{
		Global::net_logger->debug("not connect gamedb <%s>", Global::g_stURLMap["gamedb"].c_str());
		return MODI_IAppFrame::enConfigInvaild;
	}

	if(!MODI_GlobalDB::Init())
	{
		return false;
	}
	
	m_pFamilyTbl = MODI_DBManager::GetInstance().GetTable("familys");
	if(! m_pFamilyTbl)
	{
		Global::logger->fatal("[system_init] unable get familys table struct");
		return MODI_IAppFrame::enConfigInvaild;
	}

	m_pSingleMusicTbl = MODI_DBManager::GetInstance().GetTable("music");
	if( ! m_pSingleMusicTbl)
	{
		Global::logger->fatal("[system_init] unable get single music table");
		return MODI_IAppFrame::enConfigInvaild;
	}
	m_pFamilyMemTbl = MODI_DBManager::GetInstance().GetTable("family_members");
	if(! m_pFamilyMemTbl)
	{
		Global::logger->fatal("[system_init] unable get family_member table struct");
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	m_pCharactersTbl = MODI_DBManager::GetInstance().GetTable("characters");
	if(! m_pCharactersTbl)
	{
		Global::logger->fatal("[system_init] unable get characters table struct");
		return MODI_IAppFrame::enConfigInvaild;
	}

	if(! MODI_FamilyManager::GetInstance().Init())
	{
		Global::logger->fatal("[system_init] unable init family manager");
		return MODI_IAppFrame::enConfigInvaild;
	}

	if( ! MODI_SingMusicMgr::GetInstance().Init())
	{
		Global::logger->fatal("[system_init] unable init Single music manager");
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	// 创建网络服务对象
	std::vector<WORD > port_vec;
	port_vec.push_back( pZoneSvrInfo->nPort );
	port_vec.push_back( pZoneSvrInfo->nDBProxyPort );

	std::vector<std::string> ip_vec;
	ip_vec.push_back( pZoneSvrInfo->strIP.c_str() );
	ip_vec.push_back( pZoneSvrInfo->strIP.c_str() );
	m_pNetService = new MODI_ZoneService( port_vec , ip_vec );
	if( !m_pNetService )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	m_pManangerClient = new MODI_ManangerSvrClient("ManangerClient_ZS" , 
			pMsInfo->strIP.c_str() ,
			pMsInfo->nPort );
	if( !m_pManangerClient )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_ZoneSvrLogic();
	if( !m_pLogicThread )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	///创建一个系统变量用户
	MODI_SystemUser::GetInstance()->Init();	
	
	return MODI_IAppFrame::enOK;
}


int MODI_ZoneServerAPP::Run()
{

	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;

	if( m_pManangerClient)
	{
		Global::logger->info("[%s]try connect to role db server." , SYS_INIT );
		if( !m_pManangerClient->Init() )
		{
			Global::logger->info("[%s]connect to role db server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pManangerClient->Start();
			Global::logger->info("[%s]connect to role db server successful. " , SYS_INIT );
		}
	}
	else 
	{
		Global::logger->info("[%s]connect to role db  server faild. not exist role db  client. " , SYS_INIT );
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	/// init billclient
	if(! MODI_BillClient::GetInstancePtr()->Init())
	{
		Global::logger->fatal("[%s] Unable init bill(ip=%s,port=%d)", SYS_INIT, Global::g_strBillIP.c_str(), Global::g_wdBillPort);
//		return MODI_IAppFrame::enCannotConnectToServer;
	}

	sleep(2);

	MODI_S2MS_Request_ServerReg msg;
	msg.m_iServerType = SVR_TYPE_ZS;
	m_pManangerClient->SendCmd( &msg , sizeof(msg) );

	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );

	if( m_pLogicThread )
	{
		// 启动逻辑线程
		m_pLogicThread->Start();
	}
	else 
	{
		Global::logger->info("[%s] start mananger server faild. logic thread not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistLogicThread;
	}

	if( m_pNetService )
	{
		// start server for gameclients ..
		m_pNetService->Main();
	}
	else 
	{
		Global::logger->info("[%s] start mananger server faild. net service moudle not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistNetService;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_ZoneServerAPP::Shutdown()
{
	if( m_pNetService )
	{
		m_pNetService->Terminate();
	}

	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

	if( m_pManangerClient )
	{
		m_pManangerClient->TTerminate();
		m_pManangerClient->Join();
	}

	delete m_pLogicThread;
	m_pLogicThread = 0;

	delete m_pNetService;
	m_pNetService =0;

	delete m_pManangerClient;
	m_pManangerClient = 0;

	/// final billclient
	MODI_BillClient::GetInstancePtr()->DelInstance();
	
	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}
		
MODI_DBProxyClientTask * MODI_ZoneServerAPP::GetDBInterface( BYTE byServer )
{
	MODI_DBProxyClientTask * pRet = 0;
	pRet = MODI_DBProxyClientTask::FindServer( byServer );
	return pRet;
}

MODI_ZoneServerAPP * GetApp()
{
	MODI_ZoneServerAPP * pApp = (MODI_ZoneServerAPP *)(MODI_IAppFrame::GetInstancePtr());
	return pApp;
}


/** 
 * @brief 请求执行sql
 * 
 * @param sql sql语句
 * @param sql_len 语句长度
 *
 */
void MODI_ZoneServerAPP::ExecSqlToDB(const char * sql, const WORD sql_len)
{
	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface(1);
	if( !pDB )
	{
		Global::logger->info("[request_exec_sql] can't get db interface");
		return;
	}

	char cmd_buf[sql_len + sizeof(MODI_S2RDB_Request_ExecSql_Cmd) + 2];
	memset(cmd_buf, 0, sizeof(cmd_buf));
	MODI_S2RDB_Request_ExecSql_Cmd * p_send_cmd = (MODI_S2RDB_Request_ExecSql_Cmd *)cmd_buf;
	AutoConstruct(p_send_cmd);
	memcpy(p_send_cmd->m_Sql, sql, sql_len);
	p_send_cmd->m_wdSize = sql_len;
	pDB->SendCmd(p_send_cmd, p_send_cmd->GetSize());
}
