#include "OnlineClientMgr.h"
#include "OfflineUserMgr.h"
#include "protocol/c2gs.h"
#include "GameSvrClientTask.h"
#include "Base/s2zs_cmd.h"
#include "Base/HelpFuns.h"
#include "RankCore.h"

const BYTE SAVE_GROUP_NUM = 20;

MODI_OnlineClientMgr::MODI_OnlineClientMgr(): m_stSaveTime(30 * 1000)
{
}

MODI_OnlineClientMgr::~MODI_OnlineClientMgr()
{

}



/** 
 * @brief 玩家第一次登陆，切频道不会调用此函数 
 *
 *
 */
bool 	MODI_OnlineClientMgr::OnClientLogin( MODI_OnlineClient * pClient , bool bAdd , bool bSync )
{
	if( bAdd )
	{
		char role_name[ROLE_NAME_MAX_LEN + 1];
		Tolower( pClient->GetRoleName() , pClient->GetNameLen() , role_name );
		if( m_mgr.Add( pClient->GetCharID() ,  role_name , pClient->GetAccid() , pClient ) == false )
		{
			MODI_ASSERT( 0 );
			return false;
		}

		pClient->OnLoginIn();
	}

	if( !bSync )
		return true;

	MODI_RelationList & relList = pClient->GetRelationList();

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient != pClient )
		{
			if( pOnlineClient->IsNeedNotifyWithSbLogin( pClient->GetCharID() ) )
			{
				// 通知该客户端，有好友上线
				pOnlineClient->OnRelationLogin( pClient );
				/// 维护一个反向的社会关系表
				pClient->AddReverseRelation(pOnlineClient->GetCharID());
			}

			MODI_Relation * pRel = relList.FindRelation( pOnlineClient->GetCharID() );
			if( pRel )
			{
				pRel->SetOnline( true );
				pRel->SetServerName( pOnlineClient->GetServerName() );
				pRel->SetGameState( pOnlineClient->GetFullData().m_roleInfo.m_baseInfo.m_ucGameState );
				pRel->SetRoomID( pOnlineClient->GetFullData().m_roleInfo.m_baseInfo.m_roomID );
			}
		}

		itor = inext;
	}


	return true;
}

bool 	MODI_OnlineClientMgr::OnClientLoginout( MODI_OnlineClient * pClient )
{
	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( pClient->GetRoleName() , pClient->GetNameLen() , role_name );
	bool bRet = m_mgr.Del( pClient->GetCharID() , role_name , pClient->GetAccid() );
	if( !bRet )
	{
		MODI_ASSERT(0);
	}

	pClient->OnLoginOut();

	struct MODI_NotifyReverse: public MODI_ReverseRelationCallBack
	{
		virtual ~MODI_NotifyReverse(){}
		MODI_NotifyReverse(MODI_OnlineClient * p_client, MODI_NKeyMap::TMAPS_KEY1 mgr_map): p_src(p_client), m_mgr_map(mgr_map){}
		
		bool Exec(MODI_CHARID guid)
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr_map.find(guid);
			if(itor != m_mgr_map.end())
			{
				MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
				if( pOnlineClient->IsNeedNotifyWithSbLoginOut( p_src->GetCharID() ) )
				{
					pOnlineClient->OnRelationLoginOut(p_src);
				}
			}
			return true;
		}
		
		MODI_OnlineClient * p_src;
		MODI_NKeyMap::TMAPS_KEY1 m_mgr_map;
	};

	MODI_NotifyReverse notify_mem(pClient, m_mgr.GetGUIDMapContent());
	pClient->AllReverseExec(notify_mem);
	
	
#if 0
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);

		if( pOnlineClient != pClient )
		{
			if( pOnlineClient->IsNeedNotifyWithSbLoginOut( pClient->GetCharID() ) )
			{
				// 通知该客户端，有好友离线
				pOnlineClient->OnRelationLoginOut( pClient );
			}
		}

		itor = inext;
	}
#endif
	
	return true;
}


bool 	MODI_OnlineClientMgr::OnClientReEnterChannel( MODI_OnlineClient * pClient )
{
	// 通知该客户端的所有好友，状态改变
	MODI_GS2C_Notify_RelationStateChanged 	msg;
	msg.m_guid = pClient->GetGUID();
	msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enChangeChannel;
	//msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enOnlineState;
	msg.m_newState.m_onlineState.m_bOnline = true;
	memset( msg.m_newState.m_onlineState.m_szServerName , 
			0 , 
			sizeof(msg.m_newState.m_onlineState.m_szServerName) );
	safe_strncpy( msg.m_newState.m_onlineState.m_szServerName , 
			pClient->GetServerName(),
			sizeof(msg.m_newState.m_onlineState.m_szServerName) );

	struct MODI_NotifyReverse: public MODI_ReverseRelationCallBack
	{
		virtual ~MODI_NotifyReverse(){}
		MODI_NotifyReverse(MODI_OnlineClient * p_client, MODI_NKeyMap::TMAPS_KEY1 mgr_map,
						   Cmd::stNullCmd * p_cmd, WORD size):
			p_src(p_client), m_mgr_map(mgr_map), p_send_cmd(p_cmd), cmd_size(size){}
		
		bool Exec(MODI_CHARID guid)
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr_map.find(guid);
			if(itor != m_mgr_map.end())
			{
				MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
				MODI_RelationList & relList = pOnlineClient->GetRelationList();
				MODI_Relation * pRel = relList.FindRelation( p_src->GetCharID() );
				if( pRel )
				{
					pOnlineClient->SendPackage(p_send_cmd, cmd_size);
				}

				MODI_RelationList & relScrList = p_src->GetRelationList();
				MODI_Relation * pOldRel = relScrList.FindRelation( pOnlineClient->GetCharID() );
				if( pOldRel )
				{
					pOldRel->SetOnline( true );
					pOldRel->SetServerName( pOnlineClient->GetServerName() );
					pOldRel->SetGameState( pOnlineClient->GetFullData().m_roleInfo.m_baseInfo.m_ucGameState );
					pOldRel->SetRoomID( pOnlineClient->GetFullData().m_roleInfo.m_baseInfo.m_roomID );
				}
			
			}
			return true;
		}
		
		MODI_OnlineClient * p_src;
		MODI_NKeyMap::TMAPS_KEY1 m_mgr_map;
		Cmd::stNullCmd * p_send_cmd;
		WORD cmd_size;
	};

	MODI_NotifyReverse notify_mem(pClient, m_mgr.GetGUIDMapContent(), &msg, sizeof(msg));
	pClient->AllReverseExec(notify_mem);
	
#if 0
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient != pClient )
		{
			MODI_RelationList & relList = pOnlineClient->GetRelationList();
			MODI_Relation * pRel = relList.FindRelation( pClient->GetCharID() );
			if( pRel )
			{
				pOnlineClient->SendPackage(&msg,sizeof(msg));
			}
		}
		itor = inext;
	}
#endif	
	return true;
}

MODI_OnlineClient * MODI_OnlineClientMgr::FindByGUID( MODI_CHARID guid )
{
	return (MODI_OnlineClient *)(m_mgr.FindByGUID( guid ));
}

MODI_OnlineClient * MODI_OnlineClientMgr::FindByName( const std::string & strName )
{
	char role_name[ROLE_NAME_MAX_LEN + 1];
	Tolower( strName.c_str() , strName.size() , role_name );
	return (MODI_OnlineClient *)(m_mgr.FindByName( role_name ) );
}

MODI_OnlineClient * MODI_OnlineClientMgr::FindByAccid( defAccountID accid )
{
	return (MODI_OnlineClient *)(m_mgr.FindByAccid( accid ) );
}

MODI_OnlineClient * MODI_OnlineClientMgr::FindByAccName(const std::string & acc_name)
{
	MODI_OnlineClient * pOnlineClient = NULL;
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	for(; itor != m_mgr.GetGUIDMapContent().end(); itor++)
	{
		pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if(!pOnlineClient)
			continue;
		
		if(strcmp(acc_name.c_str(), pOnlineClient->GetAccName()) == 0)
		{
			return pOnlineClient;
		}
	}
	return NULL;
}
		
void MODI_OnlineClientMgr::Update(const MODI_RTime &  cur_rtime, const MODI_TTime &  cur_ttime)
{
	static BYTE group_num = 0;
	static BYTE save_group_num = 0;
	
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		DWORD acc_id = pOnlineClient->GetAccid();
		if(acc_id % SAVE_GROUP_NUM == group_num)
		{
			pOnlineClient->Update(cur_rtime, cur_ttime);
		}
		itor = inext;
	}
	group_num++;
	if(group_num == SAVE_GROUP_NUM)
	{
		group_num = 0;
	}

	if(m_stSaveTime(cur_rtime))
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
		while( itor != m_mgr.GetGUIDMapContent().end() )
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
			inext++;
			MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
			DWORD acc_id = pOnlineClient->GetAccid();
			if(acc_id % SAVE_GROUP_NUM == save_group_num)
			{
				pOnlineClient->SaveToDB();
#ifdef _DEBUG
				Global::logger->debug("[save_to_db] <group=%u,accid=%u>", save_group_num, acc_id);
#endif				
			}
			itor = inext;
		}
		save_group_num++;
		if(save_group_num == SAVE_GROUP_NUM)
		{
			save_group_num = 0;
		}
	
	}
}

void MODI_OnlineClientMgr::BroadcastSysChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type)
{
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		pOnlineClient->SendSystemChat(chat_content, chat_size, notify_type);
		itor = inext;
	}
}


void MODI_OnlineClientMgr::Broadcast(const void * pt_null_cmd, int cmd_size)
{
	/*
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		pOnlineClient->SendPackage( pt_null_cmd , cmd_size );
		itor = inext;
	}
	*/
	char	packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Broadcast_Cmd  * broad = (MODI_ZS2S_Broadcast_Cmd *)packet;
	
	AutoConstruct(broad);

	broad->m_nSize = cmd_size;
	memcpy( broad->m_csCmd,(char *)pt_null_cmd,cmd_size);	

	MODI_GameSvrClientTask::BroadcastAllServer(broad,sizeof(MODI_ZS2S_Broadcast_Cmd)+cmd_size);


}

void MODI_OnlineClientMgr::BroadcastFensi(MODI_OnlineClient * pClient , const void * pt_null_cmd, int cmd_size)
{
	if( !pClient )
		return ;
#if 0

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient )
		{
			MODI_Relation * pRel = pOnlineClient->GetRelationList().FindRelation( pClient->GetCharID() );
			if( pRel && pRel->IsIdol() )
			{
				pOnlineClient->SendPackage( pt_null_cmd , cmd_size );
			}
		}
		itor = inext;
	}
#endif
	char packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Broadcast_CmdRange * range = (MODI_ZS2S_Broadcast_CmdRange *)packet;
	AutoConstruct(range);

	range->m_nSize = cmd_size;
	memcpy(range->m_csCmd,(const char *)pt_null_cmd,cmd_size);

	MODI_CHARID 	*id = (MODI_CHARID *)(range->m_csCmd+cmd_size);
	WORD	n=0;

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient )
		{
			MODI_Relation * pRel = pOnlineClient->GetRelationList().FindRelation( pClient->GetCharID() );
			if( pRel && pRel->IsIdol() )
			{
				*id++ = pOnlineClient->GetCharID();
				n++;
			}
		}
		itor = inext;
	}

	size_t  sendsize = sizeof( MODI_ZS2S_Broadcast_CmdRange ) + range->m_nSize+sizeof(MODI_CHARID)*n;
	MODI_GameSvrClientTask::BroadcastAllServer( range,sendsize);
		

}

void MODI_OnlineClientMgr::BroadcastWithRelation( MODI_OnlineClient * pClient , const void * pt_null_cmd , int cmd_size)
{
	if( !pClient )
		return ;

#if 0
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient )
		{
			MODI_Relation * pRel = pOnlineClient->GetRelationList().FindRelation( pClient->GetCharID() );
			if( pRel && pRel->IsBlack() == false )
			{
				pOnlineClient->SendPackage( pt_null_cmd , cmd_size );
			}
		}
		itor = inext;
	}
#endif

	char packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Broadcast_CmdRange * range = (MODI_ZS2S_Broadcast_CmdRange *)packet;
	AutoConstruct(range);

	range->m_nSize = cmd_size;
	memcpy(range->m_csCmd,(char *)pt_null_cmd,cmd_size);

	MODI_CHARID 	*id = (MODI_CHARID *)(range->m_csCmd+cmd_size);
	WORD	n=0;

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient )
		{
			MODI_Relation * pRel = pOnlineClient->GetRelationList().FindRelation( pClient->GetCharID() );
			if( pRel && pRel->IsBlack() == false )
			{
				*id++ = pOnlineClient->GetCharID();
				n++;
			}
		}
		itor = inext;
	}

	size_t  sendsize = sizeof( MODI_ZS2S_Broadcast_CmdRange ) + range->m_nSize+sizeof(MODI_CHARID)*n;
	MODI_GameSvrClientTask::BroadcastAllServer( range,sendsize);

}

void MODI_OnlineClientMgr::BroadcastToServer(const void * pt_null_cmd, int cmd_size , BYTE nServerID )
{
#if 0
	if( !nServerID )
		return ;

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient->GetServerID() == nServerID )
			pOnlineClient->SendPackage( pt_null_cmd , cmd_size );
		itor = inext;
	}
#endif


	char packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Broadcast_CmdRange * range = (MODI_ZS2S_Broadcast_CmdRange *)packet;
	AutoConstruct(range);

	range->m_nSize = cmd_size;
	memcpy(range->m_csCmd,(char *)pt_null_cmd,cmd_size);

	MODI_CHARID 	*id = (MODI_CHARID *)(range->m_csCmd+cmd_size);
	WORD	n=0;
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient->GetServerID() == nServerID )
		{
				*id++ = pOnlineClient->GetCharID();
				n++;
		}
		itor = inext;
	}
	size_t  sendsize = sizeof( MODI_ZS2S_Broadcast_CmdRange ) + range->m_nSize+sizeof(MODI_CHARID)*n;
	MODI_GameSvrClientTask::BroadcastAllServer( range,sendsize);
}

void MODI_OnlineClientMgr::KickAllClients(const WORD & channel_id)
{
	if(channel_id == 200)
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
		while( itor != m_mgr.GetGUIDMapContent().end() )
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
			inext++;
			MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
			if( pOnlineClient )
			{
				MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnlineClient->GetServerID() );
				if( pServer )
				{
					MODI_ZS2S_Notify_KickSb kickmsg;
					kickmsg.m_byReason = kAdminKick;
					kickmsg.m_guid = pOnlineClient->GetGUID();
					kickmsg.m_sessionID = pOnlineClient->GetSessionID();
					pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
				}
			}
			itor = inext;
		}
	}
	else
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
		while( itor != m_mgr.GetGUIDMapContent().end() )
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
			inext++;
			MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
			if( pOnlineClient )
			{
				if(pOnlineClient->GetServerID() == channel_id)
				{
					MODI_GameSvrClientTask * pServer = MODI_GameSvrClientTask::FindServer( pOnlineClient->GetServerID() );
					if( pServer )
					{
						MODI_ZS2S_Notify_KickSb kickmsg;
						kickmsg.m_byReason = kAdminKick;
						kickmsg.m_guid = pOnlineClient->GetGUID();
						kickmsg.m_sessionID = pOnlineClient->GetSessionID();
						pServer->SendCmd( &kickmsg , sizeof(kickmsg) );
					}
				}
			}
			itor = inext;
		}
	}
}

void MODI_OnlineClientMgr::UpdateSelfRank( enRankType type )
{
	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();

	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;
		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient )
		{
			DWORD rank = rank_core->GetSelfRank( pOnlineClient->GetCharID() , type );
			pOnlineClient->SetSelfRank( type , rank );
		}
		itor = inext;
	}
}



bool 	MODI_OnlineClientMgr::OnClientLeaveChannel( MODI_OnlineClient * pClient )
{
	// 通知该客户端的所有好友，状态改变
	MODI_GS2C_Notify_RelationStateChanged 	msg;
	msg.m_guid = pClient->GetGUID();
	//msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enOnlineState;
	msg.m_byWhatChanged = MODI_GS2C_Notify_RelationStateChanged::enChangeChannel;
	msg.m_newState.m_onlineState.m_bOnline = true;
	memset( msg.m_newState.m_onlineState.m_szServerName , 
			0 , 
			sizeof(msg.m_newState.m_onlineState.m_szServerName) );
	MODI_CHARID	character[2000];
	memset(character, 0, sizeof(character));
	int n=0;
	//#if 0
	struct MODI_NotifyReverse: public MODI_ReverseRelationCallBack
	{
		virtual ~MODI_NotifyReverse(){}
		MODI_NotifyReverse(MODI_OnlineClient * p_client, MODI_NKeyMap::TMAPS_KEY1 mgr_map,
						   Cmd::stNullCmd * p_cmd, WORD size, MODI_CHARID * member, int * num):
			p_src(p_client), m_mgr_map(mgr_map), p_send_cmd(p_cmd), cmd_size(size)
		{
			mem = member;
			number = num;
		}
		
		bool Exec(MODI_CHARID guid)
		{
			MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr_map.find(guid);
			if(itor != m_mgr_map.end())
			{
				MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
				MODI_RelationList & relList = pOnlineClient->GetRelationList();
				MODI_Relation * pRel = relList.FindRelation( p_src->GetCharID() );
				if( pRel )
				{
					mem[*number] = pOnlineClient->GetCharID();
					*number = *number +1;
				}
			}
			return true;
		}
		
		MODI_CHARID	*mem;
		int	* number;
		MODI_OnlineClient * p_src;
		MODI_NKeyMap::TMAPS_KEY1 m_mgr_map;
		Cmd::stNullCmd * p_send_cmd;
		WORD cmd_size;
	};

	MODI_NotifyReverse notify_mem(pClient, m_mgr.GetGUIDMapContent(), &msg, sizeof(msg),character, &n);
	pClient->AllReverseExec(notify_mem);
	BroadcastRange( &msg, sizeof(msg), character, n );
	//#endif
	
#if 0
	MODI_NKeyMap::TMAPS_KEY1_C_ITER itor = m_mgr.GetGUIDMapContent().begin();
	while( itor != m_mgr.GetGUIDMapContent().end() )
	{
		MODI_NKeyMap::TMAPS_KEY1_C_ITER inext = itor;
		inext++;

		MODI_OnlineClient * pOnlineClient = (MODI_OnlineClient *)(itor->second);
		if( pOnlineClient != pClient )
		{
			MODI_RelationList & relList = pOnlineClient->GetRelationList();
			MODI_Relation * pRel = relList.FindRelation( pClient->GetCharID() );
			if( pRel )
			{
				pOnlineClient->SendPackage(&msg,sizeof(msg));
			}
		}
		itor = inext;
	}
#endif	
	return true;
}


void	MODI_OnlineClientMgr::BroadcastRange( const void * pt_null_cmd , int cmd_size ,MODI_CHARID * member, int nmem)
{

	char packet[Skt::MAX_PACKETSIZE];
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Broadcast_CmdRange * range = (MODI_ZS2S_Broadcast_CmdRange *)packet;
	AutoConstruct(range);

	range->m_nSize = cmd_size;
	range->m_nMemNum = nmem;
	memcpy(range->m_csCmd,(char *)pt_null_cmd,cmd_size);

	MODI_CHARID 	*id = (MODI_CHARID *)(range->m_csCmd+cmd_size);

	memcpy( id, member, nmem*sizeof(MODI_CHARID));

	size_t  sendsize = sizeof(MODI_ZS2S_Broadcast_CmdRange)+cmd_size+nmem*sizeof(MODI_CHARID);

	MODI_GameSvrClientTask::BroadcastAllServer( range,sendsize);

}














