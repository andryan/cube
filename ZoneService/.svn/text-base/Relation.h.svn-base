/** 
 * @file Relation.h
 * @brief 联系人相关
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-13
 */

#ifndef MODI_RELATION_H_
#define MODI_RELATION_H_

#include "gamestructdef.h"

class 	MODI_Relation
{
	public:
		MODI_Relation();
		~MODI_Relation();

		void Init( const MODI_DBRelationInfo & info );

		// 是否好友关系
		bool IsFriend() const
		{
			return RelationHelpFuns::IsFriend( m_info );
		}

		// 是否黑名单关系
		bool IsBlack() const
		{
			return RelationHelpFuns::IsBlack( m_info );
		}

		// 是否情人关系
		bool IsLover() const
		{
			return RelationHelpFuns::IsLover( m_info );
		}

		// 是否夫妻关系
		bool IsMarried() const
		{
			return RelationHelpFuns::IsMarried( m_info );
		}

		// 是否偶像关系
		bool IsIdol() const
		{
			return RelationHelpFuns::IsIdol( m_info );
		}

		// 设置为好友关系
		void SetFriend()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::SetFriend( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 	设置为黑名单关系
		void SetBlack()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::SetBlack( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 设置为情人关系
		void SetLover()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::SetLover( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 设置为夫妻关系
		void SetMarried()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::SetMarried( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 设置为偶像关系
		void SetIdol()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::SetIdol( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 清掉关系
		void ClearFBLM()
		{
			bool bIsIdol = IsIdol();
			m_info.m_RelationType = enRelationT_None;
			if( bIsIdol )
				SetIdol();
		}

		// 清掉偶像关系
		void ClearIdol()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::ClearIdol( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		// 清掉所有关系
		void ClearAllRelationType()
		{
			int i = m_info.m_RelationType;
			RelationHelpFuns::ClearAll( i );
			m_info.m_RelationType = (enRelationType)i;
		}

		/** 
		 * @brief 是否还有关系
		 * 
		 * @return 	
		 */
		bool IsHasRelation() const;

		void SetOnline( bool bSet )
		{
			m_info.m_bOnline = bSet;
		}

		void SetServerName( const char * szServerName );
		const char * GetServerName() const;

		void SetGameState( kGameState state );
		kGameState GetGameState() const;

		void SetRoomID( defRoomID id );
		defRoomID GetRoomID() const;

		
		bool IsOnline() const
		{
			return m_info.m_bOnline;
		}

		BYTE GetSex() const
		{
			return m_info.m_bySex;
		}

		const MODI_GUID & GetGUID() const
		{
			return m_info.m_guid;
		}

		MODI_CHARID GetCharID() const
		{
			return GUID_LOPART( GetGUID() );
		}
			
		const char * GetName() const
		{
			return m_info.m_szName;
		}

		enRelationType   GetType()
		{
			return m_info.m_RelationType;
		}



		const MODI_DBRelationInfo & GetInfo() const { return m_info; }


		bool IsNeedSave()
		{
			return m_blNeedSave;
		}

		void SetNeedSave()
		{
			m_blNeedSave = true;
		}

		void ClearNeedSave()
		{
			m_blNeedSave = false;
		}
		
		MODI_DBRelationInfo 	m_info;
	private:

		/// 是否需要保持
		bool m_blNeedSave;
};



/** 
 * @brief 联系人结构池
 */
class 	MODI_RelationPool
{
	public:

		static MODI_Relation * 	New();
		static void 			Delete( MODI_Relation * p );

};

#endif
