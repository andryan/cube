/** 
 * @file OnlineClientMgr.h
 * @brief 在线客户端管理器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-05-17
 */

#ifndef ONLINE_CLIENT_MGR_H_
#define ONLINE_CLIENT_MGR_H_



#include "OnlineClient.h"
#include "SingleObject.h"
#include "TNKeyMap.h"


class 	MODI_OnlineClientMgr : public CSingleObject<MODI_OnlineClientMgr> 
{
	friend class MODI_World;

	public:
		MODI_OnlineClientMgr();
		virtual ~MODI_OnlineClientMgr();

		/** 
		 * @brief 有客户端登入
		 * 
		 * @param pClient 	登入的客户端
		 * @param bAdd 		是否添加到在线管理器
		 * @param bSync 	是否同步相关信息
		 * 
		 * @return 	成功返回true
		 */
		bool 	OnClientLogin( MODI_OnlineClient * pClient , bool bAdd , bool bSync );

		/** 
		 * @brief 客户端登出
		 * 
		 * @param pClient 	登出的客户端
		 * 
		 * @return 	成功返回true
		 */
		bool 	OnClientLoginout( MODI_OnlineClient * pClient );

		bool 	OnClientReEnterChannel( MODI_OnlineClient * pClient );

		MODI_OnlineClient * FindByGUID( MODI_CHARID guid );

		MODI_OnlineClient * FindByName( const std::string & strName );

		MODI_OnlineClient * FindByAccid( defAccountID accid );

		MODI_OnlineClient * FindByAccName(const std::string & strAccName);

		void Update(const MODI_RTime &  cur_rtime, const MODI_TTime & cur_ttime);


		/** 
		 * @brief 广播整个游戏世界 
		 * 
		 * @param pt_null_cmd
		 * @param cmd_size
		 * 
		 * @return 	
		 */
		void Broadcast(const void * pt_null_cmd, int cmd_size);
		void BroadcastSysChat(const char * chat_content, const WORD chat_size, enSystemNoticeType notify_type  = enSystemNotice_Default);
		void	BroadcastRange(const void *pt_null_cmd, int cmd_size, MODI_CHARID *member, int nmem);

		void BroadcastFensi(MODI_OnlineClient * pClient , const void * pt_null_cmd, int cmd_size);

		/** 
		 * @brief 广播给所有和指定客户端有联系的人
		 * 
		 * @param pClient
		 * @param pt_null_cmd
		 * @param cmd_size
		 */
		void BroadcastWithRelation( MODI_OnlineClient * pClient , const void * pt_null_cmd , int cmd_size);

		/** 
		 * @brief 广播某个服务器
		 * 
		 * @param pt_null_cmd
		 * @param cmd_size
		 * @param nServerID
		 * 
		 * @return 	
		 */
		void BroadcastToServer(const void * pt_null_cmd, int cmd_size , BYTE nServerID );

		/** 
		 * @brief 查询区在线人数
		 * 
		 * @return 区实际在线人数
		 */
		const unsigned int Size() 
		{
			return m_mgr.Size();
		}

		void KickAllClients(const WORD & channel_id);

		void UpdateSelfRank( enRankType type );

	public:
		bool 	OnClientLeaveChannel( MODI_OnlineClient * pClient );

		MODI_NKeyMap 	m_mgr;
 private:
		MODI_Timer m_stSaveTime;
};






#endif
