#include "OfflineUser.h"
#include "OnlineClient.h"


MODI_OfflineUser::MODI_OfflineUser()
{
	m_dwAccId = 0;
}

MODI_OfflineUser::~MODI_OfflineUser()
{

}

void 	MODI_OfflineUser::Init(MODI_OnlineClient * p_client)
{
	const MODI_CharactarFullData & charFullData = p_client->GetFullData();
	m_charID = GUID_LOPART( charFullData.m_roleInfo.m_baseInfo.m_roleID );
	m_dwAccId = p_client->GetAccountID();
	memcpy( m_namecard.m_szName , charFullData.m_roleInfo.m_baseInfo.m_cstrRoleName , sizeof(m_namecard.m_szName) - 1 );
	memcpy( m_namecard.m_szAccName , p_client->GetAccName(), sizeof(m_namecard.m_szAccName) - 1 );
	m_namecard.m_bySex = charFullData.m_roleInfo.m_baseInfo.m_ucSex;
	m_namecard.m_byLevel = charFullData.m_roleInfo.m_baseInfo.m_ucLevel;
	m_namecard.m_RoleDetail = charFullData.m_roleInfo.m_detailInfo;
	m_namecard.m_avatar = charFullData.m_roleInfo.m_normalInfo.m_avatarData;
	m_namecard.m_nDefaultAvatarIdx = charFullData.m_roleInfo.m_normalInfo.m_nDefavatarIdx;
}

void 	MODI_OfflineUser::InitNameCard( MODI_CHARID charid , const defAccountID acc_id, const MODI_NameCard & card )
{
	m_charID = charid;
	m_dwAccId = acc_id;
	m_namecard = card;
}

/// 更新家族名片
void MODI_OfflineUser::InitFamilyCard(MODI_FamilyCard & card)
{
	m_namecard.m_stFamilyCard = card;
}
