#include "PackageHandler_c2zs.h"
#include "s2adb_cmd.h"
#include "s2rdb_cmd.h"
#include "protocol/c2gs.h"
#include "Base/AssertEx.h"
#include "Base/HelpFuns.h"
#include "GameSvrClientTask.h"
#include "s2zs_cmd.h"
#include "OnlineClientMgr.h"
#include "GameHelpFun.h"
#include "OfflineUserMgr.h"
#include "ZoneServerAPP.h"
#include "MailCore.h"
#include "GMCommand_ZS.h"
#include "BillClient.h"
#include "FamilyInvite.h"

MODI_PackageHandler_c2zs::MODI_PackageHandler_c2zs()
{

}

MODI_PackageHandler_c2zs::~MODI_PackageHandler_c2zs()
{

}

bool MODI_PackageHandler_c2zs::FillPackageHandlerTable()
{
	// 客户端请求添加联系人
	AddPackageHandler( MAINCMD_RELATION , MODI_C2GS_Request_AddRelation::ms_SubCmd ,
			&MODI_PackageHandler_c2zs::AddRelation_Handler );

	// 客户端请求改变联系人的关系类型
	AddPackageHandler( MAINCMD_RELATION , MODI_C2GS_Request_ModifyRelation::ms_SubCmd ,
			&MODI_PackageHandler_c2zs::ModifyRelation_Handler );

	// 请求某个角色的详细信息
	AddPackageHandler( MAINCMD_ROLE , MODI_C2GS_Request_RoleDetailInfo::ms_SubCmd,
			&MODI_PackageHandler_c2zs::ReqRoleDetailInfo_Handler );

	// 和某个人私聊
	AddPackageHandler( MAINCMD_CHAT , MODI_C2GS_Request_PrivateChat_ByName::ms_SubCmd,
			&MODI_PackageHandler_c2zs::PrivateChatByName_Handler );

	// 发送邮件
	AddPackageHandler( MAINCMD_MAIL , MODI_C2GS_Request_SendMail::ms_SubCmd,
			&MODI_PackageHandler_c2zs::SendMail_Handler );

	// 删除邮件
	AddPackageHandler( MAINCMD_MAIL , MODI_C2GS_Request_DelMail::ms_SubCmd,
			&MODI_PackageHandler_c2zs::DelMail_Handler );

	// 设置以读状态
	AddPackageHandler( MAINCMD_MAIL , MODI_C2GS_Request_SetReadMail::ms_SubCmd,
			&MODI_PackageHandler_c2zs::SetMailRead_Handler );

	// gm命令的处理
    AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_GMCmd::ms_SubCmd,
            &MODI_PackageHandler_c2zs::GMCommand_Handler);

    AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_SystemBroast::ms_SubCmd,
            &MODI_PackageHandler_c2zs::SystemBroast_Handler );

    AddPackageHandler(MAINCMD_MAIL, MODI_C2GS_Request_GetAttachment::ms_SubCmd,
            &MODI_PackageHandler_c2zs::GetAttachment_Handler );

    AddPackageHandler(MAINCMD_CHAT, MODI_C2GS_Request_RangeChat::ms_SubCmd,
            &MODI_PackageHandler_c2zs::WorldChat_Handler );

	/// 客户端申请充值
	AddPackageHandler(MAINCMD_PAY, MODI_C2GS_Request_PayCmd::ms_SubCmd,
            &MODI_PackageHandler_c2zs::RequestPayCmd_Handler );
	
	AddPackageHandler(MAINCMD_RANK, MODI_C2GS_Request_Rank::ms_SubCmd,
            &MODI_PackageHandler_c2zs::RequestRank_Handler );

	AddPackageHandler(MAINCMD_RANK, MODI_C2GS_Request_SpecificRank::ms_SubCmd,
            &MODI_PackageHandler_c2zs::RequestSpecificByRankNo_Handler );

	AddPackageHandler(MAINCMD_RANK, MODI_C2GS_Request_SelfRank::ms_SubCmd,
            &MODI_PackageHandler_c2zs::RequestSelfRank_Handler );

	/// 家族相关的操作
	AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyIn::ms_SubCmd,
            &MODI_PackageHandler_c2zs::LoginFamily_Handler );
	/// 创建家族
	AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_CreateFamily::ms_SubCmd,
            &MODI_PackageHandler_c2zs::CreateFamily_Handler );


	/// 某人对家族的操作
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyOpt::ms_SubCmd,
							&MODI_PackageHandler_c2zs::FamilyOpt_Handler );
	/// 对申请成员进行操作
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyOptRequest::ms_SubCmd,
							&MODI_PackageHandler_c2zs::RequestMemberOpt_Handler );
	/// 家族职位更改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyModifyPosition::ms_SubCmd,
							&MODI_PackageHandler_c2zs::FModifyPosition_Handler );
	/// 访问家族
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_VisitFamily::ms_SubCmd,
							&MODI_PackageHandler_c2zs::VisitFamily_Handler );
	/// 家族名字检测
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyNameCheck::ms_SubCmd,
							&MODI_PackageHandler_c2zs::FamilyNameCheck_Handler);
	/// 家族宣言修改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilySetXuanyan::ms_SubCmd,
							&MODI_PackageHandler_c2zs::ModiFamilyXuanyan_Handler);
	/// 家族公告修改
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilySetPublic::ms_SubCmd,
							&MODI_PackageHandler_c2zs::ModiFamilyPublic_Handler);
	/// 家族邀请
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_FamilyInvite::ms_SubCmd,
							&MODI_PackageHandler_c2zs::FamilyInvite_Handler );
	/// 家族邀请回答 
	this->AddPackageHandler(MAINCMD_FAMILY, MODI_C2GS_Request_InvitionResponse::ms_SubCmd,
							&MODI_PackageHandler_c2zs::FamilyInviteResponse_Handler );

	/// INA版本查询
	this->AddPackageHandler(MAINCMD_SHOP, MODI_C2GS_Req_Balance::ms_SubCmd,
                            &MODI_PackageHandler_c2zs::ReqBalance_Handler );
	return true;
}


/** 
 * @brief 客户端请求和某人增加某种社会关系
 *
 *
 */
int MODI_PackageHandler_c2zs::AddRelation_Handler(void * pObj, const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_AddRelation , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_AddRelation * pMsg = (const MODI_C2GS_Request_AddRelation *)(pt_null_cmd);

	size_t nNameLen = 0;
	if( MODI_GameHelpFun::StrSafeCheck( pMsg->m_szTargetName , sizeof(pMsg->m_szTargetName) , &nNameLen ) == false )
	{
		Global::logger->warn("[%s] client <name=%s> req add relation , but relation's name is invalid<too large>." , 
				ERROR_PACKAGE ,
				pClient->GetRoleName() );
		return enPHandler_Kick;
	}
	
	if( !nNameLen )
	{
		return enPHandler_Kick;
	}

	if( (pMsg->m_type > MODI_C2GS_Request_AddRelation::enBegin &&
			pMsg->m_type < MODI_C2GS_Request_AddRelation::enEnd ) == false )
	{
		Global::logger->warn("[%s] client <name=%s> req add relation , but relation's type is invalid<%u>." , 
				ERROR_PACKAGE ,
				pClient->GetRoleName(),
			    pMsg->m_type);
		return enPHandler_Kick;
	}

	if( strcmp( pMsg->m_szTargetName , pClient->GetRoleName() ) == 0 )
	{
		return enPHandler_Kick;
	}

	/// 是否存在该联系人
	MODI_RelationList & relList = pClient->GetRelationList();
	MODI_GS2C_Notify_AddRelationRes res;
	res.m_byResult = enRelationRetT_Ok;
	safe_strncpy( res.m_info.m_szName  , pMsg->m_szTargetName , sizeof(res.m_info.m_szName) );
	MODI_Relation * pFindRel = relList.FindRelation( pMsg->m_szTargetName , nNameLen );
	if( pFindRel )	
	{
		///  已经存在于联系人中
		res.m_byResult = enRelationRetT_Add_AlreadyIn;
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	/// 添加的是否陌生人，该陌生人是否在线
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pTargetClient = pMgr->FindByName( pMsg->m_szTargetName );
	enRelOptType optType = enRelOpt_Begin;

	static const std::string strOptName[] = { "AddFriend" , "AddBlack" , "AddIdol" };
	const char * szOptName = "unknow opt";

	if( pMsg->m_type == MODI_C2GS_Request_AddRelation::enFriend )
	{
		optType = enRelOpt_AddF;
		szOptName  = strOptName[0].c_str();
	}
	else if( pMsg->m_type == MODI_C2GS_Request_AddRelation::enBlack )
	{
		optType = enRelOpt_AddB;
		szOptName  = strOptName[1].c_str();
	}
	else if( pMsg->m_type == MODI_C2GS_Request_AddRelation::enIdol )
	{
		optType = enRelOpt_AddIdol;
		szOptName  = strOptName[2].c_str();
	}
	else 
	{
		MODI_ASSERT(0);
		return enPHandler_Kick;
	}


	bool bOk = true;
	if( pTargetClient )
	{
		res.m_info.m_bOnline = true;
		res.m_info.m_bySex = pTargetClient->GetSexType();
		res.m_info.m_guid = pTargetClient->GetGUID();
	}
	else 
	{
		res.m_info.m_bOnline = false;

		MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
		MODI_OfflineUser * pOfflineUser = pOfflineMgr->FindByName( pMsg->m_szTargetName );
		if( pOfflineUser )
		{
			res.m_info.m_bySex = pOfflineUser->GetSex();
			res.m_info.m_guid = MAKE_GUID( pOfflineUser->GetCharID() , enObjType_Player );
		}
		else 
		{
			/// 向db请求后，db返回加入离线管理器，最后还是走这个流程
			MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
			if( pDB )
			{
				MODI_S2RDB_Request_RoleDetailInfo reqRoleInfo;
				reqRoleInfo.m_opt = kReqRelationOpt;
				reqRoleInfo.m_reqCharid = pClient->GetCharID();
				safe_strncpy( reqRoleInfo.m_szName  , pMsg->m_szTargetName , sizeof(reqRoleInfo.m_szName)  );
				reqRoleInfo.m_RelationType = pMsg->m_type;
				pDB->SendCmd( &reqRoleInfo , sizeof(reqRoleInfo) );

				Global::logger->info("[%s] client<%s> request <%s> character<%s>, but target is offline. request to db .", 
						RELATION_MODULE , 
						pClient->GetRoleName() ,
						szOptName ,
						pMsg->m_szTargetName );

				return enPHandler_OK;
			}
			else 
			{
				res.m_byResult = enRelationRetT_UnknowError;
				Global::logger->info("[%s] client<%s> request <%s> character<%s>,  but can't get db .", 
						RELATION_MODULE , 
						pClient->GetRoleName() ,
						szOptName ,
						pMsg->m_szTargetName );
				bOk = false;
			}
		}
	}

	if( bOk )
	{
		MODI_Relation * pRel = MODI_RelationPool::New();
		if( !pRel)
		{
			Global::logger->fatal("[add_relation] <%s,%s> request add relation but relation pool is full\n",pClient->GetAccName(),res.m_info.m_szName);
			res.m_byResult = enRelationRetT_UnknowError;	
			pClient->SendPackage(&res,sizeof(MODI_GS2C_Notify_AddRelationRes));
			MODI_ASSERT(0);
			return enPHandler_OK;
		}
		MODI_DBRelationInfo dbrel;
		dbrel.Set( res.m_info );
		dbrel.m_bIsValid = 1;
		pRel->Init( dbrel );

		if( !relList.AddRelation( pRel , optType , res.m_byResult ) )
		{
			MODI_RelationPool::Delete( pRel );
			bOk = false;
		}
		res.m_info.m_RelationType = pRel->GetInfo().m_RelationType;
		
		if( bOk )
		{
			/// 需要保存这个社会关系
			pRel->SetNeedSave();
			
			if( pTargetClient )
			{
				// 通知被加的人，有人添加你为 好友/黑名单/偶像
				MODI_GS2C_Notify_RelationCU notify;
				notify.m_byType = optType;
				safe_strncpy( notify.m_szName  , pClient->GetRoleName() , sizeof(notify.m_szName) );
				pTargetClient->SendPackage( &notify , sizeof(notify) );

				if( optType == enRelOpt_AddIdol )
				{
					pTargetClient->OnBeAddIdolBySB( pClient->GetSexType() );
				}
				else if(optType == enRelOpt_AddB)
				{
					pTargetClient->BlockOptDeal(enRelOpt_AddB);
				}
			}
			else 
			{
				if( optType == enRelOpt_AddIdol )
				{
					MODI_ScriptMailData sd;
					sd.m_ScriptID = kScriptMail_IncIdolCount;
					sd.GetArgM_IncIdolCount().bySex = pClient->GetSexType();
					safe_strncpy( sd.GetArgM_IncIdolCount().szName , pClient->GetRoleName() , sizeof(sd.GetArg_IncIdolCount().szName) );

					MODI_MailInfo mail;
					mail.m_Type = kServerMail;
					mail.m_State = 0;
					safe_strncpy( mail.m_szSender , SYS_MAIL_SENDER , sizeof(mail.m_szSender) );
					safe_strncpy( mail.m_szRecevier ,  pMsg->m_szTargetName , sizeof(mail.m_szRecevier) );
					MODI_MailCore::SendScriptMail( mail , sd );
				}
				else if( optType == enRelOpt_AddB )
				{
					MODI_ScriptMailData sd;
					sd.m_ScriptID = kScriptMail_BlockOpt;
					sd.GetArgM_BlockOpt().m_byType = (BYTE)enRelOpt_AddB;///被拉黑
					
					MODI_MailInfo mail;
					mail.m_Type = kServerMail;
					mail.m_State = 0;
					strncpy( mail.m_szSender , SYS_MAIL_SENDER , strlen(SYS_MAIL_SENDER) );
					strncpy( mail.m_szRecevier ,  pMsg->m_szTargetName , strlen(pMsg->m_szTargetName) );
					MODI_MailCore::SendScriptMail( mail , sd );
				}
			}
		}
	}

	pClient->SendPackage( &res , sizeof(res) );

	// 新加联系人的当前状态
	if( bOk && pTargetClient && pClient->IsNeedNotifyWithSbLogin( pTargetClient->GetCharID() ) )
	{
		pClient->OnRelationLogin( pTargetClient );
		
		/// 增加到反向管理器
		pTargetClient->AddReverseRelation(pClient->GetCharID());
	}

	return enPHandler_OK;
}


/** 
 * @brief 修改和某人的社会关系
 * 
 */
int MODI_PackageHandler_c2zs::ModifyRelation_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_ModifyRelation , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_ModifyRelation * pMsg = (const MODI_C2GS_Request_ModifyRelation *)(pt_null_cmd);
	
	// 检查参数有效性
	MODI_CHARID charid = GUID_LOPART(pMsg->m_guid);
	if( charid == INVAILD_CHARID )
		return enPHandler_Kick;
	if( (pMsg->m_optType > enRelOpt_Begin &&
			pMsg->m_optType < enRelOpt_End ) == false )
		return enPHandler_Kick;
	if( charid == pClient->GetCharID() )
		return enPHandler_Kick;

	// 是否存存在该联系人
	MODI_RelationList & relList = pClient->GetRelationList();
	MODI_GS2C_Notify_ModifyRelationRet res;
	res.m_byResult = enRelationRetT_Ok;
	res.m_guid = pMsg->m_guid;
	bool is_idol = false;
	MODI_Relation * before_modfiy = relList.FindRelation( charid );
	if( before_modfiy && before_modfiy->IsIdol() )
	{
		is_idol = true;
	}

	MODI_Relation * pRel = relList.ModfiyRel( charid , pMsg->m_optType , res.m_byResult );

	if( res.m_byResult == enRelationRetT_Ok && pRel )
	{
		res.m_newType = pRel->GetInfo().m_RelationType;
		// 是否还有关系
		if( pRel->IsHasRelation() == false )
		{
			relList.DelRelation( pRel->GetCharID() );

			/// 删除反向关系
			MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
			MODI_OnlineClient * pTargetClient = pMgr->FindByGUID( charid );
			if( pTargetClient )
			{
				pTargetClient->RemoveReverseRelation(pClient->GetCharID());
			}
			
		}

		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		MODI_GS2C_Notify_RelationCU notify;
		safe_strncpy( notify.m_szName  , pClient->GetRoleName() ,sizeof(notify.m_szName) );
		notify.m_byType = pMsg->m_optType;

		/// 该社会关系需要保存
		pRel->SetNeedSave();
		
		// 目标对象是否在线
		MODI_OnlineClient * pTargetClient = pMgr->FindByGUID( charid );
		if( pTargetClient )
		{
			pTargetClient->SendPackage( &notify , sizeof(notify) );
			if( pMsg->m_optType == enRelOpt_AddIdol )
			{
				pTargetClient->OnBeAddIdolBySB( pClient->GetSexType() );
			}
			else if( pMsg->m_optType == enRelOpt_DelIdol || ( pMsg->m_optType == enRelOpt_AddB && is_idol ) )
			{
				pTargetClient->OnBeDelIdolBySB( pClient->GetSexType() );
				if(pMsg->m_optType == enRelOpt_AddB)
				{
					pTargetClient->BlockOptDeal(enRelOpt_AddB);
				}
			}
			else if(pMsg->m_optType == enRelOpt_AddB )
			{
				pTargetClient->BlockOptDeal(enRelOpt_AddB);
			}
			/// 解黑
			else if(pMsg->m_optType == enRelOpt_DelB )
			{
				pTargetClient->BlockOptDeal(enRelOpt_DelB);
			}
		}
		else 
		{
			// 离线处理，邮件
			if( pMsg->m_optType == enRelOpt_AddIdol )
			{
				MODI_ScriptMailData sd;
				sd.m_ScriptID = kScriptMail_IncIdolCount;
				sd.GetArgM_IncIdolCount().bySex = pClient->GetSexType();
				safe_strncpy( sd.GetArgM_IncIdolCount().szName , pClient->GetRoleName() , sizeof(sd.GetArg_IncIdolCount().szName) );

				MODI_MailInfo mail;
				mail.m_Type = kServerMail;
				mail.m_State = 0;
				safe_strncpy( mail.m_szSender , SYS_MAIL_SENDER , sizeof(mail.m_szSender) );
				safe_strncpy( mail.m_szRecevier ,  pRel->GetName() , sizeof(mail.m_szRecevier) );
				MODI_MailCore::SendScriptMail( mail , sd );
			}
			else if( pMsg->m_optType == enRelOpt_DelIdol || ( pMsg->m_optType == enRelOpt_AddB && is_idol ) )
			{	
				MODI_ScriptMailData sd;
				sd.m_ScriptID = kScriptMail_DecIdolCount;
				sd.GetArgM_DecIdolCount().bySex = pClient->GetSexType();

				MODI_MailInfo mail;
				mail.m_Type = kServerMail;
				mail.m_State = 0;
				safe_strncpy( mail.m_szSender , SYS_MAIL_SENDER , sizeof(mail.m_szSender) );
				safe_strncpy( mail.m_szRecevier ,  pRel->GetName() , sizeof(mail.m_szRecevier) );
				MODI_MailCore::SendScriptMail( mail , sd );
				if( pMsg->m_optType == enRelOpt_AddB)
				{
					MODI_ScriptMailData sd;
					sd.m_ScriptID = kScriptMail_BlockOpt;
					sd.GetArgM_BlockOpt().m_byType = (BYTE)enRelOpt_AddB;
					
					MODI_MailInfo mail;
					mail.m_Type = kServerMail;
					mail.m_State = 0;
					strncpy( mail.m_szSender , SYS_MAIL_SENDER , strlen(SYS_MAIL_SENDER) );
					strncpy( mail.m_szRecevier ,  pRel->GetName() , ROLE_NAME_MAX_LEN );
					MODI_MailCore::SendScriptMail( mail , sd );
				}
			}
			else if(pMsg->m_optType == enRelOpt_AddB || pMsg->m_optType == enRelOpt_DelB)
			{
				MODI_ScriptMailData sd;
				sd.m_ScriptID = kScriptMail_BlockOpt;
				if(pMsg->m_optType == enRelOpt_AddB)
				{
					sd.GetArgM_BlockOpt().m_byType = (BYTE)enRelOpt_AddB;
				}
				else
				{
					sd.GetArgM_BlockOpt().m_byType = (BYTE)enRelOpt_DelB;
				}

				MODI_MailInfo mail;
				mail.m_Type = kServerMail;
				mail.m_State = 0;
				strncpy( mail.m_szSender , SYS_MAIL_SENDER , strlen(SYS_MAIL_SENDER) );
				strncpy( mail.m_szRecevier ,  pRel->GetName() , ROLE_NAME_MAX_LEN );
				MODI_MailCore::SendScriptMail( mail , sd );
			}
		}

		relList.OnListChanged();
	}

	pClient->SendPackage( &res , sizeof(res) );
	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::CanYouMarryMe_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_CanyouMarryMe , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_CanyouMarryMe * pMsg = (const MODI_C2GS_Request_CanyouMarryMe *)(pt_null_cmd);
	
	// 检查参数有效性
	MODI_CHARID charid = GUID_LOPART(pMsg->m_guid);
	if( charid == INVAILD_CHARID )
		return enPHandler_Kick;

	
	MODI_RelationList & relList = pClient->GetRelationList();
	MODI_Relation * pRel = relList.FindRelation( charid );
	if( !pRel )
		return enPHandler_Kick;

	// 是否有某种道具
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pTargetClient = pMgr->FindByGUID( charid );

	MODI_GS2C_Notify_AnserMarry msg;
	// 目标对象是否在线
	if( !pTargetClient )
	{
		msg.m_ret = enRelationRetT_NotOnline;
		safe_strncpy( msg.m_szName ,  pRel->GetName() , sizeof(msg.m_szName));
		pClient->SendPackage( &msg, sizeof(msg) );
		return enPHandler_OK;
	}

	// 之前是否有请求
	if( pClient->IsReqingHoney() )
	{
		msg.m_ret = enRelationRetT_Reqing_Honeny;
		pClient->SendPackage( &msg, sizeof(msg) );
		return enPHandler_OK;
	}

	// 判断是否可以做恋人或者夫妻
	if( pMsg->m_bLover )
	{
		msg.m_ret = pClient->IsCanDoMyGF( pTargetClient );
	}
	else
	{
		msg.m_ret = pClient->IsCanMarryMe( pTargetClient );
	}

	if( msg.m_ret != enRelationRetT_Ok )
	{
		safe_strncpy( msg.m_szName ,  pRel->GetName() , sizeof(msg.m_szName) );
		pClient->SendPackage( &msg, sizeof(msg) );
		return enPHandler_OK;
	}

	// 请求对方
	MODI_GS2C_Notify_SBAskYouToMarryHim notify;
	notify.m_guid = pClient->GetGUID();
	safe_strncpy( notify.m_szName , pClient->GetRoleName() , sizeof(notify.m_szName) );
	pTargetClient->SendPackage( &notify , sizeof(notify) );
	pClient->OnBeginReqCanDoMyHoney( charid , pMsg->m_bLover );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::AnserMarry_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_AnserMarry , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_AnserMarry * pMsg = (const MODI_C2GS_Request_AnserMarry *)(pt_null_cmd);
	
	// 检查参数有效性
	MODI_CHARID charid = GUID_LOPART(pMsg->m_guid);
	if( charid == INVAILD_CHARID )
		return enPHandler_Kick;

	MODI_RelationList & relList = pClient->GetRelationList();
	MODI_Relation * pRel = relList.FindRelation( charid );
	if( !pRel )
		return enPHandler_Kick;

	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pTargetClient = pMgr->FindByGUID( charid );

	MODI_GS2C_Notify_AnserMarry msg;
	safe_strncpy( msg.m_szName ,  pRel->GetName() , sizeof(msg.m_szName) );

	// 目标对象是否在线
	if( !pTargetClient )
	{
		msg.m_ret = enRelationRetT_NotOnline;
		pClient->SendPackage( &msg, sizeof(msg) );
		return enPHandler_OK;
	}

	// 之前是否有请求
	if( !pTargetClient->IsReqingHoney() )
		return enPHandler_OK;
	if( !pTargetClient->IsMyReqHoney( pClient->GetCharID() ) )
		return enPHandler_OK;

	// 判断是否可以做恋人或者夫妻
	if( pTargetClient->IsReqMarry() )
	{
		msg.m_ret = pClient->IsCanMarryMe( pTargetClient );
	}
	else
	{
		msg.m_ret = pClient->IsCanDoMyGF( pTargetClient );
	}
	
	if( msg.m_ret != enRelationRetT_Ok )
	{
		pClient->SendPackage( &msg, sizeof(msg) );
		return enPHandler_OK;
	}

	// 通知求婚者
	pTargetClient->SendPackage( &msg , sizeof(msg) );
	pTargetClient->OnEndReqCanDoMyHoney();

	// 广播，某2个人结婚了
	MODI_GS2C_Notify_SBHoneyStateChanged bmsg;
	bmsg.m_oldtype = MODI_HoneyData::enHoneyT_None;
	if( pTargetClient->IsReqMarry() )
		bmsg.m_newtype = MODI_HoneyData::enHoneyT_Married;
	else 
		bmsg.m_newtype = MODI_HoneyData::enHoneyT_Lover;

	if( pClient->GetSexType() == enSexBoy )
	{
		safe_strncpy( bmsg.m_szBoyName , pClient->GetRoleName() , sizeof(bmsg.m_szBoyName) );
		safe_strncpy( bmsg.m_szGrilName , pTargetClient->GetRoleName() , sizeof(bmsg.m_szGrilName) );
	}
	else
	{
		safe_strncpy( bmsg.m_szBoyName , pTargetClient->GetRoleName() , sizeof(bmsg.m_szBoyName) );
		safe_strncpy( bmsg.m_szGrilName , pClient->GetRoleName() , sizeof(bmsg.m_szGrilName) );
	}
	pMgr->Broadcast( &bmsg , sizeof(bmsg) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::ReqRoleDetailInfo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_RoleDetailInfo , cmd_size , 0 , 0 , pClient );
	const MODI_C2GS_Request_RoleDetailInfo * pMsg = (const MODI_C2GS_Request_RoleDetailInfo *)(pt_null_cmd);
	
	size_t nLen = 0;
	if( MODI_GameHelpFun::StrSafeCheck( pMsg->m_szName , sizeof(pMsg->m_szName) , &nLen ) == false )
		return enPHandler_Kick;
	if( !nLen )
		return enPHandler_Kick;

	MODI_GS2C_Notify_RoleDetailInfo notify;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * p = pMgr->FindByName( pMsg->m_szName );
	if( p )
	{
		// 在线
		notify.m_namecard.m_avatar = p->GetFullData().m_roleInfo.m_normalInfo.m_avatarData;
		notify.m_namecard.m_byLevel = p->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel;
		notify.m_namecard.m_bySex = p->GetFullData().m_roleInfo.m_baseInfo.m_ucSex;
		safe_strncpy( notify.m_namecard.m_szName  , 
				p->GetFullData().m_roleInfo.m_baseInfo.m_cstrRoleName , 
				sizeof(p->GetFullData().m_roleInfo.m_baseInfo.m_cstrRoleName) );
		notify.m_namecard.m_RoleDetail = p->GetFullData().m_roleInfo.m_detailInfo;
		notify.m_namecard.m_nDefaultAvatarIdx = p->GetFullData().m_roleInfo.m_normalInfo.m_nDefavatarIdx;
		notify.m_namecard.m_wdChenghao = p->GetFullData().m_roleInfo.m_normalInfo.m_wdChenghao;

		if(p->IsHaveFamily())
		{
			MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(p->GetFamilyID());
			if(p_family)
			{
				MODI_FamilyMember * p_member = p_family->FindMember(p->GetGUID());
				if(p_member)
	   			{
					
					notify.m_namecard.m_stFamilyCard.m_stGuid = p_member->GetGuid();
					notify.m_namecard.m_stFamilyCard.m_stPosition = p_member->GetPosition();
					memset(notify.m_namecard.m_stFamilyCard.m_cstrFamilyName, 0, sizeof(notify.m_namecard.m_stFamilyCard.m_cstrFamilyName));
					strncpy(notify.m_namecard.m_stFamilyCard.m_cstrFamilyName,
							p_member->GetFamilyName(), sizeof(notify.m_namecard.m_stFamilyCard.m_cstrFamilyName) -1);

					notify.m_namecard.m_stFamilyCard.m_wLevel = p_family->GetFamilyLevel();
				}
				else
				{
					MODI_ASSERT(0);
				}
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
		
		pClient->SendPackage( &notify , sizeof(notify) );
		return enPHandler_OK;
	}

	MODI_OfflineUserMgr * pOfflineMgr = MODI_OfflineUserMgr::GetInstancePtr();
	MODI_OfflineUser * p2 = pOfflineMgr->FindByName( pMsg->m_szName );
	if( p2 )
	{
		notify.m_namecard.m_avatar = p2->GetAvatarData();
		notify.m_namecard.m_byLevel = p2->GetLevel();
		notify.m_namecard.m_bySex = p2->GetSex();
		safe_strncpy( notify.m_namecard.m_szName , p2->GetName() , sizeof(notify.m_namecard.m_szName) );
		notify.m_namecard.m_RoleDetail = p2->GetDetailData();
		notify.m_namecard.m_nDefaultAvatarIdx = p2->GetDefaultAvatar();
		notify.m_namecard.m_wdChenghao = p2->GetChenghao();
		notify.m_namecard.m_stFamilyCard = p2->GetFamilyCard();
		pClient->SendPackage( &notify , sizeof(notify) );
		return enPHandler_OK;
	}

	MODI_DBProxyClientTask * pDB = GetApp()->GetDBInterface( 0 );
	if( pDB )
	{
		MODI_S2RDB_Request_RoleDetailInfo reqRoleInfo;
		reqRoleInfo.m_reqCharid = pClient->GetCharID();
		safe_strncpy( reqRoleInfo.m_szName  , pMsg->m_szName , sizeof(reqRoleInfo.m_szName) );
		pDB->SendCmd( &reqRoleInfo , sizeof(reqRoleInfo) );
	}
	else 
	{
		Global::logger->info("[%s] client<%s> req character<%s> 's detailInfo faild , can't get db .",
				RELATION_MODULE,
				pClient->GetRoleName(),
				pMsg->m_szName);
	}

	return enPHandler_OK;
}


int MODI_PackageHandler_c2zs::PrivateChatByName_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
int i=0;
int bad=0;
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_PrivateChat_ByName , cmd_size , 0, 0, pClient );
	const MODI_C2GS_Request_PrivateChat_ByName * pMsg = (const MODI_C2GS_Request_PrivateChat_ByName *)(pt_null_cmd);
	
	size_t nLen = 0;
	if( !MODI_GameHelpFun::StrSafeCheck( pMsg->m_cstrName , sizeof(pMsg->m_cstrName) , &nLen  ) )
		return enPHandler_Kick;
	if( !nLen )
		return enPHandler_Kick;

	MODI_GS2C_Notify_SBPrivateChatWithYou notify;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pTarget = pMgr->FindByName( pMsg->m_cstrName );
	if( !pTarget )
	{
		// 目标不在线
		MODI_GS2C_Notify_PrivateChatRes res;
		res.m_res = MODI_GS2C_Notify_PrivateChatRes::enOffline;
		safe_strncpy( res.m_szName  , pMsg->m_cstrName , sizeof(res.m_szName) );
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	MODI_Relation * pRel = pTarget->GetRelationList().FindRelation( pClient->GetCharID() );
	if( pRel && pRel->IsBlack() )
	{
		// 在对方黑名单中
		MODI_GS2C_Notify_PrivateChatRes res;
		res.m_res = MODI_GS2C_Notify_PrivateChatRes::enInBlack;
		safe_strncpy( res.m_szName  , pMsg->m_cstrName , sizeof(res.m_szName) );
		pClient->SendPackage( &res , sizeof(res) );
		return enPHandler_OK;
	}

	memcpy( notify.m_cstrContents , pMsg->m_cstrContents , sizeof(pMsg->m_cstrContents) );
	safe_strncpy( notify.m_szName  , pClient->GetRoleName() , sizeof(notify.m_szName) ); 
i=0;
bad=0;
while (notify.m_cstrContents[i]) {
  if(!isprint(notify.m_cstrContents[i])) {
    notify.m_cstrContents[i]=32;
    bad=1;
  }
  i++;
}
if(bad>0) {
  Global::logger->info("[EXPLOIT] POSSIBLE BAD GUY: %s", notify.m_szName);
}
Global::logger->info("[PM_Chat] %s->%s: %s", notify.m_szName, pTarget->GetRoleName(), notify.m_cstrContents);
	pTarget->SendPackage( &notify , sizeof(notify) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::SendMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	const MODI_C2GS_Request_SendMail * pMsg = (const MODI_C2GS_Request_SendMail *)(pt_null_cmd);
	CHECK_PACKAGE( MODI_C2GS_Request_SendMail , cmd_size , 0 , 0 , pClient );
	
	// 类型检查
	if( pMsg->m_sendMail.m_Type != kTextMail &&
			pMsg->m_sendMail.m_Type != kAttachmentItemMail &&
			pMsg->m_sendMail.m_Type != kAttachmentMoneyMail )
		return enPHandler_Kick;

	// 字符串检查
	size_t nReceverLen = 0;
	if( !MODI_GameHelpFun::StrSafeCheck( pMsg->m_sendMail.m_szRecevier , 
				sizeof(pMsg->m_sendMail.m_szRecevier) , 
				&nReceverLen  ) )
		return enPHandler_Kick;
	if( !nReceverLen )
		return enPHandler_Kick;

//	size_t nCaptionLen = 0;
//	if( !MODI_GameHelpFun::StrSafeCheck( pMsg->m_sendMail.m_szCaption , 
//				sizeof(pMsg->m_sendMail.m_szCaption) , 
//				&nCaptionLen ) )
//		return enPHandler_Kick;
//	if( !nCaptionLen )
//		return enPHandler_Kick;
//
	unsigned int nContentSize = 0;
	if( !MODI_GameHelpFun::StrSafeLength(pMsg->m_sendMail.m_szContents,
				sizeof(pMsg->m_sendMail.m_szContents), 
				nContentSize ) )
		return enPHandler_Kick;
	if( nContentSize > MAIL_CONTENTS_MAX_LEN )
		return enPHandler_Kick;

	// 不能给自己发
	if( strncmp( pClient->GetRoleName() , pMsg->m_sendMail.m_szRecevier , nReceverLen ) == 0 )
	{
		return enPHandler_OK;
	}

	// 发送邮件
	MODI_MailCore::ClientSendMailToClient( pClient ,  pMsg->m_sendMail );

	//  附件数据检查
	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::DelMail_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	const MODI_C2GS_Request_DelMail * pMsg = (const MODI_C2GS_Request_DelMail *)(pt_null_cmd);
	CHECK_PACKAGE( MODI_C2GS_Request_DelMail , cmd_size , 0 , 0 , pClient );
	
	if( pMsg->m_mailid.IsInvalid() )
		return enPHandler_Kick;

	MODI_GS2C_Notify_DelMailRes res;
	res.m_mailid = pMsg->m_mailid;
	res.m_result = MODI_GS2C_Notify_DelMailRes::kMailNotExist;

	MODI_Mail * pMail = pClient->GetUserMail(pMsg->m_mailid);
	if( !pMail )
	{
		Global::logger->debug("[%s] client<charid=%u,name=%s> request delmail . but can't find mail" , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		res.m_result = MODI_GS2C_Notify_DelMailRes::kMailNotExist;
	}
	else 
	{
		if( pMail->IsLockAttachment() )
		{
			Global::logger->debug("[%s] client<charid=%u,name=%s> request delmail . but mail is lock" , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
			res.m_result = MODI_GS2C_Notify_DelMailRes::kMailLock;
		}
		else 
		{
			pClient->DelUserMail(pMsg->m_mailid);
			res.m_result = MODI_GS2C_Notify_DelMailRes::kSuccessful;
		}
	}

	pClient->SendPackage( &res, sizeof(res) );

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::SetMailRead_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	const MODI_C2GS_Request_SetReadMail * pMsg = (const MODI_C2GS_Request_SetReadMail *)(pt_null_cmd);
	CHECK_PACKAGE( MODI_C2GS_Request_SetReadMail , cmd_size , 0 , 0 , pClient );
	
	if( pMsg->m_mailid.IsInvalid() )
		return enPHandler_Kick;
	
	MODI_Mail * pMail = pClient->GetUserMail( pMsg->m_mailid  );
	if( !pMail )
	{
		Global::logger->info("[%s] client<charid=%u,name=%s> send setMailRead . but can't find mail" , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_OK;
	}

	if( pMail->IsRead() == false )
	{
		pMail->SetReadState();
		Global::logger->debug("[%s] set mail is read.",
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
	}
	else 
	{
		Global::logger->info("[%s] mail is already read.but client<charid=%u,name=%s> send setMailRead . " , 
				MAIL_MODULE ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
	}

	return enPHandler_OK;
}
    
int MODI_PackageHandler_c2zs::GetAttachment_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = (MODI_OnlineClient *)(pObj);
	const MODI_C2GS_Request_GetAttachment * pMsg = (const MODI_C2GS_Request_GetAttachment *)(pt_null_cmd);
	CHECK_PACKAGE( MODI_C2GS_Request_GetAttachment , cmd_size , 0 , 0 , pClient );
	
	if( pMsg->m_mailid.IsInvalid() )
		return enPHandler_Kick;

	MODI_MailCore::GetAttachment( pClient , pMsg->m_mailid );

	return enPHandler_OK;
}

int MODI_PackageHandler_c2zs::MODI_PackageHandler_c2zs::GMCommand_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_GMCmd , cmd_size , 0 , 0 , pClient );

	if( pClient->GetFullData().m_roleExtraData.m_iGMLevel == 0 )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> use gm cmd.but no gm powr. " , SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_OK;
	}

	// to do ..
	MODI_GMCmdHandler_zs  * p = MODI_GMCmdHandler_zs::GetInstancePtr();
	if( p )
	{
		const MODI_C2GS_Request_GMCmd * pReq = (const MODI_C2GS_Request_GMCmd *)(pt_null_cmd);
		p->SetClient( pClient );
		p->DoHandleCommand( pReq->m_szCmd );
		p->SetClient(0);
	}

	return enPHandler_OK;
}
	
int MODI_PackageHandler_c2zs::SystemBroast_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);

	Global::logger->debug("[%s] client<charid=%u,name=%s> send system broast." , SVR_TEST ,
			pClient->GetCharID(),
			pClient->GetRoleName() );

	if( !pClient->GetFullData().m_roleExtraData.m_iGMLevel )
		return enPHandler_OK;

	size_t nPackageMinSize = sizeof(MODI_C2GS_Request_SystemBroast);
	if( cmd_size < nPackageMinSize )
	{
		return enPHandler_Kick;
	}
	const MODI_C2GS_Request_SystemBroast * pReq = (const MODI_C2GS_Request_SystemBroast *)( pt_null_cmd );
	const unsigned int per_size = sizeof(char);
	unsigned int nSafePackageSize =  GET_PACKAGE_SIZE( MODI_C2GS_Request_SystemBroast , 
			per_size ,  
			pReq->m_nSize );
	if( nSafePackageSize != cmd_size )
	{
		return enPHandler_Kick;
	}	

	if( !pReq->m_nSize )
		return enPHandler_Kick;

	unsigned int nLen = 0;
	if( !MODI_GameHelpFun::StrSafeLength( pReq->m_pContents ,  
				pReq->m_nSize,  
				nLen ) )
	{
		Global::logger->debug("[%s] client<charid=%u,name=%s> send system broast.invalid contents" , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName() );
		return enPHandler_Kick;
	}

    char szPackageBuf[Skt::MAX_USERDATASIZE];
    memset(szPackageBuf, 0, sizeof(szPackageBuf));
    MODI_GS2C_Notify_SystemBroast * p_notify = (MODI_GS2C_Notify_SystemBroast *) (szPackageBuf);
    AutoConstruct(p_notify);
	p_notify->m_nSize = nLen;
	memcpy( p_notify->m_pContents  , 
			pReq->m_pContents ,
			nLen );
	int iSendSize = sizeof(MODI_GS2C_Notify_SystemBroast) + p_notify->m_nSize;
	
	// 广播全区
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	pMgr->Broadcast( p_notify , iSendSize );

	return enPHandler_OK;
}


int MODI_PackageHandler_c2zs::WorldChat_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
int i=0;
int bad=0;
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;

	const MODI_C2GS_Request_RangeChat * pReq = ( const MODI_C2GS_Request_RangeChat *)(pt_null_cmd);
	if( pReq->m_byChannel == enChatChannel_World )
	{
		MODI_GS2C_Notify_WorldChat msg;
		msg.m_color = pReq->m_color;
		safe_strncpy( msg.m_szContents , pReq->m_cstrContents , sizeof(msg.m_szContents) );
		safe_strncpy( msg.m_szName , pClient->GetRoleName() , sizeof(msg.m_szName) );
		safe_strncpy( msg.m_szServerName , pClient->GetServerName() , sizeof(msg.m_szServerName) );
i=0;
bad=0;
while (msg.m_szContents[i]) {
  if(!isprint(msg.m_szContents[i])) {
    msg.m_szContents[i]=32;
    bad=1;
  }
  i++;
}
if(bad>0) {
  Global::logger->info("[EXPLOIT] POSSIBLE BAD GUY: %s", msg.m_szName);
}
		// 广播全区
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		pMgr->Broadcast( &msg , sizeof(msg) );
		Global::logger->info("[Server_Speaker] %s: %s", msg.m_szName, msg.m_szContents);
	}
	else if( pReq->m_byChannel == enChatChannel_Fensi )
	{
		MODI_GS2C_Notify_FensiChat msg;
		msg.m_color = pReq->m_color;
		safe_strncpy( msg.m_szContents , pReq->m_cstrContents , sizeof(msg.m_szContents) );
		safe_strncpy( msg.m_szName , pClient->GetRoleName() , sizeof(msg.m_szName) );
		safe_strncpy( msg.m_szServerName , pClient->GetServerName() , sizeof(msg.m_szServerName) );
i=0;
bad=0;
while (msg.m_szContents[i]) {
  if(!isprint(msg.m_szContents[i])) {
    msg.m_szContents[i]=32;
    bad=1;
  }
  i++;
}
if(bad>0) {
  Global::logger->info("[EXPLOIT] POSSIBLE BAD GUY: %s", msg.m_szName);
}
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		pMgr->BroadcastFensi( pClient , &msg , sizeof(msg) );
		Global::logger->info("[Idol_Speaker] %s: %s", msg.m_szName, msg.m_szContents);
	}
	/// 家族聊天
	else if( pReq->m_byChannel  == enChatChannel_Team)
	{
		MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(pClient->GetFamilyID());
		if(p_family)
		{
			MODI_GS2C_Notify_RangeChat_ByName  chat;
			strncpy(chat.m_cstrName,pClient->GetRoleName(),sizeof(chat.m_cstrName) - 1);
			chat.m_byChannel = enChatChannel_Team;
			strncpy(chat.m_cstrContents,pReq->m_cstrContents,sizeof(chat.m_cstrContents) - 1);
i=0;
bad=0;
while (chat.m_cstrContents[i]) {
  if(!isprint(chat.m_cstrContents[i])) {
    chat.m_cstrContents[i]=32;
    bad=1;
  }
  i++;
}
if(bad>0) {
  Global::logger->info("[EXPLOIT] POSSIBLE BAD GUY: %s", chat.m_cstrName);
}

			chat.m_color = pReq->m_color;
			p_family->BroadcastExRequest(&chat,sizeof(MODI_GS2C_Notify_RangeChat_ByName));
			Global::logger->info("[Family_Chat] %d, %s: %s", pClient->GetFamilyID(), chat.m_cstrName, chat.m_cstrContents);
		}
		else
		{
			Global::logger->warn("[Family Warn] client request family chat without a family <name=%s>",pClient->GetRoleName());
			MODI_ASSERT(0);
		}

	}
	
	return enPHandler_OK;
}


/** 
 * @brief 充值申请
 * 
 * @param pObj 申请充值的对象
 * @param pt_null_cmd 充值的数据
 *
 */
int MODI_PackageHandler_c2zs::RequestPayCmd_Handler( void * pObj , 
			const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
#ifdef _YLXK_CODE	
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	CHECK_PACKAGE( MODI_C2GS_Request_PayCmd , cmd_size , 0 , 0 , pClient );

	MODI_C2GS_Request_PayCmd * p_recv_cmd = (MODI_C2GS_Request_PayCmd *)pt_null_cmd;
	p_recv_cmd->m_client = pClient->GetCharID();
	p_recv_cmd->m_byServerid = pClient->GetServerID();

	MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
	if( !pBill || pBill->IsConnected() == false )
	{
		MODI_GS2C_Notify_PayResult send_cmd;
		pClient->SendPackage( &send_cmd, sizeof(send_cmd));
		Global::logger->warn("[pay_cmd] billservice not connect when client send paycmd");
	}
	else 
	{
		pBill->SendCmd(pt_null_cmd , cmd_size);
	}
#endif
	return enPHandler_OK;
}

int MODI_PackageHandler_c2zs::RequestRank_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;

	const MODI_C2GS_Request_Rank * pReq = ( const MODI_C2GS_Request_Rank *)(pt_null_cmd);

	// 参数检查
	if( (pReq->rank_type > kRank_Begin && pReq->rank_type < kRank_End ) == false )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> req rank<type=%d> , type is invalid." , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				pReq->rank_type ); 

		return enPHandler_OK;
	}

	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
	if( rank_core->IsValidRankSapce( pReq->rank_type , pReq->rank_begin , pReq->rank_end ) == false )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> req rank<type=%d> , rank space<begin=%u,end=%u> is invalid." , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				pReq->rank_type ,
				pReq->rank_begin,
				pReq->rank_end); 

		return enPHandler_OK;
	}

	QWORD rank_ver = rank_core->GetRankVersion( pReq->rank_type );

	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );

	int iSendSize = sizeof(MODI_GS2C_Notify_Rank);
	MODI_GS2C_Notify_Rank * msg  = (MODI_GS2C_Notify_Rank *)(szPackageBuf);
	AutoConstruct( msg );
	msg->rank_ver = rank_ver;
	msg->result = kReqRankResUnknowErr;
	msg->rank_begin = pReq->rank_begin;
	msg->rank_end = pReq->rank_end;
	msg->rank_type = pReq->rank_type;

	if( pReq->rank_ver  == rank_ver )
	{
		msg->result = kReqRankResUselocalData;
		pClient->SendPackage( msg , iSendSize );
	}
	else 
	{
		msg->result = kReqRankResOk;
		int get_rank_count = msg->rank_end - msg->rank_begin + 1;

		switch( pReq->rank_type )
		{
		case kRank_Level:
			{
				MODI_Rank_Level * rank_data = (MODI_Rank_Level *)(msg->ranks);
				if( rank_core->GetLevelRank( msg->rank_begin , msg->rank_end ,  rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_Level) * get_rank_count;
				}
			}
		break;

		case kRank_Renqi:
			{
				MODI_Rank_Renqi * rank_data = (MODI_Rank_Renqi *)(msg->ranks);
				if( rank_core->GetRenqiRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_Renqi) * get_rank_count;
				}
			}
		break;

		case kRank_Consume:
			{
				MODI_Rank_ConsumeRMB * rank_data = (MODI_Rank_ConsumeRMB *)(msg->ranks);
				if( rank_core->GetConsumeRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_ConsumeRMB) * get_rank_count;
				}
			}
		break;

		case kRank_HighestScore:
			{
				MODI_Rank_HighestScore * rank_data = (MODI_Rank_HighestScore *)(msg->ranks);
				if( rank_core->GetHighestScoreRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_HighestScore) * get_rank_count;
				}
			}
		break;

		case kRank_HighestPrecision:
			{
				MODI_Rank_HighestPrecision * rank_data = (MODI_Rank_HighestPrecision *)(msg->ranks);
				if( rank_core->GetHighestPrecisionRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_HighestPrecision) * get_rank_count;
				}
			}
		break;

		case kRank_KeyScore:
			{
				MODI_Rank_HighestScore * rank_data = (MODI_Rank_HighestScore *)(msg->ranks);
				if( rank_core->GetKeyHighestScoreRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_HighestScore) * get_rank_count;
				}
			}
		break;

		case kRank_KeyPrecision:
			{
				MODI_Rank_HighestPrecision * rank_data = (MODI_Rank_HighestPrecision *)(msg->ranks);
				if( rank_core->GetKeyHighestPrecisionRank( msg->rank_begin , msg->rank_end , rank_data ) )
				{
					iSendSize += sizeof(MODI_Rank_HighestPrecision) * get_rank_count;
				}
			}
		break;
		default:
			{
				MODI_ASSERT(0);
			}
		break;
		}

		pClient->SendPackage( msg , iSendSize );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_c2zs::RequestSpecificByRankNo_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;

	const MODI_C2GS_Request_SpecificRank * pReq =  (const MODI_C2GS_Request_SpecificRank *)(pt_null_cmd);

	// 参数检查
	if( (pReq->rank_type > kRank_Begin && pReq->rank_type < kRank_End ) == false )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> req specific by rank No.<type=%d> , type is invalid." , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				pReq->rank_type ); 
		return enPHandler_OK;
	}

	MODI_RankCore * rank_core = MODI_RankCore::GetInstancePtr();
	if( rank_core->IsValidRankSapce( pReq->rank_type ,  1 , pReq->rank_no ) == false )
	{
		Global::logger->warn("[%s] client<charid=%u,name=%s> req specific by rank No.<type=%d> , rank No<%u> is invalid." , 
				SVR_TEST ,
				pClient->GetCharID(),
				pClient->GetRoleName(),
				pReq->rank_type ,
				pReq->rank_no );
		return enPHandler_OK;
	}

	MODI_CHARID charid = INVAILD_CHARID;
	char role_name[ROLE_NAME_MAX_LEN + 1];
	if( rank_core->GetCharIDAndNameByRankNo( pReq->rank_type , pReq->rank_no , charid , role_name ) )
	{
		MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
		MODI_OnlineClient * online = pMgr->FindByGUID(  charid );
		if( online )
		{
			rank_core->SyncOnlineClientSpecificRank( pClient ,  pReq->rank_type , pReq->rank_no , online );
		}
		else 
		{
			MODI_OfflineUserMgr * offline_mgr = MODI_OfflineUserMgr::GetInstancePtr();
			MODI_OfflineUser * offline = offline_mgr->FindByName( role_name );
			if( offline )
			{
				rank_core->SyncOfflineClientSpecificRank( pClient ,pReq->rank_type, pReq->rank_no , offline );
			}
			else 
			{
				MODI_DBProxyClientTask * dbproxy = MODI_DBProxyClientTask::FindServer( pClient->GetServerID() );
				if( dbproxy )
				{
					char szPackageBuf[Skt::MAX_USERDATASIZE];
					memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
					MODI_S2RDB_Request_RoleDetailInfo * req_role_detail = (MODI_S2RDB_Request_RoleDetailInfo *)(szPackageBuf);
					AutoConstruct( req_role_detail );
					req_role_detail->m_opt = kReqRankSpecific;
					req_role_detail->m_reqCharid = pClient->GetCharID();
					safe_strncpy( req_role_detail->m_szName , role_name , sizeof(req_role_detail->m_szName) );
					req_role_detail->m_nExtraSize = sizeof(MODI_ReqRoleDetail_RankSpecific_ExtraData);
					MODI_ReqRoleDetail_RankSpecific_ExtraData * rank_spec_extra = (MODI_ReqRoleDetail_RankSpecific_ExtraData *)(req_role_detail->m_szExtra);
					rank_spec_extra->rank_no = pReq->rank_no;
					rank_spec_extra->rank_type = pReq->rank_type;
					int send_size = sizeof(MODI_S2RDB_Request_RoleDetailInfo) + req_role_detail->m_nExtraSize; 
					dbproxy->SendCmd( req_role_detail , send_size );
				}
			}
		}
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_c2zs::RequestSelfRank_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;

	pClient->SyncSelfRank();
	return enPHandler_OK;
}


/** 
 * @brief 进入家族界面
 * 
 * @return 发送家族列表和自己的家族信息
 *
 */
int MODI_PackageHandler_c2zs::LoginFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;

	/// 通知家族列表
	MODI_FamilyManager::GetInstance().NotifyFamilyList(pClient);

	/// 有家族则通知家族信息
	if(pClient->IsHaveFamily())
	{
		MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(pClient->GetFamilyID());
		if(p_family)
		{
			MODI_FamilyMember * p_member = p_family->FindMember(pClient->GetGUID());
			if(p_member)
			{
				/// 把家族信息告知我自己
				p_member->SendFamilyToMe(enReason_LoginFamily);
			}
			else
			{
				Global::logger->error("[login_famiy] login fmaily but not find family member <name=%s,id=%u>", pClient->GetRoleName(), pClient->GetFamilyID());
				MODI_ASSERT(0);
			}
		}
		else
		{
			Global::logger->error("[login_famiy] login fmaily but not find family <name=%s,id=%u>", pClient->GetRoleName(), pClient->GetFamilyID());
			MODI_ASSERT(0);
		}
	}
	
	return enPHandler_OK;
}


/** 
 * @brief 创建家族
 * 
 */
int MODI_PackageHandler_c2zs::CreateFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
		return enPHandler_OK;
	
	const MODI_C2GS_Request_CreateFamily * p_recv_cmd = (const MODI_C2GS_Request_CreateFamily *)pt_null_cmd;

	MODI_FamilyBaseInfo create_info;
	strncpy(create_info.m_cstrFamilyName, p_recv_cmd->m_szName, sizeof(create_info.m_cstrFamilyName) - 1);
	strncpy(create_info.m_cstrXuanyan, p_recv_cmd->m_szXuanyan, sizeof(create_info.m_cstrXuanyan) - 1);
	strncpy(create_info.m_cstrPublic, p_recv_cmd->m_szPublic, sizeof(create_info.m_cstrPublic) - 1);
	
	/// 家族名字检查
	if(! MODI_FamilyManager::GetInstance().CheckFamilyName(create_info.m_cstrFamilyName))
	{
		pClient->RetFamilyResult(enFamilyRetT_Create_HaveName);
		pClient->IncMoney(CREATE_FAMILY_REQ_MONEY, MODI_ZS2S_Notify_OptMoney::enReasonCFFailed);
		return enPHandler_OK;
	}
	
	
	create_info.m_byTotem = p_recv_cmd->m_TotemID;
	
	if(!  MODI_FamilyManager::GetInstance().CreateFamily(create_info, pClient))
	{
		pClient->RetFamilyResult(enFamilyOptResult_Unknow);
		pClient->IncMoney(CREATE_FAMILY_REQ_MONEY, MODI_ZS2S_Notify_OptMoney::enReasonCFFailed);
	}
	
	return enPHandler_OK;
}


/** 
 * @brief 家族成员的操作
 * 
 */
int MODI_PackageHandler_c2zs::FamilyOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilyOpt * p_recv_cmd = (const MODI_C2GS_Request_FamilyOpt *)pt_null_cmd;
	enFamilyOptType opt_type = p_recv_cmd->m_enOptType;
 
#ifdef _FAMILY_DEBUG
	Global::logger->debug("[family_opt] family opt command <opt=%d>", opt_type);
#endif

	/// 申请加入家族
	if(opt_type == enFamilyOpt_Request)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().RequestFamily(pClient, p_recv_cmd->m_dwFamilyID);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
		return enPHandler_OK;
	}
	
	/// 取消申请
	else if(opt_type == enFamilyOpt_Cancel)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().CancelFamily(pClient);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
		return enPHandler_OK;
	}
	/// 离开家族
	else if(opt_type == enFamilyOpt_Leave)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().LeaveFamily(pClient);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
		return enPHandler_OK;
	}
	/// 解散家族
	else if(opt_type == enFamilyOpt_Free)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().FreeFamily(pClient);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
		return enPHandler_OK;
	}
	/// 升级
	else if(opt_type == enFamilyOpt_Level)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().FamilyLevel(pClient);
		if(result != enFamilyOptResult_None)
		{
			/// 等数据库返回结果
			pClient->RetFamilyResult(result);
			
		}
		return enPHandler_OK;
	}
	/// 打开家族信息共享
	else if(opt_type == enFamilyOpt_Open)
	{
		
	}
	/// 关闭家族信息
	else if(opt_type == enFamilyOpt_Close)
	{
		
	}
	return enPHandler_OK;
}


/** 
 * @brief 对家族成员的操作
 * 
 */
int MODI_PackageHandler_c2zs::RequestMemberOpt_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilyOptRequest * p_recv_cmd = (const MODI_C2GS_Request_FamilyOptRequest *)pt_null_cmd;
	MODI_GUID request_id = p_recv_cmd->m_stGuid;

#ifdef _FAMILY_DEBUG
	Global::logger->debug("[request_opt] opt request <request_guid=%u,opt=%d>", (QWORD)request_id, p_recv_cmd->m_byOpt);
#endif
	
	/// 接受
	if(p_recv_cmd->m_byOpt == 1 )
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().AcceptMember(pClient, request_id);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
	}
	
	///拒绝
	else if(p_recv_cmd->m_byOpt == 2)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().RefuseMember(pClient, request_id);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
	}
	
	return enPHandler_OK;
}


/** 
 * @brief 职位更改
 * 
 */
int MODI_PackageHandler_c2zs::FModifyPosition_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilyModifyPosition * p_recv_cmd = (const MODI_C2GS_Request_FamilyModifyPosition *)pt_null_cmd;
	MODI_GUID request_id = p_recv_cmd->m_stGuid;
	enFamilyPositionType modi_type = p_recv_cmd->m_enType;

	/// 开除
	if(modi_type == enPositionT_None)
	{
		MODI_FamilyManager::GetInstance().FireMember(pClient, request_id);
	}
	/// 族长转换
	else if(modi_type == enPositionT_Lead)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().ChangeMaster(pClient, request_id);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
	}
	/// 转换成副族长
	else if(modi_type == enPositionT_ViceLead)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().SetSlaver(pClient, request_id);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
	}
	/// 设置成普通成员
	else if(modi_type == enPositionT_Normal)
	{
		enFamilyOptResult result = MODI_FamilyManager::GetInstance().SetNormal(pClient, request_id);
		if(result != enFamilyOptResult_None)
		{	
			pClient->RetFamilyResult(result);
		}
	}
	return enPHandler_OK;
}



/** 
 * @brief 访问家族
 *
 *
 */
int MODI_PackageHandler_c2zs::VisitFamily_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	CHECK_PACKAGE(MODI_C2GS_Request_VisitFamily, cmd_size , 0 , 0 , pClient);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_VisitFamily * p_recv_cmd = (const MODI_C2GS_Request_VisitFamily *)pt_null_cmd;
	MODI_FamilyManager::GetInstance().VisitFamily(pClient, p_recv_cmd->m_nFamilyid);
	return enPHandler_OK;
}


/** 
 * @brief 家族名字唯一性检查
 *
 */
int MODI_PackageHandler_c2zs::FamilyNameCheck_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilyNameCheck * p_recv_cmd = (const MODI_C2GS_Request_FamilyNameCheck * )pt_null_cmd;
	char check_name[FAMILY_NAME_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_szName, sizeof(check_name) - 1);
	
	if(! MODI_FamilyManager::GetInstance().CheckFamilyName(check_name))
	{
		pClient->RetFamilyResult(enFamilyRetT_CheckN_Have);    /// 名字已经存在
	
	}
	
	pClient->RetFamilyResult(enFamilyRetT_CheckN_Succ);    /// 名字可以使用
	return enPHandler_OK;
}


/** 
 * @brief 修改宣言
 * 
 */
int MODI_PackageHandler_c2zs::ModiFamilyXuanyan_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilySetXuanyan * p_recv_cmd = (const MODI_C2GS_Request_FamilySetXuanyan * )pt_null_cmd;
	char check_name[FAMILY_XUANYAN_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_cstrXuanyan, sizeof(check_name) - 1);
	std::string xuan_yan = check_name;
	if(xuan_yan == "")
	{
		xuan_yan = "xuanyan";
	}
	strncpy(check_name, xuan_yan.c_str(), sizeof(check_name) - 1);
	
	MODI_FamilyManager::GetInstance().ModiXuanyan(pClient, check_name);

	return enPHandler_OK;
}


/** 
 * @brief 修改公告
 * 
 */
int MODI_PackageHandler_c2zs::ModiFamilyPublic_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}

	const MODI_C2GS_Request_FamilySetPublic * p_recv_cmd = (const MODI_C2GS_Request_FamilySetPublic * )pt_null_cmd;
	char check_name[FAMILY_PUBLIC_LEN + 1];
	memset(check_name, 0, sizeof(check_name));
	strncpy(check_name, p_recv_cmd->m_cstrPublic, sizeof(check_name) - 1);
	
	std::string xuan_yan = check_name;
	if(xuan_yan == "")
	{
		xuan_yan = "public";
	}
	strncpy(check_name, xuan_yan.c_str(), sizeof(check_name) - 1);

	MODI_FamilyManager::GetInstance().ModiPublic(pClient, check_name);
	return enPHandler_OK;
}


/** 
 * @brief 家族邀请
 * 
 */
int MODI_PackageHandler_c2zs::FamilyInvite_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}
	CHECK_PACKAGE(MODI_C2GS_Request_FamilyInvite, cmd_size , 0 , 0 , pClient);

	const MODI_C2GS_Request_FamilyInvite * p_recv_cmd = (const MODI_C2GS_Request_FamilyInvite * )pt_null_cmd;
	
	/// 不是族长
	if(!(pClient->IsFamilyMaster() || pClient->IsFamilySlaver()))
	{
		pClient->RetFamilyResult(enFamilyRetT_Free_Auth);
		return enPHandler_OK;
	}

	MODI_Family * p_family = MODI_FamilyManager::GetInstance().FindFamily(pClient->GetFamilyID());
	if(! p_family)
	{
		MODI_ASSERT(0);
		return enPHandler_OK;
	}
		
	if(p_family->IsFull())
	{
		std::ostringstream os;
		os<<"Your family members' number reached the toplimit";
		pClient->SendSystemChat(os.str().c_str(), os.str().size());
		return enPHandler_OK;
	}

	/// 对方是否在线
	char invite_name[ROLE_NAME_MAX_LEN + 1];
	memset(invite_name, 0, sizeof(invite_name));
	strncpy(invite_name, p_recv_cmd->m_szName, sizeof(invite_name) - 1);
	std::string str_name = invite_name;
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * p_client = pMgr->FindByName(str_name);

	if(! p_client)
	{
		pClient->RetFamilyResult(enFamilyInviteRet_Offline);
		return enPHandler_OK;
	}

	if(p_client->GetFullData().m_roleInfo.m_baseInfo.m_ucLevel  < 3)
	{
		std::ostringstream os;
		os<<"the invited player's level less than 3,can't join the family";
		pClient->SendSystemChat(os.str().c_str(), os.str().size());
		return enPHandler_OK;
	}

	if(p_client->IsHaveFamily())
	{
		if(! p_client->IsFamilyRequest())
		{
			pClient->RetFamilyResult(enFamilyInviteRet_Family);
			return enPHandler_OK;
		}
	}
	
	MODI_FamilyInviteData * p_invite_data = new MODI_FamilyInviteData();
	p_invite_data->Init(pClient, p_client);
	//p_invite_data->Invite();
	
	return enPHandler_OK;
}



/** 
 * @brief 家族邀请答复
 * 
 */
int MODI_PackageHandler_c2zs::FamilyInviteResponse_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
													   const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}
	CHECK_PACKAGE(MODI_C2GS_Request_InvitionResponse, cmd_size , 0 , 0 , pClient);

	const MODI_C2GS_Request_InvitionResponse* p_recv_cmd = (const MODI_C2GS_Request_InvitionResponse * )pt_null_cmd;
	MODI_FamilyInviteData * p_invite_data = (MODI_FamilyInviteData *)MODI_InviteManager::GetInstance().FindInvite(p_recv_cmd->m_dwInvitionID);

	if(p_invite_data)
	{
		p_invite_data->Responsion(p_recv_cmd->m_enResult);
	}
	
	return enPHandler_OK;
}

/// INA版本查询余额
int MODI_PackageHandler_c2zs::ReqBalance_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	MODI_OnlineClient * pClient = static_cast<MODI_OnlineClient *>(pObj);
	if( !pClient )
	{
		return enPHandler_OK;
	}
	/// INA版本同步钱
	if(Global::g_byGameShop > 0)
  	{
		MODI_BillClient * pBill = MODI_BillClient::GetInstancePtr();
		if(pBill && pBill->IsConnected())
		{
			pBill->SRefreshMoney(pClient->GetAccName(), pClient->GetAccid());
		}
	}
	return enPHandler_OK;
}
