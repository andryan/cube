/** 
 * @file ZoneServerAPP.h
 * @brief Zoneserver框架
 * @author Tang Teng
 * @version v0.1
 * @date 2010-07-19
 */
#ifndef MODI_ZONESERVER_APPFRAME_H_
#define MODI_ZONESERVER_APPFRAME_H_


#include "IAppFrame.h"
#include "AssertEx.h"
#include "PackageHandler_s2zs.h"
#include "PackageHandler_rdb2zs.h"
#include "PackageHandler_c2zs.h"
#include "PackageHandler_zs2ms.h"
#include "DBProxyClientTask.h"
#include "GameSvrClientTask.h"
#include "OnlineClientMgr.h"
#include "OfflineUserMgr.h"
#include "World.h"
#include "ManangerClient.h"
#include "DBProxyClientTask.h"
#include "GMCommand_ZS.h"
#include "ScriptMailHandler.h"
#include "Channellist.h"
#include "TodayOnlineTimeClean.h"
#include "RankCore.h"

class 	MODI_ZoneServerAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_ZoneServerAPP( const char * szAppName );

		virtual ~MODI_ZoneServerAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

		MODI_DBProxyClientTask * GetDBInterface( BYTE byServer );

		WORD 		GetNextTransCount() { m_nTransCount += 1; return m_nTransCount; }

		/** 
		 * @brief 告知数据库服务器执行sql
		 * 
		 */
		static void ExecSqlToDB(const char * sql, const WORD sql_len);
		
		static MODI_TableStruct * m_pFamilyTbl;
		static MODI_TableStruct * m_pFamilyMemTbl;
		static MODI_TableStruct * m_pCharactersTbl;
		static MODI_TableStruct * m_pSingleMusicTbl;

	private:
		MODI_SvrChannellist  		m_Channellist; // 当前区的频道列表
		MODI_PackageHandler_rdb2zs  	m_packageHandler_rdb2zs;
		MODI_PackageHandler_s2zs 	m_packageHandler_s2zs;
		MODI_PackageHandler_c2zs 	m_packageHandler_c2zs;
		MODI_PackageHandler_zs2ms 	m_packageHandler_zs2ms;
		MODI_ManangerSvrClient 	* 	m_pManangerClient;
		MODI_OnlineClientMgr  		m_ClientMgr;
		MODI_OfflineUserMgr 		m_OfflineMgr;
		MODI_World 					m_World;
		MODI_GMCmdHandler_zs 		m_gmCmdHandler;
		MODI_ScriptMailHandler  	m_ScriptMailHandler;
		WORD 						m_nTransCount;
		MODI_TodayOnlineTimeClean 	m_todayOnlineTime;
		MODI_RankCore 				rank_core_;
};

extern "C" MODI_ZoneServerAPP * 	GetApp();
#endif
