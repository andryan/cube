#include "GameSvrClientTask.h"
#include "gw2gs.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "PackageHandler_s2zs.h"
#include "PackageHandler_c2zs.h"
#include "s2zs_cmd.h"
#include "OnlineClientMgr.h"
#include "World.h"
#include "ManangerClient.h"
#include "Base/HelpFuns.h"
#include "SystemUser.h"
#include "ZoneUserVar.h"
#include "SingleMusicMgr.h"
#include "Channellist.h"

MODI_SvrPackageProcessBase 	 MODI_GameSvrClientTask::ms_ProcessBase;
MODI_GameSvrClientTask * 	MODI_GameSvrClientTask::ms_pGameServers[255];
WORD MODI_GameSvrClientTask::m_wdChannelMaxClient = 150;

typedef	__gnu_cxx::hash_map<std::string, MODI_ZoneUserVar *, str_hash > defUserVarMap;
typedef	 defUserVarMap::iterator defUserVarMapIter;
void 	MODI_GameSvrClientTask::Init()
{
	memset( ms_pGameServers , 0 , sizeof(ms_pGameServers) );
}

bool 	MODI_GameSvrClientTask::RegServer( BYTE byServer , const char * szName , DWORD nGateIP , WORD nGatePort , MODI_GameSvrClientTask * p )
{
	if( !byServer )
	{
		MODI_ASSERT(0);
		return false;
	}

	if( ms_pGameServers[byServer] )
	{
		Global::logger->warn("[%s] gameserver<id=%u,name=%s> dup register. " , SVR_TEST ,
				byServer ,
				szName );

//		MODI_GameSvrClientTask * pOld = ms_pGameServers[byServer];
//		pOld->Terminate();
		MODI_ASSERT(0);
		return false;
	}

	p->m_byServerID = byServer;
	safe_strncpy( p->m_szServerName , szName , sizeof(p->m_szServerName) );
	p->m_nGateIP = nGateIP;
	p->m_nGatePort = nGatePort;
	ms_pGameServers[byServer] = p;

	MODI_GameSvrClientTask::SyncChannelListToLoginServer();

	/// 刚注册后，立刻同步过去全局系统变量
	MODI_ZoneUserVar *p_var=NULL;
	defUserVarMap  &map = MODI_SystemUser::GetInstance()->GetClient()->m_stZoneUserVarMgr.GetVarMap();	
	char packet[Skt::MAX_PACKETSIZE];	
	memset(packet,0,sizeof(packet));
	MODI_ZS2S_Notify_GlobalVarList * notify = (MODI_ZS2S_Notify_GlobalVarList *)packet;
	AutoConstruct(notify);
	
	defUserVarMapIter iter = map.begin();
	BYTE i=0;
	for(; iter != map.end(); iter++)
	{
		p_var = iter->second;
		strncpy(notify->m_stGlobalArray[i].m_csName,p_var->GetName(),GLOBAL_VAR_NAME_LEN);
		notify->m_stGlobalArray[i].m_wValue = p_var->GetValue();
		notify->m_nCount++;
	}
	p->SendCmd(notify,notify->m_nCount*sizeof(MODI_GlobalVar)+sizeof(MODI_ZS2S_Notify_GlobalVarList));

#ifdef _DEBUG
	Global::logger->debug("Send command to game server about the global user var");
#endif

	/// 注册完成后，同步单曲列表信息
	char	m_szPacket[Skt::MAX_USERDATASIZE];
	MODI_ZS2S_Notify_SingleMusicList  *plist=(MODI_ZS2S_Notify_SingleMusicList* )m_szPacket;
	AutoConstruct(plist);

	plist->m_wNum=MODI_SingMusicMgr::GetInstance().GenPackage( plist->m_stMusic,enGameMode_Sing);
	plist->m_eMode = enGameMode_Sing;
	p->SendCmd( plist, (sizeof( MODI_ZS2S_Notify_SingleMusicList)+sizeof(SingleMusicInfo)*plist->m_wNum));
#ifdef _DEBUG
	Global::logger->debug("[send_single_list]  num = %u",plist->m_wNum);
#endif


	memset( m_szPacket,0 ,sizeof( m_szPacket));
	AutoConstruct(plist);
	plist->m_wNum = MODI_SingMusicMgr::GetInstance().GenPackage( plist->m_stMusic, enGameMode_Key);
	plist->m_eMode = enGameMode_Key;
	p->SendCmd( plist,( sizeof( MODI_ZS2S_Notify_SingleMusicList)+sizeof(SingleMusicInfo)*plist->m_wNum));

	

#ifdef _DEBUG
	Global::logger->debug("[send_single_list]  num = %u",plist->m_wNum);
#endif

	return true;
}

void 	MODI_GameSvrClientTask::IncClientNum()
{
	m_nClientNum += 1;
}

void 	MODI_GameSvrClientTask::DecClientNum()
{
	if( m_nClientNum >= 1 )
	{
		m_nClientNum -= 1;
	}
}

MODI_GameSvrClientTask * MODI_GameSvrClientTask::FindServer(BYTE byServer)
{
	if( !byServer )
		return 0;

	return ms_pGameServers[byServer];
}

// 连接上的回调,可以安全的在逻辑线程调用
void MODI_GameSvrClientTask::OnConnection()
{
	m_byServerID = 0;
	memset( m_szServerName , 0 , sizeof(m_szServerName) );
}

// 断开的回调，可以安全的在逻辑线程调用
void MODI_GameSvrClientTask::OnDisconnection()
{
	if( m_byServerID )
	{
		// delete all client on this server
		MODI_World * pWorld = MODI_World::GetInstancePtr();
		pWorld->OnServerDisconnection( m_byServerID );

		ms_pGameServers[m_byServerID] = 0;

		MODI_GameSvrClientTask::SyncChannelListToLoginServer();
	}
}

void 	MODI_GameSvrClientTask::ProcessAllPackage()
{
	ms_ProcessBase.ProcessAllPackage();
}

MODI_GameSvrClientTask::MODI_GameSvrClientTask( const int sock, const struct sockaddr_in * addr):
	MODI_IServerClientTask( sock , addr )
{
	Resize(ZSTOGS_CMDSIZE);
	m_nClientNum = 0;
}

MODI_GameSvrClientTask::~MODI_GameSvrClientTask()
{

}


bool MODI_GameSvrClientTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
		return false;

	this->Put( pt_null_cmd , cmd_size );
	return true;
}


void MODI_GameSvrClientTask::OnHandlerClientCmd(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	MODI_GS2ZS_Redirectional & redirectionalPackage = *((MODI_GS2ZS_Redirectional *) (pt_null_cmd));

	//  find client
	MODI_OnlineClientMgr * pMgr = MODI_OnlineClientMgr::GetInstancePtr();
	MODI_OnlineClient * pClient = pMgr->FindByGUID( GUID_LOPART(redirectionalPackage.m_guid) );
	// check vaild
	if( !pClient )
	{
		return ;
	}

	if( redirectionalPackage.m_nSize < 2 )
	{
		return ;
	}

	const Cmd::stNullCmd * pClientCmd = (const Cmd::stNullCmd *) (&redirectionalPackage.m_pData[0]);


	///	游戏命令的处理
	int iRet = MODI_PackageHandler_c2zs::GetInstancePtr()->DoHandlePackage(
					pClientCmd->byCmd, pClientCmd->byParam, 
					pClient , pClientCmd,
					redirectionalPackage.m_nSize );

	if( iRet == enPHandler_Ban )
	{

	}
	else  if ( iRet == enPHandler_Kick )
	{

	}
	else if( iRet == enPHandler_Warning )
	{

	}
}

void MODI_GameSvrClientTask::OnHandlerGameServerCmd(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	MODI_PackageHandler_s2zs::GetInstancePtr()->DoHandlePackage(
					ptNullCmd->byCmd, ptNullCmd->byParam, 
					this , ptNullCmd,
					dwCmdLen );
}

bool MODI_GameSvrClientTask::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	bool bGameServerCmd = true;
	if( ptNullCmd->byCmd == MAINCMD_S2ZS && ptNullCmd->byParam == MODI_GS2ZS_Redirectional::ms_SubCmd )
		bGameServerCmd = false;

	if( bGameServerCmd )
	{
		OnHandlerGameServerCmd( ptNullCmd , dwCmdLen );
	}
	else 
	{
		OnHandlerClientCmd( ptNullCmd , dwCmdLen );
	}

	return true;
}

int MODI_GameSvrClientTask::RecycleConn()
{
	if( this->IsCanDel() )
		return 1;
	return 0;
}

void MODI_GameSvrClientTask::DisConnect()
{
	Global::logger->info("[%s] remote client<ip=%s,port=%u> disconnectioned. " , 
			ERROR_CON ,
			this->GetIP() , 
			this->GetPort() );
}

		
int MODI_GameSvrClientTask::CreateChannelListPackage( char * szBuf , size_t nBufSize , int iReason , const defAccountID & acc_id)
{
	memset( szBuf , 0 , nBufSize );
	MODI_LS2C_Notify_Channellist * pNotify = (MODI_LS2C_Notify_Channellist *)(szBuf);
	AutoConstruct( pNotify );
	MODI_SvrChannellist * plist = MODI_SvrChannellist::GetInstancePtr();
	if(! plist )
	{
		MODI_ASSERT(0);
		return 0;
	}

	pNotify->m_nReason = (MODI_LS2C_Notify_Channellist::Reason)iReason;
	for( int i = 0; i < 255; i++ )
	{
		MODI_GameSvrClientTask * pTemp = ms_pGameServers[i];
		if( pTemp )
		{
			pNotify->m_pData[pNotify->m_nCount].m_nGatewayIP = pTemp->m_nGateIP;
			pNotify->m_pData[pNotify->m_nCount].m_nGatewayPort = pTemp->m_nGatePort;
			safe_strncpy( pNotify->m_pData[pNotify->m_nCount].m_szServerName  , 
					pTemp->m_szServerName , 
					sizeof(pNotify->m_pData[pNotify->m_nCount].m_szServerName) );

			pNotify->m_pData[pNotify->m_nCount].m_nServerID = pTemp->m_byServerID;
		
			MODI_ChannelAddr get_addr;
			plist->GetChannelAddr(pTemp->m_byServerID, "dx", get_addr);
			pNotify->m_pData[pNotify->m_nCount].m_nTSvrIP = PublicFun::StrIP2NumIP(get_addr.m_strTSvrIP.c_str());
			pNotify->m_pData[pNotify->m_nCount].m_nTSvrPort = get_addr.m_wdTSvrPort;
		
			//float fTemp = (float)pTemp->GetClientNum() / (float)GAME_CHANNEL_MAX_CLIENT;
			float fTemp = (float)pTemp->GetClientNum() / (float)m_wdChannelMaxClient;
			fTemp *= 100.0f;
			BYTE byLoadStatus = (BYTE)fTemp;
			if( byLoadStatus > 100 )
				byLoadStatus = 100;

			pNotify->m_pData[pNotify->m_nCount].m_byLoadStatus = byLoadStatus;
			Global::logger->debug("[channel_state] channel(name=%s,id=%d) have playerstate(max=%u,cur=%u,per=%d)", 
					pTemp->m_szServerName, 
					pTemp->m_byServerID, 
					m_wdChannelMaxClient, 
					pTemp->GetClientNum(),
					byLoadStatus);
			pNotify->m_nCount += 1;
			/*			if((acc_id != 0) && (acc_id < 9778 || acc_id > 12277))
			{
				if(i == 8)
				{
					break;
				}
				}*/
				
		}
	}

   	int iSendSize = sizeof(MODI_ServerStatus) * pNotify->m_nCount + sizeof(MODI_LS2C_Notify_Channellist);
	return iSendSize;
}


/// 生成gmtool所有查询的信息
int MODI_GameSvrClientTask::CreateChannelStatusToGMTool(MODI_ReturnChannelStatus_Cmd * p_send_cmd)
{
	p_send_cmd->m_wdMaxNum = m_wdChannelMaxClient;
	MODI_ChannelStatusInfo * p_info = &(p_send_cmd->m_pData[0]);
	for( int i = 0; i < 255; i++ )
	{
		MODI_GameSvrClientTask * pTemp = ms_pGameServers[i];
		if( pTemp )
		{
			p_info->m_nGatewayIP = pTemp->m_nGateIP;
			p_info->m_nGatewayPort = pTemp->m_nGatePort;
			strncpy(p_info->m_cstrName, pTemp->m_szServerName, sizeof(p_info->m_cstrName) - 1);
			p_info->m_nServerID = pTemp->m_byServerID;
			p_info->m_wdNum = pTemp->GetClientNum();
			p_send_cmd->m_wdChannelNum++;
			p_info++;
		}
	}
   	int iSendSize = sizeof(MODI_ChannelStatusInfo) * p_send_cmd->m_wdChannelNum + sizeof(MODI_ReturnChannelStatus_Cmd);
	return iSendSize;
}
		
void 	MODI_GameSvrClientTask::SyncChannelListToLoginServer()
{
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	int iSendSize =	CreateChannelListPackage( szPackageBuf , sizeof(szPackageBuf) , 
			MODI_LS2C_Notify_Channellist::kBegin );
	if( iSendSize > 0 )
	{
		// sync to ls
		MODI_ManangerSvrClient * pMc = MODI_ManangerSvrClient::GetInstancePtr();
		if( pMc )
		{
			pMc->SendCmd( (const Cmd::stNullCmd *)szPackageBuf , iSendSize );
		}

//		for( int i = 0; i < 255; i++ )
//		{
//			MODI_GameSvrClientTask * pTemp = ms_pGameServers[i];
//			if( pTemp )
//			{
//				pTemp->SendCmd( (const Cmd::stNullCmd *) szPackageBuf , iSendSize );
//			}
//		}
	}
}


void MODI_GameSvrClientTask::BroadcastAllServer(const stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	for(int i=1; i<255; i++)
	{
		MODI_GameSvrClientTask * p_server = NULL;
		p_server = ms_pGameServers[i];
		if(p_server)
		{
			p_server->SendCmd(pt_null_cmd, cmd_size);
		}
	}
}
