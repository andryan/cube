/**
 * @file   Mail.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jan 30 11:28:17 2012
 * 
 * @brief  邮件类整理
 * 
 * 
 */


#include "Mail.h"
#include "TimeManager.h"
#include "MailCore.h"
#include "DBProxyClientTask.h"
#include "ZoneServerAPP.h"
#include "Base/HelpFuns.h"

MODI_Mail::MODI_Mail()
{
	m_bNeedSave = false;
	m_bLockGetAttach = false;
}

MODI_Mail::~MODI_Mail()
{

}

void MODI_Mail::Init(const MODI_DBMailInfo & info)
{
	if(GetGUID().IsInvalid())
	{
		m_dbInfo.Reset();
		m_dbInfo = info;
		m_dbInfo.m_bIsValid = true;
		MODI_ASSERT( m_dbInfo.m_dbID != INVAILD_DBMAIL_ID );
		for(BYTE i=0; i<MAX_MAIL_ATTACH_NUM; i++)
		{
			m_dbInfo.m_MailInfo.m_ExtraInfo[i] = info.m_MailInfo.m_ExtraInfo[i];
		}
		m_dbInfo.m_MailInfo.m_mailGUID = m_dbInfo.m_dbID;
	}
}

void  	MODI_Mail::InitFromSendMail( const char * szSender , size_t nSenderLen , const MODI_SendMailInfo & info )
{
	if( GetGUID().IsInvalid() )
	{
		m_dbInfo.Reset();
		m_dbInfo.m_bIsValid = true;
		m_dbInfo.m_dbID = MODI_MailCore::GetNextMailID();
		m_dbInfo.m_MailInfo.m_Type = info.m_Type;
		SetCaption( info.m_szCaption );
		SetContents( info.m_szContents );
		SetSender( szSender );
		SetRecevier( info.m_szRecevier );
		m_dbInfo.m_MailInfo.m_byExtraCount = info.m_byExtraCount;
		for(BYTE i=0; i<info.m_byExtraCount; i++)
		{
			m_dbInfo.m_MailInfo.m_ExtraInfo[i] = info.m_ExtraInfo[i];
		}
		
		m_dbInfo.m_MailInfo.m_mailGUID = m_dbInfo.m_dbID;
	}
}

void 	MODI_Mail::SetReadState()
{
	if( IsRead() == false )
	{
		m_dbInfo.m_MailInfo.m_State |= kAlreadyReadMail;
		if( m_dbInfo.m_bIsValid && m_dbInfo.m_dbID != INVAILD_DBMAIL_ID )
			m_bNeedSave = true;
	}
}

bool MODI_Mail::IsAlreadyGetAttachment() const
{
	return m_dbInfo.m_MailInfo.IsAlreadyGetAttachment();
}

void MODI_Mail::SetAlreadyGetAttachment()
{
	if( IsAlreadyGetAttachment() == false )
	{
		m_dbInfo.m_MailInfo.m_State |= kAlreadyGetAttachment;
		if( m_dbInfo.m_bIsValid && m_dbInfo.m_dbID != INVAILD_DBMAIL_ID )
			m_bNeedSave = true;
	}
}

bool MODI_Mail::IsScriptMail() const
{
	if( m_dbInfo.m_ScriptData.m_ScriptID > kScriptMail_Begin && m_dbInfo.m_ScriptData.m_ScriptID < kScriptMail_End )
		return false;
	return true;
}

enScriptMailID MODI_Mail::GetScriptMailID() const
{
	return m_dbInfo.m_ScriptData.m_ScriptID;
}

void MODI_Mail::SetSender( const char * szName )
{
	safe_strncpy( m_dbInfo.m_MailInfo.m_szSender , szName ,  sizeof(m_dbInfo.m_MailInfo.m_szSender) );
}

void MODI_Mail::SetRecevier( const char * szName )
{
	safe_strncpy( m_dbInfo.m_MailInfo.m_szRecevier , szName ,  sizeof(m_dbInfo.m_MailInfo.m_szRecevier) );
}

void MODI_Mail::SetCaption( const char * szCaption )
{
	safe_strncpy( m_dbInfo.m_MailInfo.m_szCaption , szCaption ,  sizeof(m_dbInfo.m_MailInfo.m_szCaption) );
}

void MODI_Mail::SetContents( const char * szContents )
{
	safe_strncpy( m_dbInfo.m_MailInfo.m_szContents , szContents , sizeof(m_dbInfo.m_MailInfo.m_szContents) );
}

void MODI_Mail::SetTimeStamp()
{
	MODI_TimeManager * ptm = MODI_TimeManager::GetInstancePtr();
	m_dbInfo.m_MailInfo.m_SendTime = ptm->GetSavedANSITime();
}

void  MODI_Mail::DeleteFromDB()
{
	if( GetDBMailInfo().m_bIsValid && GetDBMailInfo().m_dbID != INVAILD_DBMAIL_ID )
	{
		m_bNeedSave = true;
		m_dbInfo.m_bIsValid = false;
		SaveToDB();
	}
}

void  MODI_Mail::SaveToDB()
{
	if( m_bNeedSave )
	{
		MODI_DBProxyClientTask * pDBProxy  = GetApp()->GetDBInterface(0);
		if( pDBProxy )
		{
			MODI_S2RDB_Request_SaveMail reqSave;
			reqSave.m_mail = GetDBMailInfo();
			m_bNeedSave = false;
			pDBProxy->SendCmd( &reqSave , sizeof(reqSave) );
#if 0
			Global::logger->debug("[save_mail] save mail to db successful <state=%u,mailid=%llu,transid=%llu,sender=%s,recever=%s,count=%u>",
								  (WORD)m_dbInfo.m_MailInfo.m_State, m_dbInfo.m_dbID, m_dbInfo.m_Transid,
								  m_dbInfo.m_MailInfo.m_szSender, m_dbInfo.m_MailInfo.m_szRecevier,
								  (WORD)m_dbInfo.m_MailInfo.m_byExtraCount);
#endif			
		}
	}
}






void  MODI_Mail::SetNewMail()
{
	m_bNeedSave  = true;
}
		
void MODI_Mail::ResetExtraInfo()
{
	m_dbInfo.m_MailInfo.m_byExtraCount = 0;
	for(BYTE i=0; i<MAX_MAIL_ATTACH_NUM; i++)
	{
		m_dbInfo.m_MailInfo.m_ExtraInfo[i].Reset();
	}
}
		
