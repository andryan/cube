#include "OnlineClientMgr.h"


class MODI_SystemUser 
{
public:
	void Init();
	static MODI_SystemUser * GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_SystemUser();
		}
		return m_pInstance;
	}

	MODI_OnlineClient * GetClient()
	{
		return m_pClient;

	}

private:
	MODI_SystemUser()
	{
		m_pClient = NULL;

	}
	~MODI_SystemUser()
	{

	}
	static MODI_SystemUser * m_pInstance;
	MODI_OnlineClient * m_pClient;

};
