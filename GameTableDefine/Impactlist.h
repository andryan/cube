/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Impactlist_H_CPPCODE
#define Impactlist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Impactlist
	{
	public:

		MODI_Impactlist() {} 
		~MODI_Impactlist() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_ClientID(unsigned short _v)
		{	ClientID= _v; }

		unsigned short	get_ClientID() const
		{	return ClientID; }

		void	set_TemplateID(unsigned short _v)
		{	TemplateID= _v; }

		unsigned short	get_TemplateID() const
		{	return TemplateID; }

		void	set_ContinueTime(float _v)
		{	ContinueTime= _v; }

		float	get_ContinueTime() const
		{	return ContinueTime; }

		void	set_IntervalTime(float _v)
		{	IntervalTime= _v; }

		float	get_IntervalTime() const
		{	return IntervalTime; }

		void	set_CanDispeled(unsigned char _v)
		{	CanDispeled= _v; }

		unsigned char	get_CanDispeled() const
		{	return CanDispeled; }

		void	set_CanCancel(unsigned char _v)
		{	CanCancel= _v; }

		unsigned char	get_CanCancel() const
		{	return CanCancel; }

		void	set_OutGameFadeOut(unsigned char _v)
		{	OutGameFadeOut= _v; }

		unsigned char	get_OutGameFadeOut() const
		{	return OutGameFadeOut; }

		void	set_Arg1(float _v)
		{	Arg1= _v; }

		float	get_Arg1() const
		{	return Arg1; }

		void	set_Arg2(float _v)
		{	Arg2= _v; }

		float	get_Arg2() const
		{	return Arg2; }

		void	set_Arg3(float _v)
		{	Arg3= _v; }

		float	get_Arg3() const
		{	return Arg3; }

		void	set_Arg4(float _v)
		{	Arg4= _v; }

		float	get_Arg4() const
		{	return Arg4; }

		void	set_Arg5(float _v)
		{	Arg5= _v; }

		float	get_Arg5() const
		{	return Arg5; }

		void	set_Arg6(float _v)
		{	Arg6= _v; }

		float	get_Arg6() const
		{	return Arg6; }

		void	set_Arg7(float _v)
		{	Arg7= _v; }

		float	get_Arg7() const
		{	return Arg7; }

		void	set_Arg8(float _v)
		{	Arg8= _v; }

		float	get_Arg8() const
		{	return Arg8; }


	protected:

/// 效果ID 
		unsigned int	ConfigID; 
/// 效果名称 
		char Name[512]; 
/// 客户端效果ID 
		unsigned short	ClientID; 
/// 效果模板ID 
		unsigned short	TemplateID; 
/// 效果持续时间 
		float	ContinueTime; 
/// 效果间隔触发时间 
		float	IntervalTime; 
/// 是否可以被驱散 
		unsigned char	CanDispeled; 
/// 是否可以被取消 
		unsigned char	CanCancel; 
/// 游戏结束时是否淡出效果 
		unsigned char	OutGameFadeOut; 
/// 参数1 
		float	Arg1; 
/// 参数2 
		float	Arg2; 
/// 参数3 
		float	Arg3; 
/// 参数4 
		float	Arg4; 
/// 参数5 
		float	Arg5; 
/// 参数6 
		float	Arg6; 
/// 参数7 
		float	Arg7; 
/// 参数8 
		float	Arg8; 

	};


#pragma pack() 

}



#endif
