/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef GoodsPackage_H_CPPCODE
#define GoodsPackage_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_GoodsPackage
	{
	public:

		MODI_GoodsPackage() {} 
		~MODI_GoodsPackage() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_Goods_1(unsigned short _v)
		{	Goods_1= _v; }

		unsigned short	get_Goods_1() const
		{	return Goods_1; }

		void	set_TimeLimit_1(unsigned char _v)
		{	TimeLimit_1= _v; }

		unsigned char	get_TimeLimit_1() const
		{	return TimeLimit_1; }

		void	set_Count_1(unsigned short _v)
		{	Count_1= _v; }

		unsigned short	get_Count_1() const
		{	return Count_1; }

		void	set_Goods_2(unsigned short _v)
		{	Goods_2= _v; }

		unsigned short	get_Goods_2() const
		{	return Goods_2; }

		void	set_TimeLimit_2(unsigned char _v)
		{	TimeLimit_2= _v; }

		unsigned char	get_TimeLimit_2() const
		{	return TimeLimit_2; }

		void	set_Count_2(unsigned short _v)
		{	Count_2= _v; }

		unsigned short	get_Count_2() const
		{	return Count_2; }

		void	set_Goods_3(unsigned short _v)
		{	Goods_3= _v; }

		unsigned short	get_Goods_3() const
		{	return Goods_3; }

		void	set_TimeLimit_3(unsigned char _v)
		{	TimeLimit_3= _v; }

		unsigned char	get_TimeLimit_3() const
		{	return TimeLimit_3; }

		void	set_Count_3(unsigned short _v)
		{	Count_3= _v; }

		unsigned short	get_Count_3() const
		{	return Count_3; }

		void	set_Goods_4(unsigned short _v)
		{	Goods_4= _v; }

		unsigned short	get_Goods_4() const
		{	return Goods_4; }

		void	set_TimeLimit_4(unsigned char _v)
		{	TimeLimit_4= _v; }

		unsigned char	get_TimeLimit_4() const
		{	return TimeLimit_4; }

		void	set_Count_4(unsigned short _v)
		{	Count_4= _v; }

		unsigned short	get_Count_4() const
		{	return Count_4; }

		void	set_Goods_5(unsigned short _v)
		{	Goods_5= _v; }

		unsigned short	get_Goods_5() const
		{	return Goods_5; }

		void	set_TimeLimit_5(unsigned char _v)
		{	TimeLimit_5= _v; }

		unsigned char	get_TimeLimit_5() const
		{	return TimeLimit_5; }

		void	set_Count_5(unsigned short _v)
		{	Count_5= _v; }

		unsigned short	get_Count_5() const
		{	return Count_5; }

		void	set_Goods_6(unsigned short _v)
		{	Goods_6= _v; }

		unsigned short	get_Goods_6() const
		{	return Goods_6; }

		void	set_TimeLimit_6(unsigned char _v)
		{	TimeLimit_6= _v; }

		unsigned char	get_TimeLimit_6() const
		{	return TimeLimit_6; }

		void	set_Count_6(unsigned short _v)
		{	Count_6= _v; }

		unsigned short	get_Count_6() const
		{	return Count_6; }

		void	set_Goods_7(unsigned short _v)
		{	Goods_7= _v; }

		unsigned short	get_Goods_7() const
		{	return Goods_7; }

		void	set_TimeLimit_7(unsigned char _v)
		{	TimeLimit_7= _v; }

		unsigned char	get_TimeLimit_7() const
		{	return TimeLimit_7; }

		void	set_Count_7(unsigned short _v)
		{	Count_7= _v; }

		unsigned short	get_Count_7() const
		{	return Count_7; }

		void	set_Goods_8(unsigned short _v)
		{	Goods_8= _v; }

		unsigned short	get_Goods_8() const
		{	return Goods_8; }

		void	set_TimeLimit_8(unsigned char _v)
		{	TimeLimit_8= _v; }

		unsigned char	get_TimeLimit_8() const
		{	return TimeLimit_8; }

		void	set_Count_8(unsigned short _v)
		{	Count_8= _v; }

		unsigned short	get_Count_8() const
		{	return Count_8; }

		void	set_Goods_9(unsigned short _v)
		{	Goods_9= _v; }

		unsigned short	get_Goods_9() const
		{	return Goods_9; }

		void	set_TimeLimit_9(unsigned char _v)
		{	TimeLimit_9= _v; }

		unsigned char	get_TimeLimit_9() const
		{	return TimeLimit_9; }

		void	set_Count_9(unsigned short _v)
		{	Count_9= _v; }

		unsigned short	get_Count_9() const
		{	return Count_9; }

		void	set_Goods_10(unsigned short _v)
		{	Goods_10= _v; }

		unsigned short	get_Goods_10() const
		{	return Goods_10; }

		void	set_TimeLimit_10(unsigned char _v)
		{	TimeLimit_10= _v; }

		unsigned char	get_TimeLimit_10() const
		{	return TimeLimit_10; }

		void	set_Count_10(unsigned short _v)
		{	Count_10= _v; }

		unsigned short	get_Count_10() const
		{	return Count_10; }


	protected:

/// ID 
		unsigned int	ConfigID; 
/// 名称 
		char Name[512]; 
/// 第1种物品的ID 
		unsigned short	Goods_1; 
/// 第1种物品的时间期限 
		unsigned char	TimeLimit_1; 
/// 第1种物品的个数 
		unsigned short	Count_1; 
/// 第2种物品的ID 
		unsigned short	Goods_2; 
/// 第2种物品的时间期限 
		unsigned char	TimeLimit_2; 
/// 第2种物品的个数 
		unsigned short	Count_2; 
/// 第3种物品的ID 
		unsigned short	Goods_3; 
/// 第3种物品的时间期限 
		unsigned char	TimeLimit_3; 
/// 第3种物品的个数 
		unsigned short	Count_3; 
/// 第4种物品的ID 
		unsigned short	Goods_4; 
/// 第4种物品的时间期限 
		unsigned char	TimeLimit_4; 
/// 第4种物品的个数 
		unsigned short	Count_4; 
/// 第5种物品的ID 
		unsigned short	Goods_5; 
/// 第5种物品的时间期限 
		unsigned char	TimeLimit_5; 
/// 第5种物品的个数 
		unsigned short	Count_5; 
/// 第6种物品的ID 
		unsigned short	Goods_6; 
/// 第6种物品的时间期限 
		unsigned char	TimeLimit_6; 
/// 第6种物品的个数 
		unsigned short	Count_6; 
/// 第7种物品的ID 
		unsigned short	Goods_7; 
/// 第7种物品的时间期限 
		unsigned char	TimeLimit_7; 
/// 第7种物品的个数 
		unsigned short	Count_7; 
/// 第8种物品的ID 
		unsigned short	Goods_8; 
/// 第8种物品的时间期限 
		unsigned char	TimeLimit_8; 
/// 第8种物品的个数 
		unsigned short	Count_8; 
/// 第9种物品的ID 
		unsigned short	Goods_9; 
/// 第9种物品的时间期限 
		unsigned char	TimeLimit_9; 
/// 第9种物品的个数 
		unsigned short	Count_9; 
/// 第10种物品的ID 
		unsigned short	Goods_10; 
/// 第10种物品的时间期限 
		unsigned char	TimeLimit_10; 
/// 第10种物品的个数 
		unsigned short	Count_10; 

	};


#pragma pack() 

}



#endif
