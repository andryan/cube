/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Citylist_H_CPPCODE
#define Citylist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_City
	{
	public:

		MODI_City() {} 
		~MODI_City() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_AreaName(const char * _v)
		{	 strcpy( AreaName ,  _v ); }

		const char *get_AreaName() const
		{	return AreaName; }

		void	set_CityName(const char * _v)
		{	 strcpy( CityName ,  _v ); }

		const char *get_CityName() const
		{	return CityName; }


	protected:

/// 城市ID 
		unsigned short	ConfigID; 
/// 区域(县市) 
		char AreaName[512]; 
/// 所属城市(省份) 
		char CityName[512]; 

	};


#pragma pack() 

}



#endif
