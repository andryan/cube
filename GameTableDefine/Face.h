/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Face_H_CPPCODE
#define Face_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Face
	{
	public:

		MODI_Face() {} 
		~MODI_Face() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_ImageName(const char * _v)
		{	 strcpy( ImageName ,  _v ); }

		const char *get_ImageName() const
		{	return ImageName; }


	protected:

/// ID 
		unsigned short	ConfigID; 
/// ����ͼƬ·�� 
		char ImageName[512]; 

	};


#pragma pack() 

}



#endif
