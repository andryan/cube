/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Itemlist_H_CPPCODE
#define Itemlist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Itemlist
	{
	public:

		MODI_Itemlist() {} 
		~MODI_Itemlist() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_SubClass(unsigned int _v)
		{	SubClass= _v; }

		unsigned int	get_SubClass() const
		{	return SubClass; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_LevelReq(unsigned char _v)
		{	LevelReq= _v; }

		unsigned char	get_LevelReq() const
		{	return LevelReq; }

		void	set_SexReq(unsigned char _v)
		{	SexReq= _v; }

		unsigned char	get_SexReq() const
		{	return SexReq; }

		void	set_MaxOverLap(unsigned short _v)
		{	MaxOverLap= _v; }

		unsigned short	get_MaxOverLap() const
		{	return MaxOverLap; }

		void	set_MaxOwnNum(unsigned short _v)
		{	MaxOwnNum= _v; }

		unsigned short	get_MaxOwnNum() const
		{	return MaxOwnNum; }

		void	set_CanInShop(unsigned char _v)
		{	CanInShop= _v; }

		unsigned char	get_CanInShop() const
		{	return CanInShop; }

		void	set_TargetType(unsigned char _v)
		{	TargetType= _v; }

		unsigned char	get_TargetType() const
		{	return TargetType; }

		void	set_CanUseInGamestate(int _v)
		{	CanUseInGamestate= _v; }

		int	get_CanUseInGamestate() const
		{	return CanUseInGamestate; }

		void	set_CanUseInMode(int _v)
		{	CanUseInMode= _v; }

		int	get_CanUseInMode() const
		{	return CanUseInMode; }

		void	set_UseImpactID(unsigned short _v)
		{	UseImpactID= _v; }

		unsigned short	get_UseImpactID() const
		{	return UseImpactID; }

		void	set_HaveImpactID(unsigned short _v)
		{	HaveImpactID= _v; }

		unsigned short	get_HaveImpactID() const
		{	return HaveImpactID; }

		void	set_UseServerScriptID(const char * _v)
		{	 strcpy( UseServerScriptID ,  _v ); }

		const char *get_UseServerScriptID() const
		{	return UseServerScriptID; }

		void	set_UseClientScriptID(const char * _v)
		{	 strcpy( UseClientScriptID ,  _v ); }

		const char *get_UseClientScriptID() const
		{	return UseClientScriptID; }

		void	set_Depict(const char * _v)
		{	 strcpy( Depict ,  _v ); }

		const char *get_Depict() const
		{	return Depict; }


	protected:

/// 道具ID 
		unsigned int	ConfigID; 
/// 道具类型 
		unsigned int	SubClass; 
/// 道具名称 
		char Name[512]; 
/// 等级需求 
		unsigned char	LevelReq; 
/// 性别需求 
		unsigned char	SexReq; 
/// 最大重叠个数 
		unsigned short	MaxOverLap; 
/// 最大拥有个数 
		unsigned short	MaxOwnNum; 
/// 是否可以出现在商城中 
		unsigned char	CanInShop; 
/// 能够对何种目标使用 
		unsigned char	TargetType; 
/// 可以使用的游戏状态 
		int	CanUseInGamestate; 
/// 可以使用的游戏模式 
		int	CanUseInMode; 
/// 使用道具后产生的效果id 
		unsigned short	UseImpactID; 
/// 拥有道具后获得的效果id 
		unsigned short	HaveImpactID; 
/// 使用道具后服务器调用脚本 
		char UseServerScriptID[512]; 
/// 物使用道具后客户端调用的脚本 
		char UseClientScriptID[512]; 
/// 道具描述 
		char Depict[512]; 

	};


#pragma pack() 

}



#endif
