/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Levellist_H_CPPCODE
#define Levellist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Level
	{
	public:

		MODI_Level() {} 
		~MODI_Level() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Exp(unsigned int _v)
		{	Exp= _v; }

		unsigned int	get_Exp() const
		{	return Exp; }


	protected:

/// 所需等级 
		unsigned int	ConfigID; 
/// 所需经验 
		unsigned int	Exp; 

	};


#pragma pack() 

}



#endif
