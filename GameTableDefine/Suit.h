/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Suit_H_CPPCODE
#define Suit_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Suit
	{
	public:

		MODI_Suit() {} 
		~MODI_Suit() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_transform(unsigned char _v)
		{	transform= _v; }

		unsigned char	get_transform() const
		{	return transform; }

		void	set_Sex(unsigned char _v)
		{	Sex= _v; }

		unsigned char	get_Sex() const
		{	return Sex; }

		void	set_Head(unsigned int _v)
		{	Head= _v; }

		unsigned int	get_Head() const
		{	return Head; }

		void	set_Hair(unsigned int _v)
		{	Hair= _v; }

		unsigned int	get_Hair() const
		{	return Hair; }

		void	set_Upper(unsigned int _v)
		{	Upper= _v; }

		unsigned int	get_Upper() const
		{	return Upper; }

		void	set_Lower(unsigned int _v)
		{	Lower= _v; }

		unsigned int	get_Lower() const
		{	return Lower; }

		void	set_Hand(unsigned int _v)
		{	Hand= _v; }

		unsigned int	get_Hand() const
		{	return Hand; }

		void	set_Foot(unsigned int _v)
		{	Foot= _v; }

		unsigned int	get_Foot() const
		{	return Foot; }

		void	set_Headdress(unsigned int _v)
		{	Headdress= _v; }

		unsigned int	get_Headdress() const
		{	return Headdress; }

		void	set_Glass(unsigned int _v)
		{	Glass= _v; }

		unsigned int	get_Glass() const
		{	return Glass; }

		void	set_Earring(unsigned int _v)
		{	Earring= _v; }

		unsigned int	get_Earring() const
		{	return Earring; }

		void	set_Mic(unsigned int _v)
		{	Mic= _v; }

		unsigned int	get_Mic() const
		{	return Mic; }

		void	set_Mousedress(unsigned int _v)
		{	Mousedress= _v; }

		unsigned int	get_Mousedress() const
		{	return Mousedress; }

		void	set_Necklace(unsigned int _v)
		{	Necklace= _v; }

		unsigned int	get_Necklace() const
		{	return Necklace; }

		void	set_Watch(unsigned int _v)
		{	Watch= _v; }

		unsigned int	get_Watch() const
		{	return Watch; }

		void	set_Ring(unsigned int _v)
		{	Ring= _v; }

		unsigned int	get_Ring() const
		{	return Ring; }

		void	set_Back(unsigned int _v)
		{	Back= _v; }

		unsigned int	get_Back() const
		{	return Back; }

		void	set_Pet(unsigned int _v)
		{	Pet= _v; }

		unsigned int	get_Pet() const
		{	return Pet; }

		void	set_Tail(unsigned int _v)
		{	Tail= _v; }

		unsigned int	get_Tail() const
		{	return Tail; }

		void	set_Model(unsigned short _v)
		{	Model= _v; }

		unsigned short	get_Model() const
		{	return Model; }

		void	set_Armdress(unsigned int _v)
		{	Armdress= _v; }

		unsigned int	get_Armdress() const
		{	return Armdress; }

		void	set_Waistdress(unsigned int _v)
		{	Waistdress= _v; }

		unsigned int	get_Waistdress() const
		{	return Waistdress; }

		void	set_Legdress(unsigned int _v)
		{	Legdress= _v; }

		unsigned int	get_Legdress() const
		{	return Legdress; }

		void	set_Eyedress(unsigned int _v)
		{	Eyedress= _v; }

		unsigned int	get_Eyedress() const
		{	return Eyedress; }

		void	set_Bodydress(unsigned int _v)
		{	Bodydress= _v; }

		unsigned int	get_Bodydress() const
		{	return Bodydress; }


	protected:

/// 套装ID 
		unsigned int	ConfigID; 
/// 套装变形标志 
		unsigned char	transform; 
/// 套装性别需求 
		unsigned char	Sex; 
/// 头 
		unsigned int	Head; 
/// 头发 
		unsigned int	Hair; 
/// 上衣 
		unsigned int	Upper; 
/// 下装 
		unsigned int	Lower; 
/// 手 
		unsigned int	Hand; 
/// 脚 
		unsigned int	Foot; 
/// 头饰 
		unsigned int	Headdress; 
/// 眼睛 
		unsigned int	Glass; 
/// 耳环 
		unsigned int	Earring; 
/// 麦克风 
		unsigned int	Mic; 
/// 嘴巴 
		unsigned int	Mousedress; 
/// 项链 
		unsigned int	Necklace; 
/// 手饰 
		unsigned int	Watch; 
/// 指环 
		unsigned int	Ring; 
/// 背部饰品 
		unsigned int	Back; 
/// 宠物 
		unsigned int	Pet; 
/// 尾巴 
		unsigned int	Tail; 
/// 变形模型id 
		unsigned short	Model; 
/// 手臂 
		unsigned int	Armdress; 
/// 腰部 
		unsigned int	Waistdress; 
/// 腿部 
		unsigned int	Legdress; 
/// 眼部 
		unsigned int	Eyedress; 
/// 身体 
		unsigned int	Bodydress; 

	};


#pragma pack() 

}



#endif
