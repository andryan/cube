/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Soundlist_H_CPPCODE
#define Soundlist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Soundlist
	{
	public:

		MODI_Soundlist() {} 
		~MODI_Soundlist() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_Soundname(const char * _v)
		{	 strcpy( Soundname ,  _v ); }

		const char *get_Soundname() const
		{	return Soundname; }

		void	set_ResourcePath(const char * _v)
		{	 strcpy( ResourcePath ,  _v ); }

		const char *get_ResourcePath() const
		{	return ResourcePath; }


	protected:

/// 音效ID 
		unsigned short	ConfigID; 
/// 音效名 
		char Soundname[512]; 
/// 音效路径 
		char ResourcePath[512]; 

	};


#pragma pack() 

}



#endif
