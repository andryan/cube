/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Quickbuy_H_CPPCODE
#define Quickbuy_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Quickbuy
	{
	public:

		MODI_Quickbuy() {} 
		~MODI_Quickbuy() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }


	protected:

/// ID 
		unsigned short	ConfigID; 

	};


#pragma pack() 

}



#endif
