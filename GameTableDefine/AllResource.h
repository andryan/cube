/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef AllResource_H_CPPCODE
#define AllResource_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_AllResource
	{
	public:

		MODI_AllResource() {} 
		~MODI_AllResource() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Description(const char * _v)
		{	 strcpy( Description ,  _v ); }

		const char *get_Description() const
		{	return Description; }

		void	set_IconId(const char * _v)
		{	 strcpy( IconId ,  _v ); }

		const char *get_IconId() const
		{	return IconId; }

		void	set_SoundId(unsigned int _v)
		{	SoundId= _v; }

		unsigned int	get_SoundId() const
		{	return SoundId; }

		void	set_Distribution(unsigned int _v)
		{	Distribution= _v; }

		unsigned int	get_Distribution() const
		{	return Distribution; }


	protected:

/// 物品ID 
		unsigned int	ConfigID; 
/// 物品描述 
		char Description[512]; 
/// 物品ICON路径 
		char IconId[512]; 
/// 物品使用音效Id 
		unsigned int	SoundId; 
/// 概率分布 
		unsigned int	Distribution; 

	};


#pragma pack() 

}



#endif
