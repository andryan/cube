/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef MeshResource_H_CPPCODE
#define MeshResource_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_MeshResource
	{
	public:

		MODI_MeshResource() {} 
		~MODI_MeshResource() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_ModelName(const char * _v)
		{	 strcpy( ModelName ,  _v ); }

		const char *get_ModelName() const
		{	return ModelName; }

		void	set_AnimationName(const char * _v)
		{	 strcpy( AnimationName ,  _v ); }

		const char *get_AnimationName() const
		{	return AnimationName; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_ParticleEffect(const char * _v)
		{	 strcpy( ParticleEffect ,  _v ); }

		const char *get_ParticleEffect() const
		{	return ParticleEffect; }


	protected:

/// 物品ID 
		unsigned int	ConfigID; 
/// 模型名字 
		char ModelName[512]; 
/// 动作名字 
		char AnimationName[512]; 
/// 模型名字 
		char Name[512]; 
/// 粒子效果 
		char ParticleEffect[512]; 

	};


#pragma pack() 

}



#endif
