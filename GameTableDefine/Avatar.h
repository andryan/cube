/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Avatar_H_CPPCODE
#define Avatar_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Avatar
	{
	public:

		MODI_Avatar() {} 
		~MODI_Avatar() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_SubClass(unsigned int _v)
		{	SubClass= _v; }

		unsigned int	get_SubClass() const
		{	return SubClass; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_MeshId(unsigned int _v)
		{	MeshId= _v; }

		unsigned int	get_MeshId() const
		{	return MeshId; }

		void	set_LevelReq(unsigned char _v)
		{	LevelReq= _v; }

		unsigned char	get_LevelReq() const
		{	return LevelReq; }

		void	set_SexReq(unsigned char _v)
		{	SexReq= _v; }

		unsigned char	get_SexReq() const
		{	return SexReq; }

		void	set_MaxOverLap(unsigned int _v)
		{	MaxOverLap= _v; }

		unsigned int	get_MaxOverLap() const
		{	return MaxOverLap; }

		void	set_MaxOwnNum(unsigned int _v)
		{	MaxOwnNum= _v; }

		unsigned int	get_MaxOwnNum() const
		{	return MaxOwnNum; }

		void	set_CanInShop(unsigned char _v)
		{	CanInShop= _v; }

		unsigned char	get_CanInShop() const
		{	return CanInShop; }

		void	set_ServerScriptID(const char * _v)
		{	 strcpy( ServerScriptID ,  _v ); }

		const char *get_ServerScriptID() const
		{	return ServerScriptID; }

		void	set_ClientScriptID(const char * _v)
		{	 strcpy( ClientScriptID ,  _v ); }

		const char *get_ClientScriptID() const
		{	return ClientScriptID; }

		void	set_ActionReq(unsigned int _v)
		{	ActionReq= _v; }

		unsigned int	get_ActionReq() const
		{	return ActionReq; }

		void	set_SortAction(unsigned int _v)
		{	SortAction= _v; }

		unsigned int	get_SortAction() const
		{	return SortAction; }


	protected:

/// 物品ID 
		unsigned int	ConfigID; 
/// 物品子类型 
		unsigned int	SubClass; 
/// 物品名 
		char Name[512]; 
/// 模型ResourceId 
		unsigned int	MeshId; 
/// 等级需求 
		unsigned char	LevelReq; 
/// 性别需求 
		unsigned char	SexReq; 
/// 最大重叠个数 
		unsigned int	MaxOverLap; 
/// 最大拥有个数 
		unsigned int	MaxOwnNum; 
/// 是否可以出现在商城中 
		unsigned char	CanInShop; 
/// 物品关联的服务器脚本 
		char ServerScriptID[512]; 
/// 物品关联的客户端脚本 
		char ClientScriptID[512]; 
/// 动作需求 
		unsigned int	ActionReq; 
/// 动作排序 
		unsigned int	SortAction; 

	};


#pragma pack() 

}



#endif
