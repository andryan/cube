/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Expression_H_CPPCODE
#define Expression_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Expression
	{
	public:

		MODI_Expression() {} 
		~MODI_Expression() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_D1(const char * _v)
		{	 strcpy( D1 ,  _v ); }

		const char *get_D1() const
		{	return D1; }

		void	set_F1(unsigned short _v)
		{	F1= _v; }

		unsigned short	get_F1() const
		{	return F1; }

		void	set_FS1(unsigned short _v)
		{	FS1= _v; }

		unsigned short	get_FS1() const
		{	return FS1; }

		void	set_M1(unsigned short _v)
		{	M1= _v; }

		unsigned short	get_M1() const
		{	return M1; }

		void	set_MS1(unsigned short _v)
		{	MS1= _v; }

		unsigned short	get_MS1() const
		{	return MS1; }

		void	set_D2(const char * _v)
		{	 strcpy( D2 ,  _v ); }

		const char *get_D2() const
		{	return D2; }

		void	set_F2(unsigned short _v)
		{	F2= _v; }

		unsigned short	get_F2() const
		{	return F2; }

		void	set_FS2(unsigned short _v)
		{	FS2= _v; }

		unsigned short	get_FS2() const
		{	return FS2; }

		void	set_M2(unsigned short _v)
		{	M2= _v; }

		unsigned short	get_M2() const
		{	return M2; }

		void	set_MS2(unsigned short _v)
		{	MS2= _v; }

		unsigned short	get_MS2() const
		{	return MS2; }

		void	set_D3(const char * _v)
		{	 strcpy( D3 ,  _v ); }

		const char *get_D3() const
		{	return D3; }

		void	set_F3(unsigned short _v)
		{	F3= _v; }

		unsigned short	get_F3() const
		{	return F3; }

		void	set_FS3(unsigned short _v)
		{	FS3= _v; }

		unsigned short	get_FS3() const
		{	return FS3; }

		void	set_M3(unsigned short _v)
		{	M3= _v; }

		unsigned short	get_M3() const
		{	return M3; }

		void	set_MS3(unsigned short _v)
		{	MS3= _v; }

		unsigned short	get_MS3() const
		{	return MS3; }

		void	set_D4(const char * _v)
		{	 strcpy( D4 ,  _v ); }

		const char *get_D4() const
		{	return D4; }

		void	set_F4(unsigned short _v)
		{	F4= _v; }

		unsigned short	get_F4() const
		{	return F4; }

		void	set_FS4(unsigned short _v)
		{	FS4= _v; }

		unsigned short	get_FS4() const
		{	return FS4; }

		void	set_M4(unsigned short _v)
		{	M4= _v; }

		unsigned short	get_M4() const
		{	return M4; }

		void	set_MS4(unsigned short _v)
		{	MS4= _v; }

		unsigned short	get_MS4() const
		{	return MS4; }

		void	set_D5(const char * _v)
		{	 strcpy( D5 ,  _v ); }

		const char *get_D5() const
		{	return D5; }

		void	set_F5(unsigned short _v)
		{	F5= _v; }

		unsigned short	get_F5() const
		{	return F5; }

		void	set_FS5(unsigned short _v)
		{	FS5= _v; }

		unsigned short	get_FS5() const
		{	return FS5; }

		void	set_M5(unsigned short _v)
		{	M5= _v; }

		unsigned short	get_M5() const
		{	return M5; }

		void	set_MS5(unsigned short _v)
		{	MS5= _v; }

		unsigned short	get_MS5() const
		{	return MS5; }

		void	set_D6(const char * _v)
		{	 strcpy( D6 ,  _v ); }

		const char *get_D6() const
		{	return D6; }

		void	set_F6(unsigned short _v)
		{	F6= _v; }

		unsigned short	get_F6() const
		{	return F6; }

		void	set_FS6(unsigned short _v)
		{	FS6= _v; }

		unsigned short	get_FS6() const
		{	return FS6; }

		void	set_M6(unsigned short _v)
		{	M6= _v; }

		unsigned short	get_M6() const
		{	return M6; }

		void	set_MS6(unsigned short _v)
		{	MS6= _v; }

		unsigned short	get_MS6() const
		{	return MS6; }

		void	set_D7(const char * _v)
		{	 strcpy( D7 ,  _v ); }

		const char *get_D7() const
		{	return D7; }

		void	set_F7(unsigned short _v)
		{	F7= _v; }

		unsigned short	get_F7() const
		{	return F7; }

		void	set_FS7(unsigned short _v)
		{	FS7= _v; }

		unsigned short	get_FS7() const
		{	return FS7; }

		void	set_M7(unsigned short _v)
		{	M7= _v; }

		unsigned short	get_M7() const
		{	return M7; }

		void	set_MS7(unsigned short _v)
		{	MS7= _v; }

		unsigned short	get_MS7() const
		{	return MS7; }

		void	set_D8(const char * _v)
		{	 strcpy( D8 ,  _v ); }

		const char *get_D8() const
		{	return D8; }

		void	set_F8(unsigned short _v)
		{	F8= _v; }

		unsigned short	get_F8() const
		{	return F8; }

		void	set_FS8(unsigned short _v)
		{	FS8= _v; }

		unsigned short	get_FS8() const
		{	return FS8; }

		void	set_M8(unsigned short _v)
		{	M8= _v; }

		unsigned short	get_M8() const
		{	return M8; }

		void	set_MS8(unsigned short _v)
		{	MS8= _v; }

		unsigned short	get_MS8() const
		{	return MS8; }


	protected:

/// 表情ID 
		unsigned short	ConfigID; 
/// 功能描述1 
		char D1[512]; 
/// 功能1 
		unsigned short	F1; 
/// 音效1 
		unsigned short	FS1; 
/// 功能1 
		unsigned short	M1; 
/// 音效1 
		unsigned short	MS1; 
/// 功能描述2 
		char D2[512]; 
/// 功能2 
		unsigned short	F2; 
/// 音效2 
		unsigned short	FS2; 
/// 功能2 
		unsigned short	M2; 
/// 音效2 
		unsigned short	MS2; 
/// 功能描述3 
		char D3[512]; 
/// 功能3 
		unsigned short	F3; 
/// 音效3 
		unsigned short	FS3; 
/// 功能3 
		unsigned short	M3; 
/// 音效3 
		unsigned short	MS3; 
/// 功能描述4 
		char D4[512]; 
/// 功能4 
		unsigned short	F4; 
/// 音效4 
		unsigned short	FS4; 
/// 功能4 
		unsigned short	M4; 
/// 音效4 
		unsigned short	MS4; 
/// 功能描述5 
		char D5[512]; 
/// 功能5 
		unsigned short	F5; 
/// 音效5 
		unsigned short	FS5; 
/// 功能5 
		unsigned short	M5; 
/// 音效5 
		unsigned short	MS5; 
/// 功能描述6 
		char D6[512]; 
/// 功能6 
		unsigned short	F6; 
/// 音效6 
		unsigned short	FS6; 
/// 功能6 
		unsigned short	M6; 
/// 音效6 
		unsigned short	MS6; 
/// 功能描述7 
		char D7[512]; 
/// 功能7 
		unsigned short	F7; 
/// 音效7 
		unsigned short	FS7; 
/// 功能7 
		unsigned short	M7; 
/// 音效7 
		unsigned short	MS7; 
/// 功能描述8 
		char D8[512]; 
/// 功能7 
		unsigned short	F8; 
/// 音效7 
		unsigned short	FS8; 
/// 功能8 
		unsigned short	M8; 
/// 音效8 
		unsigned short	MS8; 

	};


#pragma pack() 

}



#endif
