/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef LoadingTips_H_CPPCODE
#define LoadingTips_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_LoadingTips
	{
	public:

		MODI_LoadingTips() {} 
		~MODI_LoadingTips() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_Tips(const char * _v)
		{	 strcpy( Tips ,  _v ); }

		const char *get_Tips() const
		{	return Tips; }


	protected:

/// ID 
		unsigned short	ConfigID; 
/// �ַ������� 
		char Tips[512]; 

	};


#pragma pack() 

}



#endif
