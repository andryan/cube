/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Scenelist_H_CPPCODE
#define Scenelist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Scenelist
	{
	public:

		MODI_Scenelist() {} 
		~MODI_Scenelist() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Type(unsigned char _v)
		{	Type= _v; }

		unsigned char	get_Type() const
		{	return Type; }

		void	set_Small(unsigned char _v)
		{	Small= _v; }

		unsigned char	get_Small() const
		{	return Small; }

		void	set_Large(unsigned char _v)
		{	Large= _v; }

		unsigned char	get_Large() const
		{	return Large; }

		void	set_Offical(unsigned char _v)
		{	Offical= _v; }

		unsigned char	get_Offical() const
		{	return Offical; }

		void	set_SoundStudio(unsigned char _v)
		{	SoundStudio= _v; }

		unsigned char	get_SoundStudio() const
		{	return SoundStudio; }

		void	set_SceneName(const char * _v)
		{	 strcpy( SceneName ,  _v ); }

		const char *get_SceneName() const
		{	return SceneName; }

		void	set_Tips(const char * _v)
		{	 strcpy( Tips ,  _v ); }

		const char *get_Tips() const
		{	return Tips; }

		void	set_SceneIcon(const char * _v)
		{	 strcpy( SceneIcon ,  _v ); }

		const char *get_SceneIcon() const
		{	return SceneIcon; }

		void	set_MinIcon(const char * _v)
		{	 strcpy( MinIcon ,  _v ); }

		const char *get_MinIcon() const
		{	return MinIcon; }

		void	set_SongTitleImage(const char * _v)
		{	 strcpy( SongTitleImage ,  _v ); }

		const char *get_SongTitleImage() const
		{	return SongTitleImage; }

		void	set_pos1_x(float _v)
		{	pos1_x= _v; }

		float	get_pos1_x() const
		{	return pos1_x; }

		void	set_pos1_z(float _v)
		{	pos1_z= _v; }

		float	get_pos1_z() const
		{	return pos1_z; }

		void	set_pos2_x(float _v)
		{	pos2_x= _v; }

		float	get_pos2_x() const
		{	return pos2_x; }

		void	set_pos2_z(float _v)
		{	pos2_z= _v; }

		float	get_pos2_z() const
		{	return pos2_z; }

		void	set_Max_X(float _v)
		{	Max_X= _v; }

		float	get_Max_X() const
		{	return Max_X; }

		void	set_Max_Z(float _v)
		{	Max_Z= _v; }

		float	get_Max_Z() const
		{	return Max_Z; }

		void	set_Min_X(float _v)
		{	Min_X= _v; }

		float	get_Min_X() const
		{	return Min_X; }

		void	set_Min_Z(float _v)
		{	Min_Z= _v; }

		float	get_Min_Z() const
		{	return Min_Z; }

		void	set_SceneSound(unsigned char _v)
		{	SceneSound= _v; }

		unsigned char	get_SceneSound() const
		{	return SceneSound; }


	protected:

/// 地图ID 
		unsigned int	ConfigID; 
/// 场景类型 
		unsigned char	Type; 
/// 小房间 
		unsigned char	Small; 
/// 大房间 
		unsigned char	Large; 
/// 系统房间 
		unsigned char	Offical; 
/// 练歌房 
		unsigned char	SoundStudio; 
/// 地图名称 
		char SceneName[512]; 
/// 地图描述 
		char Tips[512]; 
/// 地图ICON 
		char SceneIcon[512]; 
/// 地图缩略ICON 
		char MinIcon[512]; 
/// 标题名称背景图 
		char SongTitleImage[512]; 
/// 位置1x 
		float	pos1_x; 
/// 位置1z 
		float	pos1_z; 
/// 位置2x 
		float	pos2_x; 
/// 位置2z 
		float	pos2_z; 
/// 最大移动X 
		float	Max_X; 
/// 最大移动Z 
		float	Max_Z; 
/// 最小移动X 
		float	Min_X; 
/// 最小移动Z 
		float	Min_Z; 
/// 音效ID 
		unsigned char	SceneSound; 

	};


#pragma pack() 

}



#endif
