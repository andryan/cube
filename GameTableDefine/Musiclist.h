/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Musiclist_H_CPPCODE
#define Musiclist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Musiclist
	{
	public:

		MODI_Musiclist() {} 
		~MODI_Musiclist() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_Songname(const char * _v)
		{	 strcpy( Songname ,  _v ); }

		const char *get_Songname() const
		{	return Songname; }

		void	set_Style(const char * _v)
		{	 strcpy( Style ,  _v ); }

		const char *get_Style() const
		{	return Style; }

		void	set_OriginalSinger(const char * _v)
		{	 strcpy( OriginalSinger ,  _v ); }

		const char *get_OriginalSinger() const
		{	return OriginalSinger; }

		void	set_Gender(unsigned char _v)
		{	Gender= _v; }

		unsigned char	get_Gender() const
		{	return Gender; }

		void	set_Tempo(float _v)
		{	Tempo= _v; }

		float	get_Tempo() const
		{	return Tempo; }

		void	set_Speed(unsigned char _v)
		{	Speed= _v; }

		unsigned char	get_Speed() const
		{	return Speed; }

		void	set_Duration(unsigned int _v)
		{	Duration= _v; }

		unsigned int	get_Duration() const
		{	return Duration; }

		void	set_Difficulty(unsigned char _v)
		{	Difficulty= _v; }

		unsigned char	get_Difficulty() const
		{	return Difficulty; }

		void	set_Pinyin_singer(const char * _v)
		{	 strcpy( Pinyin_singer ,  _v ); }

		const char *get_Pinyin_singer() const
		{	return Pinyin_singer; }

		void	set_Pinyin_song(const char * _v)
		{	 strcpy( Pinyin_song ,  _v ); }

		const char *get_Pinyin_song() const
		{	return Pinyin_song; }

		void	set_Acts(unsigned char _v)
		{	Acts= _v; }

		unsigned char	get_Acts() const
		{	return Acts; }

		void	set_StageEffect(unsigned char _v)
		{	StageEffect= _v; }

		unsigned char	get_StageEffect() const
		{	return StageEffect; }

		void	set_Date(const char * _v)
		{	 strcpy( Date ,  _v ); }

		const char *get_Date() const
		{	return Date; }

		void	set_recommend(unsigned char _v)
		{	recommend= _v; }

		unsigned char	get_recommend() const
		{	return recommend; }

		void	set_accompaniment(unsigned char _v)
		{	accompaniment= _v; }

		unsigned char	get_accompaniment() const
		{	return accompaniment; }

		void	set_DanceTime(float _v)
		{	DanceTime= _v; }

		float	get_DanceTime() const
		{	return DanceTime; }

		void	set_SingType(unsigned char _v)
		{	SingType= _v; }

		unsigned char	get_SingType() const
		{	return SingType; }

		void	set_HalfTime(float _v)
		{	HalfTime= _v; }

		float	get_HalfTime() const
		{	return HalfTime; }

		void	set_viewstart(float _v)
		{	viewstart= _v; }

		float	get_viewstart() const
		{	return viewstart; }

		void	set_viewover(float _v)
		{	viewover= _v; }

		float	get_viewover() const
		{	return viewover; }

		void	set_Label(const char * _v)
		{	 strcpy( Label ,  _v ); }

		const char *get_Label() const
		{	return Label; }

		void	set_Composer(const char * _v)
		{	 strcpy( Composer ,  _v ); }

		const char *get_Composer() const
		{	return Composer; }

		void	set_RunningText(const char * _v)
		{	 strcpy( RunningText ,  _v ); }

		const char *get_RunningText() const
		{	return RunningText; }

		void	set_MonthlySinger(unsigned char _v)
		{	MonthlySinger= _v; }

		unsigned char	get_MonthlySinger() const
		{	return MonthlySinger; }

		void	set_Language(unsigned char _v)
		{	Language= _v; }

		unsigned char	get_Language() const
		{	return Language; }

		void	set_Limit(unsigned char _v)
		{	Limit= _v; }

		unsigned char	get_Limit() const
		{	return Limit; }


	protected:

/// 歌曲ID 
		unsigned short	ConfigID; 
/// 歌曲名 
		char Songname[512]; 
/// 歌曲风格 
		char Style[512]; 
/// 歌手 
		char OriginalSinger[512]; 
/// 性别 
		unsigned char	Gender; 
/// BMP值 
		float	Tempo; 
/// 歌曲快慢 
		unsigned char	Speed; 
/// 歌曲时间 
		unsigned int	Duration; 
/// 难度 
		unsigned char	Difficulty; 
/// 歌手拼音 
		char Pinyin_singer[512]; 
/// 歌曲拼音 
		char Pinyin_song[512]; 
/// 动作表现 
		unsigned char	Acts; 
/// 场景效果 
		unsigned char	StageEffect; 
/// 歌曲日期 
		char Date[512]; 
/// 推荐 
		unsigned char	recommend; 
/// 伴奏 
		unsigned char	accompaniment; 
/// 起始时间 
		float	DanceTime; 
/// 类型 
		unsigned char	SingType; 
/// 半首结束时间 
		float	HalfTime; 
/// 预听起始时间 
		float	viewstart; 
/// 预听结束时间 
		float	viewover; 
/// 唱片公司 
		char Label[512]; 
/// 作曲 
		char Composer[512]; 
/// 歌曲流动信息 
		char RunningText[2048]; 
/// 月度推荐歌手 
		unsigned char	MonthlySinger; 
/// 语种 
		unsigned char	Language; 
/// 权限 
		unsigned char	Limit; 

	};


#pragma pack() 

}



#endif
