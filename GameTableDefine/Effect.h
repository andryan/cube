/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Effect_H_CPPCODE
#define Effect_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Effect
	{
	public:

		MODI_Effect() {} 
		~MODI_Effect() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_Description(unsigned short _v)
		{	Description= _v; }

		unsigned short	get_Description() const
		{	return Description; }

		void	set_Type(unsigned short _v)
		{	Type= _v; }

		unsigned short	get_Type() const
		{	return Type; }

		void	set_ContinueTime(unsigned int _v)
		{	ContinueTime= _v; }

		unsigned int	get_ContinueTime() const
		{	return ContinueTime; }

		void	set_StartEffect(unsigned int _v)
		{	StartEffect= _v; }

		unsigned int	get_StartEffect() const
		{	return StartEffect; }

		void	set_EndEffect(unsigned int _v)
		{	EndEffect= _v; }

		unsigned int	get_EndEffect() const
		{	return EndEffect; }

		void	set_ModelType(unsigned short _v)
		{	ModelType= _v; }

		unsigned short	get_ModelType() const
		{	return ModelType; }

		void	set_AffectedItem1(unsigned int _v)
		{	AffectedItem1= _v; }

		unsigned int	get_AffectedItem1() const
		{	return AffectedItem1; }

		void	set_AffectedItem2(unsigned int _v)
		{	AffectedItem2= _v; }

		unsigned int	get_AffectedItem2() const
		{	return AffectedItem2; }

		void	set_AffectedItem3(unsigned int _v)
		{	AffectedItem3= _v; }

		unsigned int	get_AffectedItem3() const
		{	return AffectedItem3; }

		void	set_AffectedItem4(unsigned int _v)
		{	AffectedItem4= _v; }

		unsigned int	get_AffectedItem4() const
		{	return AffectedItem4; }

		void	set_AffectedItem5(unsigned int _v)
		{	AffectedItem5= _v; }

		unsigned int	get_AffectedItem5() const
		{	return AffectedItem5; }

		void	set_AffectedItem6(unsigned int _v)
		{	AffectedItem6= _v; }

		unsigned int	get_AffectedItem6() const
		{	return AffectedItem6; }

		void	set_AffectedItem7(unsigned int _v)
		{	AffectedItem7= _v; }

		unsigned int	get_AffectedItem7() const
		{	return AffectedItem7; }

		void	set_AffectedItem8(unsigned int _v)
		{	AffectedItem8= _v; }

		unsigned int	get_AffectedItem8() const
		{	return AffectedItem8; }

		void	set_SoundId(unsigned int _v)
		{	SoundId= _v; }

		unsigned int	get_SoundId() const
		{	return SoundId; }

		void	set_Icon(const char * _v)
		{	 strcpy( Icon ,  _v ); }

		const char *get_Icon() const
		{	return Icon; }


	protected:

/// 效果ID 
		unsigned int	ConfigID; 
/// 效果名称 
		char Name[512]; 
/// 效果描述 
		unsigned short	Description; 
/// 效果类型 
		unsigned short	Type; 
/// 效果持续时间 
		unsigned int	ContinueTime; 
/// 初始效果动画 
		unsigned int	StartEffect; 
/// 结束效果动画 
		unsigned int	EndEffect; 
/// 模型类型 
		unsigned short	ModelType; 
/// 释放道具1 
		unsigned int	AffectedItem1; 
/// 释放道具2 
		unsigned int	AffectedItem2; 
/// 释放道具3 
		unsigned int	AffectedItem3; 
/// 释放道具4 
		unsigned int	AffectedItem4; 
/// 释放道具5 
		unsigned int	AffectedItem5; 
/// 释放道具6 
		unsigned int	AffectedItem6; 
/// 释放道具7 
		unsigned int	AffectedItem7; 
/// 释放道具8 
		unsigned int	AffectedItem8; 
/// 释放道具音效 
		unsigned int	SoundId; 
/// 图标 
		char Icon[512]; 

	};


#pragma pack() 

}



#endif
