/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef ParticleResource_H_CPPCODE
#define ParticleResource_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_ParticleResource
	{
	public:

		MODI_ParticleResource() {} 
		~MODI_ParticleResource() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_ParticleName(const char * _v)
		{	 strcpy( ParticleName ,  _v ); }

		const char *get_ParticleName() const
		{	return ParticleName; }


	protected:

/// ��ƷID 
		unsigned int	ConfigID; 
/// �������� 
		char ParticleName[512]; 

	};


#pragma pack() 

}



#endif
