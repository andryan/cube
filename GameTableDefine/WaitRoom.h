/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef WaitRoom_H_CPPCODE
#define WaitRoom_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_WaitRoom
	{
	public:

		MODI_WaitRoom() {} 
		~MODI_WaitRoom() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_SceneName(const char * _v)
		{	 strcpy( SceneName ,  _v ); }

		const char *get_SceneName() const
		{	return SceneName; }

		void	set_SceneType(unsigned int _v)
		{	SceneType= _v; }

		unsigned int	get_SceneType() const
		{	return SceneType; }

		void	set_LoadFile(const char * _v)
		{	 strcpy( LoadFile ,  _v ); }

		const char *get_LoadFile() const
		{	return LoadFile; }

		void	set_Icon(const char * _v)
		{	 strcpy( Icon ,  _v ); }

		const char *get_Icon() const
		{	return Icon; }


	protected:

/// 场景ID 
		unsigned int	ConfigID; 
/// 场景名称 
		char SceneName[512]; 
/// 场景类型 
		unsigned int	SceneType; 
/// 地图文件 
		char LoadFile[512]; 
/// 图标 
		char Icon[512]; 

	};


#pragma pack() 

}



#endif
