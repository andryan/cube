/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef BoxMessage_H_CPPCODE
#define BoxMessage_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_BoxMessage
	{
	public:

		MODI_BoxMessage() {} 
		~MODI_BoxMessage() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_Content(const char * _v)
		{	 strcpy( Content ,  _v ); }

		const char *get_Content() const
		{	return Content; }


	protected:

/// id 
		unsigned short	ConfigID; 
/// content 
		char Content[512]; 

	};


#pragma pack() 

}



#endif
