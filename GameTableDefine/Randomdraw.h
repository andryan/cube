/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Randomdraw_H_CPPCODE
#define Randomdraw_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Randomdraw
	{
	public:

		MODI_Randomdraw() {} 
		~MODI_Randomdraw() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Number(unsigned char _v)
		{	Number= _v; }

		unsigned char	get_Number() const
		{	return Number; }

		void	set_Interval(unsigned short _v)
		{	Interval= _v; }

		unsigned short	get_Interval() const
		{	return Interval; }

		void	set_Starting(unsigned int _v)
		{	Starting= _v; }

		unsigned int	get_Starting() const
		{	return Starting; }

		void	set_Distribution(unsigned int _v)
		{	Distribution= _v; }

		unsigned int	get_Distribution() const
		{	return Distribution; }

		void	set_Maximize(unsigned short _v)
		{	Maximize= _v; }

		unsigned short	get_Maximize() const
		{	return Maximize; }

		void	set_Arg1(int _v)
		{	Arg1= _v; }

		int	get_Arg1() const
		{	return Arg1; }

		void	set_Arg2(int _v)
		{	Arg2= _v; }

		int	get_Arg2() const
		{	return Arg2; }

		void	set_Arg3(int _v)
		{	Arg3= _v; }

		int	get_Arg3() const
		{	return Arg3; }

		void	set_Arg4(int _v)
		{	Arg4= _v; }

		int	get_Arg4() const
		{	return Arg4; }

		void	set_Arg5(int _v)
		{	Arg5= _v; }

		int	get_Arg5() const
		{	return Arg5; }

		void	set_Arg6(int _v)
		{	Arg6= _v; }

		int	get_Arg6() const
		{	return Arg6; }

		void	set_Arg7(int _v)
		{	Arg7= _v; }

		int	get_Arg7() const
		{	return Arg7; }

		void	set_Arg8(int _v)
		{	Arg8= _v; }

		int	get_Arg8() const
		{	return Arg8; }


	protected:

/// ?? 
		unsigned int	ConfigID; 
/// ?????? 
		unsigned char	Number; 
/// ???? 
		unsigned short	Interval; 
/// ???? 
		unsigned int	Starting; 
/// ???? 
		unsigned int	Distribution; 
/// ????? 
		unsigned short	Maximize; 
/// ??1 
		int	Arg1; 
/// ??2 
		int	Arg2; 
/// ??3 
		int	Arg3; 
/// ??4 
		int	Arg4; 
/// ??5 
		int	Arg5; 
/// ??6 
		int	Arg6; 
/// ??7 
		int	Arg7; 
/// ??8 
		int	Arg8; 

	};


#pragma pack() 

}



#endif
