/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Shop_H_CPPCODE
#define Shop_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Shop
	{
	public:

		MODI_Shop() {} 
		~MODI_Shop() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_SellItemID(unsigned short _v)
		{	SellItemID= _v; }

		unsigned short	get_SellItemID() const
		{	return SellItemID; }

		void	set_Name(const char * _v)
		{	 strcpy( Name ,  _v ); }

		const char *get_Name() const
		{	return Name; }

		void	set_Count(unsigned short _v)
		{	Count= _v; }

		unsigned short	get_Count() const
		{	return Count; }

		void	set_WhereSell(unsigned char _v)
		{	WhereSell= _v; }

		unsigned char	get_WhereSell() const
		{	return WhereSell; }

		void	set_BuyStrategy(unsigned char _v)
		{	BuyStrategy= _v; }

		unsigned char	get_BuyStrategy() const
		{	return BuyStrategy; }

		void	set_BS_1_Time1(unsigned int _v)
		{	BS_1_Time1= _v; }

		unsigned int	get_BS_1_Time1() const
		{	return BS_1_Time1; }

		void	set_BS_1_Time2(unsigned int _v)
		{	BS_1_Time2= _v; }

		unsigned int	get_BS_1_Time2() const
		{	return BS_1_Time2; }

		void	set_BS_1_Time3(unsigned int _v)
		{	BS_1_Time3= _v; }

		unsigned int	get_BS_1_Time3() const
		{	return BS_1_Time3; }

		void	set_BS_1_Time4(unsigned int _v)
		{	BS_1_Time4= _v; }

		unsigned int	get_BS_1_Time4() const
		{	return BS_1_Time4; }

		void	set_BS_2_Time1(unsigned int _v)
		{	BS_2_Time1= _v; }

		unsigned int	get_BS_2_Time1() const
		{	return BS_2_Time1; }

		void	set_BS_2_Time2(unsigned int _v)
		{	BS_2_Time2= _v; }

		unsigned int	get_BS_2_Time2() const
		{	return BS_2_Time2; }

		void	set_BS_2_Time3(unsigned int _v)
		{	BS_2_Time3= _v; }

		unsigned int	get_BS_2_Time3() const
		{	return BS_2_Time3; }

		void	set_BS_2_Time4(unsigned int _v)
		{	BS_2_Time4= _v; }

		unsigned int	get_BS_2_Time4() const
		{	return BS_2_Time4; }

		void	set_RecommendLevel(unsigned char _v)
		{	RecommendLevel= _v; }

		unsigned char	get_RecommendLevel() const
		{	return RecommendLevel; }

		void	set_IsNew(unsigned char _v)
		{	IsNew= _v; }

		unsigned char	get_IsNew() const
		{	return IsNew; }


	protected:

/// 商城商品ID 
		unsigned int	ConfigID; 
/// 卖的是哪种物品 
		unsigned short	SellItemID; 
/// 商品名称 
		char Name[512]; 
/// 可重叠物品的重叠数量 
		unsigned short	Count; 
/// 哪里有卖？(1为普通商店，2为神秘商店) 
		unsigned char	WhereSell; 
/// 物品购买策略(1为金币，2为人民币) 
		unsigned char	BuyStrategy; 
/// 使用金币购买,期限3天所需要的数量 
		unsigned int	BS_1_Time1; 
/// 使用金币购买,期限7天所需要的数量 
		unsigned int	BS_1_Time2; 
/// 使用金币购买,期限30天所需要的数量 
		unsigned int	BS_1_Time3; 
/// 使用金币购买,期限永久所需要的数量 
		unsigned int	BS_1_Time4; 
/// 使用M币购买,期限3天所需要的数量 
		unsigned int	BS_2_Time1; 
/// 使用M币购买,期限7天所需要的数量 
		unsigned int	BS_2_Time2; 
/// 使用M币购买,期限30天所需要的数量 
		unsigned int	BS_2_Time3; 
/// 使用M币购买,期限永久所需要的数量 
		unsigned int	BS_2_Time4; 
/// 物品推荐程度 
		unsigned char	RecommendLevel; 
/// 是否新品 
		unsigned char	IsNew; 

	};


#pragma pack() 

}



#endif
