/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef Constellationlist_H_CPPCODE
#define Constellationlist_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_Constellationlist
	{
	public:

		MODI_Constellationlist() {} 
		~MODI_Constellationlist() {}

	public:

		void	set_ConfigID(unsigned char _v)
		{	ConfigID= _v; }

		unsigned char	get_ConfigID() const
		{	return ConfigID; }

		void	set_ConstellationName(const char * _v)
		{	 strcpy( ConstellationName ,  _v ); }

		const char *get_ConstellationName() const
		{	return ConstellationName; }


	protected:

/// ����ID 
		unsigned char	ConfigID; 
/// �������� 
		char ConstellationName[512]; 

	};


#pragma pack() 

}



#endif
