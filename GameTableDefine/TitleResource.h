/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef TitleResource_H_CPPCODE
#define TitleResource_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_TitleResource
	{
	public:

		MODI_TitleResource() {} 
		~MODI_TitleResource() {}

	public:

		void	set_ConfigID(unsigned int _v)
		{	ConfigID= _v; }

		unsigned int	get_ConfigID() const
		{	return ConfigID; }

		void	set_Description(const char * _v)
		{	 strcpy( Description ,  _v ); }

		const char *get_Description() const
		{	return Description; }

		void	set_IconId(const char * _v)
		{	 strcpy( IconId ,  _v ); }

		const char *get_IconId() const
		{	return IconId; }


	protected:

/// ID 
		unsigned int	ConfigID; 
/// ���� 
		char Description[512]; 
/// ICON·�� 
		char IconId[512]; 

	};


#pragma pack() 

}



#endif
