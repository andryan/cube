/** 
 this file create by Compiler.plz do not edit it!.
 Compiler Version : 0.0.1 
 
 */ 

#ifndef UIInfo_H_CPPCODE
#define UIInfo_H_CPPCODE



namespace GameTable	
{ 

#pragma pack(1) 
	class	MODI_UIInfo
	{
	public:

		MODI_UIInfo() {} 
		~MODI_UIInfo() {}

	public:

		void	set_ConfigID(unsigned short _v)
		{	ConfigID= _v; }

		unsigned short	get_ConfigID() const
		{	return ConfigID; }

		void	set_Content(const char * _v)
		{	 strcpy( Content ,  _v ); }

		const char *get_Content() const
		{	return Content; }


	protected:

/// id 
		unsigned short	ConfigID; 
/// ���� 
		char Content[512]; 

	};


#pragma pack() 

}



#endif
