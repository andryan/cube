/** 
 * @file GameHelpFun.h
 * @brief 游戏中的一些帮助函数
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-24
 */

#ifndef GAME_HELP_FUNCTION_H_
#define GAME_HELP_FUNCTION_H_


class 	MODI_GameHelpFun
{


	public:

	static bool	StrSafeLength(const char * pIn , unsigned int nLength , unsigned int & nSafeLen )
	{
		nSafeLen = 0;
		if( !pIn || !nLength )
			return false;

		for(unsigned int i = 0 ;i<nLength;i++)
		{
			if( pIn[i] == '\0' ) 
			{
				nSafeLen = i;
				return true;
			}
		}

		return false;
	}


	/** 
	 * @brief 字符串安全性的检查
	 * 
	 * @param pIn 字符串内容
	 * @param nLength 长度
	 * 
	 * @return 安全返回TRUE	
	 */
	static bool	StrSafeCheck(const char * pIn , unsigned int nLength , size_t * outSize )
	{
		if( !pIn || !nLength )
			return false;

		for(unsigned int i = 0 ;i<nLength;i++)
		{
			switch(pIn[i]) 
			{
				case '\0':
					{
						if( outSize )
							*outSize = i + 1;
						return true;
						break;
					}
				case '\'':
				case '\"':
				case ')':
				case '(':
				case '=':
				case '%':
				case ' ':
					{
						return false;
					}
			}
		}

		return false;
	}

	static bool CharacterNameSafeCheck( const char * pIn , unsigned int nLength , unsigned int & nLen )
	{
/*int bad=0;*/
		if( !pIn || !nLength )
			return false;

		for(unsigned int i = 0 ;i < nLength; i++)
		{
/*
			switch(pIn[i]) 
			{
				case '\0':
					{
						nLen = i + 1;
						return true;
						break;
					}
				case '\'':
				case '\"':
				case ')':
				case '(':
				case '=':
				case '%':
				case ' ':
				case '\n':
					{
						return false;
					}
			}
bad=0;
if(pIn[i] == '\0') {
  nLen = i + 1;
  return true;
  break;
}
if(pIn[i] == '\'' || pIn[i] == '\"' || pIn[i] == ')' || pIn[i] == '(' || pIn[i] == '=' || pIn[i] == '%' || pIn[i] == ' ' || pIn[i] == '\n') {
  return false;
}
if(!isprint(pIn[i])) {
  bad=1;
  return false;
}
*/
			if( pIn[i] == '\0' ) {
				nLen = i + 1;
				return true;
				break;
			}

			if( (pIn[i] >= 'a' && pIn[i] <= 'z') ||
				(pIn[i] >= 'A' && pIn[i] <= 'Z') ||
				(pIn[i] >= '0' && pIn[i] <= '9') ||
				(pIn[i] == '_' ) || (pIn[i] == '-' ) || (pIn[i] == '.' ) )
				continue ;

			return false;
		}

/*
if(bad>0) {
  Global::logger->info("[EXPLOIT] POSSIBLE BAD GUY: %s", *pIn);
}
*/
		return false;
	}

	/** 
	 * @brief 帐号安全性的检查
	 * 
	 * @param szAccount 帐号
	 * @param nLength 长度
	 * 
	 * @return 	安全返回TRUE
	 */
	static bool AccountSafeCheck( const char * pIn , unsigned int nLength )
	{
		if( !pIn || !nLength )
			return false;
		return true;

		// 帐号只能是数字+字母+'_'
		for(unsigned int i = 0 ;i < nLength;i++)
		{
			if( pIn[i] == '\0' )
				return true;

			if( (pIn[i] >= 'a' && pIn[i] <= 'z') ||
				(pIn[i] >= 'A' && pIn[i] <= 'Z') ||
				(pIn[i] >= '0' && pIn[i] <= '9') ||
				(pIn[i] == '_' ) )
				continue ;

			return false;
		}

		return false;
	}

	static bool PasswordSafeCheck( const char * pIn , unsigned int nLength )
	{
		if( !pIn || !nLength )
			return false;

		// 密码为MD5，只能出现数字和字母
		for(unsigned int i = 0 ;i < nLength;i++)
		{
			if( pIn[i] == '\0' )
				return true;

			if( (pIn[i] >= 'a' && pIn[i] <= 'z') ||
				(pIn[i] >= 'A' && pIn[i] <= 'Z') ||
				(pIn[i] >= '0' && pIn[i] <= '9') )
				continue ;

			return false;
		}

		return false;
	}

	static bool StringToLower( const char * pIn , unsigned int nLength , char * szOut )
	{
		if( !pIn || !nLength )
			return false;

		// 密码为MD5，只能出现数字和字母
		for(unsigned int i = 0 ;i < nLength;i++)
		{
			if( pIn[i] == '\0' )
			{
				szOut[i] = '\0';
				return true;
			}

			char c = tolower( pIn[i] );
			szOut[i] = c;
		}

		return false;
	}

	static bool Safe_Strncpy( char * pDest, size_t nDestSize , const char * pSrc , size_t nSrcSize )
	{
		if( pDest == pSrc )
			return false;

		size_t nSafeSize = nDestSize;
		if( nSafeSize < nSrcSize )
			nSafeSize = nSrcSize;
		strncpy( pDest , pSrc , nSafeSize );
		return true;
	}


};



#endif
