/**
 * @file stdefhelp.h
 * @date 2009/12/30
 * @version $Id $
 * @author tangteng tangteng@modi.com
 * @brief
 *		产生能够被序列华的结构体的帮助宏定义文件
 */

#ifndef STRUCT_DEFINED_HELP_H_
#define STRUCT_DEFINED_HELP_H_

#ifdef _MSC_VER
#pragma once 
#endif

//#include "TStream/stream.h"
//
//template <typename T>
//inline TSTREAM::CStream &   operator << ( TSTREAM::CStream & s , const T & obj ) { MODI_PackCmd( s , obj ); return s;}
//template <typename T>
//inline TSTREAM::CStream &   operator >> ( TSTREAM::CStream & s , T & obj ) { MODI_UnpackCmd( s , obj ); return s; }
//
//template <typename T>
//inline unsigned int MODI_PackCmd( TSTREAM::CStream & s , const T & obj  ) { return 0; }
//template <typename T>
//inline unsigned int MODI_UnpackCmd( TSTREAM::CStream & s , const T & obj  ) { return 0; }

#endif
