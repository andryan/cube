/**
 * @file   InfoServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Sep  9 10:40:29 2010
 * @version $Id $
 * @brief  程序入口
 * 
 */

#include "UnifyServerAPP.h"

int main(int argc, char * argv[])
{
	MODI_UnifyServerAPP app("UnifyServer");

	int iRet = app.Init();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	app.Shutdown();

	::sleep(1);

	return iRet;

}

