/**
 * @file   UnifyServerAPP.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 16:19:28 2011
 * 
 * @brief  平台服务器
 * 
 * 
 */

#include "UnifyServerAPP.h"
#include <fstream>
#include "Share.h"
#include "DBClient.h"
#include "UnifyService.h"
#include "UnifyTick.h"
#include "ZoneTask.h"

MODI_UnifyServerAPP::MODI_UnifyServerAPP( const char * szAppName ):MODI_IAppFrame( szAppName )
{
	
}


MODI_UnifyServerAPP::~MODI_UnifyServerAPP()
{
	
}

/** 
 * @brief 初始化
 * 
 * @return 返回enum
 *
 */
int MODI_UnifyServerAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
	{
		Global::logger->fatal("[system_init] iappframe init failed <infoservice>");
		return iRet;
	}

	const MODI_SvrUnifyConfig  * pUnifyConfig = (const MODI_SvrUnifyConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_UNIFYSERVICE ));
	if( !pUnifyConfig )
	{
		Global::logger->debug("not get config unifyservice");
		return MODI_IAppFrame::enConfigInvaild;
	}

	// 增加一个日志文件到指定目录
    Global::logger->AddLocalFileLog(pUnifyConfig->m_strLogPath.c_str());
	std::string net_log_path = pUnifyConfig->m_strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());

	std::vector<WORD> word_vec;
	word_vec.push_back(pUnifyConfig->m_wdUnifyPort);
	word_vec.push_back(pUnifyConfig->m_wdZonePort);

	std::vector<std::string> ip_vec;
	ip_vec.push_back( pUnifyConfig->m_strUnifyIP.c_str() );
	ip_vec.push_back( pUnifyConfig->m_strZoneInIP.c_str() );
	m_pNetService = new MODI_UnifyService(word_vec, ip_vec);
	
	if( !m_pNetService )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	// 连接游戏数据库
	if(! MODI_DBManager::GetInstance().Init(pUnifyConfig->m_strURL))
	{
		Global::net_logger->debug("not connect db");
		return MODI_IAppFrame::enConfigInvaild;
	}

	MODI_ZoneTask::m_pAccTbl = MODI_DBManager::GetInstance().GetTable("accountstbl");
	if(! MODI_ZoneTask::m_pAccTbl)
	{
		Global::logger->fatal("[%s] Unable get accountstbl table struct", SYS_INIT);
		return MODI_IAppFrame::enConfigInvaild;
	}

	if(! MODI_UnifyTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization unify client");
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	return MODI_IAppFrame::enOK;
}


int MODI_UnifyServerAPP::Run()
{
	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();
	Global::net_logger->RemoveConsoleLog();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;

	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );
	
	if( m_pNetService )
	{
		m_pNetService->Main();
	}
	else 
	{
		Global::logger->info("[%s] start game server faild. net service moudle not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistNetService;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_UnifyServerAPP::Shutdown()
{
	if( m_pNetService )
	{
		m_pNetService->Terminate();
		delete m_pNetService;
		m_pNetService = NULL;
	}

	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}
