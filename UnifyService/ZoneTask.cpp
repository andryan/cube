/**
 * @file   ZoneTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 18:35:41 2011
 * 
 * @brief  和游戏区的链接
 * 
 * 
 */

#include "ZoneTask.h"
#include "DBClient.h"
#include "UnifyTask.h"
#include "s2unify_cmd.h"
#include "ZoneTaskManager.h"
#include "UnifyTaskManager.h"
#include "ChenMiManager.h"

MODI_TableStruct * MODI_ZoneTask::m_pAccTbl = NULL;

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_ZoneTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif

	
	//#ifdef _DISABLE_CHENMI	
	/// 沉迷相关命令处理,提交给tick线程处理
	//if(pt_null_cmd->byCmd == MAINCMD_S2UNIFY)
	//{
		Put(pt_null_cmd, cmd_size);
		return true;
	//}
	//#endif	
	return true;
}


bool MODI_ZoneTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] deal zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif
	
	/// 沉迷相关命令处理
	if(pt_null_cmd->byCmd == MAINCMD_S2UNIFY)
	{
		switch(pt_null_cmd->byParam)
		{
			/// 防沉迷用户登录
			case MODI_S2Unify_Onlogin_Cmd::ms_SubCmd :
			{
				const MODI_S2Unify_Onlogin_Cmd * recv_cmd = (const MODI_S2Unify_Onlogin_Cmd *)pt_null_cmd;
				MODI_ChenMiManager::GetInstance().OnLogin(recv_cmd->m_dwAccID, GetZoneID());
				/// 增加一个开始提示
				MODI_Unify2S_ChenMiResult_Cmd send_cmd;
				send_cmd.m_enResult = enChenMi_Begin;
				send_cmd.m_dwAccID = recv_cmd->m_dwAccID;
				SendCmd(&send_cmd, sizeof(send_cmd));
				return true;
			}
			break;

			/// 防沉迷用户登出
			case MODI_S2Unify_Onlogout_Cmd::ms_SubCmd :
			{
				const MODI_S2Unify_Onlogout_Cmd * recv_cmd = (const MODI_S2Unify_Onlogout_Cmd *)pt_null_cmd;
				MODI_ChenMiManager::GetInstance().OnLogout(recv_cmd->m_dwAccID, GetZoneID());
				return true;
			}
			break;

			/// gmtool 工具命令
			case MODI_ReturnChannelStatus_Cmd::ms_SubCmd:
			{
				/// 频道信息监控
				const MODI_ReturnChannelStatus_Cmd * p_recv_cmd = (const MODI_ReturnChannelStatus_Cmd *)pt_null_cmd;
				MODI_UnifyTask * p_unify_task = MODI_UnifyTaskManager::GetInstance().GetUnifyTask(p_recv_cmd->m_stSessionID);
				if(! p_unify_task)
				{
					MODI_ASSERT(0);
					return false;
				}
				/// value=&channel_set_num=&channel_num=&channel_name_1&channel_num_1=&channel_id_1=&channel_ip_1=&channel_port_1=.....
				std::ostringstream os;
				os << "value=1&channel_set_num=" << p_recv_cmd->m_wdMaxNum
				   << "&channel_num=" << p_recv_cmd->m_wdChannelNum;
				const MODI_ChannelStatusInfo * p_info = &(p_recv_cmd->m_pData[0]);
				for(WORD i=0; i<p_recv_cmd->m_wdChannelNum; i++)
				{
					os<< "&channel_id_" << i+1 << "=" << (WORD)p_info->m_nServerID;
					os<< "&channel_name_" << i+1 << "=" << PublicFun::StrEncode(p_info->m_cstrName);
					os<< "&channel_ip_" << i+1 << "=" << PublicFun::NumIP2StrIP(p_info->m_nGatewayIP);
					os<< "&channel_port_"<< i+1 << "=" << p_info->m_nGatewayPort;
					os<< "&channel_num_" << i+1 << "=" << p_info->m_wdNum;
					p_info++;
				}
				p_unify_task->RetUnifyResult(os.str().c_str());
			}
			break;

			/// gmtool 都某个用户的操作
			case MODI_S2Unify_ReturnOpteate_Cmd::ms_SubCmd:
			{
				const MODI_S2Unify_ReturnOpteate_Cmd * p_recv_cmd = (const MODI_S2Unify_ReturnOpteate_Cmd *)pt_null_cmd;
				MODI_UnifyTask * p_unify_task = MODI_UnifyTaskManager::GetInstance().GetUnifyTask(p_recv_cmd->m_stSessionID);
				if(! p_unify_task)
				{
					//MODI_ASSERT(0);
					return false;
				}
				
				std::ostringstream os;
				os<< "value="<< (WORD)p_recv_cmd->m_byValue << "&name=" <<p_recv_cmd->m_strName;
				p_unify_task->RetUnifyResult(os.str().c_str());
			}
			break;
			
			default:
			{
				Global::logger->fatal("zonetask recv a invalid paycmd param %d", pt_null_cmd->byParam);
				return false;
			}
			break;
		}
		return true;
	}
	
	return true;
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_ZoneTask::RecycleConn()
{
	return 1;
}


void MODI_ZoneTask::AddTaskToManager()
{
	MODI_ZoneTaskManager::GetInstance().AddTask(this);
}


void MODI_ZoneTask::RemoveFromManager()
{
	MODI_ZoneTaskManager::GetInstance().RemoveTask(this);
}
