/**
 * @file   ZoneTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:47:04 2011
 * 
 * @brief  区连接
 * 
 * 
 */



#ifndef _MD_ZONETASK_H
#define _MD_ZONETASK_H

#include "DBStruct.h"
#include "DBClient.h"
#include "ServiceTask.h"
#include "CommandQueue.h"
#include "TransCommand.h"

/**
 * @brief gm工具客户端类
 * 
 */
class MODI_ZoneTask: public MODI_ServiceTask, public MODI_CmdParse
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sock 相关的sock
	 * @param addr 相关的地址
	 * 
	 */
	MODI_ZoneTask(const int sock, const struct sockaddr_in * addr): MODI_ServiceTask(sock, addr)
	{
		Resize(GSTOZS_CMDSIZE);
		m_wdZoneID = 0;	
	}

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true 失败false
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	/** 
	 * @brief 是否可以回收
	 * 
	 * 
	 * @return 1可以回收 0继续等待
	 */
	int RecycleConn();

	/// 增加到管理器
	void AddTaskToManager();

	void RemoveFromManager();

	void SetZoneID(const WORD & zone_id)
	{
		m_wdZoneID = zone_id;
	}

	void SetZoneName(const char * zone_name)
	{
		m_strName = zone_name;
	}
	
	const WORD GetZoneID()
	{
		return m_wdZoneID;
	}

	const char * GetZoneName()
	{
		return m_strName.c_str();
	}

	/// 各个表指针
	static MODI_TableStruct * m_pAccTbl;

 private:
	/// 区号
	WORD m_wdZoneID;
	std::string m_strName;
};

#endif
