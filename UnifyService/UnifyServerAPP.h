/**
 * @file   InfoServerAPP.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 10:00:40 2011
 * 
 * @brief  信息服务器
 * 
 * 
 */


#ifndef _MD_INFOSERVERAPP_H
#define _MD_INFOSERVERAPP_H

#include "IAppFrame.h"
#include "UnifyService.h"

/**
 * @brief 信息服务器框架
 * 
 */
class MODI_UnifyServerAPP: public MODI_IAppFrame
{
 public:
	explicit MODI_UnifyServerAPP( const char * szAppName );
	virtual ~MODI_UnifyServerAPP();
	virtual int Init();
	virtual int Run();
	virtual int Shutdown();
};

#endif
