/**
 * @file   UnifyService.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:42:08 2011
 * 
 * @brief  平台服务器
 * 
 * 
 */

#include "TaskQueue.h"
#include "UnifyService.h"
#include "UnifyTask.h"
#include "ZoneTask.h"
#include "FunctionTime.h"
#include "ServerConfig.h"

MODI_UnifyService * MODI_UnifyService::m_pInstance = NULL;
MODI_TaskQueue * MODI_UnifyService::m_pUnifyTaskQueue = NULL;

/** 
 * @brief 创建新任务
 * 
 * @param sock 任务相关的task
 * @param addr 任务相关的地址
 * @param port 任务相关的端口
 * 
 * @return 成功true 失败false
 */		
bool MODI_UnifyService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	/// 增加一个充值http连接
	if(port == m_wdToolPort)
	{
		if(sock == -1 || addr == NULL)
		{
			return false;
		}
		
		std::string sync_ip = inet_ntoa(addr->sin_addr);
		if(!(sync_ip == "220.248.106.210" || sync_ip == "221.181.65.130" || sync_ip == "221.181.65.133" || sync_ip == "58.221.37.76"))
		{
			Global::logger->fatal("[sync_ip_error] <syncip=%s>", sync_ip.c_str());
			
			//#ifndef _DEBUG			
			//			TEMP_FAILURE_RETRY(close(sock));
			//			return false;
			//#endif			
		}

		MODI_UnifyTask * p_task = (MODI_UnifyTask *)(m_pUnifyTaskQueue->Get());//new MODI_InfoTask(sock, addr);
		if(p_task)
		{
			p_task->Init(sock, addr, m_pUnifyTaskQueue);
			m_stUnifyTaskSched.AddNormalSched(p_task);
		}
		else
		{
			TEMP_FAILURE_RETRY(close(sock));
			Global::logger->fatal("[get_task] Unable get a info");
			return false;
		}
		return true;
	}
	/// 增加一个游戏服务器连接
	else if(port == m_wdZonePort)
	{
		if(sock == -1 || addr == NULL)
			return false;

		std::string task_ip = inet_ntoa(addr->sin_addr);
		WORD zone_id = 0;
		std::string zone_name;
		if(! CheckTaskIP(task_ip.c_str(), zone_id,zone_name))
		{
			Global::logger->fatal("[task_ip_error] <taskip=%s>", task_ip.c_str());
 			TEMP_FAILURE_RETRY(close(sock));
 			return false;
		}
		
		MODI_ZoneTask * p_task = new MODI_ZoneTask(sock, addr);
		if(p_task == NULL)
		{
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}

		p_task->SetZoneName(zone_name.c_str());
		p_task->SetZoneID(zone_id);
		
		if(m_stZoneTaskSched.AddNormalSched(p_task))
		{
			return true;
		}
		delete p_task;
		TEMP_FAILURE_RETRY(close(sock));
		return false;
	}
	else
	{
		if(sock != -1)
			TEMP_FAILURE_RETRY(close(sock));
	}
	return false;
}


/** 
 * @brief 初始化
 * 
 * @return 成功true 失败false
 */
bool MODI_UnifyService::Init()
{
	if(! MODI_MNetService::Init())
		return false;
	m_stUnifyTaskSched.Init();
	m_stZoneTaskSched.Init();
	
	m_pUnifyTaskQueue = new MODI_TaskQueue();
	for(int i = 0; i < 100; i++)
	{
		usleep(100);
		MODI_UnifyTask * p_task = new MODI_UnifyTask(m_pUnifyTaskQueue);
		m_pUnifyTaskQueue->Put(p_task);
	}
	
	return true;
}

/** 
 * @brief 释放资源
 * 
 */
void MODI_UnifyService::Final()
{
	MODI_MNetService::Final();
}


bool MODI_UnifyService::CheckTaskIP(const char * task_ip, WORD & zone_id, std::string & zone_name)
{
	zone_id = 0;
	const MODI_SvrUnifyConfig  * pInfoConfig = (const MODI_SvrUnifyConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_UNIFYSERVICE ));
	if( !pInfoConfig )
	{
		Global::logger->debug("not get config unifyservice");
		return false;
	}
	std::string ip = task_ip;
	std::map<WORD, std::string>::const_iterator iter = pInfoConfig->m_ZoneInfoMap.begin();
	for(; iter != pInfoConfig->m_ZoneInfoMap.end(); iter++)
	{
	   	if(ip == iter->second)
		{
			zone_id = iter->first;
			break;
		}
	}

	if(zone_id == 0)
	{
		return false;
	}

	std::map<WORD, std::string>::const_iterator iter1 = pInfoConfig->m_ZoneNameMap.find(zone_id);
	if(iter1 != pInfoConfig->m_ZoneNameMap.end())
	{
		zone_name = iter1->second;
	}
	else
	{
		return false;
	}
	return true;
}


/** 
 * @brief 重新加载配置文件
 * 
 */
void MODI_UnifyService::ReloadConfig()
{
	MODI_ServerConfig::GetInstance().Unload();
	MODI_ServerConfig::GetInstance().Load("./Config/config.xml");
	Global::logger->info("[reload_info] reload unifyservice config successful");
}
