/**
 * @file   ChenMiManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 14:53:50 2011
 * 
 * @brief  沉迷管理
 * 
 * 
 */

#include "DBStruct.h"
#include "DBClient.h"
#include "ChenMiManager.h"
#include "UnifyTask.h"
#include "ZoneTask.h"
#include "protocol/gamedefine.h"
#include "s2unify_cmd.h"
#include "ZoneTaskManager.h"

MODI_ChenMiManager * MODI_ChenMiManager::m_pInstance = NULL;


/** 
 * @brief 是否登录
 * 
 * @return 登录true
 *
 */
bool MODI_CardAllUser::IsOnline()
{
	return m_blOnline;
}

/** 
 * @brief 某个沉迷用户登录
 * 
 * @param acc_id 账号id
 * @param zone_id 区id
 *
 */
void MODI_CardAllUser::OnLogin(const defAccountID & acc_id, const WORD & zone_id)
{
	/// 看下线的时间
	if(! IsOnline())
	{
		DWORD cur_time = MODI_ChenMiManager::GetInstance().GetCurSec();
		if(cur_time >= m_dwLogoutTime)
		{
			m_dwOutlineTime += (cur_time - m_dwLogoutTime);
#ifdef _HRX_DEBUG			
			Global::logger->debug("[outline_time] user outline time <accid=%u,zoneid=%d,alltime=%u>", acc_id, zone_id, m_dwOutlineTime);
#endif			
		}
		else
		{
			MODI_ASSERT(0);
		}
		if(m_dwOutlineTime > RESET_CHENMI_TIME)			
		{
			
#ifdef _HRX_DEBUG
			Global::logger->debug("[acc_chenmi] user reset chenmi time <accid=%u, zoneid=%d>", acc_id, zone_id);
#endif			
			Reset();
		}
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[acc_chenmi] user login <accid=%u,zoneid=%d>", acc_id, zone_id);
#endif
	
	m_blOnline = true;
	m_AccUserMap[acc_id][zone_id].OnLogin();
}

/** 
 * @brief 沉迷用户登录
 * 
 */
void MODI_ChenMiManager::OnLogin(const defAccountID & acc_id, const WORD & zone_id)
{
	std::string card_num;
	if(! FindCardNum(acc_id, card_num))
	{
		/// load cardnum from db
		MODI_RecordContainer record_select;
		MODI_Record select_field;
		select_field.Put("cardnum");
		
		std::ostringstream where;
		where << "account_id=\'" << acc_id << "\'";
		
		MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
		if(! p_dbclient)
		{
			Global::logger->fatal("[info_init] acc info manager not get dbclient");
			return;
		}
	
		int rec_count = p_dbclient->ExecSelect(record_select, MODI_ZoneTask::m_pAccTbl, where.str().c_str(), &select_field, false);
		
		if(rec_count != 1)
		{
			Global::logger->fatal("[chenmi] get count not current <acc=%u>", acc_id);
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			return;
			MODI_ASSERT(0);
		}
		
		MODI_Record * p_select_record = record_select.GetRecord(0);
		if(p_select_record == NULL)
		{
			MODI_ASSERT(0);
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			return;
		}

		MODI_VarType v_cardnum = p_select_record->GetValue("cardnum");
		
		if(v_cardnum.Empty())
		{
			MODI_ASSERT(0);
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			return;
		}
		/// get this account cardnum and add mem
		card_num = (const char *)v_cardnum;
		/// 空的身份证号特殊处理
		if(card_num == "null")
		{
			std::ostringstream os;
			os<< acc_id;
			card_num = os.str();
		}
		
		AddCardNum(acc_id, card_num.c_str());
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[acc_chenmi] user login <cardnum=%s,accid=%u,zoneid=%d>", card_num.c_str(), acc_id, zone_id);
#endif
	
	m_CardAllUserMap[card_num].OnLogin(acc_id, zone_id);
}


/** 
 * @brief 沉迷用户下线
 * 
 * @param acc_id 账号id
 * @param zoneid 区id
 *
 */
void MODI_CardAllUser::OnLogout(const defAccountID & acc_id, const WORD & zone_id)
{
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[acc_chenmi] user logout <accid=%u,zoneid=%d>", acc_id, zone_id);
#endif
	
	m_AccUserMap[acc_id][zone_id].OnLogout();
	/// 看是否全部下线
	if(IsAllUserLogout())
	{
		
#ifdef _HRX_DEBUG
		Global::logger->debug("[acc_chenmi] user all logout <accid=%u,zoneid=%d>", acc_id, zone_id);
#endif		
		m_blOnline = false;
		m_dwLogoutTime = MODI_ChenMiManager::GetInstance().GetCurSec();
	}
	
}

/** 
 * @brief 用户登出
 * 
 * @param card_num 用户的身份证
 *
 */
void MODI_ChenMiManager::OnLogout(const defAccountID & acc_id, const WORD & zone_id)
{
	std::string card_num;
	if(! FindCardNum(acc_id, card_num))
	{
		Global::logger->fatal("[chenmi_logout] not find a chenmi user <acc_id=%u,zone_id=%u>", acc_id, zone_id);
		//MODI_ASSERT(0);
	}
	m_CardAllUserMap[card_num].OnLogout(acc_id, zone_id);
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[acc_chenmi] user logout <cardnum=%s,accid=%u,zoneid=%d>", card_num.c_str(), acc_id, zone_id);
#endif
}


/** 
 * @brief 增加沉迷时间
 * 
 * @param chenmi_time 沉迷时间
 */
void MODI_CardAllUser::AddChenMiTime(const DWORD chenmi_time)
{
	m_dwOnlineTime += chenmi_time;
}


/** 
 * @brief 增加防沉迷的时间
 * 
 * @param acc_id 账号id
 * @param zone_id 区id
 * @param chenmi_time 在线时间
 */
void MODI_ChenMiManager::AddChenMiTime(const defAccountID & acc_id, const DWORD chenmi_time)
{
	std::string card_num;
	if(! FindCardNum(acc_id, card_num))
	{
		Global::logger->fatal("[chenmi_logout] not find a chenmi user <acc_id=%u>", acc_id);
		MODI_ASSERT(0);
	}
	m_CardAllUserMap[card_num].AddChenMiTime(chenmi_time);
}


/** 
 * @brief 增加一个沉迷相关
 * 
 * @param acc_id 沉迷的id
 * @param card_num 沉迷的身份证
 *
 */
void MODI_ChenMiManager::AddCardNum(const defAccountID & acc_id, const char * card_num)
{
	m_CardNumMap[acc_id] = card_num;
}


/** 
 * @brief 查找一个沉迷身份
 * 
 * @param acc_id 沉迷id
 * @param card_num 返回身份证
 * 
 * @return 成功true
 */
bool MODI_ChenMiManager::FindCardNum(const defAccountID & acc_id, std::string & card_num)
{
	std::map<defAccountID, std::string>::iterator iter = m_CardNumMap.find(acc_id);
	if(iter != m_CardNumMap.end())
	{
		card_num = iter->second;
		return true;
	}
	return false;
}


/** 
 * @brief 此身份证所有用户是否下线
 * 
 * 
 * @return  下线true
 */
bool MODI_CardAllUser::IsAllUserLogout()
{
	defAccUserMapIter acc_iter = m_AccUserMap.begin();
	for(; acc_iter != m_AccUserMap.end(); acc_iter++)
	{
		defChenMiUserMap & zone_map = acc_iter->second;
		defChenMiUserMapIter zone_iter = zone_map.begin();
		for(; zone_iter != zone_map.end(); zone_iter++)
		{
			if(zone_iter->second.IsOnline())
			{
				return false;
			}
		}
	}
	return true;
}


void MODI_ChenMiManager::UpDate()
{
	/// 1分钟更新一次
	m_stRTime.GetNow();
	defCardAllUserMapIter card_iter = m_CardAllUserMap.begin();
	for(; card_iter != m_CardAllUserMap.end(); card_iter++)
	{
		MODI_CardAllUser & card_user = card_iter->second;
		if(card_user.IsOnline())
		{
			card_user.UpDate();
		}
	}
}

void MODI_CardAllUser::UpDate()
{
	defAccUserMapIter acc_iter = m_AccUserMap.begin();
	for(; acc_iter != m_AccUserMap.end(); acc_iter++)
	{
		defChenMiUserMap & zone_map = acc_iter->second;
		defChenMiUserMapIter zone_iter = zone_map.begin();
		for(; zone_iter != zone_map.end(); zone_iter++)
		{
			if(zone_iter->second.IsOnline())
			{
				/// 此区在线,时间加1分钟
				m_dwOnlineTime++;
#ifdef _HRX_DEBUG
				Global::logger->debug("[chenmi_time] user add chenmi time <accid=%u,zoneid=%d>", acc_iter->first, zone_iter->first);
#endif				
			}
		}
	}

	enChenMiResult ret_result = enChenMi_None;
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[chenmi_time] user chenmi time <alltime=%u>", m_dwOnlineTime);
#endif				

// #ifdef _HRX_DEBUG
// 	///1个小时
// 	if((m_dwOnlineTime >= 1) && (!m_blNotifyOne))//60
// 	{
// 		m_blNotifyOne = true;
// 		ret_result = enChenMi_OneHour;
// 	}
// 	else if((m_dwOnlineTime >= 2) && (!m_blNotifyTwo))//2*60
// 	{
// 		m_blNotifyTwo = true;
// 		ret_result = enChenMi_TwoHour;
// 	}
// 	else if((m_dwOnlineTime >= 3) && (!m_blNotifyThree))//3*60
// 	{
// 		m_blNotifyThree = true;
// 		ret_result = enChenMi_ThreeHour;
// 	}
// 	if((m_dwOnlineTime >= 4) && (!m_blNotifyFour))//4*60
// 	{
// 		m_blNotifyFour = true;
// 		ret_result = enChenMi_FourHour;
// 	}
// 	else if((m_dwOnlineTime >=5) && (!m_blNotifyFourHalf))//4*60+30
// 	{
// 		m_blNotifyFourHalf = true;
// 		ret_result = enChenMi_FourHour;
// 	}
// 	else if(m_dwOnlineTime >= 6)//5 * 60
// 	{
// 		//if(m_stNotifyFinalTimer(MODI_ChenMiManager::GetInstance().GetCurMSec()))
// 		{
// 			ret_result = enChenMi_MoreThen_FiveHour;
// 		}
// 	}
// #else
	
	/// 1个小时
	if((m_dwOnlineTime >= 60) && (!m_blNotifyOne))//60
	{
		m_blNotifyOne = true;
		ret_result = enChenMi_OneHour;
	}
	else if((m_dwOnlineTime >= 2*60) && (!m_blNotifyTwo))//2*60
	{
		m_blNotifyTwo = true;
		ret_result = enChenMi_TwoHour;
	}
	else if((m_dwOnlineTime >= 3*60) && (!m_blNotifyThree))//3*60
	{
		m_blNotifyThree = true;
		ret_result = enChenMi_ThreeHour;
	}
	else if((m_dwOnlineTime >= 4*60) && (!m_blNotifyFour))//4*60
	{
		m_blNotifyFour = true;
		ret_result = enChenMi_FourHour;
	}
	else if((m_dwOnlineTime >= (4*60+30)) && (!m_blNotifyFourHalf))//4*60+30
	{
		m_blNotifyFourHalf = true;
		ret_result = enChenMi_FourHour;
	}
	else if(m_dwOnlineTime >= 5*60)//5 * 60
	{
		if(m_stNotifyFinalTimer(MODI_ChenMiManager::GetInstance().GetCurMSec()))
		{
			ret_result = enChenMi_MoreThen_FiveHour;
		}
	}
	//#endif
	
	/// 通知用户进入沉迷
	if(ret_result != enChenMi_None)
	{
		MODI_Unify2S_ChenMiResult_Cmd send_cmd;
		send_cmd.m_enResult = ret_result;
		defAccUserMapIter acc_iter = m_AccUserMap.begin();
		for(; acc_iter != m_AccUserMap.end(); acc_iter++)
		{
			defChenMiUserMap & zone_map = acc_iter->second;
			defChenMiUserMapIter zone_iter = zone_map.begin();
			for(; zone_iter != zone_map.end(); zone_iter++)
			{
				if(zone_iter->second.IsOnline())
				{
					send_cmd.m_dwAccID = acc_iter->first;
					MODI_ZoneTask * p_task = MODI_ZoneTaskManager::GetInstance().GetZoneTask(zone_iter->first);
					if(p_task)
					{
#ifdef _HRX_DEBUG
						Global::logger->debug("[chenmi_time] send a chenmi cmd <accid=%u,zoneid=%d>", acc_iter->first, zone_iter->first);
#endif						
						p_task->SendCmd(&send_cmd, sizeof(send_cmd));
					}
				}
			}
		}
	}
}
