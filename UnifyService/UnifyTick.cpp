/**
 * @file   UnifyTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:24:45 2011
 * 
 * @brief  处理登陆验证
 * 
 * 
 */

#include "UnifyTick.h"
#include "Global.h"
#include "ChenMiManager.h"
#include "ZoneTaskManager.h"
#include "UnifyTaskManager.h"

MODI_UnifyTick * MODI_UnifyTick::m_pInstance = NULL;

unsigned long long showsize = 0;

MODI_UnifyTick::MODI_UnifyTick() : MODI_Thread("unifytick"), m_stCmdFluxTime(24 * 3600 * 1000)
{
	
}

MODI_UnifyTick::~MODI_UnifyTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_UnifyTick::Run()
{

	MODI_RTime m_stCurrentTime;
	QWORD get_time_delay = 0;
	
	while(! IsTTerminate())
	{
		::usleep(10000);
		m_stCurrentTime.GetNow();
		
		MODI_ZoneTaskManager::GetInstance().DealQueueCmd();
		MODI_UnifyTaskManager::GetInstance().DealQueueCmd();
		
		/// 每60秒
		if(m_stCurrentTime.GetMSec() > get_time_delay)
		{
		   	get_time_delay = m_stCurrentTime.GetMSec() + 1000 * 60;
			MODI_ChenMiManager::GetInstance().UpDate();
		}
	}
	
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_UnifyTick::Final()
{
	
}
