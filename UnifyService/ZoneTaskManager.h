/**
 * @file   ZoneTaskManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:19:36 2011
 * 
 * @brief  区连接管理
 * 
 * 
 */


#ifndef _MDZONETASKMANAGER_H
#define _MDZONETASKMANAGER_H

#include "Global.h"
#include "RWLock.h"
#include "ZoneTask.h"

/**
 * @brief 回调基类
 *
 */
struct MODI_ZoneTaskCallBack
{
	virtual ~MODI_ZoneTaskCallBack(){};
	virtual bool Done(MODI_ZoneTask * p_callback_task) = 0;
};

/**
 * @brief 区连接管理
 *
 */
class MODI_ZoneTaskManager
{
 public:
	typedef std::map<WORD, MODI_ZoneTask * > defZoneTaskMap;
	typedef std::map<WORD, MODI_ZoneTask * >::iterator defZoneTaskMapIter;
	typedef std::map<WORD, MODI_ZoneTask * >::value_type defZoneTaskMapValue;
	
	static MODI_ZoneTaskManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_ZoneTaskManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	void AddTask(MODI_ZoneTask * p_task);

	void RemoveTask(MODI_ZoneTask * p_task);

	MODI_ZoneTask * GetZoneTask(const WORD & zone_id);

	const DWORD Size() 
	{
		m_stRWLock.rdlock();
		DWORD num = m_stTaskMap.size();
		m_stRWLock.unlock();
		return num;
	}

	void DealQueueCmd()
	{
		if(m_stTaskMap.size() == 0)
			return;
		defZoneTaskMapIter iter = m_stTaskMap.begin();
		for(; iter != m_stTaskMap.end(); iter++)
		{
			iter->second->Get(1000);
		}
	}

	void ExecAllTask(MODI_ZoneTaskCallBack & callback)
	{
		defZoneTaskMapIter iter = m_stTaskMap.begin();
		m_stRWLock.rdlock();
		for(; iter!=m_stTaskMap.end(); iter++)
		{
			callback.Done(iter->second);
		}
		m_stRWLock.unlock();
	}

	/// gmtool使用的
	const char * GetZoneInfo();
	
 private:
	
	static MODI_ZoneTaskManager * m_pInstance;

	defZoneTaskMap  m_stTaskMap;

	MODI_RWLock m_stRWLock;
};

#endif
