/**
 * @file   UnifyTaskManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jul 19 10:35:52 2011
 * 
 * @brief  gmtool 客户端连接
 * 
 */



#ifndef _MDINFOTASKMGR_H
#define _MDINFOTASKMGR_H

#include "Global.h"
#include "RWLock.h"
#include "UnifyTask.h"


/**
 * @brief gmtool连接管理
 *
 */
class MODI_UnifyTaskManager
{
 public:
	typedef std::map<MODI_SessionID, MODI_UnifyTask * > defUnifyTaskMap;
	typedef std::map<MODI_SessionID, MODI_UnifyTask * >::iterator defUnifyTaskMapIter;
	typedef std::map<MODI_SessionID, MODI_UnifyTask * >::value_type defUnifyTaskMapValue;
	
	static MODI_UnifyTaskManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_UnifyTaskManager();
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	void AddTask(MODI_UnifyTask * p_task);

	void RemoveTask(MODI_UnifyTask * p_task);

	MODI_UnifyTask * GetUnifyTask(const MODI_SessionID & session_id);

	const DWORD Size() 
	{
		m_stRWLock.rdlock();
		DWORD num = m_stTaskMap.size();
		m_stRWLock.unlock();
		return num;
	}

	void DealQueueCmd()
	{
		if(m_stTaskMap.size() == 0)
			return;
		defUnifyTaskMapIter iter = m_stTaskMap.begin();
		for(; iter != m_stTaskMap.end(); iter++)
		{
			iter->second->Get(1);
		}
	}
	
 private:
	
	static MODI_UnifyTaskManager * m_pInstance;

	defUnifyTaskMap  m_stTaskMap;

	MODI_RWLock m_stRWLock;
};

#endif
