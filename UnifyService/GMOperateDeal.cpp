/**
 * @file   GMOperateDeal.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Jul 25 11:00:43 2011
 * 
 * @brief  gmtool 对玩家的处理
 * 
 * 
 */


#include <iostream>
#include "md5.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "FunctionTime.h"
#include "UnifyTask.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "ZoneTask.h"
#include "sessionid_creator.h"
#include "UnifyTaskManager.h"
#include "ZoneTaskManager.h"
#include "s2unify_cmd.h"



/// 对玩家操作
bool MODI_UnifyTask::DealOperateSomeCmd(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];
	std::string op_type = deal_result["operate_type"];

	if(zone_id == "" || user_id == "" || op_type == "")
	{
		MODI_ASSERT(0);
		return false;
	}
	WORD zoneid = atoi(zone_id.c_str());

	std::string str_guid = deal_result["guid"];
	std::string str_accid = deal_result["acc_id"];
	std::string g_str_accname = deal_result["acc_name"];
	std::string g_str_name = deal_result["role_name"];
	std::string str_name = PublicFun::StrDecode(g_str_name.c_str());
	std::string str_accname = PublicFun::StrDecode(g_str_accname.c_str());

	BYTE op = atoi(op_type.c_str());
	if(op % 10 == 1) ///根据 角色id 对某个用户 操作
	{
		MODI_Unify2S_GMOperateUser_Cmd send_cmd;
		send_cmd.m_stSessionID = GetSessionID();
		send_cmd.m_wdZoneId = zoneid;
		send_cmd.operate_type = (enGMOperateUserType)op;
		send_cmd.m_stGuid = strtoul(str_guid.c_str(), NULL, 10);
		Put(&send_cmd, sizeof(send_cmd));
		return true;
	}
	else if(op % 10 == 2) /// 根据 账号id 对某个用户
	{
		MODI_Unify2S_GMOperateUser_Cmd send_cmd;
		send_cmd.m_stSessionID = GetSessionID();
		send_cmd.m_wdZoneId = zoneid;
		send_cmd.operate_type = (enGMOperateUserType)op;
		send_cmd.m_dwAccId = atoi(str_accid.c_str());
		Put(&send_cmd, sizeof(send_cmd));
		return true;
	}
	else if(op % 10 == 3) ///根据 账号名 对某个用户
	{
		MODI_Unify2S_GMOperateUser_Cmd send_cmd;
		send_cmd.m_stSessionID = GetSessionID();
		send_cmd.m_wdZoneId = zoneid;
		send_cmd.operate_type = (enGMOperateUserType)op;
		strncpy(send_cmd.m_strAccName, str_accname.c_str(), sizeof(send_cmd.m_strAccName) - 1);
		RetUnifyResult("value=2");
		//Put(&send_cmd, sizeof(send_cmd));
		return true;
	}
	else if(op % 10 == 4) /// 根据 角色名 对某个用户
	{
		MODI_Unify2S_GMOperateUser_Cmd send_cmd;
		send_cmd.m_stSessionID = GetSessionID();
		send_cmd.m_wdZoneId = zoneid;
		send_cmd.operate_type = (enGMOperateUserType)op;
		strncpy(send_cmd.m_strName, str_name.c_str(), sizeof(send_cmd.m_strName) - 1);
		Put(&send_cmd, sizeof(send_cmd));
		return true;
	}
	else
	{
		RetUnifyResult("value=2");
		return false;
	}
	
	return true;
}
