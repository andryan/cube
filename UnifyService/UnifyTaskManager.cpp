/**
 * @file   UnifyTaskManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Jul 19 10:53:42 2011
 * 
 * @brief  gmtool连接管理
 * 
 */


#include "UnifyTaskManager.h"

MODI_UnifyTaskManager * MODI_UnifyTaskManager::m_pInstance = NULL;

/** 
 * @brief 增加一个任务
 * 
 */
void MODI_UnifyTaskManager::AddTask(MODI_UnifyTask * p_task)
{
	if(p_task)
	{
		m_stRWLock.wrlock();
		m_stTaskMap.insert(defUnifyTaskMapValue(p_task->GetSessionID(), p_task));
		m_stRWLock.unlock();
	}
}

/** 
 * @brief 删除一个连接
 * 
 */
void MODI_UnifyTaskManager::RemoveTask(MODI_UnifyTask * p_task)
{
	if(p_task)
	{
		m_stRWLock.wrlock();
		defUnifyTaskMapIter iter = m_stTaskMap.find(p_task->GetSessionID());
		if(iter != m_stTaskMap.end())
		{
			m_stTaskMap.erase(iter);
		}
		m_stRWLock.unlock();
	}
}


/** 
 * @brief 获取一个gatetask
 * 
 * @param session_id task的session_id
 * 
 */
MODI_UnifyTask * MODI_UnifyTaskManager::GetUnifyTask(const MODI_SessionID & session_id)
{
	m_stRWLock.rdlock();
	defUnifyTaskMapIter iter = m_stTaskMap.find(session_id);
	if(iter != m_stTaskMap.end())
	{
		m_stRWLock.unlock();
		return iter->second;
	}
	m_stRWLock.unlock();
	return NULL;
}
