/**
 * @file   UnifyTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:42:31 2011
 * 
 * @brief  账号信息
 * 
 * 
 */


#include <iostream>
#include "md5.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "FunctionTime.h"
#include "UnifyTask.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "ZoneTask.h"
#include "sessionid_creator.h"
#include "UnifyTaskManager.h"
#include "ZoneTaskManager.h"
#include "s2unify_cmd.h"

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_UnifyTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	return true;
}


/** 
 * @brief 发送命令
 * 
 * @param send_cmd 要发送的命令 
 * @param cmd_size 发送命令的大小不能超过64K
 * 
 * @return 成功true 失败false
 */
bool MODI_UnifyTask::SendCmd(const char * send_cmd, const unsigned int cmd_size)
{
	if((! m_pSocket) || (cmd_size > RET_BUF_LENGTH))
	{
		Global::net_logger->fatal("[sock_data] send cmd to large");
		return false;
	}
	m_pSocket->SendCmdNoPacket(send_cmd, cmd_size);
#ifdef _GMTOOL_DEBUG
	Global::logger->debug("[ret_unify] <%s>", send_cmd);
#endif	
	return true;
}


/**
 * @brief 接收到数据处理
 *
 * @return  继续等待true 成功关闭连接false
 *
 */
bool MODI_UnifyTask::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > RECV_BUF_LENGTH)
		{
			Global::net_logger->fatal("%s:%d recv command more than 256", GetIP(), GetPort());
			ResetRecvBuf();
			return false;
		}

		const std::string m_strRecvContent = m_stRecvBuf;
		std::string::size_type method_pos = m_strRecvContent.find_first_of(' ', 0);
		std::string method;
		if(method_pos == std::string::npos)
		{
			if(m_strRecvContent.size() > 5)
			{
				Global::logger->fatal("%s:%d get method error", GetIP(), GetPort());
				ResetRecvBuf();
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			method = m_strRecvContent.substr(0, method_pos);
		}
		Global::logger->debug("getcommand=%s",m_strRecvContent.c_str());
		
		if(method == "GET")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n", 0, 2);
			if(end_pos == std::string::npos)
			{
#ifdef _HRX_DEBUG				
				Global::net_logger->fatal("get command not get crlf");
#endif				
				return true;
			}
			
			std::string::size_type source_pos_end = m_strRecvContent.find_first_of(' ', method_pos+1);
			if(source_pos_end == std::string::npos)
			{
				Global::logger->fatal("not get source pos end");
				ResetRecvBuf();
				return false;
			}

			std::string get_source = m_strRecvContent.substr(method_pos+2, (source_pos_end - (method_pos+2)));
			
#ifdef _HRX_DEBUG			
			Global::logger->debug("recv a content=%s,size=%d", get_source.c_str(), get_source.size());
#endif

			RetUnifyResult("ok!");
			ResetRecvBuf();
			return false;
		}
		
		else if(method == "POST")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n\r\n", 0, 4);
			if(end_pos == std::string::npos)
			{
				Global::logger->debug("not get content");
				return true;
			}
			std::string content = m_strRecvContent.substr(end_pos + 4, (m_strRecvContent.size() - (end_pos+4)));
#ifdef _GMTOOL_DEBUG			
			Global::logger->debug("[content]=[%s]", content.c_str());
#endif			

			if(! DealGmCommand(content.c_str()))
			{
				ResetRecvBuf();
				return false;
			}
			
			ResetRecvBuf();
			return true;
		}
		else
		{
			Global::logger->fatal("method failed");
		}
		return false;
	}
	return true;
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_UnifyTask::RecycleConn()
{
	return 1;
}


/** 
 * @brief 加入到管理器
 * 
 */
void MODI_UnifyTask::AddTaskToManager()
{
	/// 连接增加到管理器
	MODI_SessionID sessionID;
	if(MODI_SessionIDCreator::GetInstancePtr())
	{
		MODI_SessionIDCreator::GetInstancePtr()->Create( sessionID , GetIPNum() , GetPort());
	}
	else
	{
		MODI_ASSERT(0);
	}

	m_stSessionID = sessionID;
	
	MODI_UnifyTaskManager::GetInstance().AddTask(this);
}



/** 
 * @brief 从管理器移除
 * 
 */
void MODI_UnifyTask::RemoveFromManager()
{
	MODI_UnifyTaskManager::GetInstance().RemoveTask(this);
}


bool MODI_UnifyTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	const MODI_GMTool_Cmd * p_recv_cmd = (const MODI_GMTool_Cmd *)pt_null_cmd;
	MODI_ZoneTask * p_zone = MODI_ZoneTaskManager::GetInstance().GetZoneTask(p_recv_cmd->m_wdZoneId);
	if(p_zone)
	{
#ifdef _GMTOOL_DEBUG
		Global::logger->debug("[send_gmcmd] send a command to zone <sessionid=%s, zoneid=%d, cmd=%d,para=%d>",
							  p_recv_cmd->m_stSessionID.ToString(), p_recv_cmd->m_wdZoneId, p_recv_cmd->byCmd, p_recv_cmd->byParam);
#endif
			
		p_zone->SendCmd(p_recv_cmd, cmd_size);
		return true;
	}
	else
	{
		Global::logger->error("[cmd_parse_queue] failed zone id <id=%u>", p_recv_cmd->m_wdZoneId);
		//MODI_ASSERT(0);
		std::ostringstream os;
		os<< "value=2";
		RetUnifyResult(os.str().c_str());
		return false;
	}
	return true;
}

/** 
 * @brief 返回同步结果
 * 
 */
void MODI_UnifyTask::RetUnifyResult(const char * result)
{
	std::ostringstream os;
	os<< "HTTP/1.1 200 OK\r\n"
	  <<"Content-Type: text/html; charset=UTF-8\r\n\r\n"
	  << result
	  << "\r\n";
	
	SendCmd(os.str().c_str(), os.str().size());

	/// 放这里是否可行，先试试
	Terminate();
}

/** 
 * @brief 处理命令
 * 
 * @param cmd_line 命令行
 *
 */
bool MODI_UnifyTask::DealGmCommand(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	/// 获取命令
	std::string str_cmd = deal_result["cmd"];
	std::string str_zone_id = deal_result["zoneid"];
	std::string userid = deal_result["userid"];

	Global::logger->debug("[gm_cmd] gm tool command <cmd=%s,userid=%s,zoneid=%s>", str_cmd.c_str(), userid.c_str(), str_zone_id.c_str());
	
	/// 不判断zoneid是否非法
	if(str_cmd == "login")
	{
		if(! DealLoginCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 查询频道状态
	else if(str_cmd == "query_channel_online")
	{
		if(! DealChannelCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 查询区
	else if(str_cmd == "query_zone_online")
	{
		if(! DealZoneCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 角色操作
	else if(str_cmd == "operate_some")
	{
		if(! DealOperateSomeCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 发送聊天信息
	else if(str_cmd == "chat_to_some")
	{
		if(! DealChatToSomeCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 系统公告
	else if(str_cmd == "system_chat")
	{
		if(! DealSystemChatCmd(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	/// 邮件
	else if(str_cmd == "mail_to_some")
	{
		if(!(DealMailToSome(cmd_line)))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	else if(str_cmd == "set_channel_num")
	{
		if(!DealSetChannelNum(cmd_line))
		{
			MODI_ASSERT(0);
			return false;
		}
	}
	else
	{
		MODI_ASSERT(0);
		return false;
	}
	
	return true;
}


/// 处理请求频道信息
bool MODI_UnifyTask::DealChannelCmd(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];

	if(zone_id == "" || user_id == "")
	{
		MODI_ASSERT(0);
		return false;
	}

	WORD zoneid = atoi(zone_id.c_str());
	MODI_QueryChannelStatus_Cmd send_cmd;
	send_cmd.m_stSessionID = GetSessionID();
	send_cmd.m_wdZoneId = zoneid;

	Put(&send_cmd, sizeof(send_cmd));
	return true;
}


/// gmtool 申请区信息 
bool MODI_UnifyTask::DealLoginCmd(const char * cmd_line)
{
	std::string ret_result = MODI_ZoneTaskManager::GetInstance().GetZoneInfo();
	RetUnifyResult(ret_result.c_str());
	return true;
}

/// 出现游戏区信息
bool MODI_UnifyTask::DealZoneCmd(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];

	RetUnifyResult("value=1");
	
	return true;
}

/// 处理聊天信息
bool MODI_UnifyTask::DealChatToSomeCmd(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];
	std::string op_type = deal_result["operate_type"];

	if(zone_id == "" || user_id == "" || op_type == "")
	{
		MODI_ASSERT(0);
		return false;
	}

	std::string str_guid = deal_result["guid"];
	std::string str_accid = deal_result["acc_id"];
	std::string str_accname = deal_result["acc_name"];
	std::string str_name = deal_result["role_name"];
	
	Global::logger->debug("user_id=%s,zone_id=%s,op_type=%s,guid=%s,accid=%s,accname=%s,name=%s",
						  user_id.c_str(), zone_id.c_str(), op_type.c_str(),
						  str_guid.c_str(), str_accid.c_str(), str_accname.c_str(), str_name.c_str());
	
	RetUnifyResult("value=1");
	
	return true;
}


/// 发布公告
bool MODI_UnifyTask::DealSystemChatCmd(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];
	std::string g_str_content = deal_result["chat_content"];

	if(user_id == "" || zone_id == "" || g_str_content == "")
	{
		RetUnifyResult("value=2");
		return false;
	}

	std::string str_content = PublicFun::StrDecode(g_str_content.c_str());
	WORD zoneid = atoi(zone_id.c_str());
	MODI_Unify2S_SystemChat_Cmd send_cmd;
	send_cmd.m_stSessionID = GetSessionID();
	send_cmd.m_wdZoneId = zoneid;
	strncpy(send_cmd.m_cstrContent, str_content.c_str(), sizeof(send_cmd.m_cstrContent) - 1);
	Put(&send_cmd, sizeof(send_cmd));
	
	return true;
}


/// 修改频道人数
bool MODI_UnifyTask::DealSetChannelNum(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];
	std::string g_str_content = deal_result["chat_content"];

	if(user_id == "" || zone_id == "" || g_str_content == "")
	{
		RetUnifyResult("value=2");
		return false;
	}

	std::string str_content = PublicFun::StrDecode(g_str_content.c_str());
	WORD zoneid = atoi(zone_id.c_str());
	MODI_Unify2S_SetChannelNum_Cmd send_cmd;
	send_cmd.m_stSessionID = GetSessionID();
	send_cmd.m_wdZoneId = zoneid;
	strncpy(send_cmd.m_cstrContent, str_content.c_str(), sizeof(send_cmd.m_cstrContent) - 1);
	Put(&send_cmd, sizeof(send_cmd));
	
	return true;
}



/// 发布公告
bool MODI_UnifyTask::DealMailToSome(const char * cmd_line)
{
	MODI_SplitString deal_result;
	deal_result.Init(cmd_line, "&");
	std::string user_id = deal_result["userid"];
	std::string zone_id = deal_result["zoneid"];
	std::string str_operate_type = deal_result["operate_type"];
	
	std::string g_str_content = deal_result["mail_content"];
	std::string g_str_caption	 = deal_result["mail_caption"];
	//std::string str_id = deal_result["slave_good_id"];
	//std::string str_num = deal_result["slave_good_num"];
	//std::string str_time = deal_result["slave_good_time"];
	std::string g_str_attach = deal_result["attach"];
	std::string g_str_name = deal_result["role_name"];
	
	std::string str_check = deal_result["str_check"];
	std::string str_send_time = deal_result["send_time"];
	std::string g_str_reason = deal_result["send_reason"];

	if(user_id == "" || zone_id == "" || g_str_content == "" || str_operate_type == "" ||
	   g_str_caption == "" || g_str_name == "" || g_str_reason == ""
	   || str_send_time == "" || str_check.size() != 32)
	{
		RetUnifyResult("value=2");
		return false;
	}

	std::string str_content = PublicFun::StrDecode(g_str_content.c_str());
	std::string str_caption = PublicFun::StrDecode(g_str_caption.c_str());
	std::string str_name = PublicFun::StrDecode(g_str_name.c_str());
	std::string str_reason = PublicFun::StrDecode(g_str_reason.c_str());
	std::string send_time = PublicFun::StrDecode(str_send_time.c_str());
	std::string operate_type = PublicFun::StrDecode(str_operate_type.c_str());
	std::string str_attach = PublicFun::StrDecode(g_str_attach.c_str());

	if(! CheckMD5(send_time.c_str(), str_reason.c_str(), str_caption.c_str(), str_check.c_str()))
	{
#ifndef _DEBUG
		RetUnifyResult("value=2");
		return false;
#endif		
	}

	Global::logger->debug("[send_gm_mail] send gm mail <user=%s,time=%s,reason=%s,zoneid=%s,attach=%s,who=%s>", user_id.c_str(),
						  send_time.c_str(), str_reason.c_str(), zone_id.c_str(), str_attach.c_str(),str_name.c_str());
	
	WORD zoneid = atoi(zone_id.c_str());
	WORD operatetype = atoi(str_operate_type.c_str());
	MODI_Unify2S_SendMail_Cmd send_cmd;
	send_cmd.m_stSessionID = GetSessionID();
	send_cmd.m_wdZoneId = zoneid;
	send_cmd.m_byType = operatetype;

	strncpy(send_cmd.m_strName, str_name.c_str(), sizeof(send_cmd.m_strName) - 1);
	strncpy(send_cmd.m_szCaption, str_caption.c_str(), sizeof(send_cmd.m_szCaption) - 1);
	strncpy(send_cmd.m_szContents, str_content.c_str(), sizeof(send_cmd.m_szContents) - 1);
	if(str_attach.size())
	{
		strncpy(send_cmd.m_szAttach, str_attach.c_str(), sizeof(send_cmd.m_szAttach) - 1);
	}

	Put(&send_cmd, sizeof(send_cmd));
	return true;
}


bool MODI_UnifyTask::CheckMD5(const char * send_time, const char * send_reason, const char * send_caption, const char * str_check)
{
	
	std::string m_strSN = str_check;
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;

	str_md5<<"123456" << send_time << send_reason << send_caption;
	
#ifdef _DEBUG	
	Global::logger->debug("md5_src_str=%s", str_md5.str().c_str());
#endif
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	
	if(recv_check_str != m_strSN)
	{
		Global::logger->fatal("[check_md5] %s:%d md5 check failed(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
		return false;
	}
	Global::logger->fatal("[check_md5] %s:%d md5 check successful(%s == %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
	return true;
}
