/**
 * @file   ChenMiManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 14:50:03 2011
 * 
 * @brief  沉迷管理
 * 
 * 
 */

#ifndef _MD_CHENMIMANAGER_H
#define _MD_CHENMIMANAGER_H

#include "Global.h"
#include "ms2is_cmd.h"

/// 下线5小时复位防沉迷
/* #ifdef _HRX_DEBUG */
/* const DWORD RESET_CHENMI_TIME = 15 * 60; */
/* #else  */
const DWORD RESET_CHENMI_TIME = 5 * 3600;
//#endif

/**
 * @brief 某个身份证用户在区的角色
 * 
 */
class MODI_ChenMiUser
{
 public:
	MODI_ChenMiUser()
	{
		m_blOnline = true;
	}
	
	void OnLogin()
	{
		m_blOnline = true;
	}

	bool IsOnline()
	{
		return m_blOnline;
	}

	void OnLogout()
	{
		m_blOnline = false;
	}

 private:
	/// 是否在线
	bool m_blOnline;
};


/** 
 * @brief 身份证相关的角色(账号个数*游戏区个数)
 * 
 */
class MODI_CardAllUser
{
 public:
	typedef std::map<WORD, MODI_ChenMiUser > defChenMiUserMap;
	typedef std::map<WORD, MODI_ChenMiUser >::iterator defChenMiUserMapIter;
	typedef std::map<defAccountID, std::map<WORD, MODI_ChenMiUser> > defAccUserMap;
	typedef defAccUserMap::iterator defAccUserMapIter;
	
	MODI_CardAllUser(): m_stNotifyFinalTimer(15 * 60 * 1000)
	{
		m_blNotifyOne = false;
		m_blNotifyTwo = false;
		m_blNotifyThree = false;
		m_blNotifyFour = false;
		m_blNotifyFourHalf = false;
		m_blOnline = false;
		m_dwLogoutTime = 0;
		m_dwOnlineTime = 0;
		m_dwOutlineTime = 0;
	}

	/** 
	 * @brief 复位,只有用户角色登陆后且
	 * 下线时间超过5小时才会执行此函数
	 * 
	 */
	void Reset()
	{
		m_blNotifyOne = false;
		m_blNotifyTwo = false;
		m_blNotifyThree = false;
		m_blNotifyFour = false;
		m_blNotifyFourHalf = false;
		m_blOnline =true;
		m_dwLogoutTime = 0;
		m_dwOnlineTime = 0;
		m_dwOutlineTime = 0;
	}


	void UpDate();

	/** 
	 * @brief 是个有某个角色在线
	 * 
	 * @return 有返回true
	 *
	 */
	bool IsOnline();

	/** 
	 * @brief 在线时间
	 * 
	 * @return 分钟
	 *
	 */
	DWORD OnlienTime();

	/** 
	 * @brief 用户登录
	 * 
	 * @param acc_id 账号id
	 * @param zone_id 区号
	 */
	void OnLogin(const defAccountID & acc_id, const WORD & zone_id);

	/** 
	 * @brief 用户登出
	 * 
	 * @param acc_id 账号id
	 * @param zone_id 区号
	 */
	void OnLogout(const defAccountID & acc_id, const WORD & zone_id);

	/** 
	 * @brief 增加防沉迷的时间
	 * 
	 * @param acc_id 账号id
	 * @param zone_id 区id
	 * @param chenmi_time 在线时间
	 */
	void AddChenMiTime(const defAccountID & acc_id, const WORD & zone_id, const DWORD chenmi_time);


	/** 
 	 * @brief 此身份证所有用户是否下线
 	 * 
 	 * 
 	 * @return  下线true
 	 */
	bool IsAllUserLogout();

	/** 
	 * @brief 增加沉迷时间
	 * 
	 * @param chenmi_time 沉迷时间
	 */
	void AddChenMiTime(const DWORD chenmi_time);

 private:
	
	/// 此身份证所有用户
	defAccUserMap m_AccUserMap;

	/// 是否在线
	bool m_blOnline;
	
	/// 各个小时是否通告过
	bool m_blNotifyOne;
	bool m_blNotifyTwo;
	bool m_blNotifyThree;
	bool m_blNotifyFour;
	bool m_blNotifyFourHalf;
	
	/// 5个小时后每15分钟一次的提醒
	MODI_Timer m_stNotifyFinalTimer;
	
	/// 下线的时间
	DWORD m_dwLogoutTime;
	DWORD m_dwOnlineTime;
	DWORD m_dwOutlineTime;
};

/**
 * @brief 沉迷管理,不做数据库保存
 * 
 */
class MODI_ChenMiManager
{
 public:
	typedef __gnu_cxx::hash_map<std::string, MODI_CardAllUser, str_hash> defCardAllUserMap;
	typedef defCardAllUserMap::iterator defCardAllUserMapIter;
	typedef defCardAllUserMap::value_type defCardAllUserMapValue;

	MODI_ChenMiManager()
	{
		
	}

	~MODI_ChenMiManager()
	{
		
	}
	
	static MODI_ChenMiManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_ChenMiManager;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}

	/** 
	 * @brief 刷新客户端显示
	 * 
	 */
	void UpDate();

	/** 
	 * @brief 用户登录
	 * 
	 *
	 */
	void OnLogin(const defAccountID & acc_id, const WORD & zone_id);

	/** 
	 * @brief 用户登出
	 * 
	 *
	 */
	void OnLogout(const defAccountID & acc_id, const WORD & zone_id);

	/** 
	 * @brief 增加防沉迷的时间
	 * 
	 * @param acc_id 账号id
	 * @param zone_id 区id
	 * @param chenmi_time 在线时间
	 */
	void AddChenMiTime(const defAccountID & acc_id, const DWORD chenmi_time);

	/** 
	 * @brief 增加一个沉迷相关
	 * 
	 * @param acc_id 沉迷的id
	 * @param card_num 沉迷的身份证
	 *
	 */
	void AddCardNum(const defAccountID & acc_id, const char * card_num);


	/** 
	 * @brief 查找一个沉迷身份
	 * 
	 * @param acc_id 沉迷id
	 * @param card_num 返回身份证
	 * 
	 * @return 成功true
	 */
	bool FindCardNum(const defAccountID & acc_id, std::string & card_num);

	/** 
	 * @brief 获取当前秒数
	 * 
	 */
	DWORD GetCurSec()
	{
		return m_stRTime.GetSec();
	}

	QWORD GetCurMSec()
	{
		return m_stRTime.GetMSec();
	}
	
 private:
	static MODI_ChenMiManager * m_pInstance;
	
	MODI_RTime m_stRTime;
	/// 账号信息map
	defCardAllUserMap m_CardAllUserMap;
	/// 身份证管理
	std::map<defAccountID, std::string> m_CardNumMap;
};

#endif
