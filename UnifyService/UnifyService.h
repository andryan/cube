/**
 * @file   UnifyService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:37:21 2011
 * 
 * @brief  游戏平台服务器
 * 
 */


#ifndef _MD_UNIFYSERVICE_H
#define _MD_UNIFYSERVICE_H

#include "sessionid_creator.h"
#include "ServiceTaskSched.h"
#include "MNetService.h"


const int MULTI_PORT_NUM = 2;

/// 端口索引下标
enum enPortIndex
{
	enToolPort,
	enZonePort,
	enMaxPort
};

/**
 * @brief 信息服务器类，多监听
 * 
 */
class MODI_UnifyService: public MODI_MNetService
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param vec 要绑定的端口
	 * @param name 服务器
	 * @param count 绑定的端口个数
	 * 
	 */
	MODI_UnifyService(std::vector<WORD> & vec, const std::vector<std::string> & vecip , 
			const char * name = "unifyservice", const int count = MULTI_PORT_NUM):MODI_MNetService(vec, vecip, name, count),
		m_stUnifyTaskSched(1,1), m_stZoneTaskSched(2,1)
	{
		m_pInstance = this;
		m_wdToolPort = 0;
		m_wdZonePort = 0;
		
		if(((int)vec.size() == count) && (count == MULTI_PORT_NUM))
		{
			m_wdToolPort = vec[enToolPort];
			m_wdZonePort = vec[enZonePort];
		}
	}

	/** 
	 * @brief 创建新任务
	 * 
	 * @param sock 任务相关的task
	 * @param addr 任务相关的地址
	 * @param port 任务相关的端口
	 * 
	 * @return 成功true 失败false
	 */		
	bool CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port);

	/** 
	 * @brief 初始化
	 *
	 * @return 成功true 失败false
	 */
	bool Init();

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();

	static MODI_UnifyService * GetInstancePtr()
	{
		return m_pInstance;
	}

	bool CheckTaskIP(const char * ip, WORD & zone_id, std::string & zone_name);


	/** 
	 * @brief 重新加载配置文件
	 * 
	 */
   	void ReloadConfig();
	
 private:
	/// 充值客户端
	WORD m_wdToolPort;
	
	/// 游戏区端口
	WORD m_wdZonePort;

	/// 任务处理线程
	MODI_ServiceTaskSched m_stUnifyTaskSched;

	MODI_ServiceTaskSched m_stZoneTaskSched;

	static MODI_TaskQueue * m_pUnifyTaskQueue;

	/// 实体
	static MODI_UnifyService * m_pInstance;
};

#endif

