/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:51
	filename: 	config
	author	:	tt
	module	:	流系统(序列化模块)
	purpose	:
		流系统的一些配置
	
	log		:
	
	
	Copyright (c) 汤腾
	
***********************************************************************/
	

#ifdef _MSC_VER
#pragma once 
#endif



/**
	容器默认大小
	常量定义
*/
#define		DEF_CONTAINER_SIZE					4096 	//	4k



/*
	检查读取的数据类型是否正确
	库配置定义
*/
//#define		TSTREAM_CHECK_TAG


/**
	是否使用内存池形式的 std::string
	库配置定义
*/
//#define		USE_MEMPOOL_STRING


/** 
	是否使用共享内存
	库配置定义
*/
//#define		USE_SHAREMEMORY_POOL_ALLOC


/** 
	是否使用内存池
	库配置定义
*/
//#define		USE_FASTMEMORY_POOL_ALLOC

/** 
	是否支持 std::string 的序列化
	配置定义
*/
#define			STL_STRING_STREAM


/**
	是否支持 STL_allocator 适配器
	配置定义
 */
//#define			USE_STL_ALLOCATOR


