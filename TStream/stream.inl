
//////////////////////////////////////////////////////////////////////////
//	puts values
//////////////////////////////////////////////////////////////////////////
#if defined (STL_STRING_STREAM) && defined(USE_MEMPOOL_STRING)
bool	CStream::Put( const std::string & val )
{
#ifdef _DEBUG

	assert(m_pDataContainer && m_enState == enWrite);	

	//if( !val.size() )
	//	assert( "Error Put string!" && 0 );

#endif

	unsigned int nSize = (unsigned int)val.size();

	//#ifdef _DEBUG
	//	assert( nSize );
	//#endif

	if( (  
#ifdef TSTREAM_CHECK_TAG
		!WRITE_TAG_HELP( enTagString ) || 
#endif
		!PutCount( nSize ) ) )
	{
#ifdef _DEBUG
		assert( "Error Put string!" && 0 );
#endif
		return false;
	}

	if( nSize )
	{
		if( !m_pDataContainer->Write( (const unsigned char *) val.c_str() , nSize ) )
		{
#ifdef _DEBUG
			assert( "Error Put string!" && 0 );
#endif
			return false;
		}
	}

	return true;
}
#endif


bool	CStream::Put( const TString & val )
{
#ifdef _DEBUG

	assert(m_pDataContainer && m_enState == enWrite);	

#endif

	unsigned int nSize = (unsigned int)val.size();

	if( (  
#ifdef TSTREAM_CHECK_TAG
		!WRITE_TAG_HELP( enTagString ) || 
#endif
		!PutCount( nSize ) ) )
	{
#ifdef _DEBUG
		assert( "Error Put string!" && 0 );
#endif
		return false;
	}

	if( nSize )
	{
		if( !m_pDataContainer->Write( (const unsigned char *) val.c_str() , nSize ) )
		{
#ifdef _DEBUG
			assert( "Error Put string!" && 0 );
#endif
			return false;
		}
	}

	return true;
}


bool	CStream::Put( const char * val )
{
#ifdef _DEBUG

	assert(m_pDataContainer && m_enState == enWrite);	

	if( !val )
		assert( "Error Put c style string!" && 0 );

#endif

	unsigned int nSize = (unsigned int)strlen( val );

	if( (
#ifdef TSTREAM_CHECK_TAG
		! WRITE_TAG_HELP( enTagCStyleString ) || 
#endif
		!PutCount( nSize ) ) )
	{
#ifdef _DEBUG
		assert( "Error Put c style string!" && 0 );
#endif
		return false;
	}

	if( nSize )
	{
		if( !m_pDataContainer->Write( (const unsigned char *) val , nSize ) )
		{
#ifdef _DEBUG
			assert( "Error Put c style string!" && 0 );
#endif
			return false;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
//		Get	values
//////////////////////////////////////////////////////////////////////////

#if defined (STL_STRING_STREAM) && defined(USE_MEMPOOL_STRING)
bool	CStream::Get( std::string & val )
{
	assert(m_pDataContainer && m_enState == enRead);	

#ifdef TSTREAM_CHECK_TAG

	if( REA_TAGANDCHECK_HELP( enTagString ) == false )
	{
		assert( " Error in Get string!" && 0 );
	}

#endif

	unsigned int nSize = 0;
	if( !GetCount( nSize ) )
	{
#ifdef _DEBUG
		assert( " Error in Get string!" && 0 );
#endif
		return false;
	}

	//	��ʱBUF,���߳���Ҫע��
	static char s_TempBuff[4096];

	char * pTemp = s_TempBuff;

	bool bNew =  nSize > 4095;
	if( bNew )
	{
		pTemp= static_cast<char *>( TSTREAM_MEM_MALLOC( nSize + 1 ) );
	}

	val.reserve( nSize + 1 );

	pTemp[nSize] = '\0';

	if( nSize )
	{
		if( m_pDataContainer->Read( pTemp , nSize ) == false )
		{
			if( bNew )
			{
				TSTREAM_MEM_FREE( pTemp , nSize + 1 );
			}
#ifdef _DEBUG
			assert( " Error in Get string!" && 0 );
#endif

			return false;
		}
	}


	val = pTemp;

	if( bNew )
	{
		TSTREAM_MEM_FREE( pTemp , nSize + 1 );
	}

	return true;
}
#endif

bool	CStream::Get( TString & val )
{
	assert(m_pDataContainer && m_enState == enRead);	

#ifdef TSTREAM_CHECK_TAG

	if( REA_TAGANDCHECK_HELP( enTagString ) == false )
	{
		assert( " Error in Get string!" && 0 );
	}

#endif

	unsigned int nSize = 0;
	if( !GetCount( nSize ) )
	{
		return true;
	}

	//	��ʱBUF,���߳���Ҫע��
	static char s_TempBuff[4096];

	char * pTemp = s_TempBuff;

	bool bNew =  nSize > 4095;
	if( bNew )
	{
		pTemp= static_cast<char *>( TSTREAM_MEM_MALLOC( nSize + 1 ) );
	}

	val.reserve( nSize + 1 );

	pTemp[nSize] = '\0';

	if( nSize )
	{
		if( m_pDataContainer->Read( pTemp , nSize ) == false )
		{
			if( bNew )
			{
				TSTREAM_MEM_FREE( pTemp , nSize + 1 );
			}
#ifdef _DEBUG
			assert( " Error in Get string!" && 0 );
#endif
			return false;
		}
	}


	val = pTemp;

	if( bNew )
	{
		TSTREAM_MEM_FREE( pTemp , nSize + 1 );
	}

	return true;
}

bool	CStream::Get( char * val )
{
#ifdef _DEBUG
	assert(m_pDataContainer && m_enState == enRead);
#endif

#ifdef TSTREAM_CHECK_TAG
	if( REA_TAGANDCHECK_HELP( enTagCStyleString ) == false )
	{
		assert( " Error in Get c Style string!" && 0 );
	}
#endif

	unsigned int nSize = 0;

	if( !GetCount( nSize ) )
	{
        val[nSize] = '\0';
		return true;
	}

	val[nSize] = '\0';

	if( nSize )
	{
		if( m_pDataContainer->Read( (unsigned char *)val , nSize ) == false )
		{
#ifdef _DEBUG
			assert( " Error in Get c Style string!" && 0 );
#endif
			return false;
		}
	}

	return true;
}


bool CStream::PutRaw( const unsigned char * pData , unsigned int nSize )
{
#ifdef _DEBUG
	assert( pData && m_pDataContainer && m_enState == enWrite );
#endif

	if( (
#ifdef TSTREAM_CHECK_TAG
		! WRITE_TAG_HELP( enTagRaw ) ||
#endif
		!m_pDataContainer->Write( pData , nSize )  ) )
	{
#ifdef _DEBUG
		assert( "Write Error In CStream::PutRaw!" && 0 );
#endif
		return false;
	}

	return true;
}

bool CStream::GetRaw( unsigned char * pData , unsigned int  nSize )
{
#ifdef _DEBUG
	assert( pData && m_pDataContainer && m_enState == enRead );
#endif

	if(  
#ifdef TSTREAM_CHECK_TAG
		!REA_TAGANDCHECK_HELP( enTagRaw ) ||
#endif
		!m_pDataContainer->Read( pData , nSize ) )
	{
#ifdef _DEBUG
		assert( "Read Error In CStream::GetRaw!" && 0 );
#endif
		return false;
	}

	return true;
}

bool CStream::PutCount( unsigned int nCount )
{
	if( !m_pDataContainer->Write( (const unsigned char *)&nCount , sizeof(unsigned int) ) )
		return false;
	return true;
}

bool CStream::GetCount( unsigned int & nCount )
{
	unsigned int nRead = sizeof(unsigned int);
	if( !m_pDataContainer->Read( ( unsigned char *)&(nCount), nRead ) ) 
		return false;
	return true;
}

bool CStream::WriteBytes( const unsigned char * pData , unsigned int nSize )
{
	bool bRet = m_pDataContainer->Write( (const unsigned char *)pData, nSize );
	//assert( "Error in WriteBytes " && bRet );
	return bRet;
}

bool CStream::ReadBytes( unsigned char * pData , unsigned int & nRead )
{
	bool bRet = m_pDataContainer->Read( ( unsigned char *)pData, nRead );	
	//assert( "Error in ReadBytes " && bRet );
	return bRet;
}

bool CStream::Write( const void * p , size_t n )
{
#ifdef TSTREAM_CHECK_TAG
	assert(0);
#endif
	return WriteBytes( (const unsigned char *)p , n );

}

bool CStream::Read( void * p , size_t & n )
{
#ifdef TSTREAM_CHECK_TAG
	assert(0);
#endif
    unsigned int nTemp = 0;
	 if( ReadBytes( (unsigned char *)p , nTemp ) )
	 {
	 	n = nTemp;
	 	return true;
	 	}
	 	
	 	return false;
}

#ifdef TSTREAM_CHECK_TAG
//	��ȡ���Ҽ���ǩ
bool	CStream::REA_TAGANDCHECK_HELP( enStreamTag enTag )
{
	unsigned char	nTag = 255;

	unsigned int nRead  = 1;

	if( (ReadBytes( (unsigned char *)&nTag , nRead ) && 
		(enStreamTag)nTag == enTag ) == false )
	{
		assert( "Read Tag Error!"  && 0 );
		return false;
	}

	return true;
}

//	д���ǩ
bool	CStream::WRITE_TAG_HELP( enStreamTag tag )
{
	unsigned char	nTag = enTagMin;

	nTag = (unsigned char)tag;

	return WriteBytes((const unsigned char *)(&nTag) , 1);
}	

#endif



bool	CStream::Put( const char & val )
{
	return WRITE_NUM_HELP( val , enTagInt8 );
}

bool	CStream::Put( const short & val )
{
	return WRITE_NUM_HELP( val , enTagInt16 );
}

bool	CStream::Put( const int & val )
{
	return WRITE_NUM_HELP( val , enTagInt32 );
}

bool	CStream::Put( const long  long	 & val )
{
	return WRITE_NUM_HELP( val , enTagInt64 );
}

bool	CStream::Put( const unsigned char & val )
{
	return WRITE_NUM_HELP( val , enTagUint8 );
}

bool	CStream::Put( const unsigned short & val )
{
	return WRITE_NUM_HELP( val , enTagUint16 );
}

bool	CStream::Put( const unsigned int & val )
{
	return	WRITE_NUM_HELP( val , enTagUint32 );
}	

bool	CStream::Put( const unsigned long long & val )
{
	return WRITE_NUM_HELP( val , enTagUint64 );
}

bool	CStream::Put( const float & val )
{
	return WRITE_NUM_HELP( val , enTagFloat32 );
}

bool	CStream::Put( const double & val )
{
	return WRITE_NUM_HELP( val , enTagFloat64 );
}

bool	CStream::Put( const unsigned long & val )
{	
	return WRITE_NUM_HELP( val , enTagULong );
}

bool	CStream::Put( const long	& val )
{
	return WRITE_NUM_HELP( val , enTagLong );
}

bool	CStream::Put( const bool & val )
{
	unsigned char byte = (unsigned char)val;
	return WRITE_NUM_HELP( byte , enTagBool );
}

bool	CStream::Get( char & val )
{
	return READ_NUM_HELP( val , enTagInt8 );
}

bool	CStream::Get( short & val )
{
	return READ_NUM_HELP( val , enTagInt16 );
}

bool	CStream::Get( int & val )
{
	return READ_NUM_HELP( val , enTagInt32 );
}

bool	CStream::Get( long  long	 & val )
{
	return READ_NUM_HELP( val , enTagInt64 );
}

bool	CStream::Get( unsigned char & val )
{
	return READ_NUM_HELP( val , enTagUint8 );
}

bool	CStream::Get( unsigned short & val )
{
	return READ_NUM_HELP( val , enTagUint16 );
}

bool	CStream::Get( unsigned int & val )
{
	return READ_NUM_HELP( val , enTagUint32 );
}

bool	CStream::Get( unsigned long long & val )
{
	return READ_NUM_HELP( val , enTagUint64 );
}

bool	CStream::Get( float & val )
{
	return READ_NUM_HELP( val , enTagFloat32 );
}

bool	CStream::Get( double & val )
{
	return READ_NUM_HELP( val , enTagFloat64 );
}

bool	CStream::Get( unsigned long & val )
{
	return READ_NUM_HELP( val , enTagULong );
}

bool	CStream::Get( long	& val )
{
	return READ_NUM_HELP( val , enTagLong );
}

bool	CStream::Get( bool & val )
{
	unsigned char byte;
	if( READ_NUM_HELP( byte , enTagBool ) )
	{
		val = byte == 1 ? true : false;
		return true;
	}
	return false;
}

//	������ʽ�Ķ�д
template < typename T>
bool CStream::PutArray( const T & objs, unsigned int nSize )
{
#ifdef _DEBUG
	assert( nSize );
#else 
	if( !nSize )
		return false;
#endif

#ifdef TSTREAM_CHECK_TAG

	if( !WRITE_TAG_HELP( enTagArray ) )
	{
#ifdef _DEBUG
		assert( 0&& "error in PutArray!" );
#endif
		return false;
	}

#endif

	if( !PutCount( nSize ) )
	{
#ifdef _DEBUG
		assert( 0&& "error in PutArray!" );
#endif
		return false;
	}

	for( unsigned int n = 0; n < nSize; n++ )
	{
		if( !Put(objs[n]) )
			return false;
	}

	return true;
}

template < typename T >
bool CStream::GetArray( const T & objs , unsigned int nSize )
{
#ifdef _DEBUG
	assert( nSize );
#else
	if( !nSize )
		return false;
#endif

#ifdef TSTREAM_CHECK_TAG

	if( !REA_TAGANDCHECK_HELP( enTagArray ) )
	{
#ifdef _DEBUG
		assert( 0 && "error in GetArray!" );
#endif
		return false;
	}

#endif

	unsigned int nCheck = 0;
	if( !GetCount( nCheck ) || nCheck != nSize )
	{
#ifdef _DEBUG
		assert( 0 && "error in GetArray!" );
#endif
		return false;
	}

	for( unsigned int n = 0; n < nSize; n++ )
	{
		if( !Get(objs[n]) )
			return false;
	}
	return true;
}

template <typename T>
CStream & CStream::operator >>  (  T & val )
{
	Get( val );
	return *this;
}

template < typename T>
CStream & CStream::operator <<(  const T & val )
{
	Put( val );
	return *this;
}

/*
template < typename T >
bool CStream::Put( const T & obj )
{
#ifdef _DEBUG
	assert(m_pDataContainer && m_enState == enWrite);	
#endif

#ifdef TSTREAM_CHECK_TAG

	enStreamTag enTag = StreamHelp::GetTag(obj);

	if( !WRITE_TAG_HELP( enTag ) )
	{
		assert( 0 && "Write Error In CStream::Put!" );
	}

#endif

	return StreamHelp::Put( *this , obj );
}

template < typename T >
bool CStream::Get(  T & obj )
{
#ifdef _DEBUG
	assert( m_pDataContainer && m_enState == enRead );
#endif

#ifdef TSTREAM_CHECK_TAG

	enStreamTag enTag = StreamHelp::GetTag(obj);

	if( !REA_TAGANDCHECK_HELP( enTag ) )
	{
		assert( 0 && "Write Error In CStream::Put!" );
	}

#endif

	return StreamHelp::Get( *this , obj );
}
*/



//	��ֵ��д�����
template	< typename T >
bool CStream::WRITE_NUM_HELP( const T & val , const enStreamTag & enTag )
{
#ifdef _DEBUG
	assert(m_pDataContainer && m_enState == enWrite);	
#endif

	if( 
#ifdef TSTREAM_CHECK_TAG
		!WRITE_TAG_HELP( enTag ) ||
#endif
		!WriteBytes( (const unsigned char *)&(val), sizeof( val ) ) )
	{	
#ifdef _DEBUG
		assert( 0 && "Write Num Error!" );	
#endif
		return false;
	}

	return true;
}


//	��ֵ�Ķ�ȡ����
template	< typename T >
bool	CStream::READ_NUM_HELP( T & val , enStreamTag enTag )
{		
#ifdef _DEBUG
	assert(m_pDataContainer && m_enState == enRead);
#endif

	static unsigned int nRead  = 0;

	nRead = sizeof( T );									

	if((
#ifdef TSTREAM_CHECK_TAG
		!REA_TAGANDCHECK_HELP( enTag )	||	
#endif
		!ReadBytes( ( unsigned char *)&(val), nRead ) ) )		
	{	
#ifdef _DEBUG
		assert( 0 && "Read Num Error!" );	
#endif
		return false;
	}

	return true;
}


