/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:52
	filename: 	streamhelper
	author	:	tt
	module	:	流系统(序列化模块)
	purpose	:
		一些帮助宏。使用它们可以使自定义对象具有序列化的能力
	
	log		:
	
	
	Copyright (c) 汤腾
	
***********************************************************************/
	
#ifndef STREAMHELPHER_H_
#define STREAMHELPHER_H_

#ifdef _MSC_VER
#pragma once 
#endif



//	申明流的读写操作
#define DECLARE_STREAM_PUT( type )	\
	namespace StreamHelp{ bool	Put( TStream::CStream & stream , const type &  obj ); };

#define DECLARE_STREAM_GET( type )	\
	namespace StreamHelp{ bool	Get( TStream::CStream & stream , type &  obj ); };

#define DECLATE_STREAM_IO( type )		\
			DECLARE_STREAM_PUT( type )	\
			DECLARE_STREAM_GET( type )


			
//	申明流中对应自己的友员函数
#define DECLARE_STREAM_FRIEND_PUT( type )	friend bool StreamHelp::Put( TStream::CStream &  stream , const type & obj );
#define DECLARE_STREAM_FRIEND_GET( type )	friend bool StreamHelp::Get( TStream::CStream & stream , type & obj);
#define DECLARE_STREAM_FRIEND_IO(type )			\
			DECLARE_STREAM_FRIEND_PUT( type )	\
			DECLARE_STREAM_FRIEND_GET( type )

//	定义在流中的标识
#define DEFINE_STREAM_TAG( type , tag )	\
	namespace StreamHelp { inline enStreamTag GetTag( const type&  obj ) { return tag; } };


//	定义流的读写操作
#define DEFINE_STREAM_PUT( type , obj )		bool	StreamHelp::Put( TStream::CStream & stream , const type & obj )

#define DEFINE_STREAM_GET( type , obj )		bool	StreamHelp::Get( TStream::CStream & stream , type & obj )



#endif
