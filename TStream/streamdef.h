/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:52
	filename: 	streamdef
	author	:	tt
	module	:	��ϵͳ(���л�ģ��)
	purpose	:
		
	
	log		:
	
	
	Copyright (c) ����
	
***********************************************************************/
	
#ifndef		_TSTREAM_DEF_H_
#define		_TSTREAM_DEF_H_


#ifdef _MSC_VER
#pragma once 
#endif

#include "config.h"

#if defined ( USE_MEMPOOL_STRING )
#include "string_lls.h"
typedef string_lls 				TString;
#else  
#include <string>
typedef std::string				TString;
#endif

#if defined (USE_SHAREMEMORY_POOL_ALLOC)
#include "common/Memory/memPool/shareMemoryPool.h"
#elif defined (UUSE_FASTMEMORY_POOL_ALLOC) 
#include "common/Memory/memPool/fastPool.h"
#endif

#if defined	(USE_STL_ALLOCATOR)
#include "common/Memory/stlAllocator/Virtual_allocator.h"
#endif

#include <assert.h>



#if defined(USE_MEMPOOL_STRING) || defined(USE_SHAREMEMORY_POOL_ALLOC) || defined(USE_FASTMEMORY_POOL_ALLOC)
using namespace Memory::MemoryPool;
#endif 


#if defined ( USE_SHAREMEMORY_POOL_ALLOC )
#define TSTREAM_MEM_FREE( p , s )	shareMemoryPool::GetInstancePtr()->Free( (p) , (s) )
#define TSTREAM_MEM_MALLOC(  s )	shareMemoryPool::GetInstancePtr()->Malloc( (s) )
#elif defined( USE_FASTMEMORY_POOL_ALLOC )
#define TSTREAM_MEM_FREE( p , s )	fastPool::GetInstancePtr()->Free( (p) , (s) )
#define TSTREAM_MEM_MALLOC(  s )	fastPool::GetInstancePtr()->Malloc( (s) )
#else 
#define TSTREAM_MEM_FREE( p , s ) 	free( (p) ) 
#define TSTREAM_MEM_MALLOC( s )		malloc( (s) )
#endif


#define		TSTREAM_NAMESPACE_BEGIN		namespace	TStream { 
#define		TSTREAM_NAMESPACE_END		};
#define		TSTREAM						TStream



#ifdef _WIN32
#if !defined( TSTREAM_STATIC_LIB)
#   ifdef  TSTREAM_NOT_CLIENT
#       define TStreamExport __declspec(dllexport)
#   else
#    	define TStreamExport __declspec(dllimport)
#   endif
#else
#		define TStreamExport
#endif
#else 
#define TStreamExport
#endif

#ifdef _WIN32
#pragma warning ( disable : 4251 )
#pragma warning ( disable : 4311 )	
#pragma warning ( disable : 4267 )
#endif

#include <vector>
#include <list>
#include <queue>
#include <set>
#include <map>
#ifdef _WIN32
#include <hash_set>
#include <hash_map>
using namespace stdext;
#else
#include <ext/hash_set>
#include <ext/hash_map>
using namespace __gnu_cxx;
#endif


#endif
