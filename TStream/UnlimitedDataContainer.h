/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:55
	filename: 	RawDataContainer
	author	:	tt
	module	:	流系统(序列化模块)
	purpose	:
		定长容器类。具有固定大小的储存区
		
	log		:
	
	
	Copyright (c) 汤腾
	
***********************************************************************/
	
#ifndef UNLIMITED_DATACONTAINER_H_
#define UNLIMITED_DATACONTAINER_H_

#ifdef _MSC_VER
#pragma once 
#endif

#include "RawDataContainer.h"

TSTREAM_NAMESPACE_BEGIN



/**
* @brief  无大小限制的BUF,具有自动增长功能
*
* 
*/
class TStreamExport CUnlimitedDataContainer : public CRawDataContainer
{
	typedef CRawDataContainer	Parent;

public:

	explicit CUnlimitedDataContainer( size_t nBytes = DEF_CONTAINER_SIZE ):Parent( nBytes ) {}
	virtual ~CUnlimitedDataContainer() {}

public:


	//////////////////////////////////////////////////////////////////////////
	//	重新设置BUF大小
	//	bool bDrop = true			是否丢弃原来数据
	//	如果新的size没有原来的大,那参数 bDrop 无效.数据丢弃
	//////////////////////////////////////////////////////////////////////////
	virtual void	ResetSize( size_t nSize , bool bDrop )
	{
		return ;
	}

	//////////////////////////////////////////////////////////////////////////
	//	返回最大容量
	//////////////////////////////////////////////////////////////////////////
	virtual size_t	Capacity() const { return 0xFFFFFFFF; }						


	//////////////////////////////////////////////////////////////////////////
	//	添加一定字节后，BUF是否满
	//	const size_t nSize				添加多少字节
	//////////////////////////////////////////////////////////////////////////
	virtual bool IsFull( const size_t nSize )
	{
		while( Parent::IsFull( nSize ) )
		{
			size_t nNewSize = m_vecBuffer.size() * 2;
			m_vecBuffer.resize( nNewSize );
		}
		return false;
	}

};


TSTREAM_NAMESPACE_END


#endif
