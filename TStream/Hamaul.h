/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:51
	filename: 	Hamaul
	author	:	tt
	module	:	流系统(序列化模块)
	purpose	:
		数据搬运工。
	
	log		:
	
	
	Copyright (c) 汤腾
	
***********************************************************************/
	
#ifndef DATAHAMAUL_H_
#define DATAHAMAUL_H_

#ifdef _MSC_VER
#pragma once 
#endif


#include "streamdef.h"

TSTREAM_NAMESPACE_BEGIN

	
//	[4/26/2007 qztt]
//	数据搬运工 
class	TStreamExport CDataHamaul
{
public:

	CDataHamaul():m_pData( 0 ),m_nSize( 0 ) { }
	~CDataHamaul() { Reset(); }

public:

	inline void	SetData( const char * pData , size_t nSize );
	const char *	GetData() const { return m_pData; } 
	void	Reset() { m_pData = 0; m_nSize = 0; }
	size_t Size() const { return m_nSize; }

private:

	const char *		m_pData;
	size_t		m_nSize;

};

inline void CDataHamaul::SetData( const char * pData , size_t nSize )
{
	m_pData = pData;
	m_nSize = nSize;
}

TSTREAM_NAMESPACE_END



#endif