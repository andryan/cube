#include "TStream/stream.h"
#include "TStream/RawDataContainer.h"
#include "TStream/UnlimitedDataContainer.h"
#include "cmdhelp.h"
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

template<typename T>
bool NolmalTest(TSTREAM::CStream & s)
{
	typedef typename std::list<T> LIST;
	typedef typename LIST::iterator LIST_ITER;

	LIST m_list;
	LIST m_list2;
	size_t n = 0;
	const size_t nTestCount = 10;

	n = 0;
	while (n < nTestCount)
	{
		T t;
		t = rand() % 255;
		m_list.push_back(t);
		n++;
	}
	s.Put(m_list);
	TSTREAM::StreamScopedRead r(s.GetContainer());
	r.GetStream().Get(m_list2);

	assert( m_list2 == m_list );

	typedef typename std::vector<T> VECTOR;
	typedef typename VECTOR::iterator VECTOR_ITER;
	VECTOR m_vec1;
	VECTOR m_vec2;

	n = 0;
	while (n < nTestCount)
	{
		T t;
		t = rand() % 255;
		m_vec1.push_back(t);
		n++;
	}
	s.Put(m_vec1);
	r.GetStream().Get(m_vec2);

	assert( m_vec1 == m_vec2 );

	typedef typename std::deque<T> DEQUE;
	typedef typename DEQUE::iterator DEQUE_ITER;
	DEQUE m_d1;
	DEQUE m_d2;

	n = 0;
	while (n < nTestCount)
	{
		T t;
		t = rand() % 255;
		m_d1.push_back(t);
		n++;
	}
	s.Put(m_d1);
	r.GetStream().Get(m_d2);

	assert( m_d1 == m_d2 );

	return true;
}

template<typename F, typename S>
bool PairTest(TSTREAM::CStream & s)
{
	typedef typename std::pair<F, S> PAIR_TYPE;

	std::vector<PAIR_TYPE> vec;
	const unsigned int nTestCount = 100;
	for (unsigned int n = 0; n < nTestCount; n++)
	{
		PAIR_TYPE p = std::make_pair(F(), S());
		p.first = rand() % 255;
		p.second = rand() % 255;
		s.Put(p);
		vec.push_back(p);
	}
	TSTREAM::StreamScopedRead r(s.GetContainer());

	for (unsigned int n = 0; n < nTestCount; n++)
	{
		PAIR_TYPE p;
		r.GetStream().Get(p);
		assert( vec[n] == p );
	}

	return true;
}

template<typename K, typename V>
bool MapTest(TSTREAM::CStream & s)
{
	TSTREAM::StreamScopedRead r(s.GetContainer());
	unsigned int nTestCount = 100;
	typedef typename std::map<K, V> MAP_TYPE;
	typedef typename MAP_TYPE::iterator MAP_TYPE_ITER;

	MAP_TYPE map;
	for (unsigned int n = 0; n < nTestCount; n++)
	{
		K k = rand() % 255;
		V v = rand() % 255;
		map.insert(std::make_pair(k, v));
	}

	s.Put(map);

	MAP_TYPE r_map;
	r.GetStream().Get(r_map);
	assert( map == r_map );

	typedef typename std::multimap<K, V> MULIT_MAP_TYPE;
	typedef typename MAP_TYPE::iterator MULIT_MAP_TYPE_ITER;
	MULIT_MAP_TYPE Mulitmap;

	for (unsigned int n = 0; n < nTestCount; n++)
	{
		K k = rand() % 255;
		V v = rand() % 255;
		Mulitmap.insert(std::make_pair(k, v));
	}
	s.Put(Mulitmap);

	MULIT_MAP_TYPE r_mmap;
	r.GetStream().Get(r_mmap);
	assert( Mulitmap == r_mmap );

#ifdef _WIN32

	//	linux 下编译不过，暂时这样
	//typedef typename hash_map<K,V>						HASH_MAP_TYPE;
	hash_map<K, V> HashMap;

	for ( unsigned int n = 0; n < nTestCount; n++ )
	{
		K k = rand()%255;
		V v = rand()%255;
		HashMap.insert( std::make_pair( k , v ) );
		//HashMap[k] = v;
	}
	s.Put( HashMap );

	hash_map<K,V> r_HashMap;
	r.GetStream().Get( r_HashMap );
	assert( HashMap == r_HashMap );
#endif

	return true;
}

template<typename T>
bool SetTest(TSTREAM::CStream & s)
{
	TSTREAM::StreamScopedRead r(s.GetContainer());
	typedef typename std::set<T> SET_TYPE;
	typedef typename SET_TYPE::iterator SET_TYPE_ITER;
	unsigned int nTestCount = 100;

	SET_TYPE set;
	for (unsigned int n = 0; n < nTestCount; n++)
	{
		T t = rand() % 255;
		set.insert(t);
	}
	s.Put(set);

	SET_TYPE r_set;
	r.GetStream().Get(r_set);
	assert( set == r_set );

	typedef typename std::multiset<T> MULIT_SET_TYPE;
	typedef typename MULIT_SET_TYPE::iterator MULIT_SET_TYPE_ITER;
	MULIT_SET_TYPE Mulitset;

	for (unsigned int n = 0; n < nTestCount; n++)
	{
		T t = rand() % 255;
		Mulitset.insert(t);
	}
	s.Put(Mulitset);

	MULIT_SET_TYPE r_mset;
	r.GetStream().Get(r_mset);
	assert( Mulitset == r_mset );

#ifdef _WIN32
	typedef typename hash_set<T> HASH_SET_TYPE;
	typedef typename HASH_SET_TYPE::iterator HASH_SET_TYPE_ITER;
	HASH_SET_TYPE HashSet;

	for ( unsigned int n = 0; n < nTestCount; n++ )
	{
		T t = rand()%255;
		HashSet.insert( t );
	}
	s.Put( HashSet );

	HASH_SET_TYPE r_HashSet;
	r.GetStream().Get( r_HashSet );
	assert( r_HashSet == HashSet );
#endif

	return true;
}

void StringTest(TSTREAM::CStream & s)
{
	char szCStyle[128];
	std::string strCppStyle;

	strcpy(szCStyle, "c风格的字符串");
	strCppStyle = "C++风格的字符串";

	s.Put(szCStyle);
	s.Put(strCppStyle);
	TSTREAM::StreamScopedRead r(s.GetContainer());
	char szOutCStyle[128];
	std::string strOutCppStyle;
	r.GetStream().Get(szOutCStyle);
	r.GetStream().Get(strOutCppStyle);

	assert( strcmp( szCStyle , szOutCStyle ) == 0 );
	assert( strOutCppStyle == strCppStyle );

	std::vector<std::string> strVec;
	std::list<std::string> strList;
	std::deque<std::string> strDeq;

	std::string strTest[5] =
	{ "tangtang", "tt", "bunengyongzhongwen ", "gan", "why?" };

	const unsigned long nTestCount = 100;
	for (unsigned int n = 0; n < nTestCount; n++)
	{
		int i = rand() % 5;
		strVec.push_back(strTest[i]);
		i = rand() % 5;
		strList.push_back(strTest[i]);
		i = rand() % 5;
		strDeq.push_back(strTest[i]);
	}

	s.Put(strVec);
	s.Put(strList);
	s.Put(strDeq);

	std::vector<std::string> strVecOut;
	std::list<std::string> strListOut;
	std::deque<std::string> strDeqOut;
	r.GetStream().Get(strVecOut);
	r.GetStream().Get(strListOut);
	r.GetStream().Get(strDeqOut);

	assert( strVec == strVecOut );
	assert( strList == strListOut );
	assert( strDeq == strDeqOut );

}
;

DECLARATION_PACKAGE_0( CmdTest_0 , 1 , 1 )
;
DECLARATION_PACKAGE_1( CmdTest_1 , 1 , 2 , char , c1 )
;
DECLARATION_PACKAGE_2( CmdTest_2 , 1 , 3 , char , c1 , unsigned char , uc2 )
;
DECLARATION_PACKAGE_3( CmdTest_3 , 1 , 4 , char , c1 , unsigned char , uc2 , short int , i3 )
;
DECLARATION_PACKAGE_4( CmdTest_4 , 1 , 5 , char , c1 , unsigned char , uc2 , short int , i3 , int , i4 )
;
DECLARATION_PACKAGE_5( CmdTest_5 , 1 , 6 , char , c1 , unsigned char , uc2 , short int , i3 , int , i4 ,
		unsigned int , n5 )
;
DECLARATION_PACKAGE_6( CmdTest_6 , 1 , 7 , char , c1 , unsigned char , uc2 , short int , i3 , int , i4 ,
		unsigned int , n5 , std::string , strName )

void CmdPack_UnpackTest(TSTREAM::CStream & s)
{
	TSTREAM::StreamScopedRead r(s.GetContainer());

	MODI_CmdTest_0 cmd0;
	MODI_CmdTest_0 cmd0_out;
	unsigned int nPack = 0;//MODI_PackCmd(s, cmd0);
	unsigned int nUnPack = 0;//MODI_UnpackCmd(r.GetStream(), cmd0_out);

	s << cmd0;
	r.GetStream() >> cmd0_out;

	assert( cmd0.byCmd == cmd0_out.byCmd && cmd0.byParam == cmd0_out.byParam );
	assert( nPack == nUnPack );

	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_1 cmd1;
	MODI_CmdTest_1 cmd1_out;
	cmd1.c1 = rand() % 128;
	nPack = MODI_PackCmd(s, cmd1);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd1_out);
	assert( cmd1.byCmd == cmd1_out.byCmd && cmd1.byParam == cmd1_out.byParam && cmd1.c1 == cmd1_out.c1 );
	assert( nPack == nUnPack );
	

	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_2 cmd2;
	MODI_CmdTest_2 cmd2_out;
	cmd2.c1 = rand() % 128;
	cmd2.uc2 = rand() % 255;
	nPack = MODI_PackCmd(s, cmd2);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd2_out);
	assert( cmd2.byCmd == cmd2_out.byCmd && cmd2.byParam == cmd2_out.byParam && cmd2.c1 == cmd2_out.c1 && cmd2.uc2 == cmd2_out.uc2 );
	assert( nPack == nUnPack );


	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_3 cmd3;
	MODI_CmdTest_3 cmd3_out;
	cmd3.c1 = rand() % 128;
	cmd3.uc2 = rand() % 255;
	cmd3.i3 = rand() % 10000;
	nPack = MODI_PackCmd(s, cmd3);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd3_out);
	assert( cmd3.byCmd == cmd3_out.byCmd && cmd3.byParam == cmd3_out.byParam && cmd3.c1 == cmd3_out.c1 && cmd3.uc2 == cmd3_out.uc2 && cmd3.i3 == cmd3_out.i3 );
	assert( nPack == nUnPack );
	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_4 cmd4;
	MODI_CmdTest_4 cmd4_out;
	cmd4.c1 = rand() % 128;
	cmd4.uc2 = rand() % 255;
	cmd4.i3 = rand() % 10000;
	cmd4.i4 = rand() % 65534;
	nPack = MODI_PackCmd(s, cmd4);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd4_out);
	assert( cmd4.byCmd == cmd4_out.byCmd && cmd4.byParam == cmd4_out.byParam && cmd4.c1 == cmd4_out.c1 && cmd4.uc2 == cmd4_out.uc2 && cmd4.i3 == cmd4_out.i3 && cmd4.i4 == cmd4_out.i4 );
	assert( nPack == nUnPack );
	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_5 cmd5;
	cmd5.c1 = rand() % 128;
	cmd5.uc2 = rand() % 255;
	cmd5.i3 = rand() % 10000;
	cmd5.i4 = rand() % 65534;
	cmd5.n5 = rand() % 0xFFFFFFFF;
	MODI_CmdTest_5 cmd5_out;
	nPack = MODI_PackCmd(s, cmd5);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd5_out);
	assert( cmd5.byCmd == cmd5_out.byCmd && cmd5.byParam == cmd5_out.byParam && cmd5.c1 == cmd5_out.c1 &&
			cmd5.uc2 == cmd5_out.uc2 && cmd5.i3 == cmd5_out.i3 && cmd5.i4 == cmd5_out.i4 && cmd5.n5 == cmd5_out.n5 );
	assert( nPack == nUnPack );
	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

	MODI_CmdTest_6 cmd6;
	cmd6.c1 = rand() % 128;
	cmd6.uc2 = rand() % 255;
	cmd6.i3 = rand() % 10000;
	cmd6.i4 = rand() % 65534;
	cmd6.n5 = rand() % 0xFFFFFFFF;
	cmd6.strName = "hello TStream.";
	MODI_CmdTest_6 cmd6_out;
	nPack = MODI_PackCmd(s, cmd6);
	nUnPack = MODI_UnpackCmd(r.GetStream(), cmd6_out);
	//	assert( cmd6.byCmd == cmd6_out.byCmd && cmd6.byParam == cmd6_out.byParam && cmd6.c1 == cmd6_out.c1 &&
	//			cmd6.uc2 == cmd6_out.uc2 && cmd6.i3 == cmd6_out.i3 && cmd6.i4 == cmd6_out.i4 &&
	//			cmd6.n5 == cmd6_out.n5 && cmd6.strName = cmd6_out.strName );
	assert( nPack == nUnPack );
	cout << "Pack = " << nPack << endl;
	cout << "Unpack = " << nUnPack << endl;

}

int main()
{
	srand(0);
	//TSTREAM::CRawDataContainer	rawCon( 1024 * 1024 * 5 );
	TSTREAM::CUnlimitedDataContainer UnlimitedCon(512);
	TSTREAM::StreamScopedWrite sW(&UnlimitedCon);

	cout << "Test Cmd Pack and Unpack Funs .. " << endl;
	CmdPack_UnpackTest(sW.GetStream());

	cout << "Test std::list , std::vector , std::deque" << endl;
	NolmalTest<short int> (sW.GetStream());
	NolmalTest<int> (sW.GetStream());
	NolmalTest<double> (sW.GetStream());
	NolmalTest<float> (sW.GetStream());
	NolmalTest<char> (sW.GetStream());
	NolmalTest<long> (sW.GetStream());
	NolmalTest<unsigned int> (sW.GetStream());
	NolmalTest<unsigned long> (sW.GetStream());
	NolmalTest<unsigned char> (sW.GetStream());
	NolmalTest<unsigned long long> (sW.GetStream());
	NolmalTest<unsigned short int> (sW.GetStream());

	sW.GetStream().GetContainer()->Reset();

	cout << "Test std::map , mulitmap , hash_map " << endl;
	PairTest<short int, float> (sW.GetStream());
	PairTest<double, float> (sW.GetStream());
	PairTest<float, char> (sW.GetStream());
	PairTest<float, long> (sW.GetStream());
	PairTest<unsigned short int, unsigned int> (sW.GetStream());
	PairTest<unsigned long, unsigned char> (sW.GetStream());
	PairTest<float, char> (sW.GetStream());
	PairTest<unsigned long long, unsigned short int> (sW.GetStream());

	sW.GetStream().GetContainer()->Reset();

	cout << "Test std::string , c style string" << endl;
	MapTest<short int, float> (sW.GetStream());
	MapTest<double, float> (sW.GetStream());
	MapTest<float, char> (sW.GetStream());
	MapTest<unsigned short int, unsigned int> (sW.GetStream());
	MapTest<unsigned long, unsigned char> (sW.GetStream());
	//MapTest<unsigned long long , unsigned short int>( sW.GetStream() );

	sW.GetStream().GetContainer()->Reset();

	SetTest<short int> (sW.GetStream());
	SetTest<int> (sW.GetStream());
	SetTest<double> (sW.GetStream());
	SetTest<float> (sW.GetStream());
	SetTest<char> (sW.GetStream());
	SetTest<long> (sW.GetStream());
	SetTest<unsigned short int> (sW.GetStream());
	SetTest<unsigned int> (sW.GetStream());
	SetTest<unsigned long> (sW.GetStream());
	SetTest<unsigned char> (sW.GetStream());
	SetTest<unsigned long long> (sW.GetStream());
	SetTest<unsigned short int> (sW.GetStream());

	sW.GetStream().GetContainer()->Reset();

	StringTest(sW.GetStream());

	//hash_map<int, double, hash<int>,  equal_to<int> , allocator<double> >				HashMap;

	return 0;
}

