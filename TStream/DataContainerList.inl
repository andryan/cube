
template<typename T>
	CDataConListAllocter<T>							CDataContainerList<T>::mc_Allocter;


template < typename T >
CDataContainerList<T>::CDataContainerList():m_CurReadIter( m_ConList.end() )
{

}

template < typename T >
CDataContainerList<T>::~CDataContainerList()
{
	Clear();
}

template < typename T >
size_t	CDataContainerList<T>::GetWriteSize() const
{
	size_t nRet = 0;

	if( !m_ConList.empty() )
	{
		CONLIST::const_iterator itor = m_ConList.begin();
		while ( itor != m_ConList.end() )
		{
			T * p = *itor;
			size_t nWriteCount = p->GetWriteSize();
			nRet += nWriteCount;
			itor++;
		}
	}

	return nRet;
}

template < typename T >
size_t	CDataContainerList<T>::GetReadSize() const
{
	size_t nRet = 0;

	if( !m_ConList.empty() )
	{
		CONLIST::const_iterator itor = m_ConList.begin();
		while ( itor != m_ConList.end() )
		{
			T * p = *itor;
			size_t nReadCount = p->GetReadSize();
			if( !nReadCount )
				break;
			nRet += nReadCount;
			itor++;
		}
	}

	return nRet;
}

template < typename T >
	bool	CDataContainerList<T>::Write( const void * lpBuf , const size_t & nSize )
{
	assert( lpBuf && nSize > 0 );

	bool bRet = true;

	T * p = 0;

	CONLIST::reverse_iterator itor = m_ConList.rbegin();
	if( itor != m_ConList.rend() )
	{
		p = *itor;
	}

	size_t nLeft = nSize;
	const char * pData = ( const char * )lpBuf;
	size_t nPos = 0;

	while ( nLeft )
	{
		if( !p || !p->UnUsed() )
		{
			p = (T *)mc_Allocter.ms_ConListPool.construct();
			m_ConList.push_back( p );
		}

		size_t nWrite = min( nLeft , p->UnUsed() );

		assert( "Write Size == 0 " && nWrite );

		bRet = p->Write( pData + nPos , nWrite );

		if( !bRet )
		{
			assert(0);
			break;
		}

		assert( nLeft >= nWrite );

		nPos += nWrite;
		nLeft -= nWrite;
	}

	return bRet;
}


template < typename T >
	bool	CDataContainerList<T>::WriteSkip( const size_t & nSize )
{
	assert( nSize > 0 );

	bool bRet = true;

	T * p = 0;

	CONLIST::reverse_iterator itor = m_ConList.rbegin();
	if( itor != m_ConList.rend() )
	{
		p = *itor;
	}

	size_t nLeft = nSize;
	size_t nPos = 0;

	while ( nLeft )
	{
		if( !p || !p->UnUsed() )
		{
			p = (T*)mc_Allocter.ms_ConListPool.construct();
			m_ConList.push_back( p );
		}

		size_t nWrite = min( nLeft , p->UnUsed() );

		assert(  "WriteSkip Size == 0 " && nWrite );

		bRet = p->WriteSkip( nWrite );

		if( !bRet )
		{
			assert(0);
			break;
		}

		assert( nLeft >= nWrite );

		nPos += nWrite;
		nLeft -= nWrite;
	}

	return bRet;
}

template < typename T >
	bool	CDataContainerList<T>::ReadSkipTo( size_t n ,  char To )
{
	bool bRet = false;
	size_t i  = 0;
	
	size_t nIndex = 1;


	while ( i < n  &&  !m_ConList.empty()  )
	{
		CONLIST::iterator itor = m_ConList.begin();
		if( itor == m_ConList.end() )
		{
			break;
		}

		T * p = *itor;

		if( p->ReadSkipTo(  nIndex , To ) )
		{
			nIndex += 1;
			i += 1;
		}
		else
		{
			//mc_Allocter.ms_ConListPool.destroy( p );
			//m_ConList.erase( itor );
			nIndex = 1;
		}
	}

	if( i == n )
	{
		bRet = true;
	}

	return bRet;
}

//	读取数据
template < typename T >
	bool	CDataContainerList<T>::Read( void * lpBuf , const size_t & nSize )
{
	if( !m_ConList.empty() )
	{
		size_t nRead = nSize;
		const char * pData = ( const char * )lpBuf;
		size_t nPos = 0;

		if( m_CurReadIter == m_ConList.end() )
		{
			m_CurReadIter = m_ConList.begin();
		}

		while ( nRead != 0  )
		{
			assert( nRead <= nSize );

			CONLIST::iterator itor = m_CurReadIter; // m_ConList.begin();
			if( itor == m_ConList.end() )
			{
				break;
			}

			T * p = *itor;

			size_t nRealRead = min( nRead , p->UnRead() );

			assert( "Read Size == 0 " && nRealRead );
		
			if( !p->Read( (void *)(pData + nPos) , nRealRead ) )
			{
				assert(0);
				return false;
			}

			if( !p->UnRead() )
			{
				m_CurReadIter++;
				//mc_Allocter.ms_ConListPool.destroy( p );
				//m_ConList.erase( itor );
			}

			nPos += nRealRead;
			nRead -= nRealRead;
		}

		return true;
	}

	return false;
}

template < typename T >
	void	CDataContainerList<T>::Peek( void * lpBuf , size_t & nSize )
{
	if( m_ConList.empty() )
	{
		nSize = 0;
	}
	else
	{
		size_t nPeek = 0;

		CONLIST::iterator itor = m_ConList.begin();

		while ( nPeek  != nSize )
		{
			if( itor == m_ConList.end() )
			{
				nSize = nPeek;
				break;
			}

			T * p = *itor;

			size_t nRealPeek = min( (nSize - nPeek) , p->Capacity() );

			assert( "Peek Size == 0 " && nRealPeek );

			p->Peek( (char *)lpBuf , nRealPeek );

			nPeek += nRealPeek;

			if( !nPeek )
			{
				nSize = nPeek;
				break;
			}

			itor ++;
		}
	}
}

template < typename T >
	bool	CDataContainerList<T>::ReadSkip( const size_t & nSize )
{
	if( !m_ConList.empty() )
	{
		size_t nRemove = 0;

		if( m_CurReadIter == m_ConList.end() )
		{
			m_CurReadIter = m_ConList.begin();
		}

		while ( nRemove != nSize  )
		{
			CONLIST::iterator itor = m_CurReadIter;// m_ConList.begin();
			if( itor == m_ConList.end() )
			{
				break;
			}

			T * p = *itor;

			size_t nRealRemove = min( nSize - nRemove , p->UnRead() );

			if( !p->ReadSkip(  nRealRemove ) )
			{
				assert(0);
				return false;
			}

			nRemove += nRealRemove;

			if( !p->UnRead() )
			{
				m_CurReadIter ++;

				//mc_Allocter.ms_ConListPool.destroy( p );
				//m_ConList.erase( itor );
			}
		}

		return  nRemove == nSize;
	}

	return false;
}

//	清空buf
template < typename T >
	void	CDataContainerList<T>::Clear()
{
	while( !m_ConList.empty() )
	{
		CONLIST::iterator itor = m_ConList.begin();
		T * p = *itor;
		p->Clear();
		mc_Allocter.ms_ConListPool.destroy( p );
		m_ConList.erase( itor );
	}
}

template < typename T >
	bool	CDataContainerList<T>::IsEmpty() const
{
	return m_ConList.empty();
}

//	返回剩余未使用buf大小
template < typename T >
	size_t  CDataContainerList<T>::UnUsed() const
{
	size_t nSize = 0;

	CONLIST::const_iterator itor = m_ConList.begin();
	while( itor != m_ConList.end() )
	{
		const T * p = *itor;
		if( p )
		{
			nSize += p->UnUsed();
		}

		itor++;
	}

	return nSize;
}

template < typename T >
	size_t  CDataContainerList<T>::UnRead() const
{
	size_t nSize = 0;

	CONLIST::const_iterator itor = m_ConList.begin();
	while( itor != m_ConList.end() )
	{
		const T * p = *itor;
		if( p )
		{
			nSize += p->UnRead();
		}

		itor++;
	}

	return nSize;
}

//	得到整个BUF的数据
template < typename T >
	void	CDataContainerList<T>::GetData( CDataHamaul & ham )
{
	assert( "调用了一个没有被实现的函数!" && 0 );
}

//	返回 BUF 的有效数据开始的地址
template < typename T >
	const char *	CDataContainerList<T>::GetBufferPtr() const
{
	assert( "调用了一个没有被实现的函数!"  && 0 );
	return 0;
}

//	返回 BUF 的有效大小
template < typename T >
	size_t	CDataContainerList<T>::Capacity() const
{
	return 0xFFFFFFF;
}

template < typename T >
	void	CDataContainerList<T>::Reset()
{
	Clear();

	/*CONLIST::iterator itor = m_ConList.begin();
	while( itor != m_ConList.end() )
	{
		T * p = *itor;
		p->Reset();
		itor++;
	}
	m_CurReadIter = m_ConList.end();*/
}	

template < typename T >
	void	CDataContainerList<T>::ResetSize(size_t nSize , bool bDrop /* = true  */)
{
	assert(  "DataContainerList do not need ResetSize!" && 0 );
}

template < typename T >
	bool CDataContainerList<T>::SeekToRead(  size_t nPos ,  void * lpBuf , const size_t & nSize )	
{ 
	if( !m_ConList.empty() )
	{
		size_t nCurPos = 0;
		size_t nLeftSize = nSize;

		CONLIST::iterator itor = m_ConList.begin();
		while ( itor != m_ConList.end() )
		{
			T * p = *itor;
			size_t nCapcatiy = p->Capacity();
			if( nCurPos + nCapcatiy <= nPos )
			{
				nCurPos += nCapcatiy;
			}
			else 
			{
				size_t nBegin = nPos - nCurPos;				//	开始写的位置
				size_t nLeft = nCapcatiy - nBegin;			//	剩余大小
				size_t nRead = min( nLeft , nLeftSize );
				size_t nDataPos = 0;


				//	写完当前这个容器
				bool bRet = p->SeekToRead( nBegin , (char *)(lpBuf) + nDataPos , nRead );
				assert( bRet );
				nLeftSize -= nRead;
				nDataPos = nRead;

				//	是否还有剩余
				while( nLeftSize )
				{
					//	还有剩余，说明在AB容器尾头交接处
					itor++;
					//	是否在末尾
					if( itor == m_ConList.end() )
					{
						return false;
					}
					else 
					{
						p = *itor;
					}

					nRead = min( p->Capacity() , nLeftSize );
					p->SeekToRead( 0 , (char *)(lpBuf) + nDataPos , nRead );
					nLeftSize -= nRead;
					nDataPos += nRead;
				}

				return true;
			}
			itor++;
		}

	}

	return false;
}

template < typename T >
	bool CDataContainerList<T>::SeekToWrite( size_t nPos ,  const void * lpBuf , const size_t & nSize) 
{ 
	size_t nCurPos = 0;
	size_t nLeftSize = nSize;
/*
	if( m_ConList.empty() )
	{
		char szTemp[4096];
		memset( szTemp , 0 , sizeof(szTemp) );

		bool bRet = false;
		while ( nCurPos < nPos )
		{
			size_t nMin = min( 4096 , nPos - nCurPos );
			bRet = Write( szTemp , nMin );
			assert( bRet );
			nCurPos += nMin;
		}

		bRet =  Write( lpBuf , nSize );
		assert( bRet );

		return true;
	}*/

	if( m_ConList.empty() )
		return false;

	//	总共的容量
	size_t nTotalCap = 0;
	{
		CONLIST::iterator itor = m_ConList.begin();
		while ( itor != m_ConList.end() )
		{
			T * p = *itor;
			nTotalCap += p->Capacity();
			itor++;
		}

	}

	if( nPos + nSize > nTotalCap )
		return false;
	
	CONLIST::iterator itor = m_ConList.begin();
	while ( itor != m_ConList.end() )
	{
		T * p = *itor;
		size_t nCapcatiy = p->Capacity();
		if( nCurPos + nCapcatiy <= nPos )
		{
			nCurPos += nCapcatiy;
		}
		else 
		{
			
			size_t nBegin = nPos - nCurPos;				//	开始写的位置
			size_t nLeft = nCapcatiy - nBegin;			//	剩余大小
			size_t nWrite = min( nLeft , nLeftSize );
			size_t nDataPos = 0;
		

			//	写完当前这个容器
			bool bRet = p->SeekToWrite( nBegin , (char *)(lpBuf) + nDataPos , nWrite );
			assert( bRet );
			nLeftSize -= nWrite;
			nDataPos = nWrite;

			//	是否还有剩余
			while( nLeftSize )
			{
				//	还有剩余，说明在AB容器尾头交接处
				itor++;
#ifdef _DEBUG
				//	是否在末尾
				if( itor == m_ConList.end() )
				{
					//bRet = Write( (char *)(lpBuf) + nDataPos , nLeftSize );
					assert( false );
					return false;
					//break;
				}
				else 
#endif
				{
					p = *itor;
				}

				nWrite = min( p->Capacity() , nLeftSize );
				p->SeekToWrite( 0 , (char *)(lpBuf) + nDataPos , nWrite );
				nLeftSize -= nWrite;
				nDataPos += nWrite;
			}

			break;

		}
		itor++;
	}

	return true;
}

template < typename T >
	void	CDataContainerList<T>::GetData(CDataHamaul & ham , unsigned int nIdx /* = 0  */)
{	
	CONLIST::iterator itor = m_ConList.begin();
	unsigned int n = 0;
	while( itor != m_ConList.end() )
	{
		if( n == nIdx )
		{
			T * p = *itor;
			p->GetData( ham );
			break;
		}

		n++;
		itor++;
	}
}

template < typename T >
	size_t	CDataContainerList<T>::GetBufCount() const 
{	
	return	m_ConList.size();
}

template < typename T >
const T *	CDataContainerList<T>::GetBufferPtr( size_t nIdx ) const
{
	const T * pRet  = 0;

	CONLIST::const_iterator  itor = m_ConList.begin();
	unsigned int n = 0;
	while( itor != m_ConList.end() )
	{
		if( n == nIdx )
		{
			pRet = *itor;
			break;
		}

		n++;
		itor++;
	}

	return pRet;
}

