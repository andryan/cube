/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:54
	filename: 	streammap
	author	:	tt
	module	:	流系统(序列化模块)
	purpose	:
		流图。虽然很好用，但是没什么效率。尽量别用在网络方面
	
	log		:
	
	
	Copyright (c) 汤腾
	
***********************************************************************/
	
#ifndef STREAM_STREAM_MAP_H_
#define STREAM_STREAM_MAP_H_

#ifdef _MSC_VER
#pragma once
#endif

#include "common/Memory/memPool/fastPool.h"

namespace TStream
{
	template <typename Key>
	class	CStreamMap
	{

	public:

		CStreamMap();
		~CStreamMap();
	
		CStreamMap( const CStreamMap & that );
		const CStreamMap & operator = ( const CStreamMap  & that );

		bool operator == ( const CStreamMap  & that ) const;
		bool operator != ( const CStreamMap  & that ) const;

	private:

		void Copy( const CStreamMap & that );
		

	public:

		inline bool IsIn( const Key & key );
		inline void	Delete( const Key & key );
		void		GetKeys( std::vector<Key> & vecOut );
		void		Clear();
	

	public:

		template < typename T >
			bool Put( const Key & key , const T & putValue , const size_t nValueSize )
		{
			if( IsIn( key ) )
				return false;

			//CDataContainer * pData = new CRawDataContainer(nValueSize);
			void * pAddr = Memory::MemoryPool::fastPool::GetInstancePtr()->Malloc( sizeof( CRawDataContainer ) );
			CDataContainer * pData = new (pAddr) CRawDataContainer(nValueSize);
			CStream stream;
			stream.BeginWrite( pData );
			stream.Put( putValue );
			stream.EndWrite();
			m_map.insert( std::make_pair( key , pData ) );
			return true;
		}

		template < typename T >
			bool Get( const Key & key ,  T & GetValue )
		{
			std::map< Key,CDataContainer * >::iterator itor = m_map.find( key );
			if( itor == m_map.end() )
				return false;

			CDataContainer * pData = itor->second;
			CStream stream;
			stream.BeginRead( pData );
			stream.Get( GetValue );
			stream.EndRead();
			pData->Reset();
			
			return true;
		}

		void	PutInto( CStream & stream ) const;
		void	GetFrom( CStream & stream );

	private:

		typedef std::map< Key,CDataContainer * >						MAP;
		

	private:

		MAP					m_map;
	};

	template < typename Key > 
		CStreamMap<Key>::CStreamMap()
	{

	}

	template < typename Key  >
		CStreamMap<Key>::~CStreamMap()
	{
		Clear();
	}

	template < typename Key  >
		CStreamMap<Key>::CStreamMap( const CStreamMap & that )
	{
		Copy( that );
	}

	template < typename Key  >
		const CStreamMap<Key> & CStreamMap<Key>::operator = ( const CStreamMap & that )
	{
		if( this != &that )
		{
			Copy( that );
		}
		return *this;
	}
	
	template < typename Key  >
		void CStreamMap<Key>::Copy( const CStreamMap & that )
	{
		Clear();
		
		std::map< Key,CDataContainer * >::iterator itor = that.m_map.begin();
		while( itor != that.m_map.end() )
		{
			
			++itor;
		}
	}

	template < typename Key  >
		void CStreamMap<Key>::Clear()
	{
		std::map< Key,CDataContainer * >::iterator itor = m_map.begin();
		while( itor != m_map.end() )
		{
			CDataContainer * pTemp = itor->second;
			if( pTemp )
			{
				Memory::MemoryPool::fastPool::GetInstancePtr()->Delete( pTemp );
				//delete pTemp;
				pTemp = 0;
			}
			itor++;
		}
		m_map.clear();
	}

	template < typename Key  >
		bool CStreamMap<Key>::operator == ( const CStreamMap & that ) const
	{
		if( m_map.size() == that.m_map.size()  )
		{
			std::map< Key,CDataContainer * >::const_iterator itor = m_map.begin();
			std::map< Key,CDataContainer * >::const_iterator itor_that = m_map.begin();
			while( itor_that != m_map.end() )
			{
				const Key & thisKey = itor->first;
				const Key & thatKey = itor_that->first;
				if( thisKey != thatKey )
					return false;

				const CDataContainer * pThisDataCon = itor->second;
				const CDataContainer * pThatDataCon = itor_that->second;
				if( *pThatDataCon != *pThisDataCon )
					return false;

				itor_that++;
				itor++;
			}
			return true;
		}
		return false;
	}

	template < typename Key  >
		bool CStreamMap<Key>::operator != ( const CStreamMap & that ) const
	{
		return !(operator == ( that ) );
	}

	template < typename Key  >
		inline bool CStreamMap<Key>::IsIn( const Key & key )
	{
		std::map< Key,CDataContainer * >::iterator itor = m_map.find( key );
		return (itor != m_map.end());
	}

	template < typename Key  >
		inline void CStreamMap<Key>::Delete(const Key & key )
	{
		std::map< Key,CDataContainer * >::iterator itor = m_map.find( key );
		if( itor != m_map.end() )
		{
			CDataContainer * pTemp = itor->second;
			if( pTemp )
			{
				Memory::MemoryPool::fastPool::GetInstancePtr()->Delete( pTemp );
				//delete pTemp;
				pTemp = 0;
			}
			m_map.earse( itor );
		}
	}

	template < typename Key  >
		void	CStreamMap<Key>::GetKeys( std::vector<Key> & vecOut )
	{
		vecOut.clear();
		std::map< Key,CDataContainer * >::iterator itor = m_map.begin();
		while( itor != m_map.end() )
		{
			vecOut.push_back( itor->first );
			itor++;
		}
	}

	template < typename Key  >
		void	CStreamMap<Key>::PutInto( CStream & stream ) const
	{
		assert( "StreamMap size is 0" && !m_map.empty() );

		std::map< Key,CDataContainer * >::const_iterator itor = m_map.begin();
		stream.Put( (unsigned int)m_map.size() );

		while( itor != m_map.end() )
		{
			const Key & key = itor->first;
			CDataContainer * pTemp = itor->second;
			stream.Put( key );
			stream.Put( *pTemp );
			itor++;
		}
	}

	template < typename Key  >
		void	CStreamMap<Key>::GetFrom( CStream & stream )
	{
		Clear();
		
		size_t nSize = 0;
		stream.Get( nSize );

		assert( "StreamMap size is 0" && nSize );

		for( size_t n = 0; n < nSize; n++ )
		{
			Key key;


			//CDataContainer * pTemp = new CRawDataContainer;
			CDataContainer * pTemp = Memory::MemoryPool::fastPool::GetInstancePtr()->New( CRawDataContainer );

			stream.Get( key );
			stream.Get( *pTemp );
			m_map.insert( std::make_pair( key , pTemp ) );
		}
	}

}

namespace	StreamHelp
{
	template <typename Key  >
		enStreamTag	GetTag( const TStream::CStreamMap<Key> &  streamMap ) 
	{ 
		return enTagStreamMap; 
	} 

	template <typename Key >
		void	Put( TStream::CStream & stream ,  const TStream::CStreamMap<Key> &  streamMap )
	{
		streamMap.PutInto( stream );
	}

	template <typename Key >
		void	Get( TStream::CStream & stream ,  TStream::CStreamMap<Key> &  streamMap )
	{
		streamMap.GetFrom( stream );
	}
};


#endif