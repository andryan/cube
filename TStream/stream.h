/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:51
	filename: 	stream
	author	:	tt
	module	:	��ϵͳ(���л�ģ��)
	purpose	:
		��ϵͳ�����л��ӿ�
	
	log		:
	
	
	Copyright (c) ����
	
***********************************************************************/
	
#ifndef T_STREAM_H____
#define T_STREAM_H____

#ifdef _MSC_VER
#pragma once 
#endif


#include "streamdef.h"
#include "streamhelper.h"
#include "streamtag.h"
#include "DataContainer.h"




TSTREAM_NAMESPACE_BEGIN

class	TStreamExport CStream
{

	
public:

	CStream()
	{
			m_pDataContainer = 0;
			m_enState = enNone;
	}

	~CStream()
	{

	}

public:



	void	BeginWrite(CDataContainer * pDataContainer)
	{
	#ifdef _DEBUG

		assert( pDataContainer && "Invalid [CDataContainer * pDataContainer]" );

		if( m_enState != enNone )
		{
			assert( "Error TStream State!" && 0 );
		}

	#endif

		m_pDataContainer = pDataContainer;
		m_enState = enWrite;
	}

	void	EndWrite()
	{

	#ifdef _DEBUG

		if( m_enState != enWrite )
		{
			assert( "Error TStream State!" && 0 );
		}

	#endif

		m_pDataContainer = 0;

		m_enState = enNone;
	}

	void	BeginRead(CDataContainer * pDataContainer )
	{
	#ifdef _DEBUG

		assert( pDataContainer && "Invalid [CDataContainer * pDataContainer]" );

		if( m_enState != enNone )
		{
			assert( "Error TStream State!" && 0 );
		}

	#endif

		m_pDataContainer = pDataContainer;

		m_enState = enRead;
	}

	void	EndRead()
	{
	#ifdef _DEBUG

		if( m_enState != enRead )
		{
			assert( "Error TStream State!" && 0 );
		}

	#endif

		m_pDataContainer = 0;
		m_enState = enNone;
	}


private:

	//	��ֹCOPY
	CStream( const CStream & that ) { }
	CStream & operator = ( const CStream & that ) { return *this; }

public:

	inline bool		Write( const void * p , size_t n );
	inline bool		Read( void * p , size_t & n );

public:

	//	�ڽ����͵�д��
	inline bool	Put( const char & val );
	inline bool	Put( const short & val );
	inline bool	Put( const int & val );
	inline bool	Put( const long  long	 & val );

	inline bool	Put( const unsigned char & val );
	inline bool	Put( const unsigned short & val );
	inline bool	Put( const unsigned int & val );
	inline bool	Put( const unsigned long long & val );

	inline bool	Put( const float & val );
	inline bool	Put( const double & val );

	inline bool	Put( const bool & val );

#if defined (STL_STRING_STREAM) && defined(USE_MEMPOOL_STRING)
	inline bool	Put( const std::string & val );
#endif

	inline bool Put( const TString & val );

	inline bool	Put( const unsigned long & val );
	inline bool	Put( const long	& val );

	inline bool	Put( const char * val );


	//	�ڽ����͵Ķ�ȡ
	inline bool	Get( char & val );
	inline bool	Get( short & val );
	inline bool	Get( int & val );
	inline bool	Get( long  long	 & val );

	inline bool	Get( unsigned char & val );
	inline bool	Get( unsigned short & val );
	inline bool	Get( unsigned int & val );
	inline bool	Get( unsigned long long & val );

	inline bool	Get( float & val );
	inline bool	Get( double & val );

	inline bool	Get( bool & val );

#if defined (STL_STRING_STREAM) && defined(USE_MEMPOOL_STRING)
	inline bool	Get( std::string & val );
#endif

	inline bool Get( TString & val );

	inline bool	Get( unsigned long & val );
	inline bool	Get( long & val );

	inline bool	Get( char * val );


	//	д��/��ȡnSize��С����
	inline bool PutRaw( const unsigned char * pData , unsigned int nSize );
	inline bool GetRaw( unsigned char * pData , unsigned int  nSize );

	 //	������ʽ�Ķ�д
	template < typename T>
	inline bool PutArray( const T & objs, unsigned int nSize );

	template < typename T >
	inline	bool GetArray( const T & objs , unsigned int nSize );

	template <typename T>
	CStream & operator >>  (  T & val );
		

	template < typename T>
	CStream & operator <<(  const T & val );

//	template < typename T >
//	bool	Put( const T & obj );
//
//	template < typename T >
//	bool	Get(  T & obj );

	inline bool	PutCount( unsigned int nCount );
	inline bool	GetCount( unsigned int & nCount );

public:
		//	stl���������л�


	template < typename T >
	bool	Put( const std::vector<T> & vec )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlVector ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlVector>" );
		}
#endif

		return PutRange(  vec.begin() , vec.end() , vec.size() );
	}

	template < typename T >
	bool	Get( std::vector<T> & vec )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlVector ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlVector> " );
		}
#endif
		vec.clear();
		return GetRange( std::back_inserter( vec ) , (T*)0 );
	}

	template < typename T >
	bool	Put( const std::list<T> & list )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlList ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlList>" );
		}
#endif

		return PutRange(  list.begin() , list.end() , list.size() );
	}

	template < typename T >
	bool	Get( std::list<T> & list )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlList ) )
		{
			assert( 0 && "Write Error In CStream::Get! <enTagStlList>" );
		}
#endif
		list.clear();
		return GetRange( std::back_inserter( list ) , (T*)0 );
	}

	template < typename T >
	bool	Put( const std::deque<T> & dque )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlDeque ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlDeque>" );
		}
#endif

		return PutRange(  dque.begin() , dque.end() , dque.size() );
	}

	template < typename T >
	bool	Get( std::deque<T> & dque )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlDeque ) )
		{
			assert( 0 && "Write Error In CStream::Get!<enTagStlDeque>" );
		}
#endif
		dque.clear();
		return GetRange( std::back_inserter( dque ) , (T*)0 );
	}


	template <typename FIRST , typename SECOND >
	bool	Put( const std::pair< FIRST , SECOND > & p )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlPair ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlPair>" );
		}
#endif
		return ( this->Put( p.first ) &&  this->Put( p.second ) );
	}

	template <typename FIRST , typename SECOND >
	bool	Get( std::pair< FIRST , SECOND > & p )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlPair ) )
		{
			assert( 0 && "Write Error In CStream::Get!<enTagStlPair>" );
		}
#endif
		return ( this->Get( p.first ) && this->Get( p.second ) );
	}


	template <typename Value , typename Compare>
	bool	Put( const std::set<Value , Compare> & set )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlSet ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlSet>" );
		}
#endif
		return PutRange( set.begin() , set.end() , set.size() );
	}

	template <typename Value , typename Compare>
	bool	Get( std::set<Value , Compare> & set )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlSet ) )
		{
			assert( 0 && "Write Error In CStream::Get!<enTagStlPair>" );
		}
#endif
		set.clear();
		return GetRange( std::inserter( set , set.end() ) , (Value*)0 );
	}


	template <typename Value , typename Compare>
	bool	Put( const std::multiset<Value , Compare> & multiset )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlMultiSet) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlMultiSet>" );
		}
#endif
		return PutRange(  multiset.begin() , multiset.end() , multiset.size() );
	}

	template <typename Value , typename Compare>
	bool	Get( std::multiset<Value , Compare> & multiset )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlMultiSet ) )
		{
			assert( 0 && "Write Error In CStream::Get!<enTagStlMultiSet>" );
		}
#endif
		multiset.clear();
		return GetRange(  std::inserter( multiset , multiset.end() ) , (Value *)0 );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Put( const std::map< Key , Value , Compare > & map )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlMap ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlMap>" );
		}
#endif
		return PutRange( map.begin() , map.end() , map.size() );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Get( std::map< Key , Value , Compare > & map )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !REA_TAGANDCHECK_HELP( enTagStlMap ) )
		{
			assert( 0 && "Write Error In CStream::Get!<enTagStlMap>" );
		}
#endif
		map.clear();
		return GetRange( std::inserter( map , map.end() ) , (std::pair<Key,Value>*)0 );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Put( const std::multimap< Key , Value , Compare > & multimap )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlMultiMap ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlMultiMap>" );
		}
#endif
		return PutRange(  multimap.begin() , multimap.end() , multimap.size() );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Get( std::multimap< Key , Value , Compare > & multimap )
	{
#ifdef TSTREAM_CHECK_TAG
			if( !REA_TAGANDCHECK_HELP( enTagStlMultiMap ) )
			{
				assert( 0 && "Write Error In CStream::Get!<enTagStlMultiMap>" );
			}
#endif
		multimap.clear();
		return GetRange( std::inserter( multimap , multimap.end() ) ,(std::pair<Key,Value>*)0 );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Put( const hash_map< Key , Value , Compare > & map )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlHashMap ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlHashMap>" );
		}
#endif
		return PutRange( map.begin() , map.end() , map.size() );
	}

	template <typename Key , typename Value , typename Compare>
	bool	Get( hash_map< Key , Value , Compare > & map )
	{
#ifdef TSTREAM_CHECK_TAG
			if( !REA_TAGANDCHECK_HELP( enTagStlHashMap ) )
			{
				assert( 0 && "Write Error In CStream::Get!<enTagStlHashMap>" );
			}
#endif
		map.clear();
		return GetRange( std::inserter( map , map.end() ) , (std::pair<Key,Value>*)0 );
	}
	
	template <typename Value , typename Compare>
	bool	Put( const hash_set<Value , Compare> & set )
	{
#ifdef TSTREAM_CHECK_TAG
		if( !WRITE_TAG_HELP( enTagStlMultiSet ) )
		{
			assert( 0 && "Write Error In CStream::Put! <enTagStlMultiSet>" );
		}
#endif
		return PutRange( set.begin() , set.end() , set.size() );
	}

	template <typename Value , typename Compare>
	bool	Get( hash_set<Value , Compare> & set )
	{
#ifdef TSTREAM_CHECK_TAG
			if( !REA_TAGANDCHECK_HELP( enTagStlHashSet ) )
			{
				assert( 0 && "Write Error In CStream::Get!<enTagStlHashSet>" );
			}
#endif
		set.clear();
		return GetRange( std::inserter( set , set.end() ) , (Value*)0 );
	}

protected:

	template < typename Iterator >
	bool	PutRange(  const Iterator & begin , 
		const Iterator & end , size_t nSize  )
	{
		if( !this->PutCount( (unsigned int)nSize ) )
		{
#ifdef _DEBUG
			assert(  "Error In PutRange!" && 0 );
#endif
			return false;
		}

		size_t nCheck = 0;
		Iterator itor = begin;
		while( itor != end )
		{
			if(!this->Put( *itor ))
				break;

			itor++;
			nCheck++;
		}

		if( nCheck != nSize )
		{
#ifdef _DEBUG
			assert( nCheck == nSize );
#endif
			return false;
		}

		return true;
	}

	template < typename Iterator , typename T >
	bool	GetRange(  Iterator itor , T * p )
	{
		unsigned int nSize = 0;

		if( !this->GetCount( nSize ) )
		{
#ifdef _DEBUG
			assert(  "Error In GetRange!" && 0 );
#endif
			return false;
		}

		for( size_t n = 0; n < nSize; n++ )
		{
			T obj;
			if( !this->Get( obj ) )
				return false;
			*itor = obj; 
		}
		return true;
	}


	inline bool WriteBytes( const unsigned char * , unsigned int );
	inline bool ReadBytes( unsigned char * , unsigned int & );

	//	��ֵ��д�����
	template	< typename T >
	inline bool	WRITE_NUM_HELP( const T & val , const enStreamTag & enTag );

	//	��ֵ�Ķ�ȡ����
	template	< typename T >
	inline bool	READ_NUM_HELP( T & val , enStreamTag enTag );


#ifdef TSTREAM_CHECK_TAG

	//	��ȡ���Ҽ���ǩ
	inline bool	REA_TAGANDCHECK_HELP( enStreamTag enTag );

	//	д���ǩ
	inline bool	WRITE_TAG_HELP( enStreamTag tag );
	
#endif

public:
	
	
	CDataContainer * GetContainer() const { return m_pDataContainer; }

	unsigned int UnUsed()
	{
		return m_pDataContainer->UnUsed();
	}

	unsigned int UnRead()
	{
		return m_pDataContainer->UnRead();
	}

private:

	enum enStreamState
	{
		enNone = 0,
		enRead = 1,
		enWrite = 2,
	};

	enStreamState		m_enState;
	CDataContainer	*	m_pDataContainer;
};

class	StreamScopedRead
{
public:
	StreamScopedRead( CDataContainer * p )
	{
		m_Stream.BeginRead( p );
	}

	~StreamScopedRead()
	{
		m_Stream.EndRead();
	}

	CStream &	GetStream() { return m_Stream; }

private	:

	CStream				m_Stream;
};

class	StreamScopedWrite
{
public:
	StreamScopedWrite( CDataContainer * p )
	{
		m_Stream.BeginWrite( p );
	}

	~StreamScopedWrite()
	{
		m_Stream.EndWrite();
	}

	CStream &	GetStream() { return m_Stream; }

private:

	CStream				m_Stream;
};

#include "stream.inl"






TSTREAM_NAMESPACE_END

#endif
