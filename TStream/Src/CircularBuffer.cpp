#include "TStream/CircularBuffer.h"
#include "TStream/Hamaul.h"




TSTREAM_NAMESPACE_BEGIN


CCircularBuffer::CCircularBuffer(size_t nSize ):
	m_nBufSize( 0 ),
	m_nCounter( 0 ),
	m_nReadPtr( 0 ),
	m_nWritePtr( 0 ),
	m_nLength( 0 )
{
	AllocMemory( nSize );
}

CCircularBuffer::~CCircularBuffer()
{
	Clear();
}


bool	CCircularBuffer::operator == ( const CCircularBuffer & that ) const
{
	return m_nBufSize == that.m_nBufSize && 
		m_nLength == that.m_nLength && 
		strcmp( m_pBuf , that.m_pBuf  ) == 0;
}

bool	CCircularBuffer::operator != ( const CCircularBuffer & that ) const
{
	return this->operator ==( that ) == false;
}

size_t  CCircularBuffer::UnUsed() const
{
	assert( m_nLength <= m_nBufSize && "error in [ size_t  CCircularBuffer::UnUsed() const ] !" );
	return m_nBufSize - m_nLength;
}

size_t  CCircularBuffer::UnRead() const
{
	return m_nLength;
}

void	CCircularBuffer::GetData( CDataHamaul & ham )
{
	ham.SetData( GetBufferPtr() , Capacity() );
}

bool	CCircularBuffer::Write(const void * lpBuf , const size_t & nSize )
{
	bool bRet = false; 

	if( !IsFull( nSize ) )		//	没有写满
	{
		m_nCounter += nSize;

		if( nSize + m_nWritePtr > m_nBufSize )
		{
			//	------------		当前剩余BUF写不下
			size_t nLeft = m_nBufSize - m_nWritePtr;
			size_t nPaddSize = nSize - nLeft;
			if( lpBuf )
			{
				memcpy( m_pBuf + m_nWritePtr , lpBuf , nSize );
				memcpy( m_pBuf , ( char *)lpBuf + nLeft , nPaddSize );
			}

			m_nWritePtr = nPaddSize;
			m_nLength += nSize;
		}
		else 
		{
			//	-----------		当前剩余BUF写的下
			if( lpBuf )
			{
				memcpy( m_pBuf + m_nWritePtr , lpBuf , nSize );
				memcpy( m_pBuf + m_nBufSize + m_nWritePtr , lpBuf , nSize );
			}

			m_nWritePtr += nSize;
			if( m_nWritePtr >= m_nBufSize )
				m_nWritePtr -= m_nBufSize;
			m_nLength += nSize;
		}

		bRet = true;

	}

	return bRet;
}

bool	CCircularBuffer::WriteSkip( const size_t & nSize )
{
	return Write( 0, nSize );
}

bool	CCircularBuffer::Read( void * lpBuf , const size_t & nSize )
{
	bool bRet = false;

	if( nSize <= m_nLength )
	{
		if( nSize + m_nReadPtr > m_nBufSize )
		{
			//	-----------------		需要用到扩大的BUF部分

			size_t nLeft = m_nBufSize - m_nReadPtr;
			size_t nPaddSize = nSize - nLeft;
			if( lpBuf )
			{
				memcpy( lpBuf , m_pBuf + m_nReadPtr , nLeft );
				memcpy( ( char *)lpBuf + nLeft , m_pBuf , nPaddSize );
			}

			m_nReadPtr = nPaddSize;
			m_nLength -= nSize;
		}
		else 
		{
			if( lpBuf )
			{
				memcpy( lpBuf , m_pBuf + m_nReadPtr , nSize );
			}

			m_nReadPtr += nSize;
			if( m_nReadPtr >= m_nBufSize )
				m_nReadPtr -= m_nBufSize;

			m_nLength -= nSize;
		}


		if( !m_nLength )
		{
			m_nReadPtr = 0;
			m_nWritePtr = 0;
		}

		bRet = true;
	}

	return bRet;
}

bool	CCircularBuffer::ReadSkip( const size_t & nSize )
{
	return Read( 0 , nSize );
}

void	CCircularBuffer::Clear()
{
	if( m_pBuf )
	{

		TSTREAM_MEM_FREE( m_pBuf , m_nBufSize * 2 );

		m_pBuf = 0;
	}
	
	m_nBufSize = 0;
	m_nCounter = 0;
	m_nWritePtr = 0;
	m_nLength = 0;
	m_nReadPtr = 0;
}

bool	CCircularBuffer::IsEmpty() const
{
	return !m_nLength;
}

void	CCircularBuffer::Peek( void * lpBuf , size_t & nSize )
{
	if( nSize > m_nLength )
		nSize = m_nLength;

	if( nSize + m_nReadPtr > m_nBufSize )
	{
		//	-----------------		需要用到扩大的BUF部分

		size_t nLeft = m_nBufSize - m_nReadPtr;
		size_t nPaddSize = nSize - nLeft;
		if( lpBuf )
		{
			memcpy( lpBuf , m_pBuf + m_nReadPtr , nLeft );
			memcpy( ( char *)lpBuf + nLeft , m_pBuf , nPaddSize );
		}
	}
	else 
	{
		if( lpBuf )
		{
			memcpy( lpBuf , m_pBuf + m_nReadPtr , nSize );
		}
	}
}

void	CCircularBuffer::ResetSize( size_t nSize , bool bDrop )
{
	if( !bDrop && nSize > m_nBufSize && m_pBuf )
	{
		size_t nNew = nSize * 2;
		size_t nOldSize = m_nBufSize * 2;

		char * pTmp =  (char *) ( TSTREAM_MEM_MALLOC( nNew ) );
		memcpy( pTmp , m_pBuf , nOldSize );
		TSTREAM_MEM_FREE( m_pBuf , m_nBufSize * 2 );

		m_pBuf = pTmp;
	}
	else
	{
		Clear();
		Reset();
		AllocMemory( nSize );
	}
}

const char *	CCircularBuffer::GetBufferPtr() const
{
	return m_pBuf;
}

const char *	CCircularBuffer::GetWriteBufferPtr() const
{
	return m_pBuf + m_nWritePtr;	
}

const char *	CCircularBuffer::GetReadBufferPtr() const
{
	return m_pBuf + m_nReadPtr;
}

size_t	CCircularBuffer::Capacity() const
{
	return m_nBufSize;
}

void	CCircularBuffer::Reset()
{
	m_nCounter = 0;
	m_nWritePtr = 0;
	m_nLength = 0;
	m_nReadPtr = 0;
}

bool	CCircularBuffer::IsFull( const size_t nSize )
{
	return  m_nLength + nSize > m_nBufSize;
}

bool	CCircularBuffer::SeekToRead(  size_t nPos ,  void * lpBuf , const size_t & nSize )
{
	return false;
}

bool	CCircularBuffer::SeekToWrite( size_t nPos ,  const void * lpBuf , const size_t & nSize)
{
	return false;
}

bool	CCircularBuffer::ReadSkipTo( size_t n ,  char To )
{
	bool bRet = false;

	size_t i = 0;

	while( i < n )
	{
		char c = 0;
		if( Read(&c,1) )
		{
			if( c == To )
			{
				i += 1;
			}
		}
		else 
			break;
	}

	if ( i == n )
	{
		bRet = true;
	}

	return bRet;
}

size_t	CCircularBuffer::GetWriteCounter(bool bClear /* = false  */)
{	
	size_t nRet = m_nCounter;
	if( bClear )
		m_nCounter = 0;
	return nRet;
}

void	CCircularBuffer::AllocMemory( size_t nSize )
{
	m_pBuf = static_cast<char *>( TSTREAM_MEM_MALLOC( nSize * 2 ) );
	if( m_pBuf )
	{
		memset( m_pBuf , 0 , 2 * nSize );
		m_nBufSize = nSize;
	}
}

TSTREAM_NAMESPACE_END