#include "TStream/RawDataContainer.h"
#include "TStream/Hamaul.h"
#include <algorithm>
#include <assert.h>


TSTREAM_NAMESPACE_BEGIN

CRawDataContainer::CRawDataContainer(size_t nBytes):m_nReadPos( 0 ),m_nWritePos(0)
{
	m_vecBuffer.resize( nBytes );
}

CRawDataContainer::~CRawDataContainer()
{
	m_nReadPos = 0;
	m_nWritePos = 0;
}

bool	CRawDataContainer::operator == ( const CRawDataContainer & that ) const
{
	return m_vecBuffer.size() == that.m_vecBuffer.size() && 
		m_vecBuffer == that.m_vecBuffer;
}

bool	CRawDataContainer::operator != ( const CRawDataContainer & that ) const
{
	return this->operator ==( that ) == false;
}

bool CRawDataContainer::Write(const void * lpBuf , const size_t & nSize )
{

#ifdef _DEBUG
	//	为了速度,r 版不做判断了
	if( !lpBuf || !nSize )
		return false;
#endif

	if( IsFull( nSize ) )
		return false;
	
	memcpy( &m_vecBuffer[m_nWritePos] , lpBuf , nSize );
	m_nWritePos += nSize;
	
	return true;
}

bool CRawDataContainer::WriteSkip( const size_t & nSize )
{
	if( IsFull( nSize ) )
		return false;

	m_nWritePos += nSize;

	return true;
}

bool CRawDataContainer::Read( void * lpBuf , const size_t & nSize )
{

#ifdef _DEBUG
	//	为了速度,r 版不做判断了
	if( !lpBuf || !nSize )
		return false;
#endif

	if( UnRead() < nSize )
	{
		return false;
	}

	memcpy( lpBuf , &m_vecBuffer[m_nReadPos] , nSize );
	m_nReadPos += nSize;
	return true;
}

bool	CRawDataContainer::ReadSkip( const size_t & nSize )
{
	if( UnRead() < nSize )
		return false;

	m_nReadPos += nSize;
	return true;
}

size_t CRawDataContainer::UnUsed() const
{
	assert( m_vecBuffer.size() >= m_nWritePos );
	return m_vecBuffer.size() - m_nWritePos;
}

size_t  CRawDataContainer::UnRead() const
{
	assert( m_nWritePos >= m_nReadPos );
	return m_nWritePos - m_nReadPos;
}

void CRawDataContainer::Peek( void * lpBuf , size_t & nSize )
{
	assert( lpBuf && nSize );
	assert( m_nWritePos >= m_nReadPos );

	if( m_nWritePos - m_nReadPos < nSize )
	{
		nSize = m_nWritePos - m_nReadPos;
	}

	if( !nSize )
		return ;

	memcpy( lpBuf , &m_vecBuffer[m_nReadPos] , nSize );
}


bool CRawDataContainer::SeekToRead(  size_t nPos , void * lpBuf , const size_t & nSize )
{

#ifdef _DEBUG
	if( !lpBuf || !nSize  )
		return false;
#endif

	if(  nPos + nSize > m_vecBuffer.size() )
		return false;

	memcpy( lpBuf , &m_vecBuffer[nPos] , nSize );
	
	return true;
}

bool CRawDataContainer::SeekToWrite(  size_t nPos , const void * lpBuf , const size_t & nSize)
{
#ifdef _DEBUG
	if( !lpBuf || !nSize )
		return false;
#endif

	if( nPos + nSize > m_vecBuffer.size() )
		return false;

	memcpy( &m_vecBuffer[nPos] , lpBuf , nSize );

	return true;
}

bool CRawDataContainer::ReadSkipTo( size_t n ,  char To )
{
	bool bRet = false;
	
	size_t i = 0;
	size_t nPos = m_nReadPos;
	size_t nSize = Capacity();

	while( i < n && nPos < nSize )
	{
		const char & value = m_vecBuffer[nPos];
		if( value == To )
		{
			i += 1;

			if( i == n )
			{
				bRet = true;
				m_nReadPos = nPos;
			}
		}
		nPos += 1;
	}

	return bRet;
}

size_t	CRawDataContainer::Capacity() const
{
	return  m_vecBuffer.size();
}

const char *	CRawDataContainer::GetBufferPtr() const
{
	return &m_vecBuffer[0];
}

const char *	CRawDataContainer::GetWriteBufferPtr() const
{
	return &m_vecBuffer[m_nWritePos];
}

const char *	CRawDataContainer::GetReadBufferPtr() const
{
	return &m_vecBuffer[m_nReadPos];
}

bool CRawDataContainer::IsFull(const size_t nSize )
{
	if( m_nWritePos + nSize  > m_vecBuffer.size() )
		return true;
	return false;
}

void CRawDataContainer::Reset()
{
	m_nWritePos = 0;
	m_nReadPos = 0;
}

void CRawDataContainer::ResetSize( size_t nSize , bool bDrop )
{
	if( !bDrop && nSize > m_vecBuffer.size() )
	{
		m_vecBuffer.resize( nSize );
	}
	else 
	{
		Reset();
		m_vecBuffer.resize( nSize );
	}
}

void CRawDataContainer::Clear()
{
	m_nReadPos = 0;
	m_nWritePos = 0;
	m_vecBuffer.clear();
}

bool CRawDataContainer::IsEmpty() const
{
	return !m_nWritePos;
}

void CRawDataContainer::GetData( CDataHamaul & ham )
{
	ham.SetData( GetBufferPtr() , Capacity() );
}



TSTREAM_NAMESPACE_END