/***********************************************************************
	created	:	2008/09/19
	created	:	19:9:2008   13:53
	filename: 	streamtag
	author	:	tt
	module	:	��ϵͳ(���л�ģ��)
	purpose	:
		���ı�ǩ����Ҫ����������
	
	log		:
	
	
	Copyright (c) ����
	
***********************************************************************/
	
#ifndef STREAM_TAG_H_
#define STREAM_TAG_H_

#ifdef _MSC_VER
#pragma once 
#endif



//	����ǩ����
enum	enStreamTag
{
	enTagMin = 0,

	//	�ڽ����Ͷ���
	enTagInt8,
	enTagInt16,
	enTagInt32,
	enTagInt64,
	enTagUint8,
	enTagUint16,
	enTagUint32,
	enTagUint64,
	enTagFloat32,
	enTagFloat64,
	enTagULong,
	enTagLong,
	enTagBool,
	enTagString,
	enTagCStyleString,
	enTagRaw,
	enTagArray,
	enTagStlPair,
	enTagStlVector,
	enTagStlDeque,
	enTagStlQuque,
	enTagStlMap,
	enTagStlList,
	enTagStlSet,
	enTagStlHashMap,
	enTagStlHashSet,
	enTagStlMultiSet,
	enTagStlMultiMap,
	enTagStreamMap,
	enTagRawDataCon,
	enTagCircularBuffer,
	enTagDynamicRawDataCon,
	eTagUserClass,
	enTagUserClass_1, 
	enTagUserClass_2,
	enTagUserClass_3,
	enTagMax = 255,
};


#endif



