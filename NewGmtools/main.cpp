#include <iostream>
#include "DBStruct.h"
#include <sys/types.h>
#include <dirent.h>
#include <fstream>
#include <string>
#include <map>
#include "DBClient.h"
#include "Register.h"

using namespace std;

string _url = "modidb:://192.168.2.208@3306&hrx@123456&logdb";	
void	parse_file(const char * path)
{
	ifstream  _if;
	_if.open(path);
	string  lines;
	if( _if.is_open())
	{
		MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
		while( getline(_if,lines))
		{
			string _tmp_action;
			string _tmp_rolename;

			string _tmp_time;
			string _unique;
			string _str_sql;

			size_t ipos=0;
			size_t _tmp_pos=0;

			/// 错误的日志格式
			if( (ipos = lines.find_first_of("[",0) ) == string::npos)
			{
				continue;
			}
			if( (_tmp_pos = lines.find_first_of("]",ipos) ) == string::npos)
			{
				continue;
			}

			_tmp_action = lines.substr(ipos+1,_tmp_pos-ipos-1);

			/// 没有注册
			if( !MODI_RegisterManager::GetInstance()->IsInteresting(_tmp_action))
			{
				continue;
			}
			
			_unique = MODI_RegisterManager::GetInstance()->GetUnique(_tmp_action);

			/// 不是想要的信息
			if( lines.find(_unique.c_str()) == string::npos)
			{
				continue;
			}

			_tmp_time += "20";
			_tmp_time += lines.substr(0,2);
			_tmp_time += "-";
			_tmp_time += lines.substr(2,2);
			_tmp_time += "-";
			_tmp_time += lines.substr(4,2);
			_tmp_time += " ";
			_tmp_time += lines.substr(7,2);
			_tmp_time += ":";
			_tmp_time += lines.substr(9,2);
			_tmp_time += ":";
			_tmp_time += lines.substr(11,2);


			std::map<string,string> param;
			std::map<string,string>::iterator itor; 
			ipos = lines.find_first_of("<",0);	
			if( ipos == string::npos)
			{
				continue;
			}
			_tmp_pos=  lines.find_first_of("=",ipos);

			while( _tmp_pos != string::npos)
			{
				std::string _tmp_param = lines.substr(ipos+1,_tmp_pos-1-ipos);
				std::string _tmp_value;

				ipos = lines.find_first_of(",",_tmp_pos);
				if( ipos == string::npos)
				{
					ipos = lines.find_first_of(">",0);
				}
				_tmp_value = lines.substr(_tmp_pos+1,ipos-1-_tmp_pos);

				param.insert(pair<string,string>(_tmp_param,_tmp_value));

				_tmp_pos = lines.find_first_of("=",ipos+1);
			}	

			if( param.empty())
			{
				continue;
			}
			
			/*			GenSql(param,_tmp_action,_str_sql,_tmp_time);
			
			std::ostringstream os;
			os<<"use  ";
			os<<g_db.c_str()<<";";
			p_dbclient->ExecSql( os.str().c_str(),os.str().size());
			Global::logger->debug("[exec_sql] <sql=%s>", _str_sql.c_str());
			p_dbclient->ExecSql( _str_sql.c_str(),_str_sql.size());
			*/

			/// 有数据，插入到数据库中
			MODI_TableStruct * p_table = MODI_DBManager::GetInstance().GetTable(_tmp_action.c_str());
			if(!p_table)
			{
				Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", _tmp_action.c_str());
				continue;
			}

			MODI_Record insert_record;
			insert_record.Put("times", _tmp_time);
			std::map<string,string>::iterator  iter = param.begin();
			for(; iter != param.end(); iter++)
			{
				insert_record.Put(iter->first, iter->second);
			}

			MODI_RecordContainer insert_container;
			insert_container.Put(&insert_record);
			p_dbclient->ExecInsert(p_table, &insert_container);
			
		}
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	}
	else
	{
		cout<<"can't open the file name="<<path<<endl;
	}

}

int main(int argc,char * argv[])
{
	Global::logger = new MODI_Logger("testdb");
	char * path=NULL;
	struct dirent *dirp=NULL;
	DIR	*dp=NULL;
	string 	_spath;

	if( argc < 3)
	{
		Global::logger->debug("NewGmtools path url");
		return 1;
	}
	
	_url=argv[2];
	path = argv[1];
	_spath = path;
	_spath += "/";

	if(! MODI_DBManager::GetInstance().Init(_url.c_str(),0))
	{
		Global::logger->debug("[system_init] dbmanager init failed <url=%s>", _url.c_str());
		return 0;
	}

	MODI_RegisterManager::GetInstance()->Init();
	
	if( (dp=opendir(path))==NULL)
	{
		Global::logger->debug("[system_init] open dir failed <path=%s>", path);
		return 1;
	}	

	/// 分析文件
	while( (dirp = readdir(dp)) != NULL)
	{

		if( strcmp(dirp->d_name,".") == 0 || strcmp(dirp->d_name,"..") == 0)
		{
			continue;
		}
		
		string _tmp_path = _spath;
		_tmp_path += dirp->d_name;

		parse_file(_tmp_path.c_str());
		std::string rm_dir = "rm -f " + _tmp_path;
		system(rm_dir.c_str());
	}	
}
