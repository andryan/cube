/**
 * @file   Register.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Nov 29 15:43:21 2011
 * 
 * @brief  日志变成数据库的配置文件
 * 
 * 
 */

#include "Global.h"
#include "DBStruct.h"
#include "Register.h"


MODI_RegisterManager * MODI_RegisterManager::m_pInstance = NULL;
std::string MODI_RegisterManager::ms_EmptyString;


MODI_RegisterManager::MODI_RegisterManager()
{
}

MODI_RegisterManager::~MODI_RegisterManager()
{
}

MODI_RegisterManager *  MODI_RegisterManager::GetInstance()
{
	if( !m_pInstance)
	{
		m_pInstance = new MODI_RegisterManager();
	}

	return m_pInstance;
}


bool MODI_RegisterManager::IsInteresting(std::string & strings)
{

	const_list_iterator iter = m_stList.find(strings.c_str());
	if( iter != m_stList.end())
	{
		return true;
	}
	return false;
}



void 	MODI_RegisterManager::AddInteresting(std::string & strings,std::string & string_unique)
{
	const_list_iterator iter = m_stList.find( strings.c_str());
	if( iter == m_stList.end())
	{
		m_stList[strings.c_str()] = string_unique.c_str();
	}

}


void	MODI_RegisterManager::Init()
{
	MODI_XMLParser  parse;
	if (!parse.InitFile("./conf.xml"))
	{
		std::cout<<"Can't Initial the conf.xml"<<std::endl;		
		return;
	}

	xmlNodePtr __root=parse.GetRootNode("conf");
	xmlNodePtr __interest = parse.GetChildNode(__root,"interesting");
       while(__interest)
       {
		std::string _name;
		std::string _unique;
		if(! parse.GetNodeStr(__interest,"name",_name))
		{
			std::cout<<"Can't get the attribute name 's value"<<std::endl;
			return;
		}
		if( !parse.GetNodeStr(__interest,"unique",_unique))
		{
			std::cout<<"Can't get the attribute unique 's value"<<std::endl;
			return;
		}
		/// 查询数据库是否有表
		if(! MODI_DBManager::GetInstance().GetTable(_name.c_str()))
		{
			Global::logger->fatal("[get_table_struct] Unable get table struct <name=%s>", _name.c_str());
			continue;
		}
	
		AddInteresting(_name,_unique);
		__interest=parse.GetNextNode(__interest,NULL);
       }	       
}	


const std::string & MODI_RegisterManager::GetUnique(std::string & strings)
{
	const_list_iterator iter = m_stList.find(strings.c_str());
	if( iter != m_stList.end())
	{
		return iter->second;
	}
	return  ms_EmptyString;
}







