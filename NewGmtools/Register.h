/**
 * @file   Register.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Nov 29 15:40:53 2011
 * 
 * @brief  日志变成数据库配置
 * 
 * 
 */

#include <iostream>
#include <string>
#include <map>
#include "XMLParser.h"

/**
 * @brief 日志变成数据库注册
 * 
 */
class MODI_RegisterManager
{
 public:
	static MODI_RegisterManager * GetInstance();
	bool	IsInteresting(std::string & strings);
	void 	Init();
	const std::string & GetUnique(std::string & strings);


private:	
	void	AddInteresting(std::string & strings, std::string & string_unique);
	MODI_RegisterManager();
	~MODI_RegisterManager();

	static MODI_RegisterManager * m_pInstance;	

	std::string	m_sString;
		
	typedef std::map<std::string, std::string > LIST;
	typedef  LIST::iterator list_iterator;
	typedef  LIST::const_iterator const_list_iterator;

	LIST	m_stList;
	static  std::string ms_EmptyString;
};






