#include "GameSvrClientTask.h"
#include "gw2gs.h"
#include "protocol/c2gs.h"
#include "Share.h"
#include "MSServerClient.h"
#include "PackageHandler_s2sm.h"
#include "ManangerSvrLogic.h"
#include "Channellist.h"
#include "AssertEx.h"


MODI_SvrPackageProcessBase 	 MODI_GameSvrClientTask_Ms::ms_ProcessBase;

// 连接上的回调,可以安全的在逻辑线程调用
void MODI_GameSvrClientTask_Ms::OnConnection()
{
}

// 断开的回调，可以安全的在逻辑线程调用
void MODI_GameSvrClientTask_Ms::OnDisconnection()
{
	if( m_pClient )
	{
		MODI_SvrChannellist::GetInstancePtr()->Del( m_pClient->GetServerID() );
	}
}

void 	MODI_GameSvrClientTask_Ms::ProcessAllPackage()
{
	ms_ProcessBase.ProcessAllPackage();
}

MODI_GameSvrClientTask_Ms::MODI_GameSvrClientTask_Ms( const int sock, const struct sockaddr_in * addr):
	MODI_IServerClientTask( sock , addr ),
	m_pClient(0)
{
	SetEnableCheck();
	Resize(SVR_CMDSIZE);	
}

MODI_GameSvrClientTask_Ms::~MODI_GameSvrClientTask_Ms()
{
	delete m_pClient;
}

void MODI_GameSvrClientTask_Ms::SetClient( MODI_MSServerClient * p )
{
	MODI_ASSERT( m_pClient == 0 );
	m_pClient = p;
}

bool MODI_GameSvrClientTask_Ms::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if( !pt_null_cmd || cmd_size < (int)(sizeof(Cmd::stNullCmd)) )
		return false;

	this->Put( pt_null_cmd , cmd_size );
	return true;
}


bool MODI_GameSvrClientTask_Ms::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	if( !m_pClient )
		return false;

	///	游戏命令的处理
	int iRet = MODI_PackageHandler_s2sm::GetInstancePtr()->DoHandlePackage(
					ptNullCmd->byCmd, ptNullCmd->byParam, m_pClient , ptNullCmd,
					dwCmdLen);

	if( iRet == enPHandler_Ban )
	{

	}
	else  if ( iRet == enPHandler_Kick )
	{

	}
	else if( iRet == enPHandler_Warning )
	{

	}

	return true;
}

int MODI_GameSvrClientTask_Ms::RecycleConn()
{
	if( this->IsCanDel() )
		return 1;
	return 0;
}

void MODI_GameSvrClientTask_Ms::DisConnect()
{
	Global::logger->info("[%s] remote client<ip=%s,port=%u> disconnectioned. " , ERROR_CON ,
			this->GetIP() , this->GetPort() );
}

