/**
 * @file   UnifyClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:15:17 2011
 * 
 * @brief  平台验证连接
 * 
 * 
 */

#include "AssertEx.h"
#include "s2unify_cmd.h"
#include "UnifyClient.h"
#include "MSServerClient.h"

MODI_UnifyClient * MODI_UnifyClient::m_pInstance = NULL;
 
/** 
 * @brief 初始化
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_UnifyClient::Init()
{
	if(! MODI_RClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 释放
 * 
 */
void MODI_UnifyClient::ClientFinal()
{
	Terminate(true);
	TTerminate();
	Join();
}


/** 
 * @brief 发送命令
 * 
 * @param pt_null_cmd 要发送的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_UnifyClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}
	return MODI_RClientTask::SendCmdInConnect(pt_null_cmd, cmd_size);
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 
 */
bool MODI_UnifyClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd))))
    {
        return false;
    }
	this->Put( pt_null_cmd , cmd_size );
    return true;
}

/** 
 * @brief 队列内的命令
 * 
 * @param pt_null_cmd 获取的命令
 * @param dwCmdLen 命令长度
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_UnifyClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	if(pt_null_cmd == NULL || dwCmdLen < 2)
	{
		return false;
	}

#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] unifyclient recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, dwCmdLen);
#endif

	if(pt_null_cmd->byCmd == MAINCMD_S2UNIFY)
	{
		//if(pt_null_cmd->byParam == MODI_Unify2S_ChenMiResult_Cmd::ms_SubCmd)
		/// 所有的都发送到zoneservice去
		{
			MODI_MSServerClient * pZoneServer = MODI_MSServerClient::GetZoneServerClient();
			if( pZoneServer )
			{
				pZoneServer->SendPackage( pt_null_cmd , dwCmdLen);		
				Global::logger->warn("[%s] send a cmd to zoneservie <cmd=%d,para=%d>." , ERROR_CON, pt_null_cmd->byCmd, pt_null_cmd->byParam);
			}
			else
			{
				Global::logger->warn("[%s] can't direction package to Zone server . disconnection with Zone server." , 
						ERROR_CON );
			}
		}
	}
	
	return true;	
}


const bool MODI_UnifyClient::IsConnected()
{
	bool is_connect = false;
	is_connect = MODI_RClientTask::IsConnected();
	return is_connect;
}
