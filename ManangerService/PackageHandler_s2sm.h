/** 
 * @file PackageHandler_s2sm.h
 * @brief 游戏服务器和mananger server 之间数据包的处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-11
 */
#ifndef PACKAGE_HANDLER_H_S2SM_
#define PACKAGE_HANDLER_H_S2SM_

#include "IPackageHandler.h"
#include "SingleObject.h"


class MODI_PackageHandler_s2sm : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_s2sm>
{
public:

    MODI_PackageHandler_s2sm();
    virtual ~MODI_PackageHandler_s2sm();

public:

	/// 游戏服务器注册自己的处理
	static int ServerRequestReg_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 转发给RDB的处理
	static int DirectionToRoleDB_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 转发给LS的处理
	static int DirectionToLoginServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 转发给ADB的处理
	static int DirectionToAccountServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 转发给zoneserver
	static int DirectionToZoneServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 转发给zoneserver
	static int DirectionToUnifyServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

private:

    virtual bool FillPackageHandlerTable();
};



#endif

