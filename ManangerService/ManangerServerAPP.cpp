#include "ManangerServerAPP.h"
#include "UnifyClient.h"


MODI_ManangerServerAPP::MODI_ManangerServerAPP( const char * szAppName ):MODI_IAppFrame( szAppName ),
	m_pAccountDBClient(0)
{


}


MODI_ManangerServerAPP::~MODI_ManangerServerAPP()
{


}

int MODI_ManangerServerAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	// 获取相关配置信息
	const MODI_SvrMSConfig * pManangerSvrInfo = (const MODI_SvrMSConfig * )(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_MANGGERSERVER ) );
	if( !pManangerSvrInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	const MODI_SvrADBConfig * pADBInfo = (const MODI_SvrADBConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ACCOUNTSERVER ) );
	if( !pADBInfo )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

    Global::logger->AddLocalFileLog(pManangerSvrInfo->strLogPath);
	std::string net_log_path = pManangerSvrInfo->strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();

	if( !m_PackageHalder_s2sm.Initialization() )
	{
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}

	// 创建网络服务对象
	std::vector<WORD > port_vec;
	std::vector<std::string> ip_vec;
	port_vec.push_back( pManangerSvrInfo->nPort );
	ip_vec.push_back( pManangerSvrInfo->strIP );

	m_pNetService = new MODI_ManangerService( port_vec , ip_vec );
	if( !m_pNetService )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	/// init unifyclient
	if(! MODI_UnifyClient::GetInstance().Init())
	{
		Global::logger->fatal("[%s] Unable init unify(ip=%s,port=%d)", SYS_INIT, Global::g_strUnifyIP.c_str(), Global::g_wdUnifyPort);
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	// 创建连接ACCOUNT DB SERVER 的客户端对象
	unsigned short nAccountDBPort = pADBInfo->nPort;
	m_pAccountDBClient = new MODI_ADBClient_ManangerSvr("AccountDB_Client", pADBInfo->strIP.c_str() , nAccountDBPort );
	if( !m_pAccountDBClient )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_ManangerSvrLogic();
	if( !m_pLogicThread )
	{
		return MODI_IAppFrame::enNoMemory;
	}

	
	return MODI_IAppFrame::enOK;
}


int MODI_ManangerServerAPP::Run()
{

	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;

	if( m_pAccountDBClient )
	{
		Global::logger->info("[%s]try connect to account db server." , SYS_INIT );
		if( !m_pAccountDBClient->Init() )
		{
			Global::logger->fatal("[%s]connect to account db server faild. " , SYS_INIT );
			return MODI_IAppFrame::enCannotConnectToServer;
		}
		else 
		{
			m_pAccountDBClient->Start();
			Global::logger->info("[%s]connect to account db server successful. " , SYS_INIT );
		}
	}
	else 
	{
		Global::logger->fatal("[%s]connect to account db server faild. not exist account db client. " , SYS_INIT );
		return MODI_IAppFrame::enCannotConnectToServer;
	}

	sleep(2);

	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );

	if( m_pLogicThread )
	{
		// 启动逻辑线程
		m_pLogicThread->Start();
	}
	else 
	{
		Global::logger->fatal("[%s] start mananger server faild. logic thread not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistLogicThread;
	}


	if( m_pNetService )
	{
		// start server for gameclients ..
		m_pNetService->Main();
	}
	else 
	{
		Global::logger->fatal("[%s] start mananger server faild. net service moudle not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistNetService;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_ManangerServerAPP::Shutdown()
{
	if( m_pNetService )
	{
		m_pNetService->Terminate();
	}

	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

	if( m_pAccountDBClient )
	{
		m_pAccountDBClient->TTerminate();
		m_pAccountDBClient->Join();
	}

	delete m_pLogicThread;
	m_pLogicThread = 0;

	delete m_pNetService;
	m_pNetService =0;

	delete m_pAccountDBClient;
	m_pAccountDBClient = 0;

	
	m_Channellist.Clear( true );

	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}


