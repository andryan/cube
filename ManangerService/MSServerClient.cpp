#include "MSServerClient.h"
#include "ServiceTask.h"


MODI_MSServerClient * 	MODI_MSServerClient::ms_pLoginServer = 0;
MODI_MSServerClient * 	MODI_MSServerClient::ms_pZoneServer = 0;
MODI_MSServerClient * 	MODI_MSServerClient::ms_pDBProxyServer = 0;

std::vector<MODI_MSServerClient *> 	MODI_MSServerClient::ms_listGameServers;

void 	MODI_MSServerClient::SetLoginServerClient( MODI_MSServerClient * pSet )
{
	ms_pLoginServer = pSet;
}

MODI_MSServerClient * 	MODI_MSServerClient::GetLoginServerClient()
{
	return ms_pLoginServer;
}

void 	MODI_MSServerClient::SetZoneServerClient( MODI_MSServerClient * pSet )
{
	ms_pZoneServer = pSet;
}

MODI_MSServerClient * 	MODI_MSServerClient::GetZoneServerClient()
{
	return ms_pZoneServer;
}

void 	MODI_MSServerClient::SetDBProxyClient( MODI_MSServerClient * pSet )
{
	ms_pDBProxyServer = pSet;
}

MODI_MSServerClient * 	MODI_MSServerClient::GetDBProxyClient()
{
	return ms_pDBProxyServer;
}

//bool 	MODI_MSServerClient::AddToGameServerslist( MODI_MSServerClient * p )
//{
//	if( p->IsGameServer() == false )
//		return false;
//
//	for( size_t n = 0; n < ms_listGameServers.size(); n++ )
//	{
//		MODI_MSServerClient * pTemp = ms_listGameServers[n];
//		if( pTemp->GetServerID() == p->GetServerID() )
//			return false;
//	}
//
//	ms_listGameServers.push_back( p );
//	return true;
//}
//
//bool 	MODI_MSServerClient::DelFromGameServerlist( MODI_MSServerClient * p )
//{
//	if( p->IsGameServer() == false )
//		return false;
//
//	std::vector<MODI_MSServerClient *>::iterator it = 	ms_listGameServers.begin();
//	while( it != ms_listGameServers.end() )
//	{
//		MODI_MSServerClient * pTemp = *it;
//		if( pTemp->GetServerID() == p->GetServerID() )
//		{
//			ms_listGameServers.erase( it );
//			return true;
//		}
//		it++;
//	}
//
//	return false;
//}
//
//MODI_MSServerClient * MODI_MSServerClient::FindFromGameServerlist( unsigned int nChannelID )
//{
//	for( size_t n = 0; n < ms_listGameServers.size(); n++ )
//	{
//		MODI_MSServerClient * pTemp = ms_listGameServers[n];
//		if( pTemp->GetServerID() == nChannelID )
//			return pTemp;
//	}
//
//	return 0;
//}
//
//void 	MODI_MSServerClient::Broadcast( const void * pCmd , size_t nCmdSize  )
//{
//	BroadcastToGameServer( pCmd , nCmdSize );
//
//	if( GetLoginServerClient() )
//	{
//		GetLoginServerClient()->SendPackage( pCmd , nCmdSize );
//	}
//}
//
//void 	MODI_MSServerClient::BroadcastToGameServer( const void * pCmd , size_t nCmdSize )
//{
//	for( size_t n = 0; n < ms_listGameServers.size(); n++ )
//	{
//		MODI_MSServerClient * pTemp = ms_listGameServers[n];
//		pTemp->SendPackage( pCmd , nCmdSize );
//	}
//}
//
MODI_MSServerClient::MODI_MSServerClient( void * p ):m_pNetIO(p) , 
	m_iSvrType(SVR_TYPE_NULL),m_pDisconnectionCallback(0),m_nServerID(0)
{

}

MODI_MSServerClient::~MODI_MSServerClient()
{
	if( GetLoginServerClient() == this )
		SetLoginServerClient( 0 );

	else if( GetZoneServerClient() == this )
		SetZoneServerClient( 0 );

	else if( GetDBProxyClient() == this )
		SetDBProxyClient( 0 );

//	DelFromGameServerlist(this);

	delete m_pDisconnectionCallback;
	ResetNetIO();
}

bool 	MODI_MSServerClient::SendPackage( const void * pCmd , size_t nCmdSize )
{
	MODI_RecurisveMutexScope lock( &m_mutexNetIO );
	MODI_ServiceTask * pTask = (MODI_ServiceTask *)(m_pNetIO);
	if( pTask )
	{
		return	pTask->SendCmd( (const Cmd::stNullCmd *)(pCmd) , nCmdSize );
	}
	
	return false;
}

void 	MODI_MSServerClient::ResetNetIO()
{
	MODI_RecurisveMutexScope lock( &m_mutexNetIO );
	m_pNetIO = 0;
}

void 	MODI_MSServerClient::SetDisconnectionCallback( MODI_SVRCLIENT_CALLBACK pFn )
{
	delete m_pDisconnectionCallback;
	m_pDisconnectionCallback = new MODI_SVRCLIENT_CALLBACK( pFn );
}


void 	MODI_MSServerClient::Disconnection()
{
	MODI_RecurisveMutexScope lock( &m_mutexNetIO );
	MODI_ServiceTask * pTask = (MODI_ServiceTask *)(m_pNetIO);
	if( pTask )
	{
		pTask->Terminate();
		ResetNetIO();
	}
}








