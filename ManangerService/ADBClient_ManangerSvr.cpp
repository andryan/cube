#include "ADBClient_ManangerSvr.h"
#include "PackageHandler_s2sm.h"


MODI_ADBClient_ManangerSvr::MODI_ADBClient_ManangerSvr(const char * name, const char * server_ip,const WORD & port)
	:MODI_ClientTask(name, server_ip, port)
{
	Resize(SVR_CMDSIZE);
}

MODI_ADBClient_ManangerSvr::~MODI_ADBClient_ManangerSvr()
{

}

/**
 * @brief 初始化
 *
 */
bool MODI_ADBClient_ManangerSvr::Init()
{
    if (!MODI_ClientTask::Init())
    {
        return false;
    }
    return true;
}

/**
 * @brief 结束,临时方案
 *
 */
void MODI_ADBClient_ManangerSvr::Final()
{
	MODI_ClientTask::Final();
}

bool MODI_ADBClient_ManangerSvr::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
    if ((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd) )) )
    {
        return false;
    }

    // 放到消息包处理队列
	this->Put( pt_null_cmd , cmd_size );

    return true;
}

bool MODI_ADBClient_ManangerSvr::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}

	if (m_pSocket)
	{
		bool ret_code = true;
		ret_code = m_pSocket->SendCmd(pt_null_cmd, cmd_size);
		return ret_code;
	}

	return false;
}

bool MODI_ADBClient_ManangerSvr::CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen)
{
	// 数据包处理
	if( MODI_ADBClient_ManangerSvr::GetInstancePtr() )
	{
            ///	游戏命令的处理
		int iRet = MODI_PackageHandler_s2sm::GetInstancePtr()->DoHandlePackage(
						ptNullCmd->byCmd, ptNullCmd->byParam, MODI_ADBClient_ManangerSvr::GetInstancePtr() , ptNullCmd,
						dwCmdLen);

		if( iRet == enPHandler_Ban )
		{

		}
		else  if ( iRet == enPHandler_Kick )
		{

		}
		else if( iRet == enPHandler_Warning )
		{

		}

		return true;
	}

	return true;
}

void MODI_ADBClient_ManangerSvr::ProcessPackages()
{
	const unsigned int nMaxGetCmdADB = 10000;
	MODI_CmdParse::Get(nMaxGetCmdADB );
}
