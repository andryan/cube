/** 
 * @file MSServerClient.h
 * @brief 管理服务器上，来自其他服务器的客户端对象
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-12
 */

#ifndef MANANGERSERVER_CLIENT_H_
#define MANANGERSERVER_CLIENT_H_

#include "Type.h"
#include "RecurisveMutex.h"
#include <set>
#include <vector>
#include "functionPtr.h"

typedef CFreeFunctionPtr<void *,int> 	MODI_SVRCLIENT_CALLBACK;

/** 
 * @brief mananger server 上的客户端对象（代表某个服务器)
 */
class 	MODI_MSServerClient 
{
	public:

		explicit MODI_MSServerClient( void * p );
		~MODI_MSServerClient();


	public:

		/** 
		 * @brief 发送数据包
		 * 
		 * @param pCmd 命令
		 * @param nCmdSize 命令大小
		 * 
		 * @return 	成功返回TRUE
		 */
		bool 	SendPackage( const void * pCmd , size_t nCmdSize );

		/** 
		 * @brief 是否注册过服务器类型
		 * 
		 * @return 如果是，则返回TRUE	
		 */
		bool 	IsRegisterType() const
		{
			return m_iSvrType != SVR_TYPE_NULL;
		}

		/** 
		 * @brief 该客户端是否 GameServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsGameServer() const
		{
			return m_iSvrType == SVR_TYPE_GS;
		}

		/** 
		 * @brief 该客户端是否 LoginServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsLoginServer() const
		{
			return m_iSvrType == SVR_TYPE_LS;
		}

		/** 
		 * @brief 该客户端是否 ZoneServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsZoneServer() const
		{
			return m_iSvrType == SVR_TYPE_ZS;
		}

		bool 	IsDBProxy() const
		{
			return m_iSvrType == SVR_TYPE_DB;
		}

		/** 
		 * @brief 设置服务器类型
		 * 
		 * @param iType 服务器具体的类型
		 */
		void 	SetServerType( int iType )
		{
			m_iSvrType = iType;
		}

		void 	SetServerID( unsigned int n )
		{
			m_nServerID = n;
		}

		unsigned int GetServerID() const
		{
			return m_nServerID;
		}

		void 	Disconnection();

		/** 
		 * @brief 重置网络接口
		 */
		void 	ResetNetIO();

		void 	SetDisconnectionCallback( MODI_SVRCLIENT_CALLBACK pFn );

		static void 	SetLoginServerClient( MODI_MSServerClient * pSet );

		static MODI_MSServerClient * 	GetLoginServerClient();

		static void 	SetZoneServerClient( MODI_MSServerClient * pSet );

		static MODI_MSServerClient * 	GetZoneServerClient();

		static void 	SetDBProxyClient( MODI_MSServerClient * pSet );

		static MODI_MSServerClient * 	GetDBProxyClient();
//		static bool 	AddToGameServerslist( MODI_MSServerClient * p );
//		static bool 	DelFromGameServerlist( MODI_MSServerClient * p );
//		static MODI_MSServerClient * FindFromGameServerlist( unsigned int nChannelID );
//		static void 	BroadcastToGameServer( const void * pCmd , size_t nCmdSize  ); 
//		static void 	Broadcast( const void * pCmd , size_t nCmdSize  ); 

	private:

		/// 网络接口锁
		MODI_RecurisveMutex 	m_mutexNetIO;

		/// 网络接口
		void * 	m_pNetIO;

		/// 该客户端是哪种服务器类型
		int 	m_iSvrType; 

		/// 某个服务器断开的回调对象
		MODI_SVRCLIENT_CALLBACK 	* m_pDisconnectionCallback; 

		unsigned int 	m_nServerID;

		static 	MODI_MSServerClient * 	ms_pLoginServer;

		static 	MODI_MSServerClient * 	ms_pZoneServer;

		static 	MODI_MSServerClient * 	ms_pDBProxyServer;

		static  std::vector<MODI_MSServerClient *> 	ms_listGameServers;

};





#endif
