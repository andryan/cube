#include "ManangerSvrLogic.h"
#include "ManangerService.h"
#include "ADBClient_ManangerSvr.h"
#include "GameSvrClientTask.h"
#include "AssertEx.h"
#include "MSServerClient.h"
#include "ManangerService.h"
#include "Base/FPSControl.h"
#include "UnifyClient.h"

MODI_ManangerSvrLogic::MODI_ManangerSvrLogic()
{

}


MODI_ManangerSvrLogic::~MODI_ManangerSvrLogic()
{

}

/**
 * @brief 主循环
 *
 */
void MODI_ManangerSvrLogic::Run()
{
	MODI_FPSControl fps;
	fps.Initial( 30 );
    while (!IsTTerminate())
    {
		fps.Begin();

		// 处理来自ADB的数据包
		if( MODI_ADBClient_ManangerSvr::GetInstancePtr() )
		{
			MODI_ADBClient_ManangerSvr::GetInstancePtr()->ProcessPackages();
		}

		// 处理来自其他服务器的数据包
		MODI_GameSvrClientTask_Ms::ProcessAllPackage();

		MODI_UnifyClient::GetInstance().Get(100);
		
		fps.End();
    }

    Final();
}

void MODI_ManangerSvrLogic::Final()
{
	if( MODI_ManangerService::GetInstancePtr() )
	{
		// 逻辑线程终止，则服务器终止
		MODI_ManangerService::GetInstancePtr()->Terminate();
	}
}
