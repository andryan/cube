#include "Global.h"
#include "PackageHandler_s2sm.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "Channellist.h"
#include "MSServerClient.h"
#include "s2ms_cmd.h"
#include "s2rdb_cmd.h"
#include "s2adb_cmd.h"
#include "s2zs_cmd.h"
#include "ADBClient_ManangerSvr.h"
#include "UnifyClient.h"
#include "s2unify_cmd.h"


MODI_PackageHandler_s2sm::MODI_PackageHandler_s2sm()
{

}

MODI_PackageHandler_s2sm::~MODI_PackageHandler_s2sm()
{

}

bool MODI_PackageHandler_s2sm::FillPackageHandlerTable()
{
	/*-----------------------------------------------------------------------------
	 *   服务器和MANANGER SERVER之间的处理
	 *-----------------------------------------------------------------------------*/

	// 各个服务器向MS注册的通知
	this->AddPackageHandler( MAINCMD_S2MS , MODI_S2MS_Request_ServerReg::ms_SubCmd , 
			&MODI_PackageHandler_s2sm::ServerRequestReg_Handler , "MODI_S2MS_Request_ServerReg" );

	// ls->adb 验证帐号的请求
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_LoginAuth::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToAccountServer_Handle , "MODI_S2ADB_Request_LoginAuth" );

	// ls->zs 请求角色列表
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_S2ZS_Request_ClientLogin::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToZoneServer_Handle ,"MODI_S2ZS_Request_ClientLogin"  );

	// zs -> ls 返回角色列表
	this->AddPackageHandler( MAINCMD_S2ZS , MODI_ZS2S_Notify_Charlist::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToLoginServer_Handle ,"MODI_ZS2S_Notify_Charlist" );

	// ls -> rdb 请求创建角色
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_S2RDB_Request_CreateChar::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToRoleDB_Handle ,  "MODI_S2RDB_Request_CreateChar" );

	// rdb -> ls 创建角色结果的返回通知
	this->AddPackageHandler( MAINCMD_S2RDB , MODI_RDB2S_Notify_CreateCharResult::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToLoginServer_Handle , "MODI_RDB2S_Notify_CreateCharResult" );

	// adb->gs pp验这的结果
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_LoginAuthResult::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToZoneServer_Handle , "MODI_ADB2S_Notify_LoginAuthResult" );

	// zs->adb 请求重新生成pp
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_ReMakePP::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToAccountServer_Handle , "MODI_S2ADB_Request_ReMakePP" );

	// adb->zs 重新生成pp的结果
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_ADB2S_Notify_ReMakePPResult::ms_SubCmd ,
			&MODI_PackageHandler_s2sm::DirectionToZoneServer_Handle , "MODI_ADB2S_Notify_ReMakePPResult" );

	// 频道列表
	this->AddPackageHandler( MAINCMD_LOGIN , MODI_LS2C_Notify_Channellist::ms_SubCmd,
			&MODI_PackageHandler_s2sm::DirectionToLoginServer_Handle , "MODI_LS2C_Notify_Channellist" );

	/// 通知沉迷用户登入
	this->AddPackageHandler( MAINCMD_S2UNIFY , MODI_S2Unify_Onlogin_Cmd::ms_SubCmd,
			&MODI_PackageHandler_s2sm::DirectionToUnifyServer_Handle , "MODI_S2Unify_Onlogin_Cmd" );

	/// 通知沉迷用户登出
	this->AddPackageHandler( MAINCMD_S2UNIFY , MODI_S2Unify_Onlogout_Cmd::ms_SubCmd,
			&MODI_PackageHandler_s2sm::DirectionToUnifyServer_Handle , "MODI_S2Unify_Onlogout_Cmd" );

	/// 通知gmtool 频道信息
	this->AddPackageHandler( MAINCMD_S2UNIFY , MODI_ReturnChannelStatus_Cmd::ms_SubCmd,
			&MODI_PackageHandler_s2sm::DirectionToUnifyServer_Handle , "MODI_ReturnChannelStatus_Cmd" );

	/// 通知gmtool 用户操作结果
	this->AddPackageHandler( MAINCMD_S2UNIFY , MODI_S2Unify_ReturnOpteate_Cmd::ms_SubCmd,
			&MODI_PackageHandler_s2sm::DirectionToUnifyServer_Handle , "MODI_S2Unify_ReturnOpteate_Cmd" );

	return true;
}


int MODI_PackageHandler_s2sm::DirectionToAccountServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_PackageHandler_s2sm * pPH = MODI_PackageHandler_s2sm::GetInstancePtr();
	const char * szCmdName = pPH->GetCmdName( pt_null_cmd->byCmd , pt_null_cmd->byParam );
	if( szCmdName )
	{
		Global::logger->debug("[%s] direction package<%s> to AccountDBServer ." , 
				SVR_TEST , 
				szCmdName );
	}

	bool bOk = false;
	MODI_ADBClient_ManangerSvr * pAdb = MODI_ADBClient_ManangerSvr::GetInstancePtr();
	if( pAdb )
	{
		bOk = pAdb->SendCmd( pt_null_cmd , cmd_size );
		if( !bOk )
		{
			Global::logger->warn("[%s] can't direction package to AccountDB Server . sendcmd faild." , 
					ERROR_CON );
		}
	}
	else 
	{
		Global::logger->warn("[%s] can't direction package to AccountDB Server . disconnection with AccountDB Server." , 
				ERROR_CON );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2sm::DirectionToZoneServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	bool bOk = false;
	
	MODI_PackageHandler_s2sm * pPH = MODI_PackageHandler_s2sm::GetInstancePtr();
	const char * szCmdName = pPH->GetCmdName( pt_null_cmd->byCmd , pt_null_cmd->byParam );
	if( szCmdName )
	{
		Global::logger->debug("[%s] direction package<%s> to AccountDBServer ." , 
				SVR_TEST , 
				szCmdName );
	}

	MODI_MSServerClient * pZoneServer = MODI_MSServerClient::GetZoneServerClient();
	if( pZoneServer )
	{
		bOk = pZoneServer->SendPackage( pt_null_cmd , cmd_size );		
		if( !bOk )
		{
			Global::logger->warn("[%s] can't direction package to Zone Server . sendcmd faild." , 
					ERROR_CON );
		}
	}
	else
	{
		Global::logger->warn("[%s] can't direction package to Zone server . disconnection with Zone server." , 
				ERROR_CON );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2sm::DirectionToRoleDB_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	bool bOk = false;	

	MODI_PackageHandler_s2sm * pPH = MODI_PackageHandler_s2sm::GetInstancePtr();
	const char * szCmdName = pPH->GetCmdName( pt_null_cmd->byCmd , pt_null_cmd->byParam );
	if( szCmdName )
	{
		Global::logger->debug("[%s] direction package<%s> to AccountDBServer ." , 
				SVR_TEST , 
				szCmdName );
	}

	MODI_MSServerClient * pDBProxy = MODI_MSServerClient::GetDBProxyClient();
	if( pDBProxy )
	{
		bOk = pDBProxy->SendPackage( pt_null_cmd , cmd_size );
		if( !bOk )
		{
			Global::logger->warn("[%s] can't direction package to RoleDB Server . sendcmd faild." , 
					ERROR_CON );
		}
	}
	else
	{
		Global::logger->warn("[%s] can't direction package to RoleDB Server . disconnection with RoleDB Server." , ERROR_CON );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2sm::DirectionToLoginServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	bool bOk = false;

	MODI_PackageHandler_s2sm * pPH = MODI_PackageHandler_s2sm::GetInstancePtr();
	const char * szCmdName = pPH->GetCmdName( pt_null_cmd->byCmd , pt_null_cmd->byParam );
	if( szCmdName )
	{
		Global::logger->debug("[%s] direction package<%s> to AccountDBServer ." , 
				SVR_TEST , 
				szCmdName );
	}

	MODI_MSServerClient * pLoginSvr = MODI_MSServerClient::GetLoginServerClient();
	if( pLoginSvr )
	{
		bOk = pLoginSvr->SendPackage( pt_null_cmd , cmd_size );
		if( !bOk )
		{
			Global::logger->warn("[%s] can't direction package to Login Server . sendcmd faild." , 
					ERROR_CON );
		}
	}
	else
	{
		Global::logger->warn("[%s] can't direction package to Login server . disconnection with Login server." , 
				ERROR_CON );
	}

	return enPHandler_OK;
}

int MODI_PackageHandler_s2sm::ServerRequestReg_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	Global::logger->info("[%s] recv server register package. bleow information :" , "ServerRegister" );

	MODI_MSServerClient * pClient = (MODI_MSServerClient *)(pObj);
	const MODI_S2MS_Request_ServerReg * pMsg = (const MODI_S2MS_Request_ServerReg *)(pt_null_cmd);

//	if( pClient->IsRegisterType() )
//	{
//		Global::logger->error("[%s] dup register !!!" , "ServerRegister" );
//		return enPHandler_OK;
//	}

	pClient->SetServerType( pMsg->m_iServerType );

	if( pMsg->m_iServerType == SVR_TYPE_LS )
	{
		Global::logger->info("[%s] server type is <loginserver> ." , "ServerRegister" );
		if( MODI_MSServerClient::GetLoginServerClient() )
		{
			Global::logger->info("[%s]  but is already exists other one . kick this one." , "ServerRegister" );
			pClient->Disconnection();
			return enPHandler_OK;
		}
		else 
		{
			MODI_MSServerClient::SetLoginServerClient( pClient );
		}
	}
	else if ( pMsg->m_iServerType == SVR_TYPE_GS )
	{

	}
	else if( pMsg->m_iServerType == SVR_TYPE_ZS )
	{
		Global::logger->info("[%s] server type is <zoneserver> ." , "ServerRegister" );
		if( MODI_MSServerClient::GetZoneServerClient() )
		{
			Global::logger->info("[%s]  but is already exists other one . kick this one." , "ServerRegister" );
			pClient->Disconnection();
			return enPHandler_OK;
		}
		else
		{
			MODI_MSServerClient::SetZoneServerClient( pClient );
		}
	}
	else if( pMsg->m_iServerType == SVR_TYPE_DB )
	{
		Global::logger->info("[%s] server type is <dbproxy> ." , "ServerRegister" );
		if( MODI_MSServerClient::GetDBProxyClient() )
		{
			Global::logger->info("[%s]  but is already exists other one . kick this one." , "ServerRegister" );
			pClient->Disconnection();
			return enPHandler_OK;
		}
		else
		{
			MODI_MSServerClient::SetDBProxyClient( pClient );
		}
	}

	return enPHandler_OK;
}

/** 
 * @brief 发送命令给UnifyService
 * 
 * @param pObj 
 * @param pt_null_cmd 
 * @param cmd_size 
 * 
 * @return 
 */
int MODI_PackageHandler_s2sm::DirectionToUnifyServer_Handle( void * pObj , const Cmd::stNullCmd * pt_null_cmd , const unsigned int cmd_size )
{
	if(pObj == NULL || pt_null_cmd == NULL || cmd_size <= sizeof(Cmd::stNullCmd))
	{
		Global::logger->fatal("[directto_unify_cmd] send a command to unify failed");
		return enPHandler_OK;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[directto_unify_cmd] send a command to unify service<byCmd=%d,byParam=%d>", pt_null_cmd->byCmd, pt_null_cmd->byParam);
#endif
	
	MODI_UnifyClient::GetInstance().SendCmd(pt_null_cmd, cmd_size);
	return enPHandler_OK;
}



