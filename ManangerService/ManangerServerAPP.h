#ifndef MODI_MANANGERSERVER_APPFRAME_H_
#define MODI_MANANGERSERVER_APPFRAME_H_


#include "IAppFrame.h"
#include "AssertEx.h"
#include "ADBClient_ManangerSvr.h"
#include "ManangerService.h"
#include "PackageHandler_s2sm.h"
#include "ManangerSvrLogic.h"
#include "Channellist.h"




class 	MODI_ManangerServerAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_ManangerServerAPP( const char * szAppName );

		virtual ~MODI_ManangerServerAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

	private:

		MODI_SvrChannellist  		m_Channellist; // 当前区的频道列表
		MODI_PackageHandler_s2sm 	m_PackageHalder_s2sm;	// 数据包处理器
		MODI_ADBClient_ManangerSvr * m_pAccountDBClient; // 连接 account db 服务器的客户端
};

#endif
