#include "ManangerService.h"
#include "Global.h"
#include "ServerConfig.h"
#include "AssertEx.h"
#include "GameSvrClientTask.h"
#include "MSServerClient.h"


MODI_ManangerService::MODI_ManangerService(std::vector<WORD > &  vec, const std::vector<std::string> & vecip , const char * name , const int count): 
	MODI_MNetService(vec, vecip , name, count)
{

}

/// 创建新的连接
bool MODI_ManangerService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	if(sock == -1 || addr == NULL)
	{
		return false;
	}
	
	if( this->m_PortVec.size() !=  MSSVR_PORTIDX_COUNT )
	{
		MODI_ASSERT(0);
		return false;
	}
	
	if(port == this->m_PortVec[ MSSVR_PORTIDX_SVRS ] )
	{
		Global::logger->debug("add a gameserver task(%s,%d)", inet_ntoa(addr->sin_addr), ntohs(addr->sin_port));

		MODI_GameSvrClientTask_Ms * pTask = new MODI_GameSvrClientTask_Ms( sock , addr );
		MODI_MSServerClient * pClient = new MODI_MSServerClient( pTask );
		pTask->SetClient( pClient );

		MODI_GameSvrClientTask_Ms::ms_ProcessBase.OnAcceptConnection( pTask );

		//m_pTaskPools[ MSSVR_PORTIDX_SVRS ]->AddTask( pTask );
		m_pTaskSched[MSSVR_PORTIDX_SVRS].AddNormalSched(pTask);
		return true;
	}

	TEMP_FAILURE_RETRY(::close(sock));
	MODI_ASSERT(0);

	return false;
}

void MODI_ManangerService::Final()
{
	for( int i = 0 ; i < MSSVR_PORTIDX_COUNT ; i++ )
	{
		m_pTaskSched[i].Final();
	}
	// 
}

bool MODI_ManangerService::Init()
{
	if( MODI_MNetService::Init() )
	{
		for( int i = 0 ; i < MSSVR_PORTIDX_COUNT ; i++ )
		{
			m_pTaskSched[i].Init(); 
		}
			
		return true;
	}

	return false;
}

