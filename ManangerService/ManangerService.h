/** 
 * @file SvrMgrService.h
 * @brief MANANGER SERVER 的网络服务
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-11
 */

#ifndef MANANGERSVR_SERVICE_H_
#define MANANGERSVR_SERVICE_H_

#include <vector>
#include <iostream>
#include "MNetService.h"
#include "ServiceTaskSched.h"
#include "SingleObject.h"

enum
{
	MSSVR_PORTIDX_SVRS = 0,
	MSSVR_PORTIDX_COUNT,
};

/**
 * @brief 多监听的服务器
 * 
 */
class MODI_ManangerService: public MODI_MNetService , public CSingleObject<MODI_ManangerService>
{
 public:
	MODI_ManangerService(std::vector<WORD > &  vec, const std::vector<std::string> & vecip , 
			const char * name = "ManangerService", const int count = MSSVR_PORTIDX_COUNT );

	bool CreateTask(const int & sock, const struct sockaddr_in * addr , const WORD & port);
		
	virtual bool Init();

	virtual void Final();

 private:

	//MODI_ServiceTaskPoll * m_pTaskPools[MSSVR_PORTIDX_COUNT];
	MODI_ServiceTaskSched m_pTaskSched[MSSVR_PORTIDX_COUNT];
};

#endif
