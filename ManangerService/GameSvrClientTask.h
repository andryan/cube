/** 
 * @file GameSvrClientTask.h
 * @brief MANANGER SERVER 上的代表游戏服务器的TASK
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-11
 */
#ifndef GAMESVRCLIENT_TASK_H_
#define GAMESVRCLIENT_TASK_H_

#include "ServiceTask.h"
#include "RecurisveMutex.h"
#include "CommandQueue.h"
#include <set>
#include "SvrPackageProcessBase.h"

class MODI_MSServerClient;

class  MODI_GameSvrClientTask_Ms : public MODI_IServerClientTask
{
	friend class MODI_ManangerService;

		void 	SetClient( MODI_MSServerClient * p );

	public:

		MODI_GameSvrClientTask_Ms(const int sock, const struct sockaddr_in * addr);
		virtual ~MODI_GameSvrClientTask_Ms();

	public:

		virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

		virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

		virtual int RecycleConn();
		virtual void DisConnect();

		// 连接上的回调,可以安全的在逻辑线程调用
		virtual void OnConnection(); 

		// 断开的回调，可以安全的在逻辑线程调用
		virtual void OnDisconnection(); 

	public:


		/** 
		 * @brief 处理所有客户端的数据包
		 */
		static void 	ProcessAllPackage();

		static MODI_SvrPackageProcessBase 	ms_ProcessBase;

	 private:

		MODI_MSServerClient * m_pClient;
};


#endif
