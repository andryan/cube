/**
 * @file   UnifyClient.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:07:02 2011
 * 
 * @brief  连接平台账号验证
 * 
 * 
 */


#ifndef _MD_UNIFYCLIENT_H
#define _MD_UNIFYCLIENT_H

#include "Global.h"
#include "RClientTask.h"
#include "CommandQueue.h"

/**
 * @brief unify连接
 * 
 */
class MODI_UnifyClient: public MODI_RClientTask, public MODI_CmdParse
{
public:
	MODI_UnifyClient(const char * name, const char * server_ip,const WORD & port): MODI_RClientTask(name, server_ip, port), m_strClientName(name)
	{
		Resize(GWTOGS_CMDSIZE);
	}

	static MODI_UnifyClient & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_UnifyClient("unifyclient", Global::g_strUnifyIP.c_str(), Global::g_wdUnifyPort);
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	/// 初始化
	bool Init();

	const bool IsConnected();

	/** 
	 * @brief 发送命令
	 * 
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 队列内的命令
	 * 
	 * @param pt_null_cmd 获取的命令
	 * @param dwCmdLen 命令长度
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

 private:
	static MODI_UnifyClient * m_pInstance;
	virtual ~MODI_UnifyClient()
	{
		ClientFinal();
	}
	
	void ClientFinal();

	std::string m_strClientName;

};


#endif
