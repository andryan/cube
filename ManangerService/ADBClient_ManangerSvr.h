/** 
 * @file ADBClient_ManangerSvr.h
 * @brief 连接帐号数据库的客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-12
 */
#ifndef ACCOUNTDBCLIENT_MANANGERSERVER_H_
#define ACCOUNTDBCLIENT_MANANGERSERVER_H_

#include "ClientTask.h"
#include "SingleObject.h"
#include "CommandQueue.h"
#include "RecurisveMutex.h"

/** 
 * @brief 连接帐号DB的客户端模型
 */
class MODI_ADBClient_ManangerSvr : public MODI_ClientTask  , 
	public CSingleObject<MODI_ADBClient_ManangerSvr> ,
	public MODI_CmdParse
{
public:

	MODI_ADBClient_ManangerSvr(const char * name, const char * server_ip,const WORD & port);

	~MODI_ADBClient_ManangerSvr();

	/// 发送命令
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/// 命令处理
	virtual bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	virtual bool CmdParseQueue(const Cmd::stNullCmd * ptNullCmd, const unsigned int dwCmdLen); 

	/// 初始化
	virtual	bool Init();

	void 	ProcessPackages();

 protected:

	virtual void Final();

};


#endif
