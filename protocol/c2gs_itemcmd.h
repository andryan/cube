/**
 * @file   c2gs_itemcmd.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jun 24 18:40:38 2011
 * 
 * @brief  道具相关命令定义
 * 
 */

#ifndef C2GS_ITEMCMD_DEFINED_H_
#define C2GS_ITEMCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Item_Cmd : public stNullCmd
{
	MODI_Item_Cmd()
	{
        byCmd = MAINCMD_ITEM;
	}
};


/// 物品列表的相关同步(只有进入频道和进入商场里面刷新)
struct MODI_GS2C_Notify_Itemlist : public MODI_Item_Cmd
{
	/// 物品个数
	WORD m_nItemSize;
	/// 物品信息
	MODI_ItemInfo m_pItemlist[0];

	MODI_GS2C_Notify_Itemlist()
	{
		byParam = ms_SubCmd;
		m_nItemSize = 0;
	}

	static const BYTE ms_SubCmd = 1;
};

/// 增加物品
struct MODI_GS2C_AddItem_Cmd: public MODI_Item_Cmd
{
	/// 增加理由
	enAddItemReason m_enAddReason;
	/// 物品信息
	MODI_ItemInfo m_stItemInfo;

	MODI_GS2C_AddItem_Cmd()
	{
		m_enAddReason = enAddReason_None;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 2;
};


/// 减去物品(过期，使用掉了)
struct MODI_GS2C_DelItem_Cmd: public MODI_Item_Cmd
{
	/// 减去的物品id
	QWORD m_qdItemId;
	/// 减去的理由
	enDelItemReason m_enDelReason;

	MODI_GS2C_DelItem_Cmd()
	{
		m_enDelReason = enDelReason_None;
		byParam = ms_SubCmd;
		m_qdItemId = 0;
	}

	static const BYTE ms_SubCmd = 3;
};

/// 更新道具信息
struct MODI_GS2C_UpdateItem_Cmd: public MODI_Item_Cmd
{
	/// 更新的理由
	enUpdateItemReason m_enUpdateReason;

	WORD m_wdSize;
	/// 道具信息
	MODI_ItemInfo m_stItemInfo[0];

	MODI_GS2C_UpdateItem_Cmd()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
		m_enUpdateReason = enUpdateReason_None;
	}
	
	static const BYTE ms_SubCmd = 4;
};


/// 使用物品
struct MODI_C2GS_UseItem_Cmd: public MODI_Item_Cmd
{
	/// 物品id
	QWORD m_qdItemId;
	/// 对象
	MODI_GUID m_qdTargetId;

	MODI_C2GS_UseItem_Cmd()
	{
		byParam = ms_SubCmd;
		m_qdItemId = 0;
	}

	static const BYTE ms_SubCmd = 5;
};


// 使用物品的结果
struct MODI_GS2C_Notify_UseItemResult : public MODI_Item_Cmd
{
	enUseItemResult 	m_result; // 结果
	QWORD 				m_qdItemId;

	MODI_GS2C_Notify_UseItemResult()
	{
		m_qdItemId = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 7;
};


// 客户端请求使用key兑换东西
struct MODI_C2GS_Request_ExchangeYunYingKey : public MODI_Item_Cmd
{
	char 				m_szKey[YUNYING_CDKEY_MAXSIZE + 1];

	MODI_C2GS_Request_ExchangeYunYingKey()
	{
		byParam = ms_SubCmd;
		memset( m_szKey , 0 , sizeof(m_szKey) );
	}

	static const BYTE ms_SubCmd = 9;
};

// 服务器对兑换的结果返回
struct MODI_GS2C_Notify_ExchangeYunYingKeyRes : public MODI_Item_Cmd
{
	enYunYingKeyExchangeResult m_result;

	MODI_GS2C_Notify_ExchangeYunYingKeyRes()
	{
		byParam = ms_SubCmd;
		m_result = kYYKeyExchange_UnKnowError;
	}

	static const BYTE ms_SubCmd = 10;
};

// 请求录音
struct MODI_C2GS_Request_RecordMusic : public MODI_Item_Cmd
{
	MODI_C2GS_Request_RecordMusic()
	{
		byParam = ms_SubCmd;
		m_qdItemId = 0;
	}

	QWORD m_qdItemId;
	static const BYTE ms_SubCmd = 11;
};


struct MODI_GS2C_Notify_RecordMusicRes : public MODI_Item_Cmd
{
	enum
	{
		kSuccessful,
		kNoItem,
		kFaild,
	};

	BYTE 	result;

	MODI_GS2C_Notify_RecordMusicRes()
	{
		result = kFaild;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 12;
};


/// 礼包增加物品
struct MODI_GS2C_Notify_AddItemByBox : public MODI_Item_Cmd
{    
    BYTE                m_count;
    WORD                m_configids[20];

    MODI_GS2C_Notify_AddItemByBox()
    {
        m_count = 0;
        memset( m_configids ,  INVAILD_CONFIGID , sizeof(m_configids) );
        byParam = ms_SubCmd;
    }

    static const BYTE ms_SubCmd = 13;
};


///	服务器通知客户端礼包列表
struct MODI_GS2C_Notify_PacketList : public MODI_Item_Cmd
{
	MODI_GS2C_Notify_PacketList()
	{
		byParam = ms_SubCmd;
		m_wSize = 0;
	}

	WORD	m_wSize;
	PacketInfo  m_pPacket[0];
	static const BYTE	ms_SubCmd = 17;
};

///  服务器通知客户端，有物品将要24小时内过期
struct MODI_GS2C_Notify_ItemWillExpire : public MODI_Item_Cmd
{
	MODI_GS2C_Notify_ItemWillExpire()
	{
		byParam = ms_SubCmd;
	}
	static const BYTE	ms_SubCmd = 19;
};
enum	enVipTimeType
{
	enVipTimeType_None=0,	///非法的时间
	enVipTimeType_30days,	/// 30天的VIP
	enVipTimeType_90days,	/// 90天的VIP
	
};
///服务器通知成为VIP
//
struct MODI_GS2C_Notify_VipTime : public MODI_Item_Cmd
{
	MODI_GS2C_Notify_VipTime()
	{
		byParam = ms_SubCmd;
		m_eTime = enVipTimeType_None;
	}
	enVipTimeType		m_eTime;
	static const BYTE	ms_SubCmd = 20;

};

///VIP过期
//
struct MODI_GS2C_Notify_VipExpire : public MODI_Item_Cmd
{
	MODI_GS2C_Notify_VipExpire()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE	ms_SubCmd = 21;
};

/// 客户端请求获得宝盒
//
struct MODI_C2GS_Request_AddItemBox : public 	MODI_Item_Cmd
{
	MODI_C2GS_Request_AddItemBox()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE	ms_SubCmd = 23;
};

struct BagItemInfos
{
	BagItemInfos()
	{
		m_wConfigId = INVAILD_CONFIGID;
		m_bTime = 0;
		m_bNum = 0;
	}

	///  物品的CONFIGID
	WORD	m_wConfigId;
	///物品的时间类型 1 7天， 2 30 天, 3无限
	BYTE	m_bTime;
	/// 物品数量 
	BYTE	m_bNum;
};
#define	MAX_ITEM_BOX_NUM  5

///打开宝盒后，通知宝盒物品列表
struct MODI_GS2C_Notify_BoxItemList : public MODI_Item_Cmd
{
	/// 一共开出来了几个格子的物品
	BYTE	m_bNum; 	
	BagItemInfos 	m_sInfo[MAX_ITEM_BOX_NUM];

	MODI_GS2C_Notify_BoxItemList()
	{
		byParam = ms_SubCmd;
	}
	static const BYTE	ms_SubCmd = 24;
};

#define	BOX_NEED_YINFU_NUM 	4
///  请求打开宝盒
struct MODI_C2GS_Request_OpenItemBox : public MODI_Item_Cmd
{
	/// 宝盒的ID
	QWORD	m_qwBagId;
	///四个音符
	QWORD	m_qwYinFu[BOX_NEED_YINFU_NUM];

	MODI_C2GS_Request_OpenItemBox()
	{
		m_qwBagId  = 0;
		memset(m_qwYinFu,0,sizeof(m_qwYinFu));
		byParam = ms_SubCmd;
	}

	static const BYTE	ms_SubCmd = 26;
};



#pragma pack(pop)

#endif
