/** 
 * @file svrresult.h
 * @brief 游戏中通用操作结果定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef SERVER_RESULT_H_
#define SERVER_RESULT_H_

#include "BaseOS.h"


enum
{
	SvrResult_Ok = 0,
	SvrResult_UnknowErr = 0, //	未知错误

	SvrResult_Login_InvaildAP = 100, //	帐号或密码无效
	SvrResult_Login_AccountUsing, // 帐号正在使用中...（临时方案)
	SvrResult_Login_OK , //	登入成功
	SvrResult_Login_ServerError, // 服务器内部异常，无法登入，稍候再试
	SvrResult_Login_NoActive, // 账号未激活
	SvrResult_Login_RechangeChannelFaild, // 切换频道失败
	SvrResult_Login_RechangeChannelOk, // 可以切换频道
	SvrResult_Login_InvalidVersion, // 无效版本

	SvrResult_Chat_CannotFindTarget = 200, //	聊天目标不在线
	SvrResult_Chat_InvaildChannel, //	无效的聊天频道
	SvrResult_Chat_YouNotHaveItem, //	无法聊天，没有相关物品（飞子道具或者频道聊天道具)

	SvrResult_Room_CInvaildPara = 300,//	创建房间失败，无效的房间参数
	SvrResult_Room_CreateFaild,//无法创建房间，未知原因（请稍候再试)
	SvrResult_Room_InvaildModeC, //创建房间失败，无效的游戏模式
	SvrResult_Room_LobbyFullC,//无法创建房间，大厅房间个数已满
	SvrResult_Room_LimitsC,//无法创建房间，权限限制
	SvrResult_Room_InvaildPwdJ,//无法进入房间，无效密码
	SvrResult_Room_NotExistJ,//无法进入房间，房间不存在
	SvrResult_Room_StartedJ,//无法进入房间，房间已经开始游戏
	SvrResult_Room_PlayerFullJ,//无法进入房间，房间中人数已满
	SvrResult_Room_CannotRandomJ,//随机进入房间失败
	SvrResult_Room_LeaveOk,//成功离开房间
	SvrResult_Room_BeKick,//被房主踢了
	SvrResult_Room_CanNotStart,//房主无法开始游戏
	SvrResult_Room_ReadyCC,//无法交换位置，自己处于准备状态 ( cc = can't change)
	SvrResult_Room_FullCC,// 无法交换位置，对面的位置已满
	SvrResult_Room_ExistClientCC,//无法交换位置，目标位置有玩家
	SvrResult_Room_ClosedPosCC,//无法交换位置，目标位置已经关闭
	SvrResult_Room_KickPlayerFaild, // 踢人失败
	SvrResult_Room_TransferMasterFaild, // 转让房主失败
	  
//#ifdef _DEBUG
	SvrResult_Room_NotInRoomButSendReady, // 玩家不在房间中，却发送了准备请求
	SvrResult_Room_IsMasterButSendReady,// 玩家不是房主，却发送了准备请求
	SvrResult_Room_IsStartButSendReady,// 房间已经开始游戏，却发送了准备请求
	SvrResult_Room_IsNotPlayerButSendReady,// 不是参战玩家，却发送了准备请求

	SvrResult_Room_NotInRoomButSendUnReady, // 玩家不在房间中，却发送了取消准备请求
	SvrResult_Room_IsMasterButSendUnReady,// 玩家不是房主，却发送了取消准备请求
	SvrResult_Room_IsStartButSendUnReady,// 房间已经开始游戏，却发送了取消准备请求
	SvrResult_Room_IsNotPlayerButSendUnReady,// 不是参战玩家，却发送了取消准备请求

	SvrResult_Room_NotInRoomButSendKick,//不在房间中，却发送了踢人请求
    SvrResult_Room_IsNotMasterButSendKick,//不是房主，却发送了踢人请求
	SvrResult_Room_IsStartButSendKick,//游戏已经开始，却发送了踢人请求

	SvrResult_Room_NotInRoomButSendOnPos,// 不在房间中，却发送了开位置请求
	SvrResult_Room_IsNotMasterButSendOnPos,//不是房主，却发送了开位置请求
	SvrResult_Room_IsStartButSendOnPos,//游戏开始后，却发送了开位置请求

	SvrResult_Room_NotInRoomButSendOffPos,// 不在房间中，却发送了关位置请求
	SvrResult_Room_IsNotMasterButSendOffPos,//不是房主，却发送了关位置请求
	SvrResult_Room_IsStartButSendOffPos,//游戏开始后，却发送了关位置请求

	SvrResult_Room_NotInRoomButSendChangeMusic,//不在房间中，却发送了改变歌曲的请求
	SvrResult_Room_IsNotMasterButSendChangeMusic,//不是房主，却发送了改变歌曲的请求
	SvrResult_Room_IsStartButSendChangeMusic,//游戏开始后，却发送了改变歌曲的请求

	SvrResult_Room_NotInRoomButSendChangeMap,//不在房间中，却发送了改变地图的请求
	SvrResult_Room_IsNotMasterButSendChangeMap,//不是房主，却发送了改变地图的请求
	SvrResult_Room_IsStartButSendChangeMap,//游戏开始后，却发送了改变地图的请求

	SvrResult_Room_NotInRoomButSendStart, // 不在房间中，却发送了开始游戏请求
	SvrResult_Room_IsNotMasterButSendStart,// 不是房主，却发送了开始游戏请求
	SvrResult_Room_IsStartButSendStart,//已经开始游戏，却发送了开始游戏请求

	SvrResult_Room_ChangeSonger_InvaildGUID, // 改变听歌对象失败，无效的GUID
	SvrResult_Room_ChangeSonger_SelfGUID, // 改变听歌对象失败，不能自己听自己
	SvrResult_Room_ChangeSonger_NewSongerNotChange, // 改变听歌对象失败，新的听歌对象和之前的一样，没有改变
	SvrResult_Room_ChangeSonger_DestSongerNotPlayer, // 改变听歌对象失败，目标GUID的客户端不是参战玩家
	SvrResult_Room_ChangeSonger_DestSongerNotExist, // 改变听歌对象失败，目标GUID的客户端不存在

//#endif
	SvrResult_Role_CreateRoleSuccessful = 400,// 创建角色成功
	SvrResult_Role_CreateRoleFaild,// 创建角色失败
	SvrResult_Role_CRFNameAlreadyExist, // 创建角色失败,角色名已经存在
	SvrResult_Role_CRFNameHaveDisableString, // 创建角色失败，名字有脏字
	SvrResult_Role_CRFFaildUnknowError, // 创建角色失败，服务器异常



	SvrResult_World_Kick_SbLogin = 500, // 有玩家在其他地方登入

	SvrResult_ExchangeAvatar_ItemTypeFaild = 600, // 所要装备的物品和装备位置 类型不匹配
	SvrResult_ExchangeAvatar_ReqLevelFaild, // 等级需求不符合条件，
	SvrResult_ExchangeAvatar_ReqSexFaild, // 性别需求不符合条件
	SvrResult_ExchangeAvatar_BagFullFaild, // 玩家背包（衣橱）已经满，无法将物品放入背包
	SvrResult_ExchangeAvatar_Faild, // 更换AVATAR失败。。服务器原因

	SvrResult_ModifyRoleinfo_Faild = 700, // 修改角色信息失败
	SvrResult_ModifyRoleinfo_Ok, // 修改角色信息成功

	SvrResult_InvitePlay_TargetIsInRoom = 800, // 邀请的目标玩家已经在房间中了
	SvrResult_InvitePlay_NotExist, // 邀请的玩家不存在，（已经离线)

	SvrResult_InvitePlayRes_RoomNotExist = 900, // 被邀请进入的房间不存在了
	SvrResult_InVitePlayRes_RoomIsPlaying, // 被邀请进入的房间已经开始游戏了
	SvrResult_InVitePlayRes_RoomIsFull, // 被邀请进入的房间已经满

	SvrResult_ModifyRoomInfo_Faild = 1000, // 修改房间信息失败
};

#endif
