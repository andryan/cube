/** 
 * @file c2gs_musiccmd.h
 * @brief 音乐数据相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_MUSICCMD_DEFINED_H_
#define C2GS_MUSICCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"

/*
 * 音乐传输相关命令
 */

#pragma pack(push,1)

struct MODI_Music_Cmd : public stNullCmd
{
	MODI_Music_Cmd()
	{
        byCmd = MAINCMD_MUSIC;
	}
};

 
struct NoteValue
{
   int  PitchValue;   //音高
   float32  StartTime;    //起始时间
   float32  EndTime;      //结束时间
   
   NoteValue()
   {
      PitchValue = 0;
      StartTime  = 0.f;
      EndTime  = 0.f;
   }
};

// 客户端给其他客户端传输语音数据(接收客户端也是收到此包)
struct MODI_C2C_Notify_MusicData : public MODI_Music_Cmd
{
    WORD m_nSize;//语音数据的大小
	DWORD m_nCrc; // crc 值
	MODI_GUID 		m_nSongerGUID; // 音乐发送者的GUID
	float32       m_CurrentTime;  //当前时间
	int         m_NoteValueArySize;     //音准条大小
	NoteValue   m_NoteValueAry[100]; //当前Note数据
	char 		m_pMusicData[0];//语音数据内容

	
	MODI_C2C_Notify_MusicData()
	{
		byParam = ms_SubCmd;
		m_nSize = 0;
		m_nSongerGUID = INVAILD_GUID;
		m_nCrc = 0;
		m_NoteValueArySize = 0;
	}

	static const BYTE 	ms_SubCmd = 1;
};

/// 客户端请求切换听歌对象
struct MODI_C2GS_Request_ChangeObSinger : public MODI_Music_Cmd
{
	MODI_GUID      m_SingerID;//要听的对象GUID

	MODI_C2GS_Request_ChangeObSinger()
	{
		byParam = ms_SubCmd;
		m_SingerID = INVAILD_GUID;
	}

	static const BYTE 	ms_SubCmd = 2;
};

///  服务器同步音乐头给客户端
struct MODI_GS2C_Notify_MusicHeaderData : public MODI_Music_Cmd
{
	MODI_GUID 		m_nSongerGUID;
	DWORD 	m_nSize;
	char 			m_pData[0];
	MODI_GS2C_Notify_MusicHeaderData()
	{
		byParam = ms_SubCmd;
		m_nSongerGUID = INVAILD_GUID;
		m_nSize = 0;
	}

	static const BYTE 	ms_SubCmd = 3;
};

/// 客户端告诉服务器，游戏结束(临时方案)
struct MODI_C2GS_Request_GameEnd : public MODI_Music_Cmd
{
	MODI_C2GS_Request_GameEnd()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 4;
};


/// 客户端发送音乐头（进入游戏的第一个包)
struct MODI_C2GS_Request_MusicHeaderData : public MODI_Music_Cmd
{
	MODI_C2GS_Request_MusicHeaderData()
	{
		byParam = ms_SubCmd;
		m_nMusicDataHeaderSize = 0;
	}

	WORD m_nMusicDataHeaderSize; // 语音数据的包头大小
	char  m_pMusicDataHeader[0];//语音数据的包头

	static const BYTE 	ms_SubCmd = 5;
};

///  客户端通知自己的麦克风有声音数据
struct MODI_C2GS_Request_MicHasData : public MODI_Music_Cmd
{
	bool 	m_bHasData;
	float 	m_fTime;

	MODI_C2GS_Request_MicHasData()
	{
		byParam = ms_SubCmd;
		m_bHasData = false;
		m_fTime = 0.0f;
	}

	static const BYTE 	ms_SubCmd = 6;
};

//   服务器通知客户端，某个客户端的麦克风有数据
struct MODI_GS2C_Notify_MicHasData : public MODI_Music_Cmd
{
	MODI_GUID 	m_guid; // 哪个客户端的麦克风有数据
	bool 		m_bHasData;
	float 		m_fTime;

	MODI_GS2C_Notify_MicHasData()
	{
		byParam = ms_SubCmd;
		m_bHasData = false;
		m_fTime = 0.0f;
	}

	static const BYTE 	ms_SubCmd = 7;
};

/// 键盘数据
struct KeyData
{
	float m_time;
	bool m_state;
	bool m_attack;

	KeyData()
	{
		m_time = 0.0f;
		m_state = false;
		m_attack = false;
	}
};

// 客户端给其他客户端传输键盘数据(接收客户端也是收到此包)
struct MODI_C2C_Notify_KeyData : public MODI_Music_Cmd
{
	DWORD m_nCrc; // crc 值
	MODI_GUID 		m_nSongerGUID; // 音乐发送者的GUID
	float32       m_CurrentTime;  //当前时间
	int         m_KeyDataArySize;     //键大小
	KeyData     m_KeyDataAry[20]; //当前key数据


	MODI_C2C_Notify_KeyData()
	{
		byParam = ms_SubCmd;
		m_nSongerGUID = INVAILD_GUID;
		m_nCrc = 0;
		m_KeyDataArySize = 0;
	}

	static const BYTE 	ms_SubCmd = 8;
};

#pragma pack(pop)

#endif
