#ifndef C2GS_MAILCMD_DEFINED_H_
#define C2GS_MAILCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Mail_Cmd : public stNullCmd
{
	MODI_Mail_Cmd()
	{
        byCmd = MAINCMD_MAIL;
	}
};

struct MODI_C2GS_Request_SendMail : public MODI_Mail_Cmd
{
	MODI_SendMailInfo 	m_sendMail;

	MODI_C2GS_Request_SendMail()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};
 
struct MODI_GS2C_Notify_SendMailRes : public MODI_Mail_Cmd
{
	enum enResult
	{
		kSuccessful = 0,  // 邮件发送成功
		kMailBoxFull, // 对方的邮箱满了
		kNoChar, // 没有该角色
		kUnknowError, // 发送失败，邮件服务器异常。客户端提示下
	};

	enResult 	m_result;
	char 	m_szTargetName[ROLE_NAME_MAX_LEN + 1]; // 发送谁的邮件

	MODI_GS2C_Notify_SendMailRes()
	{
		m_result = kUnknowError;
		memset( m_szTargetName , 0 , sizeof(m_szTargetName) );
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 2;
};


struct MODI_MailAttachInfo
{
	MODI_MailAttachInfo()
	{
		m_wdConfigId = 0;
		m_wdCount = 0;
		m_byTime = 0;
	}

	WORD m_wdConfigId;
	WORD m_wdCount;
	BYTE m_byTime;
};


struct MODI_GS2C_Notify_AddMail : public MODI_Mail_Cmd
{
	enum enType
	{
		kLogin, // 登入时发的邮件列表
		kNew, // 新的邮件
	};
	
	enType 	m_type;
	MODI_GUID 		m_mailGUID;
	enMailType 		m_Type; // 邮件类型
	char 		 	m_State; // 邮件状态
	char 			m_szSender[ROLE_NAME_MAX_LEN + 1]; 	// 发送者
	char 			m_szRecevier[ROLE_NAME_MAX_LEN + 1]; // 接受者
	char 			m_szCaption[MAIL_CAPTION_MAX_LEN + 1]; // 邮件标题
	time_t 			m_SendTime; 	// 	发送的时间
	BYTE 			m_byExtraCount;
	MODI_MailExtraInfo m_ExtraInfo[MAX_MAIL_ATTACH_NUM];
	
	WORD 			m_nContentsize;
	char  			m_szContents[0]; // 内容

	MODI_GS2C_Notify_AddMail()
	{
		m_type = kLogin;
		byParam = ms_SubCmd;

		m_Type = kTextMail;
		m_State = 0;
		memset( m_szSender , 0 , sizeof(m_szSender) );
		memset( m_szRecevier , 0 , sizeof(m_szRecevier) );
		memset( m_szCaption , 0 , sizeof(m_szCaption) );
		m_byExtraCount = 0;
		
		for(int i=0; i<MAX_MAIL_ATTACH_NUM; i++)
		{
			m_ExtraInfo[i].Reset();
		}
		
		m_nContentsize = 0;
	}

	void 	Set( const MODI_MailInfo & info )
	{
		m_mailGUID = info.m_mailGUID;
		m_Type = info.m_Type;
		m_State = info.m_State;
		memcpy( m_szSender , info.m_szSender , sizeof(m_szSender) );
		memcpy( m_szRecevier , info.m_szRecevier , sizeof(m_szRecevier) );
		memcpy( m_szCaption , info.m_szCaption , sizeof(m_szCaption) );
		m_SendTime = info.m_SendTime;
		m_byExtraCount = info.m_byExtraCount;
		for(BYTE i=0; i<m_byExtraCount; i++)
		{
			m_ExtraInfo[i] = info.m_ExtraInfo[i];
		}
		
		m_nContentsize = strlen( info.m_szContents );
		memcpy( m_szContents , info.m_szContents , m_nContentsize );
	}

	void 	Get( MODI_MailInfo & info )
	{
		info.m_mailGUID = m_mailGUID;
		info.m_Type = m_Type;
		info.m_State = m_State;
		memcpy( info.m_szSender , m_szSender , sizeof(info.m_szSender) );
		memcpy( info.m_szRecevier , m_szRecevier , sizeof(info.m_szRecevier) );
		memcpy( info.m_szCaption , m_szCaption , sizeof(info.m_szCaption) );
		info.m_SendTime = m_SendTime;
		info.m_byExtraCount = m_byExtraCount;
		for(BYTE i=0; i<m_byExtraCount; i++)
		{
			info.m_ExtraInfo[i] = m_ExtraInfo[i];
		}
		m_nContentsize = strlen(m_szContents);
		memcpy( info.m_szContents , m_szContents , m_nContentsize );
	}

	static const BYTE ms_SubCmd = 3;
};

// 客户端通知服务器，将一封邮件设置为以读状态
struct MODI_C2GS_Request_SetReadMail : public MODI_Mail_Cmd
{
	MODI_GUID 	m_mailid;

	MODI_C2GS_Request_SetReadMail()
	{

		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 4;
};

/// 请求删除一封邮件
struct MODI_C2GS_Request_DelMail : public MODI_Mail_Cmd
{
	MODI_GUID 	m_mailid;

	MODI_C2GS_Request_DelMail()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 5;
};

/// 删除邮件结果
struct MODI_GS2C_Notify_DelMailRes : public MODI_Mail_Cmd
{
	enum enResult
	{
		kSuccessful, // 成功
		kMailNotExist, // 不存在的邮件.客户端和服务器存在不同步 
		kUnknowError, // 删除失败，未知错误。 邮件系统异常。客户端提示下
		kMailLock, // 邮件被锁，说明某个操作正在进行中
	};

	enResult 	m_result;
	MODI_GUID 	m_mailid;

	MODI_GS2C_Notify_DelMailRes()
	{
		m_result = kSuccessful;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 6;
};

/// 客户端请求获得附件
struct MODI_C2GS_Request_GetAttachment : public MODI_Mail_Cmd
{
	MODI_GUID 	m_mailid; // 邮件id

	MODI_C2GS_Request_GetAttachment()
	{

		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 7;
};

/// 服务器回复获得附件的结果
struct MODI_GS2C_Notify_GetAttachmentRes : public MODI_Mail_Cmd
{
	enum enResult
	{
		kSuccessful,
		kMailNotExist, // 不存在的邮件.客户端和服务器存在不同步 
		kNotExistAttachment, // 不存在附件
		kGetItemFaild_BagFull, // 获得物品物品附件失败
		kGetAttachFaild, // 获得附件失败
		kUnknowError, // 未知错误。 邮件系统异常。客户端提示下
	};

	enResult 	m_ressult; // 结果
	MODI_GUID 	m_mailid; // 邮件id
	
	MODI_GS2C_Notify_GetAttachmentRes()
	{
		m_ressult = kSuccessful;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 8;
};

// 服务器通知客户端，请清理下邮箱，还有其他邮件无法进来
struct MODI_GS2C_Notify_PlzCleanMailBox : public MODI_Mail_Cmd
{
	size_t 	m_nMailCount; 	// 还有几封邮件等待进入邮箱

	MODI_GS2C_Notify_PlzCleanMailBox()
	{
		m_nMailCount = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 9;
};

#pragma pack(pop)

#endif
