/**
 * @file   c2gs_pay.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Nov 30 15:34:33 2010
 * 
 * @brief  充值命令
 * 
 * 防合并主干代码是冲突，开了新的文件
 *
 */

#ifndef _MODI_PAY_H
#define _MODI_PAY_H

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"

const BYTE MAX_TKEY_LENGTH = 21;

enum enPayResult
{
	/// 充值服务器异常
	PAY_RESLULT_NULL = 0,
	/// 认证成功，扣除金额
	PAY_RESLULT_SUCCESS = 8,
	/// 验证失败
	PAY_RESLULT_FAILED = 1,
	/// TKEY错误
	PAY_RESLULT_ERRTKEY = 2,
	/// 金额不足；
	PAY_RESLULT_NOMONEY = 3,
	/// 服务器名错
	PAY_RESLULT_NOSERVER = 4
};

#pragma pack(push,1)


/// 支付命令(后期合并注意不要和c2gs_cmddef.h里面的MAINCMD_SHOP冲突)

struct MODI_PayCmd : public stNullCmd
{
	MODI_PayCmd()
	{
        byCmd = MAINCMD_PAY;
	}
	static const BYTE m_bySCmd = MAINCMD_PAY;
};

/// 请求用平台币兑换游戏币
const BYTE MD_REQUEST_PAY_PARA = 1;
struct MODI_C2GS_Request_PayCmd : public MODI_PayCmd 
{
	MODI_C2GS_Request_PayCmd()
	{
		byParam = MD_REQUEST_PAY_PARA;
		m_wdPayMoney = 0;
		m_dwAccID = 0;
		m_client = INVAILD_CHARID;
		m_byServerid = 0;
		memset(m_cstrTKEY, 0, sizeof(m_cstrTKEY));
	}

	/// 问用户要兑换多少游戏币,最小单位1
	WORD m_wdPayMoney;
	
	/// 用户 ACCID
	DWORD m_dwAccID;

	/// 平台给的 TKEY
	char m_cstrTKEY[MAX_TKEY_LENGTH];

	/// 充值对象
	MODI_CHARID m_client;

	/// 充值的服务器
	BYTE m_byServerid;
	
	static const BYTE ms_SubCmd = MD_REQUEST_PAY_PARA;
};

/// 兑换结果
const BYTE MD_PAY_RESULT_PARA = 2;
struct MODI_GS2C_Notify_PayResult : public MODI_PayCmd
{
	MODI_GS2C_Notify_PayResult()
	{
		m_enPayResult = PAY_RESLULT_NULL;
		m_client = INVAILD_CHARID;
		m_byServerid = 0;
		byParam = MD_PAY_RESULT_PARA;
	}

	/// 结果
	enPayResult m_enPayResult;

	/// 充值对象
	MODI_CHARID m_client;

	/// 充值的服务器
	BYTE m_byServerid;
	
	static const BYTE ms_SubCmd = MD_PAY_RESULT_PARA;
};


#pragma pack(pop)

#endif

