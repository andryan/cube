/**
 * @file c2gs_Activity.h
 * @brief  活动的命令
 * @date    2011-07-12 
 *
 */


#ifndef C2GS_ACTIVITY_H__
#define C2GS_ACTIVITY_H__


#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Active_Cmd : public stNullCmd
{
	MODI_Active_Cmd()
	{
		byCmd = MAINCMD_ACTIVE;
	}
};	



// 客户端请求当前活动内容
struct MODI_C2GS_Request_Activities : public MODI_Active_Cmd
{
	MODI_C2GS_Request_Activities()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};	

struct PacketInfos
{

	WORD	m_nConfigId;
	PacketInfos()
	{
		m_nConfigId = 0;
	}
};

///	签到活动返回的有关个人的签到信息
struct MODI_GS2C_Notify_Signup : public MODI_Active_Cmd
{
	WORD		m_nYear;	///年份
	BYTE		m_nMonth;	/// 月份 1~12
	BYTE		m_nDay;	///天 1~30
	BYTE		m_tWeek;	///今天是周几，0是周日，6是周六

	DWORD	 	m_nTime;	///	今天在线时间 (单位min )
	DWORD		m_nRequire;	///	活动需要多少时间才能完成(单位S)
	WORD		m_nSecDays;	/// 完成本阶段还需要多少时间
	DWORD		m_dwSignCondition;	///签到情况，bit n表示第N天情况是否已经签到
	DWORD		m_dwSignResult;		/// 获得奖励情况 bit n 为1表示第n天获得了奖励
	BYTE		m_nPackets;		///	礼包的数量
	PacketInfos 	m_aPacketCon[];		///	礼包情况	

	MODI_GS2C_Notify_Signup()
	{
		m_nYear = 2011;
		m_nMonth = 0; 
		m_nDay = 0;
		m_tWeek = 0;
			
		m_nTime =0;
		m_nRequire  =0;
		m_nSecDays  = 0;
		m_dwSignCondition = 0;
		m_dwSignResult = 0;

		m_nPackets = 0;

		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 3;
};

///	客户端请求签到
struct MODI_C2GS_Request_Signup : public MODI_Active_Cmd
{
	MODI_C2GS_Request_Signup()
	{

		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 5;
};

///	服务器返回签到结果
struct MODI_GS2C_Notify_SignupResult : public MODI_Active_Cmd
{
	enSignUpResult   m_eRetResult;
	
	MODI_GS2C_Notify_SignupResult()
	{
		m_eRetResult = enSignUpResult_Unknown;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 7;
};


/// 	服务器通知客户端单曲列表信息
//
struct MODI_GS2C_Notify_SingleMusicList: public MODI_Active_Cmd
{
	enGameMode  m_eMode;	
	WORD	m_wNum;
	struct SingleMusicInfo   m_stMusic[0];
	MODI_GS2C_Notify_SingleMusicList()
	{
		m_wNum = 0;
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 9;
};


#pragma pack(pop)

#endif


