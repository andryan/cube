/** 
 * @file c2gs_family.h
 * @brief 家族相关数据包定义
 * @author Xu	Xiaopei
 * @version v0.3
 * @date 2011-03-17
 */

#ifndef	C2GS_FAMILY_DEFINED_H_
#define	C2GS_FAMILY_DEFINED_H_  


#include "protocol/c2gs_cmddef.h"
#include "protocol/family_def.h"
#include "nullCmd.h"

#pragma pack(push,1)

/// 家族命令
struct MODI_Family_Cmd : public stNullCmd
{
	MODI_Family_Cmd()
	{
		byCmd = MAINCMD_FAMILY;
	}
};

/// 家族名字检测
struct 	MODI_C2GS_Request_FamilyNameCheck  : public MODI_Family_Cmd
{
	char	m_szName[ FAMILY_NAME_LEN+1 ];  /// 家族名字
			
	MODI_C2GS_Request_FamilyNameCheck()
	{
		byParam = ms_SubCmd;
		memset( m_szName, 0, sizeof( m_szName) );
	}
	static	const	unsigned int 	ms_SubCmd = 1;
};

/// 客户端请求创建家族
struct MODI_C2GS_Request_CreateFamily : public MODI_Family_Cmd
{	
	char	m_szName[ FAMILY_NAME_LEN+1 ];       //家族名字
	char 	m_szXuanyan[ FAMILY_XUANYAN_LEN +1]; //家族宣言
	char 	m_szPublic [ FAMILY_PUBLIC_LEN +1 ]; //家族公告
	BYTE 	m_TotemID;			                 //图腾ID
		
	MODI_C2GS_Request_CreateFamily()
	{
		byParam = ms_SubCmd;
		m_TotemID = 0;
		memset( m_szName, 0, sizeof(m_szName) );
		memset( m_szXuanyan, 0, sizeof( m_szXuanyan) );
		memset( m_szPublic, 0 , sizeof( m_szPublic) );
	}
	
	static const unsigned int ms_SubCmd =2;
};

/// 申请加入家族
/// 取消家族申请
/// 退出家族
/// 解散家族
/// 开放家族
/// 升级家族

struct MODI_C2GS_Request_FamilyOpt: public MODI_Family_Cmd
{
	DWORD    m_dwFamilyID;	///	家族的ID,要赋值
	enFamilyOptType m_enOptType;
	
	MODI_C2GS_Request_FamilyOpt()
	{
		m_dwFamilyID = 0;
		m_enOptType = enFamilyOpt_None;
		byParam = ms_SubCmd;
	}
	
	static const unsigned int 	ms_SubCmd = 3;
};

/// 职位更改
struct MODI_C2GS_Request_FamilyModifyPosition : public MODI_Family_Cmd
{
	MODI_GUID	m_stGuid;	                ///	目标的GUID
	enFamilyPositionType	m_enType; 	/// 职位类型
	
	MODI_C2GS_Request_FamilyModifyPosition()
	{
		m_stGuid = INVAILD_GUID;
		m_enType = enPositionT_None;
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 4;
};

/// 邀请某人加入家族
struct MODI_C2GS_Request_FamilyInvite : public MODI_Family_Cmd
{
	MODI_GUID m_stGuid;                     /// 被邀请人的guid
	char 	m_szName[ROLE_NAME_MAX_LEN+1];  ///被邀请的人,先保留
	MODI_C2GS_Request_FamilyInvite()
	{
		m_stGuid = INVAILD_GUID;
		memset( m_szName,0,sizeof( m_szName));
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 5;
};

/// 告知被邀请的人，有人邀请你
struct MODI_GS2C_Notify_FamilyInvition : public MODI_Family_Cmd
{
	DWORD m_dwInvitionID;                    /// 服务器用的，记得返回
	char m_szName[ ROLE_NAME_MAX_LEN+1];	 /// 邀请者的姓名
	MODI_FamilyListInfo m_stListInfo;
	
	MODI_GS2C_Notify_FamilyInvition()
	{
		byParam = ms_SubCmd;
		m_dwInvitionID = 0;
		memset( m_szName,0,sizeof( m_szName));
	}
	
	static const unsigned int 	ms_SubCmd = 6;
};

/// 被邀请者对邀请的回复，要么拒绝，要么同意
struct MODI_C2GS_Request_InvitionResponse : public MODI_Family_Cmd
{
	DWORD m_dwInvitionID;
	enOptRequestResult m_enResult;
	
	MODI_C2GS_Request_InvitionResponse()
	{
		byParam = ms_SubCmd;
		
		m_dwInvitionID = 0;
		m_enResult = enOptRequestInValid;
	}
	
	static const unsigned int 	ms_SubCmd = 7;
};

/// 被邀请人的回复,告知邀请者
struct MODI_GS2C_Notify_FamilyInvitionResponse : public MODI_Family_Cmd
{
	char m_szName[ ROLE_NAME_MAX_LEN+1];	/// 被邀请者的姓名
	enOptRequestResult m_enResult;
	
	MODI_GS2C_Notify_FamilyInvitionResponse()
	{
		byParam = ms_SubCmd;
		m_enResult = enOptRequestInValid;
		memset( m_szName,0,sizeof( m_szName));
	}
	
	static const unsigned int 	ms_SubCmd = 8;
};

/// 对申请成员进行的操作(接收或者拒绝)
struct  MODI_C2GS_Request_FamilyOptRequest: public MODI_Family_Cmd
{
	MODI_GUID	m_stGuid;
	BYTE 		m_byOpt; /// 1接收，2拒绝
		
	MODI_C2GS_Request_FamilyOptRequest ()
	{
		m_byOpt = 0;
		m_stGuid = INVAILD_GUID;
		byParam = ms_SubCmd;
	}
	
	static const unsigned int 	ms_SubCmd = 9;
};

/// 客户端请求进入家族大厅
struct  MODI_C2GS_Request_FamilyIn:  public  MODI_Family_Cmd
{
	MODI_C2GS_Request_FamilyIn()
	{
		byParam = ms_SubCmd;
	}
			
	static const unsigned int 	ms_SubCmd = 10;
};

/// 离开家族大厅
struct MODI_C2GS_Request_FamilyOut: public MODI_Family_Cmd
{
	MODI_C2GS_Request_FamilyOut()
	{
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 11;
};

/// 进入家族，发家族列表
struct MODI_GS2C_Notify_FamilyList: public  MODI_Family_Cmd
{
	DWORD	m_dwSize;
	MODI_FamilyListInfo	m_pListInfo[0];

	MODI_GS2C_Notify_FamilyList()
	{
		m_dwSize = 0;
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 12;
};

/// 服务器通知增加一个家族列表
struct MODI_GS2C_Notify_AddFamilyList: public MODI_Family_Cmd
{
	MODI_FamilyListInfo	m_stListInfo;
	
	MODI_GS2C_Notify_AddFamilyList()
	{
		byParam = ms_SubCmd; 
	}
	static const unsigned int 	ms_SubCmd = 13;
};

/// 服务器通知减少一个家族列表
struct MODI_GS2C_Notify_DelFamilyList: public MODI_Family_Cmd
{
	DWORD m_dwFamilyID;
	
	MODI_GS2C_Notify_DelFamilyList()
	{
		m_dwFamilyID = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned int ms_SubCmd = 14;
};

/// 家族成员离开
struct MODI_GS2C_Notify_DelFamilyMem: public MODI_Family_Cmd
{
	DWORD m_dwFamilyID; /// 先保留
	MODI_GUID	m_guid;
	enDelMemInfoReason m_enReason;
	
	MODI_GS2C_Notify_DelFamilyMem()
	{
		m_enReason = enDMReason_None;
		m_dwFamilyID = 0;
		m_guid = INVAILD_GUID;
		byParam = ms_SubCmd;
	}
	
	static const unsigned int 	ms_SubCmd = 16;
};

/// 家族成员更新
struct MODI_GS2C_Notify_UpdateFamilyMem: public MODI_Family_Cmd
{
	enSendFullInfoReason m_enReason; /// 保留
	MODI_FamilyMemInfo	m_stInfo;
	MODI_GS2C_Notify_UpdateFamilyMem()
	{
		byParam = ms_SubCmd;
	}
	
	static const unsigned int 	ms_SubCmd = 17;
};

/// 发送某个家族的所有信息
struct MODI_GS2C_Notify_FamilyFullInfo: public  MODI_Family_Cmd
{
	enSendFullInfoReason m_enReason; /// 保留
	MODI_FamilyListInfo m_stListInfo;
	char   	m_cstrXuanyan[FAMILY_XUANYAN_LEN +1 ];
	char  m_cstrPublic[FAMILY_PUBLIC_LEN+1 ];
	
	DWORD m_dwMemberSize;
	MODI_FamilyMemInfo m_stMemInfo[0];
	
	MODI_GS2C_Notify_FamilyFullInfo()
	{
		m_dwMemberSize = 0;
		memset(m_cstrXuanyan, 0, sizeof(m_cstrXuanyan));
		memset(m_cstrPublic, 0, sizeof(m_cstrPublic));
		 byParam = ms_SubCmd;
	}
	
	static const unsigned int 	ms_SubCmd = 18;
};

/// 设置家族宣言
struct MODI_C2GS_Request_FamilySetXuanyan : public  MODI_Family_Cmd
{
	char   	m_cstrXuanyan[FAMILY_XUANYAN_LEN +1 ];
	MODI_C2GS_Request_FamilySetXuanyan()
	{
		memset( m_cstrXuanyan, 0, sizeof( m_cstrXuanyan));
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 19;
};

/// 设置家族公告
struct MODI_C2GS_Request_FamilySetPublic : public MODI_Family_Cmd
{
	char  m_cstrPublic[FAMILY_PUBLIC_LEN+1 ];
	MODI_C2GS_Request_FamilySetPublic()
	{
		memset( m_cstrPublic,0,sizeof(m_cstrPublic));
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 20;
};

/// 访问家族
struct MODI_C2GS_Request_VisitFamily : public MODI_Family_Cmd
{
	DWORD		m_nFamilyid;	
	MODI_C2GS_Request_VisitFamily()
	{
		m_nFamilyid=0;
		byParam = ms_SubCmd;
	}
	static const unsigned int 	ms_SubCmd = 21;
};


/// 家族名片
struct MODI_GS2C_Notify_FamilyCard: public MODI_Family_Cmd
{
	WORD m_wdSize;
	MODI_FamilyCard m_stCard[0];
	
	MODI_GS2C_Notify_FamilyCard()
	{
		m_wdSize = 0;
		byParam = ms_SubCmd;
	}

	static const unsigned int ms_SubCmd = 22;
};


/// 服务器返回家族操作相关结果
struct MODI_GS2C_Return_FamilyOpt_Result: public MODI_Family_Cmd
{
	enFamilyOptResult m_enResult;
	MODI_GS2C_Return_FamilyOpt_Result()
	{
		byParam = ms_SubCmd;
		m_enResult = enFamilyOptResult_None;
	}

	static const unsigned int ms_SubCmd =253;
};


#pragma pack(pop)

#endif


