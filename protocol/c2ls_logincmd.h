/** 
 * @file c2ls_logincmd.h
 * @brief 登入游戏的相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2LS_LOGINCMD_DEFINED_H_
#define C2LS_LOGINCMD_DEFINED_H_

#include "protocol/cube_version.h"
#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"

#pragma pack(push,1)

struct MODI_C2LS_Cmd : public stNullCmd
{
	MODI_C2LS_Cmd()
	{
		byCmd = MAINCMD_LOGIN;
	}
};

/// 请求登入(正式版) <= 1.8.0
struct MODI_C2LS_Request_Login_Pre180 : public MODI_C2LS_Cmd
{
	modi_version_t version; // 版本信息
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; // 帐号
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1];    // 密码,注意。这里不是填充字符串

	MODI_C2LS_Request_Login_Pre180()
	{
		byParam = ms_SubCmd;

		memset( m_szAccountName , 0 , sizeof(m_szAccountName) );
		memset( m_szAccountPwd , 0 , sizeof(m_szAccountPwd) );
	}
	static const BYTE 	ms_SubCmd = 2;
};


/// 请求登入(正式版)
struct MODI_C2LS_Request_Login : public MODI_C2LS_Cmd 
{
	modi_version_t version; // 版本信息
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; // 帐号
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1];    // 密码,注意。这里不是填充字符串
	char 	m_pMacAddr[MAC_ADDR_LEN];

	MODI_C2LS_Request_Login()
	{
		byParam = ms_SubCmd;

		memset( m_szAccountName , 0 , sizeof(m_szAccountName) );
		memset( m_szAccountPwd , 0 , sizeof(m_szAccountPwd) );
		memset( m_pMacAddr , 0 , sizeof(m_pMacAddr) );
	}
	static const BYTE 	ms_SubCmd = 2;
};


// 返回角色列表给客户端
struct MODI_LS2C_Notify_Charlist : public MODI_C2LS_Cmd
{
	BYTE  m_nRoleCount; // 角色个数
	MODI_CharInfo  m_pChars[0]; // 具体每个角色的信息

	MODI_LS2C_Notify_Charlist()
	{
		byParam = ms_SubCmd;
		m_nRoleCount = 0;
	}
	static const BYTE 	ms_SubCmd = 3;
};

/// 服务器下发频道列表
struct MODI_LS2C_Notify_Channellist : public MODI_C2LS_Cmd
{
	// 描述了刷频道列表的原因
	enum Reason
	{
		kBegin = 0,
		kLogin, // 登入成功，第一次给予的频道列表
		kRechangeChannel, // 重选频道，第一此给予的频道列表
		kChannelFull, // 频道满，无法进入,重刷频道列表
		kChannelNotExist, // 所选频道不存在，无法进入,重刷频道列表
		kEnd,
	};

	Reason 	m_nReason; // 刷频道列表的原因
	BYTE 	m_nCount;  // 有几个频道
	MODI_ServerStatus 	m_pData[0]; // 每个频道的状态

	MODI_LS2C_Notify_Channellist()
	{
		byParam = ms_SubCmd;
		m_nCount = 0;
	}

	static const BYTE 	ms_SubCmd = 4;
};

/// 客户端请求选择一个频道
struct MODI_C2LS_Request_SelChannel : public MODI_C2LS_Cmd
{
	defServerChannelID m_nChannelID; // 频道ID
	char m_szNetType[8]; //网络类型(dx,lt,yxt.....)
	
	MODI_C2LS_Request_SelChannel()
	{
		byParam = ms_SubCmd;
		m_nChannelID = INVAILD_SVRCHANNELID;
		memset(m_szNetType, 0, sizeof(m_szNetType));
	}

	static const BYTE ms_SubCmd = 5;
};

/// 客户端选择频道后，服务器下发网关地址
struct MODI_LS2C_Notify_GatewayInfo : public MODI_C2LS_Cmd
{
	DWORD 	m_nIP; // 网关IP
	WORD 	m_nPort; //  网关端口
	MODI_LoginKey 	m_Key; // 登入用的KEY 
	defAccountID 	m_nAccountID; // 帐号ID

	MODI_LS2C_Notify_GatewayInfo() 
	{
		byParam = ms_SubCmd;
		m_nIP = 0;
		m_nPort = 0;
		m_nAccountID = INVAILD_ACCOUNT_ID;
	}

	static const BYTE ms_SubCmd = 6;
};


/// 客户端请求进入游戏，给的验证
struct MODI_C2LS_Request_Auth : public MODI_C2LS_Cmd
{
	MODI_LoginPassport 	m_Passport;
	MODI_C2LS_Request_Auth()
	{
		byParam = ms_SubCmd;
	}	

	static const BYTE ms_SubCmd = 7;
};

/// 客户端请求切换频道
struct MODI_C2LS_Request_ChangeServerChannel : public MODI_C2LS_Cmd
{
	MODI_C2LS_Request_ChangeServerChannel()
	{
		byParam = ms_SubCmd;
	}	

	static const BYTE ms_SubCmd = 8;
};




#pragma pack(pop)

#endif
