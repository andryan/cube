/**
 * @file   c2yyls_cmd.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Sep 21 14:10:22 2011
 * 
 * @brief  client to yyloginservice command
 * 
 * 
 */

#ifndef _MD_CYYLS_CMD_H_
#define _MD_CYYLS_CMD_H_

#include "protocol/cube_version.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"

#pragma pack(push, 1)

/// 版本信息记录
#define MODI_CUBE_YYLOGIN_VERSION       1

struct MODI_C2YYLS_Cmd: public stNullCmd
{
	MODI_C2YYLS_Cmd()
	{
		byCmd = 1;
	}
};


/// 请求登陆
struct MODI_C2YYLS_Request_Login: public MODI_C2YYLS_Cmd
{
	MODI_C2YYLS_Request_Login()
	{
		byParam = ms_SubCmd;
	}
	
	/// 版本信息
	modi_version_t yyls_version;
	
	static const BYTE ms_SubCmd = 1;
};

/// 返回请求登陆key
struct MODI_YYLS2C_Return_LoginKey: public MODI_C2YYLS_Cmd
{

	MODI_YYLS2C_Return_LoginKey()
	{
		byParam = ms_SubCmd;
		m_dwKeyLen = 0;
	}

	DWORD m_dwKeyLen;
	char m_pKey[0];
	
	static const BYTE ms_SubCmd = 2;
};


/// 返回YY服务器登陆信息
struct MODI_C2YYLS_Return_LoginInfo: public MODI_C2YYLS_Cmd
{

	MODI_C2YYLS_Return_LoginInfo()
	{
		byParam = ms_SubCmd;
		m_dwInfoLen = 0;
	}
	
	DWORD m_dwInfoLen;
	char m_pInfo[0];
	
	static const BYTE ms_SubCmd = 3;
};

/// 登陆结果
struct MODI_YYLS2C_Return_LoginResult: public MODI_C2YYLS_Cmd
{

	MODI_YYLS2C_Return_LoginResult()
	{
		byParam = ms_SubCmd;
		m_iResult = 0;
		m_dwUserId = 0;
	}
	/// 结果
	int m_iResult;
	DWORD m_dwUserId;
	static const BYTE ms_SubCmd = 4;
};

#pragma pack(pop)
#endif
