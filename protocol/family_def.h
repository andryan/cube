/**
 * @file   family_def.h
 * @author  <hurixin@localhost.localdomain>
 * @date   Wed May 11 16:21:40 2011
 * 
 * @brief  家族相关定义
 * 
 * 
 */

#ifndef	FAMILY_DEFINE_H_
#define FAMILY_DEFINE_H_

#include "gamedefine.h"

#pragma pack(push,1)

/// 家族id
typedef DWORD defFamilyID;

/// 非法家族
#define INVAILD_FAMILY_ID 0

/// 创建家族所需游戏币
#define CREATE_FAMILY_REQ_MONEY	500

/// 创建家族所需级别
#define CREATE_FAMILY_REQ_LEVEL	10

/// 家族最多人数
#define MAX_FAMILY_MEM_NUM	1000

/// 家族最多申请人数
#define MAX_FAMILY_REQUEST_NUM	24

/// 家族名字最小
#define FAMILY_NAME_LESS 4

/// 家族宣言最大长度
#define FAMILY_XUANYAN_LEN 60

/// 家族留意
#define FAMILY_PUBLIC_LEN 450

/// 家族升级
namespace nsFamilyLevel
{
	/// 升级人气
	const DWORD LEVEL_1_RENQI = 10000;
	const DWORD LEVEL_2_RENQI = 30000;
	const DWORD LEVEL_3_RENQI = 60000;
	const DWORD LEVEL_4_RENQI = 100000;
	const DWORD LEVEL_5_RENQI = 150000;
	const DWORD LEVEL_6_RENQI = 210000;
	const DWORD LEVEL_7_RENQI = 280000;
	const DWORD LEVEL_8_RENQI = 360000;
	const DWORD LEVEL_9_RENQI = 450000;

	/// 升级花费
	const DWORD LEVEL_MONEY = 10000;
	const WORD MAX_FAMILY_LEVEL = 10;
};


/// 删除某个成员信息的理由
enum enDelMemInfoReason
{
	enDMReason_None = 0,
	enDMReason_Cancel,        	/// 取消申请
	enDMReason_Fire,  		  	/// 被人开除
	enDMReason_Leave,  			/// 离开家族
	enDMReason_Free, 			/// 家族解散
	enDMReason_Refuse, 			/// 被拒绝
	enDMReason_End,				
};

/// 发送某个家族的全部信息的原因
enum enSendFullInfoReason
{
	enReason_None = 0,
	enReason_Login,        /// 登陆游戏
	enReason_LoginFamily,  /// 进入家族
	enReason_CreateFamily, /// 创建家族
	enReason_RequestFamily, /// 请求家族
	enReason_JoinFamily,   /// 加入家族
	enReason_VisitFamily,  /// 访问家族
	enReason_ModiBase,   ///  修改资料
	enReason_ChangeMaster, /// 家族族长变更
	enReason_ModiPublic, /// 修改公告成功
	enReason_ModiXuanyan, /// 修改宣言成功
	enReason_Levelup, 	  /// 家族升级成功
	enReason_End,
};


/// 个人对家族相关操作类型
enum enFamilyOptType
{
	enFamilyOpt_None =0,
	enFamilyOpt_Request,   /// 申请加入家族
	enFamilyOpt_Cancel,    /// 取消申请
	enFamilyOpt_Leave,	   /// 退出家族
	enFamilyOpt_Free,	   /// 解散家族
	enFamilyOpt_Open,	   /// 开放家族
	enFamilyOpt_Close,     /// 关闭家族信息
	enFamilyOpt_Level, 	 	/// 升级家族
	enFamilyOpt_End,
};


/// 家族操作返回结果
enum enFamilyOptResult
{
	enFamilyOptResult_None = 0,  /// 没结果
	enFamilyOptResult_Unknow,	 /// 系统错误

	/// 创建结果
	enFamilyRetT_Create_Money,   /// 金钱不足
	enFamilyRetT_Create_Level,	 /// 级别不足
	enFamilyRetT_Create_Totem,	 /// 没有设置图腾
	enFamilyRetT_Create_Name,	 /// 家族名字有问题
	enFamilyRetT_Create_Xuanyan, /// 家族宣言有问题
	enFamilyRetT_Create_Public,	 /// 家族公告有问题
	enFamilyRetT_Create_HaveName,/// 名字已经存在
	enFamilyRetT_Create_Time,	 /// 24小时内不能再创建家族
	enFamilyRetT_All_Request,    /// 已经申请过别人的家族
	enFamilyRetT_All_Have,       /// 已经有了家族

	/// 名字检查
	enFamilyRetT_CheckN_Failed,  /// 名字不合法
	enFamilyRetT_CheckN_Have,    /// 名字已经存在
	enFamilyRetT_CheckN_Succ,    /// 名字可以使用

	/// 申请结果
	enFamilyRequestRetT_Ok,      /// 申请成功
	enFamilyRetT_Request_Num,	 /// 申请人数已满
	enFamilyRetT_Request_Family, /// 申请人已经有了家族
	enFamilyRetT_Request_Time,	 /// 申请人24小时内不能申请
	enFamilyRetT_Request_frequ,	 /// 申请人已经申请过了
	enFamilyRetT_Request_No, 	 /// 家族不存在了
	
	enFamilyRetT_Request_Refuse, /// 族长拒绝加入
	enFamilyRetT_OnFire, 		 /// 被族长开除了

	/// 取消申请结果
	enFamilyRetT_Cancel_Succ,   /// 取消成功

	/// 退出家族
	enFamilyRetT_Level_Succ,    /// 退出成功

	/// 开放家族
	enFamilyRetT_Open_Succ,    /// 开发成功
	enFamilyRetT_Close_Succ,   /// 关闭成功

	/// 解散结果
	enFamilyFreeRetT_Ok,         /// 解散成功
	enFamilyFreeT_Request,		/// 告知申请成员
	enFamilyRetT_Free_Member,	 /// 家族内部还有其他成员
	enFamilyRetT_Free_Auth,		 /// 没有权限

	/// 调整职位返回结果
	enFamilyPositionRetT_Ok,    // 成功
	enFamilyRetT_Position_Auth,	//没有权限
	enFamilyRetT_Position_Num,	//对应职位的人数已经满了

	/// 邀请结果
	enFamilyInviteRet_Offline,	//对方不在线
	enFamilyInviteRet_Family,	//对方已经有了家族

	/// 访问家族
	enVisitFamily_NoExit,  		/// 家族不存在
	enVisitFamily_NoOpen, 		/// 家族不开放

	/// 家族升级
	enLevelFamily_Level, 		/// 最高等级
	enLevelFamily_NoAuth,		/// 权限不够
	enLevelFamily_NoRenqi,		/// 人气不够
	enLevelFamily_NoMoney,		/// 钱不够
	enLevelFamily_Succ, 		/// 家族升级成功
	enLevelFamily_Error,		/// 未知错误
};


/// 成员状态
enum  enFamilyMemStateType
{
	enMemStateT_None,           /// 无状态
	enMemStateT_Offline,		/// 离线
	enMemStateT_Free,		    /// 空闲
	enMemStateT_Playing,		/// 游戏中
	enMemStateT_End,
};


/// 家族成员数据
struct MODI_FamilyMemInfo
{
	DWORD m_dwFamilyID;     	/// 家族id
	MODI_GUID	m_stGuid;		/// 成员的GUID
	char		m_szName[ ROLE_NAME_MAX_LEN + 1]; //成员昵称
	BYTE        m_bySex;		///	成员性别
	enFamilyPositionType	m_enPosition;	/// 成员的职位
	enFamilyMemStateType	m_enState;		/// 成员的状态
	DWORD 		m_dwContribute;				/// 贡献值
	DWORD 		m_dwRenqi;	                /// 人气
	WORD		m_wdChenghao;               /// 称号
    time_t		m_stLastlogin;				/// 上次登录时间
	
	MODI_FamilyMemInfo ()
	{
		m_dwFamilyID = INVAILD_FAMILY_ID;
		m_stGuid = INVAILD_GUID;
		memset(m_szName , 0, sizeof( m_szName) );
		m_bySex = 0;
		m_enState = enMemStateT_Offline;
		m_enPosition = enPositionT_Normal;
		m_dwContribute = 0;
		m_dwRenqi = 0;
		m_wdChenghao = 0;
		m_stLastlogin = 0;
	}			
};


/// 家族列表信息
struct MODI_FamilyListInfo
{
	DWORD m_dwFamilyID;			          /// 家族的ID
	char m_szName[ FAMILY_NAME_LEN+1 ];   ///家族的名字
	WORD m_wdLevel;		                  ///家族级别
	WORD m_wdMemnum;		//家族成员总数
	DWORD 	m_dwRenqi;		//家族的人气总值
	DWORD 	m_dwMoney;		//家族财富总值

	MODI_FamilyListInfo ()
	{
		m_dwFamilyID = 0;
		memset( m_szName, 0, sizeof( m_szName) );
		m_wdLevel = 0;
		m_wdMemnum =0;
		m_dwRenqi = 0;
		m_dwMoney = 0;
	}
};

#pragma pack(pop)

#endif









