/** 
 * @file gamedefine.h
 * @brief 游戏相关结构，常量等等定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_GAMEDEFINE_H_____
#define C2GS_GAMEDEFINE_H_____

#include "modi_guid.h"
#include "stdefhelp.h"
#include <string>
#include <assert.h>


// 编解码 guid 
#define GUID_HIPART(x)   (DWORD)((x) >> 32)
#define GUID_LOPART(x)   (DWORD)((x) & 0xFFFFFFFFULL)
#define MAKE_GUID(l, h)  QWORD( DWORD(l) | ( QWORD(h) << 32 ) )

#define UINT64_HIPART(x) 	GUID_HIPART(x)   
#define UINT64_LOPART(x) 	GUID_LOPART(x) 
#define MAKE_UINT64(l, h)  	MAKE_GUID(l,h)
#define	VIP_EXP		120
#define VIP_MONEY	120

// 游戏角色ID类型
typedef DWORD 	MODI_CHARID;

typedef DWORD  defAccountID;

//	房间ID
typedef BYTE defRoomID;

//	音乐ID
typedef DWORD defMusicID;

//	地图ID
typedef WORD defMapID;

//	房间类型
typedef BYTE defRoomType;

//	房间位置状态掩码
typedef QWORD defRoomStateMask;

//	房间更新掩码
typedef DWORD defRoomUpdateMask;

//	房间中的位置索引
typedef WORD defRoomPosSolt;

//	城市ID
typedef WORD defCityID;

//	avatar 部件ID
typedef WORD defAvatarID;

// 游戏频道ID
typedef BYTE  defServerChannelID;

//  音乐文件的文件头最大字节数
#define MAX_MUSICFILE_HEADER (1024 * 32)

//  最大房间个数
#define MAX_ROOM_COUNT      254

// 	单人房间ID号
#define SINGLE_ROOM_ID 		255

///	每个礼包中最多有10个物品
#define MAX_ITEMS_PER_PACKET	10
//  无效的房间ID
#define INVAILD_ROOM_ID     0

//  无效的房间类型
#define INVAILD_ROOM_TYPE   0

///	礼包的图片名字的长度

#define MAX_PACKT_IMAGE_NAME_LEN 80
#define PACK_DESC_LEN		240
//	房间名字最大长度
#define ROOM_NAME_MAX_LEN			24

///	礼包名字的最大长度
#define	PACK_NAME_MAX_LEN			30

//	房间密码最大长度
#define ROOM_PWD_MAX_LEN			8

//	角色名字最大长度
#define ROLE_NAME_MAX_LEN			16

//  角色名字最小长度
#define ROLE_NAME_MIN_LEN   		4

//	 email地址最大长度
#define EMAIL_MAX_LEN				32

//  聊天数据最大字节数
#define CHATMSG_MAX_LEN				180

//  私聊长度
#define PRIVATE_CHATMSG_MAX_LEN 	1200

/// 普通房间
#define NORMAL_PLAYSLOT_INROOM 6
#define NORMAL_SPECTATORSLOT_INROOM 12

/// vip房间
#define VIP1_PLAYSLOT_INROOM 6
#define VIP1_SPECTATORSLOT_INROOM 36

#define VIP2_PLAYSLOT_INROOM 6
#define VIP2_SPECTATORSLOT_INROOM 66

#define MAX_PLAYSLOT_INROOM 		6

#define MAX_SPECTATORSLOT_INROOM 	100


// 游戏中邮件标题大最数
#define MAIL_CAPTION_MAX_LEN 		30

// 游戏中邮件内容最大数
#define MAIL_CONTENTS_MAX_LEN 		300

// 游戏中最大可以接收的邮件个数
#define MAILLIST_MAX_COUNT 	40

//  无效的角色ID
#define INVAILD_CHARID  0

//  无效的城市ID
#define INVAILD_CITYID  0

//  无效的工会ID


// 无效的游戏频道ID
#define INVAILD_SVRCHANNELID 0

// 无效的位置插槽
#define INVAILD_POSSOLT  0xFFFF

//  随机音乐ID
#define RANDOM_MUSIC_ID 0
//  随机场景ID
#define RANDOM_MAP_ID 0

//  最大等级
#define MAX_LEVEL 70

//  一局游戏产生SHOW TIME 的最大次数
#define MAX_SHOWTIMECOUNT_PER_MATCH 	1

//  一局游戏产生FERVER TIME 的最大次数
#define MAX_FERVERTIMECOUNT_PER_MATCH 	0xFFFFFFFF


// 无效的帐号ID
#define INVAILD_ACCOUNT_ID 0

// 帐号的最大长度
#define MAX_ACCOUNT_LEN 	32

// 帐号的最小长度
#define MIN_ACCOUNT_LEN 	4

// 帐号密码长度
#define ACCOUNT_PWD_LEN 	32

// MAC Address Length
#define MAC_ADDR_LEN		6

// 个性签名
#define MAX_PERSONAL_SIGN 	150

// 服务器名字
#define MAX_SERVERNAME_LEN 	64

// 无效的表格ID
#define INVAILD_CONFIGID 	0

// 无效的背包位置
#define INVAILD_POS_INBAG 	0

// 频道最大人数
#define GAME_CHANNEL_MAX_CLIENT 	200

// 单个物品拥有个数无限制
#define ITEM_OWN_UNLIMITED 	65535

// 某类型物品拥有无限制
#define ITEM_SUBOWN_UNLIMITED 65535

// 表情包中表情最大数
#define EXPRESS_INDEX_COUNT  8

// 表情索引有效范围
#define EXPRESS_INDEX_BEGIN  1

#define EXPRESS_INDEX_END  8

// 表情使用时间间隔
#define EXPRESS_PLAY_TIME 	3 * 1000

#define SYS_MAIL_SENDER 	"SystemMail"

// 最大金币数量
#define MAX_MONEY 			1000000000

// 最但RMB数量
#define MAX_RMBMONEY 		100000000

// 运营时用的CDKEY 
#define YUNYING_CDKEY_MAXSIZE 	128

/// 大地图格子的像素大小
#define GRID_DIMENSION 50
/// 移动命令时间100ms
#define MOVECMD_INTERVAL 100

/// 道具名字最
const DWORD MAX_ITEM_NAME = 256;

const BYTE MAX_CARDNUM_LENGTH = 19;

// 用秒来表示时间的枚举定义
enum
{
	enOneHour = 60 * 60,
	enOneDay = 24 * enOneHour,
	enOneWeak = 7 * enOneDay,
	enHalfMonth = 15 * enOneDay,
	enOneMonth = 30 * enOneDay,
	enThreeMonth = 3 * enOneMonth,
	enForever = 0,
};

// 游戏状态
enum kGameState
{
	kOutServer, // 已经登入，但是不在游戏频道中（如在选择频道界面)
	kInRoomWaiting, // 在房间中等待
	kInRoomPlaying, // 在房间中游戏
	kInLobby, 		// 在大厅中
	kInSingleRoom, // 单人游戏中
};

// 对象类型
enum OBJTYPE
{
	enObjType_Player = 0,// 玩家
	enObjType_Item = 1 ,// 道具
	enObjType_Impact = 2, // 效果对象
	enObjType_Mail = 4, // 邮件对象
	enObjType_Npc = 255, // npc
};

struct tagFakeHighID
{
	char type;
	char szTemp[3];
};

inline OBJTYPE 	GetObjectType( const MODI_GUID & id )
{
	tagFakeHighID temp;
	DWORD h = GUID_HIPART(id);
	memcpy(&temp , &h , sizeof(h) );
	return OBJTYPE(temp.type);
}

//	房间类型
enum ROOM_TYPE
{
    ROOM_TYPE_BEGIN = 0, 
	ROOM_TYPE_NORMAL,  	// 普通房间
	ROOM_TYPE_SINGLE,  	// 单人房间
	ROOM_TYPE_BIG,   	// 30人大房间
	ROOM_TYPE_BIG_PRESENTER,  /// 	抢麦类型的房间
	ROOM_TYPE_VIP, 		///vip房间类型
	ROOM_TYPE_END,
};

// 	房间子类型，游戏模式类型
enum ROOM_SUB_TYPE
{
    ROOM_STYPE_BEGIN = 0, 
	ROOM_STYPE_NORMAL, // 最普通的模式，就是当前的这种玩法
	ROOM_STYPE_NORMAL_PROP, // 普通道具模式
	ROOM_STYPE_KEYBOARD, // 键盘模式
	ROOM_STYPE_KEYBOARD_PROP, // 键盘道具模式
	ROOM_STYPE_TEAM,
	ROOM_STYPE_SINGLE_2KEY,
	ROOM_STYPE_KEYBOARD_TEAM, // 组队键盘模式
	ROOM_STYPE_END,
};

//  队伍类型
enum ROOM_TEAM_TYPE
{
	ROOM_TEAM_BEGIN = 0,
	ROOM_TEAM_RED,
	ROOM_TEAM_YELLOW,
	ROOM_TEAM_BLUE,
	ROOM_TEAM_END
};

//	聊天频道
enum
{
    enChatChannel_Current, //	当前聊天
    enChatChannel_Team, //	小组
    enChatChannel_Channel,// 频道
    enChatChannel_Single,// 私聊
	enChatChannel_Feizi, // 游戏内会飞的字
	enChatChannel_World, // 世界聊天
	enChatChannel_Fensi, // 粉丝聊天
};

/// 房间更新掩码
enum
{
	enRoomUP_Name = 1 << 0,
    enRoomUP_MusicID = 1 << 1,
    enRoomUP_MapID = 1 << 2,
    enRoomUP_PosState = 1 << 3,
	enRoomUP_RoomType = 1 << 4,
	enRoomUP_IsPlaying = 1 << 5,
};

/// 性别
enum 
{
    enSexBoy = 1, 
	enSexGril = 0,
	enSexAll = 2,
};

/// 离开房间的原因
enum
{
    enLeaveReason_Kick = 1, //  被踢
    enLeaveReason_Initiative, //自己主动离开
    enLeaveReason_GameStartTimeout,//  因为游戏开始后，一定时间内未发送准备就绪消息，而被踢出房间
    enLeaveReason_ExitGame,//退出游戏的离开
	enLeaveReason_AnotherLogin, // 另外一个帐号在其他地方登入，被踢
	enLeaveReason_RechangeChannel, // 选择另外一个频道
	enLeaveReason_Chenghao, //称号不能做房主，房间解散
	enLeaveReason_Vip, /// vip离开房间了,你不是vip
};

/// 得分类型
enum
{
	enScore_Begin = 0,
	enScore_Pefect,
	enScore_Cool,
	enScore_Good,
	enScore_Bad,
	enScore_Miss,
	enScore_End,
};

/// 评价类型
enum
{
	enEval_Begin = 0,
	enEval_F_Type = 1,
	enEval_E_Type = 2,
	enEval_D_Type = 3,
	enEval_C_Type = 4,
	enEval_B_Type = 5,
	enEval_ADEC_Type = 6,
	enEval_A_Type = 7,
	enEval_SDEC_Type = 8,
	enEval_S_Type = 9,
	enEval_SS_Type = 10,
	enEval_End = 11,
};

/// gameplay中的效果类型(注意位操作，有可能几种效果同时出现）
enum
{
	enEffect_None = 0, // 没有特殊效果
	enEffect_ShowTime = 1, // show time 
	enEffect_FerverTime = 2, // ferver time
};

// 血型
enum
{
	enXuexingBegin,
	enAXingXue,
	enBXingXue,
	enABXingXue,
	enOXingXue,
	enXuexingEnd,
};


// 场景类型定义
enum SceneType
{
	kGameScene = 0,  	// 	游戏场景
	kWaittingSecne, 	// 	等待房间场景
	kTrainSecne, 		// 	练歌
	kSystemScene, 		// 	系统场景
};

// 物品使用结果
enum enUseItemResult
{
	kUseItem_Ok = 0, // 成功
	kUseItem_CanNot_Use, // 无法使用
	kUseItem_Level_Fail, // 等级错误
	kUseItem_Sex_Fail, // 性别错误
	kUseItem_Type_Fail, // 类型错误
	kUseItem_TargetType_Fail, // 目标类型错误
	kUseItem_Target_NotExist, // 目标不存在
};

// 效果类型
enum enImpactType
{
	kImpactOnce, // 一次性效果（直接效果)
	kImpactContinue, // 持续性效果
};

// 道具在哪种游戏状态能够使用
enum enItemCanUseInGameState
{
	kItemCanUse_AllGameState= 0,  	// 都可以
	kItemCanUse_OutGame = 1,  		// 不在游戏中时
	kItemCanUse_InRoom = 2, 		// 在房间中
	kItemCanUse_InPlaying = 3, 		// 在游戏中
};

// 道具在何种模式中能够使用
enum enItemCanUseMode
{
	kItemCanUse_AllMode = 0, 		// 所有游戏模式都能使用
	kItemCanUse_ItemMode = 1, 		// 	道具模式
	kItemCanUse_NormalMode = 2, 	// 	普通模式
};

// 物品能够使用的目标类型
enum enItemCanUseTargetType
{
	kItemCanUse_TargetAll = 0, // 所有目标
	kItemCanUse_OnlySelf = 1, // 只能是自己
	kItemCanUse_ItemParent = 2, // 队友
};

// 移动标志
enum enMovementFlags
{
	kMoveMentFlag_None = 0,
	kMoveMentFlag_Walk = 1,
	kMoveMentFlag_Jumping = 2,
	kMoveMentFlag_Falling = 4,
	kMoveMentFlag_Turn = 8,
	kMoveMentFlag_Back = 16,
};

enum enActionReq
{
	enActionReq_None,
	enActionReq_Fly,
	enActionReq_Suit
};

//	键盘模式按键声音的类型
enum	enKeyDownVolType
{
	enKeyDownVol_None = 0,
	enKeyDownVol_Dagu,
	enKeyDownVol_Gedou,
	enKeyDownVol_Qiang,
	enKeyDownVol_End,
};

namespace RoomHelpFuns
{

/**
 * @brief 生成房间类型字段
 *   bool bPwd  是否有密码
 *   BYTE ucMainType   指定房间的主类型
 *   BYTE ucSubType    指定房间的子类型
 *      成功返回非 INVAILD_ROOM_TYPE 的值
 */
inline defRoomType MakeRoomType(bool bPwd, BYTE ucMainType, BYTE ucSubType)
{
    defRoomType ret = INVAILD_ROOM_TYPE;

#ifdef _DEBUG
    assert( ROOM_TYPE_BEGIN != ROOM_TYPE_END );
    assert( ucMainType > ROOM_TYPE_BEGIN  ); //  must > 0
    assert( ucMainType < ROOM_TYPE_END ); //  must be 1-7
#endif

    // higest 1:bit is has pwd?
    // 3:bit main type
	// 4:bit sub type
    if (ucMainType > ROOM_TYPE_BEGIN && ucMainType < ROOM_TYPE_END)
    {
        if (bPwd)
        {
            ret |= 0x80;
        }
        ret |= ((ucMainType&0x07)<<4);

        //add subtype?fuck tt!
		ret |= (ucSubType&0x0F);
    }
    return ret;
}

/**
 * @brief 判断房间是否有密码
 *   defRoomType type           房间信息中的 <房间类型> 字段
 *      有密码返回true
 */
inline bool IsHasPassword(defRoomType type)
{
    return type & 0x80 ? true : false;
}

/**
 * @brief 获得房间的主类型
 *   defRoomType type           房间信息中的 <房间类型> 字段
 *      返回房间的主类型
 */
inline BYTE GetRoomMainType(defRoomType type)
{
    BYTE ucMainType = (type & 0x70)>>4;
    return ucMainType;
}

inline BYTE GetRoomSubType(defRoomType type)
{
	BYTE ucSubType = (type & 0x0F);
	return ucSubType;
}

inline void SetHasPassword(defRoomType & type , bool bSet)
{
	if( bSet )
	{
		type |= 0x80;
	}
	else 
	{
		type &= ~0x80;
	}
	
}


/**
 * @brief 编码房间状态字段
 *   BYTE nMaxPlay 房间最大参战玩家个数
 *   BYTE nPlayNum 房间当前参战玩家个数
 *   BYTE nMaxSpectatorNum 房间最大观战玩家个数
 *   BYTE nSpectatorNum 房间当前观战玩家个数
 *    BYTE nBoys 房间当前男孩个数
 *   bool bOffSpectator 房间是否允许观战
 *      返回房间的状态掩码
 */
inline defRoomStateMask EncodeRoomState(BYTE nMaxPlay, BYTE nPlayNum,
        BYTE nMaxSpectatorNum, BYTE nSpectatorNum, BYTE nBoys,
        bool bOffSpectator)
{
#ifdef _DEBUG
    assert( nPlayNum <= nMaxPlay );
    assert( nSpectatorNum <= nMaxSpectatorNum );
#endif
#if 0
	// 1byte 最大玩家个数 + 当前玩家个数
	// 2byte 最大旁观个数 ＋ 当前旁观个数
	// 1byte 玩家性别信息
	// 1bit 是否允许观战
    defRoomStateMask ret = 0;
	BYTE ucPlayMask = nMaxPlay;
	ucPlayMask <<= 4;
	ucPlayMask |= nPlayNum;
	DWORD  nSpectatorMask = nMaxSpectatorNum;
	nSpectatorMask <<= 8;
	nSpectatorMask |= nSpectatorNum;
	nSpectatorMask <<= 8;
	DWORD nSexMask = nBoys;
	nSexMask <<= 24;

    if (bOffSpectator)
    {
        ret |= 0x8000000000000000;
    }
	ret |= ucPlayMask;
	ret |= nSpectatorMask;
	ret |= nSexMask;
#endif
	defRoomStateMask ret = 0;
	defRoomStateMask max_a_player = 0;
	defRoomStateMask max_player = 0;
	defRoomStateMask max_a_sp = 0;
	defRoomStateMask max_sp = 0;
	defRoomStateMask max_boy = 0;
	defRoomStateMask is_s = 0;

	max_a_player = nMaxPlay;
	max_player = nPlayNum;
	max_player <<= 8;
	max_a_sp = nMaxSpectatorNum;
	max_a_sp <<= 16;
	max_sp = nSpectatorNum;
	max_sp <<= 24;
	max_boy = nBoys;
	max_boy <<= 32;
	is_s = bOffSpectator;
	is_s <<= 40;
	
	ret |= max_a_player;
	ret |= max_player;
	ret |= max_a_sp;
	ret |= max_sp;
	ret |= max_boy;
	ret |= is_s;
	
    return ret;
}
 
/**
 * @brief 编码房间状态字段
 *   defRoomStateMask  stateMask 状态掩码
 *   unsigned & char nMaxPlay 房间最大参战玩家个数（输出参数)
 *   unsigned & char nPlayNum 房间当前参战玩家个数（输出参数)
 *   unsigned & char nMaxSpectatorNum 房间最大观战玩家个数（输出参数)
 *   unsigned & char nSpectatorNum 房间当前观战玩家个数（输出参数)
 *   unsigned & char nBoys 房间当前男孩个数（输出参数)
 *   bool & bOffSpectator 房间是否允许观战（输出参数)
 */
inline void DecodeRoomState(defRoomStateMask stateMask, BYTE & nMaxPlay,
        BYTE & nPlayNum, BYTE & nMaxSpectatorNum, BYTE & nSpectatorNum,
        BYTE & nBoys, bool & bOffSpectator)
{
#if 0	
    bOffSpectator = stateMask & 0x8000000000000000 ? true : false;
	BYTE ucPlayMask = stateMask & 0x000000FF;
	DWORD  nSpectatorMask = stateMask & 0x00FFFF00;
	DWORD  nSexMask = stateMask & 0xFF000000;
	
	nMaxPlay = ucPlayMask;
	nMaxPlay >>= 4;
	nPlayNum = ucPlayMask & 0x0F;
	nMaxSpectatorNum = (DWORD)(nSpectatorMask >> 16);
	nMaxSpectatorNum &= 0xFF;
	nSpectatorNum = (nSpectatorMask >> 8) & 0x00FF;
	nBoys = (BYTE)(nSexMask >> 24);
#endif
	
	defRoomStateMask max_a_player = 0;
	defRoomStateMask max_player = 0;
	defRoomStateMask max_a_sp = 0;
	defRoomStateMask max_sp = 0;
	defRoomStateMask max_boy = 0;
	defRoomStateMask is_s = 0;
/* 	max_a_player = nMaxPlay; */
/* 	max_player = nPlayNum; */
/* 	max_player <<= 8; */
/* 	max_a_sp = nMaxSpectatorNum; */
/* 	max_a_sp <<= 16; */
/* 	max_sp = nSpectatorNum; */
/* 	max_sp <<= 24; */
/* 	max_boy = nBoys; */
/* 	max_boy <<= 32; */
/* 	is_s = bOffSpectator; */
/* 	is_s <<= 40; */
	
	max_a_player = stateMask;
	max_player = stateMask >> 8;
	max_a_sp = stateMask >> 16;
	max_sp = stateMask >> 24;
	max_boy = stateMask >> 32;
	is_s  = stateMask >> 40;

	bOffSpectator = (BYTE)is_s;
	nMaxPlay = (BYTE)max_a_player;
	nPlayNum = (BYTE)max_player;
	nMaxSpectatorNum = (BYTE)max_a_sp;
	nSpectatorNum = (BYTE)max_sp;
	nBoys = (BYTE)max_boy;
}
}

namespace RoleHelpFuns
{

const WORD nPlm = 0x8000;
const WORD nPosm = 0x7fff;
/// 编码defRoomPosSolt字段
inline defRoomPosSolt EncodePosSolt(bool bIsPlayer, defRoomPosSolt nPos)
{
    defRoomPosSolt ret = 0;
    if (bIsPlayer)
    {
        ret |= nPlm;
    }
    nPos &= nPosm;
    ret |= nPos;
    return ret;
}

/// 是否观战者
inline bool IsSpectator(defRoomPosSolt nSolt)
{
    return nSolt & nPlm ? false : true;
}

/// 获得位置索引
inline defRoomPosSolt GetPosSolt(defRoomPosSolt nSolt)
{
    defRoomPosSolt nPos = nSolt & nPosm;
    return nPos;
}

/// 解码defRoomPosSolt字段
inline void DecodePosSolt(defRoomPosSolt nSolt, bool & bIsPlayer, defRoomPosSolt & nPos)
{
    bIsPlayer = !IsSpectator(nSolt);
    nPos = nSolt & nPosm;
}

}



// 	音乐标记
enum enMusicFlag
{
	kMusicFlag_Banzou,
	kMusicFlag_Yuanchang,
	kMusicFlag_Count,
};

// 唱歌类型
enum enMusicSType 
{
	kMusicSongType_Song = 1,
	kMusicSongType_Heng = 2,
	kMusicSongType_NewPlayer = 3,
	kMusicSongType_Off = 4,
	kMusicSongType_Hide=5,	//隐藏歌曲

	kMusicSongType_Count,
};

// 歌曲时间类型
enum enMusicTime
{
	kMusicTime_All = 1, // 正首
	kMusicTime_Half = 2, // 半首
};



/*
 * 游戏房间结构定义
 */

///	普通房间详细信息结构定义
typedef struct tagNormalRoomInfo
{
    defMusicID 	m_nMusicId;
	enMusicFlag m_flag; 	// 	标记
	enMusicSType m_songType;
    defMapID 	m_nMapId;
    defRoomStateMask m_nStateMask;
	bool 	m_bRandomMusic;
	bool 	m_bRandomMap;
	enMusicTime m_timeType;

} MODI_NormalRoomInfo;

///		大房间详细信息结构定义
typedef struct tagMODIBigRoomInfo
{

} MODI_BigRoomInfo;

///	房间信息结构定义
typedef struct tagMODIRoomInfo
{
    MODI_GUID m_masterID;
    char m_cstrRoomName[ROOM_NAME_MAX_LEN + 1]; // 房间名
    defRoomType m_roomType; //	房间类型
    defRoomID m_roomID; 	//id
	WORD m_wdSceneId; 		/// 场景信息
	bool 	m_bPlaying; // 是否开始游戏

    //	房间的详细信息
    union
    {
        MODI_NormalRoomInfo m_detailNormalRoom;
        MODI_BigRoomInfo m_detailBigRoom;
    };

    tagMODIRoomInfo()
    {
        memset(m_cstrRoomName, 0, sizeof(m_cstrRoomName));
        m_roomType = INVAILD_ROOM_TYPE;
        m_roomID = INVAILD_ROOM_ID;
		m_bPlaying = false;
		m_wdSceneId = 0;
    }

	const bool operator == ( const tagMODIRoomInfo & that ) const
	{
		if( this == &that )
			return true;
		if( strncmp( this->m_cstrRoomName , that.m_cstrRoomName , sizeof(this->m_cstrRoomName) ) )
			return false;
		if( m_roomType != that.m_roomType ||
			m_roomID != that.m_roomID )
			return false;
		return true;
	}

	bool operator != ( const tagMODIRoomInfo & that ) const
	{
		return this->operator == ( that ) ? false : true;
	}

} MODI_RoomInfo;
// 更新物品信息的结构
struct MODI_UpdateItemInfo
{
	MODI_GUID 	m_itemguid;
	WORD 		m_byCount;

	MODI_UpdateItemInfo()
	{
		m_byCount = 0;
	}
};

enum MODI_AvatarSlotType
{
    enAvatarT_Head = 1,
	enAvatarT_Hair,
	enAvatarT_Upper,
	enAvatarT_Lower,
	enAvatarT_Hand,
	enAvatarT_Foot,
	enAvatarT_Headdress,
	enAvatarT_Glass,
	enAvatarT_Earring,
	enAvatarT_Mic,
	enAvatarT_Mousedress,
	enAvatarT_Necklace,
	enAvatarT_Watch,
	enAvatarT_Ring,
	enAvatarT_Back,
	enAvatarT_Pet,
	enAvatarT_Tail,
	enAvatarT_Suit,
	enAvatarT_Express,
	enAvatarT_Armdress,
	enAvatarT_Waistdress,
	enAvatarT_Legdress,
	enAvatarT_Eyedress,
	enAvatarT_Bodydress,
 	MAX_BAG_AVATAR_COUNT = enAvatarT_Bodydress, // 装备背包大小
};


enum MODI_ItemType
{
    enItemType_Unknow = 0,

	// 装备(avatar)
    enItemType_Head = enAvatarT_Head ,
    enItemType_Hair = enAvatarT_Hair,
    enItemType_Upper = enAvatarT_Upper,
    enItemType_Lower = enAvatarT_Lower,
    enItemType_Hand = enAvatarT_Hand,
    enItemType_Foot = enAvatarT_Foot,
    enItemType_Headdress = enAvatarT_Headdress,
    enItemType_Glass = enAvatarT_Glass,
    enItemType_Earring = enAvatarT_Earring,
    enItemType_Mic = enAvatarT_Mic,
    enItemType_Mousedress = enAvatarT_Mousedress,
    enItemType_Necklace = enAvatarT_Necklace,
    enItemType_Watch = enAvatarT_Watch,
    enItemType_Ring = enAvatarT_Ring,
    enItemType_Back = enAvatarT_Back,
    enItemType_Pet = enAvatarT_Pet,
    enItemType_Tail = enAvatarT_Tail,
    enItemType_Suit = enAvatarT_Suit,
	enItemType_Express = enAvatarT_Express,

	enItemType_Armdress = enAvatarT_Armdress,
	enItemType_Waistdress = enAvatarT_Waistdress,
	enItemType_Legdress = enAvatarT_Legdress,
	enItemType_Eyedress = enAvatarT_Eyedress,
	enItemType_Bodydress = enAvatarT_Bodydress,

	AVATARTYPE_BIG_CLASS = enItemType_Bodydress, //  属于avatar大类的物品
	CAN_NOT_LAP_ITEMTYPE = enItemType_Bodydress, // 无法重叠的物品类型上限值

	// 道具
    enItemType_Item_Laba, 		// 喇叭
	enItemType_Item_Jingyanka,  // 经验卡
	enItemType_Item_Bianxingka, // 变形卡
	enItemType_Item_Xiaoguoka,  // 效果卡
	enItemType_Item_Box, // 宝箱
	enItemType_Item_Diamond, //  彩钻物品
	enItemType_Item_Other, // 其他类，不太好分的都属于这类

	ITEM_BIG_CLASS = enItemType_Item_Other, // 属于道具大类的物品

    enItemType_MaxCount
};

// 喇叭的种类
enum enLabaType
{
	kLaba_Channel = 1,
	kLaba_Feizi,
	kLaba_Fensi,
	kLaba_Jiazu,
	kLaba_World,
};

// 某个类型的物品的最大拥有个数
inline WORD 	GetItemSubClassMaxOwn( MODI_ItemType type )
{
	static WORD s_MaxOwn[enItemType_MaxCount] = { 
		// avatar
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED , ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , 
		ITEM_SUBOWN_UNLIMITED ,

		// 道具
		ITEM_SUBOWN_UNLIMITED , 1, ITEM_SUBOWN_UNLIMITED ,ITEM_SUBOWN_UNLIMITED ,
		ITEM_SUBOWN_UNLIMITED , 1 , ITEM_SUBOWN_UNLIMITED };

	if( type > enItemType_Unknow && type < enItemType_MaxCount )
		return s_MaxOwn[type - 1];
	return 0;
}



inline MODI_ItemType GetItemType(WORD nItemID)
{
	if( nItemID == INVAILD_CONFIGID )
		return enItemType_Unknow;

	// avatar预留7个，目前用到的0－38000，预留12000 ， 0－50000表示AVATAR
	static const DWORD nAvatarIDSpace = 2000;
	static const DWORD nAvatarIDBegin = 1;
	static const DWORD nAvatarIDEnd = nAvatarIDSpace * AVATARTYPE_BIG_CLASS;

	// item预留6个，目前用到的道具ID，50000－55000，50000－60000表示ITEM
	static const DWORD nItemIDSpace = 1000; 
	static const DWORD nItemIDBegin = 50001;
	static const DWORD nItemIDEnd = nItemIDBegin + nItemIDSpace * (ITEM_BIG_CLASS - AVATARTYPE_BIG_CLASS);

	if( nItemID >= nAvatarIDBegin && nItemID <= nAvatarIDEnd )
	{
		WORD nMod = nItemID / nAvatarIDSpace;
		if( nItemID % nAvatarIDSpace )
			nMod += 1;
		return (MODI_ItemType)(nMod);
	}
	else if( nItemID >= nItemIDBegin && nItemID <= nItemIDEnd )
	{
		WORD nTmp = nItemID - nItemIDBegin + 1;
		WORD nMod = nTmp / nItemIDSpace;
		if( nTmp % nItemIDSpace )
			nMod += 1;
		return (MODI_ItemType)(nMod + AVATARTYPE_BIG_CLASS);
	}

    return enItemType_Unknow;
}

// 某种道具是否可以主动使用
inline bool 	IsCanActiveUse( WORD nItemID )
{
	if( nItemID == INVAILD_CONFIGID )
		return false;

	MODI_ItemType t = GetItemType( nItemID );
	if( t <= ITEM_BIG_CLASS && t > AVATARTYPE_BIG_CLASS )
	{
		// 喇叭，经验卡都是不可以主动使用道具
		static bool ms_bCanActiveUse[] = { false ,  // 喇叭
			false ,  // 经验卡
			true ,  // 变形卡
			true,   // 效果卡
			true ,  // 宝箱
			false,
			false }; 

		int idx = (int)(t - AVATARTYPE_BIG_CLASS);
		if( idx <= 0 )
			return false;
		return ms_bCanActiveUse[idx - 1];
	}
	return false;
}

// 玩家背包大小(衣橱)
#define MAX_BAG_PLAYER_COUNT 	254

// 定义背包类型
enum MODI_PackageType
{
	enBagType_None = 0,
	enBagType_Begin = 0,
	enBagType_Avatar = 1, //  装备背包（衣饰）
	enBagType_PlayerBag, // 玩家背包( 衣橱 )
	enBagType_End,
};


/*
 * 	数据数据结构定义
 */
struct MODI_ItemInfo
{
	/// 道具id
	QWORD m_qdItemId;
	/// 道具配置id
	DWORD m_dwConfigId;
	/// 包裹类型
	BYTE m_byBagType;
	/// 包裹位置
	WORD m_wdBagPos;
	/// 过期时间
	DWORD m_dwRemainTime;

	MODI_ItemInfo()
	{
		Reset();
	}

	/// 清除该位置的信息
	void Reset()
	{
		m_qdItemId = 0;
		m_dwConfigId = INVAILD_CONFIGID;
		m_byBagType = enBagType_Avatar;
		m_wdBagPos = INVAILD_POS_INBAG;
		m_dwRemainTime = 0;
	}

	
	/** 
	 * @brief 是否合法
	 * 
	 * @return true合法
	 *
	 */
	bool IsVaild() const
	{
		if(m_qdItemId == 0 || m_dwConfigId == INVAILD_CONFIGID)
		{
			return false;
		}
		return true;
	}

	/// 是否永久物品
	bool IsForeverItem() const
	{
		return m_dwRemainTime == enForever;
	}
};


/* struct MODI_ItemInfoInBag : public MODI_ItemInfo */
/* { */
/* 	BYTE 				m_nBagType; */
/* 	BYTE 				m_nPosInBag; 		// 	物品在背包中的位置 */

/* 	MODI_ItemInfoInBag() */
/* 	{ */
/* 		m_nBagType = enBagType_Avatar; */
/* 		m_nPosInBag = INVAILD_POS_INBAG; */
/* 	} */


/* 	bool 	IsVaildPos() const */
/* 	{ */
/* 		if( m_nPosInBag == INVAILD_POS_INBAG ) */
/* 			return false; */

/* 		if( m_nBagType == enBagType_Avatar &&	m_nPosInBag > MAX_BAG_AVATAR_COUNT ) */
/* 			return false; */

/* 		return true; */
/* 	} */
/* }; */

inline bool IsValidBagPos(MODI_PackageType packagetype,BYTE pos)
{
    if (packagetype == enBagType_PlayerBag
        &&  pos < MAX_BAG_PLAYER_COUNT) 
    {
        return true;
    }

    if (packagetype == enBagType_Avatar
        &&  pos < MAX_BAG_AVATAR_COUNT) 
    {
        return true;
    }

    return false;
}



typedef struct tagClientAvatarData
{
	WORD 			m_Avatars[MAX_BAG_AVATAR_COUNT];

	tagClientAvatarData()
	{
		memset( m_Avatars , INVAILD_CONFIGID , sizeof(m_Avatars ) );
	}

} MODI_ClientAvatarData;

typedef struct tagRoleInfoBase
{
    MODI_GUID m_roleID;
    char m_cstrRoleName[ROLE_NAME_MAX_LEN + 1];
    defRoomID m_roomID;
    kGameState m_ucGameState;
    BYTE m_ucLevel;
    BYTE m_ucSex;
	BYTE	m_bVip;	///是否是VIP，0的情况是非VIP

    tagRoleInfoBase() :m_roomID(INVAILD_ROOM_ID), 
					m_ucGameState(kInLobby), 
					m_ucLevel(0),
					m_ucSex(enSexBoy)
    {
        memset(m_cstrRoleName, 0, sizeof(m_cstrRoleName));
		m_bVip = 0;
    }

} MODI_RoleInfo_Base;

typedef struct tagRoleInfoNormal
{
    defRoomPosSolt m_roomSolt; //角色在房间中的位置
	MODI_GUID m_SingerID;//正在听哪个人在唱歌（唱歌那个人的GUID） 
	WORD 		m_nDefavatarIdx; // 默认形象的AVATAR  
	MODI_ClientAvatarData m_avatarData; //角色的AVATAR相关信息 
	DWORD m_nExp;// 角色的当前经验
	WORD m_wdChenghao;

    tagRoleInfoNormal() :
        m_roomSolt( INVAILD_POSSOLT ),
		m_nDefavatarIdx(INVAILD_CONFIGID),
		m_nExp(0)
    {
	    m_wdChenghao = 0;
    }

} MODI_RoleInfo_Normal;

// 情侣关系的额外信息
struct MODI_LoverExtraData
{
	WORD 					m_Avatars[MAX_BAG_AVATAR_COUNT]; // 情侣的形象
	char 					m_szName[ROLE_NAME_MAX_LEN + 1]; // 名字
};

// 夫妻关系的额外信息
struct MODI_MarriedExtraData
{
	WORD 					m_Avatars[MAX_BAG_AVATAR_COUNT]; // 老公/老婆的形象
	char 					m_szName[ROLE_NAME_MAX_LEN + 1]; // 名字
};


// 配偶／情人相关数据 
struct MODI_HoneyData
{
	enum enHoneyType
	{
		enHoneyT_None , // 没有情侣／夫妻关系
		enHoneyT_Lover, // 是情侣关系
		enHoneyT_Married, // 是夫妻关系
	};

	enHoneyType m_enHoneyType; // 指明是情侣关系还是夫妻关系，或者都不是
	union
	{
		MODI_LoverExtraData 	m_LoverData;
		MODI_MarriedExtraData 	m_MarriedData;
	};

	MODI_HoneyData()
	{
		m_enHoneyType = MODI_HoneyData::enHoneyT_None;
		memset( m_LoverData.m_szName , 0 , sizeof(m_LoverData.m_szName) );
		memset( m_LoverData.m_Avatars , INVAILD_CONFIGID , sizeof(WORD) * MAX_BAG_AVATAR_COUNT );
	}
};

// 角色的社会关系的数据
struct MODI_SocialRelationData
{
	time_t 	m_nLastAddIdolTime; // 最后一次添加偶像的时间
	DWORD	m_nRenqi; // 人气值
	DWORD	m_nFensiGril; // 女性粉丝个数
	DWORD	m_nFensiBoy; // 男性粉丝个数

	// 配偶／情人相关数据 , 根据m_enHoneyType 字段决定
	MODI_HoneyData 	m_HoneyData;

	MODI_SocialRelationData():
		m_nLastAddIdolTime(0),
		m_nRenqi(0),
		m_nFensiGril(0),
		m_nFensiBoy(0)
	{
	
	}
};

typedef struct tagRoleInfoDetail
{
	BYTE m_byAge; // 年龄
	DWORD m_nCity; // 城市ID
	DWORD m_nClan;  // 预留
	DWORD m_nGroup; //预留
	DWORD m_nQQ; // qq
	BYTE m_byBlood; // 血型
	DWORD m_nBirthDay; // 生日
	char m_szPersonalSign[MAX_PERSONAL_SIGN + 1]; // 个性签名
	DWORD m_nMoney; // 游戏货币
	DWORD m_nRMBMoney; // 人民币

	DWORD m_nVoteCount; // 获得的票数
	DWORD m_nWin;  // 胜利次数
	DWORD m_nLoss; // 失败次数
	DWORD m_nTie; // 平局次数
	DWORD m_nPefect; // 最高的 PEFECT COUNT
   	DWORD m_nCool; // 最高的 COOL COUNT
	DWORD m_nGood; // 最高的 GOOD COUNT
	DWORD m_nBad; // 最高的 BAD COUNT
	DWORD m_nMiss; // 最高的 MISS COUNT
	DWORD m_nCombo; // 最高的 连击数
	float32  m_fExact; // 最高的 精确度
	BYTE m_byReadHelp;
	DWORD m_Chang; // 唱
	DWORD m_Heng; // 哼
	DWORD m_KeyBoard; // 键盘
	DWORD m_Toupiao; // 主动投票次数
	DWORD highest_socre; // 最高分数
	WORD highest_score_musicid; // 分数最高的音乐id
	float highest_precision; // 最高精度
	WORD highest_precision_musicid; // 精度最高的音乐id

	//// 键盘模式
	DWORD key_highest_socre; // 最高分数
	WORD key_highest_score_musicid; // 分数最高的音乐id
	float key_highest_precision; // 最高精度
	WORD key_highest_precision_musicid; // 精度最高的音乐id
	MODI_SocialRelationData 	m_SocialRelData;

    tagRoleInfoDetail() :
        m_byAge(1),
		m_nCity(INVAILD_CITYID),m_nClan(0),
		m_nGroup(0),m_nQQ(0),m_byBlood(enXuexingBegin),
		m_nBirthDay(0),
		m_nMoney(0),m_nRMBMoney(0),
		m_nVoteCount(0),
		m_nWin(0),m_nLoss(0),m_nTie(0),
		m_nPefect(0),m_nCool(0),m_nGood(0),m_nBad(0),m_nMiss(0),m_nCombo(0),m_fExact(0.0f),
		m_byReadHelp(0)
    {
		m_Chang = 0; // 唱
		m_Heng = 0; // 哼
		m_KeyBoard = 0; // 键盘
		m_Toupiao = 0; // 主动投票次数
		memset( m_szPersonalSign , 0 , sizeof(m_szPersonalSign) );
		highest_socre = 0; // 最高分数
		highest_score_musicid = INVAILD_CONFIGID; // 分数最高的音乐id
		highest_precision = 0.0f; // 最高精度
		highest_precision_musicid = INVAILD_CONFIGID; // 精度最高的音乐id
		//// 键盘模式
		key_highest_socre = 0; // 最高分数
		key_highest_score_musicid = INVAILD_CONFIGID; // 分数最高的音乐id
		key_highest_precision = 0.0f; // 最高精度
		key_highest_precision_musicid = INVAILD_CONFIGID; // 精度最高的音乐id
    }

	bool IsHasLover() const
	{
		return m_SocialRelData.m_HoneyData.m_enHoneyType == MODI_HoneyData::enHoneyT_Lover;
	}

	bool IsHasMarrid() const
	{
		return m_SocialRelData.m_HoneyData.m_enHoneyType == MODI_HoneyData::enHoneyT_Married;
	}

	// 是否单身
	bool IsSingle() const
	{
		return m_SocialRelData.m_HoneyData.m_enHoneyType == MODI_HoneyData::enHoneyT_None;
	}

} MODI_RoleInfo_Detail;

typedef struct tagRoleInfo
{ 
	MODI_RoleInfo_Base m_baseInfo; //角色的基本信息
    MODI_RoleInfo_Normal m_normalInfo; //角色的普通信息
    MODI_RoleInfo_Detail m_detailInfo; //角色的详细信息

} MODI_RoleInfo;

// 游戏结果
typedef struct tagGameResult
{
	MODI_GUID 	  m_nClientID;
	char 		  m_szRoleName[ROLE_NAME_MAX_LEN + 1]; // 角色名
	BYTE m_byLevel; // 当前等级
	WORD m_byPefect; //PEFECT COUNT
   	WORD m_byCool; //COOL COUNT
	WORD m_byGood; //GOOD COUNT
	WORD m_byBad; // BAD COUNT
	WORD m_byMiss; //MISS COUNT
	WORD m_byCombo; //连击数
	float32  m_fExact; //精确度
	DWORD  m_nTotalScore; // 总分
	BYTE m_byEvaluation; //评价
	DWORD  m_nExp; //获得的经验
	DWORD  m_nMoney; //获得的金钱数
	BYTE m_byLevelUP; // 是否升级
	WORD m_nSentensCount;//唱了几句
	

	tagGameResult()
	{
		Reset();
	}

	void Reset()
	{
		memset( m_szRoleName , 0 , sizeof( m_szRoleName ) );
		m_byLevel = 0;
		m_byPefect = 0;
		m_byCombo = 0;
		m_byCool = 0;
		m_byGood = 0;
		m_byBad = 0;
		m_byMiss = 0;
		m_fExact = 0.0f;
		m_nTotalScore = 0;
		m_byEvaluation = 0;
		m_nExp = 0;
		m_nMoney = 0;
		m_byLevelUP = 0;
		m_nSentensCount = 0;
	}

}MODI_GameResult;

/// 游戏中音乐的语句相关信息
typedef struct tagMdmSentenceInfo
{
	float  m_fBeginSecs; // 该句歌词开始的时间
	float  m_fEndSecs; // 该句歌词结束的时间

	tagMdmSentenceInfo()
	{
		m_fBeginSecs = 0.0f;
		m_fEndSecs = 0.0f;
	}

}MODI_MdmSentenceInfo;


// 登入KEY,客户端将该结构扔给 生成 MODI_LoginPassport 的接口函数
// 之后使用 通行证 登入网关服务器
struct MODI_LoginKey
{
	char 	m_szContnet[4];

	MODI_LoginKey()
	{
		memset( m_szContnet , 0 , sizeof(m_szContnet) );
	}

};

// 登入用的通行证
struct MODI_LoginPassport
{
	char 			m_byRechangeChannel; 	// 	是否重选游戏频道 , 0 = 不是 1 = 是 , 默认登入为0，重选频道为1
	MODI_CHARID 	m_nCharID; 				//  所选的角色ID 
	defAccountID m_nAccountID; 				//  登入的帐号
	char 	m_szContent[16]; 				//  PP内容

	MODI_LoginPassport()
	{
		m_byRechangeChannel = 0;
		m_nCharID = INVAILD_CHARID;
		m_nAccountID = INVAILD_ACCOUNT_ID;
		memset( m_szContent , 0 , sizeof(m_szContent) );
	}
};

// 选频道时需要的角色信息
struct MODI_CharInfo
{
	MODI_CHARID 	m_nCharID; // 角色ID
	char 	m_szRoleName[ROLE_NAME_MAX_LEN + 1]; // 角色名
	MODI_ClientAvatarData 	m_AvatarInfo; // avatar 信息
	BYTE m_byLevel; // 等级
	BYTE m_bySex; // 性别
	WORD m_nDefaultAvatarIdx; // 默认的形象
	BYTE m_byReadHelp;
	BYTE m_byVip;

	MODI_CharInfo()
	{
		m_nCharID = INVAILD_CHARID;
		memset( m_szRoleName , 0 , sizeof(m_szRoleName) );
		m_byLevel = 0;
		m_bySex = enSexBoy;
		m_nDefaultAvatarIdx = INVAILD_CONFIGID;
		m_byReadHelp = 0;
		m_byVip = 0;
	}
};

/// 创建角色时用到的数据结构
struct MODI_CreateRoleInfo
{
	char 	m_szRoleName[ROLE_NAME_MAX_LEN + 1]; // 角色名
	DWORD 	m_nBirthday; // 生日
	char 	m_byXuexing; // 血型
	DWORD 	m_qq; // qq 
	char 	m_Sign[MAX_PERSONAL_SIGN + 1]; // 个性签名
	BYTE 	m_bySex;
	WORD 	m_nCityID; // 城市id
	BYTE 	m_AvatarIdx; // 所选的形象组合（表中的ID）

	MODI_CreateRoleInfo()
	{
		memset( m_szRoleName , 0 , sizeof(m_szRoleName) );
		memset( m_Sign , 0 , sizeof(m_Sign) );
		m_nBirthday = 0;
		m_byXuexing = enOXingXue;
		m_qq = 0;
		m_bySex = enSexGril; 
		m_nCityID = INVAILD_CITYID;
		m_AvatarIdx = INVAILD_CONFIGID;
	}
};

/// 家族名字最大长度
#define FAMILY_NAME_LEN 30

/// 家族职位类型
enum enFamilyPositionType
{
	enPositionT_None,		/// 开除此人(默认没有家族也是此)
	enPositionT_Request,  /// 申请成员
	enPositionT_Normal,	  /// 普通成员
	enPositionT_ViceLead, /// 副族长
	enPositionT_Lead,	  /// 族长
	enPositionT_End,	  
};

/// 家族名片
struct MODI_FamilyCard
{
	MODI_FamilyCard()
	{
		m_stGuid = INVAILD_GUID;
		m_stPosition = enPositionT_None;
		memset(m_cstrFamilyName, 0, sizeof(m_cstrFamilyName));
		m_wLevel = 0;
	}
	
	MODI_GUID m_stGuid;
	enFamilyPositionType m_stPosition;
	char m_cstrFamilyName[FAMILY_NAME_LEN + 1];
	WORD	m_wLevel;
};


/// 名片信息
struct MODI_NameCard
{
	char 				m_szName[ROLE_NAME_MAX_LEN + 1]; // 名字
	BYTE 				m_byLevel; // 等级
	BYTE 				m_bySex; // 性别
	WORD 				m_nDefaultAvatarIdx; // 默认AVATAR形象
	WORD				m_wdChenghao; //名片显示称号
	MODI_FamilyCard m_stFamilyCard; /// 家族名片
	
	MODI_ClientAvatarData 	m_avatar; // 形象
	MODI_RoleInfo_Detail m_RoleDetail; // 具体详细信息
	
	/// 增加accname
	char m_szAccName[MAX_ACCOUNT_LEN+1];

	MODI_NameCard()
	{
		memset( m_szName , 0 , sizeof(m_szName) );
		memset(m_szAccName, 0, sizeof(m_szAccName));
		m_byLevel = 0;
		m_bySex = enSexBoy;
		m_nDefaultAvatarIdx = 0;
	}
};

/// 服务器状态
struct MODI_ServerStatus
{
	defServerChannelID m_nServerID; // 服务器ID 
	char 	m_szServerName[MAX_SERVERNAME_LEN + 1]; // 服务器名字
	BYTE   	m_byLoadStatus; // 负载情况
	DWORD 	m_nGatewayIP; // 对应网关IP
	WORD 	m_nGatewayPort;  // 对应网关PORT
	DWORD 	m_nTSvrIP; /// teamserverip
	WORD 	m_nTSvrPort; /// teamserverport

	MODI_ServerStatus()
	{
		m_byLoadStatus =  0;
		m_nServerID = INVAILD_SVRCHANNELID;
		m_nGatewayIP = 0;
		m_nGatewayPort = 0;
		memset( m_szServerName , 0 , sizeof(m_szServerName) );
	}
};
	
	
namespace TimeHelpFuns
{
    inline DWORD GenerateDate(int year, int month, int day)
    {
    	DWORD ymd = year<<16 | month<<8 | day;
    	return ymd;
    }
    
    inline void GetDate(DWORD date,int & year,int & month,int & day)
    {
       year = (date & 0xFFFF0000) >> 16;
       month = (date & 0x0000FF00) >> 8;
       day = date & 0x000000FF;
    }
    
    inline std::string GetDateString(DWORD date)
    {
        int year = 0;
        int month = 0;
        int day = 0;
        GetDate(date,year,month,day);
        std::string datestr;
        char buf[10];
	if( year == 0)
	{
		SNPRINTF( buf , sizeof(buf) , "%d-" , year+1890 );
	}
	else
	{
		SNPRINTF( buf , sizeof(buf) , "%d-" , year );
	}
        datestr += buf;
		SNPRINTF( buf , sizeof(buf) , "%d" , month );
        if( month < 10 )
            datestr += "0";
        datestr += buf;
	datestr += "-";
	SNPRINTF( buf , sizeof(buf) , "%d" , day );
        if( day < 10 )
            datestr += "0";
        datestr += buf;
        
        return datestr;        
    }

	
	inline DWORD EncodeDateFromString( const char * szDate )
	{
		DWORD dwDate = 0;
		int year = 0;
		int month = 0;
		int day = 0;
		sscanf( szDate , "%d-%d-%d" , &year , &month , &day );
		dwDate = GenerateDate( year , month , day );
		return dwDate;
	}
}

// 物品同步信息结构
struct MODI_ItemSyncInfo
{
	BYTE 				m_bySrcBagType; // 源背包类型
	BYTE 				m_byDestBagType; // 目标背包类型
	BYTE 				m_bySrcItemPos; // 物品在源背包中的位置
	BYTE 				m_byDestItemPos; // 物品在目标背包中的位置

	MODI_ItemSyncInfo()
	{
		m_bySrcBagType = enBagType_None;
		m_byDestBagType = enBagType_None;
		m_bySrcItemPos = INVAILD_POS_INBAG;
		m_byDestItemPos = INVAILD_POS_INBAG;
	}

	bool 	IsValidParam() const
	{
		if( (this->m_bySrcBagType > enBagType_Begin && this->m_bySrcBagType < enBagType_End) &&
			(this->m_byDestBagType > enBagType_Begin && this->m_byDestBagType < enBagType_End) &&
			this->m_bySrcItemPos != INVAILD_POS_INBAG &&
			this->m_byDestItemPos != INVAILD_POS_INBAG )
			return true;
		return false;
	}

};

// 使用表情结果定义
enum  enPlayExpressionResult
{
	kPExpression_Successful = 0,
	kPExpression_UnEquip,// 未装备表情包
	kPExpression_Transfroming, // 变形中，无法使用
	kPExpression_Playing, // 当前正在播放表情
	kPExpression_UnKnowError,// 未知错误，（服务器内部引起)
};

// 运营活动用的兑换key的活动
enum enYunYingKeyExchangeResult
{
	kYYKeyExchange_Successful = 0,
	kYYKeyExchange_Invalid, // key 无效
	kYYKeyExchange_BagFull, // 背包满
	kYYKeyExchange_ClassCof,//类型冲突 用户已经使用了同类型的CDKEY
	kYYKeyExchange_UnKnowError ,// 服务器异常
};

/// 沉迷等级
enum enChenMiResult
{
	enChenMi_None = 0,
	enChenMi_OneHour = 1,
	enChenMi_TwoHour = 2,
	enChenMi_ThreeHour = 3,
	enChenMi_FourHour = 4,
	enChenMi_MoreThen_FiveHour = 5,
	/// 此账号不再是沉迷的账号了
	enChenMi_Over = 6,
	enChenMi_Begin = 7,
};

enum  enSystemNoticeType
{
	enSystemNotice_Default=0,
	enSystemNotice_LevelUp,
	enSystemNotice_Mission,
	enSystemNotice_Money,
	enSystemNotice_Social,
	enSystemNotice_Exp,
	enSystemNotice_Item,
	enSystemNotice_Family,
	enSystemNotice_Renqi,
	enSystemNotice_FamilyLevelup, /// 家族升级

};

/// 对邀请的回复
enum enOptRequestResult
{
	enOptRequestInValid=0, 
	enOptRequestRefuse, /// 拒绝
	enOptRequestAccept, /// 同意
	enOptRequestNum,
};

/// 道具的来由
enum enAddItemReason
{
	enAddReason_None,
	/// 创建角色
	enAddReason_CreateRole,
	/// 系统赠送
	enAddReason_SystemSend,
	/// cdkey领用
	enAddReason_Cdkey,
	/// 任务奖励
	enAddReason_Task,
	/// 玩家赠予的
	enAddReason_Mail,
	/// 自己金币购买的
	enAddReason_Money,
	/// 自己人民币购买
	enAddReason_MoneyRmb,
	/// 礼包兑换的
	enAddReason_GoodPackages,
	/// 网页道具
	enAddReason_Web,
	/// 开幸运宝盒
	enAddReason_ItemXYBox,
	/// 开愿望宝盒
	enAddReason_ItemYWBox,
};

///	签到活动的结果
enum enSignUpResult
{
	enSignUpResult_Unknown=0,	///未知错误
	enSignUpResult_Had,	///	已经签到了
	enSignUpResult_Time,	///	时间未到
	enSignUpResult_Suc,	///	签到成功了
	enSignUpResult_Reward,	///	获得了本阶段的奖励
};
struct PacketItemDec
{
	WORD	m_wConfigID;
	BYTE	m_nCount;
	BYTE	m_tLimit;

	PacketItemDec()
	{
		m_wConfigID = 0;
		m_nCount = 0;
		m_tLimit = 0;
	}
};

/// 删除道具的理由
enum enDelItemReason
{
	enDelReason_None,
	
	/// 过期
	enDelReason_Exprie,

	/// 使用掉
	enDelReason_Used,

	/// 自己删除
	enDelReason_User,

	/// 系统任务删除
	enDelReason_Task,
};

/// 更新道具的理由
enum enUpdateItemReason
{
	enUpdateReason_None,
	
	/// 换装
	enUpdateReason_Change,

	/// 移动
	enUpdateReason_Move,
};


struct PacketInfo
{
	WORD	m_nConfigId;	
	char	m_szName[PACK_NAME_MAX_LEN+1];
	char	m_szDes[PACK_DESC_LEN+1];
	BYTE	m_nLevelReq;
	BYTE	m_bSexReq;
//	WORD	m_wItems[MAX_ITEMS_PER_PACKET];
	PacketItemDec  m_aItems[MAX_ITEMS_PER_PACKET];	
	char	m_szPicture[MAX_PACKT_IMAGE_NAME_LEN+1];
	PacketInfo()
	{
		m_nConfigId = INVAILD_CONFIGID;
		memset(m_szName ,0,sizeof(m_szName));
		memset(m_szDes,0,sizeof(m_szDes));
		m_nLevelReq=0;
		m_bSexReq = 3;
		memset(m_aItems,0,sizeof(m_aItems));
		memset(m_szPicture,0,sizeof(m_szPicture));
	}
};
enum	enGameMode
{
	enGameMode_Invalid=0,
	enGameMode_Sing,
	enGameMode_Key,

};

#define	TIME_DATE_LEN	20

/// 音乐单曲信息
struct SingleMusicInfo
{

	WORD	m_wMusicid;	///音乐ID
	DWORD	m_dwCount;	///音乐点击次数
	DWORD	m_dwScore;	///最高分数
	WORD	m_wPrecise;	///	最高精度
	char	m_szScorePerson[ROLE_NAME_MAX_LEN+1];	///最高得分玩家名字
	char	m_szPrecisePerson[ROLE_NAME_MAX_LEN+1];///最高精度玩家名字
	time_t	m_tScoreFresh;	///最高分刷新时间
	time_t	m_tPreciseFresh;	/// 最高准度刷新时间
	SingleMusicInfo()
	{
		memset(this,0,sizeof(SingleMusicInfo));
	}


};

/// 格子属性
enum enGridAttr
{
	/// 普通能走
	enGridAttrNormal = 0,
	/// 出生点
	enGridAttrBorn = 5,

	/// 普通阻挡
	enGridAttrBlock = 255,
};

/// 场景头信息
struct MODI_SceneHeadInfo
{
	//WORD m_dwSceneID; /// 场景id
    //WORD m_dwVersion; /// 版本号
    DWORD m_dwWidth;  /// 场景宽	
    DWORD m_dwHeight; /// 场景高
};

/// 场景物品属性
enum enSceneAttr
{
	enSceneAttrNone,
	enSceneAttrPlayer,
	enSceneAttrNpc,
	enSceneAttrMax,
};

/// 浮点数比较容差
const float _FLOAT_TOLERANCE = 0.0001f;

/// 检查浮点数是否相等
inline bool FloatEqualTolerance(float a, float b)
{
	return (a + _FLOAT_TOLERANCE > b) && (a - _FLOAT_TOLERANCE < b);
}

/// 2 dimension vector
class Vector_f
{
public:
	float x;
	float y;

	Vector_f()
		:x(0), y(0)
	{
	}

	Vector_f(float _x, float _y)
		:x(_x), y(_y)
	{
	}

	Vector_f& operator = (const Vector_f& vf)
	{
		x = vf.x;
		y = vf.y;
		return *this;
	}

	bool operator == (const Vector_f& rhs) const
	{
		return x == rhs.x && y == rhs.y;
	}

	bool operator != (const Vector_f& rhs) const
	{
		return x != rhs.x || y != rhs.y;
	}

	Vector_f operator * (float f) const
	{
		return Vector_f(x * f, y * f);
	}

	Vector_f& operator *= (float f)
	{
		x *= f;
		y *= f;
		return *this;
	}

	Vector_f operator + (const Vector_f& rhs) const
	{
		return Vector_f(x + rhs.x, y + rhs.y);
	}

	Vector_f operator - (const Vector_f& rhs) const
	{
		return Vector_f(x - rhs.x, y - rhs.y);
	}

	Vector_f operator += (const Vector_f& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	Vector_f operator -= (const Vector_f& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	float dot(const Vector_f& rhs) const
	{
		return x * rhs.x + y * rhs.y;
	}

	float normalize()
	{
		float len = sqrt(x*x + y*y);
		if(len != 0)
		{
			x /= len;
			y /= len;
		}

		return len;
	}

	float sqrLength() const
	{
		return x*x + y*y;
	}

	float length() const
	{
		return sqrt(x*x + y*y);
	}

	bool equivalence(const Vector_f& rhs) const
	{
		return FloatEqualTolerance(x, rhs.x) && FloatEqualTolerance(y, rhs.y);
	}
};

/// 线段类
class LineSegement
{
public:
	Vector_f mOrign;
	Vector_f mDirection;		// must be normalized
	float m_t;		// <0 - t>

	LineSegement()
		:m_t(0)
	{
	}

	Vector_f getPoint(float t)
	{
		return mOrign + mDirection * t;
	}

	bool ptOnLineSeg(const Vector_f& pt) const
	{
		Vector_f dt = pt - mOrign;
		float t = dt.normalize();
		return mDirection.equivalence(dt) && (t >= 0 && t <= m_t);
	}

	bool onLinePtInSeg(const Vector_f& pt) const
	{
		Vector_f dst = mOrign + mDirection * m_t;
		float dx0 = fabs(dst.x - mOrign.x);
		float dy0 = fabs(dst.y - mOrign.y);
		float dx1 = fabs(pt.x - mOrign.x);
		float dy1 = fabs(pt.y - mOrign.y);

		return (dx1 + dy1) <= (dx0 + dy0);
	}

	bool getCrossPt(const LineSegement& ls, Vector_f& crossPt)
	{
		float crossV = mDirection.y * ls.mDirection.x - mDirection.x * ls.mDirection.y;
		if(crossV == 0)
			return false;

		float t1 = ls.mDirection.x * (ls.mOrign.y - mOrign.y) - ls.mDirection.y * (ls.mOrign.x - mOrign.x);
		t1 /= crossV;

		if(t1 < 0 || t1 > m_t)
			return false;

		crossPt = mOrign + mDirection * t1;

		return true;
	}
};

/// 移动状态
struct MODI_MoveState
{
	MODI_MoveState()
	{
		m_fSpeed = 0;
		m_fRadii = 0;
		m_enMoveFlag = kMoveMentFlag_None;
		m_byRotateFlag = 0;
		m_Delta = 0;
	}
	
	Vector_f m_Pos;
	Vector_f m_fDir;
	Vector_f m_fCharDir;
	
	/// 阻挡点
	Vector_f m_stBlockPoint;
	float m_fSpeed;
	float m_fRadii;
	float m_Delta;
	char m_byRotateFlag; 
	enMovementFlags m_enMoveFlag;
};

/// 场景用户信息
struct MODI_SceneUserInfo
{
	MODI_SceneUserInfo()
	{
		
	}
		
	MODI_GUID m_stGuid;
	MODI_MoveState m_stMoveState;
};



#endif
