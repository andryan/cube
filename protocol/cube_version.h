/** 
 * @file cube_version.h
 * @brief 版本信息
 * @author Tang Teng
 * @version v0.1
 * @date 2011-01-14
 */

#ifndef MODI_CUBE_VERSION_H_
#define MODI_CUBE_VERSION_H_

/// 2011-04-29 商业化1.0.0
/// 2011-05-16 增加家族好组队模式 1.1.0
/// 2011-06-08 增加键盘模式 1.2.0
/// 2011-08-01 使用新的道具数据库 1.3.0
/// 2011-09-03 增加签到功能和家族升级功能1.4.0
/// 2011-10-25 增加网页道具和修改签名1.5.0
/// 2011-11-21 使用了YY版本1.6.0
/// 2012-02-01 新的gamedefine.h(vip) 1.7.0
/// 2012-0-14 VIP房间版本1.8.0
/// 2012-06-18 rPE cheat prevention and preliminary MAC address logging 1.9.0
enum enCountry
{
	enCountry_Mainland=0,
	enCountry_Ina,
	enCountry_Sgp,
	enCountry_Tha,

};
/// 版本信息记录
//#define MODI_CUBE_COUNTRY_NUMBER	 	enCountry_Ina
#define MODI_CUBE_MAJOR_VERSION       1
#define MODI_CUBE_MINOR_VERSION       10
#define MODI_CUBE_PATCH_VERSION       0

#ifdef _DEBUG
#define MODI_CUBE_IS_DEV_STRING "-dev"
#else
#define MODI_CUBE_IS_DEV_STRING ""
#endif


#ifndef MODI_STRINGIFY
#define MODI_STRINGIFY(n) MODI_STRINGIFY_HELPER(n)
#define MODI_STRINGIFY_HELPER(n) #n
#endif

#define MODI_CUBE_VERSION_STRING \
     MODI_STRINGIFY(MODI_CUBE_MAJOR_VERSION) "." \
     MODI_STRINGIFY(MODI_CUBE_MINOR_VERSION) "." \
     MODI_STRINGIFY(MODI_CUBE_PATCH_VERSION)  \
     MODI_CUBE_IS_DEV_STRING

#ifdef __cplusplus
extern "C" 
{
#endif

typedef struct {
	/*    unsigned int country;*/	/** (contry number ) **/
    unsigned int major;      /**< major number */
    unsigned int minor;      /**< minor number */
    unsigned int patch;      /**< patch number */
    unsigned int is_dev;     /**< is development (1 or 0) */
} modi_version_t;

inline void get_cube_version(modi_version_t *pvsn)
{
	pvsn->major = MODI_CUBE_MAJOR_VERSION;
	pvsn->minor = MODI_CUBE_MINOR_VERSION;
	pvsn->patch = MODI_CUBE_PATCH_VERSION;

#if defined (_DEBUG) || !defined(FINAL_RELEASE)
	pvsn->is_dev = 1;
#else 	
	pvsn->is_dev = 0;
#endif
}

inline const char * get_cube_version_string(void)
{
	return MODI_CUBE_VERSION_STRING;
}

inline bool is_valid_version( unsigned int major , unsigned int minor , unsigned patch )
{
	if(  MODI_CUBE_MAJOR_VERSION == major && 
		MODI_CUBE_MINOR_VERSION == minor )
		return true;
	return false;
}

#ifdef __cplusplus
}
#endif



#endif
