#ifndef C2GS_ZONECMD_DEFINED_H_
#define C2GS_ZONECMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Zone_Cmd : public stNullCmd
{
	MODI_Zone_Cmd()
	{
		byCmd = MAINCMD_ZONE;
	}
};

// 请求进入个人空间
struct MODI_C2GS_Request_EnterZone : public MODI_Zone_Cmd 
{
	MODI_C2GS_Request_EnterZone()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};


// 请求离开个人空间
struct MODI_C2GS_Request_LeaveZone : public MODI_Zone_Cmd 
{
	MODI_C2GS_Request_LeaveZone()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 5;
};



#pragma pack(pop)

#endif
