#ifndef C2GS_SHOPCMD_DEFINED_H_
#define C2GS_SHOPCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Shop_Cmd : public stNullCmd
{
	MODI_Shop_Cmd()
	{
        byCmd = MAINCMD_SHOP;
	}
};

// 请求进入商城
struct MODI_C2GS_Request_EnterShop : public MODI_Shop_Cmd 
{
	MODI_C2GS_Request_EnterShop()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};

// 进入商城结果
struct MODI_GS2C_Notify_EnterShopResult : public MODI_Shop_Cmd
{
	enInOutShopResult 	m_result; // 结果
	bool 		m_bRareShop; 

	MODI_GS2C_Notify_EnterShopResult()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 2;
};

// 客户端发起交易
struct MODI_C2GS_Request_Transaction : public MODI_Shop_Cmd
{
	enShopTransactionType 	m_actionType; // 交易类型
	char 		m_szTargetName[ROLE_NAME_MAX_LEN + 1]; // 赠送或者索取，需要填写目标对象角色名
	BYTE 		m_nCount; 							// 	购买个数
	MODI_ShopGood 	m_pGoods[ONCE_TRANSACTION_MAX_GOODSCOUNT]; 	// 	购买的商品
	WORD 		m_nContentSize; // 内容长度
	char 		m_pContent[0]; // 内容

	MODI_C2GS_Request_Transaction()
	{
		m_actionType = kTransaction_Buy;
		memset( m_szTargetName , 0 , sizeof(m_szTargetName) );
		m_nCount = 0;
		m_nContentSize = 0;
		byParam = ms_SubCmd;
	}
	
	static const BYTE ms_SubCmd = 3;
};
 
// 一次交易结果
struct MODI_GS2C_Notify_TransactionResult : public MODI_Shop_Cmd
{
	enShopTransactionType 		m_actionType; // 交易类型
	enShopTransactionResult 	m_result;
	char 						m_szTargetName[ROLE_NAME_MAX_LEN + 1]; // 赠送或者索取，目标对象角色名

	MODI_GS2C_Notify_TransactionResult()
	{
		m_actionType = kTransaction_Buy;
		memset( m_szTargetName , 0 , sizeof(m_szTargetName) );
		m_result = kTransaction_UnknowError;

		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 4;
};

// 请求离开商城
struct MODI_C2GS_Request_LeaveShop : public MODI_Shop_Cmd 
{
	MODI_C2GS_Request_LeaveShop()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 100;
};

// 离开商城结果
struct MODI_GS2C_Notify_LeaveShopResult : public MODI_Shop_Cmd
{
	enInOutShopResult 	m_result;

	MODI_GS2C_Notify_LeaveShopResult()
	{
		m_result = kIO_Shop_UnknowError; // 进出失败
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 101;
};


/// INA查询余额
struct MODI_C2GS_Req_Balance : public MODI_Shop_Cmd
{    
    MODI_C2GS_Req_Balance()
    {
        byParam = ms_SubCmd;
    }

    static const BYTE ms_SubCmd = 102;
};

#pragma pack(pop)

#endif
