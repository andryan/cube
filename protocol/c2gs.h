/** 
 * @file c2gs.h
 * @brief 客户端与游戏服务器的所有数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */

#ifndef MODI_CTOGS_DEFINED_H____
#define MODI_CTOGS_DEFINED_H____

#ifdef _MSC_VER
#pragma once
#endif

#include <string>
#include <list>
using std::list;
using std::string;

#include "protocol/cube_version.h"

#include "protocol/c2ls_logincmd.h"
#include "protocol/c2gs_chatcmd.h"
#include "protocol/c2gs_roomcmd.h"
#include "protocol/c2gs_rolecmd.h"
#include "protocol/c2gs_musiccmd.h"
#include "protocol/c2gs_relation.h"
#include "protocol/c2gs_itemcmd.h"
#include "protocol/c2gs_syscmd.h"
#include "protocol/c2gs_mail.h"
#include "protocol/c2gs_shop.h"
#include "protocol/c2gs_pay.h"
#include "protocol/c2gs_rank.h"
#include "protocol/c2gs_family.h"
#include "protocol/c2gs_zone.h"
#include "protocol/c2gs_Activity.h"

#pragma pack(push,1)
/// 服务器返回操作结果
const BYTE  SUBCMD_OPTRESULT   = 0xf0;

struct MODI_GS2C_Respone_OptResult : public stNullCmd
{
    WORD m_nResult;

    MODI_GS2C_Respone_OptResult()
    {
        byCmd = MAINCMD_OPTRESULT;
        byParam = SUBCMD_OPTRESULT;
    }
};



#pragma pack(pop)

#endif
