/** 
 * @file c2gs_roomcmd.h
 * @brief 房间相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_ROOMCMD_DEFINED_H_
#define C2GS_ROOMCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"

/*
 *    房间相关操作
 */
#pragma pack(push,1)

struct MODI_Room_Cmd : public stNullCmd
{
	MODI_Room_Cmd()
	{
        byCmd = MAINCMD_ROOM;
	}
};


/// 刷新房间列表
struct MODI_GS2C_Notify_Roomlist: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_Roomlist()
    {
		byParam = ms_SubCmd;
    }

    BYTE m_nCount;
    MODI_RoomInfo  m_pRooms[0];

	static const BYTE ms_SubCmd = 1;
};

/// 添加一个房间
struct MODI_GS2C_Notify_AddRoom: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_AddRoom()
    {
		byParam = ms_SubCmd;
    }

    MODI_RoomInfo stRoom; //  房间列表结构

	static const BYTE ms_SubCmd = 2;
};

/// 删除一个房间
struct MODI_GS2C_Notify_DelRoom: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_DelRoom()
    {
		byParam = ms_SubCmd;
		roomID = INVAILD_ROOM_ID;
    }

    defRoomID roomID; //  房间ID
	static const BYTE ms_SubCmd = 3;
};

/// 房间信息更新
struct MODI_GS2C_Notify_RoomUpdate: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_RoomUpdate()
    {
		byParam = ms_SubCmd;
		m_byUpdateSize = 0;
    }


    BYTE m_byUpdateSize;
	char 		  m_pUpdateInfos[0];

	static const BYTE ms_SubCmd = 4;
};

/// 请求创建房间
struct MODI_C2GS_Request_CreateRoom: public MODI_Room_Cmd
{
    MODI_C2GS_Request_CreateRoom()
    {
        byParam = ms_SubCmd;
		memset( m_cstrRoomName , 0 , sizeof(m_cstrRoomName) );
		m_roomType = 0;
		memset( m_cstrPassword , 0 , sizeof(m_cstrPassword) );
		m_MusicID = RANDOM_MUSIC_ID;
		m_MapID = RANDOM_MAP_ID;
		m_wdSceneID = 0;
		m_bIsOffSpectator = false;
    }

    char m_cstrRoomName[ROOM_NAME_MAX_LEN + 1]; //  房间名
    defRoomType m_roomType; //  房间模式
    char m_cstrPassword[ROOM_PWD_MAX_LEN + 1]; // 房间密码
	defMusicID 	m_MusicID; 				// 音乐id
	defMapID 	m_MapID; 				// 游戏场景id
	WORD 		m_wdSceneID; 		/// 等待房间场景id
	bool 		m_bIsOffSpectator; // 是否关闭观战功能

	static const BYTE ms_SubCmd = 5;
};

/// 创建房间的应答，只有成功才收到
struct MODI_C2GS_Response_CreateRoom: public MODI_Room_Cmd
{
    MODI_C2GS_Response_CreateRoom()
    {
        byParam = ms_SubCmd;
    }

    MODI_RoomInfo m_roomInfo;
	defRoomPosSolt 	m_selfPos; 	// 自己的位置

	static const BYTE ms_SubCmd = 6;
};

/// 请求进入房间
struct MODI_C2GS_Request_JoinRoom: public MODI_Room_Cmd
{
    MODI_C2GS_Request_JoinRoom()
    {
        byParam = ms_SubCmd;
		m_roomID = INVAILD_ROOM_ID;
		memset( m_cstrPassword, 0 , sizeof(m_cstrPassword) );
    }

    defRoomID m_roomID;//  房间ID
    char m_cstrPassword[ROOM_PWD_MAX_LEN + 1];//  进入房间的密码（没有密码则不填)

	static const BYTE ms_SubCmd = 7;
};

///服务器同步角色普通信息(进入房后，服务器会同步的角色信息)
struct MODI_GS2C_Notify_CreateClient: public MODI_Room_Cmd
{
	MODI_GS2C_Notify_CreateClient()
    {
        byParam = ms_SubCmd;
		m_fPosX = 0.0f;
		m_fPosZ = 0.0f;
		m_fSped = 0.0f;
		m_fDir = 0.0f;
		m_eVolType = enKeyDownVol_None;
    }
	
	float m_fPosX;
	float m_fPosZ;
	float m_fDir;

	float m_fSped;    //  键盘模式的速度
	enKeyDownVolType   m_eVolType;  //键盘模式按键声音的类型

	MODI_RoleInfo_Base m_baseInfo; //  基本信息
	MODI_RoleInfo_Normal m_normalInfo; //  普通信息
	
	static const BYTE ms_SubCmd = 8;
};

/// 客户端请求离开房间
struct MODI_C2GS_Request_LeaveRoom: public MODI_Room_Cmd
{
    MODI_C2GS_Request_LeaveRoom()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 9;
};

///   有玩家离开房间(相当于 DeleteClient )
struct MODI_GS2C_Notify_LeaveRoom: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_LeaveRoom()
    {
        byParam = ms_SubCmd;
		m_byLeaveReason = 0;
    }

    MODI_GUID m_objID;
    BYTE m_byLeaveReason;

	static const BYTE ms_SubCmd = 10;
};

///   同步新的房主
struct MODI_GS2C_Notify_NewMaster: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_NewMaster()
    {
        byParam = ms_SubCmd;
		m_roomID = INVAILD_ROOM_ID;
    }

    MODI_GUID m_objID;
    defRoomID m_roomID;

	static const BYTE ms_SubCmd = 11;
};

///  参战玩家 准备
struct MODI_C2GS_Request_Ready: public MODI_Room_Cmd
{
    MODI_C2GS_Request_Ready()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 12;
};

/// 取消准备
struct MODI_C2GS_Request_UnReady: public MODI_Room_Cmd
{
    MODI_C2GS_Request_UnReady()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 13;
};

/// 房主开始游戏
struct MODI_C2GS_Request_Start: public MODI_Room_Cmd
{
    MODI_C2GS_Request_Start()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 14;
};

///    房主踢人
struct MODI_C2GS_Request_KickPlayer: public MODI_Room_Cmd
{
    MODI_C2GS_Request_KickPlayer()
    {
        byParam = ms_SubCmd;
		m_kickPos = INVAILD_POSSOLT;
    }

    defRoomPosSolt m_kickPos;//  要踢的人所在的位置
	static const BYTE ms_SubCmd = 15;
};

/// 房主开位置
struct MODI_C2GS_Request_OnPos: public MODI_Room_Cmd
{
    MODI_C2GS_Request_OnPos()
    {
        byParam = ms_SubCmd;
		m_targetPos = INVAILD_POSSOLT;
    }

    defRoomPosSolt m_targetPos;//  要开的目标位置
	static const BYTE ms_SubCmd = 16;
};

// /    房主关位置
struct MODI_C2GS_Request_OffPos: public MODI_Room_Cmd
{
    MODI_C2GS_Request_OffPos()
    {
        byParam = ms_SubCmd;
		m_targetPos = INVAILD_POSSOLT;
    }

    defRoomPosSolt m_targetPos;//  要关的目标位置
	static const BYTE ms_SubCmd = 17;
};

///   交换位置
struct MODI_C2GS_Request_ChangePostion: public MODI_Room_Cmd
{
    MODI_C2GS_Request_ChangePostion()
    {
        byParam = ms_SubCmd;
    }
	static const BYTE ms_SubCmd = 18;
};

///   房主请求更换音乐
struct MODI_C2GS_Request_ChangeMusic: public MODI_Room_Cmd
{
    MODI_C2GS_Request_ChangeMusic()
    {
        byParam = ms_SubCmd;
		m_musicID = RANDOM_MUSIC_ID;
    }

    defMusicID m_musicID; // 音乐ID
	enMusicFlag 	m_flag;
	enMusicSType 	m_songType;
	enMusicTime m_mt;
	static const BYTE ms_SubCmd = 19;
};

/// 	房主音乐改变的通知
struct MODI_GS2C_Notify_MusicChanged: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_MusicChanged()
    {
        byParam = ms_SubCmd;
		m_musicID = RANDOM_MUSIC_ID;
		m_bRandom = false;
    }

    defMusicID m_musicID;
	enMusicFlag 	m_flag;
	enMusicSType 	m_songType;
	enMusicTime m_mt;
	bool 		m_bRandom;
	static const BYTE ms_SubCmd = 20;
};

///   房主请求更换场景
struct MODI_C2GS_Request_ChangeMap: public MODI_Room_Cmd
{
    MODI_C2GS_Request_ChangeMap()
    {
        byParam = ms_SubCmd;
		m_MapID = 0;
    }

    defMapID m_MapID; //地图ID
	static const BYTE ms_SubCmd = 21;
};

///   房主改变地图的通知
struct MODI_GS2C_Notify_MapChanged: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_MapChanged()
    {
        byParam = ms_SubCmd;
		m_MapID = 0;
		m_bRamdom = false;
    }

    defMapID m_MapID;
	bool 	m_bRamdom;
	static const BYTE ms_SubCmd = 22;
};

///   更换位置的通知
struct MODI_GS2C_Notify_PostionChagned: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_PostionChagned()
    {
        byParam = ms_SubCmd;
		m_srcPos = INVAILD_POSSOLT;
		m_targetPos = INVAILD_POSSOLT;
    }

    defRoomPosSolt m_srcPos;// 交换的源位置
    defRoomPosSolt m_targetPos; // 目标位置
	static const BYTE ms_SubCmd = 23;
};

///   通知客户端准备加载前半部分资源
struct MODI_GS2C_Notify_ReadyPlay: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_ReadyPlay()
    {
		m_roomid = INVAILD_ROOM_ID;
		m_musicid = RANDOM_MUSIC_ID;
        byParam = ms_SubCmd;
    }

	defRoomID 		m_roomid;
	defMusicID 		m_musicid;
	enMusicFlag 	m_flag;
	enMusicSType 	m_songType;
	enMusicTime 	m_mt;
	defMapID 		m_mapid;


	static const BYTE ms_SubCmd = 24;
};



/// 客户端告诉服务器， 后半部分资源加载完毕
struct MODI_C2GS_Request_IamReady: public MODI_Room_Cmd
{
    MODI_C2GS_Request_IamReady()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 25;
};

/// 服务器通知客户端，开始游戏
struct MODI_GS2C_Notify_StartGame: public MODI_Room_Cmd
{
    MODI_GS2C_Notify_StartGame()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 26;
};

/// 服务器通知客户端，你被踢了
struct MODI_GS2C_Notify_BeKick : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_BeKick()
	{
        byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 27;
};

/// 服务器通知客户端，某个玩家后半部分资源准备好了(资源加载就绪)
struct MODI_GS2C_Notify_SbReady : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_SbReady()
	{
		byParam = ms_SubCmd;
	}

	MODI_GUID      m_nClientID; // 客户端的GUID
	static const BYTE ms_SubCmd = 28;
};

/// 服务器通知客户端，某个玩家观察的歌手改变了
struct MODI_GS2C_Notify_SbObSingerChanged : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_SbObSingerChanged()
	{
        byParam = ms_SubCmd;
	}

	MODI_GUID m_nObserverGUID; // 被观察对象的GUID(有可能是INVAILD_GUID)
   	MODI_GUID m_nTargetGUID; //观察者的GUID 
	static const BYTE ms_SubCmd = 29;
};

/// 服务器下发游戏结果
struct MODI_GS2C_Notify_GameResult : public MODI_Room_Cmd
{
	BYTE   m_bySize;
	MODI_GameResult m_pResults[0];

	MODI_GS2C_Notify_GameResult()
	{
        byParam = ms_SubCmd;
		m_bySize = 0;
	}
	static const BYTE ms_SubCmd = 30;
};

/// 服务器通知客户端，某个玩家的准备状态改变了
struct MODI_GS2C_Notify_SbReadyStateChanged : public MODI_Room_Cmd
{
	MODI_GUID 		m_nClientGUID;
	BYTE  m_byReady;
	MODI_GS2C_Notify_SbReadyStateChanged()
	{
        byParam = ms_SubCmd;
		m_byReady = 0;
	}
	static const BYTE ms_SubCmd = 31;
};

/// 服务器通知客户端,某个位置的开关状态改变了
struct MODI_GS2C_Notify_PostionStateChagned : public MODI_Room_Cmd
{
	defRoomPosSolt m_nTargetPos;
	BYTE m_byClosed;
	MODI_GS2C_Notify_PostionStateChagned()
	{
        byParam = ms_SubCmd;
		m_nTargetPos = INVAILD_POSSOLT;
		m_byClosed = 0;
	}

	static const BYTE ms_SubCmd = 32;
};

///  客户端告知服务器，一句歌词的分数
struct MODI_C2GS_Request_SentenceScore : public MODI_Room_Cmd
{
	WORD m_nNo; // 第几句
	DWORD m_nScore;// 分数
	BYTE m_nType; // 成绩类型，GOOD BAD 等
	float m_fExact; // 准确度
	float m_fRightSec; // 唱对几秒 
	WORD m_nCombat; // 连击数
	float m_fTime;
	QWORD m_nFlag; // Duplicate packet detection flag

	MODI_C2GS_Request_SentenceScore()
	{
        byParam = ms_SubCmd;
		m_nNo = 0;
		m_nScore = 0;
		m_nType = enScore_Begin;
		m_fExact = 0.0f;
		m_fRightSec = 0.0f;
		m_nCombat = 0;
		m_fTime = 0.0f;
		m_nFlag = 0;
	}
	static const BYTE ms_SubCmd = 33;
};

/// 服务器同步客户端，某个客户端一句歌词的分数
struct MODI_GS2C_Notify_SentenceScore : public MODI_Room_Cmd 
{
	WORD m_nNo; // 第几句
	MODI_GUID m_nClient; // 哪个客户端
	DWORD m_nScore; // 当前的总分数
	WORD m_nCombat; // 连击数
	BYTE m_byEvaluation; // 当前评价 
	BYTE m_byEffect; // 是否有效果，SHOW TIME 
	float m_fTime;
	QWORD m_nFlag; // Duplicate packet detection flag

	MODI_GS2C_Notify_SentenceScore()
	{
        byParam = ms_SubCmd;
		m_nNo = 0;
		m_nClient = INVAILD_GUID;
		m_nScore = 0;
		m_nCombat = 0;
		m_byEvaluation = 0;
		m_byEffect = 0;
		m_fTime = 0.0f;
		m_nFlag = 0;
	}
	static const BYTE ms_SubCmd = 34;
};

/// 客户端请求改变房间信息
struct MODI_C2GS_Request_ChangeRoomInfo : public MODI_Room_Cmd
{
	char 	m_szRoomName[ROOM_NAME_MAX_LEN + 1]; // 新的房间名，不改变则不填
	bool 	m_bHasPwd; //有没有密码 
	char 	m_szRoomPwd[ROOM_PWD_MAX_LEN + 1]; // 新的房间密码，如果m_bHasPwd被设，则该字段必须有内容
	char  	m_ucNewOffSpectator; // 是否允许观战, 0 不改变， 1 允许 ， －1 不允许
	
	MODI_C2GS_Request_ChangeRoomInfo()
	{
        byParam = ms_SubCmd;
		memset( m_szRoomName , 0 , sizeof(m_szRoomName) );
		memset( m_szRoomPwd , 0 , sizeof(m_szRoomPwd) );
		m_ucNewOffSpectator = 0;
	}
	static const BYTE ms_SubCmd = 35;
};

/// 服务器通知房间中的玩家，房间信息改变
struct MODI_GS2C_Notify_RoomInfoChanged : public MODI_Room_Cmd
{
	char 	m_szRoomName[ROOM_NAME_MAX_LEN + 1]; // 新的房间名，不改变则不填
	bool 	m_bHasPwd;// 是否有密码
	char  	m_ucNewOffSpectator; // 是否允许观战, 0 不改变， 1 允许 ， －1 不允许
	
	MODI_GS2C_Notify_RoomInfoChanged()
	{
        byParam = ms_SubCmd;
		memset( m_szRoomName , 0 , sizeof(m_szRoomName) );
		m_bHasPwd = false;
		m_ucNewOffSpectator = 0;
	}
	static const BYTE ms_SubCmd = 36;
};

/// 客户端告知服务器，某个效果开始
struct MODI_C2GS_Request_EffectBegin : public MODI_Room_Cmd
{
	BYTE m_byEffectType;

	MODI_C2GS_Request_EffectBegin()
	{
        byParam = ms_SubCmd;
		m_byEffectType = enEffect_None;
	}
	static const BYTE ms_SubCmd = 37;
};

/// 服务器同步某个客户端开始某个效果
struct MODI_GS2C_Notify_EffectBegin : public MODI_Room_Cmd
{
	MODI_GUID m_nClientID;
	BYTE m_byEffectType;

	MODI_GS2C_Notify_EffectBegin()
	{
        byParam = ms_SubCmd;
		m_nClientID = INVAILD_GUID;
		m_byEffectType = enEffect_None;
	}
	static const BYTE ms_SubCmd = 38;
};



/// 客户端告知服务器，某个效果结束
struct MODI_C2GS_Request_EffectOver : public MODI_Room_Cmd
{
	BYTE m_byEffectType;

	MODI_C2GS_Request_EffectOver()
	{
        byParam = ms_SubCmd;
		m_byEffectType = enEffect_None;
	}
	static const BYTE ms_SubCmd = 39;
};

/// 服务器同步某个客户端的某个效果结束
struct MODI_GS2C_Notify_EffectOver : public MODI_Room_Cmd
{
	MODI_GUID 		m_nClientID;
	BYTE  			m_byEffectType;

	MODI_GS2C_Notify_EffectOver()
	{
        byParam = ms_SubCmd;
		m_nClientID = INVAILD_GUID;
		m_byEffectType = enEffect_None;
	}
	static const BYTE ms_SubCmd = 40;
};


/// 邀请某个玩家游戏
struct MODI_C2GS_Request_InvitePlay : public MODI_Room_Cmd
{
	MODI_GUID 	m_nTarget; // 邀请的目标玩家

	MODI_C2GS_Request_InvitePlay()
	{ 
        byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 41;
};

/// 服务器通知客户端某个玩家邀请你
struct MODI_GS2C_Notify_SbInviteYou : public MODI_Room_Cmd
{
	char 			m_szHostName[ROLE_NAME_MAX_LEN + 1]; // 邀请发起者的名字
	char 			m_szRoomName[ROOM_NAME_MAX_LEN + 1];
	defRoomID 		m_nRoomID; // 房间ID 
	defRoomStateMask  m_roomState; // 房间状态

	MODI_GS2C_Notify_SbInviteYou()
	{ 
        byParam = ms_SubCmd;
		memset( m_szHostName , 0 , sizeof(m_szHostName) );
		memset( m_szRoomName , 0 , sizeof(m_szRoomName) );
		m_nRoomID = INVAILD_ROOM_ID;
		m_roomState = 0;
	}
	static const BYTE ms_SubCmd = 42;

};

/// 客户端对邀请的回复
struct MODI_C2GS_Request_InvitePlayRes : public MODI_Room_Cmd
{
	enum
	{
		enNo , // 不
		enNoAndForver, // 不，并且拒绝所有邀请
		enSure, // 好滴
	};

	BYTE 		m_byResult;

	MODI_C2GS_Request_InvitePlayRes()
	{
        byParam = ms_SubCmd;
		m_byResult = enNo;
	}
	
	static const BYTE 	ms_SubCmd = 43;
};

// 客户端同步G槽
struct MODI_C2GS_Request_SyncGPower : public MODI_Room_Cmd
{
	bool 	m_bRelease;
	float 	m_fSynctime;

	MODI_C2GS_Request_SyncGPower()
	{
        byParam = ms_SubCmd;
		m_fSynctime = 0.0f;
		m_bRelease = false;
	}

	static const BYTE 	ms_SubCmd = 44;
};

// 服务器同步G槽给听歌的客户端
struct MODI_GS2C_Notify_SyncGPower : public MODI_Room_Cmd
{
	MODI_GUID 	m_guidClient;
	bool 	m_bRelease;
	float 	m_fSynctime;

	MODI_GS2C_Notify_SyncGPower()
	{
        byParam = ms_SubCmd;
		m_fSynctime = 0.0f;
		m_bRelease = false;
	}

	static const BYTE 	ms_SubCmd = 45;
};

// 客户端告知服务器听歌结束
struct MODI_C2GS_Request_ListenersOver : public MODI_Room_Cmd
{
	MODI_C2GS_Request_ListenersOver()
	{
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 46;
};

/// 开始投票
struct MODI_GS2C_Notify_BeginVote : public MODI_Room_Cmd
{
	DWORD 	m_nVoteTime; 	// 	投票时间

	MODI_GS2C_Notify_BeginVote()
	{
        byParam = ms_SubCmd;
		m_nVoteTime = 0; 
	}

	static const BYTE 	ms_SubCmd = 47;
};

// 展示游戏结果
struct MODI_GS2C_Notify_ShowGameResult : public MODI_Room_Cmd
{
	MODI_GUID 		m_renqiKing; 		// 	人气王
	DWORD 			m_nShowTime; 	// 	show时间

	MODI_GS2C_Notify_ShowGameResult()
	{
        byParam = ms_SubCmd;
		m_nShowTime = 0; 	
	}

	static const BYTE 	ms_SubCmd = 48;
};

// 游戏结束，返回等待房间
struct MODI_GS2C_Notify_GameMatchEnd : public MODI_Room_Cmd
{
	bool 			m_bBackRoom; // 是否回到房间，true回房间，false开始倒计时
	DWORD 			m_nTime;  		// 	倒计时时间
	MODI_GS2C_Notify_GameMatchEnd()
	{
		m_bBackRoom = false;
		m_nTime = 0;
        byParam = ms_SubCmd;
	}
	static const BYTE 	ms_SubCmd = 49;
};

// 请求随机进入房间
struct MODI_C2GS_Request_RandomJoinRoom: public MODI_Room_Cmd
{
    MODI_C2GS_Request_RandomJoinRoom()
    {
        byParam = ms_SubCmd;
	
		m_type = ROOM_TYPE_NORMAL; 	// 大类型
		m_subtype = ROOM_STYPE_BEGIN; 	// 子类型
		m_musicid = INVAILD_CONFIGID;  // 音乐id
		m_mapid = INVAILD_CONFIGID;  	// 场景id
    }

	ROOM_TYPE 		m_type; 	// 大类型
	ROOM_SUB_TYPE 	m_subtype; 	// 子类型
	defMusicID 		m_musicid;  // 音乐id
	defMapID 		m_mapid;  	// 场景id

	static const BYTE ms_SubCmd = 52;
};

// 投票给玩家
struct MODI_C2GS_Request_VoteTo : public MODI_Room_Cmd
{
	MODI_C2GS_Request_VoteTo()
	{
        byParam = ms_SubCmd;

	}

	MODI_GUID 		m_player;  // 要投给的玩家

	static const BYTE ms_SubCmd = 53;
};

// 服务器通知投票事件，谁给谁投了一票
struct MODI_GS2C_Notify_SbVoteTo : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_SbVoteTo()
	{
        byParam = ms_SubCmd;
	}

	MODI_GUID 		m_voter; // 投票的人
	MODI_GUID 		m_player;  // 投给谁

	static const BYTE ms_SubCmd = 54;
};

// 同步投票结果
struct MODI_GS2C_Notify_VoteResult : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_VoteResult()
	{
        byParam = ms_SubCmd;
		votecount = 0;
	}

	MODI_GUID 	player; // 参战玩家
	BYTE 		votecount; // 票数

	static const BYTE ms_SubCmd = 55;
};

// 请求获得mdm数据在单人游戏中
struct MODI_C2GS_Request_GetMdm : public MODI_Room_Cmd
{
	defMusicID 	m_musicid; // 所选的音乐

	MODI_C2GS_Request_GetMdm()
	{
        byParam = ms_SubCmd;
		m_musicid = INVAILD_CONFIGID;
	}

	static const BYTE ms_SubCmd = 57;
};

// 单人模式中将MDM文件同步给客户端
struct MODI_GS2C_Notify_MdmFile : public MODI_Room_Cmd
{
	defMusicID 	m_musicid;
	DWORD m_nSize; // MDM数据大小
	char m_pMdmData[0]; // mdm实际数据

	MODI_GS2C_Notify_MdmFile()
	{
        byParam = ms_SubCmd;
		m_nSize = 0;
		m_musicid = INVAILD_CONFIGID;
	}

	static const BYTE ms_SubCmd = 58;
};

// 进入房间成功
struct MODI_GS2C_Notify_JoinRoomSuccessful : public MODI_Room_Cmd
{
	MODI_GUID 			master; // 房主
	defRoomID 			m_roomid; 		// 房间ID 
	bool 				m_bIsPlaying;  	// 	是否游戏中进入房间
	defRoomPosSolt 		m_selfPos;  	// 自己的位置

	MODI_GS2C_Notify_JoinRoomSuccessful()
	{
        byParam = ms_SubCmd;
		m_roomid = INVAILD_ROOM_ID;
		m_bIsPlaying = false;
		m_selfPos = INVAILD_POSSOLT;
	}

	static const BYTE ms_SubCmd = 59;
};

///   房主请求更换场景
struct MODI_C2GS_Request_ChangeGameMode : public MODI_Room_Cmd
{
	ROOM_SUB_TYPE m_newSubType;
	MODI_C2GS_Request_ChangeGameMode()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 60;
};

///   房主改变地图的通知
struct MODI_GS2C_Notify_GameModeChanged : public MODI_Room_Cmd
{
	ROOM_SUB_TYPE m_newSubType;
	MODI_GS2C_Notify_GameModeChanged()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 61;
};

// 转让房主
struct MODI_C2GS_Request_TransferMaster : public MODI_Room_Cmd
{
	MODI_GUID 	new_master; // 新房主

	MODI_C2GS_Request_TransferMaster()
	{
        byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 62;
};


/// 通知组队情况
struct MODI_GS2C_Notify_RoomTeam_State: public MODI_Room_Cmd
{
	MODI_GS2C_Notify_RoomTeam_State()
	{
		byParam = ms_SubCmd ;
		memset(m_byState, 0, sizeof(m_byState));
	}

	BYTE m_byState[MAX_PLAYSLOT_INROOM]; /// 6个位置的状态

	static const BYTE ms_SubCmd = 63;
};


///  参战玩家改变组队情况
struct MODI_C2GS_Request_ChangeTeam: public MODI_Room_Cmd
{
    MODI_C2GS_Request_ChangeTeam()
    {
        byParam = ms_SubCmd;
		m_byChangeTeam = (BYTE)ROOM_TEAM_BEGIN;
    }

	BYTE m_byChangeTeam;

	static const BYTE ms_SubCmd = 64;
};


//客户端通知服务器前半部分资源加载完毕
struct MODI_C2GS_Request_FirstHalfReady:public MODI_Room_Cmd
{
	MODI_C2GS_Request_FirstHalfReady()
	{
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 66;
};


//服务器通知客户端加载后半部分资源
struct MODI_GS2C_Notify_LoadSecondHalf : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_LoadSecondHalf()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 70;
};
///  客户端告知服务器，一个按键的得分
struct MODI_C2GS_Request_OneKeyScore : public MODI_Room_Cmd
{
	DWORD m_nScore;// 分数
	BYTE m_nType; // 成绩类型，GOOD BAD 等
	float m_fExact; // 准确度
	WORD m_nCombat; // 连击数
	float m_fTime;
	float m_tFever;
	QWORD m_nFlag; // Duplicate packet detection flag

	MODI_C2GS_Request_OneKeyScore()
	{
        byParam = ms_SubCmd;
		m_nScore = 0;
		m_nType = enScore_Begin;
		m_fExact = 0.0f;
		m_nCombat = 0;
		m_fTime = 0.0f;
		m_tFever=0.0f;
		m_nFlag = 0;
	}
	static const BYTE ms_SubCmd = 72;
};


/// 服务器同步客户端，某个客户端一个按键的分数
struct MODI_GS2C_Notify_OneKeyScore : public MODI_Room_Cmd 
{
	MODI_GUID m_nClient; // 哪个客户端
	DWORD m_nScore; // 当前的总分数
	WORD m_nCombat; // 连击数
	BYTE m_byEvaluation; // 当前评价 
	BYTE m_byEffect; // 是否有效果，SHOW TIME 
	float m_fTime;
	float m_tFever;
	QWORD m_nFlag; // Duplicate packet detection flag

	MODI_GS2C_Notify_OneKeyScore()
	{
        byParam = ms_SubCmd;
		m_nClient = INVAILD_GUID;
		m_nScore = 0;
		m_nCombat = 0;
		m_byEvaluation = 0;
		m_byEffect = 0;
		m_fTime = 0.0f;
		m_tFever=0.0f;
		m_nFlag = 0;
	}
	static const BYTE ms_SubCmd = 80;
};


///	客户端请求修改键盘模式的参数
struct MODI_C2GS_Request_ModifyKeyModeParam: public MODI_Room_Cmd 

{

	MODI_C2GS_Request_ModifyKeyModeParam()
	{
		m_fSped = 0.0f;
		m_eVolType = enKeyDownVol_None;
		byParam = ms_SubCmd;
	}
	float  m_fSped;
	enKeyDownVolType  m_eVolType;

	static const BYTE ms_SubCmd = 84;

};

///	服务器广播某个客户端的键盘模式参数改变
struct  MODI_GS2C_Notify_SbKeyModeParamChange : public MODI_Room_Cmd
{
	MODI_GS2C_Notify_SbKeyModeParamChange()
	{
		m_fSped = 0.0f;
		m_eVolType = enKeyDownVol_None;
		byParam = ms_SubCmd;
	}
	MODI_GUID	m_Guid;
	
	float  m_fSped;
	enKeyDownVolType  m_eVolType;
	static const BYTE ms_SubCmd = 86;
};

/// ispeak标识
struct MODI_ISpeak_Id
{
	MODI_ISpeak_Id()
	{
		m_stGuid = INVAILD_GUID;
		m_stISpeakId = 0;
	}
	
	/// 玩家id
	MODI_GUID m_stGuid;
	/// ispeak标识
	int m_stISpeakId;
};

/// 服务器转发is语音id
struct MODI_C2GS_Enter_ISpeak: public MODI_Room_Cmd
{
	MODI_C2GS_Enter_ISpeak()
	{
		byParam = ms_SubCmd;
	}
	
	MODI_ISpeak_Id m_stId;
	static const BYTE ms_SubCmd = 87;
};


/// 通知用户
struct MODI_GS2C_Notify_ISpeak: public MODI_Room_Cmd
{
	MODI_GS2C_Notify_ISpeak()
	{
		m_wdSize = 0;
		byParam = ms_SubCmd;
	}
	
	/// 玩家个数
	WORD m_wdSize;
	MODI_ISpeak_Id m_pData[0];
	
	static const BYTE ms_SubCmd = 88;
};
	
#pragma pack(pop)

#endif

