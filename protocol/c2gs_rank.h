#ifndef C2GS_RANKCMD_DEFINED_H_
#define C2GS_RANKCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_RankCmd : public stNullCmd
{
	MODI_RankCmd()
	{
        byCmd = MAINCMD_RANK;
	}
};

// 客户端请求排行榜
struct MODI_C2GS_Request_Rank : public MODI_RankCmd
{
	enRankType rank_type; // 排行榜类型
	QWORD 	rank_ver; // 排行棒版本号
	WORD 	rank_begin; // 从第几名开始
	WORD 	rank_end; // 到第几名结束

	MODI_C2GS_Request_Rank()
	{
		rank_type = kRank_Begin; // 排行榜类型
		rank_ver = RANK_VER_FOR_INITIAL; // 排行棒版本号
		rank_begin = 0; // 从第几名开始
		rank_end = 0; // 到第几名结束
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};

// 服务器返回排行榜信息
struct MODI_GS2C_Notify_Rank : public MODI_RankCmd
{
	enRankType rank_type; // 排行榜类型
	enReqRankResult result;
	QWORD 	rank_ver; // 排行棒版本号
	WORD 	rank_begin; // 从第几名开始
	WORD 	rank_end; // 到第几名结束
	char * 	ranks[0]; // 具体信息

	MODI_GS2C_Notify_Rank()
	{
		rank_type = kRank_Begin; // 排行榜类型
		rank_ver = RANK_VER_FOR_INITIAL; // 排行棒版本号
		rank_begin = 0; // 从第几名开始
		rank_end = 0; // 到第几名结束
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 2;
};

// 客户端请求排行榜中的某个具体名次的角色的具体信息
struct MODI_C2GS_Request_SpecificRank : public MODI_RankCmd
{
	enRankType rank_type; // 排行榜类型
	WORD 	rank_no; // 第几名

	MODI_C2GS_Request_SpecificRank()
	{
		rank_type = kRank_Begin; // 排行榜类型
		rank_no = 0; // 第几名
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 3;
};

struct MODI_GS2C_Notify_SpecificRank : public MODI_RankCmd
{
	enRankType rank_type; // 排行榜类型
	WORD 	rank_no; // 第几名
	enRankSpecificResult result; // 详细信息查询结果
	char 	specific_data[0]; // 具体信息数据

	MODI_GS2C_Notify_SpecificRank()
	{
		rank_type = kRank_Begin; // 排行榜类型
		rank_no = 0; // 第几名
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 4;
};

// 请求我的排名
struct MODI_C2GS_Request_SelfRank : public MODI_RankCmd
{
	MODI_C2GS_Request_SelfRank()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 5;
};

// 我的排名返回
struct MODI_GS2C_Notify_SelfRank : public MODI_RankCmd
{
	DWORD 	self_rank_level;
	DWORD 	self_rank_renqi;
	DWORD 	self_rank_consume;
	DWORD 	self_rank_highestscore;
	DWORD 	self_rank_highestprecision;
	DWORD	self_rank_keyscore;
	DWORD	self_rank_keyprecise;

	MODI_GS2C_Notify_SelfRank()
	{
		self_rank_level = SELF_RANK_OUT;
		self_rank_renqi = SELF_RANK_OUT;
		self_rank_consume= SELF_RANK_OUT;
		self_rank_highestscore= SELF_RANK_OUT;
		self_rank_highestprecision= SELF_RANK_OUT;
		self_rank_keyscore = SELF_RANK_OUT;
		self_rank_keyprecise = SELF_RANK_OUT;
		byParam = ms_SubCmd;

	}
	static const BYTE ms_SubCmd = 6;
};

#pragma pack(pop)

#endif
