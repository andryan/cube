/** 
 * @file c2gs_syscmd.h
 * @brief 系统相关的数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_SYSCMD_DEFINED_H_
#define C2GS_SYSCMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_System_Cmd : public stNullCmd
{
	MODI_System_Cmd()
	{
        byCmd = MAINCMD_SYSTEM;
	}
};

// 客户端PING
struct MODI_C2GS_Request_Ping : public MODI_System_Cmd
{
	DWORD 	m_nSendTime;

	MODI_C2GS_Request_Ping()
	{
		m_nSendTime = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 1;
};
 
//  服务器返回ping
struct MODI_GS2C_Notify_Ping : public MODI_System_Cmd
{
	DWORD 	m_nSendTime; // (nowtime - sendtime) / 2

	MODI_GS2C_Notify_Ping()
	{
		m_nSendTime = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 2;
};

// 方沉迷通知
struct MODI_GS2C_Notify_FangChengMi : public MODI_System_Cmd
{
	enum
	{
		enNormal, // 正常
		enTired, // 疲劳，收益减半
		enUnHelath, // 不健康
		enNotify, //通知
	};

	BYTE 	m_byState;
	DWORD 	m_nTodayPlayTime;

	MODI_GS2C_Notify_FangChengMi()
	{
		m_byState = enNormal;
		m_nTodayPlayTime = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 3;
};

/// 视频通过
struct MODI_GS2C_Notify_VideoPass : public MODI_System_Cmd
{
	MODI_GS2C_Notify_VideoPass()
	{
		memset(m_cstrName, 0, sizeof(m_cstrName));
		byParam = ms_SubCmd;
		m_wdValue = 0;
	}
	char m_cstrName[ROLE_NAME_MAX_LEN + 1];  //    目标名字
	WORD m_wdValue;
	static const BYTE ms_SubCmd = 4;
};



/// 分数异常
struct MODI_C2GS_Point_Error_Cmd : public MODI_System_Cmd
{
	MODI_C2GS_Point_Error_Cmd()
	{
		m_dwPoint = 0;
		byParam = ms_SubCmd;
	}
	DWORD m_dwPoint;
	static const BYTE ms_SubCmd = 5;
};

/// 退出游戏

struct MODI_C2GS_Request_Logout_Cmd : public MODI_System_Cmd
{
	MODI_C2GS_Request_Logout_Cmd()
	{
		byParam = ms_SubCmd;
	}
	static const BYTE ms_SubCmd = 8;
};

#pragma pack(pop)

#endif
