/** 
 * @file relation_def.h
 * @brief 联系人相关定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef RELATION_DEFINE_H_
#define RELATION_DEFINE_H_

#include "gamedefine.h"
//VIP最大好友个数
#define MAX_VIPFRIENDSCOUNT	300


#define MAX_FRIENDCOUNT 	150
#define MAX_BLACKCOUNT 	 	50	

//	vip最大的偶像个数
#define	MAX_VIPIDOLCOUNT		300

// 最大偶像个数
#define MAX_IDOLCOUNT 		150

/// 全民偶像条件
const DWORD QUANMIN_OUXIANG_COUNT = 6000;
/// 被拉黑的条件和称号ID
const DWORD LAIPI_XIAOREN_COUNT = 100;
const DWORD QUANMIN_GONGDI_COUNT = 500;
const DWORD ZHONGPAN_QINLI_COUNT = 1000;

/// 特殊称号
const WORD TESHU_CHENGHAO_ID = 1000;
const WORD INVALID_CHENGHAO_ID = 0;

/// 全民偶像称号ID
const WORD QUANMIN_OUXIANG_ID = 200;
/// 赖皮小人
const WORD LAIPI_XIAOREN_ID = 201;
/// 全民公敌
const WORD QUANMIN_GONGDI_ID = 202;
/// 众叛亲离
const WORD ZHONGPAN_QINLI_ID = 203;

///	GM称号ID
const WORD GM_CHENGHAO_ID  = 2001;

// 联系人类型
enum	enRelationType
{
	enRelationT_None = 0, // 没有任何关系，包括偶像关系
	enRelationT_FriendRelationMask = 0x7f, 	

	enRelationT_Black = 1, // 黑名单
	enRelationT_Friend = 2, // 好友
	enRelationT_Lover = 3,  // 情人
	enRelationT_Married = 4, // 夫妻


	enRelationT_Idol = 0x80, // 偶像
};

// 操作类型
enum enRelOptType
{
	enRelOpt_Begin = 0,
	enRelOpt_AddF,  // 添加为好友
	enRelOpt_DelF,  // 删除好友
	enRelOpt_AddB,  // 添加到黑名单
	enRelOpt_DelB,  // 从黑名单中删除
	enRelOpt_AddIdol, // 添加为偶像
	enRelOpt_DelIdol, // 删除偶像
	enRelOpt_End,
};


// 服务器回复类型
enum enRelationRetType
{
	enRelationRetT_Ok = 0, // 成功
	enRelationRetT_NotOnline, // 目标不在线
	enRelationRetT_Add_NotExist, // 添加联系人失败,目标不存在
	enRelationRetT_Add_AlreadyIn, // 添加联系人失败，已经是联系人
	enRelationRetT_AddF_Full, // 添加好友失败，好友列表满
	enRelationRetT_AddB_Full, // 添加黑名单失败，黑名单满
	enRelationRetT_AddI_ArriveTime, // 添加偶像失败，偶像添加时间间隔未到
	enRelationRetT_AddI_Full, // 添加偶像失败，偶像满
	enRelationRetT_AddI_Level, // 添加偶像失败，等级不够
	enRelationRetT_AddF_IsInBlack, // 添加好友失败，对方在黑名单中（先从删除联系人)
	enRelationRetT_AddI_IsInBlack, // 添加偶像失败，对方在黑名单中（先从删除联系人)
	enRelationRetT_Add_AlreadyIsF, // 添加好友失败，已经是好友
	enRelationRetT_Add_AlreadyIsB, // 添加黑名单失败 ，已经是黑名单
	enRelationRetT_Add_AlreadyIsI, // 添加偶像失败 ，已经是偶像
	enRelationRetT_Mod_NotExist, // 修改联系人的关系类型失败，目标不存在于联系人列表中
	enRelationRetT_DelF_NotExist, // 删除好友失败，不是好友
	enRelationRetT_DelI_NotExist, // 删除偶像失败，不是偶像
	enRelationRetT_DelB_NotExist, // 删除黑名单失败，不是黑名单
	enRelationRetT_CanMarry_LevelReq, // 无法嫁给你，未达到等级需求
	enRelationRetT_CanMarry_NotF, // 无法嫁给你，必须互为好友
	enRelationRetT_CanMarry_SelfNotSingle, // 自己不是单身
	enRelationRetT_CanMarry_TargetNotSingle, // 目标不是单身
	enRelationRetT_CanMarry_SexSame, // 性别一样
	enRelationRetT_DoMyGF_LevelReq, // 无法做你女／男 朋友，未到达等级需求
	enRelationRetT_DoMyGF_NotF, // 无法作你女／男朋友，必须互为好友
	enRelationRetT_DoMyGF_SelfNotSingle, // 自己不是单身
	enRelationRetT_DoMyGF_TargetNotSingle, // 目标不是单身
	enRelationRetT_DoMyGF_SexSame, // 性别一样
	enRelationRetT_Reqing_Honeny, // 正在求爱中（包括恋人和夫妻）
	enRelationRetT_UnknowError,
};

// 好友关系的额外信息
struct MODI_FriendExtraData
{

};

// 偶像关系的额外信息
struct MODI_IdolExtraData
{

};

// 联系人信息
struct MODI_RelationInfo
{
	MODI_GUID	m_guid;			//	联系人ID
	char		m_szName[ROLE_NAME_MAX_LEN + 1];	//	联系人名字
	BYTE		m_bySex;		//	联系人性别
	enRelationType	m_RelationType; // 关系类型
	bool 		m_bOnline; // 是否在线
	MODI_FriendExtraData 	m_FreindExtraData; // 好友的额外信息
	MODI_IdolExtraData 		m_IdolExtraData; // 偶像的额外信息

	struct TempData
	{
		char 	m_szServerName[MAX_SERVERNAME_LEN + 1]; // 服务器名字
		kGameState 	m_nGameState;  // 游戏状态
		defRoomID 	m_roomID;

		TempData()
		{
			memset( m_szServerName , 0 , sizeof(m_szServerName) );
			m_nGameState = kOutServer;
			m_roomID = 25;
		}
	};

	TempData 	m_TempData;

	MODI_RelationInfo()
	{
		memset( m_szName , 0 , sizeof(m_szName) );
		m_bySex = 0;
		m_RelationType = enRelationT_None;
		m_bOnline = false;
	}
};


/// 联系人状态改变（游戏状态相关）
struct MODI_RelationStateChanged_GameState
{
	kGameState 	m_nGameState;
	BYTE 	m_nRoomID;
};

/// 联系人状态改变（在线状态）
struct MODI_RelationStateChanged_OnlineState
{
	BYTE 	m_bOnline; // 是否在线
	char 	m_szServerName[MAX_SERVERNAME_LEN + 1]; // 服务器名字
};

/// 联系人状态改变
struct MODI_RelationStateChanged
{
	union
	{
		MODI_RelationStateChanged_GameState 	m_gameState;
		MODI_RelationStateChanged_OnlineState 	m_onlineState;
	};
};


// 关系系统相关帮助函数
namespace RelationHelpFuns
{

// 是否好友关系
inline bool IsFriend( const MODI_RelationInfo & info ) 
{
	BYTE byTemp = info.m_RelationType & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Friend;
}

// 是否好友关系
inline bool IsFriend( enRelationType type )
{
	BYTE byTemp = type & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Friend;
}

// 是否黑名单关系
inline bool IsBlack( const MODI_RelationInfo & info )
{
	BYTE byTemp = info.m_RelationType & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Black;
}

inline bool IsBlack( enRelationType type )
{
	BYTE byTemp = type & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Black;
}

// 是否情人关系
inline bool IsLover( const MODI_RelationInfo & info )
{
	BYTE byTemp = info.m_RelationType & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Lover;
}

inline bool IsLover( enRelationType type )
{
	BYTE byTemp = type & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Lover;
}

// 是否夫妻关系
inline bool IsMarried( const MODI_RelationInfo & info )
{
	BYTE byTemp = info.m_RelationType & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Married;
}

inline bool IsMarried( enRelationType type )
{
	BYTE byTemp = type & enRelationT_FriendRelationMask; 
	return byTemp == enRelationT_Married;
}

// 是否偶像关系
inline bool IsIdol( const MODI_RelationInfo & info )
{
    return info.m_RelationType & enRelationT_Idol ? true : false;
}

inline bool IsIdol( enRelationType type )
{
	return type & enRelationT_Idol ? true : false;
}

// 设置为好友关系
inline void SetFriend( int & bySet )
{
	bool b = IsIdol( (enRelationType)(bySet) );
	bySet = enRelationT_Friend;
	if( b )
		bySet |= enRelationT_Idol;
}

inline void SetFriend( enRelationType & type )
{
	int i = (int)type;
	SetFriend( i );
	type = (enRelationType)i;
}

// 	设置为黑名单关系
inline void SetBlack( int & bySet )
{
	bySet = enRelationT_Black;
}

inline void SetBlack( enRelationType & type )
{
	int i = (int)type;
	SetBlack( i );
	type = (enRelationType)i;
}

// 设置为情人关系
inline void SetLover( int & bySet )
{
	bool b = IsIdol( enRelationType(bySet) );
	bySet = enRelationT_Lover;
	if( b )
		bySet |= enRelationT_Idol;
}

inline void SetLover( enRelationType & type )
{
	int i = (int)type;
	SetLover( i );
	type = (enRelationType)i;
}

// 设置为夫妻关系
inline void SetMarried( int & bySet )
{
	bool b = IsIdol( enRelationType(bySet) );
	bySet = enRelationT_Married;
	if( b )
		bySet |= enRelationT_Idol;
}

inline void SetMarried( enRelationType & type )
{
	int i = (int)type;
	SetMarried( i );
	type = (enRelationType)i;
}

// 设置为偶像关系
inline void SetIdol( int & bySet )
{
	bySet |= enRelationT_Idol;
}

inline void SetIdol( enRelationType & type )
{
	int i = (int)type;
	SetIdol( i );
	type = (enRelationType)i;
}

// 清掉偶像关系
inline void ClearIdol( int & bySet )
{
	bySet &= ~enRelationT_Idol;
}

// 清掉所有关系
inline void ClearAll( int & bySet )
{
	bySet = enRelationT_None;
}


} // end namespace 


#endif
