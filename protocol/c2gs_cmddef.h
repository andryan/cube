/** 
 * @file c2gs_cmddef.h
 * @brief 一级命令定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */

#ifndef MODI_CMD_DEFINED_H_
#define MODI_CMD_DEFINED_H_

#ifdef _MSC_VER
#pragma once
#endif

#include "svrresult.h"

enum
{
C2GS_CMD_BEGIN = 0,
MAINCMD_OPTRESULT,
/// 主登录命令(c2gs.h)
MAINCMD_LOGIN,
/// 聊天命令(c2gs_chatcmd.h  3)
MAINCMD_CHAT,
/// 房间命令(c2gs_roomcmd.h 4)
MAINCMD_ROOM,
/// 角色相关命令(c2gs_rolecmd.h 5)
MAINCMD_ROLE,
/// 音乐数据相关命令(c2gs_musiccmd.h 6)
MAINCMD_MUSIC,
/// 关系系统  7
MAINCMD_RELATION,
/// 系统命令  8
MAINCMD_SYSTEM,
/// 物品系统 9
MAINCMD_ITEM,
/// 邮件  10
MAINCMD_MAIL, 
/// 商城  11 
MAINCMD_SHOP, 
// 排行榜  12
MAINCMD_RANK,
/// 兑换命令 13
MAINCMD_PAY,
//家族命令 14
MAINCMD_FAMILY,
/// 个人空间 15
MAINCMD_ZONE,
///	活动命令 16
MAINCMD_ACTIVE,
C2GS_CMD_END, 
};

#endif
