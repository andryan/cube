/** 
 * @file c2gs_rolecmd.h
 * @brief 角色相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_ROLECMD_DEFINED_H_
#define C2GS_ROLECMD_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine2.h"
#include "nullCmd.h"


#pragma pack(push,1)

struct MODI_Role_Cmd : public stNullCmd
{
	MODI_Role_Cmd()
	{
        byCmd = MAINCMD_ROLE;
	}
};


///  玩家列表通知
struct MODI_GS2C_Notify_Playerlist: public MODI_Role_Cmd
{
    WORD m_nSize;
    MODI_RoleInfo_Base m_pRoles[0];

    MODI_GS2C_Notify_Playerlist()
    {
        byParam = ms_SubCmd;
        m_nSize = 0;
    }


	static const BYTE ms_SubCmd = 1;
};

/// 服务器下发角色的所有信息
struct MODI_GS2C_Notify_RoleAllInfo: public MODI_Role_Cmd
{
	MODI_RoleInfo m_RoleAllInfo;

	MODI_GS2C_Notify_RoleAllInfo()
    {
        byParam = ms_SubCmd;
    }

	static const BYTE ms_SubCmd = 2;
};


/// 创建角色的请求
struct MODI_C2GS_Request_CreateRole : public MODI_Role_Cmd
{
	MODI_CreateRoleInfo 	m_createInfo;

	MODI_C2GS_Request_CreateRole()
	{
        byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 3;
};

struct MODI_C2GS_Request_SyncAvatar : public MODI_Role_Cmd
{
	// 客户端自己判断，最后确定后发送最新的AVATAR形象
	QWORD 	 	m_NewAvatar[MAX_BAG_AVATAR_COUNT];

	MODI_C2GS_Request_SyncAvatar()
	{
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 5;
};

// 更改角色信息
struct MODI_C2GS_Request_ModifyRoleInfo : public MODI_Role_Cmd
{
	DWORD 	m_nBirthday; // 生日
	char 	m_byXuexing; // 血型
	DWORD 	m_qq; // qq 
	char 	m_Sign[MAX_PERSONAL_SIGN + 1]; // 个性签名
	WORD 	m_nCityID; // 城市id

	MODI_C2GS_Request_ModifyRoleInfo() 
	{
		memset( m_Sign , 0 , sizeof(m_Sign) );
		m_nBirthday = 0;
		m_byXuexing = enOXingXue;
		m_qq = 0;
		m_nCityID = INVAILD_CITYID;

		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 9;
};

// 客户端请求某个角色的详细信息
struct MODI_C2GS_Request_RoleDetailInfo: public MODI_Role_Cmd
{
	char 				m_szName[ROLE_NAME_MAX_LEN + 1]; // 查看的对象的名字

	MODI_C2GS_Request_RoleDetailInfo()
	{
		memset( m_szName , 0 , sizeof(m_szName) );
		byParam = ms_SubCmd;
	}


	static const BYTE ms_SubCmd = 10;
};

// 服务器返回某个角色的详细信息
struct MODI_GS2C_Notify_RoleDetailInfo: public MODI_Role_Cmd
{
	enum
	{
		kSuccessful, // 成功
		kNotExist, // 不存在该角色
	};

	BYTE 			m_byResult;
	MODI_NameCard 	m_namecard; // 如果角色不存在，名字字段则代表客户端请求查询的角色名字
	BYTE		m_bVip;	///是否是VIP

	MODI_GS2C_Notify_RoleDetailInfo()
	{
		m_byResult = kSuccessful;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 11;
};

//  服务器通知客户端产生效果
struct MODI_GS2C_Notify_Impact : public MODI_Role_Cmd
{
	// 标志
	enum enFlag
	{
		kEnable,  // 开始效果
		kContinue, // 效果持续中（中途进入视野）
		kFadeout, // 效果淡出
	};

	MODI_GUID 		m_ImpactGUID; 	// 效果id
	MODI_GUID 	 	m_ReceiverGUID; // 效果接受者id
	MODI_GUID 		m_SenderGUID;   // 效果发送者id
	WORD 			m_nImpactConfigID;    // 效果configid
	enFlag			m_byFlag;	 	// 效果标志
	enImpactType 	m_Type; 	 	// 效果类型
	DWORD 			m_ContinueTime; // 持续时间
	DWORD 			m_nElapsed; 
	
	MODI_GS2C_Notify_Impact()
	{
		m_nImpactConfigID = INVAILD_CONFIGID;    
		m_Type = kImpactOnce;
		m_byFlag = kEnable;
		m_ContinueTime = 0;
		m_nElapsed = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 12;
};

// 服务器通知客户端作动作
struct MODI_GS2C_Notify_Action : public MODI_Role_Cmd
{
	MODI_GUID  		m_SrcGUID;  	// 动作发起者
	MODI_GUID 		m_DestGUID; 	// 动作接受目标
	WORD 			m_ActionConfigID; // 动作ID
	DWORD 			m_StartTime;  	// 动作开始的时间
	DWORD 			m_ActionTime;  // 动作持续时间

	MODI_GS2C_Notify_Action()
	{
		m_ActionConfigID = INVAILD_CONFIGID; // 动作ID
		m_StartTime = 0;
		m_ActionTime = 0;  // 动作的时间
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 13;
};

// 服务器通知对象更新
struct MODI_GS2C_Notify_UpdateObj : public MODI_Role_Cmd
{
	OBJTYPE 			m_objType; 		// 	对象类型
	MODI_GUID 			m_guid; 		// 需要更新对象
	MODI_ObjUpdateMask 	m_UpdateMask;  	// 更新掩码
	WORD 				m_nSize; 		// 	数据大小
	char 				m_pData[0]; 	// 	内容

	MODI_GS2C_Notify_UpdateObj()
	{
		m_nSize = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 14;
};

// 服务器通知客户端，某个对象升级
struct MODI_GS2C_Notify_LevelUp : public MODI_Role_Cmd
{
	MODI_GUID 		m_guid; 	// 对象
	BYTE 			m_byLevel; 	// 等级

	MODI_GS2C_Notify_LevelUp()
	{
		m_byLevel = 0;
		byParam = ms_SubCmd;
	}

	static const BYTE ms_SubCmd = 15;
};

struct MODI_GS2C_Notify_ImpactModfiyData : public MODI_Role_Cmd
{
	MODI_GUID 	m_Impactguid; // 效果对象
	float 		m_ModfiyScore; // 修改分数多少
	float 		m_ModfiyRenqi; // 修改人气多少
	DWORD		m_nConfig;
	
	MODI_GS2C_Notify_ImpactModfiyData()
	{
		m_ModfiyScore = 0;
		m_ModfiyRenqi = 0;
		m_nConfig = 0;
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 16;
};


/// 人物移动
struct MODI_C2GS_Request_Movement : public MODI_Role_Cmd
{
	enMovementFlags m_Flag;
	float 	m_fPosX;
	float 	m_fPosZ;
	float 	m_fDir;
	float 	m_fTime;
	DWORD 	m_nClientLogicCount;

	MODI_C2GS_Request_Movement()
	{
        byParam = ms_SubCmd;
		m_Flag = kMoveMentFlag_None;
		m_fPosX = 0.0f;
		m_fPosZ = 0.0f;
		m_fDir = 0.0f;
		m_fTime = 0.0f;
		m_nClientLogicCount = 0;
	}

	static const BYTE 	ms_SubCmd = 50;
};

/// 同步人物移动
struct MODI_GS2C_Notify_Movement : public MODI_Role_Cmd
{
	MODI_GUID 	m_obj;
	enMovementFlags m_Flag;
	float 	m_fPosX;
	float 	m_fPosZ;
	float 	m_fDir;
	float 	m_fTime;
	DWORD 	m_nClientLogicCount;
	DWORD 	m_nServerLogicCount;


	MODI_GS2C_Notify_Movement()
	{
        byParam = ms_SubCmd;
		m_Flag = kMoveMentFlag_None;
		m_fPosX = 0.0f;
		m_fPosZ = 0.0f;
		m_fDir = 0.0f;
		m_fTime = 0.0f;
		m_nClientLogicCount = 0;
		m_nServerLogicCount = 0;
	}

	static const BYTE 	ms_SubCmd = 51;
};

// 调整某个对象的位置
struct MODI_GS2C_Notify_AdjustPostion : public MODI_Role_Cmd
{
	MODI_GUID 	m_obj;
	float 	m_fPosX;
	float 	m_fPosZ;
	float 	m_fDir;
	DWORD 	m_nServerLogicCount;

	MODI_GS2C_Notify_AdjustPostion()
	{
        byParam = ms_SubCmd;
		m_fPosX = 0.0f;
		m_fPosZ = 0.0f;
		m_fDir = 0.0f;
		m_nServerLogicCount = 0;
	}

	static const BYTE 	ms_SubCmd = 52;
};

// 播放表情
struct MODI_C2GS_Request_PlayExpression : public MODI_Role_Cmd
{
	BYTE 		m_nIndex; 	// 	表情索引1-8

	MODI_C2GS_Request_PlayExpression()
	{
		m_nIndex = 0;
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 70;
};


// 表情使用结果
struct MODI_GS2C_Notify_PlayExpressionResult : public MODI_Role_Cmd
{
	BYTE  	m_byResult;
	MODI_GS2C_Notify_PlayExpressionResult()
	{
		m_byResult = kPExpression_Successful;
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 71;
};

// 服务器通知某个对象 开始/停止 播放表情
struct MODI_GS2C_Notify_PlayExpression : public MODI_Role_Cmd
{
	enum
	{
		kPlay, 	// 	开始播放
		eStop, 	// 	停止播放
	};

	MODI_GUID 	m_guid; 	// 	角色
	WORD 		m_ExpressionPackageID; 	// 表情包
	BYTE 		m_nIndex; // 表情索引
	BYTE 		m_byEnable; 

	MODI_GS2C_Notify_PlayExpression()
	{
		m_ExpressionPackageID = INVAILD_CONFIGID;
		m_nIndex = 0;
		m_byEnable = kPlay;
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 72;
};

// 通知改变称号
struct MODI_GS2C_Notify_ChangeChenghao : public MODI_Role_Cmd
{
	MODI_GUID m_PlayerID;
	WORD  	m_wdChenghao;
	MODI_GS2C_Notify_ChangeChenghao()
	{
		m_wdChenghao = 0;
        byParam = ms_SubCmd;
	}
	
	static const BYTE 	ms_SubCmd = 73;
};

/// 全服广播某人称号改变
struct MODI_GS2C_Notify_Zone_ChangeChenghao : public MODI_Role_Cmd
{
	char 	m_szName[ROLE_NAME_MAX_LEN + 1]; 
	WORD  	m_wdChenghao;
	MODI_GS2C_Notify_Zone_ChangeChenghao()
	{
		m_wdChenghao = 0;
		memset(m_szName, 0, sizeof(m_szName));
        byParam = ms_SubCmd;
	}
	
	static const BYTE 	ms_SubCmd = 74;
};

struct MODI_C2GS_Request_ReadHelp : public MODI_Role_Cmd
{
	MODI_C2GS_Request_ReadHelp()
	{
        byParam = ms_SubCmd;
	}

	static const BYTE 	ms_SubCmd = 100;
};

/// 客户端请求移动命令
struct MODI_C2GS_Request_Move_Cmd: public MODI_Role_Cmd
{
	MODI_C2GS_Request_Move_Cmd()
	{
		byParam = ms_SubCmd;
	}

	MODI_GUID m_stGuid;
	
	/// 移动初始状态
	MODI_MoveState m_stSrcState;
	
	static const BYTE ms_SubCmd = 101;
};


/// 服务器校正客户端的移动状态
struct MODI_GS2C_Adjuest_MoveState_Cmd: public MODI_Role_Cmd
{
	MODI_GS2C_Adjuest_MoveState_Cmd()
	{
		byParam = ms_SubCmd;
	}

	MODI_GUID m_stGuid;
	
	MODI_MoveState m_stMoveState;
	static const BYTE ms_SubCmd = 102;
};


/// 增加一个场景用户
struct MODI_GS2C_Add_SceneUser_Cmd: public MODI_Role_Cmd
{
	MODI_GS2C_Add_SceneUser_Cmd()
	{
		byParam = ms_SubCmd;
	}
	
	MODI_SceneUserInfo m_stAddUser;
	static const BYTE ms_SubCmd = 103;
};

/// 减少一个场景用户
struct MODI_GS2C_Del_SceneUser_Cmd: public MODI_Role_Cmd
{
	MODI_GS2C_Del_SceneUser_Cmd()
	{
		byParam = ms_SubCmd;
	}
	
	MODI_GUID m_stGuid;
	static const BYTE ms_SubCmd = 104;
};


/// 通知更新或者增加用户
struct MODI_GS2C_Update_SceneUser_Cmd: public MODI_Role_Cmd
{
	MODI_GS2C_Update_SceneUser_Cmd()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}
	
	WORD m_wdSize;
	MODI_SceneUserInfo m_pData[0];
	
	static const BYTE ms_SubCmd = 105;
};



#pragma pack(pop)

#endif
