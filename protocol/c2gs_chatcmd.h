/** 
 * @file c2gs_chatcmd.h
 * @brief 聊天相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_CHATCMDE_DEFINE_H_
#define C2GS_CHATCMDE_DEFINE_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/gamedefine.h"
#include "nullCmd.h"

/*
 * 聊天相关结构定义
 */

#pragma pack(push,1)

struct MODI_Chat_Cmd : public stNullCmd
{
	MODI_Chat_Cmd()
	{
        byCmd = MAINCMD_CHAT;
	}
};

/// 客户端聊天请求  
struct MODI_C2GS_Request_PrivateChat_ByName : public MODI_Chat_Cmd
{
    MODI_C2GS_Request_PrivateChat_ByName()
    {
        byParam = ms_SubCmd;
		memset( m_cstrName , 0 , sizeof(m_cstrName) );
		memset( m_cstrContents , 0 , sizeof(m_cstrContents) );
    }
    
    char        m_cstrName[ROLE_NAME_MAX_LEN + 1];  //    目标名字
    char        m_cstrContents[PRIVATE_CHATMSG_MAX_LEN + 1]; // 内容
	DWORD 	 	m_color; // 颜色

	static const unsigned int 	ms_SubCmd = 2;
};

/// 客户端请求范围聊天 (当前，世界频道等)
struct MODI_C2GS_Request_RangeChat : public MODI_Chat_Cmd
{
    MODI_C2GS_Request_RangeChat()
    {
        byParam = ms_SubCmd;
		m_byChannel = enChatChannel_Current;
		m_qdItemId = 0;
		memset( m_cstrContents , 0 , sizeof(m_cstrContents) );
    }

	///  道具id
	QWORD m_qdItemId;
    BYTE m_byChannel; // 频道
    char        m_cstrContents[CHATMSG_MAX_LEN + 1]; // 内容
	DWORD 	 	m_color; // 颜色

	static const unsigned int 	ms_SubCmd = 3;
};

/// 服务器通知有人请求范围聊天
struct MODI_GS2C_Notify_RangeChat_ByName : public MODI_Chat_Cmd
{
    MODI_GS2C_Notify_RangeChat_ByName()
    {
        byParam = ms_SubCmd;
		memset( m_cstrName , 0 , sizeof(m_cstrName) );
		m_byChannel = enChatChannel_Current;
		memset( m_cstrContents , 0 , sizeof(m_cstrContents) );
    }
    
    char        m_cstrName[ROLE_NAME_MAX_LEN + 1]; //    发起者ID
    BYTE 		m_byChannel; // 频道
    char        m_cstrContents[CHATMSG_MAX_LEN + 1]; // 内容
	DWORD 	 	m_color; // 颜色

	static const unsigned int 	ms_SubCmd = 5;
};


// 客户端发送游戏GM命令
struct MODI_C2GS_Request_GMCmd : public MODI_Chat_Cmd
{
	MODI_C2GS_Request_GMCmd()
	{
		byParam = ms_SubCmd;
		memset( m_szCmd , 0 , sizeof(m_szCmd) );
	}

	char 		m_szCmd[1024];

	static const unsigned int 	ms_SubCmd = 6;
};

/// 客户端使用GM命令的结果返回(直接输出字符串内容就可以）
struct MODI_GS2C_Notify_GMCmdResult : public MODI_Chat_Cmd
{
	MODI_GS2C_Notify_GMCmdResult()
	{
		byParam = ms_SubCmd;
		memset( m_szResult , 0 , sizeof(m_szResult) );
	}

	char 		m_szResult[1024];
	static const unsigned int 	ms_SubCmd = 7;
};

/// 客户端私聊回复
struct MODI_GS2C_Notify_PrivateChatRes : public MODI_Chat_Cmd
{
	
	enum enResult
	{
		enOffline, // 对方不在线
		enInBlack, // 在对方黑名单中
	};

	enResult 	m_res; // 结果
	char 		m_szName[ROLE_NAME_MAX_LEN + 1]; // 私聊目标的名字

	MODI_GS2C_Notify_PrivateChatRes()
	{
		m_res = enOffline;
		memset( m_szName , 0 , sizeof(m_szName) );
        byParam = ms_SubCmd;
	}

	static const unsigned int 	ms_SubCmd = 8;
};

// 有人找你私聊
struct MODI_GS2C_Notify_SBPrivateChatWithYou : public MODI_Chat_Cmd
{
	MODI_GS2C_Notify_SBPrivateChatWithYou()
    {
        byParam = ms_SubCmd;
		memset( m_szName , 0 , sizeof(m_szName) );
		memset( m_cstrContents , 0 , sizeof(m_cstrContents) );
    }
    
	char 		m_szName[ROLE_NAME_MAX_LEN + 1]; // 谁
    char        m_cstrContents[PRIVATE_CHATMSG_MAX_LEN + 1]; // 内容

	static const unsigned int 	ms_SubCmd = 9;
};

//// 发送系统公告到游戏世界
struct MODI_C2GS_Request_SystemBroast : public MODI_Chat_Cmd
{
	WORD 			m_nSize;
	char 			m_pContents[0];

	MODI_C2GS_Request_SystemBroast() 
    {
		m_nSize = 0;
        byParam = ms_SubCmd;
    }
    
	static const unsigned int 	ms_SubCmd = 10;
};

/// 整个游戏世界的系统公告
struct MODI_GS2C_Notify_SystemBroast : public MODI_Chat_Cmd
{
	WORD 			m_nSize;
	char 			m_pContents[0];

	MODI_GS2C_Notify_SystemBroast()
    {
		m_nSize = 0;
        byParam = ms_SubCmd;
    }
    
	static const unsigned int 	ms_SubCmd = 11;
};

// 世界聊天
struct MODI_GS2C_Notify_WorldChat : public MODI_Chat_Cmd
{
	char 		m_szServerName[MAX_SERVERNAME_LEN + 1]; // 哪个服务器的
	char        m_szName[ROLE_NAME_MAX_LEN + 1]; //    那个玩家
    char        m_szContents[CHATMSG_MAX_LEN + 1]; // 内容
	DWORD 	 	m_color; // 颜色

	MODI_GS2C_Notify_WorldChat()
	{
		memset( m_szServerName , 0 , sizeof(m_szServerName) );
		memset( m_szName, 0 , sizeof(m_szName) );
		memset( m_szContents, 0 , sizeof(m_szContents) );
		m_color = 0;
        byParam = ms_SubCmd;
	}

	static const unsigned int 	ms_SubCmd = 12;
};

// 粉丝世界聊天

struct MODI_GS2C_Notify_FensiChat : public MODI_Chat_Cmd
{
	char 		m_szServerName[MAX_SERVERNAME_LEN + 1]; // 哪个服务器的
	char        m_szName[ROLE_NAME_MAX_LEN + 1]; //    那个玩家
    char        m_szContents[CHATMSG_MAX_LEN + 1]; // 内容
	DWORD 	 	m_color; // 颜色

	MODI_GS2C_Notify_FensiChat()
	{
		memset( m_szServerName , 0 , sizeof(m_szServerName) );
		memset( m_szName, 0 , sizeof(m_szName) );
		memset( m_szContents, 0 , sizeof(m_szContents) );
		m_color = 0;
        byParam = ms_SubCmd;
	}

	static const unsigned int 	ms_SubCmd = 13;
};

// 有人进入神秘商城
struct MODI_GS2C_Notify_SbEnterRareShop : public MODI_Chat_Cmd
{
	char 		m_szName[ROLE_NAME_MAX_LEN + 1];
	DWORD 	 	m_color; // 颜色

	MODI_GS2C_Notify_SbEnterRareShop()
	{
		memset( m_szName, 0 , sizeof(m_szName) );
		m_color = 0;
        byParam = ms_SubCmd;
	}

	static const unsigned int 	ms_SubCmd = 14;
};

/// 动态系统公告栏
struct MODI_GS2C_SystemChat_Cmd: public MODI_Chat_Cmd
{
	MODI_GS2C_SystemChat_Cmd()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
		m_eType = enSystemNotice_Default;
	}
	enSystemNoticeType	m_eType;
	static const unsigned int ms_SubCmd = 15;
	
	/// 大小
	WORD m_wdSize;
	/// 内容
	char m_stContent[0];
};


/// 固定系统公告栏
struct MODI_GS2C_SystemChatID_Cmd: public MODI_Chat_Cmd
{
	MODI_GS2C_SystemChatID_Cmd()
	{
		byParam = ms_SubCmd;
		m_dwChatID = 0;
	}
	
	static const unsigned int ms_SubCmd = 16;
	
	///固定聊天内容id
	DWORD m_dwChatID;
};


struct MODI_YYChatState
{
	MODI_YYChatState()
	{
		m_byState = 0;
	}
	MODI_GUID m_stGuid;
	BYTE m_byState; /// 0不说话了，1开始说话
};


/// 通知YY聊天状态，好让用户知道谁在说话
struct MODI_C2GS_YYChatState_Cmd: public MODI_Chat_Cmd
{
	MODI_C2GS_YYChatState_Cmd()
	{
		byParam = ms_SubCmd;
	}

	MODI_YYChatState m_stState;
	
	static const BYTE ms_SubCmd = 17;
};


/// 服务器通知客户端谁在讲话
struct MODI_GS2C_Notify_YYChatState_Cmd: public MODI_Chat_Cmd
{
	MODI_GS2C_Notify_YYChatState_Cmd()
	{
		byParam = ms_SubCmd;
		m_wdSize = 0;
	}
	
	WORD m_wdSize;
	MODI_YYChatState m_pDate[0];
	static const BYTE ms_SubCmd = 18;
};

#pragma pack(pop)
  
#endif
