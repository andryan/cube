/** 
 * @file gamedefine2.h
 * @brief 游戏结构，常量定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef GAME_DEFINE_2_H____
#define GAME_DEFINE_2_H____

#include "gamedefine.h"

/// 一次最多发送8个附件
#define MAX_MAIL_ATTACH_NUM 6

// 对象更新掩码
struct MODI_ObjUpdateMask 
{
	QWORD	m_nFlags;

	MODI_ObjUpdateMask():m_nFlags(0)
	{

	}

	bool isSetBit( BYTE bit ) const
	{
		if ( m_nFlags & (1<<bit) )
			return true;
		return false;
	}

	void SetBit( BYTE bit )
	{
		m_nFlags |= (1<<(BYTE)bit);
	}

	void ResetBit( BYTE bit )
	{
		m_nFlags &= (~(1<<(BYTE)bit));
	}

	void CleanUp( )
	{
		m_nFlags = 0;
	}

	bool IsDirty() const { return m_nFlags != 0; }
};


// 角色信息更新掩码定义
class 	MODI_RoleUPMaskDefine
{
	private:
		MODI_RoleUPMaskDefine() {}
		~MODI_RoleUPMaskDefine() {}

	public:

		enum
		{
			kGameState = 1,  // 游戏状态
			kLevel , 	// 角色等级
			kAvatar , // 角色avatar形象
			kExp , // 角色经验
			kAge , // 年龄
			kCity , // 城市ID
			kClan ,  // 预留
			kGroup , //预留
			kQQ , // qq
			kRenqi,
			kBlood , // 血型
			kBirthDay , // 生日
			kVoteCount , // 获得的票数
			kPersonalSign , // 个性签名
			kWin , // 总的胜利次数
			kLoss , // 总的失败次数
			kTie , // 总的平局
			kPefect , // 总共的 PEFECT COUNT
			kCool ,   //总共的 COOL COUNT
			kGood ,   //总共的 GOOD COUNT
			kBad ,    // 总共的 BAD COUNT
			kMiss ,   // 总共的 MISS COUNT
			kCombo ,  //总共的 连击数
			kExact ,  //总的平均 精确度
			kMoney , // 游戏币数量
			kRMBMoney , // 人民币数量
			kChangCount , // 唱的次数
			kHengCount , // 哼的次数
			kKeyBoardCount , // 键盘次数
			kToupiaoCount , // 投票次数
			kRoomID , // 房间号改变，客户端不用
			kConsumeRMB , // 消费RMB数量，客户端不用
			kHighestScore,
			kHighestScoreMusicid,
			kHighestPrecision,
			kHighestPrecisionMusicid,
			kRoleUPMask_Max,
		};
};

// 物品对象更新
class 	MODI_ItemUPMaskDefine
{
	private:
		MODI_ItemUPMaskDefine() {}
		~MODI_ItemUPMaskDefine() {}

	public:

		const static BYTE	kItemCount = 1;  // 物品个数个更新
		const static BYTE	kItemUPMask_Max = 1;
};

// 对象数据更新回调
// 对象，类型，哪个属性，新的值
typedef void ( *ObjUpdateCallback)( void * , OBJTYPE , WORD , void * );

class 	MODI_UpdateDataWrite
{
	public:

		MODI_UpdateDataWrite( char * szBuf , size_t nSize ):m_pBuf(szBuf),m_nSize(nSize),m_nCurSize(0)
		{

		}

		bool 	Write( const void * szData , size_t nSize )
		{
			if( m_nCurSize + nSize > m_nSize )
				return false;
			memcpy( m_pBuf + m_nCurSize , szData , nSize );
			m_nCurSize += nSize;
			return true;
		}

		size_t 	GetWriteSize() const { return m_nCurSize; }

	private:

		char * 	m_pBuf;
		size_t 	m_nSize;
		size_t 	m_nCurSize;

};

class 	MODI_UpdateDataRead
{
	public:

		MODI_UpdateDataRead( const char * szBuf , size_t nSize ):m_pBuf(szBuf),m_nSize(nSize),m_nCurSize(0)
		{

		}

		bool 	Read(  void * szData , size_t nSize )
		{
			if( m_nSize < nSize )
				return false;
			
			memcpy( szData , m_pBuf + m_nCurSize , nSize );
			m_nSize -= nSize;
			m_nCurSize += nSize;
			return true;
		}

	private:

		const char * m_pBuf;
		size_t 	m_nSize;
		size_t 	m_nCurSize;
};

//class 	MODI_ObjUpdateCore
//{
//	public:
//
//		void 	RegisterCallback( ObjUpdateCallback pFun )
//		{
//			ms_pFun = pFun;
//		}
//
//		void 	OnObjUpdate( void * pObj ,
//				OBJTYPE type , 
//				void * pUpdateObj  , 
//				const MODI_ObjUpdateMask & mask  , 
//				const void * pUpChunck ,
//				size_t nSize )
//		{
//			if( !ms_pFun )
//				return ;
//
//			if( type == enObjType_Player )
//			{
//				OnRoleUpdate( pObj , mask , pUpdateObj , pUpChunck , nSize );
//			}
//			else if( type == enObjType_Item )
//			{
//				OnItemUpdate( pObj , mask , pUpdateObj , pUpChunck , nSize );
//			}
//		}
//
//		void 	OnRoleUpdate( void * pObj ,
//				const MODI_ObjUpdateMask & mask  ,
//				void * pUpdateObj ,  
//				const void * pUpChunck ,
//				size_t nSize )
//		{
//			MODI_RoleInfo * info = (MODI_RoleInfo *)(pUpdateObj);
//			MODI_UpdateDataRead r((const char *)pUpChunck , nSize);
//
//			// 对象，类型，哪个属性，新的值
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kGameState ) )
//			{
//				r.Read( &info->m_baseInfo.m_ucGameState , sizeof(info->m_baseInfo.m_ucGameState ) );
//				ms_pFun( pObj , enObjType_Player , MODI_RoleUPMaskDefine::kGameState , &(info->m_baseInfo.m_ucGameState) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kLevel ) )
//			{
//				r.Read( &info->m_baseInfo.m_ucLevel , sizeof(info->m_baseInfo.m_ucLevel ) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kLevel , &(info->m_baseInfo.m_ucLevel) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kAvatar ) )
//			{
//				r.Read( &info->m_normalInfo.m_avatarData , sizeof(info->m_normalInfo.m_avatarData) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kAvatar , &(info->m_normalInfo.m_avatarData) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kExp ) )
//			{
//				r.Read( &info->m_normalInfo.m_nExp , sizeof(info->m_normalInfo.m_nExp) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kExp , &(info->m_normalInfo.m_nExp) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kAge ) )
//			{
//				r.Read( &info->m_detailInfo.m_byAge , sizeof(info->m_detailInfo.m_byAge) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kExp , &(info->m_detailInfo.m_byAge) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kCity ) )
//			{
//				r.Read( &info->m_detailInfo.m_nCity , sizeof(info->m_detailInfo.m_nCity) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kCity , &(info->m_detailInfo.m_nCity) );
//			}
//		
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kClan ) )
//			{
//				r.Read( &info->m_detailInfo.m_nClan , sizeof(info->m_detailInfo.m_nClan) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kClan , &(info->m_detailInfo.m_nClan) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kGroup ) )
//			{
//				r.Read( &info->m_detailInfo.m_nGroup , sizeof(info->m_detailInfo.m_nGroup) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kGroup , &(info->m_detailInfo.m_nGroup) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kQQ ) )
//			{
//				r.Read( &info->m_detailInfo.m_nQQ , sizeof(info->m_detailInfo.m_nQQ) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kQQ , &(info->m_detailInfo.m_nQQ) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kBlood ) )
//			{
//				r.Read( &info->m_detailInfo.m_byBlood , sizeof(info->m_detailInfo.m_byBlood) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kBlood , &(info->m_detailInfo.m_byBlood) );
//			}
//			
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kBirthDay ) )
//			{
//				r.Read( &info->m_detailInfo.m_nBirthDay , sizeof(info->m_detailInfo.m_nBirthDay) );
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kBirthDay , &(info->m_detailInfo.m_nBirthDay) );
//			}
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kVoteCount ) )
//			{
//				r.Read( &info->m_detailInfo.m_nVoteCount , sizeof(info->m_detailInfo.m_nVoteCount) ); 
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kVoteCount , &(info->m_detailInfo.m_nVoteCount) ); 
//			} 
//
//			if( mask.isSetBit( MODI_RoleUPMaskDefine::kPersonalSign ) )
//			{
//				r.Read( info->m_detailInfo.m_szPersonalSign , sizeof(info->m_detailInfo.m_szPersonalSign) ); 
//				ms_pFun( pObj , enObjType_Player ,MODI_RoleUPMaskDefine::kPersonalSign , info->m_detailInfo.m_szPersonalSign );
//			}
//		}
//
//		void 	OnItemUpdate( void * pObj,
//				const MODI_ObjUpdateMask & mask  ,
//				void * pUpdateObj ,  
//				const void * pUpChunck ,
//				size_t nSize )
//		{
//			MODI_ItemInfo * info = (MODI_ItemInfo *)(pUpdateObj);
//			MODI_UpdateDataRead r((const char *)pUpChunck , nSize);
//
//			if( mask.isSetBit( MODI_ItemUPMaskDefine::kItemCount ) )
//			{
//				r.Read( &info->m_nCount , sizeof(info->m_nCount) );
//				ms_pFun( pObj , enObjType_Item ,MODI_ItemUPMaskDefine::kItemCount , &(info->m_nCount) );
//			}
//		}
//
//		ObjUpdateCallback 	ms_pFun;
//};

enum enMailType
{
	kTextMail = 1,  		// 纯文本邮件
	kAttachmentItemMail, // 附件为物品的邮件
	kAttachmentMoneyMail, // 附件为金钱的邮件
	kServerMail, 		// 服务器邮件(客户端不会用到)
};

#define 	MAIL_ATTACHMENT_MAX_MONEY 	100000

// 邮件参数
typedef struct tagMailArg
{
	union
	{
		int 	nIntValue;
		DWORD 	nDwordValue;
		float 	fFloatValue;
	};

	tagMailArg()
	{ 
		nIntValue = 0;
	}

	DWORD 	GetDWORD() const { return nDwordValue; }
	int 	GetInt() const { return nIntValue; }
	float 	GetFloat() const { return fFloatValue; }
	
	void 	SetDWORD( DWORD n ) { nDwordValue = n; }
	void  	SetInt( int n ) { nIntValue = n; }
	void 	SetFloat( float n ) { fFloatValue = n; }

}MODI_MailArg;

enum enMailStateMask
{
	kAlreadyReadMail = 1, // 已读邮件
	kAlreadyGetAttachment = 2, // 已经获得过附件的邮件
};

#define MAILINFO_ARG_COUNT 	4

enum enMailExtraInfoArg
{
	// money 
	kMailExtraArg_MoneyV = 0,

	// item
	kMailExtraArg_ItemConfigID = 0,
	kMailExtraArg_ItemCount = 1,
	kMailExtraArg_ItemExpretime_L = 2,
	kMailExtraArg_ItemExpretime_H = 3,


	kMailExtraArg_SendItem_LGUID = 0,
	kMailExtraArg_SendItem_HGUID = 1,
};

typedef struct tagExtraInfo
{
	MODI_MailArg 	m_Arg[MAILINFO_ARG_COUNT]; // 邮件参数

	void 	Reset()
	{
		for( int i = 0; i < MAILINFO_ARG_COUNT; i++ )
		{
			m_Arg[i].SetInt(0);
		}
	}

	// 设置附件为 游戏币的邮件中的货币数量
	void 	SetAttachment_Money( DWORD nMoney )
	{
		m_Arg[kMailExtraArg_MoneyV].SetDWORD( nMoney );
	}

	DWORD 	GetAttachment_Money() const
	{
		return m_Arg[kMailExtraArg_MoneyV].GetDWORD(); 
	}

	// 设置附件为 游戏物品的邮件中的物品GUID 
	void 	SetAttachment_ItemGUID( const MODI_GUID & id )
	{
		m_Arg[kMailExtraArg_SendItem_LGUID].SetDWORD( GUID_LOPART(id) );
		m_Arg[kMailExtraArg_SendItem_HGUID].SetDWORD( GUID_HIPART(id) );
	}

	MODI_GUID 	GetAttachment_ItemGUID() const
	{
		DWORD lid = m_Arg[kMailExtraArg_SendItem_LGUID].GetDWORD();
		DWORD hid = m_Arg[kMailExtraArg_SendItem_HGUID].GetDWORD();
		return MAKE_GUID( lid , hid );
	}

	void 	SetAttachment_ItemInfo( WORD nConfigID , WORD nCount ,  time_t time )
	{
		m_Arg[kMailExtraArg_ItemConfigID].SetDWORD( nConfigID );
		m_Arg[kMailExtraArg_ItemCount].SetDWORD( nCount );
		m_Arg[kMailExtraArg_ItemExpretime_L].SetDWORD( UINT64_LOPART(time) );
		m_Arg[kMailExtraArg_ItemExpretime_H].SetDWORD( UINT64_HIPART(time) );
	}

	WORD 	GetAttachment_ItemInfo_ConfigID() const
	{
		return (WORD)m_Arg[kMailExtraArg_ItemConfigID].GetDWORD();
	}

	WORD 	GetAttachment_ItemInfo_Count() const
	{
		return (WORD)m_Arg[kMailExtraArg_ItemCount].GetDWORD();
	}

	time_t	GetAttachment_ItemInfo_TimeExpretime() const
	{
		DWORD l =  m_Arg[kMailExtraArg_ItemExpretime_L].GetDWORD();
		DWORD h =  m_Arg[kMailExtraArg_ItemExpretime_H].GetDWORD();
		return MAKE_UINT64( l , h );
	}

}MODI_MailExtraInfo;

// 邮件信息
typedef struct tagMailInfo
{
	MODI_GUID 		m_mailGUID;
	enMailType 		m_Type; // 邮件类型
	char 		 	m_State; // 邮件状态
	char 			m_szSender[ROLE_NAME_MAX_LEN + 1]; 	// 发送者
	char 			m_szRecevier[ROLE_NAME_MAX_LEN + 1]; // 接受者
	char 			m_szCaption[MAIL_CAPTION_MAX_LEN + 1]; // 邮件标题
//	BYTE 			m_nLeftDay; // 剩余几天
	time_t 			m_SendTime; 	// 	发送的时间
	BYTE 			m_byExtraCount;
	MODI_MailExtraInfo m_ExtraInfo[MAX_MAIL_ATTACH_NUM];

	char  			m_szContents[MAIL_CONTENTS_MAX_LEN + 1]; // 内容
	tagMailInfo()
	{
		Reset();
	}

	void 	Reset()
	{
		m_Type = kTextMail;
		m_State = 0;
		memset( m_szSender , 0 , sizeof(m_szSender) );
		memset( m_szRecevier , 0 , sizeof(m_szRecevier) );
		memset( m_szCaption , 0 , sizeof(m_szCaption) );
		memset( m_szContents , 0 , sizeof(m_szContents) );
		for(BYTE i=0; i<MAX_MAIL_ATTACH_NUM; i++)
		{
			m_ExtraInfo[i].Reset();
		}
	}

	// 邮件是否已读
	bool 	IsAlreadyRead() const
	{
		if( m_State & kAlreadyReadMail )
			return true;
		return false;
	}

	// 邮件是否已经获得过附件
	bool 	IsAlreadyGetAttachment() const
	{
		if( m_State & kAlreadyGetAttachment )
			return true;
		return false;
	}

} MODI_MailInfo;

// 物品附件信息
typedef struct tagMailAttachmentItem
{
	MODI_GUID 	m_itemGUID; // 物品的GUID

}MODI_MailAttachmentItem;

typedef struct tagMailAttachmentMoney
{
	DWORD 	m_nMoeny; // 游戏币数量
}MODI_MailAttachmentMoney;

#define MAX_MAILATTACHMENT_SIZE 	20

// 发送的邮件信息
typedef struct tagSendMailInfo
{
	enMailType 		m_Type; // 邮件类型
	char 			m_szRecevier[ROLE_NAME_MAX_LEN + 1]; // 接受者
	char 			m_szCaption[MAIL_CAPTION_MAX_LEN + 1]; // 邮件标题
	char  			m_szContents[MAIL_CONTENTS_MAX_LEN + 1]; // 内容

	// 	note:
	// 目前对物品附件进行限制，用户无法发送物品附件
	// 但是接收的邮件中，可以存在物品附件。由服务器发送
	/// 一次最多可以发送10个道具
	BYTE m_byExtraCount;
	MODI_MailExtraInfo m_ExtraInfo[MAX_MAIL_ATTACH_NUM];

	void 	Reset()
	{
		m_Type = kTextMail;
		memset( m_szRecevier , 0 , sizeof(m_szRecevier) );
		memset( m_szCaption , 0 , sizeof(m_szCaption) );
		memset( m_szContents , 0 , sizeof(m_szContents) );
		m_byExtraCount = 0;
		
		for(int i=0; i<MAX_MAIL_ATTACH_NUM; i++)
		{
			m_ExtraInfo[i].Reset();
		}
	}

	
} MODI_SendMailInfo;

/* // 临时，考虑使用动态密匙代替 */
/* const char  g_szPackageKey[] = {3,12,9,21,10,16,127,21,'m','o','d','i','\0'}; */

/* inline void xor_encrypt( char * szEncrypt , size_t nLen , const char * szKey , unsigned int nBeginPlace ) */
/* { */
/* 	if( !szEncrypt || !szKey || !nLen ) */
/* 		return ; */
	
/* 	unsigned int KeyLen = (unsigned int)strlen(szKey);  */
/* 	for (size_t i = 0; i < nLen; i++)  */
/* 	{  */
/* 		size_t nIdx = i + nBeginPlace; */
/* 		nIdx = nIdx % KeyLen; */
/* 		char c = *szEncrypt; */
/* 		c ^= szKey[nIdx]; */
/* 		*szEncrypt = c; */
/* 		szEncrypt += 1; */
/* 	} */
/* } */

/* inline void xor_decrypt( char * szDecrypt , size_t nLen , const char * szKey , unsigned int nBeginPlace ) */
/* { */
/* 	return xor_encrypt( szDecrypt , nLen , szKey , nBeginPlace ); */
/* } */

// 一次交易最多的商品数量上限
#define 	ONCE_TRANSACTION_MAX_GOODSCOUNT 	20

// 商店类型
enum enShopType
{
	kNullShop = 0,
	kNormalShop = 1, // 普通商店
	kRareShop = 2, // 稀有商店，神秘商店
};

// 游戏货币类型
enum enMoneyType
{
	kGameMoney = 1, // 游戏币
	kRMBMoney = 2, // rmb币换取的货币
};

// 商店中的物品的时间期限类型
enum enShopGoodTimeType
{
	kShopGoodT_Begin,
	kShopGoodT_7Day,
	kShopGoodT_30Day,
	kShopGoodT_Forver,
	kShopGoodT_End,
};

// 商城中的商品
struct MODI_ShopGood
{
	WORD 		m_pConfigID; 					// 	购买的商品ID
	enShopGoodTimeType 	m_TimeStrategy; 		// 	时间类型
};

// 交易类型
enum enShopTransactionType
{
	kTransaction_Buy, // 购买
	kTransaction_ToGive, // 给予／赠送
	kTransaction_AskFor,  // 索取
	kTransaction_BuyAndUse, // 购买并且使用
	kTransaction_Web, /// 网页
};

// 交易结果
enum enShopTransactionResult
{
	kTransaction_Successful, // 成功,根据交易类型。分为购买成功，赠送成功，已经向XX索取
	kTransaction_NotEnoughMoney, // 游戏货币不足
	kTransaction_NotEnoughRMBMoney, // rmb不够
	kTransaction_InvalidArgs, // 无效的参数
	kTransaction_ReceverNotExist, //  接收者不存在
	kTransaction_AskForNotExist, //  索取的对象不存在
	kTransaction_InvalidSex, // 性别不匹配，无法购买
	kTransaction_BuyAndUseFaild_BagFull, // 购买并且使用失败，背包满
	kTransaction_UnknowError, // 未知错误，服务器商店系统异常。稍候在试
};

// 出入商城的结果
enum enInOutShopResult
{
	kIO_Shop_Successful, // 进出成功
	kIO_Shop_Out_Processing, // 正在处理购买订单，无法离开商城,请稍候再试
	kIO_Shop_UnknowError, // 进出失败
};

// 排行榜类型
enum enRankType
{
	kRank_Begin,
	kRank_Level,
	kRank_Renqi,
	kRank_Consume,
	kRank_HighestScore,
	kRank_HighestPrecision,
	kRank_KeyScore,
	kRank_KeyPrecision,
	kRank_End,
};

// 排行榜一次请求最大个数
#define RANK_REQ_MAX_SIZE 	10

// 用于初始化排行榜用的版本号
#define RANK_VER_FOR_INITIAL 0

// 等级排行榜最大上限
#define RANK_LEVEL_MAX_TOP 	100

// 我的排名最大上限 , 超过这个上限显示 排名'xxxxx'外 
#define SELF_RANK_LEVEL_MAX_TOP 10000

// 人气排行榜最大上限
#define RANK_RENQI_MAX_TOP 	100
#define SELFRANK_RENQI_MAX_TOP 	10000

// 消费排行榜最大上限
#define RANK_CONSUMERMB_MAX_TOP 	100
#define SELFRANK_CONSUMERMB_MAX_TOP 	10000

// 歌曲最高分数排行榜上限
#define RANK_HIGHESTSCORE_MAX_TOP 	100
#define SELFRANK_HIGHESTSCORE_MAX_TOP 	10000

// 歌曲最高精度排行榜上限
#define RANK_HIGHESTPRECISION_MAX_TOP 	100
#define SELFRANK_HIGHESTPRECISION_MAX_TOP 	10000

// 我的排名，如果为该值，代表排名XXXX外。
#define SELF_RANK_OUT 			0


//  请求排行榜某个排名的详细信息结果
enum enRankSpecificResult
{
	kRankSpecificOk = 0,
	kRankSpecificFaild,
};

//  请求排行排名
enum enReqRankResult
{
	kReqRankResOk = 0,
	kReqRankResUselocalData, // 不用更新，使用本地数据刷新
	kReqRankResUnknowErr, // 服务器异常，稍后试
};

// 等级排行榜信息
struct MODI_Rank_Level
{
	char 	role_name[ROLE_NAME_MAX_LEN + 1]; // 昵称 , 昵称为空代表该排名空缺
	BYTE 	level; // 等级
	WORD 	title; // 称号

	MODI_Rank_Level()
	{
		memset( role_name , 0 , sizeof(role_name) );
		level = 0;
		title = 0; // 称号
	}
};

// 人气排行榜信息
struct MODI_Rank_Renqi
{
	char 	role_name[ROLE_NAME_MAX_LEN + 1]; // 昵称, 昵称为空代表该排名空缺
	DWORD 	renqi; // 获得人气
	DWORD 	fensi_count; // 粉丝个数
	DWORD 	get_vote_count; // 获得票数
	WORD 	title; // 称号

	MODI_Rank_Renqi()
	{
		memset( role_name , 0 , sizeof(role_name) );
		renqi = 0; // 获得人气
		fensi_count = 0; // 粉丝个数
		get_vote_count = 0; // 获得票数
		title = 0; // 称号
	}
};

struct MODI_Rank_ConsumeRMB
{
	char 	role_name[ROLE_NAME_MAX_LEN + 1]; // 昵称 , 昵称为空代表该排名空缺
	DWORD 	consume;// 消费额
	WORD 	title; // 称号

	MODI_Rank_ConsumeRMB()
	{
		memset( role_name , 0 , sizeof(role_name) );
		consume = 0;
		title = 0;
	}
};

struct MODI_Rank_HighestScore
{
	char 	role_name[ROLE_NAME_MAX_LEN + 1]; // 昵称 , 昵称为空代表该排名空缺
	WORD 	musicid;
	DWORD 	highest_score;
	WORD 	title; // 称号

	MODI_Rank_HighestScore()
	{
		memset( role_name , 0 , sizeof(role_name) );
		musicid = 0;
		highest_score = 0;
		title = 0;
	}
};

struct MODI_Rank_HighestPrecision
{
	char 	role_name[ROLE_NAME_MAX_LEN + 1]; // 昵称 , 昵称为空代表该排名空缺
	WORD 	musicid;
	float 	highest_precision;
	WORD 	title; // 称号

	MODI_Rank_HighestPrecision()
	{
		memset( role_name , 0 , sizeof(role_name) );
		musicid = 0;
		highest_precision = 0.0f;
		title = 0;
	}
};


// 登记排行榜详细信息
struct MODI_Rank_Level_Specific
{
	char 	name[ROLE_NAME_MAX_LEN + 1]; // 名字
	BYTE 	sex; // 性别
	BYTE 	level; // 等级
	WORD 	default_avatar_idx; // 默认形象
	MODI_ClientAvatarData 	avatars; // 当前装备
};

// 人气排行榜详细信息
typedef MODI_Rank_Level_Specific 	MODI_Rank_Renqi_Specific;
// 消费额排行榜详细信息
typedef MODI_Rank_Level_Specific 	MODI_Rank_Consume_Specific;

typedef MODI_Rank_Level_Specific 	MODI_Rank_HighestScore_Specific;

typedef MODI_Rank_Level_Specific 	MODI_Rank_HighestPrecision_Specific;

#endif

