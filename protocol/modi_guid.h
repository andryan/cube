/** 
 * @file modi_guid.h
 * @brief 游戏中网络对象GUID 
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef _MODI_GUID_H___
#define _MODI_GUID_H___


#include "BaseOS.h"


#pragma pack(push,1)
class MODI_GUID
{
	QWORD m_id; 

	public:


	MODI_GUID():m_id(0)
	{

	}

	bool IsInvalid() const
	{
		return m_id == 0;
	}

	MODI_GUID( const MODI_GUID & that )
	{
		m_id = that.m_id;
	}

	MODI_GUID( const QWORD & id )
	{
		m_id = id;
	}

	MODI_GUID( const double & d )
	{
		memcpy( &m_id , &d , sizeof(m_id) );
	}

	MODI_GUID( const char * s )
	{
		m_id = STRTOULL( s , 0 , 10 );
	}
	
	bool operator == (const MODI_GUID & that ) const
	{
		return m_id == that.m_id;
	}

	bool operator == (const QWORD & id ) const
	{
		return m_id == id;
	}

	bool operator == (const double & id ) const
	{
		int iRet = memcmp( &m_id , &id , sizeof(m_id) );
		return iRet == 0;
	}


	bool operator != ( const MODI_GUID & that ) const
	{
		return (operator == ( that )) ? false : true;
	}

	bool operator != ( const QWORD & id ) const
	{
		return (operator == ( id )) ? false : true;
	}

	bool operator != ( const double & id ) const
	{
		return (operator == ( id )) ? false : true;
	}

	MODI_GUID & operator = ( const MODI_GUID & that )
	{
		if( this == &that )
			return *this;
		m_id = that.m_id;
		return *this;
	}

	MODI_GUID & operator = ( const QWORD & ul )
	{
		memcpy( &m_id , &ul , sizeof(m_id) );
		return *this;
	}

	MODI_GUID & operator = ( const double & d )
	{
		memcpy( &m_id , &d , sizeof(m_id) );
		return *this;
	}

	MODI_GUID & operator = (const char * s)
	{
		m_id = STRTOULL( s , 0 , 10 );
		return *this;
	}
	
	inline operator QWORD() const
	{
		return m_id;
	}

	inline operator double() const
	{
		double d = 0.0f;
		memcpy( &d , &m_id , sizeof(m_id) );
		return d;
	}

	bool operator < ( const MODI_GUID & r ) const
	{
		if( m_id < r.m_id )
			return true;
		return false;
	}

	bool operator > ( const MODI_GUID & r ) const 
	{
		if( m_id > r.m_id )
			return true;
		return false;
	}

	QWORD ToUint64() const
	{
		return m_id;
	}

	double ToFloat64() const
	{
		double d = *this;
		return d;
	}

	const char * ToString() const
	{
		static char szTemp[256];
		memset( szTemp , 0 , sizeof(szTemp) );
		SNPRINTF( szTemp , sizeof(szTemp) , I64FMTD  , ToUint64() );
		
		return szTemp;
	}

	bool IsEqual( const MODI_GUID & that ) const
	{
		return operator == ( that );
	}

	bool IsUnEqual( const MODI_GUID & that ) const
	{
		return operator != ( that );
	}

	static bool IsEqual( const MODI_GUID & l , const MODI_GUID & r )
	{
		return l.IsEqual( r );
	}

	static bool IsUnEqual( const MODI_GUID & l , const MODI_GUID & r )
	{
		return l.IsUnEqual( r );
	}

};
#pragma pack(pop)


//  无效的GUID
static const MODI_GUID INVAILD_GUID;

#endif
