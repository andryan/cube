/** 
 * @file c2gs_relation.h
 * @brief 联系人相关数据包定义
 * @author Tang Teng
 * @version v0.7
 * @date 2010-08-05
 */
#ifndef C2GS_RELATION_DEFINED_H_
#define C2GS_RELATION_DEFINED_H_

#include "protocol/c2gs_cmddef.h"
#include "protocol/relation_def.h"
#include "nullCmd.h"

#pragma pack(push,1)
struct MODI_Relation_Cmd : public stNullCmd
{
	MODI_Relation_Cmd()
	{
		byCmd = MAINCMD_RELATION;
	}
};

// 客户端请求添加联系人
// 前提条件：
// 1。目标对象为陌生人时（即不在自己的联系人名单中)
// 参数：
// 1. 目标对象的名字
// 2. 目标对象的关系类型
struct MODI_C2GS_Request_AddRelation : public MODI_Relation_Cmd
{
	enum
	{
		enBegin = 0,
		enFriend,
		enBlack,
		enIdol,
		enEnd,
	};

	char 	m_szTargetName[ROLE_NAME_MAX_LEN + 1]; // 目标名字
	BYTE    m_type; // 关系类型

	MODI_C2GS_Request_AddRelation()
	{
        byParam = ms_SubCmd;
		memset( m_szTargetName , 0 , sizeof(m_szTargetName) );
	}

	static const unsigned int 	ms_SubCmd = 1;
};

// 服务器对添加联系人请求的回复
struct MODI_GS2C_Notify_AddRelationRes : public MODI_Relation_Cmd
{

	enRelationRetType  	m_byResult;
	MODI_RelationInfo 	m_info;

	MODI_GS2C_Notify_AddRelationRes()
	{
        byParam = ms_SubCmd;
		m_byResult = enRelationRetT_Ok;
	}

	static const unsigned int 	ms_SubCmd = 2;
};

// 客户端请求改变联系人的关系类型
// 前提条件：
// 1.目标对象在自己的联系人名单中
// 参数:
// 1.目标GUID
// 2.操作类型
struct MODI_C2GS_Request_ModifyRelation : public MODI_Relation_Cmd
{
	MODI_GUID 	m_guid; // 目标GUID
	enRelOptType 	m_optType; // 操作类型

	MODI_C2GS_Request_ModifyRelation()
	{
        byParam = ms_SubCmd;
		m_optType = enRelOpt_Begin;
	}
	
	static const unsigned int 	ms_SubCmd = 3;
};

// 服务器对改变联系人关系类型的回复
struct MODI_GS2C_Notify_ModifyRelationRet : public MODI_Relation_Cmd
{
	enRelationRetType  	m_byResult; // 操作结果
	MODI_GUID 	m_guid; // 目标ID
	enRelationType m_newType; // 新的关系类型

	MODI_GS2C_Notify_ModifyRelationRet()
	{
        byParam = ms_SubCmd;
		m_byResult = enRelationRetT_Ok;
		m_newType = enRelationT_None;
	}

	static const unsigned int 	ms_SubCmd = 4;
};

// 求婚，求爱
struct MODI_C2GS_Request_CanyouMarryMe : public MODI_Relation_Cmd
{
	MODI_GUID 	m_guid; // 目标ID
	bool 		m_bLover; // 是否恋人，不是恋人就是夫妻

	MODI_C2GS_Request_CanyouMarryMe()
	{
        byParam = ms_SubCmd;
		m_bLover = false;
	}

	static const unsigned int 	ms_SubCmd = 5;
};

//// 服务器通知 某2个人 的情侣／夫妻 改变（包括创建，接触，改变）
struct MODI_GS2C_Notify_SBHoneyStateChanged : public MODI_Relation_Cmd
{
	MODI_HoneyData::enHoneyType 	m_oldtype; // 之前的关系
	MODI_HoneyData::enHoneyType 	m_newtype; // 现在的状态关系
	char 	m_szBoyName[ROLE_NAME_MAX_LEN + 1];
	char 	m_szGrilName[ROLE_NAME_MAX_LEN + 1];

	MODI_GS2C_Notify_SBHoneyStateChanged()
	{
        byParam = ms_SubCmd;
		m_oldtype = MODI_HoneyData::enHoneyT_None;
		m_newtype = MODI_HoneyData::enHoneyT_None;
		memset( m_szBoyName , 0 , sizeof(m_szBoyName) );
		memset( m_szGrilName , 0 , sizeof(m_szGrilName) );
	}

	static const unsigned int 	ms_SubCmd = 6;
};

// 服务器通知客户端， 有人和你 建立／更新 关系
struct MODI_GS2C_Notify_RelationCU : public MODI_Relation_Cmd
{
	char 	m_szName[ROLE_NAME_MAX_LEN + 1]; // 哪个人
	enRelOptType	m_byType; // 对你做了哪种类型的操作

	MODI_GS2C_Notify_RelationCU()
	{
        byParam = ms_SubCmd;
		memset( m_szName , 0 , sizeof(m_szName) );
	}	


	static const unsigned int 	ms_SubCmd = 7;
};

// 服务器下发的联系人列表
struct MODI_GS2C_Notify_RelationList : public MODI_Relation_Cmd
{
	WORD 		m_nSize;
	MODI_RelationInfo 	m_pData[0];

	MODI_GS2C_Notify_RelationList()
	{
		byParam = ms_SubCmd;
		m_nSize = 0;
	}	

	static const unsigned int 	ms_SubCmd = 8;
};

/// 服务器通知客户端，某个联系人的状态改变
struct MODI_GS2C_Notify_RelationStateChanged : public MODI_Relation_Cmd
{
	enum
	{
		enOnlineState, // 在线情况
		enGameState , // 游戏情况
		enChangeChannel, /// 切换频道
	};

	MODI_GUID 	m_guid;
	BYTE 		m_byWhatChanged; // 何种状态改变
	MODI_RelationStateChanged 	m_newState; // 新状态

	MODI_GS2C_Notify_RelationStateChanged()
	{
        byParam = ms_SubCmd;
		m_byWhatChanged = enGameState;
	}	

	static const unsigned int 	ms_SubCmd = 9;
};

struct MODI_GS2C_Notify_SBAskYouToMarryHim : public MODI_Relation_Cmd
{
	MODI_GUID 	m_guid;
	char m_szName[ROLE_NAME_MAX_LEN + 1]; // 求婚／求爱的人

	MODI_GS2C_Notify_SBAskYouToMarryHim()
	{
        byParam = ms_SubCmd;
		memset( m_szName , 0 , sizeof(m_szName) );
	}

	static const unsigned int 	ms_SubCmd = 10;
};

struct MODI_C2GS_Request_AnserMarry : public MODI_Relation_Cmd
{
	MODI_GUID m_guid;
	bool m_bOk;

	MODI_C2GS_Request_AnserMarry()
	{
        byParam = ms_SubCmd;
		m_bOk = false;
	}

	static const unsigned int 	ms_SubCmd = 11;
};

struct MODI_GS2C_Notify_AnserMarry : public MODI_Relation_Cmd
{
	char 	m_szName[ROLE_NAME_MAX_LEN + 1];
	enRelationRetType 	m_ret;

	MODI_GS2C_Notify_AnserMarry()
	{
        byParam = ms_SubCmd;
	}

	static const unsigned int 	ms_SubCmd = 12;
};


// 人气粉丝的更新
struct MODI_GS2C_Notify_UpdateFensiRenqi : public MODI_Relation_Cmd
{
	DWORD 	m_nFensiBoy;
	DWORD 	m_nFensiGril;
	DWORD 	m_nRenqi;

	MODI_GS2C_Notify_UpdateFensiRenqi()
	{
        byParam = ms_SubCmd;
		m_nFensiBoy = 0;
		m_nFensiGril = 0;
		m_nRenqi = 0;
	}

	static const unsigned int 	ms_SubCmd = 13;
};

#pragma pack(pop)






#endif
