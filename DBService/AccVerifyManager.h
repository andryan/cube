/**
 * @file   AccVerifyManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 16:52:55 2011
 * 
 * @brief  登录管理
 * 
 * 
 */


#ifndef _MD_ACCVERIFYMANAGER_H
#define _MD_ACCVERIFYMANAGER_H

#include "Global.h"
#include "Timer.h"
#include "Thread.h"
#include "s2info_cmd.h"

/**
 * @brief 登录数据
 * 
 */
class MODI_AccVerifyData
{
 public:
	MODI_AccVerifyData()
	{
		m_pSource = NULL;
		memset(m_szAccountName, 0, sizeof(m_szAccountName));
		memset(m_szAccountPwd, 0, sizeof(m_szAccountPwd));
		memset(m_cstrCardNum, 0, sizeof(m_cstrCardNum));
		m_enResult = enVerifyNull;
		m_stAccountID = INVAILD_ACCOUNT_ID;
	}

	bool IsChenMi(MODI_TTime & cur_time) const;

	/// in
	//DWORD m_dwVerifySerial;
	MODI_SessionID m_SessionID;
	MODI_DBServerClient * m_pSource;
	char 	m_szAccountName[MAX_ACCOUNT_LEN + 1]; 
	char 	m_szAccountPwd[ACCOUNT_PWD_LEN + 1];
	char 	m_cstrCardNum[MAX_CARDNUM_LENGTH];
	/// out
	enVerifyResult m_enResult;
	defAccountID m_stAccountID;
};


const BYTE SGP_CLIENT_IN_CMD = 2;
const BYTE SGP_CLIENT_IN_PARA = 2;
struct MODI_SGPClientInPara: public Cmd::stNullCmd
{
	MODI_SGPClientInPara()
	{
		byCmd = SGP_CLIENT_IN_CMD;
		byParam = SGP_CLIENT_IN_PARA;
	}
	MODI_AccVerifyData in_para;
};


/**
 * @brief 登录数据管理
 * 
 */
class MODI_AccVerifyManager
{
 public:
	typedef std::map<MODI_SessionID, MODI_AccVerifyData> defAccVerifyDataMap;
	typedef defAccVerifyDataMap::iterator defAccVerifyDataMapIter;
	typedef defAccVerifyDataMap::value_type defAccVerifyDataMapValue;

	MODI_AccVerifyManager()
	{
		
	}

	~MODI_AccVerifyManager()
	{
	}
	
	static MODI_AccVerifyManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_AccVerifyManager;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}

	/** 
	 * @brief 增加一个登录信息
	 * 
	 * @param pay_serial 支付序号
	 * @param pay_data 支付数据
	 * 
	 * @return 成功ture,失败false
	 */
	bool AddAccVerifyData(const MODI_AccVerifyData & acc_data);

	/** 
	 * @brief 删除一个登录数据
	 * 
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool RemoveAccVerifyData(const MODI_SessionID & session_id);

	bool IsAccVerifyData(const MODI_SessionID & session_id, MODI_AccVerifyData & ret_data);
	
 private:

	static MODI_AccVerifyManager * m_pInstance;
	defAccVerifyDataMap m_AccVerifyDataMap;
	MODI_RWLock m_stLock;
};

#endif
