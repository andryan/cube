#include "DBSqlQueue.h"

MODI_DBSqlQueue::MODI_DBSqlQueue()
{

}

MODI_DBSqlQueue::~MODI_DBSqlQueue()
{
	Clear();
}


void MODI_DBSqlQueue::Push( MODI_DBSql * p )
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );
	m_listSqls.push_back(p);
}

MODI_DBSql * MODI_DBSqlQueue::Pop()
{
	MODI_DBSql * p = 0;
	MODI_RecurisveMutexScope lock( &m_mutexSqls );
	if( m_listSqls.empty() == false )
	{
		p = m_listSqls.front();
		m_listSqls.pop_front();
	}
	return p;
}


void MODI_DBSqlQueue::Clear()
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );
	LIST_DBSQL_ITER itor = m_listSqls.begin();
	while( itor != m_listSqls.end() )
	{
		delete *itor;
		itor++;
	}
	m_listSqls.clear();
}

size_t MODI_DBSqlQueue::Size()
{
	MODI_RecurisveMutexScope lock( &m_mutexSqls );
	return m_listSqls.size();
}
