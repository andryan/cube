/**
 * @file   PayManager.cpp
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Wed Oct 13 14:08:47 2010
 * @version $Id:$
 * @brief  支付管理
 * 
 * 
 */

#include "AccVerifyManager.h"

MODI_AccVerifyManager * MODI_AccVerifyManager::m_pInstance = NULL;

/** 
 * @brief 增加一个登录数据
 * 
 * 
 * @return 成功ture,失败false
 */
bool MODI_AccVerifyManager::AddAccVerifyData(const MODI_AccVerifyData & acc_data)
{
	std::pair<defAccVerifyDataMapIter, bool> ret_code;
	m_stLock.wrlock();
	ret_code = m_AccVerifyDataMap.insert(defAccVerifyDataMapValue(acc_data.m_SessionID, acc_data));
	m_stLock.unlock();
	return ret_code.second;
}


/** 
 * @brief 删除一个支付数据
 * 
 * @param pay_serial 支付数据
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccVerifyManager::RemoveAccVerifyData(const MODI_SessionID & session_id)
{
	m_stLock.wrlock();
	defAccVerifyDataMapIter iter = m_AccVerifyDataMap.find(session_id);
	if(iter == m_AccVerifyDataMap.end())
	{
		m_stLock.unlock();
		return false;
	}
	m_AccVerifyDataMap.erase(iter);
	m_stLock.unlock();
	return true;
}


/** 
 * @brief 是不是登录数据
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccVerifyManager::IsAccVerifyData(const MODI_SessionID & session_id, MODI_AccVerifyData & ret_data)
{
	m_stLock.rdlock();
	defAccVerifyDataMapIter iter = m_AccVerifyDataMap.find(session_id);
	if(iter == m_AccVerifyDataMap.end())
	{
		m_stLock.unlock();
		return false;
	}
	ret_data = iter->second;
	m_stLock.unlock();
	return true;
}


bool MODI_AccVerifyData::IsChenMi(MODI_TTime & cur_time) const
{
	std::string card_num = m_cstrCardNum;
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("[chenmi] chenmi <card_num=%s,size=%d>", card_num.c_str(), card_num.size());
#endif
	if(card_num.size() == 0)
	{
		return true;
	}
	else if(card_num.size() == 15)
	{
		return false;
	}
	else if(card_num.size() == 18)
	{
		std::string str_year = card_num.substr(6, 4);
		std::string str_mon = card_num.substr(10, 2);
		std::string str_day = card_num.substr(12, 2);
		
		WORD year = atoi(str_year.c_str());
		WORD mon = atoi(str_mon.c_str());
		WORD day = atoi(str_day.c_str());

		WORD cur_year = cur_time.GetYear();
		WORD cur_mon = cur_time.GetMon();
		WORD cur_day = cur_time.GetMDay();
		
// #ifdef _HRX_DEBUG
// 		Global::logger->debug("[chenmi_state] <str_year=%s,str_mon=%s,str_day=%s>", str_year.c_str(), str_mon.c_str(), str_day.c_str());
// 		Global::logger->debug("[chenmi_state] <year=%u,mon=%u,day=%u>", year, mon, day);
// 		Global::logger->debug("[chenmi_state] <cur_year=%u,cur_mon=%u,cur_day=%u>", cur_year, cur_mon, cur_day);
// #endif
		
		QWORD sub_day = (cur_year * 365 + cur_mon * 30 + cur_day) - (year * 365 + mon * 30 + day);
		if(sub_day > 18 * 365)
		{
			return false;
		}
			
#ifdef _HRX_DEBUG
		Global::logger->debug("[user_login] a user login  is chenmi<card_num=%s,year=%u,mon=%u,day=%u>", card_num.c_str(), year, mon, day);
#endif
		return true;
	}
	return true;
}
