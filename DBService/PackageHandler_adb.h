/** 
 * @file PackageHandler_adb.h
 * @brief ACCOUNT DB服务器上的数据包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef ACCOUNTDB_PACKAGE_HANDLER_H_
#define ACCOUNTDB_PACKAGE_HANDLER_H_


#include "DBServerClient.h"
#include "PackageHandler_db.h"

/**
 * @brief ACCOUNT DB 服务器上的数据包的处理函数
 *
 */
class MODI_PackageHandler_adb : public MODI_PackageHandler_db  
{

public:

    MODI_PackageHandler_adb();
    virtual ~MODI_PackageHandler_adb();


public:

	/// 请求登录的处理
	static int RequestLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 登出的处理
	static int RequestLoginOut_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size );

	/// 验证PP请求
	static int RequestAuth_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size );

	/// 重新生成PP请求
	static int RequestRemakePP_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size );
private:

    virtual bool FillPackageHandlerTable();
};

#endif
