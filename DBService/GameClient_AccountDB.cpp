#include "GameClient_AccountDB.h"
#include "AssertEx.h"


MODI_GameClient_ADB::MODI_GameClient_ADB():m_nCreateTime(0),
	m_nAccountID(INVAILD_ACCOUNT_ID),
	m_pOnServerPtr(0)
{
	memset( m_szPassword , 0 , sizeof(m_szPassword) );
	m_bFangChengMi = true;
}

MODI_GameClient_ADB::~MODI_GameClient_ADB()
{

}

void 	MODI_GameClient_ADB::SetAccount( const char * szAccount )
{
	m_strAccount = szAccount;
}

void 	MODI_GameClient_ADB::SetPassword( const char * szPassword , size_t nLen )
{
	if( nLen > ACCOUNT_PWD_LEN )
	{
		MODI_ASSERT(0);
		return ;
	}

	memcpy( m_szPassword , szPassword , nLen );
}

const char * MODI_GameClient_ADB::GetPassowrd() const
{
	return m_szPassword;
}

void 	MODI_GameClient_ADB::SetAccountID( defAccountID id )
{
	m_nAccountID = id;
}



MODI_DBServerClientPtr 	MODI_GameClient_ADB::GetOnServerPtr()
{
	return m_pOnServerPtr;
}

void 			MODI_GameClient_ADB::SetOnServerPtr( MODI_DBServerClientPtr p )
{
	m_pOnServerPtr = p;
}
