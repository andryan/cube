/** 
 * @file DBTask.h
 * @brief 
 * @author Tang Teng
 * @version v0.1
 * @date 2010-08-04
 */

#ifndef _MDDBTASK_H
#define _MDDBTASK_H

#include "ServiceTask.h"
#include "DBServerClient.h"

class MODI_DBTask: public MODI_ServiceTask
{
 public:
	MODI_DBTask(const int sock, const struct sockaddr_in * addr);

	~MODI_DBTask();

	/// 命令处理
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	/// 回收连接
	int RecycleConn();
	
 private:

	MODI_DBServerClientPtr  	m_pDBSvrClient;
};

#endif
