/**
 * @file   AccInfoManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:38:42 2011
 * 
 * @brief  账号信息管理
 * 
 * 
 */


#ifndef _MD_ACCINFOMANAGER_H
#define _MD_ACCINFOMANAGER_H

#include "Global.h"
#include "Thread.h"
#include "protocol/gamedefine.h"
#include "DBServerClient.h"

/**
 * @brief 账号信息数据
 * 
 */
class MODI_AccInfo
{
 public:
	MODI_AccInfo()
	{
		memset(m_cstrCardNum, 0, sizeof(m_cstrCardNum));
		m_dwAccID = INVAILD_ACCOUNT_ID;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
		memset(m_cstrPassword, 0, sizeof(m_cstrPassword));
	}

	bool IsChenMi(MODI_TTime & cur_time) const;
	
	/// 账号id
	defAccountID m_dwAccID;
	/// 账号名
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];

   	char m_cstrPassword[ACCOUNT_PWD_LEN + 1];
		
	/// 账号身份证
	char m_cstrCardNum[MAX_CARDNUM_LENGTH];
	
}__attribute__((packed));


/**
 * @brief 账号信息管理
 * 
 */
class MODI_AccInfoManager: public MODI_Thread
{
 public:
	typedef __gnu_cxx::hash_map<std::string, MODI_AccInfo * , str_hash> defAccInfoMap;
	typedef defAccInfoMap::iterator defAccInfoMapIter;
	typedef defAccInfoMap::value_type defAccInfoMapValue;

	MODI_AccInfoManager(): m_stUpDateTime(1000)
	{
		
	}

	~MODI_AccInfoManager()
	{
		
	}
	
	static MODI_AccInfoManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_AccInfoManager;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}

	bool Init();

	/** 
 	 * @brief 从队列里面获取账号信息到list里面
 	 * 
 	 */
	void GetAccFromQueue();

	/** 
 	 * @brief 增加一个账号信息到队列
 	 * 
 	 * @param p_info 要增加的账号
 	 */
 	void AddAccToQueue(MODI_AccInfo * p_info);

	/** 
	 * @brief 获取一个账号信息
	 * 
	 * @param acc_name 账号名
	 * 
	 * @return 成功true,失败false
	 */
	MODI_AccInfo * GetAccInfo(const char * acc_name);

	/// 更新
	void Run();

	/** 
	 * @brief 校验返回
	 * 
	 * @param out_para 返回结果
	 * 
	 */
	void RetVerifyResult(MODI_AccInfo * p_info, MODI_SessionID & session_id, MODI_DBServerClientPtr p_client);
	
 private:
	bool AddAccInfo(MODI_AccInfo * acc_info);
		
	static MODI_AccInfoManager * m_pInstance;
	
	/// 账号信息缓冲队列
	std::queue<MODI_AccInfo * , std::deque<MODI_AccInfo * > > m_AccInfoQueue;

	/// 账号信息map
	defAccInfoMap m_AccInfoMap;

	MODI_TTime m_stTTime;
	MODI_Timer m_stUpDateTime;
	MODI_RTime m_stCurrentTime;
	MODI_RWLock m_stMapLock;
	MODI_RWLock m_stQueueLock;
};

#endif
