/**
 * @file   SGPClientSched.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 15:29:18 2011
 * 
 * @brief  新加坡登录效验
 * 
 * 
 */


#ifndef _MD_SGPCLIENTSCHED_H_
#define _MD_SGPCLIENTSCHED_H_

#include "SGPClient.h"
#include "PClientTask.h"
#include "PClientTaskSched.h"
#include "CommandQueue.h"

/**
 * @brief 校验入口
 * 
 */
class MODI_SGPClientSched: public MODI_CmdParse
{
 public:
	MODI_SGPClientSched()
	{
		Resize(1024);
	}
	
	static MODI_SGPClientSched & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_SGPClientSched;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}


	bool Init();

	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);
	
	/** 
	 * @brief 账号校验
	 * 
	 * @param in_para 需要校验的参数
	 * 
	 */
	bool ReqVerifyAcc(const MODI_AccVerifyData & in_para);
	

	/** 
	 * @brief 更新
	 * 
	 */
	void UpDate();
	

	/** 
	 * @brief 增加一个校验
	 * @param p_client 校验
	 *
	 */
	void AddSGPClient(MODI_PClientTask * p_client);

	/** 
	 * @brief 移出一个校验
	 * 
	 * @param p_client 
	 */
	void RemoveSGPClient(MODI_PClientTask * p_client);
	
 private:
	/// 队列
	std::list< MODI_PClientTask * > m_ClientList;
	MODI_RWLock m_stRWLock;
	
	static MODI_SGPClientSched * m_pInstance;
	MODI_PClientTaskSched m_stClientSched;
};

#endif
