#include "DBLogicThread.h"
#include "Global.h"
#include "SGPClientSched.h"

MODI_LogicThread::MODI_LogicThread():MODI_Thread("MODI_LogicThread")
{

}

MODI_LogicThread::~MODI_LogicThread()
{
	m_sqlQueue.Clear();
}

void MODI_LogicThread::Run()
{
    while (true)
    {
		// 线程终止，并且队列空，则退出
		if( IsTTerminate() && !this->Pop() )
			break;

		MODI_SGPClientSched::GetInstance().UpDate();
		// 该线程的工作:
		// 不断对SQL结果进行游戏逻辑的处理

		MODI_DBSql * p = this->Pop();
		if( !p )
		{
			::usleep(10000);
			continue ;
		}
		
		// 回调处理函数

		int iRet = p->m_pCallbackFun( p );
		if( iRet )
		{
			// do somthing	
		}

		// free memory
		delete p;
    }

	Global::logger->info("[%s] DB LogicThread will Exit." , SVR_TEST ); 
}

void MODI_LogicThread::Push( MODI_DBSql * p )
{
	m_sqlQueue.Push(p);
}

MODI_DBSql * 	MODI_LogicThread::Pop()
{
	return m_sqlQueue.Pop();
}
