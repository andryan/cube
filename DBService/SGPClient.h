/**
 * @file   SGPClient.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 15:39:36 2011
 * 
 * @brief  新加坡登录
 * 
 * 
 */


#ifndef _MD_SGPCLIENT_H
#define _MD_SGPCLIENT_H

#include "PClientTask.h"
#include "CommandQueue.h"
#include "AccVerifyManager.h"


/// 校验返回命令
const BYTE ACCOUNT_VERIFY_CMD = 1;
const BYTE ACCOUNT_VERIFY_PARA = 1;
struct MODI_RetVerifyResult: public Cmd::stNullCmd
{
	MODI_RetVerifyResult()
	{
		byCmd = ACCOUNT_VERIFY_CMD;
		byParam = ACCOUNT_VERIFY_PARA;
		m_wdRetCode = 1000;
		m_dwAccountID = 0;
		m_wdGameCC = 0;
	}
	WORD m_wdRetCode;
	WORD m_wdGameCC;
	DWORD m_dwAccountID;
}__attribute__((packed));


/**
 * @brief 登录校验连接
 * 
 */
class MODI_SGPClient: public MODI_PClientTask, public MODI_CmdParse
{
 public:
	
	MODI_SGPClient(MODI_AccVerifyData & para):
		MODI_PClientTask(Global::g_strPlatformIP.c_str(), Global::g_wdPlatformPort)
	{
		ResetRecvBuf();
		m_stInPara = para;
	}

	bool CmdParse(const Cmd::stNullCmd*, int)
	{
		return true;
	}

	bool RecvDataNoPoll();
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	bool SendCmd(const char * cmd, const unsigned int cmd_size)
	{
		if(m_pSocket)
		{
			m_pSocket->SendCmdNoPacket(cmd, cmd_size,false);
			return true;
		}
		return false;
	}

	bool Init();

	void SendVerifyCmd();

	int RecycleConn()
	{
		if(IsTerminateFinal())
			return 1;
		return 0;
	}

   	void AddTaskToManager();
	void RemoveTaskFromManager();
	void TimeOut();
	void VerifyFailed();
	static const char * MakeMd5(const char * inpara);
	void RetVerifyResult();
 private:
	void ResetRecvBuf()
	{
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}
	
	/// 接收缓冲
	char m_stRecvBuf[1024];

	/// 接收的个数
	unsigned int m_dwRecvCount;

	/// 需要校验的参数
	MODI_AccVerifyData m_stInPara;
};

#endif
