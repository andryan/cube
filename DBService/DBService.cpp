/**
 * @file DBService.cpp
 * @date 2010-01-18 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库服务器类
 *
 */

#include "DBService.h"
#include "DBTask.h"
#include "InfoClient.h"
#include "Base/ServerConfig.h"

/**
 * @brief 服务器初始化
 *
 */
bool MODI_DBService::Init()
{
	// account db 的配置信息
	const MODI_SvrADBConfig  * pADBConfig = (const MODI_SvrADBConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ACCOUNTSERVER ));
	if( !pADBConfig )
	{
		return false;
	}

	if (!MODI_NetService::Init( pADBConfig->nPort , pADBConfig->strIP.c_str() ))
	{
		Global::logger->fatal(LOGINFO_INFO2, LOGACT_SYS_INIT);
		return false;
	}

	if(! m_stTaskSched.Init())
	{
		Global::logger->fatal("Unable initialization task sched");
		return false;
	}

	/// 不再直接问平台，而是直接问infoclient
// 	if(! MODI_AccVerifySched::GetInstance().Init())
// 	{
// 		Global::logger->fatal("Unable initialization client sched");
// 		return false;
// 	}
	
	return true;
}


/**
 * @brief 创建一个新的连接
 *
 * @param sock 连接的socket句柄
 * @param ader 连接的地址
 * @return 创建成功返回true,失败返回false
 *
 */
bool MODI_DBService::CreateTask(const int sock, const sockaddr_in * addr)
{
	if(sock == -1 || addr == NULL)
	{
		Global::logger->fatal("[%s] DBServer CreateTask Faild. sock<%d> , addr<%p>" , 
				SVR_TEST ,
				sock , 
				addr );
		return false;
	}

	MODI_DBTask * p_task = new MODI_DBTask(sock, addr);
	if(p_task)
	{
		//m_stTaskPoll->AddTask(p_task);
		m_stTaskSched.AddNormalSched(p_task);
		return true;
	}
	Global::logger->fatal("[%s] DBServer CreateTask Faild." , SVR_TEST );
	TEMP_FAILURE_RETRY(::close(sock));
	return false;
}

void MODI_DBService::Final()
{

}

