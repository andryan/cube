/** 
 * @file DBSqlThread.h
 * @brief sql 执行线程
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-27
 */

#ifndef DBSQL_THREAD_H_
#define DBSQL_THREAD_H_

#include "Type.h"
#include "Thread.h"
#include "DBSqlQueue.h"
#include "SingleObject.h"



class MODI_SqlThread: public MODI_Thread , public CSingleObject<MODI_SqlThread>
{
public:

	MODI_SqlThread(const std::string & name = std::string("MODI_SqlThread"));

	virtual ~MODI_SqlThread();

    /// 主循环
    void Run();

	void Push( MODI_DBSql * p );

private:

	MODI_DBSql * Pop();

	bool ExecuteSelect( MODI_DBSql * pDBSql );

	bool ExecuteDelete( MODI_DBSql * pDBSql );

	bool ExecuteUpdate( MODI_DBSql * pDBSql );

	bool ExecuteInsert( MODI_DBSql * pDBSql );

private:


	MODI_DBSqlQueue 	m_sqlQueue;

};

#endif
