/** 
 * @file GameClient_AccountDB.h
 * @brief 帐号数据库服务器上的游戏客户端对象 
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-03
 */

#ifndef GAMECLIENT_ACCOUNTDB_H_
#define GAMECLIENT_ACCOUNTDB_H_

#include "protocol/gamedefine.h"
#include "AccountMgr.h"
#include "DBServerClient.h"


class 	MODI_GameClient_ADB
{
	public:

		MODI_GameClient_ADB();
		~MODI_GameClient_ADB();

	public:

		void 	SetCreateTime( unsigned long nTime )
		{
			m_nCreateTime = nTime;
		}

		unsigned long GetCreateTime() const
		{
			return m_nCreateTime;
		}

		void 	SetAccount( const char * szAccount );
		const char * GetAccount() const
		{
			return m_strAccount.c_str();
		}

		void 	SetPassword( const char * szPassword , size_t nLen );
		const char * GetPassowrd() const;
		
		void 	SetAccountID( defAccountID id );
		defAccountID GetAccountID() const 
		{
			return m_nAccountID;
		}

		void 	SetLoginPassport( const MODI_LoginPassport & passport )
		{
			memcpy( &m_Passport , &passport , sizeof(MODI_LoginPassport) );
		}

		const MODI_LoginPassport & GetLoginPoassport() const
		{
			return m_Passport;
		}
		
		void 	SetLoginKey( MODI_LoginKey & key )
		{
			memcpy( &m_Key , &key , sizeof(MODI_LoginKey) );
		}

		const MODI_LoginKey & GetLoginKey() const
		{
			return m_Key;
		}

		void 			SetKeyCreateTime( unsigned long nTime )
		{
			m_nKeyCreateTime = nTime;
		}

		unsigned long 	GetKeyCreateTime() const 
		{
			return m_nKeyCreateTime;
		}

		void 			SetFangChengMi( bool b ) { m_bFangChengMi = b; }
		bool 			IsFangChengMi() const { return m_bFangChengMi; }

		MODI_DBServerClientPtr 	GetOnServerPtr();
		void 			SetOnServerPtr( MODI_DBServerClientPtr p );

	private:

		
		unsigned long 	m_nCreateTime;
		std::string 	m_strAccount; 
		char 			m_szPassword[ACCOUNT_PWD_LEN];
		defAccountID 	m_nAccountID; 
		MODI_DBServerClientPtr  m_pOnServerPtr;
		MODI_LoginPassport 	m_Passport;
		MODI_LoginKey 	m_Key;
		unsigned int 	m_nKeyCreateTime;
		bool 			m_bFangChengMi;
};


// ACCOUNT DB 上的帐号管理器
typedef MODI_AccountMgr<MODI_GameClient_ADB,QWORD> 	MODI_AccountMgr_ADB;

#endif
