/** 
 * @file DBServerClient.h
 * @brief DB服务器上的客户端
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef DBSERVER_CLIENT_H_
#define DBSERVER_CLIENT_H_

#include "Type.h"
#include "RecurisveMutex.h"
#include <set>
#include "DBServerBase.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "functionPtr.h"
#include "protocol/gamedefine.h"



// MODI_DBServerClient 对象身上的事件回调对象
typedef CFreeFunctionPtr<void *,int> 	MODI_DBSVRCLIENT_CALLBACK;

/** 
 * @brief DB上的客户端类
 */
class 	MODI_DBServerClient 
{
	public:

		explicit MODI_DBServerClient( void * p );
		~MODI_DBServerClient();


	public:

		/** 
		 * @brief 发送数据包
		 * 
		 * @param pCmd 命令
		 * @param nCmdSize 命令大小
		 * 
		 * @return 	成功返回TRUE
		 */
		bool 	SendPackage( const void * pCmd , size_t nCmdSize );

		/** 
		 * @brief 该客户端是否 GameServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsGameServer() const
		{
			return m_iSvrType == SVR_TYPE_GS;
		}

		/** 
		 * @brief 该客户端是否 LoginServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsLoginServer() const
		{
			return m_iSvrType == SVR_TYPE_LS;
		}

		/** 
		 * @brief 该客户端是否 ZoneServer
		 * 
		 * @return 如果是返回TRUE	
		 */
		bool 	IsZoneServer() const
		{
			return m_iSvrType == SVR_TYPE_ZS;
		}

		/** 
		 * @brief 重置网络接口
		 */
		void 	ResetNetIO();

		void 	SetDisconnectionCallback( MODI_DBSVRCLIENT_CALLBACK pFn );

	private:

		/// 网络接口锁
		MODI_RecurisveMutex 	m_mutexNetIO;

		/// 网络接口
		void * 	m_pNetIO;

		/// 该客户端是哪种服务器类型
		int 	m_iSvrType; 

		/// 某个服务器断开的回调对象
		MODI_DBSVRCLIENT_CALLBACK 	* m_pDisconnectionCallback; 
};

typedef MODI_DBServerClient * MODI_DBServerClientPtr;
class 	MODI_RecordContainer;

class MODI_DBSql;
/// 数据库结果处理回调
typedef CFreeFunctionPtr<MODI_DBSql *,int> 	MODI_DBRES_HANDLER_CALLBACK;
typedef CFreeFunctionPtr<MODI_DBSql *,void> MODI_DBSQL_USERDATA_CLEAN;

struct MODI_DBSql
{
	std::string  m_strSql; // sql 查询语句
	MODI_DBServerClientPtr m_pClient; // 查询发起者
	MODI_RecordContainer * 	m_pResults; // SQL查询结果集
	int 		m_iSqlType; // SQL类型，SELECT ， DEL ， UP , Insert , None
	int m_nSqlRet; // sql 执行返回值
	defAccountID 	m_nAccountID; // account id
	char 		m_szAccount[MAX_ACCOUNT_LEN + 1]; // account name
	char 		m_szPassword[ACCOUNT_PWD_LEN + 1]; // password
	MODI_SessionID m_SessionID;
	MODI_DBRES_HANDLER_CALLBACK m_pCallbackFun; // 结果处理回调
	char  * 	m_pUserData; // 用户数据
	size_t 		m_nUserDataSize; // 用户数据的大小
	bool 		m_bUserDataOwner; // 是否用户数据的所有者

	MODI_DBSql();
	~MODI_DBSql();

	void 	AllocResultRecordContainer();
	void 	FreeResultRecordContainer();
	char * 	AllocUserDataMemory( size_t nSize , bool bOwner  = true );
	void 	SetUserData( char * pData , size_t nSize , bool bOwner );
	void 	FreeUserDataMemory();

	void 	SetAccount( const char * szAccount );
	const char * 	GetAccount() const;
	void 	SetPassword(const char * szAccount);

	bool 	IsUserDataOwner() const { return m_bUserDataOwner; }
	void 	SetUserDataOwner( bool b ) { m_bUserDataOwner = b; }
};





/* 
class 	MODI_DBServerClientMgr
{
	public:
		MODI_DBServerClientMgr();
		~MODI_DBServerClientMgr();

	public:

		bool 	Add( MODI_DBServerClient * pAdd );

		bool 	Del( MODI_DBServerClient * pDel );

		bool 	IsIn( MODI_DBServerClient * p );

		size_t 	Size();

		void 	Clear();


		static 	MODI_DBServerClientMgr & 	GetInstance();

	private:

		typedef std::set<MODI_DBServerClient *> 	SET_CLIENTS;
		typedef SET_CLIENTS::iterator 				SET_CLIENTS_ITER;
		typedef SET_CLIENTS::const_iterator 		SET_CLIENTS_C_ITER;
		typedef std::pair<SET_CLIENTS_ITER,bool> 	SET_CLIENTS_INSERT_RESULT;

		MODI_RecurisveMutex 	m_mutexClients;
		SET_CLIENTS 	m_setClients;
};
*/

#endif
