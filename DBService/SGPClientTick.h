/**
 * @file   SGPClientTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 17:18:39 2011
 * 
 * @brief  新加坡登录
 * 
 * 
 */


#ifndef _MDSGPCLIENTTICK_H
#define _MDSGPCLIENTTICK_H

#include "Timer.h"
#include "Thread.h"

class MODI_SGPClientTick: public MODI_Thread 
{
public:
    MODI_SGPClientTick();
	
    virtual ~MODI_SGPClientTick();

    void Final();

    virtual void Run();

	static MODI_SGPClientTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_SGPClientTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
private:
	
	static MODI_SGPClientTick * m_pInstance;
	MODI_RTime m_stRTime;
	MODI_Timer m_stCmdFluxTime;
};

#endif
