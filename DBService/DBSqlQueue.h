/** 
 * @file DBSqlQueue.h
 * @brief DB服务器的 SQL 信息结构队列
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */
#ifndef DBSQL_QUEUE_H_
#define DBSQL_QUEUE_H_


#include <list>
#include "RecurisveMutex.h"
#include "DBServerClient.h"


class 	MODI_DBSqlQueue
{
	public:
		MODI_DBSqlQueue();
		~MODI_DBSqlQueue();

	public:

		void Push( MODI_DBSql * p );
		MODI_DBSql * Pop();
		void Clear();
		size_t Size();


	private:

		typedef std::list<MODI_DBSql *> 	LIST_DBSQL;
		typedef LIST_DBSQL::iterator 		LIST_DBSQL_ITER;
		typedef LIST_DBSQL::const_iterator  LIST_DBSQL_C_ITER;

		MODI_RecurisveMutex 	m_mutexSqls;
		LIST_DBSQL 			m_listSqls;
};





#endif
