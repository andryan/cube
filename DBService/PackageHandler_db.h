/** 
 * @file PackageHandler_db.h
 * @brief DB服务器上的数据包处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef PACKAGE_HANDLER_H_
#define PACKAGE_HANDLER_H_


#include "IPackageHandler.h"
#include "SingleObject.h"

class MODI_DBSql;

/**
 * @brief DB 服务器上的数据包的处理函数
 *
 */
class MODI_PackageHandler_db : public MODI_IPackageHandler , public CSingleObject<MODI_PackageHandler_db>
{

public:

    MODI_PackageHandler_db();
    ~MODI_PackageHandler_db();


	void PushToSqlThread( MODI_DBSql * pDBSql );

	MODI_DBSql * 	AllocDBSql();
	void 			FreeDBSql(MODI_DBSql * p);
};

#endif
