#include "PackageHandler_db.h"
#include "SqlStatement.h"
#include "DBSqlThread.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "Global.h"



MODI_PackageHandler_db::MODI_PackageHandler_db()
{

}

MODI_PackageHandler_db::~MODI_PackageHandler_db()
{

}

void MODI_PackageHandler_db::PushToSqlThread( MODI_DBSql * pDBSql )
{
	// push 
	MODI_SqlThread::GetInstanceRef().Push( pDBSql );
}


MODI_DBSql * 	MODI_PackageHandler_db::AllocDBSql()
{
	MODI_DBSql * pRet = new MODI_DBSql();
	return pRet;
}

void 			MODI_PackageHandler_db::FreeDBSql(MODI_DBSql * p)
{
	delete p;
}
