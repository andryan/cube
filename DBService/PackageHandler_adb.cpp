#include "PackageHandler_adb.h"
#include "SqlStatement.h"
#include "DBSqlThread.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "DBResultHandler.h"
#include "s2adb_cmd.h"
#include "InfoClient.h"
#include "AccVerifyManager.h"
#include "AccInfoManager.h"
#include "SGPClientSched.h"
#include "SGPClient.h"

MODI_PackageHandler_adb::MODI_PackageHandler_adb()
{

}

MODI_PackageHandler_adb::~MODI_PackageHandler_adb()
{

}

bool MODI_PackageHandler_adb::FillPackageHandlerTable()
{
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_LoginIn::ms_SubCmd  , 
			&MODI_PackageHandler_adb::RequestLogin_Handler );
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_LoginAuth::ms_SubCmd  , 
			&MODI_PackageHandler_adb::RequestAuth_Handler );
	this->AddPackageHandler( MAINCMD_S2ADB , MODI_S2ADB_Request_ReMakePP::ms_SubCmd , 
			&MODI_PackageHandler_adb::RequestRemakePP_Handler );
	return true;
}


/*-----------------------------------------------------------------------------
 *  帐号服务器对登入请求的处理
 *-----------------------------------------------------------------------------*/
int MODI_PackageHandler_adb::RequestLogin_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
			const unsigned int cmd_size )
{
	MODI_DBServerClientPtr  pClient  =  (MODI_DBServerClientPtr )(pObj);

	int iRet = enPHandler_OK;

	const MODI_S2ADB_Request_LoginIn  * pReq = (const MODI_S2ADB_Request_LoginIn *)( pt_null_cmd );

	/// 艺为登录
	if(Global::g_wdPlatformFlag == 1)
	{
		MODI_AccVerifyData para;
		para.m_pSource = pClient;
		memcpy(para.m_szAccountName, pReq->m_szAccountName, sizeof(para.m_szAccountName) - 1);
		memcpy(para.m_szAccountPwd, pReq->m_szAccountPwd, sizeof(para.m_szAccountPwd) - 1);
		para.m_SessionID = pReq->m_SessionID;
		
		//MODI_InfoClient::GetInstance().ReqVerifyAcc(para);
		//	return iRet;
		
		/// 不再直接问平台,通过infoservice到平台
		MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(para.m_szAccountName);
		if(p_info)
		{
			if(strcmp(para.m_szAccountPwd, p_info->m_cstrPassword) == 0)
			{
				/// 效验成功
				std::string card_num = p_info->m_cstrCardNum;
				if(card_num == "")
				{
#ifdef _HRX_DEBUG
					Global::logger->debug("[login_verify] card num null <user=%s>", para.m_szAccountName);
#endif				
					/// 身份证空
					MODI_InfoClient::GetInstance().ReqVerifyAcc(para);
					return enPHandler_OK;
				}
				else
				{
#ifdef _HRX_DEBUG
					Global::logger->debug("[login_verify] direct ret a verify result <user=%s,pwd=%s,accid=%u>", p_info->m_cstrAccName, p_info->m_cstrPassword,
										  p_info->m_dwAccID);
#endif				
					MODI_AccInfoManager::GetInstance().RetVerifyResult(p_info, para.m_SessionID, pClient);
					return enPHandler_OK;
				}
			}
#ifdef _HRX_DEBUG
			Global::logger->debug("[login_verify] password change <user=%s>", para.m_szAccountName);
#endif			
		}
#ifdef _HRX_DEBUG
		Global::logger->debug("[login_verify] in dbservice cache not find acc_info <name=%s,pwd=%s>)", pReq->m_szAccountName, pReq->m_szAccountPwd);
#endif
		MODI_InfoClient::GetInstance().ReqVerifyAcc(para);
	}
	/// 新加坡的登录验证方式
	else if(Global::g_wdPlatformFlag == 2)
	{
		MODI_AccVerifyData para;
		para.m_pSource = pClient;
		memcpy(para.m_szAccountName, pReq->m_szAccountName, sizeof(para.m_szAccountName) - 1);
		memcpy(para.m_szAccountPwd, pReq->m_szAccountPwd, sizeof(para.m_szAccountPwd) - 1);
		para.m_SessionID = pReq->m_SessionID;

		/// 先不缓冲了，方便新加坡统计数据
#if 0		
		/// 这里也可以直接询问后返回，不要缓冲
		MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(para.m_szAccountName);
		if(p_info)
		{
			if(strcmp(para.m_szAccountPwd, p_info->m_cstrPassword) == 0)
			{
				/// 效验成功
#ifdef _HRX_DEBUG
				Global::logger->debug("[login_verify] direct ret a verify result <user=%s,pwd=%s,accid=%u>", p_info->m_cstrAccName, p_info->m_cstrPassword,
									  p_info->m_dwAccID);
#endif				
				MODI_AccInfoManager::GetInstance().RetVerifyResult(p_info, para.m_SessionID, pClient);
				return enPHandler_OK;
			}
#ifdef _HRX_DEBUG
			Global::logger->debug("[login_verify] password change <user=%s>", para.m_szAccountName);
#endif			
		}
		
#ifdef _HRX_DEBUG
		Global::logger->debug("[login_verify] in dbservice cache not find sgp_acc_info <name=%s,pwd=%s>)", pReq->m_szAccountName, pReq->m_szAccountPwd);
#endif
		
#endif		
		MODI_SGPClientSched::GetInstance().ReqVerifyAcc(para);
	}
	/// TG
	else if(Global::g_wdPlatformFlag == 3)
	{
		std::string send_passwd = MODI_SGPClient::MakeMd5(pReq->m_szAccountPwd);
		MODI_S2ADB_Request_LoginIn * p_change_cmd = const_cast<MODI_S2ADB_Request_LoginIn *>(pReq);
		memset(p_change_cmd->m_szAccountPwd, 0, sizeof(p_change_cmd->m_szAccountPwd));
		strncpy(p_change_cmd->m_szAccountPwd, send_passwd.c_str(), sizeof(p_change_cmd->m_szAccountPwd));
		const char * szSql = MODI_SqlStatement::CreateStatement_Sel_Account( pReq->m_szAccountName , pReq->m_szAccountPwd );
		MODI_ASSERT( szSql );
		if( szSql )
		{
			MODI_DBRES_HANDLER_CALLBACK pFn( &MODI_DBResultHandler::Handler_RequestLogin );

			MODI_DBSql * pDBSql = MODI_PackageHandler_db::GetInstancePtr()->AllocDBSql();
			pDBSql->m_iSqlType = enSqlStatement_Type_Select;
			pDBSql->m_strSql = szSql;
			pDBSql->m_pClient = pClient;
			pDBSql->m_pCallbackFun = pFn;
			pDBSql->m_SessionID = pReq->m_SessionID;
			pDBSql->SetAccount( pReq->m_szAccountName );
			pDBSql->SetPassword(pReq->m_szAccountPwd);

			MODI_PackageHandler_db::GetInstancePtr()->PushToSqlThread( pDBSql );
		}
	}
	/// 本地测试用
	else
	{
		const char * szSql = MODI_SqlStatement::CreateStatement_Sel_Account( pReq->m_szAccountName , pReq->m_szAccountPwd );
		MODI_ASSERT( szSql );
		if( szSql )
		{
			MODI_DBRES_HANDLER_CALLBACK pFn( &MODI_DBResultHandler::Handler_RequestLogin );

			MODI_DBSql * pDBSql = MODI_PackageHandler_db::GetInstancePtr()->AllocDBSql();
			pDBSql->m_iSqlType = enSqlStatement_Type_Select;
			pDBSql->m_strSql = szSql;
			pDBSql->m_pClient = pClient;
			pDBSql->m_pCallbackFun = pFn;
			pDBSql->m_SessionID = pReq->m_SessionID;
			pDBSql->SetAccount( pReq->m_szAccountName );

			MODI_PackageHandler_db::GetInstancePtr()->PushToSqlThread( pDBSql );
		}
	}
	return iRet;
}

int MODI_PackageHandler_adb::RequestAuth_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_DBServerClientPtr  pClient  =  (MODI_DBServerClientPtr )(pObj);
	int iRet = enPHandler_OK;

	const MODI_S2ADB_Request_LoginAuth * pReq = (const MODI_S2ADB_Request_LoginAuth *)(pt_null_cmd);
	const size_t nPackageSize = GET_PACKAGE_SIZE( MODI_S2ADB_Request_LoginAuth , 0 , 0 );

	MODI_DBRES_HANDLER_CALLBACK pFn( &MODI_DBResultHandler::Handler_RequestAuth );
	MODI_DBSql * pDBSql = MODI_PackageHandler_db::GetInstancePtr()->AllocDBSql();
	pDBSql->AllocUserDataMemory( nPackageSize );
	MODI_S2ADB_Request_LoginAuth * pUserData = (MODI_S2ADB_Request_LoginAuth *)(pDBSql->m_pUserData);
	AutoConstruct( pUserData );
	memcpy( pDBSql->m_pUserData , pReq , nPackageSize );
	pDBSql->m_iSqlType = enSqlStatement_Type_Logic;
	pDBSql->m_pClient = pClient;
	pDBSql->m_pCallbackFun = pFn;

	MODI_PackageHandler_db::GetInstancePtr()->PushToSqlThread( pDBSql );

	return iRet;
}

int MODI_PackageHandler_adb::RequestRemakePP_Handler( void * pObj , const Cmd::stNullCmd * pt_null_cmd , 
		const unsigned int cmd_size )
{
	MODI_DBServerClientPtr  pClient  =  (MODI_DBServerClientPtr )(pObj);
	int iRet = enPHandler_OK;

	const MODI_S2ADB_Request_ReMakePP * pReq = (const MODI_S2ADB_Request_ReMakePP *)(pt_null_cmd);

	MODI_DBRES_HANDLER_CALLBACK pFn( &MODI_DBResultHandler::Handler_RequestRemakePP );
	MODI_DBSql * pDBSql = MODI_PackageHandler_db::GetInstancePtr()->AllocDBSql();
	pDBSql->m_iSqlType = enSqlStatement_Type_Logic;
	pDBSql->m_pClient = pClient;
	pDBSql->m_pCallbackFun = pFn;
	pDBSql->m_nAccountID = pReq->m_nAccountID;

	const size_t nPackageSize = GET_PACKAGE_SIZE( MODI_S2ADB_Request_ReMakePP , 0 , 0 );
	pDBSql->AllocUserDataMemory( nPackageSize );
	MODI_S2ADB_Request_ReMakePP * pUserData = (MODI_S2ADB_Request_ReMakePP *)(pDBSql->m_pUserData);
	AutoConstruct( pUserData );
	memcpy( pDBSql->m_pUserData , pReq , nPackageSize );

	MODI_PackageHandler_db::GetInstancePtr()->PushToSqlThread( pDBSql );

	return iRet;
}
