/**
 * @file   SGPClientSched.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 15:34:06 2011
 * 
 * @brief  新加坡登录效验
 * 
 * 
 */


#include "SGPClientSched.h"
#include "SGPClientTick.h"

MODI_SGPClientSched * MODI_SGPClientSched::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_SGPClientSched::Init()
{
	if(! m_stClientSched.Init())
	{
		Global::logger->fatal("Unable initialization sgp client sched");
		return false;
	}

	if(! MODI_SGPClientTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization sgp start");
		return false;
	}
	
	return true;
}

/** 
 * @brief 增加一个校验
 * @param p_client 校验
 *
 */
void MODI_SGPClientSched::AddSGPClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.push_back(p_client);
	m_stRWLock.unlock();
}

/** 
 * @brief 移出一个校验
 * @param p_client 校验
 *
 */
void MODI_SGPClientSched::RemoveSGPClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.remove(p_client);
	m_stRWLock.unlock();
}


/** 
 * @brief 账号校验
 * 
 * @param in_para 需要校验的参数
 * 
 */
bool MODI_SGPClientSched::ReqVerifyAcc(const MODI_AccVerifyData & in_para)
{
	/// 不阻塞调用线程,放到tick里面去做
	MODI_SGPClientInPara send_cmd;
	send_cmd.in_para = in_para;
	Put((Cmd::stNullCmd *) &send_cmd, sizeof(MODI_SGPClientInPara));
	return true;
}

void MODI_SGPClientSched::UpDate()
{

	if(m_ClientList.empty())
	{
		return;
	}

	m_stRWLock.rdlock();
	std::list<MODI_PClientTask * >::iterator next, iter;
	for(iter=m_ClientList.begin(), next=iter, next++; iter!=m_ClientList.end(); iter=next, next++)
	{
		MODI_SGPClient * p_client = (MODI_SGPClient *)(*iter);
		p_client->Get(1);
	}
	m_stRWLock.unlock();
}



bool MODI_SGPClientSched::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	MODI_SGPClientInPara * in_para = (MODI_SGPClientInPara *)pt_null_cmd;
	MODI_SGPClient * p_client = new MODI_SGPClient(in_para->in_para);
	if(!p_client)
	{
		Global::logger->fatal("Unable new a sgp client");
		return false;
	}
	if(p_client->Init())
	{
		m_stClientSched.AddNormalSched(p_client);
		p_client->SendVerifyCmd();
		return true;
	}
	delete p_client;
	return false;
}
