/**
 * @file   SGPClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 15:51:29 2011
 * 
 * @brief  新加坡登录
 * 
 * 
 */

#include "Global.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "SGPClient.h"
#include "SGPClientSched.h"
#include "md5.h"
#include "XMLParser.h"
#include "AccInfoManager.h"
#include "base64.h"
#include "DBResultHandler.h"
#include "SqlStatement.h"
#include "DBSqlThread.h"
#include "protocol/c2gs.h"
#include "AssertEx.h"
#include "DBResultHandler.h"
#include "s2adb_cmd.h"

/** 
 * @brief 初始化
 *
 * @return 成功true,失败false
 *
 */
bool MODI_SGPClient::Init()
{
	if(! MODI_PClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 发送校验命令
 * 
 */
void MODI_SGPClient::SendVerifyCmd()
{
	std::ostringstream send_cmd;
	/// 新加坡
	if(Global::g_wdPlatformFlag == 2)
	{
		std::string send_name = base64_encode(m_stInPara.m_szAccountName,strlen(m_stInPara.m_szAccountName));
		std::string send_passwd = MakeMd5(m_stInPara.m_szAccountPwd);
		std::string old_name = m_stInPara.m_szAccountName;
		std::string check_str = "auth123" + old_name;
		std::string auth_str = MakeMd5(check_str.c_str());
		std::string send_ip = m_stInPara.m_SessionID.GetIPString();
	
		std::ostringstream send_content;
		send_content<< "username=" <<send_name << "&password=" << send_passwd <<"&gametype=modijct&auth=" << auth_str<< "&ip=" << send_ip;
		//<<"Host: " << "http://www.showtimesea.com/receiveSTcontent.aspx" << ":" << "80" <<"\r\n"
		//	<<"Content-Type: application/x-www-form-urlencoded\r\n"
		send_cmd << "POST /receiveSTcontent.aspx HTTP/1.1\r\n"
			<<"Content-Type: application/x-www-form-urlencoded\r\n"
			<<"Host: " << "www.showtimesea.com" <<"\r\n"
			 <<"Content-Length: "<< send_content.str().size() <<"\r\n"
			 <<"Connection: Keep-Alive\r\n\r\n"
			 <<send_content.str();
	}
	/// 泰国
	else if(Global::g_wdPlatformFlag == 3)
	{
		std::string send_name = base64_encode(m_stInPara.m_szAccountName,strlen(m_stInPara.m_szAccountName));
		std::string send_ip = m_stInPara.m_SessionID.GetIPString();
		std::string send_passwd = m_stInPara.m_szAccountPwd;
		std::ostringstream send_content;
		send_content<< "user=" <<send_name << "&password=" << send_passwd <<"&ip=" << send_ip;
		send_cmd << "POST /action/Activate.aspx HTTP/1.1\r\n"
			<<"Content-Type: application/x-www-form-urlencoded\r\n"
			<<"Host: " << "www.showtimesea.com" <<"\r\n"
			 <<"Content-Length: "<< send_content.str().size() <<"\r\n"
			 <<"Connection: Keep-Alive\r\n\r\n"
			 <<send_content.str();
	}

#ifdef _HRX_DEBUG
	Global::logger->debug("-->>send a command =%s", send_cmd.str().c_str());
#endif
	
	SendCmd(send_cmd.str().c_str(), send_cmd.str().size());
}


/**
 * @brief 校验
 * 
 */
bool MODI_SGPClient::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > 1000)
		{
			Global::logger->fatal("recv command more than 1000");
			ResetRecvBuf();
			return false;
		}
		
		const std::string m_strRecvContent = m_stRecvBuf;
		
#ifdef _HRX_DEBUG		
		Global::logger->debug(m_strRecvContent.c_str());
#endif

		std::string::size_type end_pos = m_strRecvContent.find_first_of("\r\n", 0);
		if(end_pos == std::string::npos)
		{
			Global::logger->debug("not get crlf");
			return true;
		}
		std::string str_answer;
		str_answer= m_strRecvContent.substr(0, end_pos);
		std::string::size_type answer_end_pos = str_answer.find_last_of(' ', str_answer.size());
		if(answer_end_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get answer");
#endif
			VerifyFailed();
			return false;
		}

		std::string ret_result = str_answer.substr(answer_end_pos+1, (str_answer.size() - answer_end_pos + 1));
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("ret_result=%s.", ret_result.c_str());
#endif
		if(ret_result != "OK")
		{
#ifndef _HRX_DEBUG			
			Global::logger->debug("answer not ok");
#endif
			VerifyFailed();
			return false;
		}

		std::string::size_type begin_content_pos = m_strRecvContent.find("\r\n\r\n", 0);
		if(begin_content_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get begin crlfcrlf");
#endif
			VerifyFailed();
			return false;
		}

		if(m_strRecvContent.size() < (begin_content_pos + 6))
		{
			VerifyFailed();
			return false;
		}
		std::string final_str_result = m_strRecvContent.substr(begin_content_pos+4, (m_strRecvContent.size() - (begin_content_pos+4)));
#ifdef _HRX_DEBUG
		Global::logger->debug("final_str_result=%s.", final_str_result.c_str());
#endif
		MODI_RetVerifyResult send_cmd;
		send_cmd.m_wdRetCode = enVerifyFailed;
		m_stInPara.m_enResult = enVerifyFailed;

		/// 新加坡返回
		if(Global::g_wdPlatformFlag == 2)
		{
			if(final_str_result.size() < 22)
			{
				VerifyFailed();
				return false;
			}
		
#ifdef _HRX_DEBUG		
			Global::logger->debug("result=%s", final_str_result.c_str());
#endif


			MODI_RetVerifyResult send_cmd;
			
		
			MODI_SplitString deal_result;
			deal_result.Init(final_str_result.c_str(), "&");
			std::string get_result = deal_result["result"];
			std::string get_accid = deal_result["accid"];
			std::string get_gamecc = deal_result["get_gamecc"];

			if(get_result == "1")
			{
				send_cmd.m_wdRetCode = enVerifySuccess;
				DWORD acc_id = atoi(get_accid.c_str());
				//WORD gamecc = atoi(get_gamecc.c_str());
				m_stInPara.m_stAccountID = acc_id;
				m_stInPara.m_enResult = enVerifySuccess;
			}
		}
		/// 泰国
		else if(Global::g_wdPlatformFlag == 3)
		{
#ifdef _HRX_DEBUG		
			Global::logger->debug("result=%s", final_str_result.c_str());
#endif
			if(final_str_result == "SUCCESS\r\n")
			{
				send_cmd.m_wdRetCode = enVerifySuccess;
				m_stInPara.m_enResult = enVerifySuccess;			
			}
		}
		
		Put(&send_cmd, sizeof(send_cmd));
		ResetRecvBuf();
		return false;
	}
	return true;
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 
 * @param cmd_size 
 * 
 * @return 
 */
bool MODI_SGPClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	Global::logger->debug("recv a command");
	if(!pt_null_cmd)
		return false;
	
	if(pt_null_cmd->byCmd == ACCOUNT_VERIFY_CMD && pt_null_cmd->byParam == ACCOUNT_VERIFY_PARA)
	{
		/// 命令处理
		RetVerifyResult();
		TerminateFinal();
		return true;
	}
	
	return false;
}




/** 
 * @brief 校验返回
 * 
 * @param out_para 返回结果
 * 
 */
void MODI_SGPClient::RetVerifyResult()
{
	if(Global::g_wdPlatformFlag == 2)
	{
		if(m_stInPara.m_enResult == enVerifySuccess)
		{
// 			m_stTTime.GetNow();
 			bool chen_mi = false;
			
			 
			MODI_DBResultHandler ::GetAccountMgr().Del(m_stInPara.m_stAccountID, true );
			MODI_DBResultHandler::OnClientLogin(m_stInPara.m_stAccountID,
												m_stInPara.m_szAccountName,
												m_stInPara.m_szAccountPwd,
												chen_mi,
											   	m_stInPara.m_SessionID,
												m_stInPara.m_pSource);
			
#ifdef _HRX_DEBUG
			Global::logger->debug("[ret_login] ret login <name=%s,pwd=%s,accid=%u>", m_stInPara.m_szAccountName, m_stInPara.m_szAccountPwd, m_stInPara.m_stAccountID);
#endif
			
			MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(m_stInPara.m_szAccountName);
			if(p_info)
			{
				p_info->m_dwAccID = m_stInPara.m_stAccountID;
				//strncpy(p_info->m_cstrAccName, m_stInPara.m_szAccountName, sizeof(p_info->m_cstrAccName) - 1);
				strncpy(p_info->m_cstrPassword, m_stInPara.m_szAccountPwd, sizeof(p_info->m_cstrPassword) - 1);
				strncpy(p_info->m_cstrCardNum, m_stInPara.m_cstrCardNum, sizeof(p_info->m_cstrCardNum) - 1);
			}
			else
			{
				/// 在这里做个缓冲队列
				MODI_AccInfo * p_info = new MODI_AccInfo();
				p_info->m_dwAccID = m_stInPara.m_stAccountID;
				strncpy(p_info->m_cstrAccName, m_stInPara.m_szAccountName, sizeof(p_info->m_cstrAccName) - 1);
				strncpy(p_info->m_cstrPassword, m_stInPara.m_szAccountPwd, sizeof(p_info->m_cstrPassword) - 1);
				strncpy(p_info->m_cstrCardNum, m_stInPara.m_cstrCardNum, sizeof(p_info->m_cstrCardNum) - 1);
				MODI_AccInfoManager::GetInstance().AddAccToQueue(p_info);
#ifdef _HRX_DEBUG
				Global::logger->debug("[add_info] add a info <name=%s,pwd=%s,accid=%u>", p_info->m_cstrAccName, p_info->m_cstrPassword, p_info->m_dwAccID);
#endif			
			}
			
		}
		else if(m_stInPara.m_enResult == enVerifyNoActive)
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_NoActive , 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
		else if(m_stInPara.m_enResult == enVerifyTimeout)
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_ServerError, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
		else
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_InvaildAP, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
	}
	/// 泰国
	else if(Global::g_wdPlatformFlag == 3)
	{
		if(m_stInPara.m_enResult == enVerifySuccess)
		{
			/// 直接读取数据里面的数据，不能和泰国平台成了死循环了
			const char * szSql = MODI_SqlStatement::CreateStatement_Sel_Account(m_stInPara.m_szAccountName, m_stInPara.m_szAccountPwd);
			MODI_RecordContainer record_container;
			MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
			MODI_ScopeHandlerRelease lock( pDBClient );
			int get_rec = pDBClient->ExecSelect(record_container, szSql, strlen(szSql));
			if(get_rec == 1)
			{
				MODI_Record * pRecord = record_container.GetRecord( 0 );
				if(pRecord)
				{				
					const MODI_VarType & v_account =  pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_account));
					const MODI_VarType & v_password = pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_password));
					const MODI_VarType & v_accountID = pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_id));
					const char * szAccount = v_account.ToStr();
					const char * szPassword = v_password.ToStr();
					defAccountID nAccountID = v_accountID.ToUInt();
					bool chen_mi = false;
					MODI_DBResultHandler ::GetAccountMgr().Del(m_stInPara.m_stAccountID, true );
					MODI_DBResultHandler::OnClientLogin(nAccountID,
														szAccount,
														szPassword,
														chen_mi,
														m_stInPara.m_SessionID,
														m_stInPara.m_pSource);
#ifdef _HRX_DEBUG
					Global::logger->debug("[ret_login] ret login <name=%s,pwd=%s,accid=%u>",
										  szAccount, szPassword, nAccountID);
#endif
				}
				else
				{
					MODI_DBResultHandler::SendLoginResult(SvrResult_Login_InvaildAP, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
				}
			}
			else
			{
				MODI_DBResultHandler::SendLoginResult(SvrResult_Login_InvaildAP, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
			}

			
		}
		else if(m_stInPara.m_enResult == enVerifyNoActive)
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_NoActive , 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
		else if(m_stInPara.m_enResult == enVerifyTimeout)
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_ServerError, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
		else
		{
			MODI_DBResultHandler::SendLoginResult(SvrResult_Login_InvaildAP, 0 ,m_stInPara.m_SessionID, m_stInPara.m_pSource);
		}
	}
}


/** 
 * @brief 添加到管理中
 * 
 */
void MODI_SGPClient::AddTaskToManager()
{
	MODI_SGPClientSched::GetInstance().AddSGPClient(this);
}

/** 
 * @brief 从管理中删除
 * 
 */
void MODI_SGPClient::RemoveTaskFromManager()
{
	MODI_SGPClientSched::GetInstance().RemoveSGPClient(this);
}

/** 
 * @brief 超时
 * 
 */
void MODI_SGPClient::TimeOut()
{
	Global::logger->fatal("%s check timeout", m_stInPara.m_szAccountName);
	MODI_RetVerifyResult send_cmd;
	send_cmd.m_wdRetCode = enVerifyTimeout;
	send_cmd.m_dwAccountID = INVAILD_ACCOUNT_ID;
	Put(&send_cmd, sizeof(send_cmd));
	ResetRecvBuf();
	//TerminateFinal();
}


void MODI_SGPClient::VerifyFailed()
{
	MODI_RetVerifyResult send_cmd;
	send_cmd.m_wdRetCode = enVerifyFailed;
	send_cmd.m_dwAccountID = 0;
	Put(&send_cmd, sizeof(send_cmd));
	ResetRecvBuf();	
}


const char * MODI_SGPClient::MakeMd5(const char * inpara)
{
	unsigned char md5_out[17];
    memset(md5_out, 0 , sizeof(md5_out));
    char final_out[33];
    memset(final_out, 0, sizeof(final_out));

    MD5Context check_str;
    MD5Init( &check_str );
    MD5Update( &check_str , (const unsigned char*)(inpara) , strlen(inpara));
    MD5Final(md5_out , &check_str );
    Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
    transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
    return recv_check_str.c_str();
}
