#include "DBResultHandler.h"
#include "SqlStatement.h"
#include "DBServerBase.h"
#include "DBSqlThread.h"
#include "AssertEx.h"
#include "protocol/c2gs.h"
#include "GameHelpFun.h"
#include "Base/md5.h"
#include "AccVerifyManager.h"
#include "AccInfoManager.h"
#include "SGPClientSched.h"

MODI_AccountMgr_ADB 	MODI_DBResultHandler::ms_AccountsMgr;

MODI_DBResultHandler::MODI_DBResultHandler()
{

}

MODI_DBResultHandler::~MODI_DBResultHandler()
{

}


void 	MODI_DBResultHandler::SendLoginResult( WORD nResult ,
				MODI_ADB2S_Notify_LoginResult::MODI_Result * pResult ,
				const MODI_SessionID  & sessionid ,
				MODI_DBServerClientPtr pClient )
{
	// 构造登入结果数据包
	int iSendSize = sizeof(MODI_ADB2S_Notify_LoginResult);

	// 构造结构
	char szPackageBuf[Skt::MAX_USERDATASIZE];
	memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
	MODI_ADB2S_Notify_LoginResult  * p_notify = (MODI_ADB2S_Notify_LoginResult *)(szPackageBuf);
	AutoConstruct( p_notify );
	MODI_ADB2S_Notify_LoginResult::MODI_Result * pDeailResult = (MODI_ADB2S_Notify_LoginResult::MODI_Result *)(p_notify->m_pData);

	// 填充该帐号的详细结果，发给登入服务器

	p_notify->m_SessionID = sessionid;
	p_notify->m_nResult = nResult;
	size_t nResultSize = 0;
	if( pResult )
	{
		nResultSize = sizeof(MODI_ADB2S_Notify_LoginResult::MODI_Result);
		memcpy( pDeailResult , pResult , nResultSize );
	}

	p_notify->m_nSize = nResultSize;
	// update send size
	iSendSize += p_notify->m_nSize;
	pClient->SendPackage( p_notify , iSendSize );
}

void 	MODI_DBResultHandler::OnMakeKeyAndPP( MODI_GameClient_ADB * pGameClient , 
				defAccountID nAccountID ,
				const char * szAccount ,
				const char * szPwd ,
				MODI_DBServerClientPtr pClient )
{
	if( szAccount )
		pGameClient->SetAccount( szAccount );
	if( szPwd )
		pGameClient->SetPassword( szPwd , ACCOUNT_PWD_LEN );
	if( pClient )
		pGameClient->SetOnServerPtr( pClient );

	pGameClient->SetAccountID( nAccountID );
	unsigned long nTime = GetTickCount( true );
	pGameClient->SetCreateTime( nTime );

	/*-----------------------------------------------------------------------------
	 * 登入依据以最后次登入请求所生成的KEY为准 
	 *-----------------------------------------------------------------------------*/
	// KEY
	MODI_LoginKey key;
	CreateKey( key );
	pGameClient->SetLoginKey( key );

	// PP
	MODI_LoginPassport passport;
	CreatePP( pGameClient->GetAccountID() , 
			INVAILD_CHARID ,
			key ,
			pGameClient->GetAccount() ,
			pGameClient->GetPassowrd() ,
			passport );
	pGameClient->SetLoginPassport( passport );
	pGameClient->SetKeyCreateTime( nTime );
}

void 	MODI_DBResultHandler::OnClientLogin( defAccountID nAccountID ,
				const char * szAccount ,
				const char * szPwd ,
				bool 		bFangChengMi,
				const MODI_SessionID & sessionid,
				MODI_DBServerClientPtr pClient )
{
	// 重新生成一个新的
	MODI_GameClient_ADB * pGameClient = new MODI_GameClient_ADB();
	if( !GetAccountMgr().Add( nAccountID , pGameClient ) )
	{
		Global::logger->error("[%s] can't add gameclient_adb to mgr.accid<%llu>." , 
				LOGIN_OPT ,
				nAccountID );

		MODI_ASSERT(0);
		delete pGameClient;
		return ;
	}

	pGameClient->SetFangChengMi( bFangChengMi );

	// 生成key和pp
	OnMakeKeyAndPP( pGameClient , nAccountID , szAccount , szPwd ,  pClient );

	// 发送结果
	MODI_ADB2S_Notify_LoginResult::MODI_Result DeailResult;
	strncpy(DeailResult.m_cstrAccName, szAccount, sizeof(DeailResult.m_cstrAccName) - 1);
	DeailResult.m_nAccountID = nAccountID;
	DeailResult.m_Key = pGameClient->GetLoginKey();
	SendLoginResult( SvrResult_Login_OK  , &DeailResult , sessionid ,  pClient );
}


int MODI_DBResultHandler::Handler_RequestLogin( MODI_DBSql * pResult )
{
	MODI_ASSERT( pResult );
	MODI_ASSERT( pResult->m_iSqlType == enSqlStatement_Type_Select );
	MODI_ASSERT( pResult->m_pClient );

	if( !pResult->m_pClient )
		return -1;

	Global::logger->info("[%s] client<session=%s> , Request Login Result<successful=%u>." , 
			LOGIN_OPT ,
			pResult->m_SessionID.ToString(),
			pResult->m_nSqlRet == 1 ? 1 : 0 );

	if( pResult->m_nSqlRet == 1 ) 
	{
		MODI_Record * pRecord = pResult->m_pResults->GetRecord( 0 );
		MODI_ASSERT( pRecord );

		const MODI_VarType & v_account =  pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_account));
		const MODI_VarType & v_password = pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_password));
		const MODI_VarType & v_accountID = pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_id));
		const MODI_VarType & v_chengmi = pRecord->GetValue(MODI_SqlStatement::GetFieldInAccountTable(enAccountTableField_activeGame));
		const char * szAccount = v_account.ToStr();
		const char * szPassword = v_password.ToStr();
		defAccountID nAccountID = v_accountID.ToUInt();
		BYTE cheng_mi = v_chengmi.ToUShort();


		// 删除之前的
		GetAccountMgr().Del( nAccountID , true );

		bool bFangChengmi = true;//(nAccountID % 2 == 0);
		if(cheng_mi > 0)
			bFangChengmi = false;

	Global::logger->info("[%s] client<session=%s,accid=%u,acc=%s,chenmi=%d> login ok!" ,
				LOGIN_OPT ,
				pResult->m_SessionID.ToString(),
				nAccountID,
						 szAccount,bFangChengmi);
 
		// 帐号登入
		OnClientLogin( nAccountID , szAccount , szPassword , bFangChengmi,
				pResult->m_SessionID , pResult->m_pClient ); 
	}
	else 
	{		
		/// 是泰国的不要立刻返回
		if(Global::g_wdPlatformFlag == 3)
		{
			MODI_AccVerifyData para;
			para.m_pSource = pResult->m_pClient;
			memcpy(para.m_szAccountName, pResult->m_szAccount, sizeof(para.m_szAccountName) - 1);
			memcpy(para.m_szAccountPwd, pResult->m_szPassword, sizeof(para.m_szAccountPwd) - 1);
			para.m_SessionID = pResult->m_SessionID;
			MODI_SGPClientSched::GetInstance().ReqVerifyAcc(para);
		}
		
		// 无效帐号密码
		SendLoginResult( SvrResult_Login_InvaildAP ,  
				0 ,
				pResult->m_SessionID,
				pResult->m_pClient );
		
	}


	return 0;
}

int MODI_DBResultHandler::Handler_RequestAuth( MODI_DBSql * pResult )
{
	MODI_ADB2S_Notify_LoginAuthResult msg;
	msg.m_byResult = 0;

	const MODI_S2ADB_Request_LoginAuth * pReq  = (const MODI_S2ADB_Request_LoginAuth *)(pResult->m_pUserData);
	MODI_GameClient_ADB * pClient = 0;

	pClient = (MODI_GameClient_ADB *)GetAccountMgr().Find( pReq->m_Passport.m_nAccountID );
	if( pReq->m_Passport.m_byRechangeChannel )
	{
		Global::logger->info("[%s] accid<%llu> Request Auth P.P , to rechange channel... " ,  LOGIN_OPT ,
				pReq->m_Passport.m_nAccountID );
	}
	else 
	{
		Global::logger->info("[%s] accid<%llu> Request Auth P.P , to login server... " ,  LOGIN_OPT ,
				pReq->m_Passport.m_nAccountID );
	}

	msg.m_nServerID = pReq->m_nServerID;
	msg.m_nCharID = pReq->m_Passport.m_nCharID;
	msg.m_nAccountID = pReq->m_Passport.m_nAccountID;
	msg.m_nSessionID = pReq->m_nSessionID;
	msg.m_bFangChengMi = true;

	if( !pClient )
	{
		pResult->m_pClient->SendPackage( &msg , sizeof(msg) );
		return 0;
	}

	// pp 时间是否失效

	bool bSuccessful = false ;
	const MODI_LoginPassport &  pp = pClient->GetLoginPoassport();

//		unsigned long nCreateTime = pClient->GetCreateTime();
//		unsigned long nNow = GetTickCount( true );
//		const unsigned long nPPMaxVaildTime = 45 * 1000;
//		if( nNow - nCreateTime < nPPMaxVaildTime )
//		{
//			bSuccessful = true;
//		}
//		else 
//		{
//			Global::logger->info("[%s] client acc<%s> auth successful. auth loginpassport faild.<PP time out> " , 
//					LOGIN_AUTH ,
//					pClient->GetAccount() ); 
//		}
//		
	if( 
		( memcmp( pp.m_szContent , pReq->m_Passport.m_szContent , sizeof(pp.m_szContent) ) == 0 ) &&
		   pp.m_nAccountID == pReq->m_Passport.m_nAccountID )
	{
		bSuccessful = true;
	}		
	else 
	{
		pResult->m_pClient->SendPackage( &msg , sizeof(msg) );
		Global::logger->warn("[%s] client acc<%s> auth faild. loginpassport is invaild. " , LOGIN_AUTH ,
			pClient->GetAccount() ); 
		return 0;
	}

	// 通行证是否正确
	if( bSuccessful )
	{
		Global::logger->info("[%s] client acc<%s> auth successful. auth loginpassport successful " , 
				LOGIN_AUTH ,
				pClient->GetAccount() ); 

		MODI_LoginPassport passport = pClient->GetLoginPoassport();
		passport.m_nCharID = pReq->m_Passport.m_nCharID;
		passport.m_nAccountID = pReq->m_Passport.m_nAccountID;
		pClient->SetLoginPassport( passport );
		msg.m_byResult = 1;
		msg.m_bFangChengMi = pClient->IsFangChengMi();
		pResult->m_pClient->SendPackage( &msg , sizeof(msg) );
	}

	return 0;
}


int MODI_DBResultHandler::Handler_RequestRemakePP( MODI_DBSql * pResult )
{
	const MODI_S2ADB_Request_ReMakePP * pReq = (const MODI_S2ADB_Request_ReMakePP *)(pResult->m_pUserData);
	MODI_ASSERT( pReq->m_nAccountID == pResult->m_nAccountID );

	MODI_GameClient_ADB * pGameClient = (MODI_GameClient_ADB *)GetAccountMgr().Find( pReq->m_nAccountID );
	MODI_ADB2S_Notify_ReMakePPResult msg;
	msg.m_bySuccessful = 0;
	msg.m_nAccountID = pReq->m_nAccountID;
	msg.m_nServerID = pReq->m_nServerID;
	msg.m_nSessionID = pReq->m_nSessionID;

	if( pGameClient )
	{
		MODI_ASSERT( pGameClient->GetAccountID() == pReq->m_nAccountID );

		// 生成key和pp
		OnMakeKeyAndPP( pGameClient , pGameClient->GetAccountID() , 0, 0 , 0 );

		msg.m_bySuccessful = 1;
		msg.m_Key = pGameClient->GetLoginKey();

		Global::logger->info("[%s] accid<%llu> Remake P.P " ,  LOGIN_OPT ,
		   pGameClient->GetAccountID() );
	}
	else 
	{
		MODI_ASSERT(0);
	}
		
	pResult->m_pClient->SendPackage( &msg , sizeof(msg) );

	return 0;
}

void 	MODI_DBResultHandler::CreateKey( MODI_LoginKey & key )
{
	unsigned int nRand = rand()%0xFFFF;
	unsigned long nCreateTime = GetTickCount( true ) % (nRand+1);

	memset( key.m_szContnet , 0 , sizeof(key.m_szContnet) );
	memcpy( key.m_szContnet , &nCreateTime , sizeof(nCreateTime) ); // login key
}

void 	MODI_DBResultHandler::CreatePP( defAccountID nAccountID ,
				MODI_CHARID 	nCharID ,
				const MODI_LoginKey & key ,
				const char * szAccount , 
				const char * szPwd , 
				MODI_LoginPassport & pp )
{
	char szOut[512];
	memset( szOut , 0 , sizeof(szOut) );
	size_t nAccSize = strlen(szAccount);
	if( nAccSize > MAX_ACCOUNT_LEN )
		return ;

	pp.m_nAccountID = nAccountID;
	pp.m_nCharID = nCharID;
	size_t nKeySize = sizeof(key);
	memcpy( szOut , &key , nKeySize );
	MODI_GameHelpFun::StringToLower( szAccount ,  nAccSize , szOut + nKeySize );
	size_t nTotalLen = nAccSize + nKeySize;

	// md5 pp
	MD5Context m;
	MD5Init( &m );
	MD5Update( &m , (unsigned char*)szOut , nTotalLen );
	MD5Final( (unsigned char *)(pp.m_szContent) , &m );
}
