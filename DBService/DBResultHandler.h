/** 
 * @file DBResultHandler.h
 * @brief DB操作结果处理
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef DBRESULT_HANDLER_H_
#define DBRESULT_HANDLER_H_

#include <map>
#include "Type.h"
#include "DBServerClient.h"
#include "GameClient_AccountDB.h"
#include "s2adb_cmd.h"
#include "DBServerClient.h"
 
 
/* brief DB服务器上的数据库操作结果处理函数
 *
 */
class MODI_DBResultHandler 
{
	public:

		MODI_DBResultHandler();
		virtual	~MODI_DBResultHandler();

	public:

		/** 
		 * @brief 对登入请求的处理
		 * 
		 * @param pResult   数据库操作结果
		 * 
		 * @return 成功返回0	
		 */
		static int Handler_RequestLogin( MODI_DBSql * pResult );

		/** 
		 * @brief 登入请求的(PP的)验证
		 * 
		 * @param pResult
		 * 
		 * @return 	
		 */
		static int Handler_RequestAuth( MODI_DBSql * pResult );


		/// 重新生成PP的请求
		static int Handler_RequestRemakePP( MODI_DBSql * pResult );

		/** 
		 * @brief 帐号管理器
		 * 
		 * @return 返回帐号管理器的引用	
		 */
		static MODI_AccountMgr_ADB & 	GetAccountMgr()
		{
			return ms_AccountsMgr;
		}

	private:

		static void 	CreateKey( MODI_LoginKey & key );

		static void 	CreatePP( defAccountID nAccountID ,
				MODI_CHARID 	nCharID ,
				const MODI_LoginKey & key ,
				const char * szAccount , 
				const char * szPwd , 
				MODI_LoginPassport & pp );
 public:
		/** 
		 * @brief 客户端成功登入时验证
		 * 
		 * @param nAccountID 	帐号id
		 * @param szAccount 	帐号
		 * @param szPwd 		密码
		 * @param bFangChengMi   是否需要防沉迷
		 * @param sessionid 	会话ID
		 * @param pClient  		游戏服务器客户端
		 */
		static void 	OnClientLogin( defAccountID nAccountID ,
				const char * szAccount ,
				const char * szPwd ,
				bool 		bFangChengMi,
				const MODI_SessionID & sessionid,
				MODI_DBServerClientPtr pClient );

		/** 
		 * @brief 生成某个客户端的KEY和PP 
		 * 
		 * @param pGameClient 	客户端
		 * @param nAccountID 	帐号id
		 * @param szAccount 	帐号
		 * @param szPwd 		帐号密码
		 * @param pClient       游戏服务器客户端
		 */
		static void 	OnMakeKeyAndPP( MODI_GameClient_ADB * pGameClient ,
			   	defAccountID nAccountID ,
				const char * szAccount ,
				const char * szPwd ,
				MODI_DBServerClientPtr pClient );

		/** 
		 * @brief 发送登入结果
		 * 
		 * @param nResult  	登入结果
		 * @param pResult 	具体结果
		 * @param sessionid 会话id
		 * @param pClient   游戏服务器客户端
		 */
		static void 	SendLoginResult( WORD nResult ,
				MODI_ADB2S_Notify_LoginResult::MODI_Result * pResult ,
				const MODI_SessionID  & sessionid ,
				MODI_DBServerClientPtr pClient );

	private:

		static 	MODI_AccountMgr_ADB 	ms_AccountsMgr;
};

#endif
