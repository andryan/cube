/** 
 * @file DBLogicThread.h
 * @brief DB服务器的逻辑处理线程
 * @author Tang Teng
 * @version v0.1
 * @date 2010-02-27
 */

#ifndef DBLOGIC_THREAD_H_
#define DBLOGIC_THREAD_H_

#include "Type.h"
#include "Thread.h"
#include "DBSqlQueue.h"
#include "SingleObject.h"

class MODI_LogicThread: public MODI_Thread , public CSingleObject<MODI_LogicThread>
{
public:

	MODI_LogicThread();

	virtual ~MODI_LogicThread();

    /// 主循环
    void Run();

	void Push( MODI_DBSql * p );

private:

	MODI_DBSql * 	Pop();

private:

	MODI_DBSqlQueue 	m_sqlQueue;
};

#endif
