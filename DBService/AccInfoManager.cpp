/**
 * @file   AccInfoManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:46:11 2011
 * 
 * @brief  账号信息管理
 * 
 * 
 */


#include "DBStruct.h"
#include "DBClient.h"
#include "AccInfoManager.h"
#include "DBServerClient.h"
#include "DBResultHandler.h"

MODI_AccInfoManager * MODI_AccInfoManager::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccInfoManager::Init()
{
	Start();
	return true;
}


/** 
 * @brief 增加一个账号
 *
 * @return 成功ture,失败false
 */
bool MODI_AccInfoManager::AddAccInfo(MODI_AccInfo *  acc_info)
{
	std::pair<defAccInfoMapIter, bool> ret_code;
	m_stMapLock.wrlock();
	ret_code = m_AccInfoMap.insert(defAccInfoMapValue(acc_info->m_cstrAccName, acc_info));
	m_stMapLock.unlock();
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("[add_info] add a info to map <name=%s,pwd=%s,cardnum=%s>", acc_info->m_cstrAccName, acc_info->m_cstrPassword, acc_info->m_cstrCardNum);
#endif
	
	return ret_code.second;
}


/** 
 * @brief 获取账号信息 
 * 
 * @return 成功true,失败false
 *
 */
MODI_AccInfo *  MODI_AccInfoManager::GetAccInfo(const char * acc_name)
{
	MODI_AccInfo * p_info = NULL;
	m_stMapLock.rdlock();
	defAccInfoMapIter iter = m_AccInfoMap.find(acc_name);
	if(iter != m_AccInfoMap.end())
	{
		p_info = iter->second;
	}
	m_stMapLock.unlock();
	return p_info;
}


/** 
 * @brief 从队列里面获取账号信息到map里面
 * 
 */
void MODI_AccInfoManager::GetAccFromQueue()
{
	m_stQueueLock.wrlock();
	while(! m_AccInfoQueue.empty())
	{
		MODI_AccInfo * p_info = m_AccInfoQueue.front();
		AddAccInfo(p_info);
		m_AccInfoQueue.pop();
	}
	m_stQueueLock.unlock();
}


/** 
 * @brief 增加一个账号信息到队列
 * 
 * @param p_info 要增加的账号
 */
void MODI_AccInfoManager::AddAccToQueue(MODI_AccInfo * p_info)
{
	m_stQueueLock.wrlock();
	m_AccInfoQueue.push(p_info);
	m_stQueueLock.unlock();
}


/** 
 * @brief 更新
 * 
 */
void MODI_AccInfoManager::Run()
{
	while(! IsTTerminate())
	{
		::usleep(1000*100);
		m_stCurrentTime.GetNow();
		/// 10秒
		if(m_stUpDateTime(m_stCurrentTime))
		{
			GetAccFromQueue();
		}
	}
}

bool MODI_AccInfo::IsChenMi(MODI_TTime & cur_time) const
{
	//// 压力测试不需要沉迷
	std::string card_num = m_cstrCardNum;
	if(card_num.size() == 18)
	{
		std::string str_year = card_num.substr(7, 4);
		std::string str_mon = card_num.substr(11, 2);
		std::string str_day = card_num.substr(13, 2);
		
		WORD year = atoi(str_year.c_str());
		WORD mon = atoi(str_mon.c_str());
		WORD day = atoi(str_day.c_str());

		WORD cur_year = cur_time.GetYear();
		WORD cur_mon = cur_time.GetMon();
		WORD cur_day = cur_time.GetMDay();

		QWORD sub_day = (cur_year * 365 + cur_mon * 30 + cur_day) - (year * 365 + mon * 30 + day);
		if(sub_day > 18 * 365)
		{
			return false;
		}
			
#ifdef _HRX_DEBUG
		Global::logger->debug("[user_login] a user login  is chenmi<card_num=%s,year=%u,mon=%u,day=%u>", card_num.c_str(), year, mon, day);
#endif
		return true;
	}
	return false;
}


/** 
 * @brief 校验返回
 * 
 * @param out_para 返回结果
 * 
 */
void MODI_AccInfoManager::RetVerifyResult(MODI_AccInfo * p_info, MODI_SessionID & session_id, MODI_DBServerClientPtr p_client)
{
	m_stTTime.GetNow();
	bool chen_mi = p_info->IsChenMi(m_stTTime);
	MODI_DBResultHandler ::GetAccountMgr().Del(p_info->m_dwAccID, true );
	MODI_DBResultHandler::OnClientLogin(p_info->m_dwAccID,
										p_info->m_cstrAccName,
										p_info->m_cstrPassword,
										chen_mi,
									   	session_id,
										p_client);
}
