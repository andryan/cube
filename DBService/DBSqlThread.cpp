#include "DBSqlThread.h"
#include "Global.h"
#include "DBLogicThread.h"


MODI_SqlThread::MODI_SqlThread(const std::string & name):MODI_Thread(name.c_str())
{

}

MODI_SqlThread::~MODI_SqlThread()
{
	m_sqlQueue.Clear();
}

void MODI_SqlThread::Run()
{
    while ( true )
    {
		if( IsTTerminate() && !this->Pop() )
			break;

		// 该线程的工作:
		// 访问数据库,返回结果值,递交给逻辑线程处理

		MODI_DBSql * p = this->Pop();
		if( !p )
		{
			::usleep(10);
			continue ;
		}

		// 根据 SQL 语句的类型，进行相关查询
		if( p->m_iSqlType == enSqlStatement_Type_Select ) 
		{
			this->ExecuteSelect( p );
		}
		else if( p->m_iSqlType == enSqlStatement_Type_Insert )
		{
			this->ExecuteInsert( p );
		}
		else if( p->m_iSqlType == enSqlStatement_Type_Delete )
		{
			this->ExecuteDelete( p );
		}
		else if( p->m_iSqlType == enSqlStatement_Type_Update )
		{
			this->ExecuteUpdate( p );
		}
		else if( p->m_iSqlType == enSqlStatement_Type_Logic )
		{
			// NONE 类型，直接推给逻辑线程处理
			MODI_LogicThread::GetInstanceRef().Push( p );
		}
    }

	Global::logger->debug("[%s] DB SqlThread will Exit." , SVR_TEST ); 
}

void MODI_SqlThread::Push( MODI_DBSql * p )
{
	m_sqlQueue.Push( p );
}

MODI_DBSql * MODI_SqlThread::Pop()
{
	return m_sqlQueue.Pop();
}


bool MODI_SqlThread::ExecuteDelete( MODI_DBSql * pDBSql )
{
	if( !MODI_LogicThread::GetInstancePtr() )
		return false;

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	if( !pDBClient )
		return false;

	pDBSql->m_nSqlRet =	pDBClient->ExecSql( pDBSql->m_strSql.c_str() , pDBSql->m_strSql.size() );

	MODI_LogicThread::GetInstanceRef().Push( pDBSql );

	MODI_DBManager::GetInstance().PutHandle( pDBClient );

	return true;
}

bool MODI_SqlThread::ExecuteUpdate( MODI_DBSql * pDBSql )
{
	return this->ExecuteDelete( pDBSql );
}

bool MODI_SqlThread::ExecuteInsert( MODI_DBSql * pDBSql )
{
	return this->ExecuteDelete( pDBSql );
}

bool MODI_SqlThread::ExecuteSelect( MODI_DBSql * pDBSql )
{
	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	if( !pDBClient )
		return false;

	pDBSql->AllocResultRecordContainer();
	pDBSql->m_nSqlRet = pDBClient->ExecSelect( *pDBSql->m_pResults , pDBSql->m_strSql.c_str() , pDBSql->m_strSql.size() );

	// 将结果推入逻辑处理线程处理（有可能没有结果)

	MODI_LogicThread::GetInstanceRef().Push( pDBSql );

	// 回收
	MODI_DBManager::GetInstance().PutHandle( pDBClient );

	return true;
}
