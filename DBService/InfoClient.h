/**
 * @file   InfoClient.h
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:07:02 2011
 * 
 * @brief  连接平台账号验证
 * 
 * 
 */


#ifndef _MD_INFOTASK_H
#define _MD_INFOTASK_H

#include "Global.h"
#include "Timer.h"
#include "RClientTask.h"
#include "CommandQueue.h"
#include "s2info_cmd.h"

class MODI_AccVerifyData;

/**
 * @brief bill连接
 * 
 */
class MODI_InfoClient: public MODI_RClientTask, public MODI_CmdParse
{
public:
	MODI_InfoClient(const char * name, const char * server_ip,const WORD & port): MODI_RClientTask(name, server_ip, port), m_strClientName(name)
	{
		Resize(GWTOGS_CMDSIZE);
	}

	static MODI_InfoClient & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_InfoClient("infoclient", Global::g_strInfoIP.c_str(), Global::g_wdInfoPort);
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
	/// 初始化
	bool Init();

	const bool IsConnected();

	/** 
	 * @brief 发送命令
	 * 
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 队列内的命令
	 * 
	 * @param pt_null_cmd 获取的命令
	 * @param dwCmdLen 命令长度
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

	/** 
	 * @brief 账号校验
	 * 
	 * @param in_para 需要校验的参数
	 * 
	 */
	bool ReqVerifyAcc(const MODI_AccVerifyData & in_para);
	

	/** 
	 * @brief 校验返回
	 * 
	 * @param out_para 返回结果
	 * 
	 */
	void RetVerifyResult(const MODI_AccVerifyData & ret_data);

 private:
	static MODI_InfoClient * m_pInstance;
	virtual ~MODI_InfoClient()
	{
		ClientFinal();
	}
	
	void ClientFinal();

	std::string m_strClientName;

	MODI_TTime m_stTTime;
};


#endif
