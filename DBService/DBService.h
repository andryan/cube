/**
 * @file DBService.h
 * @date 2010-01-18 CST
 * @version $Id $
 * @author hurixin hurixin@modi.com
 * @brief 数据库服务器类
 *
 */

#ifndef MDDBSERVICE_H
#define MDDBSERVICE_H

//#include "ServiceTaskPoll.h"
#include "ServiceTaskSched.h"
#include "NetService.h"
#include "SingleObject.h"

/**
 * @brief 数据库服务器类
 *
 */
class MODI_DBService: public MODI_NetService , public CSingleObject<MODI_DBService>
{
 public:
	MODI_DBService( unsigned short nPort , const std::string & service_name = "DBService"): MODI_NetService(service_name.c_str()),m_nADBPort(nPort)
	{

	}
		
	~MODI_DBService(){};

	/// 初始化
	bool Init( );

	/// 创建一个新的连接
	bool CreateTask(const int sock, const sockaddr_in * addr);

	/// 释放资源
	void Final();

	
 private:

	/// 轮询对象
	//MODI_ServiceTaskPoll * m_stTaskPoll;
	MODI_ServiceTaskSched m_stTaskSched;

	unsigned short 	m_nADBPort;
};

#endif


