/**
 * @file   InfoClient.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Sun Jan 30 15:15:17 2011
 * 
 * @brief  平台验证连接
 * 
 * 
 */


#include "AssertEx.h"
#include "InfoClient.h"
#include "AccVerifyManager.h"
#include "DBResultHandler.h"
#include "AccInfoManager.h"

MODI_InfoClient * MODI_InfoClient::m_pInstance = NULL;
 
/** 
 * @brief 初始化
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_InfoClient::Init()
{
	if(! MODI_RClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 释放
 * 
 */
void MODI_InfoClient::ClientFinal()
{
	Terminate(true);
	TTerminate();
	Join();
}


/** 
 * @brief 发送命令
 * 
 * @param pt_null_cmd 要发送的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_InfoClient::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}
	return MODI_RClientTask::SendCmdInConnect(pt_null_cmd, cmd_size);
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 
 */
bool MODI_InfoClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd))))
    {
        return false;
    }
	this->Put( pt_null_cmd , cmd_size );
    return true;
}

/** 
 * @brief 队列内的命令
 * 
 * @param pt_null_cmd 获取的命令
 * @param dwCmdLen 命令长度
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_InfoClient::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}

#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] infoclient recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif

	/// 登录效验返回
	if(pt_null_cmd->byCmd == MAINCMD_S2INFO && pt_null_cmd->byParam == MODI_Info2S_Verify_Result::ms_Param)
	{
		const MODI_Info2S_Verify_Result * recv_cmd = (const MODI_Info2S_Verify_Result *)pt_null_cmd;
		MODI_AccVerifyData ret_data;
		if(! MODI_AccVerifyManager::GetInstance().IsAccVerifyData(recv_cmd->m_SessionID, ret_data))
		{
			Global::logger->fatal("[ret_acc_verify] acc verify ret but not find session <accid=%u>", recv_cmd->m_dwAccID);
			MODI_ASSERT(0);
			return false;
		}
		strncpy(ret_data.m_cstrCardNum, recv_cmd->m_cstrCardNum, sizeof(ret_data.m_cstrCardNum) -1 );
		ret_data.m_enResult = recv_cmd->m_enResult;
		ret_data.m_stAccountID = recv_cmd->m_dwAccID;
		RetVerifyResult(ret_data);
		MODI_AccVerifyManager::GetInstance().RemoveAccVerifyData(recv_cmd->m_SessionID);
		return true;
	}
	return true;	
}


const bool MODI_InfoClient::IsConnected()
{
	bool is_connect = false;
	is_connect = MODI_RClientTask::IsConnected();
	return is_connect;
}


/** 
 * @brief 账号校验
 * 
 * @param in_para 需要校验的参数
 * 
 */
bool MODI_InfoClient::ReqVerifyAcc(const MODI_AccVerifyData & in_para)
{
	/// 添加到管理器
	if(! MODI_AccVerifyManager::GetInstance().AddAccVerifyData(in_para))
	{
		Global::logger->debug("[add_verify_data] add verify data faild <%s,accname=%s>", in_para.m_SessionID.ToString(), in_para.m_szAccountName);
		//return false;
	}
	
	MODI_S2Info_Verfiy_Account send_cmd;
	send_cmd.m_SessionID = in_para.m_SessionID;
	strncpy(send_cmd.m_szAccountName, in_para.m_szAccountName, sizeof(send_cmd.m_szAccountName) - 1);
	strncpy(send_cmd.m_szAccountPwd, in_para.m_szAccountPwd, sizeof(send_cmd.m_szAccountPwd) - 1);
	SendCmd(&send_cmd, sizeof(send_cmd));

#ifdef _HRX_DEBUG
	Global::logger->debug("[verify_acc] send a acc verify command <name=%s,password=%s>", in_para.m_szAccountName, in_para.m_szAccountPwd);
#endif
	
	return true;
}


/** 
 * @brief 校验返回
 * 
 * @param out_para 返回结果
 * 
 */
void MODI_InfoClient::RetVerifyResult(const MODI_AccVerifyData & ret_data)
{
#ifdef _HRX_DEBUG
	Global::logger->debug("[acc_verify] infoservice ret acc verify <satate=%d>", ret_data.m_enResult);
#endif	
	if(ret_data.m_enResult == enVerifySuccess)
	{
		m_stTTime.GetNow();
		bool chen_mi = false;
		
#ifdef _USE_CHENMI		
		 chen_mi = ret_data.IsChenMi(m_stTTime);
#endif
		 
		MODI_DBResultHandler ::GetAccountMgr().Del(ret_data.m_stAccountID, true );
		MODI_DBResultHandler::OnClientLogin(ret_data.m_stAccountID,
											ret_data.m_szAccountName,
											ret_data.m_szAccountPwd,
											chen_mi,
										   	ret_data.m_SessionID,
											ret_data.m_pSource);
		
#ifdef _HRX_DEBUG
		Global::logger->debug("[ret_login] ret login <name=%s,pwd=%s,accid=%u>", ret_data.m_szAccountName, ret_data.m_szAccountPwd, ret_data.m_stAccountID);
#endif
		
		MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(ret_data.m_szAccountName);
		if(p_info)
		{
			p_info->m_dwAccID = ret_data.m_stAccountID;
			//strncpy(p_info->m_cstrAccName, ret_data.m_szAccountName, sizeof(p_info->m_cstrAccName) - 1);
			strncpy(p_info->m_cstrPassword, ret_data.m_szAccountPwd, sizeof(p_info->m_cstrPassword) - 1);
			strncpy(p_info->m_cstrCardNum, ret_data.m_cstrCardNum, sizeof(p_info->m_cstrCardNum) - 1);
		}
		else
		{
			/// 在这里做个缓冲队列
			MODI_AccInfo * p_info = new MODI_AccInfo();
			p_info->m_dwAccID = ret_data.m_stAccountID;
			strncpy(p_info->m_cstrAccName, ret_data.m_szAccountName, sizeof(p_info->m_cstrAccName) - 1);
			strncpy(p_info->m_cstrPassword, ret_data.m_szAccountPwd, sizeof(p_info->m_cstrPassword) - 1);
			strncpy(p_info->m_cstrCardNum, ret_data.m_cstrCardNum, sizeof(p_info->m_cstrCardNum) - 1);
			MODI_AccInfoManager::GetInstance().AddAccToQueue(p_info);
#ifdef _HRX_DEBUG
			Global::logger->debug("[add_info] add a info <name=%s,pwd=%s,accid=%u>", p_info->m_cstrAccName, p_info->m_cstrPassword, p_info->m_dwAccID);
#endif			
		}
		
	}
	else if(ret_data.m_enResult == enVerifyNoActive)
	{
		MODI_DBResultHandler::SendLoginResult(SvrResult_Login_NoActive , 0 ,ret_data.m_SessionID, ret_data.m_pSource);
	}
	else if(ret_data.m_enResult == enVerifyTimeout)
	{
		MODI_DBResultHandler::SendLoginResult(SvrResult_Login_ServerError, 0 ,ret_data.m_SessionID, ret_data.m_pSource);
	}
	else
	{
		MODI_DBResultHandler::SendLoginResult(SvrResult_Login_InvaildAP, 0 ,ret_data.m_SessionID, ret_data.m_pSource);
	}
}
