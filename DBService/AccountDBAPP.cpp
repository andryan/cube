#include "AccountDBAPP.h"
#include "InfoClient.h"
#include "AccInfoManager.h"
#include "SGPClientSched.h"

MODI_AccountDBAPP::MODI_AccountDBAPP( const char * szAppName ):MODI_IAppFrame( szAppName ),
	m_pSqlThread(0)
{


}


MODI_AccountDBAPP::~MODI_AccountDBAPP()
{


}

int MODI_AccountDBAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
		return iRet;

	// account db 的配置信息
	const MODI_SvrADBConfig  * pADBConfig = (const MODI_SvrADBConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_ACCOUNTSERVER ));
	if( !pADBConfig )
	{
		Global::logger->fatal("[%s] Faild 3 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	// 增加一个日志文件到指定目录
    Global::logger->AddLocalFileLog(pADBConfig->strLogPath);
	std::string net_log_path = pADBConfig->strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());
	Global::net_logger->RemoveConsoleLog();

	// 初始化网络派发器
	if( !m_PackagehandlerInst.Initialization() )
	{
		Global::logger->fatal("[%s] Faild 4 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enInitPackageHandlerFaild;
	}

	/// 本地测试和TG
	if(Global::g_wdPlatformFlag == 0 || Global::g_wdPlatformFlag == 3)
	{
		// 连接数据库
		if(! MODI_DBManager::GetInstance().Init(pADBConfig->m_strURL.c_str()) )
		{
			Global::logger->fatal("[%s] Faild 5 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
			return MODI_IAppFrame::enInitDBSystemFaild;
		}
	}

	/// 启动accinfomanager
	if(! MODI_AccInfoManager::GetInstance().Init())
	{
		Global::logger->fatal("[%s] Faild is acc info not start %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enNoMemory;
	}

	if(Global::g_wdPlatformFlag > 1)
	{

		if(! MODI_SGPClientSched::GetInstance().Init())
		{
			Global::logger->fatal("[%s] Faild is sgp client not start %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
			return MODI_IAppFrame::enNoMemory;
		}
	}
	

	// 创建逻辑线程对象
	m_pLogicThread = new MODI_LogicThread();
	if( !m_pLogicThread )
	{
		Global::logger->fatal("[%s] Faild 6 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enNoMemory;
	}

	// 创建SQL执行线程对象
	m_pSqlThread = new MODI_SqlThread();
	if( !m_pSqlThread )
	{
		Global::logger->fatal("[%s] Faild 7 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enNoMemory;
	}
	
	// 创建ADB网络服务
	m_pNetService = new MODI_DBService( pADBConfig->nPort );
	if( !m_pNetService )
	{
		Global::logger->fatal("[%s] Faild 8 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
		return MODI_IAppFrame::enNoMemory;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_AccountDBAPP::Run()
{
	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 	{
// 		Global::logger->fatal("[%s] Faild 9 %s ...", LOGACT_SYS_INIT , m_strAppName.c_str() );
// 		return enPidFileFaild;
// 	}

	/// 启动infoclient
	if(! MODI_InfoClient::GetInstance().Init())
	{
		Global::logger->fatal("[%s] Unable init infoclient(ip=%s,port=%d)", SYS_INIT, Global::g_strInfoIP.c_str(), Global::g_wdInfoPort);
	}
	
	// 启动逻辑处理线程
	if( m_pLogicThread )
	{
		m_pLogicThread->Start();
	}

	// 启动SQL执行线程
	if( m_pSqlThread )
	{
		m_pSqlThread->Start();
	}

	// 运行网络服务
	if( m_pNetService )
	{
		m_pNetService->Main();
	}

	return MODI_IAppFrame::enOK;
}


int MODI_AccountDBAPP::Shutdown()
{
	// 终止网络服务
	if( m_pNetService )
	{
		m_pNetService->Terminate();
	}

	// 终止SQL线程
	if( m_pSqlThread )
	{
		m_pSqlThread->TTerminate();
		m_pSqlThread->Join();
	}

	// 终止逻辑处理线程
	if( m_pLogicThread )
	{
		m_pLogicThread->TTerminate();
		m_pLogicThread->Join();
	}

	// free 
	delete m_pNetService;

	delete m_pLogicThread;

	delete m_pSqlThread;

	MODI_InfoClient::GetInstance().DelInstance();
	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}


