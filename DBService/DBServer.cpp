/** 
 * @file DBServer.cpp
 * @brief 帐号服务器
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-10
 */

#include "AccountDBAPP.h"

int Test()
{
	if(! MODI_DBManager::GetInstance().Init(Global::Value[GAME_DB_URL].c_str()))
	{
		return -1;
	}

	MODI_DBClient * pDBClient = MODI_DBManager::GetInstance().GetHandle();
	if( !pDBClient )
		return -2;


//   const char * szInsertSql = "INSERT INTO `modi_account_db`.`ACCOUNTS` (`account_id` ,`account` ,`password` ,`email` ,`activeGame`)VALUES (NULL , 'guest%d', 'guest%d', 'guest%d@modi.com.cn', '1')"; 
//	
//	for( unsigned int n = 0; n < 100000; n++ )
//	{
//		char szTemp[512];
//		char szSql[1024 * 2];
//		memset( szSql , 0 , sizeof(szSql) );
//		memset( szTemp , 0 , sizeof(szTemp) );
//		SNPRINTF( szTemp , sizeof( szTemp ) , szInsertSql , n , n , n );
//		pDBClient->ExecSql( szTemp , strlen(szTemp) , false );
//	}
//
	MODI_RecordContainer * pResult = new MODI_RecordContainer();
	std::string strSql = MODI_SqlStatement::CreateStatement_Load_CharData( 2011 , 42 );
	if( !pDBClient->ExecSelect( *pResult ,  strSql.c_str() , strSql.size() ) )
		return -3;

	
	for( int i = 0; i < pResult->Size(); i++ )
	{
		MODI_Record * pRecord = pResult->GetRecord( i );
		if( !pRecord )
			continue ;

		const MODI_VarType & v_avataridx = pRecord->GetValue("avataridx");
		int i1 = v_avataridx.ToInt();
		unsigned int n1 = v_avataridx.ToUInt();
		
		cout << i1 << n1 << endl;
	}

	pResult->Clear();
	delete pResult;
	MODI_DBManager::GetInstance().PutHandle( pDBClient );

	return 0;
}

/**
 * @brief 主函数
 *
 *
 */
int main(int argc, char * argv[])
{
	// 解析命令行参数
	PublicFun::ArgParse(argc, argv);

	MODI_AccountDBAPP 	app("Account DB Server.");

	int iRet = app.Init();

	if( iRet == MODI_IAppFrame::enOK )
	{
		iRet = app.Run();
	}

	app.Shutdown();

	::sleep(1);

	return iRet;
}

