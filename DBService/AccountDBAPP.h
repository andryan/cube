/** 
 * @file AccountDBAPP.h
 * @brief 帐号数据库服务器的应用程序框架
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-20
 */

#ifndef MODI_ADB_APPFRAME_H_
#define MODI_ADB_APPFRAME_H_


#include "IAppFrame.h"
#include "DBClient.h"
#include "DBStruct.h"
#include "PackageHandler_adb.h"
#include "DBLogicThread.h"
#include "DBSqlThread.h"
#include "DBService.h"
#include "SqlStatement.h"


class 	MODI_AccountDBAPP : public MODI_IAppFrame
{
	public:

		explicit MODI_AccountDBAPP( const char * szAppName );

		virtual ~MODI_AccountDBAPP();

		virtual 	int Init();

		virtual 	int Run();

		virtual 	int Shutdown();

	private:

		MODI_PackageHandler_adb 	m_PackagehandlerInst; //  数据包派发器
		MODI_SqlThread  *  			m_pSqlThread; // sql 操作执行线程
};

#endif
