/**
 * @file   SGPClientTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Tue Sep 27 17:19:44 2011
 * 
 * @brief  新加坡登录
 * 
 * 
 */



#include "SGPClientTick.h"
#include "Global.h"
#include "SGPClientSched.h"

MODI_SGPClientTick * MODI_SGPClientTick::m_pInstance = NULL;

MODI_SGPClientTick::MODI_SGPClientTick() : MODI_Thread("sgpclienttick"), m_stCmdFluxTime(24 * 3600 * 1000)
{
	
}

MODI_SGPClientTick::~MODI_SGPClientTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_SGPClientTick::Run()
{
    while(!IsTTerminate())
    {
		m_stRTime.GetNow();
    	::usleep(10000);
		MODI_SGPClientSched::GetInstance().Get(1000);
    }
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_SGPClientTick::Final()
{
	
}
