#include "DBTask.h"
#include "AssertEx.h"
#include "PackageHandler_db.h"

MODI_DBTask::MODI_DBTask(const int sock, const struct sockaddr_in * addr): MODI_ServiceTask(sock, addr)
{
	m_pDBSvrClient = MODI_DBServerClientPtr( new MODI_DBServerClient( this ) );
}

MODI_DBTask::~MODI_DBTask()
{
	m_pDBSvrClient = 0;
}


/// 回收连接
int MODI_DBTask::RecycleConn()
{
	return 1;
}
	
/**
 * @brief 命令处理
 *
 */
bool MODI_DBTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{

	if(pt_null_cmd == NULL || cmd_size == 0)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	// 处理消息包,生成SQL语句,丢给 SQL 执行线程执行
	
	// handler package 
	MODI_PackageHandler_db::GetInstancePtr()->DoHandlePackage( pt_null_cmd->byCmd , pt_null_cmd->byParam ,
			m_pDBSvrClient , pt_null_cmd , cmd_size );
	
	return true;
}


