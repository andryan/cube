/**
 * @file   InfoTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:24:45 2011
 * 
 * @brief  处理登陆验证
 * 
 * 
 */

#include "Global.h"
#include "InfoTick.h"
#include "AccInfoManager.h"
#include "AccVerifySched.h"

MODI_InfoTick * MODI_InfoTick::m_pInstance = NULL;


MODI_InfoTick::MODI_InfoTick() : MODI_Thread("infotick"), m_stCmdFluxTime(24 * 3600 * 1000)
{
	
}

MODI_InfoTick::~MODI_InfoTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_InfoTick::Run()
{

	MODI_RTime m_stCurrentTime;
	QWORD get_time_delay = 0;
	
	while(! IsTTerminate())
	{
		::usleep(10000);
		m_stCurrentTime.GetNow();

		/// 同步账号
		//MODI_AccInfoManager::GetInstance().UpDate(m_stCurrentTime);
		MODI_AccVerifySched::GetInstance().UpDate();
		
		/// 每60秒
		if(m_stCurrentTime.GetMSec() > get_time_delay)
		{
		   	get_time_delay = m_stCurrentTime.GetMSec() + 1000 * 60;
		}
	}
	
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_InfoTick::Final()
{
	
}
