/**
 * @file   AccVerifySched.cpp
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Mon Oct 11 10:24:55 2010
 * @version $Id:$
 * @brief  校验登录账号
 * 
 * 
 */

#include "AccVerifySched.h"
#include "AccVerifyTick.h"

MODI_AccVerifySched * MODI_AccVerifySched::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccVerifySched::Init()
{
	if(! m_stClientSched.Init())
	{
		Global::logger->fatal("Unable initialization client sched");
		return false;
	}

	if(! MODI_AccVerifyTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization acc verify start");
		return false;
	}
	
	return true;
}

/** 
 * @brief 增加一个校验
 * @param p_client 校验
 *
 */
void MODI_AccVerifySched::AddVerifyClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.push_back(p_client);
	m_stRWLock.unlock();
}

/** 
 * @brief 移出一个校验
 * @param p_client 校验
 *
 */
void MODI_AccVerifySched::RemoveVerifyClient(MODI_PClientTask * p_client)
{
	m_stRWLock.wrlock();
	m_ClientList.remove(p_client);
	m_stRWLock.unlock();
}


/** 
 * @brief 账号校验
 * 
 * @param in_para 需要校验的参数
 * 
 */
bool MODI_AccVerifySched::ReqVerifyAcc(MODI_S2Info_Verfiy_Account * in_para)
{
	if(!in_para)
	{
		return false;
	}
	
	/// 不阻塞调用线程,放到tick里面去做
	Put((Cmd::stNullCmd *) in_para, sizeof(MODI_S2Info_Verfiy_Account));
	return true;
	
// 	MODI_AccVerifyClient * p_client = new MODI_AccVerifyClient(in_para);
// 	if(!p_client)
// 	{
// 		Global::logger->fatal("Unable new a client");
// 		return false;
// 	}
// 	if(p_client->Init())
// 	{
// 		m_stClientSched.AddNormalSched(p_client);
// 		p_client->SendVerifyCmd();
// 		return true;
// 	}
// 	delete p_client;
// 	return false;
}

void MODI_AccVerifySched::UpDate()
{

	if(m_ClientList.empty())
	{
		return;
	}

	m_stRWLock.rdlock();
	std::list<MODI_PClientTask * >::iterator next, iter;
	for(iter=m_ClientList.begin(), next=iter, next++; iter!=m_ClientList.end(); iter=next, next++)
	{
		MODI_AccVerifyClient * p_client = (MODI_AccVerifyClient *)(*iter);
		p_client->Get(1);
	}
	m_stRWLock.unlock();
}




bool MODI_AccVerifySched::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
#ifdef _HRX_DEBUG
	Global::logger->debug("--->>>> have new a acc verify client");
#endif	
	MODI_S2Info_Verfiy_Account * in_para = (MODI_S2Info_Verfiy_Account *)pt_null_cmd;
	MODI_AccVerifyClient * p_client = new MODI_AccVerifyClient(in_para);
	if(!p_client)
	{
		Global::logger->fatal("Unable new a client");
		return false;
	}
	if(p_client->Init())
	{
		m_stClientSched.AddNormalSched(p_client);
		p_client->SendVerifyCmd();
		return true;
	}
	delete p_client;
	return false;
}
