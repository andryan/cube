/**
 * @file   InfoTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:23:37 2011
 * 
 * @brief  处理登陆验证
 * 
 * 
 */



#ifndef _MDINFOTICK_H
#define _MDINFOTICK_H

#include "Type.h"
#include "Timer.h"
#include "Thread.h"

class MODI_InfoTick: public MODI_Thread 
{
public:
    MODI_InfoTick();
	
    virtual ~MODI_InfoTick();

    void Final();

    virtual void Run();

	static MODI_InfoTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_InfoTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
private:
	
	static MODI_InfoTick * m_pInstance;
	MODI_RTime m_stRTime;
	MODI_Timer m_stCmdFluxTime;
};

#endif
