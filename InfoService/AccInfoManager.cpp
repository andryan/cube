/**
 * @file   AccInfoManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:46:11 2011
 * 
 * @brief  账号信息管理
 * 
 * 
 */


#include "DBStruct.h"
#include "DBClient.h"
#include "AccInfoManager.h"
#include "InfoTask.h"

MODI_AccInfoManager * MODI_AccInfoManager::m_pInstance = NULL;

/** 
 * @brief 初始化
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccInfoManager::Init()
{
	/// load acc info from db
	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("account_id");
	select_field.Put("name");
	select_field.Put("cardnum");
	
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("[info_init] acc info manager not get dbclient");
		return false;
	}
	
	int rec_count = p_dbclient->ExecSelect(record_select, MODI_InfoTask::m_pAccTbl, NULL, &select_field, true);
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("[info_init] get %d acc info record from accountstbl", rec_count);
#endif

	for(int i=0; i<rec_count; i++)
	{
		MODI_Record * p_select_record = record_select.GetRecord(i);
		if(p_select_record == NULL)
		{
			continue;
		}

		MODI_VarType v_id = p_select_record->GetValue("account_id");
		MODI_VarType v_name = p_select_record->GetValue("name");
		MODI_VarType v_cardnum = p_select_record->GetValue("cardnum");
		if(v_id.Empty() || v_name.Empty() || v_cardnum.Empty())
		{
			continue;
		}
		
		defAccountID account_id = (defAccountID)v_id;
		std::string name = (const char *)v_name;
		std::string card_num = (const char *)v_cardnum;
		
		MODI_AccInfo * acc_info = new MODI_AccInfo();
		strncpy(acc_info->m_cstrAccName, name.c_str(), sizeof(acc_info->m_cstrAccName) - 1);
		strncpy(acc_info->m_cstrCardNum, card_num.c_str(), sizeof(acc_info->m_cstrCardNum) - 1);
		acc_info->m_dwAccID = account_id;
		AddAccInfo(acc_info);
		
		//#ifdef _HRX_DEBUG
		if(i % 10000 == 0)
			Global::logger->debug("add a acc info <accid=%u,name=%s,cardnum=%s>", account_id, name.c_str(), card_num.c_str());
		//#endif
	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);
	
	return true;
}

/** 
 * @brief 把第一队列里面的账号放到list
 * 
 * @param acc_info 账号信息，没有m_dwAccID
 */
void MODI_AccInfoManager::AddAccInfoToList(MODI_AccInfo * acc_info)
{
	m_AccInfoList.push_back(acc_info);
}

/** 
 * @brief 把list里面的信息写到数据库
 * 
 */
void MODI_AccInfoManager::WriteAccInfoToDB()
{
	if(m_AccInfoList.size() == 0)
		return;
	
	std::list<MODI_AccInfo * >::iterator iter, next;
	for(iter=m_AccInfoList.begin(), next=iter, next++; iter != m_AccInfoList.end(); iter=next, next++)
	{
		MODI_AccInfo * p_info = (*iter);

		bool ret_code = true;
		/// write to db
		MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
		if(! p_dbclient)
		{
			Global::logger->fatal("[sync_data] sync account not get dbclient");
			return;
		}

		MODI_Record record_insert;
		record_insert.Put("name", p_info->m_cstrAccName);
		record_insert.Put("cardnum", p_info->m_cstrCardNum);
		MODI_RecordContainer record_container;
		record_container.Put(&record_insert);
		if(p_dbclient->ExecInsert(MODI_InfoTask::m_pAccTbl, &record_container) != 1)
		{
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			Global::logger->fatal("[sync_data] insert to accountstbl failed <account=%s>", p_info->m_cstrAccName);
			ret_code = false;
			//continue;
		}
		/// get m_dwAccID;
		MODI_RecordContainer record_select;
		MODI_Record select_field;
		select_field.Put("account_id");
		std::ostringstream where;
		where << "name='" <<p_info->m_cstrAccName<< "'";

		if((p_dbclient->ExecSelect(record_select, MODI_InfoTask::m_pAccTbl, where.str().c_str(), &select_field)) != 1)
		{
			Global::logger->fatal("[get_accid] read acc form accountstbl faield <name=%s>", p_info->m_cstrAccName);
			ret_code = false;
		}

		MODI_Record * p_select_record = record_select.GetRecord(0);
		if(p_select_record != NULL)
		{
			MODI_VarType v_AccID = p_select_record->GetValue("account_id");
			if(! v_AccID.Empty())
			{
				p_info->m_dwAccID = (defAccountID)v_AccID;
			}
			else
			{
				ret_code = false;
			}
		}
		else
		{
			ret_code = false;
		}
		
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);

		/// 从list 删除p_info
		m_AccInfoList.remove(p_info);
		/// 加到已经好的队列
		if(ret_code)
		{
#ifdef _HRX_DEBUG
			Global::logger->debug("[sync_acc] add a info to okqueue <name=%s,accid=%u,cardnum=%s>", p_info->m_cstrAccName, p_info->m_dwAccID,
								  p_info->m_cstrCardNum);
#endif			
			AddAccToOkQueue(p_info);
		}
	}
}

/** 
 * @brief 增加一个账号
 *
 * @return 成功ture,失败false
 */
bool MODI_AccInfoManager::AddAccInfo(MODI_AccInfo *  acc_info)
{
	std::pair<defAccInfoMapIter, bool> ret_code;
	m_stMapLock.wrlock();
	ret_code = m_AccInfoMap.insert(defAccInfoMapValue(acc_info->m_cstrAccName, acc_info));
	m_stMapLock.unlock();
	
#ifdef _HRX_DEBUG	
	//Global::logger->debug("[add_info] add a info to map <name=%s>", acc_info->m_cstrAccName);
#endif
	
	return ret_code.second;
}

/** 
 * @brief 是不是激活账号
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_AccInfoManager::IsAccInfo(const char * acc_name)
{
	bool ret_code = false;
	m_stMapLock.rdlock();
	defAccInfoMapIter iter = m_AccInfoMap.find(acc_name);
	if(iter != m_AccInfoMap.end())
	{
		ret_code = true;
	}
	m_stMapLock.unlock();
	return ret_code;
}

/** 
 * @brief 获取账号信息 
 * 
 * @return 成功true,失败false
 *
 */
MODI_AccInfo *  MODI_AccInfoManager::GetAccInfo(const char * acc_name)
{
	MODI_AccInfo * p_info = NULL;
	m_stMapLock.rdlock();
	defAccInfoMapIter iter = m_AccInfoMap.find(acc_name);
	if(iter != m_AccInfoMap.end())
	{
		p_info = iter->second;
	}
	m_stMapLock.unlock();
	return p_info;
}


/** 
 * @brief 从队列里面获取账号信息到map里面
 * 
 */
void MODI_AccInfoManager::GetAccFromQueue()
{
	m_stQueueLock.wrlock();
	while(! m_AccInfoQueue.empty())
	{
		MODI_AccInfo * p_info = m_AccInfoQueue.front();
		AddAccInfoToList(p_info);
		m_AccInfoQueue.pop();
	}
	m_stQueueLock.unlock();
}


/** 
 * @brief 从队列里面获取账号信息到map里面
 * 
 */
void MODI_AccInfoManager::GetAccFromOkQueue()
{
	while(! m_AccInfoOkQueue.empty())
	{
		MODI_AccInfo * p_info = m_AccInfoOkQueue.front();
		AddAccInfo(p_info);
		m_AccInfoOkQueue.pop();
	}
}


/** 
 * @brief 增加一个账号信息到队列
 * 
 * @param p_info 要增加的账号
 */
void MODI_AccInfoManager::AddAccToQueue(MODI_AccInfo * p_info)
{
	m_stQueueLock.wrlock();
	m_AccInfoQueue.push(p_info);
	m_stQueueLock.unlock();
}


/** 
 * @brief 增加一个账号信息到队列
 * 
 * @param p_info 要增加的账号
 */
void MODI_AccInfoManager::AddAccToOkQueue(MODI_AccInfo * p_info)
{
	m_AccInfoOkQueue.push(p_info);
}


/** 
 * @brief 是否防沉迷
 * 
 * @param card_num 身份证号
 * 
 * @return 1沉迷
 */
bool MODI_AccInfoManager::IsChenMi(const char * card_num)
{
	return true;
}


/** 
 * @brief 更新
 * 
 */
void MODI_AccInfoManager::UpDate(const MODI_RTime & current_time)
{
	/// 10秒
	if(m_stUpDateTime(current_time))
	{		
		GetAccFromQueue();
		WriteAccInfoToDB();
		GetAccFromOkQueue();
	}
}
