/**
 * @file   InfoTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 14:42:31 2011
 * 
 * @brief  账号信息
 * 
 * 
 */

#include <iostream>
#include "md5.h"
#include "base64.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "FunctionTime.h"
#include "InfoTask.h"
#include "DBStruct.h"
#include "DBClient.h"
#include "ZoneTask.h"
#include "AccInfoManager.h"

MODI_TableStruct * MODI_InfoTask::m_pAccTbl = NULL;

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_InfoTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	return true;
}


/** 
 * @brief 发送命令
 * 
 * @param send_cmd 要发送的命令 
 * @param cmd_size 发送命令的大小不能超过64K
 * 
 * @return 成功true 失败false
 */
bool MODI_InfoTask::SendCmd(const char * send_cmd, const unsigned int cmd_size)
{
	if((! m_pSocket) || (cmd_size > RET_BUF_LENGTH))
	{
		Global::net_logger->fatal("[sock_data] send cmd to large");
		return false;
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[sync_acc] return a answer <ret=%s>", send_cmd);
#endif
	
	m_pSocket->SendCmdNoPacket(send_cmd, cmd_size);
	return true;
}


/**
 * @brief 接收到数据处理
 *
 * @return  继续等待true 成功关闭连接false
 *
 */
bool MODI_InfoTask::RecvDataNoPoll()
{
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}

	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > RECV_BUF_LENGTH)
		{
			Global::net_logger->fatal("[recv_data] recv command more than %u <ip=%s,port=%d>", RECV_BUF_LENGTH, GetIP(), GetPort());
			ResetRecvBuf();
			return false;
		}

		const std::string m_strRecvContent = m_stRecvBuf;
		std::string::size_type method_pos = m_strRecvContent.find_first_of(' ', 0);
		std::string method;
		if(method_pos == std::string::npos)
		{
			if(m_strRecvContent.size() > 5)
			{
				Global::logger->fatal("[recv_data] get method error <ip=%s,port=%d>", GetIP(), GetPort());
				ResetRecvBuf();
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			method = m_strRecvContent.substr(0, method_pos);
		}
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("[recv_data] recv a <command=%s>",m_strRecvContent.c_str());
#endif		
		
		if(method == "GET")
		{
			std::string::size_type end_pos = m_strRecvContent.find("\r\n", 0, 2);
			if(end_pos == std::string::npos)
			{
#ifdef _HRX_DEBUG				
				Global::net_logger->fatal("get command not get crlf");
#endif				
				return true;
			}
			
			std::string::size_type source_pos_end = m_strRecvContent.find_first_of(' ', method_pos+1);
			if(source_pos_end == std::string::npos)
			{
				Global::logger->fatal("not get source pos end");
				ResetRecvBuf();
				return false;
		}

			std::string get_source = m_strRecvContent.substr(method_pos+2, (source_pos_end - (method_pos+2)));
			
#ifdef _HRX_CUBE			
			Global::logger->debug("[recv_data] recv a command <content=%s,size=%d>", get_source.c_str(), get_source.size());
#endif

			/// 判断一下命令大小
// 			if(get_source.size() < 60)
// 			{
// 				ResetRecvBuf();
// 				return false;
// 			}

			if(! ParseSync(get_source.c_str()))
			{
				Global::logger->fatal("sync a account failed <%s>", m_strDecodeAccName.c_str());
			}
			else
			{
				Global::logger->debug("sync a account successful <%s>", m_strDecodeAccName.c_str());
			}
			
			ResetRecvBuf();
			return false;
		}
		
		else if(method == "POST")
		{
			return false;
		}
		else
		{
			Global::logger->fatal("method failed");
		}
		return false;
	}
	return true;
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_InfoTask::RecycleConn()
{
	return 1;
}



/** 
 * @brief 转换成md5
 * 
 * @param in_put 输入
 * 
 * @return 输出
 */
const char * MODI_InfoTask::ToMD5(const char * in_put)
{
	if(!in_put)
	{
		std::string ret_str;
		return ret_str.c_str();
	}
	
	std::string in_str = in_put;
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(in_str.c_str()) , in_str.size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	return recv_check_str.c_str();
}

/** 
 * @brief 同步处理
 * 
 * @param cmd_line 要处理的命令
 */
bool MODI_InfoTask::ParseSync(const char * cmd_line)
{
	std::string sync_cmd = cmd_line;
	std::string::size_type get_pos = sync_cmd.find_first_of('?', 0);
	if(get_pos == std::string::npos)
	{
		Global::logger->fatal("get ? failed");
		return false; 
	}
	
	std::string str_html = sync_cmd.substr(0, get_pos);
	if(str_html != "sync_account.jsp")
	{
		Global::logger->fatal("get raldenol_account.php html failed");
		return false;
	}

	std::string sync_content = sync_cmd.substr(get_pos+1, sync_cmd.size() - (get_pos+1));
	if( ! GetSyncContent(sync_content.c_str()))
	{
		RetSyncResult(-1);
		return false;
	}

	if(! CheckMD5())
	{
		RetSyncResult(-3);
		return false;
	}

	if(! DealSync())
	{
		return false;
	}

	return true;
	
}


/** 
 * @brief 分析同步账号
 * 
 * @param sync_content 同步账号内容
 */
bool MODI_InfoTask::GetSyncContent(const char * sync_content)
{
	MODI_SplitString deal_result;
	deal_result.Init(sync_content, "&");

	m_strEncodeAccName = deal_result["account"];
	m_strCardNum = deal_result["idcard"];
	m_strTime = deal_result["time_u"];
	m_strSN = deal_result["sn"];
	m_strThirdType = deal_result["otype"];
	m_strThirdUser = deal_result["oid"];
	
	if(m_strEncodeAccName == "" || 
	   m_strCardNum.size() > 18 ||
	   m_strTime == "" || m_strTime.size() > 30 ||
	   m_strSN.size() != 32)
   	{
	   	Global::logger->fatal("[sync_data] sync content error <ip=%s,port=%d>", GetIP(), GetPort());
	   	return false;
   	}

	unsigned char buf[m_strEncodeAccName.size() + 1];
	memset(buf, 0, sizeof(buf));
	
	base64_decode(buf, m_strEncodeAccName.c_str());
	m_strDecodeAccName = (const char *)buf;
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[sync_content] <account=%s,cardnum=%s,time=%s,sn=%s,thirdtype=%s,thirduser=%s>",
						  m_strDecodeAccName.c_str(), m_strCardNum.c_str(),
						  m_strTime.c_str(), m_strSN.c_str(),
						  m_strThirdType.c_str(), m_strThirdUser.c_str());
#endif
	
	return true;
}


/** 
 * @brief 处理同步账号
 * 
 * 
 * @return 成功true,失败false
 */
bool MODI_InfoTask::DealSync()
{
	if(MODI_AccInfoManager::GetInstance().IsAccInfo(m_strDecodeAccName.c_str()))
	{
#ifdef _HRX_DEBUG
		Global::logger->debug("[sync_data] acc have sync <account=%s> ", m_strDecodeAccName.c_str());
#endif
		/// 更新信息
		MODI_AccInfo * acc_info = MODI_AccInfoManager::GetInstance().GetAccInfo(m_strDecodeAccName.c_str());
		if(acc_info)
		{
			std::string old_card_num = acc_info->m_cstrCardNum;
			if(m_strCardNum == old_card_num)
			{
				RetSyncResult(-2);/// 先这样定义
				return false;
			}
			strncpy(acc_info->m_cstrCardNum, m_strCardNum.c_str(), sizeof(acc_info->m_cstrCardNum) - 1);
		}

		/// 更新到数据库
		MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
		if(! p_dbclient)
		{
			Global::logger->fatal("[sync_data] sync account not get dbclient");
			RetSyncResult(-4);
			return false;
		}

		std::ostringstream where;
		where << "name=\'" << m_strDecodeAccName << "\'";
		
		MODI_Record record_update;
		record_update.Put("cardnum", acc_info->m_cstrCardNum);
		record_update.Put("where", where.str());
		if(p_dbclient->ExecUpdate(MODI_InfoTask::m_pAccTbl, &record_update) != 1)
		{
			Global::logger->fatal("[pay_db] updata cardnum failed<name=%s,cardnum=%s>", m_strDecodeAccName.c_str(), acc_info->m_cstrCardNum);
			MODI_DBManager::GetInstance().PutHandle(p_dbclient);
			RetSyncResult(-2);/// 先这样定义
			return false;
		}
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
		RetSyncResult(-2);
		return false;
	}

	/// 没有6000/s的，直接写数据库

	MODI_AccInfo * p_info = new MODI_AccInfo();
	strncpy(p_info->m_cstrAccName, m_strDecodeAccName.c_str(), sizeof(p_info->m_cstrAccName) - 1);
	strncpy(p_info->m_cstrCardNum, m_strCardNum.c_str(), sizeof(p_info->m_cstrCardNum) - 1);
	//acc_info.m_stBaseInfo.m_dwAccID = account_id; 导入到map的时候读取acc_id
	/// 如果能确定为单线程也可以MAX(account_id)获取

	bool ret_code = true;
	/// write to db
	MODI_DBClient * p_dbclient = MODI_DBManager::GetInstance().GetHandle();
	if(! p_dbclient)
	{
		Global::logger->fatal("[sync_data] sync account not get dbclient");
		if(p_info)
			delete p_info;
		RetSyncResult(-4);
		return false;
	}

	MODI_Record record_insert;
	record_insert.Put("name", p_info->m_cstrAccName);
	record_insert.Put("cardnum", p_info->m_cstrCardNum);
	if(m_strThirdType != "")
	{
		record_insert.Put("third_type", m_strThirdType);
	}
	if(m_strThirdUser != "")
	{
		record_insert.Put("third_user", m_strThirdUser);
	}
	MODI_RecordContainer record_container;
	record_container.Put(&record_insert);
	if(p_dbclient->ExecInsert(MODI_InfoTask::m_pAccTbl, &record_container) != 1)
	{
		MODI_DBManager::GetInstance().PutHandle(p_dbclient);
		Global::logger->fatal("[sync_data] insert to accountstbl failed <account=%s>", p_info->m_cstrAccName);
		if(p_info)
			delete p_info;
		RetSyncResult(-2);
		return false;
	}
	/// get m_dwAccID;
	MODI_RecordContainer record_select;
	MODI_Record select_field;
	select_field.Put("account_id");
	std::ostringstream where;
	where << "name='" <<p_info->m_cstrAccName<< "'";

	if((p_dbclient->ExecSelect(record_select, MODI_InfoTask::m_pAccTbl, where.str().c_str(), &select_field)) != 1)
	{
		Global::logger->fatal("[get_accid] read acc form accountstbl faield <name=%s>", p_info->m_cstrAccName);
		ret_code = false;
	}

	MODI_Record * p_select_record = record_select.GetRecord(0);
	if(p_select_record != NULL)
	{
		MODI_VarType v_AccID = p_select_record->GetValue("account_id");
		if(! v_AccID.Empty())
		{
			p_info->m_dwAccID = (defAccountID)v_AccID;
		}
		else
		{
			ret_code = false;
		}
	}
	else
	{
		ret_code = false;
	}
	
	MODI_DBManager::GetInstance().PutHandle(p_dbclient);

	if(ret_code)
	{
		MODI_AccInfoManager::GetInstance().AddAccInfo(p_info);
		RetSyncResult(1); //success
	}
	else
	{
		Global::logger->fatal("[sync_acc_inof] sync info failed <%s>", p_info->m_cstrAccName);
		if(p_info)
			delete p_info;
		RetSyncResult(-4);
	}

	return true;
	
#if 0	/// 之前说6000/s并发，		
	MODI_AccInfoManager::GetInstance().AddAccToQueue(acc_info);
	RetSyncResult(1); //success
	return true;
#endif
	
}


/** 
 * @brief 返回同步结果
 * 
 */
void MODI_InfoTask::RetSyncResult(int ret_code)
{
	std::string ret_result;
	if(ret_code == 1)
	{
		ret_result = "ACTIVATION_SUCCESS";
	}
	else if(ret_code == -3)
	{
		ret_result = "ACTIVATION_FAIL_SIGNATURE_ERR";
	}
	else if(ret_code == -4)
	{
		ret_result = "ACTIVATION_FAIL_SYSTEM_ERR";
	}
	else if(ret_code == -1)
	{
		ret_result = "ACTIVATION_FAIL_PARAMETER_ERR";
	}
	else if(ret_code == -2)
	{
		ret_result = "ACTIVATION_FAIL_EXIST_ACCOUNT";
	}

	std::ostringstream os;
	os<< "HTTP/1.1 200 OK\r\n"
	 	<<"Content-Type: text/html; charset=utf-8\r\n\r\n"
	  << ret_code << "&&" << m_strEncodeAccName << "&&" << ret_result << "\r\n";
	
	SendCmd(os.str().c_str(), os.str().size());
}



/** 
 * @brief 签名校验
 * 
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_InfoTask::CheckMD5()
{
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;
	//sn=md5(base64_encode(account)+KEY+idcard+time_u)
	str_md5<< m_strEncodeAccName  << "qwerads" << m_strCardNum << m_strTime;
	
#ifdef _HRX_DEBUG	
	Global::logger->debug("md5_src_str=%s", str_md5.str().c_str());
#endif
	
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	
	if(recv_check_str == m_strSN)
	{
#ifdef _HRX_DEBUG		
		Global::logger->fatal("[check_md5] %s:%d md5 check successful(%s == %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
#endif		
		return true;
	}
	
	Global::logger->fatal("[check_md5] %s:%d md5 check failed(%s != %s)", GetIP(), GetPort(), recv_check_str.c_str(), m_strSN.c_str());
	return false;
}
