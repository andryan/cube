/**
 * @file   InfoTask.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jan 26 18:42:34 2011
 * 
 * @brief  同步账号信息
 * 
 * 
 */



#ifndef _MD_INFOTASK_H
#define _MD_INFOTASK_H

#include "ServiceTask.h"


/// 返回最大长度
const unsigned int RET_BUF_LENGTH = 128;
const unsigned int RECV_BUF_LENGTH = 256;

class MODI_TableStruct;


/**
 * @brief 激活一个账号连接
 * 
 */
class MODI_InfoTask: public MODI_ServiceTask
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param sock 相关的sock
	 * @param addr 相关的地址
	 * 
	 */
	MODI_InfoTask(const int sock, const struct sockaddr_in * addr): MODI_ServiceTask(sock, addr)
	{
		SetInBuf();
		m_blCheckTimeOut = true;
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}

	MODI_InfoTask(MODI_TaskQueue * p_queue): MODI_ServiceTask(p_queue)
	{
		SetInBuf();
		m_blCheckTimeOut = true;
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}

	/** 
	 * @brief 发送命令
	 * 
	 * @param send_cmd 要发送的命令 
	 * @param cmd_size 发送命令的大小不能超过64K
	 * 
	 * @return 成功true 失败false
	 */
	bool SendCmd(const char * send_cmd, const unsigned int cmd_size);
	
	/// 接收命令处理		
	bool RecvDataNoPoll();

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true 失败false
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);


	/** 
	 * @brief 是否可以回收
	 * 
	 * 
	 * @return 1可以回收 0继续等待
	 */
	int RecycleConn();

	/// 各个表指针
	static MODI_TableStruct * m_pAccTbl;

	/** 
	 * @brief 转换成md5
	 * 
	 * @param in_put 输入
	 * 
	 * @return 输出
	 */
	static const char * ToMD5(const char * in_put);

	void Init(const int s_sock, const struct sockaddr_in * s_addr, MODI_TaskQueue * task_queue)
	{
		MODI_ServiceTask::Init(s_sock, s_addr, task_queue);
	}

	inline void Reset()
	{
		m_blCheckTimeOut = true;
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
		MODI_ServiceTask::Reset();
	}
	
 private:
	/// 处理同步账号
	bool ParseSync(const char *line_cmd);
	bool GetSyncContent(const char * pay_content);
	bool DealSync();
	inline void RetSyncResult(int ret_code);
	inline bool CheckMD5();

	/** 
	 * @brief 清空接收缓冲
	 * 
	 */
	inline void ResetRecvBuf()
	{
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}

	/// 接收缓冲
	char m_stRecvBuf[RECV_BUF_LENGTH];

	/// 接收的个数
	unsigned int m_dwRecvCount;

	/// 账号
	std::string m_strDecodeAccName;
	std::string m_strEncodeAccName;
	
	/// 身份证
	std::string m_strCardNum;
	/// 同步时间
	std::string m_strTime;
	/// 签名
	std::string m_strSN;
	/// 第三方标识
	std::string m_strThirdType;
	/// 第三方用户id标识
	std::string m_strThirdUser;
};

#endif
