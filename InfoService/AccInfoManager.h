/**
 * @file   AccInfoManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:38:42 2011
 * 
 * @brief  账号信息管理
 * 
 * 
 */


#ifndef _MD_ACCINFOMANAGER_H
#define _MD_ACCINFOMANAGER_H

#include "Global.h"
#include "Thread.h"
#include "s2info_cmd.h"

/**
 * @brief 账号信息数据
 * 
 */
struct MODI_AccInfo
{
	MODI_AccInfo()
	{
		memset(m_cstrCardNum, 0, sizeof(m_cstrCardNum));
		m_dwAccID = INVAILD_ACCOUNT_ID;
		memset(m_cstrAccName, 0, sizeof(m_cstrAccName));
	}
	
	/// 账号id
	defAccountID m_dwAccID;
	/// 账号名
	char m_cstrAccName[MAX_ACCOUNT_LEN + 1];
		
	/// 账号身份证
	char m_cstrCardNum[MAX_CARDNUM_LENGTH];
	
}__attribute__((packed));


/**
 * @brief 账号信息管理
 * 
 */
class MODI_AccInfoManager
{
 public:
	typedef __gnu_cxx::hash_map<std::string, MODI_AccInfo * , str_hash> defAccInfoMap;
	typedef defAccInfoMap::iterator defAccInfoMapIter;
	typedef defAccInfoMap::value_type defAccInfoMapValue;

	MODI_AccInfoManager(): m_stUpDateTime(1000)
	{
		
	}

	~MODI_AccInfoManager()
	{
		
	}
	
	static MODI_AccInfoManager & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_AccInfoManager;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
			delete m_pInstance;
		m_pInstance = NULL;
	}

	bool Init();

	/** 
 	 * @brief 从队列里面获取账号信息到list里面
 	 * 
 	 */
	void GetAccFromQueue();

	/** 
 	 * @brief 从队列里面获取账号信息到map里面
 	 * 
 	 */
	void GetAccFromOkQueue();

	/** 
 	 * @brief 增加一个账号信息到队列
 	 * 
 	 * @param p_info 要增加的账号
 	 */
 	void AddAccToQueue(MODI_AccInfo * p_info);

	/** 
 	 * @brief 增加一个账号信息到队列
 	 * 
 	 * @param p_info 要增加的账号
 	 */
 	void AddAccToOkQueue(MODI_AccInfo * p_info);
	
	/** 
	 * @brief 是不是激活的账号
	 * 
	 * @param acc_name 账号名
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool IsAccInfo(const char * acc_name);

	/** 
	 * @brief 获取一个账号信息
	 * 
	 * @param acc_name 账号名
	 * 
	 * @return 成功true,失败false
	 */
	MODI_AccInfo * GetAccInfo(const char * acc_name);

	/** 
	 * @brief 是否防沉迷
	 * 
	 * @param card_num 身份证号
	 * 
	 * @return 1沉迷
	 */
	bool IsChenMi(const char * card_num);

	/// 更新
	void UpDate(const MODI_RTime & current_time);

	bool AddAccInfo(MODI_AccInfo * acc_info);
 private:
	/** 
	 * @brief 增加一个激活账号
	 * 
	 * @return 成功ture,失败false
	 */
	void AddAccInfoToList(MODI_AccInfo * acc_info);
	void WriteAccInfoToDB();
		
	
		
	static MODI_AccInfoManager * m_pInstance;
	
	/// 账号信息缓冲队列
	std::queue<MODI_AccInfo * , std::deque<MODI_AccInfo * > > m_AccInfoQueue;

	/// 账号信息缓冲链表
	std::list<MODI_AccInfo * > m_AccInfoList;

	/// 账号信息缓冲队列
	std::queue<MODI_AccInfo * , std::deque<MODI_AccInfo * > > m_AccInfoOkQueue;
	
	/// 账号信息map
	defAccInfoMap m_AccInfoMap;

	MODI_TTime m_stTTime;
	MODI_Timer m_stUpDateTime;
	
	MODI_RWLock m_stQueueLock;
	MODI_RWLock m_stMapLock;
};

#endif
