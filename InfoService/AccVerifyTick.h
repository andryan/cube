/**
 * @file   AccVerifyTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Dec  6 15:10:39 2010
 * 
 * @brief  专门处理登陆
 * 
 */

#ifndef _MDACCVERIFYTICK_H
#define _MDACCVERIFYTICK_H

#include "Timer.h"
#include "Thread.h"

class MODI_AccVerifyTick: public MODI_Thread 
{
public:
    MODI_AccVerifyTick();
	
    virtual ~MODI_AccVerifyTick();

    void Final();

    virtual void Run();

	static MODI_AccVerifyTick & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new MODI_AccVerifyTick;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}
	
private:
	
	static MODI_AccVerifyTick * m_pInstance;
	MODI_RTime m_stRTime;
	MODI_Timer m_stCmdFluxTime;
};

#endif
