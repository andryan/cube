/**
 * @file   InfoService.h
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jan 26 18:19:06 2011
 * 
 * @brief  信息服务器
 * 
 * 
 */



#ifndef _MD_INFOSERVICE_H
#define _MD_INFOSERVICE_H

#include "ServiceTaskSched.h"
#include "MNetService.h"
#include "InfoTask.h"


const int MULTI_PORT_NUM = 2;

/// 端口索引下标
enum enPortIndex
{
	enInfoPort,
	enZonePort,
	enMaxPort
};

/**
 * @brief 信息服务器类，多监听
 * 
 */
class MODI_InfoService: public MODI_MNetService
{
 public:
	/** 
	 * @brief 构造
	 * 
	 * @param vec 要绑定的端口
	 * @param name 服务器
	 * @param count 绑定的端口个数
	 * 
	 */
	MODI_InfoService(std::vector<WORD> & vec, const std::vector<std::string> & vecip , 
			const char * name = "infoservice", const int count = MULTI_PORT_NUM):MODI_MNetService(vec, vecip, name, count),
		m_stInfoTaskSched(8,1), m_stZoneTaskSched(2,1)//m_stInfoTaskSched must (n,1)使用了任务池
	{
		m_pInstance = this;
		m_wdInfoPort = 0;
		m_wdZonePort = 0;
		
		if(((int)vec.size() == count) && (count == MULTI_PORT_NUM))
		{
			m_wdInfoPort = vec[enInfoPort];
			m_wdZonePort = vec[enZonePort];
		}
	}

	/** 
	 * @brief 创建新任务
	 * 
	 * @param sock 任务相关的task
	 * @param addr 任务相关的地址
	 * @param port 任务相关的端口
	 * 
	 * @return 成功true 失败false
	 */		
	bool CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port);

	/** 
	 * @brief 初始化
	 *
	 * @return 成功true 失败false
	 */
	bool Init();

	/** 
	 * @brief 释放资源
	 * 
	 */
	void Final();

	static MODI_InfoService * GetInstancePtr()
	{
		return m_pInstance;
	}

	/** 
	 * @brief ip限制
	 * 
	 */
	bool CheckTaskIP(const char * ip, WORD & zone_id);


	/** 
	 * @brief 重新加载配置文件
	 * 
	 */
   	void ReloadConfig();

 private:
	/// 充值客户端
	WORD m_wdInfoPort;
	
	/// 游戏区端口
	WORD m_wdZonePort;

	/// 任务处理线程
	MODI_ServiceTaskSched m_stInfoTaskSched;

	MODI_ServiceTaskSched m_stZoneTaskSched;

	static MODI_TaskQueue * m_pInfoTaskQueue;
	
	/// 实体
	static MODI_InfoService * m_pInstance;
};

#endif

