/**
 * @file   AccVerifyClient.h
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Mon Oct 11 10:44:22 2010
 * @version $Id:$
 * @brief  校验连接
 * 
 */

#ifndef _MD_ACCVERIFYCLIENT_H
#define _MD_ACCVERIFYCLIENT_H

#include "PClientTask.h"
#include "CommandQueue.h"
#include "s2info_cmd.h"


/// 校验返回命令
const BYTE ACCOUNT_VERIFY_CMD = 1;
const BYTE ACCOUNT_VERIFY_PARA = 1;
struct MODI_RetVerifyResult: public Cmd::stNullCmd
{
	MODI_RetVerifyResult()
	{
		byCmd = ACCOUNT_VERIFY_CMD;
		byParam = ACCOUNT_VERIFY_PARA;
		m_wdRetCode = 1000;
		m_dwAccountID = 0;
	}
	WORD m_wdRetCode;
	DWORD m_dwAccountID;
}__attribute__((packed));


/**
 * @brief 登录校验连接
 * 
 */
class MODI_AccVerifyClient: public MODI_PClientTask, public MODI_CmdParse
{
 public:
	
	MODI_AccVerifyClient(MODI_S2Info_Verfiy_Account * para):
		MODI_PClientTask(Global::g_strPlatformIP.c_str(), Global::g_wdPlatformPort)
		//MODI_PClientTask("192.168.2.208", 80)
	{
		ResetRecvBuf();
		m_stInPara = *para;
	}

	bool CmdParse(const Cmd::stNullCmd*, int)
	{
		return true;
	}

	bool RecvDataNoPoll();
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size);

	bool SendCmd(const char * cmd, const unsigned int cmd_size)
	{
		if(m_pSocket)
		{
			m_pSocket->SendCmdNoPacket(cmd, cmd_size,false);
			return true;
		}
		return false;
	}

	bool Init();

	void SendVerifyCmd();

	int RecycleConn()
	{
		if(IsTerminateFinal())
			return 1;
		return 0;
	}

   	void AddTaskToManager();
	void RemoveTaskFromManager();
	void TimeOut();
	void VerifyFailed();
	
 private:
	void ResetRecvBuf()
	{
		memset(m_stRecvBuf, 0, sizeof(m_stRecvBuf));
		m_dwRecvCount = 0;
	}
	
	/// 接收缓冲
	char m_stRecvBuf[1024];

	/// 接收的个数
	unsigned int m_dwRecvCount;

	/// 需要校验的参数
	MODI_S2Info_Verfiy_Account m_stInPara;
};

#endif
