/**
 * @file   InfoServerAPP.h
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 10:00:40 2011
 * 
 * @brief  信息服务器
 * 
 * 
 */


#ifndef _MD_INFOSERVERAPP_H
#define _MD_INFOSERVERAPP_H

#include "IAppFrame.h"
#include "InfoService.h"

/**
 * @brief 信息服务器框架
 * 
 */
class MODI_InfoServerAPP: public MODI_IAppFrame
{
 public:
	explicit MODI_InfoServerAPP( const char * szAppName );
	virtual ~MODI_InfoServerAPP();
	virtual int Init();
	virtual int Run();
	virtual int Shutdown();
};

#endif
