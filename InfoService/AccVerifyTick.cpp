/**
 * @file   AccVerifyTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Mon Dec  6 15:12:48 2010
 * 
 * @brief  专门处理登陆
 * 
 */


#include "AccVerifyTick.h"
#include "Global.h"
#include "AccVerifySched.h"

MODI_AccVerifyTick * MODI_AccVerifyTick::m_pInstance = NULL;

MODI_AccVerifyTick::MODI_AccVerifyTick() : MODI_Thread("accverifytick"), m_stCmdFluxTime(24 * 3600 * 1000)
{
	
}

MODI_AccVerifyTick::~MODI_AccVerifyTick()
{
	
}


/**
 * @brief 主循环
 *
 */
void MODI_AccVerifyTick::Run()
{
    while(!IsTTerminate())
    {
		m_stRTime.GetNow();
    	::usleep(10000);
		MODI_AccVerifySched::GetInstance().Get(1000);
    }
    Final();
}

/**
 * @brief 释放资源
 *
 */
void MODI_AccVerifyTick::Final()
{
	
}
