/**
 * @file   AccVerifyClient.cpp
 * @author hurixin <hrx@hrxOnLinux.modi.com>
 * @date   Mon Oct 11 11:01:29 2010
 * @version $Id:$
 * @brief  校验连接
 * 
 */

#include "Global.h"
#include "HelpFuns.h"
#include "SplitString.h"
#include "AccVerifyClient.h"
#include "AccVerifySched.h"
#include "md5.h"
#include "XMLParser.h"
#include "ywencrypt.h"
#include "ZoneTaskManager.h"
#include "AccInfoManager.h"


/** 
 * @brief 初始化
 *
 * @return 成功true,失败false
 *
 */
bool MODI_AccVerifyClient::Init()
{
	if(! MODI_PClientTask::Init())
	{
		return false;
	}
	return true;
}


/** 
 * @brief 发送校验命令
 * 
 */
void MODI_AccVerifyClient::SendVerifyCmd()
{
	std::ostringstream send_cmd;
	
// 	/// LUNTAN
//#ifdef _USE_LUNTAN
	
#if 0
	/// gen md5
	unsigned char md5_out[17];
	memset(md5_out, 0 , sizeof(md5_out));
	char final_out[33];
	memset(final_out, 0, sizeof(final_out));
	
	std::ostringstream str_md5;
	str_md5<< m_stInPara.m_szAccountName << m_stInPara.m_szAccountPwd<< "123456";
	MD5Context check_str;
	MD5Init( &check_str );
	MD5Update( &check_str , (const unsigned char*)(str_md5.str().c_str()) , str_md5.str().size());
	MD5Final(md5_out , &check_str );
	Binary2String((const char *)md5_out, 16, final_out);

	/// gen time
	MODI_TTime st_time;
	char login_time[15];
	memset(login_time, 0, sizeof(login_time));
 	sprintf(login_time, "%04u%02u%02u%02u%02u%02u", (DWORD)st_time.GetYear(), (DWORD)st_time.GetMon(),
			(DWORD)st_time.GetMDay(), (DWORD)st_time.GetHour(), (DWORD)st_time.GetMin(), (DWORD)st_time.GetSec());
//http://www.ijiayou.com/login.jsp?accountname=账号名&password=密码&randx=&randy=&value=aa&time=yyyymmddhhmmss&checkstr=UPPER(md5(accountname+pwd))
	
	send_cmd.str("");
	send_cmd << "GET /login.jsp?accountname=" << m_stInPara.m_szAccountName << "&password=" << m_stInPara.m_szAccountPwd
													 << "&randx=&randy=&invalue=&time="
													 << login_time <<"&checkstr=" << final_out << " HTTP/1.1\r\n";
#endif
	
	
	
#if 0
	<<"POST /gm/info.php HTTP/1.1\r\n"
			// <<"Accept-Language: zh-cn\r\n"
			<<"Content-Type: application/x-www-form-urlencoded\r\n"
			//<<"UA-CPU: x86\r\n"
			// <<"Accept-Encoding: gzip, deflate\r\n"
			// <<"User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; MAXTHON 2.0)\r\n"
				 <<"Host: 192.168.2.208:80\r\n"
			<<"Content-Length: 58\r\n"
				 <<"Connection: Keep-Alive\r\n\r\n"
			// <<"Cache-Control: no-cache\r\n\r\n"
				 <<"username=hhh&password=&cpassword=&email=&sex=male&code=123";
#endif

	//// 用户名和密码记得加密
	char buf[100];
	memset(buf, 0, sizeof(buf));
	Encrypt(m_stInPara.m_szAccountName,buf);
	/// 加密后urlencode
	Global::logger->debug("-->>>encode name=%s", buf);
	std::string en_name = PublicFun::StrEncode(buf);
	Global::logger->debug("-->>>urlencode name=%s", en_name.c_str());
	memset(buf, 0, sizeof(buf));
	Encrypt(m_stInPara.m_szAccountPwd,buf);
	Global::logger->debug("-->>>encode pwd=%s", buf);
	std::string en_pwd = PublicFun::StrEncode(buf);
	Global::logger->debug("-->>>urlencode pwd=%s", en_pwd.c_str());
	

	send_cmd.str("");
	std::ostringstream send_content;
	send_content<<"client=true&" << "username=" <<en_name << "&password=" << en_pwd <<"&gameType=JCT";
	
	send_cmd << "POST /login HTTP/1.1\r\n"
			 <<"Content-Type: application/x-www-form-urlencoded\r\n"
			 <<"Host: " << "passport.9igame.com" << ":" << "80" <<"\r\n"
			 <<"Content-Length: "<< send_content.str().size() <<"\r\n"
			 <<"Connection: Keep-Alive\r\n\r\n"
			 <<send_content.str();

	// send_cmd << "GET /gm/info.php"
// 			 << " HTTP/1.1\r\n"
// 			<<"Host: " << "192.168.2.208" << ":" << "80" <<"\r\nConnection: Keep-Alive\r\n\r\n";
	
	
#ifdef _HRX_DEBUG
	Global::logger->debug("-->>send a command =%s", send_cmd.str().c_str());
#endif
	
	SendCmd(send_cmd.str().c_str(), send_cmd.str().size());
}


/**
 * @brief 校验
 * 
 */
bool MODI_AccVerifyClient::RecvDataNoPoll()
{
	/// LUNTAN
// #ifdef _USE_LUNTAN
// 	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
// 	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
// 	{
// 		return true;
// 	}
	
// 	if (retcode > 0)
// 	{
// 		m_dwRecvCount += retcode;
// 		if(m_dwRecvCount > 1000)
// 		{
// 			Global::logger->fatal("recv command more than 1000");
// 			ResetRecvBuf();
// 			return false;
// 		}
// 		const std::string m_strRecvContent = m_stRecvBuf;
		
// #ifdef _HRX_DEBUG		
// 		Global::logger->debug(m_strRecvContent.c_str());
// #endif
		
// 		std::string::size_type end_pos = m_strRecvContent.find_first_of("\r\n", 0);
// 		if(end_pos == std::string::npos)
// 		{
// 			Global::logger->debug("not get crlf");
// 			return true;
// 		}
// 		std::string str_answer;
// 		str_answer= m_strRecvContent.substr(0, end_pos);

// 		MODI_SplitString deal_result;
// 		deal_result.Init(str_answer.c_str(), "&");
// 		std::string str_retcode = deal_result["retcode"];
// 		std::string str_accountid = deal_result["accid"];
		
// #ifdef _HRX_DEBUG		
// 		Global::logger->debug("answer = %s, ret_code=%s, accountid=%s", str_answer.c_str(), str_retcode.c_str(), str_accountid.c_str());
// #endif
		
// 		MODI_RetVerifyResult send_cmd;
// 		send_cmd.m_wdRetCode = atoi(str_retcode.c_str());
// 		send_cmd.m_dwAccountID = atoi(str_accountid.c_str());
// 		Put(&send_cmd, sizeof(send_cmd));
// 		ResetRecvBuf();
// 		return false;
// 	}
// 	return true;
	
	/// use other check login account
	int retcode = TEMP_FAILURE_RETRY(::recv(m_pSocket->GetSock(),(char * )&m_stRecvBuf[m_dwRecvCount], sizeof(m_stRecvBuf) - m_dwRecvCount, MSG_NOSIGNAL));
	if (retcode == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
	{
		return true;
	}
	
	if (retcode > 0)
	{
		m_dwRecvCount += retcode;
		if(m_dwRecvCount > 1000)
		{
			Global::logger->fatal("recv command more than 1000");
			ResetRecvBuf();
			return false;
		}
		
		const std::string m_strRecvContent = m_stRecvBuf;
		
#ifdef _HRX_DEBUG		
		Global::logger->debug(m_strRecvContent.c_str());
#endif
// 		/// 临时测试
// 		MODI_RetVerifyResult send_cmd1;
// 		if(m_strRecvContent == "40000")
// 		{
// 			send_cmd1.m_wdRetCode = enVerifySuccess;
// 		}
// 		Put(&send_cmd1, sizeof(send_cmd1));
// 		ResetRecvBuf();
// 		return false;

		std::string::size_type end_pos = m_strRecvContent.find_first_of("\r\n", 0);
		if(end_pos == std::string::npos)
		{
			Global::logger->debug("not get crlf");
			return true;
		}
		std::string str_answer;
		str_answer= m_strRecvContent.substr(0, end_pos);
		std::string::size_type answer_end_pos = str_answer.find_last_of(' ', str_answer.size());
		if(answer_end_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get answer");
#endif
			VerifyFailed();
			return false;
		}

		std::string ret_result = str_answer.substr(answer_end_pos+1, (str_answer.size() - answer_end_pos + 1));
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("ret_result=%s.", ret_result.c_str());
#endif
		if(ret_result != "OK")
		{
#ifndef _HRX_DEBUG			
			Global::logger->debug("answer not ok");
#endif
			VerifyFailed();
			return false;
		}

		std::string::size_type begin_content_pos = m_strRecvContent.find("\r\n\r\n", 0);
		if(begin_content_pos == std::string::npos)
		{
#ifdef _HRX_DEBUG			
			Global::logger->debug("not get begin crlfcrlf");
#endif
			VerifyFailed();
			return false;
		}

		if(m_strRecvContent.size() < (begin_content_pos + 6))
		{
			VerifyFailed();
			return false;
		}
		std::string final_str_result = m_strRecvContent.substr(begin_content_pos+4, (m_strRecvContent.size() - (begin_content_pos+4)));
#ifdef _HRX_DEBUG
		Global::logger->debug("final_str_result=%s.", final_str_result.c_str());
#endif

		if(final_str_result.size() < 5)
		{
			VerifyFailed();
			return false;
		}
		
#ifdef _HRX_DEBUG		
		Global::logger->debug("result=%s", final_str_result.c_str());
#endif


		MODI_RetVerifyResult send_cmd;
		std::string get_real_answer;
		std::string::size_type end_pos_s = final_str_result.find("\r\n\r\n", 0, 4);
		if(end_pos_s != std::string::npos)
		{
			std::string get_answer = final_str_result.substr(0, end_pos_s);
			Global::logger->debug("-->>>>get_answer=%s", get_answer.c_str());
			std::string::size_type end_pos1 = get_answer.find("\r\n", 0, 2);
			if(end_pos1 != std::string::npos)
			{
				std::string get_last_answer = get_answer.substr(end_pos1+2,get_answer.size()-(end_pos1+2));
				Global::logger->debug("-->>>>get_last_answer=%s", get_last_answer.c_str());
				std::string::size_type end_pos2 = get_last_answer.find("\r\n", 0, 2);
				if(end_pos2 != std::string::npos)
				{
					get_real_answer = get_last_answer.substr(0,end_pos2);
				}
			}
		}
		
#ifdef _HRX_DEBUG
		Global::logger->debug("-->>>>real answer = <%s>", get_real_answer.c_str());
#endif
		
		//if(get_real_answer == "40000")
		if(final_str_result == "40000")
		{
			send_cmd.m_wdRetCode = enVerifySuccess;
		}
		
		Put(&send_cmd, sizeof(send_cmd));
		ResetRecvBuf();
		return false;
	}
	return true;
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 
 * @param cmd_size 
 * 
 * @return 
 */
bool MODI_AccVerifyClient::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if(!pt_null_cmd)
		return false;
	
	if(pt_null_cmd->byCmd == ACCOUNT_VERIFY_CMD && pt_null_cmd->byParam == ACCOUNT_VERIFY_PARA)
	{
		MODI_RetVerifyResult * recv_cmd = (MODI_RetVerifyResult *)pt_null_cmd;
		/// 直接返回给dbservice
		MODI_Info2S_Verify_Result send_cmd;
		send_cmd.m_SessionID = m_stInPara.m_SessionID;
		if(recv_cmd->m_wdRetCode == enVerifySuccess)
		{
			send_cmd.m_enResult = enVerifySuccess;
			const MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(m_stInPara.m_szAccountName);
			if(p_info)
			{
				/// 更改了身份证不能立刻去除防沉迷
				strncpy(send_cmd.m_cstrCardNum, p_info->m_cstrCardNum, sizeof(send_cmd.m_cstrCardNum)-1);
				send_cmd.m_dwAccID = p_info->m_dwAccID;
			}
			else
			{
				send_cmd.m_enResult = enVerifyPwdErr;
				Global::logger->fatal("[account_not_sync] why not sync accout <name=%s>", m_stInPara.m_szAccountName);
				MODI_ASSERT(0);
			}
		}
		else
		{
			send_cmd.m_enResult = enVerifyPwdErr;
		}
		MODI_ZoneTask * p_task = MODI_ZoneTaskManager::GetInstance().GetZoneTask(m_stInPara.m_wdZoneID);
		if(p_task)
		{
			p_task->SendCmd(&send_cmd, sizeof(send_cmd));
#ifdef _HRX_DEBUG
			Global::logger->debug("[acc_verify] account verify satate <result=%d,accname=%s,accid=%u,chenmi=%s>",
								  send_cmd.m_enResult, m_stInPara.m_szAccountName, send_cmd.m_dwAccID, send_cmd.m_cstrCardNum);
#endif			
		}
		TerminateFinal();
		return true;
	}
	
	return false;
}

/** 
 * @brief 添加到管理中
 * 
 */
void MODI_AccVerifyClient::AddTaskToManager()
{
	MODI_AccVerifySched::GetInstance().AddVerifyClient(this);
}

/** 
 * @brief 从管理中删除
 * 
 */
void MODI_AccVerifyClient::RemoveTaskFromManager()
{
	MODI_AccVerifySched::GetInstance().RemoveVerifyClient(this);
}

/** 
 * @brief 超时
 * 
 */
void MODI_AccVerifyClient::TimeOut()
{
	Global::logger->fatal("%s check timeout", m_stInPara.m_szAccountName);
	MODI_RetVerifyResult send_cmd;
	send_cmd.m_wdRetCode = enVerifyTimeout;
	send_cmd.m_dwAccountID = INVAILD_ACCOUNT_ID;
	Put(&send_cmd, sizeof(send_cmd));
	ResetRecvBuf();
	//	TerminateFinal();
}


void MODI_AccVerifyClient::VerifyFailed()
{
	MODI_RetVerifyResult send_cmd;
	send_cmd.m_wdRetCode = enVerifyFailed;
	send_cmd.m_dwAccountID = 0;
	Put(&send_cmd, sizeof(send_cmd));
	ResetRecvBuf();	
}
