/**
 * @file   ZoneTaskManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Jan 28 10:20:40 2011
 * 
 * @brief  区连接管理
 * 
 */


#include "ZoneTaskManager.h"

MODI_ZoneTaskManager * MODI_ZoneTaskManager::m_pInstance = NULL;

void MODI_ZoneTaskManager::AddTask(MODI_ZoneTask * p_task)
{
	if(p_task)
	{
		m_stRWLock.wrlock();
		m_stTaskMap.insert(defZoneTaskMapValue(p_task->GetZoneID(), p_task));
		m_stRWLock.unlock();
	}
}

void MODI_ZoneTaskManager::RemoveTask(MODI_ZoneTask * p_task)
{
	if(p_task)
	{
		m_stRWLock.wrlock();
		defZoneTaskMapIter iter = m_stTaskMap.find(p_task->GetZoneID());
		if(iter != m_stTaskMap.end())
		{
			m_stTaskMap.erase(iter);
		}
		m_stRWLock.unlock();
	}
}


MODI_ZoneTask * MODI_ZoneTaskManager::GetZoneTask(const WORD & zone_id)
{
	m_stRWLock.wrlock();
	defZoneTaskMapIter iter = m_stTaskMap.find(zone_id);
	if(iter != m_stTaskMap.end())
	{
		m_stRWLock.unlock();
		return iter->second;
	}
	m_stRWLock.unlock();
	return NULL;
}
