/**
 * @file   ZoneTask.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 18:35:41 2011
 * 
 * @brief  和游戏区的链接
 * 
 * 
 */

#include "ZoneTask.h"
#include "DBClient.h"
#include "InfoTask.h"
#include "s2info_cmd.h"
#include "AccVerifySched.h"
#include "AccInfoManager.h"
#include "ZoneTaskManager.h"
#include "md5.h"
#include "HelpFuns.h"

/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true 失败false
 */
bool MODI_ZoneTask::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}

	Put(pt_null_cmd, cmd_size);
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif

	return true;
}


bool MODI_ZoneTask::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if(pt_null_cmd == NULL || cmd_size < 2)
	{
		return false;
	}

	/// 登录效验
	if(pt_null_cmd->byCmd == MAINCMD_S2INFO && pt_null_cmd->byParam == MODI_S2Info_Verfiy_Account::ms_Param)
	{
		MODI_S2Info_Verfiy_Account * recv_cmd = (MODI_S2Info_Verfiy_Account *)pt_null_cmd;
		//// 先判断是否激活
		if(! MODI_AccInfoManager::GetInstance().IsAccInfo(recv_cmd->m_szAccountName))
		{
			//// 告知未激活
			MODI_Info2S_Verify_Result send_cmd;
			send_cmd.m_enResult = enVerifyNoActive;
			send_cmd.m_SessionID = recv_cmd->m_SessionID;
			SendCmd(&send_cmd, sizeof(send_cmd));
			
#ifdef _HRX_DEBUG
			Global::logger->debug("[acc_verify] acc verify not active <name=%s,pwd=%s>", recv_cmd->m_szAccountName, recv_cmd->m_szAccountPwd);
#endif			
			return true;
		}
		
		/// 看是否是yy的用户
		std::string user_name = recv_cmd->m_szAccountName;
		if(user_name >= "0" && user_name <= "a")
		{
			Global::logger->debug("[yy_login] <accname=%s>", user_name.c_str());
			std::string user_passwd = "123456" + user_name;
			std::string check_passwd = MakeYYMd5(user_passwd);
			std::string recv_passwd = recv_cmd->m_szAccountPwd;
				

			MODI_Info2S_Verify_Result send_cmd;
			send_cmd.m_enResult = enVerifySuccess;
			send_cmd.m_SessionID = recv_cmd->m_SessionID;

			//Global::logger->debug("<%s><%s>", check_passwd.c_str(), recv_passwd.c_str());
			if(check_passwd != recv_passwd)
			{
				send_cmd.m_enResult = enVerifyPwdErr;
				SendCmd(&send_cmd, sizeof(send_cmd));
				Global::logger->error("[check_yy_password] yyuser password error <accname=%s>", user_name.c_str());
				return true;
			}
			
			const MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(recv_cmd->m_szAccountName);
			if(p_info)
			{
				strncpy(send_cmd.m_cstrCardNum, p_info->m_cstrCardNum, sizeof(send_cmd.m_cstrCardNum)-1);
				send_cmd.m_dwAccID = p_info->m_dwAccID;
			}
			else
			{
				send_cmd.m_enResult = enVerifyPwdErr;
			}
			
			SendCmd(&send_cmd, sizeof(send_cmd));
			return true;
		}
		else
		{
			recv_cmd->m_wdZoneID = GetZoneID();
			MODI_AccVerifySched::GetInstance().ReqVerifyAcc(recv_cmd);
			return true;
		}
#if 0
		/// 下面是测试代码
		MODI_Info2S_Verify_Result send_cmd;
		send_cmd.m_SessionID = recv_cmd->m_SessionID;
		send_cmd.m_enResult = enVerifySuccess;
		const MODI_AccInfo * p_info = MODI_AccInfoManager::GetInstance().GetAccInfo(recv_cmd->m_szAccountName);
		if(p_info)
		{
			/// 下次改成直接发身份证给db
			//send_cmd.m_blChenMi = MODI_AccInfoManager::GetInstance().IsChenMi(p_info->m_cstrCardNum);
			strncpy(send_cmd.m_cstrCardNum, p_info->m_cstrCardNum, sizeof(send_cmd.m_cstrCardNum)-1);
			send_cmd.m_dwAccID = p_info->m_dwAccID;
		}

		SendCmd(&send_cmd, sizeof(send_cmd));
		return true;
#endif		
	}
	
#ifdef _HRX_DEBUG
	Global::logger->debug("[recv_cmd] deal zonetask recv a command(%d->%d, size=%d)", pt_null_cmd->byCmd, pt_null_cmd->byParam, cmd_size);
#endif
	
	return true;
}


/** 
 * @brief 是否可以回收
 * 
 * 
 * @return 1可以回收 0继续等待
 */
int MODI_ZoneTask::RecycleConn()
{
	return 1;
}


void MODI_ZoneTask::AddTaskToManager()
{
	MODI_ZoneTaskManager::GetInstance().AddTask(this);
}


void MODI_ZoneTask::RemoveFromManager()
{
	MODI_ZoneTaskManager::GetInstance().RemoveTask(this);
}


const char *  MODI_ZoneTask::MakeYYMd5(std::string & password)
{
    unsigned char md5_out[17];
    memset(md5_out, 0 , sizeof(md5_out));
    char final_out[33];
    memset(final_out, 0, sizeof(final_out));

    MD5Context check_str;
    MD5Init( &check_str );
    MD5Update( &check_str , (const unsigned char*)(password.c_str()) , password.size());
    MD5Final(md5_out , &check_str );
    Binary2String((const char *)md5_out, 16, final_out);
	std::string recv_check_str = final_out;
	//   transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
    return recv_check_str.c_str();
}
