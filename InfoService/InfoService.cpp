/**
 * @file   InfoService.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Jan 26 18:29:23 2011
 * 
 * @brief  信息服务器
 * 
 * 
 */


#include "InfoService.h"
#include "InfoTask.h"
#include "ZoneTask.h"
#include "FunctionTime.h"
#include "ServerConfig.h"
#include "TaskQueue.h"

MODI_InfoService * MODI_InfoService::m_pInstance = NULL;
MODI_TaskQueue * MODI_InfoService::m_pInfoTaskQueue = NULL;

/** 
 * @brief 创建新任务
 * 
 * @param sock 任务相关的task
 * @param addr 任务相关的地址
 * @param port 任务相关的端口
 * 
 * @return 成功true 失败false
 */		
bool MODI_InfoService::CreateTask(const int & sock, const struct sockaddr_in * addr, const WORD & port)
{
	/// 增加一个同步账号服务器
	if(port == m_wdInfoPort)
	{
		if(sock == -1 || addr == NULL)
			return false;
		std::string sync_ip = inet_ntoa(addr->sin_addr);
		if(!(sync_ip == "220.248.106.210" || sync_ip == "221.181.65.130" || sync_ip == "221.181.65.133" || sync_ip == "58.221.37.76" || sync_ip == "210.14.67.24"
		 ||sync_ip == "119.188.10.237" || sync_ip == "192.168.2.208" || sync_ip == "202.107.244.122" || sync_ip == "115.238.144.171" || sync_ip == "60.12.206.171"
			 || sync_ip == "115.238.144.175" || sync_ip == "60.12.206.175"))
		{
			Global::logger->fatal("[sync_ip_error] <syncip=%s>", sync_ip.c_str());
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}

		MODI_InfoTask * p_task = (MODI_InfoTask *)(m_pInfoTaskQueue->Get());//new MODI_InfoTask(sock, addr);
		if(p_task)
		{
			p_task->Init(sock, addr, m_pInfoTaskQueue);
			m_stInfoTaskSched.AddNormalSched(p_task);
		}
		else
		{
			TEMP_FAILURE_RETRY(close(sock));
			Global::logger->fatal("[get_task] Unable get a info");
			return false;
		}
		return false;
	}
	/// 增加一个游戏服务器连接
	else if(port == m_wdZonePort)
	{
		if(sock == -1 || addr == NULL)
			return false;

		std::string task_ip = inet_ntoa(addr->sin_addr);
		WORD zone_id = 0;
		if(! CheckTaskIP(task_ip.c_str(), zone_id))
		{
			Global::logger->fatal("[task_ip_error] <taskip=%s>", task_ip.c_str());
 			TEMP_FAILURE_RETRY(close(sock));
 			return false;
		}
		
		MODI_ZoneTask * p_task = new MODI_ZoneTask(sock, addr);
		if(p_task == NULL)
		{
			TEMP_FAILURE_RETRY(close(sock));
			return false;
		}
#ifdef _HRX_DEBUG
		static WORD test_id = 1;
		p_task->SetZoneID(test_id++);
#else 		
		p_task->SetZoneID(zone_id);
#endif		
		
		
		if(m_stZoneTaskSched.AddNormalSched(p_task))
			return true;
		delete p_task;
		TEMP_FAILURE_RETRY(close(sock));
		return false;
	}
	else
	{
		if(sock != -1)
			TEMP_FAILURE_RETRY(close(sock));
	}
	return false;
}


/** 
 * @brief 初始化
 * 
 * @return 成功true 失败false
 */
bool MODI_InfoService::Init()
{
	if(! MODI_MNetService::Init())
		return false;
	m_stInfoTaskSched.Init();
	m_stZoneTaskSched.Init();
	m_pInfoTaskQueue = new MODI_TaskQueue();
	
	for(int i = 0; i < 2000; i++)
	{
		usleep(100);
		MODI_InfoTask * p_task = new MODI_InfoTask(m_pInfoTaskQueue);
		m_pInfoTaskQueue->Put(p_task);
	}

	Global::logger->debug("[sys_init] infoservice initialization successful");
	return true;
}

/** 
 * @brief 释放资源
 * 
 */
void MODI_InfoService::Final()
{
	MODI_MNetService::Final();
}


bool MODI_InfoService::CheckTaskIP(const char * task_ip, WORD & zone_id)
{
	const MODI_SvrInfoConfig  * pInfoConfig = (const MODI_SvrInfoConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_INFOSERVICE ));
	if( !pInfoConfig )
	{
		Global::logger->debug("not get config infoservice");
		return false;
	}
	std::string ip = task_ip;
	std::map<WORD, std::string>::const_iterator iter = pInfoConfig->m_ZoneInfoMap.begin();
	for(; iter != pInfoConfig->m_ZoneInfoMap.end(); iter++)
	{
	   	if(ip == iter->second)
		{
			zone_id = iter->first;
			return true;
		}
	}
	return false;
}


/** 
 * @brief 重新加载配置文件
 * 
 */
void MODI_InfoService::ReloadConfig()
{
	MODI_ServerConfig::GetInstance().Unload();
	MODI_ServerConfig::GetInstance().Load("./Config/config.xml");
	Global::logger->info("[reload_info] reload infoservice config successful");
}
