/**
 * @file   InfoServerAPP.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Thu Jan 27 10:01:57 2011
 * 
 * @brief  信息服务器
 * 
 * 
 */


#include "InfoServerAPP.h"
#include <fstream>
#include "Share.h"
#include "InfoTask.h"
#include "DBClient.h"
#include "InfoService.h"
#include "InfoTick.h"
#include "AccInfoManager.h"
#include "AccVerifySched.h"

MODI_InfoServerAPP::MODI_InfoServerAPP( const char * szAppName ):MODI_IAppFrame( szAppName )
{
	
}


MODI_InfoServerAPP::~MODI_InfoServerAPP()
{
	
}

/** 
 * @brief 初始化
 * 
 * @return 返回enum
 *
 */
int MODI_InfoServerAPP::Init()
{
	int iRet = MODI_IAppFrame::Init();
	if( iRet != MODI_IAppFrame::enOK )
	{
		Global::logger->fatal("[system_init] iappframe init failed <infoservice>");
		return iRet;
	}

	const MODI_SvrInfoConfig  * pInfoConfig = (const MODI_SvrInfoConfig *)(MODI_ServerConfig::GetInstance().GetConfig( SVRCFG_INFOSERVICE ));
	if( !pInfoConfig )
	{
		Global::logger->debug("not get config infoservice");
		return MODI_IAppFrame::enConfigInvaild;
	}

	// 增加一个日志文件到指定目录
    Global::logger->AddLocalFileLog(pInfoConfig->m_strLogPath.c_str());
	std::string net_log_path = pInfoConfig->m_strLogPath + ".net";
	Global::net_logger->AddLocalFileLog(net_log_path.c_str());

	std::vector<WORD> word_vec;
	word_vec.push_back(pInfoConfig->m_wdInfoPort);
	word_vec.push_back(pInfoConfig->m_wdZonePort);

	std::vector<std::string> ip_vec;
	ip_vec.push_back( pInfoConfig->m_strInfoIP.c_str() );
	ip_vec.push_back( pInfoConfig->m_strZoneInIP.c_str() );
	m_pNetService = new MODI_InfoService(word_vec, ip_vec);
	
	if( !m_pNetService )
	{
		return MODI_IAppFrame::enConfigInvaild;
	}

	// 连接游戏数据库
	if(! MODI_DBManager::GetInstance().Init(pInfoConfig->m_strURL))
	{
		Global::net_logger->debug("not connect db");
		return MODI_IAppFrame::enConfigInvaild;
	}

	MODI_InfoTask::m_pAccTbl = MODI_DBManager::GetInstance().GetTable("accountstbl");
	if(! MODI_InfoTask::m_pAccTbl)
	{
		Global::logger->fatal("[%s] Unable get accountstbl table struct", SYS_INIT);
		return MODI_IAppFrame::enConfigInvaild;
	}

	/// 读取数据库里面的账号信息(后期读取的时候要加limit)
	MODI_AccInfoManager::GetInstance().Init();
	
	MODI_AccVerifySched::GetInstance().Init();
	
	if(! MODI_InfoTick::GetInstance().Start())
	{
		Global::logger->fatal("Unable initialization info client");
		return MODI_IAppFrame::enConfigInvaild;
	}
	
	return MODI_IAppFrame::enOK;
}


int MODI_InfoServerAPP::Run()
{
	// 后台运行（如果设置了的话)
	MODI_IAppFrame::RunDaemonIfSet();
	Global::net_logger->RemoveConsoleLog();

// 	if( !MODI_IAppFrame::SavePidFile() )
// 		return enPidFileFaild;

	Global::logger->info("[%s] START >>>>>>   	%s ." , SYS_INIT , m_strAppName.c_str() );
	
	if( m_pNetService )
	{
		m_pNetService->Main();
	}
	else 
	{
		Global::logger->info("[%s] start game server faild. net service moudle not exist. " , SYS_INIT );
		return MODI_IAppFrame::enNotExistNetService;
	}

	return MODI_IAppFrame::enOK;
}


int MODI_InfoServerAPP::Shutdown()
{
	if( m_pNetService )
	{
		m_pNetService->Terminate();
		delete m_pNetService;
		m_pNetService = NULL;
	}

	MODI_AccInfoManager::GetInstance().DelInstance();
	MODI_AccVerifySched::GetInstance().DelInstance();
	MODI_InfoTick::GetInstance().DelInstance();
	
	//	MODI_IAppFrame::RemovePidFile();

	return MODI_IAppFrame::enOK;
}
