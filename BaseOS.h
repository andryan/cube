/** 
 * @file BaseOS.h
 * @brief 一些不同平台（操作系统）相关的API 封装
 * @author Tang Teng
 * @version v0.1
 * @date 2010-03-01
 */

#ifndef CUBE_BASE_OS_H_
#define CUBE_BASE_OS_H_


/*-----------------------------------------------------------------------------
 *  include files
 *-----------------------------------------------------------------------------*/
#ifdef _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#else 
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <signal.h>

#include <set>
#include <list>
#include <string>
#include <map>
#include <queue>
#include <sstream>
#include <algorithm>


#ifdef _WIN32
/*-----------------------------------------------------------------------------
 *  windows 
 *-----------------------------------------------------------------------------*/

#define MODI_EXPORT __declspec(dllexport)
#define MODI_LIBRARY_HANDLE HMODULE
#define MODI_LOAD_LIBRARY(a) LoadLibrary(a)
#define MODI_CLOSE_LIBRARY FreeLibrary
#define MODI_GET_PROC_ADDR GetProcAddress

#define SNPRINTF _snprintf
#define STRTOULL _strtoui64
#define I64FMT "%016I64X"
#define I64FMTD "%I64u"
#define SI64FMTD "%I64d"

typedef float 				   float32;
typedef double 				   float64;

typedef unsigned long long	   QWORD;


#else 


/*-----------------------------------------------------------------------------
 *  linux
 *-----------------------------------------------------------------------------*/
#define MODI_EXPORT export
#define MODI_LIBRARY_HANDLE void*
#define MODI_LOAD_LIBRARY(a) dlopen(a,RTLD_NOW)
#define MODI_CLOSE_LIBRARY dlclose
#define MODI_GET_PROC_ADDR dlsym

#define SNPRINTF snprintf
#define STRTOULL strtoull
#define I64FMT "%016llX"
#define I64FMTD "%llu"
#define SI64FMTD "%lld"

typedef unsigned char    BYTE;
typedef unsigned short int   WORD;
typedef unsigned int   DWORD;
typedef unsigned long long QWORD;

typedef float 				   float32;
typedef double 				   float64;

#endif







#endif


