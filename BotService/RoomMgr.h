/**
 * @file RoomMgr.h
 * @date 2010-2-20 CST
 * @version Ver
 * @author calendar gongliang@modi.com
 * @brief 房间管理器类
 */
#ifndef __ENGINE_ROOM_ROOMMGR_H__
#define __ENGINE_ROOM_ROOMMGR_H__
#include "Room.h"
#include "protocol/c2gs.h"
/**
 * @brief 房间管理器类
 */
class MODI_RoomMgr
{
public:
	typedef std::vector<MODI_Room *> defRoomVec;

    template<class Type>
    bool CreateRoom(MODI_RoomInfo*pRoomInfo)
    {
        //需要对房间id进行判断
        //如果房间Id为非法值则放回
        if(pRoomInfo)
        {
            Type* pRoom = new Type(pRoomInfo);
            return AddRoom(pRoom);
        }
        return false;
    }

public:
    /**
     * @brief 获取实例
     * @return MODI_RoomMgr* 实例指针
     */
    static MODI_RoomMgr* GetInstance();

    /**
     * @brief 释放实例资源
     */
    static void Release();

    /**
     * @brief 获取当前房间数量
     * @return int 当前房间数量
     */
    int GetRoomCount(ROOM_SUB_TYPE roomType = ROOM_STYPE_END); 

    /**
     * @brief 通过房间ID获取房间指针
     * @param unsigned char roomid 房间ID
     * @return MODI_Room* 房间指针
     */
    MODI_Room* GetRoomById(unsigned char roomid);

	/**
	 * @brief 通过房间位置索引获取房间指针
	 * @param unsigned int index 房间位置索引
	 * @return MODI_Room* 房间指针
	 */
	MODI_Room* GetRoomByIdx(unsigned int index, ROOM_SUB_TYPE roomType = ROOM_STYPE_END);

    /**
     * @brief 增加房间
     * @param MODI_RoomInfo * pRoominfo 房间信息
     * @return bool 成功true，失败false
     */
    bool AddRoomByRoomInfo(MODI_RoomInfo* pRoominfo);

    /**
     * @brief 删除房间
     * @param unsigned char roomid 房间ID
     * @return bool 成功true，失败false
     */
    bool DeleteRoom(unsigned char roomid);

    /**
     * @brief 更新房间列表
     * @param MODI_GS2C_Notify_Roomlist * pRoomList 房间列表信息
     */
    void UpdateRoomInfoList(MODI_GS2C_Notify_Roomlist* pRoomList);

    /**
     * @brief 更新房间信息
     * @param unsigned char size 信息长度
     * @param char * updateinfos 更新信息结构
     */
    void UpdateRoomInfo(unsigned char size,char* updateinfos);

	/**
     * @brief 清空所有房间
     */
    void ClearAllRoom();

	MODI_RoomMgr();
	~MODI_RoomMgr();
private:

    /**
     * @brief 增加房间
     * @param MODI_Room * pRoom 房间指针
     * @return bool 成功true，失败false
     */
    bool AddRoom(MODI_Room* pRoom);

	/// 房间列表
    defRoomVec m_RoomVec;
	/// 单实例
    static MODI_RoomMgr* s_pRoomMgr;

	int m_iDanrenjinggeRooms[MAX_ROOM_COUNT];
	int m_iDanrenjinggeRoomsCnt;
	int m_iZuduijinggeRooms[MAX_ROOM_COUNT];
	int m_iZuduijinggeRoomsCnt;
	int m_iDanrenjianpan2Rooms[MAX_ROOM_COUNT];
	int m_iDanrenjianpan2RoomsCnt;
};

#endif // __ENGINE_ROOM_ROOMMGR_H__
