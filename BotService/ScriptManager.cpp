/**
 * @file   ScriptManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 12:03:42 2011
 * 
 * @brief  脚本管理
 * 
 * 
 */

#include "Global.h"
#include "XMLParser.h"
#include "ScriptManager.h"

const std::string MODI_DIRTask::m_strDir =  "./BotScript/TriggerTask/";
const std::string MODI_DIREnter::m_strDir = "./BotScript/TriggerEnter/";
const std::string MODI_DIRTime::m_strDir = "./BotScript/TriggerTime/";


