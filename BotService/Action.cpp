/**
 * @file   Action.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 17:07:07 2011
 * 
 * @brief  动作实现
 * 
 * 
 */

#include "Action.h"
#include "BotUser.h"
#include "SplitString.h"
#include "BotUserVar.h"
#include "HelpFuns.h"
#include "protocol/c2ls_logincmd.h"
#include "protocol/c2gs.h"

/** 
 * @brief 变量更新
 * 
 * @return 
 */
bool MODI_VarAction::Init(const char * action_line)
{
	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	m_strName = split["name"];
	m_strAttribute = split["attribute"];
	m_strOp = split["op"];
	m_wdValue = atoi(split["value"]);
	return true;
}


/** 
 * @brief 变量更新
 * 
 */
bool MODI_VarAction::do_it(MODI_BotUser * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		MODI_ASSERT(0);
		return false;
	}

	MODI_BotUserVar * p_var = p_client->m_stVarManager.GetVar(m_strName.c_str());

	if(! p_var)
	{
		p_var = new MODI_BotUserVar(m_strName.c_str());
		if(p_var->Init(p_client, m_strAttribute.c_str()))
		{
			if(! p_client->m_stVarManager.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
				//#ifdef _VAR_DEBUG
				//				Global::logger->debug("[do_action] do action when new a var <name=%s,type=%d,value=%u,time=%ul>",
				//									  p_var->GetName(), p_var->GetType(), p_var->GetValue(), p_var->GetTime());
				//#endif				
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	if(!p_var)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	WORD new_value = 0;
	if(m_strOp == "add")
	{
		new_value = m_wdValue + p_var->GetValue();
	}
	else if(m_strOp == "sub")
	{
		if(p_var->GetValue() > m_wdValue)
		{
			new_value =  p_var->GetValue() - m_wdValue;
		}
		else
		{
			Global::logger->error("[var_action] var action sub failed <name=%s>", p_var->GetName());
			MODI_ASSERT(0);
			return false;
		}
	}
	else if(m_strOp == "set")
	{
		new_value = m_wdValue;
	}
	else
	{
		return false;
	}
	
// #ifdef _VAR_DEBUG
// 	Global::logger->debug("[do_action] do action <name=%s,op=%s,oldvalue=%u,opvalue=%u,newvalue=%u>",
// 						  p_var->GetName(), m_strOp.c_str(),p_var->GetValue(), m_wdValue, new_value);
// #endif	
	p_var->SetValue(new_value);
	return true;
}




/** 
 * @brief 触发登陆
 * 
 */
bool MODI_Trigger_Login_Action::Init(const char * action_line)
{
	return true;
}


/** 
 * @brief 触发登陆
 * 
 */
bool MODI_Trigger_Login_Action::do_it(MODI_BotUser * p_client)
{
	MODI_C2LS_Request_Login loginpacket;
	loginpacket.version.major = MODI_CUBE_MAJOR_VERSION;
	loginpacket.version.minor = MODI_CUBE_MINOR_VERSION;
	//std::string accountname = m_->GetAccountName();
	strncpy(loginpacket.m_szAccountName, p_client->GetAccountName(), sizeof(loginpacket.m_szAccountName));
#ifndef _DEBUG	
	std::string password = "12345678";
	strncpy(loginpacket.m_szAccountPwd, password.c_str(), sizeof(loginpacket.m_szAccountPwd));
#else	
	strncpy(loginpacket.m_szAccountPwd, p_client->GetAccountName(), sizeof(loginpacket.m_szAccountPwd));
#endif	
	memset(loginpacket.m_pMacAddr, 0xFF, sizeof(loginpacket.m_pMacAddr));
	
	p_client->SendCmd(&loginpacket, sizeof(loginpacket));
	return true;
}

bool	MODI_Speak_Action::Init(const char * action_line)
{

	if(! action_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(action_line);
	
	m_sContext = split["context"];
	m_nChannel = atoi(split["channel"]);

	return true;
}


bool	MODI_Speak_Action::do_it(MODI_BotUser * p_client)
{
	MODI_C2GS_Request_RangeChat  req;
	req.m_byChannel = m_nChannel;
	strncpy(req.m_cstrContents ,m_sContext.c_str(),CHATMSG_MAX_LEN);

	p_client->SendCmd(&req,sizeof(MODI_C2GS_Request_RangeChat));
	return true;
}

bool	MODI_EnterShop_Action::Init(const char * action_line)
{
	return true;
}

bool	MODI_EnterShop_Action::do_it(MODI_BotUser * p_client)
{
	MODI_C2GS_Request_EnterShop   req;
	p_client->SendCmd(&req,sizeof(MODI_C2GS_Request_RangeChat));		
	return true;
}

bool	MODI_LeaveShop_Action::Init(const char * action_line)
{
	return true;
}

bool	MODI_LeaveShop_Action::do_it(MODI_BotUser * p_client)
{
	MODI_C2GS_Request_LeaveShop  req;
	p_client->SendCmd(&req,sizeof(MODI_C2GS_Request_LeaveShop));
	return true;
}


bool	MODI_RandJoin_Action::Init(const char * action_line)
{
	return true;
}


bool	MODI_RandJoin_Action::do_it(MODI_BotUser * p_client)
{
	if( p_client->m_eState == enBotUserState_Room)
		return true;
	MODI_C2GS_Request_RandomJoinRoom  req;
	req.m_type = ROOM_TYPE_NORMAL; 
	req.m_subtype = ROOM_STYPE_END;
	p_client->SendCmd(&req,sizeof(MODI_C2GS_Request_RandomJoinRoom));
	return true;
}


bool	MODI_LeaveRoom_Action::Init(const char * action_line)
{
	return true;
}

bool	MODI_LeaveRoom_Action::do_it(MODI_BotUser * p_client)
{
	if( p_client->m_eState != enBotUserState_Room)
		return true;
	MODI_C2GS_Request_LeaveRoom req;
	p_client->SendCmd(&req,sizeof(MODI_C2GS_Request_LeaveRoom));
	p_client->m_eState = enBotUserState_Lobby;
	return true;
}



bool	MODI_CreateRoom_Action::Init(const char * action_line)
{
	return true;
}

bool 	MODI_CreateRoom_Action::do_it(MODI_BotUser * p_client)
{
	if( p_client->m_eState == enBotUserState_Room)
		return true;
	MODI_C2GS_Request_CreateRoom req_createroom;
	req_createroom.m_roomType = RoomHelpFuns::MakeRoomType(false,ROOM_TYPE_NORMAL,ROOM_STYPE_NORMAL);
	req_createroom.m_MapID = INVAILD_CONFIGID;  //随机地图ID
	req_createroom.m_MusicID = INVAILD_CONFIGID; //随机音乐ID
	//strncpy(req_createroom.m_cstrPassword, roompsd.c_str());
	strncpy(req_createroom.m_cstrRoomName, p_client->GetAccountName(), sizeof(req_createroom.m_cstrRoomName) );
	req_createroom.m_bIsOffSpectator = false;
	p_client->SendCmd(&req_createroom,sizeof(MODI_C2GS_Request_CreateRoom));
	return true;
}








