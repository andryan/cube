#ifndef _C2GS_PACKAGE_HANDLER_H__
#define _C2GS_PACKAGE_HANDLER_H__

#include "IPackageHandler.h"

class  MODI_PackageHandler : public MODI_IPackageHandler
{

public:
	MODI_PackageHandler();
	~MODI_PackageHandler();
	static MODI_PackageHandler * GetInstance();

	static int ReceiveRoleInfo(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int ReceiveChannelList(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int ReceiveGateWayInfo(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int ReceiveOptResult(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int CreateRoomSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int EnterShopSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);

	static int LeaveShopSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size);




protected:

    virtual bool FillPackageHandlerTable();
private:

	static MODI_PackageHandler * m_pInstance;


};


#endif







