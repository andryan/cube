/**
 * @file   BotGlobal.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 17:25:24 2011
 * 
 * @brief  机器人全局变量
 * 
 * 
 */

#include "Global.h"

namespace Global
{
	/// 机器人进入那个频道
	extern WORD g_wdChannelId;
};

