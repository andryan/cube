/**
  	* @file Room.h
	* @date 21:2:2010  14:32
	* @version Ver
	* @author calendar gongliang@modi.com
	* @brief 
	*
*/
#ifndef _Room_h_2010_01_07_14_31
#define _Room_h_2010_01_07_14_31

#include "protocol/gamedefine.h"
#define MAX_PLAYER_NUM MAX_PLAYSLOT_INROOM      //最大玩家人数
#define MAX_AUDIENCE_NUM MAX_SPECTATORSLOT_INROOM   //最大观众人数
#define MAX_TEAM_NUM 3      //最大玩家人数
/**
	* @brief  房间类
	*
	* 
*/
class MODI_Room
{
public:
    /**
     * @brief 
     *
     * @return: 
     */
    virtual ~MODI_Room();
    
    /**
     * @brief 设置房间地图ID
     *
     * @param:   defMapID mapid  地图ID
     * @return: void
     */
    virtual void SetMapId(defMapID mapid);

    /**
     * @brief 获得房间地图ID
     *
     * @return: defMapID
     */
    virtual unsigned char GetMapId();
    
    /**
     * @brief 设置房间音乐
     *
     * @param:   defMusicID musicid 音乐ID
     * @return: void
     */
    virtual void SetMusicId(defMusicID musicid,enMusicFlag flag=kMusicFlag_Yuanchang,
							enMusicSType type = kMusicSongType_Heng,bool randam = false,
							enMusicTime timetype = kMusicTime_All);
    
    /**
     * @brief 设置新的房主ID
     *
     * @param:   unsigned long long masterid  房主ID
     * @return: void
     */
    virtual void SetMasterId(MODI_GUID masterid);
    
    /**
     * @brief 设置新的房间名称
     *
     * @param:   const char * pRoomName
     * @return: void
     */
    virtual void SetRoomName(const char* pRoomName);
    
    /**
     * @brief 房间里选择的音乐ID
     *
     * @return: unsigned char
     */
    virtual unsigned long GetMusicId();

    /** 获得音乐是否伴奏和原唱
     * @brief 
     *
     * @return: enMusicFlag
     */
    virtual enMusicFlag GetMusicFlag();

	virtual unsigned short GetMusicType();
	
	virtual unsigned short GetMusicTimeType();
    /**
     * @brief 设置当前听歌状态
     *
     * @param:   bool bIsListen
     * @return: void
     */
    virtual void SetListen(bool bIsListen);

	 /**
     * @brief 设置当前房间是否加锁掩码
     *
     * @param:   bool bLock
     * @return: void
     */
	void SetLock(bool bLock);

	 /**
     * @brief 设置当前房间是否禁止旁观掩码
     *
     * @param:   bool bIsForbiddenListen
     * @return: void
     */
	void SetForbiddenListen(bool bIsForbiddenListen);

    /**
     * @brief 当前是否是听歌状态
     *
     * @return: bool
     */
    virtual bool IsListen();

    //房间ID
    /**
     * @brief 
     *
     * @return: unsigned char
     */
    virtual unsigned char GetRoomId();
    
    /**
     * @brief 房间类型
     *
     * @return: unsigned char
     */
    virtual ROOM_TYPE GetRoomType();

    /**
     * @brief 房间子类型
     *
     * @return: unsigned char
     */
	virtual ROOM_SUB_TYPE GetRoomSubType();
    
    /**
     * @brief 获取房间名称
     *
     * @return: std::string  房间名称
     */
    virtual const char* GetRoomName();
    
    /**
     * @brief 获得房间的选择歌曲名称
     *
     * @return: std::string  歌曲名称
     */

	/**
	 * @brief 获得当前房间的选择歌曲的信息
	 *
	 * @return: const GameTable::MODI_Musiclist*
	 */
    
    /**
     * @brief 判断该占位是否有玩家
     *
     * @param:   unsigned char playerpos
     * @return: bool
     */
    virtual bool IsHavePlayer(unsigned short playerpos);
    
    /**
     * @brief 判断该ID是否是玩家，还是旁观,或者这个玩家不在房间中
     *
     * @param:   unsigned long long playerid
     * @return: int
     */
    virtual int IsPlayer(const MODI_GUID& playerid);
    
	virtual unsigned char GetTeamType(const MODI_GUID& playerid);
    /**
     * @brief 该占位是否准备状态
     *
     * @param:   unsigned char playerpos
     * @return: bool
     */
    virtual bool IsPlayerPosReady(unsigned short playerpos);
    
    /**
     * @brief 该占位是否被关闭
     *
     * @param:   unsigned char playerpos
     * @return: bool
     */
    virtual bool IsPlayerPosClosed(unsigned short playerpos);
    
    /**
     * @brief 旁听位置是否被关闭
     *
     * @param:   unsigned char playerpos
     * @return: bool
     */
    virtual bool IsListenerPosClosed(unsigned short playerpos);
    
    /**
     * @brief 房间是否加密
     *
     * @return: bool
     */
    virtual bool IsLocked();

	/**
     * @brief 歌曲随机
     *
     * @return: bool
     */
	virtual bool IsSongRandom()
	{
		return m_RoomInfo.m_detailNormalRoom.m_bRandomMusic;
	}
    
    /**
     * @brief 房间是否禁止旁观
     *
     * @return: bool
     */
    virtual bool IsForbiddenListen();
    
    /**
     * @brief 当前参赛的玩家数量
     *
     * @return: int
     */
    virtual int GetPlayerNumInLobby();

    /**
     * @brief 游戏中获得演唱者数量
     *
     * @return: unsigned int
     */
    virtual unsigned int GetPlayerNumInPlaying();

    /**
     * @brief 游戏中获得旁听者数量
     *
     * @return: unsigned int
     */
    virtual unsigned int GetListenerNumInPlaying();
    
    /**
     * @brief 当前参赛的男性玩家数量
     *
     * @return: int
     */
    virtual int BoyPlayerNum();
    
    /**
     * @brief 当前参赛的女性玩家数量
     *
     * @return: int
     */
    virtual int GirlPlayerNum();
    
    /**
     * @brief 当前旁观的玩家数量
     *
     * @return: int
     */
    virtual int GetListenerNumInLobby();
    
    /**
     * @brief 最大允许的旁观玩家数量
     *
     * @return: int
     */
    virtual int GetMaxListenerNum();
    
    /**
     * @brief 获得房主的全局ID
     *
     * @return: unsigned long long  房主ID
     */
    virtual MODI_GUID GetMasterId();
    
    /**
     * @brief 获得房主所在的占位
     *
     * @return: unsigned short  玩家占位
     */
    virtual unsigned short GetMasterPos();
    
    /**
     * @brief 根据玩家的占位获得玩家的GUID
     *
     * @param:   unsigned short playerpos  玩家占位
     * @return: unsigned long long  玩家ID
     */
    virtual MODI_GUID GetPlayerGUIDByPos(unsigned short playerpos);
	
	/**
     * @brief 根据玩家的名字获得玩家的GUID
     *
     * @param:  const char* pName 玩家名字
     * @return: unsigned long long  玩家ID
     */
	MODI_GUID GetPlayerGUIDByName(const char* pPlayerName);
    
	/**
     * @brief 
     *
     * @param:   unsigned short playerpos
     * @param:   const MODI_GUID & playerid
     * @return: void
     */
    virtual void SetPlayerGUIIDByPos(unsigned short playerpos,const MODI_GUID& playerid);
    
    /**
     * @brief 根据旁听的占位获得玩家的GUID
     *
     * @param:   unsigned short listenerpos  玩家占位
     * @return: unsigned long long  玩家ID
     */
    virtual MODI_GUID GetListenerGUIDByPos(unsigned short listenerpos);

    /**
     * @brief 
     *
     * @param:   unsigned short listenerpos
     * @param:   const MODI_GUID & playerid
     * @return: void
     */
    virtual void SetListenerGUIDByPos(unsigned short listenerpos,const MODI_GUID& playerid);
    
    /**
     * @brief 获取玩家在房间中的占位信息,返回-1为错误信息
     *
     * @param:   unsigned long long playerid 玩家ID
     * @return: int  玩家占位
     */
    virtual int GetPlayerPos(const MODI_GUID& playerid);
    
    /**
     * @brief 房主打开房间占位
     *
     * @param:   unsigned short playerpos 玩家占位
     * @return: void
     */
    virtual void OpenPlayerPos(unsigned short playerpos);
    
    /**
     * @brief 房主关闭房间占位
     *
     * @param:   unsigned short playerpos 玩家占位
     * @return: void
     */
    virtual void ClosePlayerPos(unsigned short playerpos);
    
    /**
     * @brief 改变玩家准备状态
     *
     * @param:   unsigned short playerpos  玩家占位
     * @param:   bool bReady  是否准备
     * @return: void
     */
	virtual void ChangePlayerState(unsigned short playerpos,bool bReady);

    /**
     * @brief 改变玩家队伍
     *
     * @param:   unsigned short playerpos  玩家占位
     * @param:   ROOM_TEAM_TYPE iTeam  在哪个队伍
     * @return: void
     */
	virtual	void ChangePlayerTeam(unsigned short playerpos,ROOM_TEAM_TYPE iTeam);
    
    /**
     * @brief 打开旁观占位
     *
     * @param:   unsigned short listenerpos  玩家占位
     * @return: void
     */
    virtual void OpenListenerPos(unsigned short listenerpos);
    
    /**
     * @brief 关闭旁观占位
     *
     * @param:   unsigned short listenerpos  玩家占位
     * @return: void
     */
    virtual void CloseListenerPos(unsigned short listenerpos);
    
    /**
     * @brief 有玩家离开房间
     *
     * @param:   unsigned long long playerid
     * @return: void
     */
    virtual void LeavePlayer(MODI_GUID playerid);
    
    /**
     * @brief 有玩家进入房间
     *
     * @param:   unsigned short playerpos
     * @param:   unsigned long long playerid
     * @return: void
     */
    virtual void AddPlayer(unsigned short playerpos,MODI_GUID playerid);
    
    /**
     * @brief 玩家切换占位状态
     *
     * @param:   bool bIsSrcPlayer   toplayer  false  切换为旁观  toplayer true 切换为参赛
     * @param:   unsigned short playerpos 
     * @param:   unsigned short listenerpos
     * @return: void
     */
    virtual void ChangePlayerPosition(bool bIsSrcPlayer,unsigned short playerpos,unsigned short listenerpos);
	 
	/**
	 * @brief 迭代获取玩家
	 *
	 * @return: unsigned long long
	 */
	virtual MODI_GUID GetNextPlayer();

	/**
	 * @brief 
	 *
	 * @return: void
	 */
	virtual void RewindPlayerCursor();
    
    /**
     * @brief 获得房间中所有玩家的全局ID
     *
     * @param:   std::vector<MODI_GUID> & vecPlayerGUID
     * @return: void
     */
    virtual void GetAllPlayerGUID(std::vector<MODI_GUID>& vecPlayerGUID);
	 
	/**
	 * @brief 获取打开房间占位数量
	 *
	 * @return: int
	 */
	virtual int GetMaxPlayerNum();

    /**
     * @brief 房间开始游戏
     *
     * @return: bool
     */
    virtual bool StartGame();

    /**
     * @brief 比赛结束
     *
     * @return: bool
     */
    virtual bool GameOver();

    /**
     * @brief 玩家离开房间
     *
     * @return: bool
     */
    virtual bool LeaveRoom();

    /**
     * @brief 判断玩家是否在房间中
     *
     * @param:   const char * playername
     * @return: bool
     */
    virtual bool IsInRoom(const char* playername);
    
	/**
	 * @brief 播放摄像机动画
	 *
	 * @param: const char * lensType 类型名
	 * @param: const char * lensName 镜头名
	 * @return: void
	 */
	virtual void PlayCamera(const char *lensType, const char *lensName);

	/**
	* @brief 播放缺省角色动作
	*
	* @return: void
	*/
	virtual void PlayDefaultActorMotion();

    /**
     * @brief 发送每句的得分给服务器
     *
     * @return: void
     */
    virtual void SendOneSentenceResult(int sentence, unsigned int score,unsigned char type,
                                        int comboNum,float exact,float second);

     /**
     * @brief 发送每一次按键得分给服务器
     *
     * @return: void
     */
    virtual void SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever);

    /**
     * @brief 发送口型数据给服务器
     *
     * @return: void
     */
    virtual void SendMicInput(bool input, float time);

    /**
     * @brief 发送fevertime 给服务器
     *
     * @param:   float time
     * @return: void
     */
    virtual void SendFeverTime(float time);

    /**
     * @brief 发送fever 结束时间给服务器
     *
     * @param:   float time
     * @return: void
     */
    virtual void SendFeverTimeOver(float time);

    /**
     * @brief 通知服务器游戏结束
     *
     * @return: void
     */
    virtual void SendGameOver();

	virtual unsigned char GetGameMode();

	virtual void SetGameMode( unsigned char gameMode );

	inline virtual bool IsGamePlaying( void )
	{
		return m_RoomInfo.m_bPlaying; 
	}

	void SetNPCSinging(bool npcsinging);

	inline bool IsNPCSinging(void) 
	{
		return m_blNPCSinging; 
	}

	void SetTutorialing(bool tutorialing);

	inline bool IsTutorialing(void) 
	{
		return m_blTutorialing; 
	}

	void SetMicTesting(bool mictesting);

	inline bool IsMicTesting(void)
	{
		return m_blMicTesting; 
	}
	//设置玩家位置的队伍信息
	void SetPlayerTeam(unsigned char* playerTeam);
	//获取特定位置玩家队伍
	unsigned char GetTeamTypeByPos(unsigned short ipos);
	//队伍是否分配均衡
	bool IsTeamOk();
	//在组队模式下，获取特定位置玩家的GUID
	MODI_GUID GetTeamPlayerGUIDByPos(unsigned short playerpos);
	//队伍排序
	void TeamSort();


protected:

    struct PlayerPos
    {
        bool bIsClosed;  //位置是否被关闭
        bool bReady;     //该位置玩家是否准备
        MODI_GUID playerid;    //玩家的角色id
		unsigned char iTeamType; //队伍类型

        PlayerPos()
        {
			playerid = INVAILD_GUID;
            bReady = false;
            bIsClosed = false;
			iTeamType = ROOM_TEAM_BEGIN;
        }
    };

    MODI_Room();
    MODI_Room(MODI_RoomInfo* pRoomInfo);
	MODI_RoomInfo m_RoomInfo;
    //玩家数组,固定位置
    PlayerPos m_Player[MAX_PLAYER_NUM];
    //观众数组
    PlayerPos m_Audience[MAX_AUDIENCE_NUM];
	//按队伍排序的玩家位置
	PlayerPos m_TeamPlayer[MAX_PLAYER_NUM];

    bool m_bListen;

	//! 索引玩家游标
	int m_iPlayerCursor;

	//是否NPC在演唱
	bool m_blNPCSinging;

	//是否在新手引导
	bool m_blTutorialing;

	//是否在MIC采样检测
	bool m_blMicTesting;

	//队伍是否分配均衡
	bool m_bTeamOk;

    friend class MODI_RoomMgr;
};


#endif
