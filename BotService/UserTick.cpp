/**
 * @file   UserTick.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 14:56:00 2011
 * 
 * @brief  机器人时钟
 * 
 * 
 */

#include "UserTick.h"
#include "BotUser.h"

/// 初始化
bool MODI_UserTick::Init()
{
	if(! Start())
	{
		return false;
	}
	return true;
}


/// 主循环
void MODI_UserTick::Run()
{
	while(!IsTTerminate())
	{
		::usleep(2000);
		
		m_stRTime.GetNow();
		m_stTTime = m_stRTime;
				
		if(m_pBotUser)
		{
			m_pBotUser->UpDate();
		}

		m_pBotUser->m_stVarManager.UpDate(m_stRTime, m_stTTime);
	}
	
	Final();
}


/// 释放
void MODI_UserTick::Final()
{
	m_pBotUser = NULL;
}

