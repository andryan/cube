#include "c2gs_PackageHandler.h"
#include "protocol/c2gs.h"
#include "BotUser.h"
#include "BotGlobal.h"


MODI_PackageHandler * MODI_PackageHandler::m_pInstance = NULL;

MODI_PackageHandler::MODI_PackageHandler()
{

}


MODI_PackageHandler::~MODI_PackageHandler()
{

}

MODI_PackageHandler * MODI_PackageHandler::GetInstance()
{
	if( NULL == m_pInstance)
	{
		m_pInstance = new MODI_PackageHandler();
	}
	return m_pInstance;
}

bool	MODI_PackageHandler::FillPackageHandlerTable()
{
	this->AddPackageHandler(MAINCMD_LOGIN,MODI_LS2C_Notify_Charlist::ms_SubCmd,&MODI_PackageHandler::ReceiveRoleInfo);

	this->AddPackageHandler(MAINCMD_LOGIN,MODI_LS2C_Notify_Charlist::ms_SubCmd,&MODI_PackageHandler::ReceiveChannelList);

	this->AddPackageHandler(MAINCMD_LOGIN,MODI_LS2C_Notify_GatewayInfo::ms_SubCmd,&MODI_PackageHandler::ReceiveGateWayInfo);

	this->AddPackageHandler(MAINCMD_OPTRESULT,SUBCMD_OPTRESULT,&MODI_PackageHandler::ReceiveOptResult);

	this->AddPackageHandler(MAINCMD_ROOM,MODI_C2GS_Response_CreateRoom::ms_SubCmd,&MODI_PackageHandler::CreateRoomSuc);

	this->AddPackageHandler(MAINCMD_SHOP,MODI_GS2C_Notify_EnterShopResult::ms_SubCmd,&MODI_PackageHandler::EnterShopSuc);

	this->AddPackageHandler(MAINCMD_SHOP,MODI_GS2C_Notify_LeaveShopResult::ms_SubCmd,&MODI_PackageHandler::LeaveShopSuc);
	
	return true;
}


int MODI_PackageHandler::ReceiveRoleInfo(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{
	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;
	
	const MODI_LS2C_Notify_Charlist * pCharList = (MODI_LS2C_Notify_Charlist * )pt_null_cmd;
	if(pCharList->m_nRoleCount > 0)
	{
		const MODI_CharInfo * pCharinfo = &pCharList->m_pChars[0];
		m_pUser->m_CharId = pCharinfo->m_nCharID;
	}
	else
	{
		m_pUser->ClientFinal();
	}

	return enPHandler_OK;
}



int MODI_PackageHandler::ReceiveChannelList(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{

	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;


	MODI_C2LS_Request_SelChannel SelectChannel;
	strncpy(SelectChannel.m_szNetType,"dx", sizeof(SelectChannel.m_szNetType));
	SelectChannel.m_nChannelID = Global::g_wdChannelId;
	m_pUser->SendCmd(&SelectChannel, sizeof(SelectChannel));
	return enPHandler_OK;
}


int MODI_PackageHandler::ReceiveGateWayInfo(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{
	const MODI_LS2C_Notify_GatewayInfo   * _pGwayInfo = (MODI_LS2C_Notify_GatewayInfo *) pt_null_cmd;
	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;
	m_pUser->m_LoginKey = _pGwayInfo->m_Key;
	m_pUser->m_dwAccountID = _pGwayInfo->m_nAccountID;
	m_pUser->m_strGateIP = PublicFun::NumIP2StrIP(_pGwayInfo->m_nIP);
	m_pUser->m_wdGatePort = _pGwayInfo->m_nPort;
	m_pUser->m_blIsRecvGatewayInfo = true;

	m_pUser->SetServiceIP(m_pUser->m_strGateIP.c_str());
	m_pUser->SetServicePort(m_pUser->m_wdGatePort);
	m_pUser->ReConnect();
	return enPHandler_OK;
}



int MODI_PackageHandler::ReceiveOptResult(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{
	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;

	const MODI_GS2C_Respone_OptResult * _pOptresult = (MODI_GS2C_Respone_OptResult *)pt_null_cmd;
	switch( _pOptresult->m_nResult)
	{

		/// 登陆成功
		case SvrResult_Login_OK:
		{
			m_pUser->m_eState = enBotUserState_Lobby;
			Global::logger->debug("[login_successful] <name=%s>",m_pUser->GetAccountName());
			char szPackageBuf[Skt::MAX_PACKETSIZE];
			memset(szPackageBuf,0,sizeof(szPackageBuf));
			MODI_C2GS_Request_MusicHeaderData  *_pMusicHeadData = (MODI_C2GS_Request_MusicHeaderData *)szPackageBuf;
			AutoConstruct(_pMusicHeadData);
			_pMusicHeadData->m_nMusicDataHeaderSize = 100;
			int iSendSize = sizeof(MODI_C2GS_Request_MusicHeaderData )+_pMusicHeadData->m_nMusicDataHeaderSize;
			m_pUser->SendCmd(_pMusicHeadData,iSendSize);

		}
		break;

		default:
		break;
	}

	return enPHandler_OK;
}


int MODI_PackageHandler::CreateRoomSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{

	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;
	m_pUser->m_eState  = enBotUserState_Room;
	return enPHandler_OK;
}

int MODI_PackageHandler::EnterShopSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{
	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;
	MODI_GS2C_Notify_EnterShopResult *_res = (MODI_GS2C_Notify_EnterShopResult *)pt_null_cmd;

	if( kIO_Shop_Successful == _res->m_result)
	{
		m_pUser->m_eState = enBotUserState_Shop;
	}
	else
	{
	}

	return enPHandler_OK;
}




int MODI_PackageHandler::LeaveShopSuc(void * pObj,const Cmd::stNullCmd * pt_null_cmd,const unsigned int cmd_size)
{

	///	离开商店成功就回到了大厅
	MODI_BotUser * m_pUser = (MODI_BotUser * )pObj;
	MODI_GS2C_Notify_LeaveShopResult * _res = (MODI_GS2C_Notify_LeaveShopResult *)pt_null_cmd;
	if( kIO_Shop_Successful == _res->m_result)
	{
		m_pUser->m_eState = enBotUserState_Lobby;
	}	
	else
	{
	}

	return enPHandler_OK;
}

















