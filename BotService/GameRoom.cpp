/**
  	* @file GameRoom.cpp
	* @date 5:7:2010  13:45
	* @version Ver
	* @author calendar gongliang@modi.com
	* @brief 
	*
*/

#include "GameRoom.h"
MODI_GameRoom::MODI_GameRoom()
{

}

MODI_GameRoom::MODI_GameRoom(MODI_RoomInfo* pRoomInfo)
:MODI_Room(pRoomInfo)
{
    
}

MODI_GameRoom::~MODI_GameRoom()
{

}

bool MODI_GameRoom::StartGame()
{
	MODI_Room::StartGame();

	return true;
}

void MODI_GameRoom::SendOneSentenceResult(int sentence, unsigned int score,unsigned char type,
                                          int comboNum,float exact,float second)
{
  //  MODI_GameNetHandler::GetInstance()->SendOneSentenceResult(sentence,score,type,comboNum,exact,second);
}

void MODI_GameRoom::SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever)
{
   // MODI_GameNetHandler::GetInstance()->SendOneKeyResult(score,type,comboNum,exact,time,ftotalFever);
}

void MODI_GameRoom::SendMicInput(bool input, float time)
{
  //  MODI_GameNetHandler::GetInstance()->SendMicInput(input,time);
}

void MODI_GameRoom::SendFeverTime(float time)
{
   // MODI_GameNetHandler::GetInstance()->SendFeverTime(time);
}

void MODI_GameRoom::SendFeverTimeOver(float time)
{
    //MODI_GameNetHandler::GetInstance()->SendFeverTimeOver(time);

	//
	//const char* pSoundName = MODI_ItemSystem::GetInstance()->GetSoundPath(6);
//	MODI_AudioSystem::GetInstance()->PlaySound(pSoundName, false);
}

void MODI_GameRoom::SendGameOver()
{
//     MODI_Player* pLocalPlayer = MODI_GameNetHandler::GetInstance()->GetPlayer();
//     if(pLocalPlayer && !pLocalPlayer->IsPlayer())
//         MODI_GameNetHandler::GetInstance()->SendGameOver();
}

//////////////////////////////////////////////////////////////////////////
MODI_SingleRoom::MODI_SingleRoom()
{
    Init();
}

MODI_SingleRoom::MODI_SingleRoom(MODI_RoomInfo* pRoomInfo)
:MODI_Room(pRoomInfo)
{
    Init();
}

bool MODI_SingleRoom::Init()
{
    Reset();
    return true;
}

void MODI_SingleRoom::Reset()
{
	m_totalScore = 0;
	m_SenctenceCount = 0;
	m_Exact = 0;

	for (int i=0;i<SCORE_TYPE_LENGTH;++i)
		m_Evaluation[i] = 0;
}

MODI_SingleRoom::~MODI_SingleRoom()
{

}

bool MODI_SingleRoom::StartGame()
{
	MODI_Room::StartGame();
	return true;
}

void MODI_SingleRoom::SendOneSentenceResult(int sentence, unsigned int score,unsigned char type,
                                            int comboNum,float exact,float second)
{
    //统计
    m_SenctenceCount = sentence;
    m_totalScore += score;


    
    m_Exact = exact;
	if (m_Exact>=0.99f)
        m_Exact=1.0f;
}

void MODI_SingleRoom::SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever)
{
	//统计
	m_totalScore += score;

	m_Exact = exact;
	if (m_Exact>=0.99f)
		m_Exact=1.0f;
}

void MODI_SingleRoom::SendGameOver()
{
//     MODI_Player* pLocalPlayer = MODI_GameNetHandler::GetInstance()->GetPlayer();
//     if (!pLocalPlayer)
//         return;
// 
//     MODI_GameResult gameresult;
//     gameresult.m_nClientID = pLocalPlayer->GetPlayerId();
//     gameresult.m_byMiss = m_Evaluation[SCORE_TYPE_MISS];
//     gameresult.m_byBad = m_Evaluation[SCORE_TYPE_BAD];
//     gameresult.m_byGood = m_Evaluation[SCORE_TYPE_GOOD];
//     gameresult.m_byCool = m_Evaluation[SCORE_TYPE_COOL];
//     gameresult.m_byPefect = m_Evaluation[SCORE_TYPE_PERFECT];
//     gameresult.m_byCombo = m_Evaluation[SCORE_TYPE_CONMBO];
// 
//     gameresult.m_byLevel = pLocalPlayer->GetPlayerLevel();
//     //if (m_SenctenceCount>0)
//     //    gameresult.m_fExact = m_Exact/m_SenctenceCount;
//     //else
//         gameresult.m_fExact = m_Exact;
// 
//     float fExacts[enEval_End - 1] = { 1.0f , 0.9f , 0.8f , 0.7f , 0.6f ,0.5f ,0.4f, 0.3f,0.2f,0.1f };
//     unsigned char byEvalutaion[enEval_End - 1] = { enEval_SS_Type,  enEval_S_Type,
//                                                    enEval_SDEC_Type,enEval_A_Type,
//                                                    enEval_ADEC_Type,enEval_B_Type,
//                                                    enEval_C_Type,   enEval_D_Type,
//                                                    enEval_E_Type,   enEval_F_Type};
// 
//     for( int iExact = 0; iExact < enEval_End - 1; iExact++ )
//     {
//         if( gameresult.m_fExact <= fExacts[iExact] && 
//             gameresult.m_fExact > fExacts[iExact + 1] )
//         {
//             gameresult.m_byEvaluation = byEvalutaion[iExact];
//             break;
//         }
//     }
//     
//     gameresult.m_nTotalScore = m_totalScore;
//     gameresult.m_nMoney = 0;
//     gameresult.m_nExp = 0;
// 
// 	Reset();
// 
//     MODI_GameResultMgr::GetInstance()->AddResult(&gameresult);
//     MODI_UISystem::GetInstance()->executeScriptFile("Karaoke_ExerciseOver");//通知脚本，音乐结束
//     //将结算结果统计
// 	MODI_PREffect *prEff = MODI_PREffectMgr::GetInstance()->GetPREffect("KaraokeRoot/PlaySettlement");
// 	if ( prEff )
// 	{
// 		prEff->Start();
// 	}
//     //清空结算结果
//     MODI_GameResultMgr::GetInstance()->ClearAllResult();
}

unsigned int MODI_SingleRoom::GetPlayerNumInPlaying()
{
	return 1;
}
