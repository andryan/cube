/**
 * @file   ScriptManager.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 11:48:19 2011
 * 
 * @brief  脚本管理
 * 
 * 
 */

#ifndef _MD_SCRIPT_MANAGER_H
#define _MD_SCRIPT_MANAGER_H

#include "AssertEx.h"
#include "Global.h"
#include "Script.h"

struct MODI_DIRTask
{
	static const std::string m_strDir;
};

struct MODI_DIREnter
{
	static const std::string m_strDir;
};


struct MODI_DIRTime
{
	static const std::string m_strDir;
};
	


template<class DIR>
class MODI_TriggerType
{
 public:
	MODI_TriggerType(const WORD script_id, const WORD process_id = 0):
		m_wdScriptID(script_id), m_wdProcessID(process_id)
	{
		
	}

	const WORD  & GetScriptID()
	{
		return m_wdScriptID;
	}

	const WORD &  GetProcessID()
	{
		return m_wdProcessID;
	}

	static const char * GetScriptPath()
	{
		return DIR::m_strDir.c_str();
	}

 private:
	/// 脚本id
	WORD m_wdScriptID;

	/// 过程id
	WORD m_wdProcessID;
};


/// 任务
typedef MODI_TriggerType<MODI_DIRTask> MODI_TriggerTask;

/// 登陆时
typedef MODI_TriggerType<MODI_DIREnter> MODI_TriggerEnter;

/// 时间
typedef MODI_TriggerType<MODI_DIRTime> MODI_TriggerTime;


/**
 * @brief 脚本管理
 * 
 */
template<typename trigger_type>
class MODI_ScriptManager
{
 public:
	typedef MODI_ScriptManager<trigger_type> myself;
	typedef std::map<WORD, MODI_Script * > defScriptMap;
	typedef std::map<WORD, MODI_Script * >::iterator defScriptMapIter;
	typedef std::map<WORD, MODI_Script * >::value_type defScriptMapValue;
	
	static myself & GetInstance()
	{
		if(m_pInstance == NULL)
		{
			m_pInstance = new myself;
		}
		return * m_pInstance;
	}

	static void DelInstance()
	{
		if(m_pInstance)
		{			
			delete m_pInstance;
		}
		m_pInstance = NULL;
	}

	/** 
	 * @brief 脚本入口
	 * 
	 * @param p_client 执行脚本的用户
	 * @param trigger_type 触发的类型
	 * 
	 */
	void Execute(MODI_BotUser * p_client, trigger_type & trigger_info);


	/** 
	 * @brief 加载脚本
	 * 
	 * @param files 脚本文件
	 * 
	 */
	bool LoadScript(const char * files)
	{
		std::string in_files = files;
		std::string file_path = trigger_type::GetScriptPath() + in_files;
		MODI_Script * p_new_script = new MODI_Script(0);
		if(! p_new_script->Load(file_path.c_str()))
		{
			delete p_new_script;
			p_new_script = NULL;
			return false;
		}
	
		AddScript(p_new_script);
		return true;
	}

	/** 
	 * @brief 删除
	 * 
	 */
	void Unload()
	{
		MODI_Script * p_script = NULL;
		m_stRWLock.wrlock();
		defScriptMapIter iter = m_stScriptMap.begin();
		for(; iter != m_stScriptMap.end(); iter++)
		{
			p_script = iter->second;
			if(p_script)
			{
				p_script->Unload();
				delete p_script;
			}
		}
		m_stScriptMap.clear();
		m_stRWLock.unlock();
	}

	
 private:
	/** 
	 * @brief 增加一个脚本
	 * 
	 * @param p_script 脚本
	 */
	bool AddScript(MODI_Script * p_script)
	{
		std::pair<defScriptMapIter, bool > ret_code;
		m_stRWLock.wrlock();
		ret_code = m_stScriptMap.insert(defScriptMapValue(p_script->GetScriptID(), p_script));
		m_stRWLock.unlock();
	
#ifdef _HRX_DEBUG	
		if(ret_code.second == false)
		{
			Global::logger->fatal("[load_script] script load faild <id=%u>", p_script->GetScriptID());
			MODI_ASSERT(0);
		}
#endif
		return ret_code.second;
	}


	/** 
	 * @brief 获取一个脚本
	 * 
	 * @param script_id 脚本id
	 * 
	 * @return 失败返回null
	 *
	 */
	MODI_Script * GetScript(const WORD & script_id)
	{
		MODI_Script * p_script = NULL;
		m_stRWLock.wrlock();
		defScriptMapIter iter = m_stScriptMap.find(script_id);
		if(iter != m_stScriptMap.end())
		{
			p_script = iter->second;
		}
		m_stRWLock.unlock();
		return p_script;
	}

	
	MODI_ScriptManager(){}
	~MODI_ScriptManager(){}
	
	static myself * m_pInstance;

	defScriptMap m_stScriptMap;

	MODI_RWLock m_stRWLock;
};

template<class trigger_type>
MODI_ScriptManager<trigger_type> * MODI_ScriptManager<trigger_type>::m_pInstance = NULL;


typedef MODI_ScriptManager<MODI_TriggerTime> MODI_TimeScriptManager;
typedef MODI_ScriptManager<MODI_TriggerTask> MODI_TaskScriptManager;
typedef MODI_ScriptManager<MODI_TriggerEnter> MODI_EnterScriptManager;


/** 
 * @brief 脚本执行
 * 
 * @param p_client 执行者 
 * @param trigger_info 执行脚本信息
 *
 */
template<class trigger_type>
void MODI_ScriptManager<trigger_type>::Execute(MODI_BotUser * p_client,  trigger_type & trigger_info)
{
	if(! p_client)
	{
		Global::logger->error("[exec_script] execute script client=null <script=%u>", trigger_info.GetScriptID());
		MODI_ASSERT(0);
		return;
	}

	MODI_Script * p_script = MODI_ScriptManager<trigger_type>::GetInstance().GetScript(trigger_info.GetScriptID());
	if(p_script)
	{
		p_script->Execute(p_client, trigger_info.GetProcessID());
	}
	else
	{
		Global::logger->fatal("[exec_script] unable find a script <id=%u>", trigger_info.GetScriptID());
		MODI_ASSERT(0);
	}
}


#endif
