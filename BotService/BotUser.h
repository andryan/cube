/**
 * @file   BotUser.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 14:38:20 2011
 * 
 * @brief  机器人
 * 
 * 
 */
#ifndef _MODI_BOT_USER__
#define _MODi_BOT_USER__

#include "UserTick.h"
#include "Global.h"
#include "RClientTask.h"
#include "BotUserVar.h"
#include "CommandQueue.h"
#include "RoomMgr.h"
#include "GameRoom.h"
enum enBotUserState
{
	enBotUserState_Login=0, //登录中
	enBotUserState_Lobby,	//大厅中
	enBotUserState_Room,	//房间中
	enBotUserState_Shop,	//商店中
};

/**
 * @brief 机器人实例
 * 
 */
class MODI_BotUser: public MODI_RClientTask, public MODI_CmdParse
{
 public:
	MODI_BotUser(const char * name, const char * server_ip, const WORD & port): MODI_RClientTask(name, server_ip, port)
	{
		m_blIsLogin = false;
		m_blIsRecvGatewayInfo = false;
		m_CharId = 0;
		m_eState = enBotUserState_Login;
		m_pRoomMgr = new MODI_RoomMgr();
		m_nTicks =0;
	}

	WORD		m_nTicks;
	void	RefreshRoomList(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

	void	AddRoom(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

	
	void 	DelRoom(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

	/// 初始化
	bool Init(const DWORD & account_id);

	const bool IsConnected()
	{
		bool is_connect = false;
		is_connect = MODI_RClientTask::IsConnected();
		return is_connect;
	}

	const void ReConnect()
	{
		MODI_RClientTask::ReadyReConnect();
	}

	/** 
	 * @brief 发送命令
	 * 
	 * @param pt_null_cmd 要发送的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size);

	/** 
	 * @brief 命令处理
	 * 
	 * @param pt_null_cmd 要处理的命令
	 * @param cmd_size 命令大小
	 * 
	 * @return 
	 */
	bool CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size);
	

	/** 
	 * @brief 队列内的命令
	 * 
	 * @param pt_null_cmd 获取的命令
	 * @param dwCmdLen 命令长度
	 * 
	 * @return 成功true,失败false
	 *
	 */
	bool CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen);

	void UpDate();

	const DWORD & GetAccountID()
	{
		return m_dwAccountID;
	}

	const char * GetAccountName()
	{
		return m_strAccountName.c_str();
	}

	bool IsALogin()
	{
		return m_blIsLogin;
	}

	void SetALogin()
	{
		m_blIsLogin = true;
	}

	MODI_BotUserVarMgr m_stVarManager;

	enBotUserState 	m_eState;
	
	void ClientFinal();

	MODI_UserTick * m_pUserTick;

	/// 账号信息
	std::string m_strAccountName;
	
	/// 登陆服务器地址信息
	std::string m_strLoginIP;
	WORD m_wdLoginPort;

	/// 网关服务器地址信息
	std::string m_strGateIP;
	WORD m_wdGatePort;
	MODI_LoginKey m_LoginKey;
	DWORD m_dwAccountID;
	bool m_blIsRecvGatewayInfo;
	MODI_CHARID m_CharId;
	
	MODI_RoomMgr	*m_pRoomMgr;

	bool m_blIsLogin;

};

#endif
