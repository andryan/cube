/**
 * @file   MakeManager.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Wed Mar 23 11:53:46 2011
 * 
 * @brief  制造管理
 * 
 * 
 */

#include "MakeManager.h"

/** 
 * @brief 条件注册
 * 
 */
template<>
void MODI_MakeConditionManager::RegCondition()
{
	RegMake("var", new MODI_ConditionMake<MODI_VarCondition>);
	RegMake("isconnect_loginservice", new MODI_ConditionMake<MODI_IsConnectLogin_Condition>);
	RegMake("rand",new MODI_ConditionMake<MODI_Random_Condition>);
	RegMake("state",new MODI_ConditionMake<MODI_State_Condition>);
	RegMake("roomnum",new MODI_ConditionMake<MODI_RoomNum_Condition>);
	RegMake("ticks", new MODI_ConditionMake<MODI_Ticks_Condition>);
}



/** 
 * @brief 动作注册
 * 
 */
template<>
void MODI_MakeActionManager::RegAction()
{
	RegMake("var", new MODI_ActionMake<MODI_VarAction>);
	RegMake("login", new MODI_ActionMake<MODI_Trigger_Login_Action>);
	RegMake("speak",new MODI_ActionMake<MODI_Speak_Action>);
	RegMake("randenter", new MODI_ActionMake<MODI_RandJoin_Action>);
	RegMake("leaveroom",new MODI_ActionMake<MODI_LeaveRoom_Action>);
	RegMake("entershop", new MODI_ActionMake<MODI_EnterShop_Action>);
	RegMake("leaveshop" , new MODI_ActionMake<MODI_LeaveShop_Action>);
	RegMake("createroom", new MODI_ActionMake<MODI_CreateRoom_Action>);
}
