/********************************************************************
	created:	2010/01/07
	created:	7:1:2010   14:31
	filename: 	f:\subversion\cube\trunk\Program\Client\cube\Engine\Room\Room.cpp
	file path:	f:\subversion\cube\trunk\Program\Client\cube\Engine\Room
	file base:	Room
	file ext:	cpp
	author:		calendar
	
	purpose:	
*********************************************************************/
#include "Room.h"
#include "string.h"

MODI_Room::MODI_Room()
{
    //m_RoomInfo需要初始化
	m_iPlayerCursor = 0;
    m_bListen = false;
	m_blNPCSinging = false;
	m_blTutorialing = false;
	m_blMicTesting = false;
	m_bTeamOk = false;
}

MODI_Room::MODI_Room(MODI_RoomInfo* pRoomInfo)
{
    if(pRoomInfo)
    {
        m_RoomInfo = *pRoomInfo;
    }
	///
	m_iPlayerCursor = 0;
    m_bListen = false;
	m_blNPCSinging = false;
	m_blTutorialing = false;
	m_blMicTesting = false;
	m_bTeamOk = false;
}

MODI_Room::~MODI_Room()
{
    
}

void MODI_Room::SetMapId(defMapID mapid)
{
    //要从配置信息中确认是否越界,存在该地图
    if(m_RoomInfo.m_detailNormalRoom.m_nMapId != mapid)
    {
        m_RoomInfo.m_detailNormalRoom.m_nMapId = mapid;
        //通知切换地图图标
    }
}

unsigned char MODI_Room::GetMapId()
{
    return m_RoomInfo.m_detailNormalRoom.m_nMapId;
}

void MODI_Room::SetMusicId(defMusicID musicid,enMusicFlag flag,enMusicSType type,bool random,enMusicTime timetype)
{
    m_RoomInfo.m_detailNormalRoom.m_nMusicId = musicid;
	m_RoomInfo.m_detailNormalRoom.m_flag = flag;
	m_RoomInfo.m_detailNormalRoom.m_songType = type;
	m_RoomInfo.m_detailNormalRoom.m_bRandomMusic = random;
	m_RoomInfo.m_detailNormalRoom.m_timeType = timetype;
}

void MODI_Room::SetMasterId(MODI_GUID masterid)
{
    m_RoomInfo.m_masterID = masterid;
}

void MODI_Room::SetRoomName(const char* pRoomName)
{   
    int length = strlen(pRoomName);
	if (0 >= length )
	{
		return ;
	}
    if (length > ROOM_NAME_MAX_LEN)
    {
        assert(false && "set room name failed");
        return ;
    }
	
    strcpy(m_RoomInfo.m_cstrRoomName,pRoomName);
}

unsigned long MODI_Room::GetMusicId()
{
    return m_RoomInfo.m_detailNormalRoom.m_nMusicId;
}

enMusicFlag MODI_Room::GetMusicFlag()
{
    return m_RoomInfo.m_detailNormalRoom.m_flag;
}

unsigned short MODI_Room::GetMusicType()
{
	return m_RoomInfo.m_detailNormalRoom.m_songType;
}

unsigned short MODI_Room::GetMusicTimeType()
{
	return m_RoomInfo.m_detailNormalRoom.m_timeType;
}

void MODI_Room::SetListen(bool bIsListen)
{
    m_bListen = bIsListen;
}

bool MODI_Room::IsListen()
{
    return m_bListen;
}

unsigned char MODI_Room::GetRoomId()
{
    return m_RoomInfo.m_roomID;
}

ROOM_TYPE MODI_Room::GetRoomType()
{
    return (ROOM_TYPE)RoomHelpFuns::GetRoomMainType(m_RoomInfo.m_roomType);
}

const char* MODI_Room::GetRoomName()
{
    return m_RoomInfo.m_cstrRoomName;
}


bool MODI_Room::IsHavePlayer(unsigned short playerpos)
{

    return false;
}

int MODI_Room::IsPlayer(const MODI_GUID& playerid)
{
    for(int i =0;i<MAX_PLAYER_NUM;++i)
    {
        if(playerid == m_Player[i].playerid)
        {
            return 0;//参赛玩家
        }
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        if(playerid == m_Audience[j].playerid)
        {
            return 1;//旁观
        }
    }

    return -1;//不在房间里
}

bool MODI_Room::IsInRoom(const char* playername)
{

    return false;
}

bool MODI_Room::IsPlayerPosReady(unsigned short playerpos)
{
   /* if (playerpos>= 0&& playerpos < MAX_PLAYER_NUM)
    {
        return m_Player[playerpos].bReady;
    }*/

    return false;
}

bool MODI_Room::IsPlayerPosClosed(unsigned short playerpos)
{
    /*if (playerpos>= 0&& playerpos < MAX_PLAYER_NUM)
    {
        return m_Player[playerpos].bIsClosed;
    }*/

    return false;
}

bool MODI_Room::IsListenerPosClosed(unsigned short playerpos)
{
    /*if (playerpos>= 0&& playerpos < MAX_AUDIENCE_NUM)
    {
        return m_Audience[playerpos].bIsClosed;
    }*/

    return false;
}

bool MODI_Room::IsLocked()
{
    return RoomHelpFuns::IsHasPassword(m_RoomInfo.m_roomType);
}

bool MODI_Room::IsForbiddenListen()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战  //false不关闭，true关闭
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return bOffSpectator;
}

void MODI_Room::SetLock(bool bLock)
{
	RoomHelpFuns::SetHasPassword( m_RoomInfo.m_roomType ,  bLock );
}

void MODI_Room::SetForbiddenListen(bool bIsForbiddenListen)
{
	m_RoomInfo.m_detailNormalRoom.m_nStateMask = RoomHelpFuns::EncodeRoomState(GetMaxPlayerNum(), GetPlayerNumInPlaying(),
		GetMaxListenerNum(), GetListenerNumInPlaying(),
		BoyPlayerNum(), bIsForbiddenListen);
}


int MODI_Room::GetMaxPlayerNum()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nMaxPlay;
}

int MODI_Room::GetPlayerNumInLobby()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nPlayNum;
}

unsigned int MODI_Room::GetPlayerNumInPlaying()
{
    unsigned int playernum = 0;
    for(int i=0;i<MAX_PLAYER_NUM;++i)
    {
        if(m_Player[i].playerid != INVAILD_GUID)
        {
            playernum ++;
        }
    }

    return playernum;
}

unsigned int MODI_Room::GetListenerNumInPlaying()
{
    unsigned int listenernum = 0;
    for(int i=0;i<MAX_AUDIENCE_NUM;++i)
    {
        if(m_Audience[i].playerid != INVAILD_GUID)
        {
            listenernum++;
        }
    }

    return listenernum;
}

int MODI_Room::BoyPlayerNum()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nBoys;
}

int MODI_Room::GirlPlayerNum()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nPlayNum - nBoys;
}

int MODI_Room::GetListenerNumInLobby()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nSpectatorNum;
}

int MODI_Room::GetMaxListenerNum()
{
    unsigned char  nMaxPlay; //房间最大参战玩家个数
    unsigned char  nPlayNum;//房间当前参战玩家个数
    unsigned char  nMaxSpectatorNum;//房间最大观战玩家个数
    unsigned char  nSpectatorNum;//房间当前观战玩家个数
    unsigned char  nBoys;//房间当前男孩个数
    bool bOffSpectator;//房间是否允许观战
    RoomHelpFuns::DecodeRoomState(m_RoomInfo.m_detailNormalRoom.m_nStateMask,nMaxPlay,
        nPlayNum,nMaxSpectatorNum,nSpectatorNum,nBoys,bOffSpectator);

    return nMaxSpectatorNum;
}

MODI_GUID MODI_Room::GetMasterId()
{
   return m_RoomInfo.m_masterID;
}

unsigned short MODI_Room::GetMasterPos()
{
    return GetPlayerPos(m_RoomInfo.m_masterID);
}

//占位操作
MODI_GUID MODI_Room::GetPlayerGUIDByPos(unsigned short playerpos)
{
    /*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
        return INVAILD_GUID;*/

    return m_Player[playerpos].playerid;
}

//队伍占位排序
void MODI_Room::TeamSort()
{
 	//排序
 	unsigned char _teamType[3];
 	_teamType[0] = ROOM_TEAM_RED;
 	_teamType[1] = ROOM_TEAM_YELLOW;
 	_teamType[2] = ROOM_TEAM_BLUE;
 	unsigned short _index = 0;
 	for ( unsigned short i=0; i<3; i++)
 	{
 		for ( unsigned short j=0; j<MAX_PLAYER_NUM; j++ )
 		{
 			if ( m_Player[j].iTeamType == _teamType[i])
 			{
 				m_TeamPlayer[_index++] = m_Player[j];
 			}
 		}
 	}
 	PlayerPos invalidPlayer;
 	if ( _index < 6)
 	{
 		for ( unsigned short k=_index; k<6; k++ )
 		{
 			m_TeamPlayer[k] = invalidPlayer;
 		}
 	}
}

//按队伍排的占位信息
MODI_GUID MODI_Room::GetTeamPlayerGUIDByPos(unsigned short playerpos)
{
/*	if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
		return INVAILD_GUID;*/

	return m_TeamPlayer[playerpos].playerid;
}

MODI_GUID MODI_Room::GetPlayerGUIDByName(const char* pPlayerName)
{
/*	if (NULL == pPlayerName)
	{
		return INVAILD_GUID;
	}

	for(int i =0;i<MAX_PLAYER_NUM;++i)
	{
		MODI_GUID playerid = m_Player[i].playerid;
		if(!playerid.IsInvalid())
		{
			MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
			if (pPlayer)
			{
				const char* pName = pPlayer->GetPlayerName();
				if (strcmp(pPlayerName,pName) == 0)
				{
					return playerid;
				}
			}
		}
	}

	for(int i =0;i<MAX_AUDIENCE_NUM;++i)
	{
		MODI_GUID playerid = m_Audience[i].playerid;
		if(!playerid.IsInvalid())
		{
			MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
			if (pPlayer)
			{
				const char* pName = pPlayer->GetPlayerName();
				if (strcmp(pPlayerName,pName) == 0)
				{
					return playerid;
				}
			}
		}
	}*/

	return INVAILD_GUID;

}

void MODI_Room::SetPlayerGUIIDByPos(unsigned short playerpos,const MODI_GUID& playerid)
{
    /*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
        return ;*/

    m_Player[playerpos].playerid = playerid;
}

MODI_GUID MODI_Room::GetListenerGUIDByPos(unsigned short listenerpos)
{
    /*if(listenerpos <0 || listenerpos>= MAX_AUDIENCE_NUM)
        return INVAILD_GUID;*/

    return m_Audience[listenerpos].playerid;
}

void MODI_Room::SetListenerGUIDByPos(unsigned short listenerpos,const MODI_GUID& playerid)
{
    /*if(listenerpos <0 || listenerpos>= MAX_AUDIENCE_NUM)
        return ;*/

    m_Audience[listenerpos].playerid = playerid;
}

int MODI_Room::GetPlayerPos(const MODI_GUID& playerid)
{
   /* for (int i=0;i<MAX_PLAYER_NUM;++i)
    {
        if(m_Player[i].playerid == playerid)
            return i;
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        if (m_Audience[j].playerid == playerid)
            return j;
    }*/

    return -1;
}

void MODI_Room::OpenPlayerPos(unsigned short playerpos)
{
    /*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
        return;*/

    m_Player[playerpos].bIsClosed = false;
}

void MODI_Room::ClosePlayerPos(unsigned short playerpos)
{
    /*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
        return;*/

    m_Player[playerpos].bIsClosed = true;
}

void MODI_Room::ChangePlayerState(unsigned short playerpos,bool bReady)
{
    /*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
        return;*/

    m_Player[playerpos].bReady = bReady;
}

void MODI_Room::ChangePlayerTeam(unsigned short playerpos,ROOM_TEAM_TYPE iTeam)
{
	/*if(playerpos <0 || playerpos>= MAX_PLAYER_NUM)
		return;*/

	m_Player[playerpos].iTeamType = iTeam;
}

void MODI_Room::OpenListenerPos(unsigned short listenerpos)
{
    /*if(listenerpos <0 || listenerpos>= MAX_AUDIENCE_NUM)
        return;*/

    m_Audience[listenerpos].bIsClosed = false;
}

void MODI_Room::CloseListenerPos(unsigned short listenerpos)
{
    /*if(listenerpos <0 || listenerpos>= MAX_AUDIENCE_NUM)
        return;*/

    m_Audience[listenerpos].bIsClosed = true;
}

void MODI_Room::LeavePlayer(MODI_GUID playerid)
{
   /* for(int i =0;i<MAX_PLAYER_NUM;++i)
    {
        if(playerid == m_Player[i].playerid)
        {
            m_Player[i].playerid = 0ull;//清除该玩家的占位            
            return;
        }
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        if(playerid == m_Audience[j].playerid)
        {
            m_Audience[j].playerid = 0ull;//清除该玩家的占位
            return;
        }
    }*/
}

void MODI_Room::AddPlayer(unsigned short playerpos,MODI_GUID playerid)
{
    bool bIsPlayer = false;
    defRoomPosSolt pos = 100;
    RoleHelpFuns::DecodePosSolt(playerpos,bIsPlayer,pos);

/*    if(bIsPlayer)
    {
        if (pos>= 0&& pos < MAX_PLAYER_NUM)
            m_Player[pos].playerid = playerid;
    }
    else
    {
        if(pos >=0 && pos < MAX_AUDIENCE_NUM)
            m_Audience[pos].playerid = playerid;
    }    */
}

void MODI_Room::ChangePlayerPosition(bool bIsSrcPlayer, unsigned short playerpos,unsigned short listenerpos)
{
   /* if(bIsSrcPlayer)
    {
        if(playerpos <0 || playerpos >= MAX_PLAYER_NUM)
            return;

        if(listenerpos <0 || listenerpos >= MAX_AUDIENCE_NUM)
            return;

        m_Audience[listenerpos].bIsClosed = false;
        m_Audience[listenerpos].bReady = false;
        m_Audience[listenerpos].playerid = m_Player[playerpos].playerid;

        m_Player[playerpos].bIsClosed = false;
        m_Player[playerpos].bReady = false;
        m_Player[playerpos].playerid = INVAILD_GUID;

		MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(m_Audience[listenerpos].playerid);
		if (pPlayer)
			pPlayer->SetPlayerImagePos(false,listenerpos);
    }
    else
    {
        if(playerpos <0 || playerpos >= MAX_AUDIENCE_NUM)
            return;

        if(listenerpos <0 || listenerpos >= MAX_PLAYER_NUM)
            return;

        m_Player[listenerpos].bIsClosed = false;
        m_Player[listenerpos].bReady = false;
        m_Player[listenerpos].playerid = m_Audience[playerpos].playerid;

        m_Audience[playerpos].bIsClosed = false;
        m_Audience[playerpos].bReady = false;
        m_Audience[playerpos].playerid = INVAILD_GUID;

		MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(m_Player[listenerpos].playerid);
		if (pPlayer)
			pPlayer->SetPlayerImagePos(true,listenerpos);
    }*/
}

void MODI_Room::GetAllPlayerGUID(std::vector<MODI_GUID>& vecPlayerGUID)
{
    /*for(int i =0;i<MAX_PLAYER_NUM;++i)
    {
        if(m_Player[i].playerid != INVAILD_GUID)
        {
            vecPlayerGUID.push_back(m_Player[i].playerid);
        }
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        if(m_Audience[j].playerid != INVAILD_GUID)
        {
            vecPlayerGUID.push_back(m_Audience[j].playerid);
        }
    }*/
}

//----------------------------------------------------------------------------//
// 重置玩家获取游标
void MODI_Room::RewindPlayerCursor()
{
	m_iPlayerCursor = 0;
}
//获取下一个玩家ID
MODI_GUID MODI_Room::GetNextPlayer()
{
/*	if( m_iPlayerCursor == MAX_PLAYER_NUM ){ return INVAILD_GUID; }

	// 找到下一个有效的玩家
	while( (m_Player[m_iPlayerCursor].playerid == INVAILD_GUID)
		&& m_iPlayerCursor < MAX_PLAYER_NUM)
	{
		++m_iPlayerCursor;
	}

	if( m_iPlayerCursor == MAX_PLAYER_NUM )
    {
        return INVAILD_GUID; 
    }*/
	return m_Player[m_iPlayerCursor++].playerid;
}

bool MODI_Room::StartGame()
{
    //获取当前房间中的玩家数量，并取得该玩家对应的模型ID
   /* unsigned int PlayerNum = GetPlayerNumInPlaying();
    int EnterPos = 0;
    for (int i =0; i<MAX_PLAYER_NUM; ++i)
    {
        MODI_GUID playerid = GetPlayerGUIDByPos(i);
        if (playerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
            if (pPlayer)
            {
                float Xpos = 0.f,Ypos = 0.f,Zpos = 0.f;
                bool bGetSuccess = MODI_SceneMgr::GetInstance()->GetOccupyPoint(PlayerNum,EnterPos,Xpos,Ypos,Zpos);
                ++EnterPos;
                if(bGetSuccess)
                {
                    pPlayer->RemovePlayerEffect(enEffect_FerverTime);//测试代码
                    pPlayer->EnterGame(Xpos,Ypos,Zpos);
                }
                else
                {
                    g_OutputDeviceFile.Logf("player GetOccupyPoint failed roomplayernum: %d",PlayerNum);
                }
            }
        }
    }
    
    //隐藏旁观玩家
    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        MODI_GUID listenerid = GetListenerGUIDByPos(j);
        if (listenerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(listenerid);
            if (pPlayer)
            {
                float Xpos = -10000.f,Ypos = -10000.f,Zpos = -10000.f;
                pPlayer->RemovePlayerEffect(enEffect_FerverTime);
                pPlayer->EnterGame(Xpos,Ypos,Zpos);
                pPlayer->SetVisible(false);
            }
        }
    }*/

    return true;
}

bool MODI_Room::GameOver()
{
   /* for (int i =0; i<MAX_PLAYER_NUM; ++i)
    {
        MODI_GUID playerid = GetPlayerGUIDByPos(i);
        if (playerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
            if (pPlayer)
                pPlayer->LeaveGame();
        }
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        MODI_GUID listenerid = GetListenerGUIDByPos(j);
        if (listenerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(listenerid);
            if (pPlayer)
            {
                pPlayer->LeaveGame();
                pPlayer->SetVisible(true);
            }
        }
    }

	for ( unsigned int i = 0; i < MAX_PLAYER_NUM ; ++i)
	{
		m_Player[i].bReady = false;
	}*/

	return true;
}

bool MODI_Room::LeaveRoom()
{
   /* for (int i =0; i<MAX_PLAYER_NUM; ++i)
    {
        MODI_GUID playerid = GetPlayerGUIDByPos(i);
        if (playerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
            if (pPlayer)
            {
                pPlayer->LeaveRoom();
                //SetPlayerGUIIDByPos(i,INVAILD_GUID);
            }
        }
    }

    for (int j=0;j<MAX_AUDIENCE_NUM;++j)
    {
        MODI_GUID listenerid = GetListenerGUIDByPos(j);
        if (listenerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(listenerid);
            if (pPlayer)
            {
                pPlayer->LeaveRoom();
                //SetListenerGUIDByPos(j,INVAILD_GUID);
            }
        }
    } */

    return true;
}

void MODI_Room::PlayCamera(const char *lensType, const char *lensName)
{
    /*MODI_CameraMgr::MODI_Camera *pCamera = MODI_CameraMgr::GetInstance()->Get(MODI_CameraMgr::GetInstance()->GetMainCamera());
    if ( NULL != pCamera )
        pCamera->SwitchLens(lensType,lensName);*/
}

//播放缺省角色动作
void MODI_Room::PlayDefaultActorMotion()
{
    /*for (int i=0; i<MAX_PLAYER_NUM; ++i)
    {
        MODI_GUID playerid = GetPlayerGUIDByPos(i);
        if (playerid != INVAILD_GUID)
        {
            MODI_Player* pPlayer = MODI_PlayerMgr::GetInstance()->GetPlayer(playerid);
            if (pPlayer != NULL)
            {
                unsigned char sex = pPlayer->GetPlayerGender();
				pPlayer->PlayMotion(sex?MODI_Player::s_Default_M.c_str():MODI_Player::s_Default_F.c_str(), true);
            }
        }
    }*/
}

void MODI_Room::SendOneSentenceResult(int sentence,unsigned int score,unsigned char type,
                                      int comboNum,float exact,float second)
{
    
}

void MODI_Room::SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever)
{

}

void MODI_Room::SendMicInput(bool input, float time)
{
    
}

void MODI_Room::SendFeverTime(float time)
{

}

void MODI_Room::SendFeverTimeOver(float time)
{
	//
	//const char* pSoundName = MODI_ItemSystem::GetInstance()->GetSoundPath(6);
//	MODI_AudioSystem::GetInstance()->PlaySound(pSoundName, false);
}

void MODI_Room::SendGameOver()
{

}

unsigned char MODI_Room::GetGameMode()
{
//	return RoomHelpFuns::GetRoomSubType(m_RoomInfo.m_roomType);
	return 0;
}

void MODI_Room::SetGameMode( unsigned char gameMode )
{
/*	m_RoomInfo.m_roomType = RoomHelpFuns::MakeRoomType(
		RoomHelpFuns::IsHasPassword(m_RoomInfo.m_roomType),
		RoomHelpFuns::GetRoomMainType(m_RoomInfo.m_roomType),
		gameMode);*/
}

void MODI_Room::SetNPCSinging(bool npcsinging)
{
	m_blNPCSinging = npcsinging;
}

void MODI_Room::SetTutorialing(bool tutorialing)
{
	m_blTutorialing = tutorialing;
}

void MODI_Room::SetMicTesting(bool mictesting)
{
	m_blMicTesting = mictesting;
}

ROOM_SUB_TYPE MODI_Room::GetRoomSubType()
{
	//return (ROOM_SUB_TYPE)RoomHelpFuns::GetRoomSubType(m_RoomInfo.m_roomType);	
	return ROOM_STYPE_NORMAL;
}

unsigned char MODI_Room::GetTeamType( const MODI_GUID& playerid )
{
/*	for(int i =0;i<MAX_PLAYER_NUM;++i)
	{
		if(playerid == m_Player[i].playerid)
		{
			return m_Player[i].iTeamType;
		}
	}*/
	return 0;//找不到
}

unsigned char MODI_Room::GetTeamTypeByPos(unsigned short ipos)
{
	return m_Player[ipos].iTeamType;
}
void MODI_Room::SetPlayerTeam(unsigned char* playerTeam)
{
	/*unsigned short iRed = 0;
	unsigned short iYellow = 0;
	unsigned short iBlue = 0;

	for ( unsigned char i=0;i<MAX_PLAYER_NUM;i++)
	{
		m_Player[i].iTeamType = playerTeam[i];
		
		if ( m_Player[i].iTeamType == ROOM_TEAM_RED )
		{
			iRed++;
		}
		else if ( m_Player[i].iTeamType == ROOM_TEAM_YELLOW )
		{
			iYellow++;
		}
		else if ( m_Player[i].iTeamType == ROOM_TEAM_BLUE )
		{
			iBlue++;
		}
	}
	
	unsigned short icolorSum = iRed + iYellow + iBlue;

	if ( icolorSum == 0)
	{
		m_bTeamOk = false;
	}
	else if ( iRed == iYellow && iYellow == iBlue)
	{
		m_bTeamOk = true;
	}
	else if ( iRed == 0 || iYellow == 0 || iBlue == 0)
	{
		if ( iRed+iYellow == 0 
			|| iYellow+iBlue == 0
			|| iBlue+iRed == 0 )
		{
			m_bTeamOk = false;
		}
		else if (icolorSum%2 == 0 
			&& ( iRed == iYellow || iYellow == iBlue || iBlue == iRed))
		{
			m_bTeamOk = true;
		}
		else
		{
			m_bTeamOk = false;
		}
	}
	else
	{
		m_bTeamOk = false;
	}*/

}

bool MODI_Room::IsTeamOk()
{
	return m_bTeamOk;
	//return true;
}
