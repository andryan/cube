/**
 * @file   Condition.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:00:30 2011
 * 
 * @brief  脚本执行条件
 * 
 * 
 */

#ifndef _MD_CONDITION_H
#define _MD_CONDITION_H

#include "Global.h"
class MODI_BotUser;

/**
 * @brief 条件
 * 
 */
class MODI_Condition
{
 public:
	virtual ~MODI_Condition(){}
	virtual bool Init(const char * con_line) = 0;
	virtual bool is_valid(MODI_BotUser * p_client) = 0;
};


/** 
 * @brief 用户变量
 * 
 */
class MODI_VarCondition: public MODI_Condition
{
 public:
	bool Init(const char * con_line);
	bool is_valid(MODI_BotUser * p_client);
 private:
	std::string m_strName;
	std::string m_strAttribute;
	WORD m_wdCondition;
	std::string m_strOp;
};


/** 
 * @brief 用户是否连接到loginservice
 * 
 */
class MODI_IsConnectLogin_Condition: public MODI_Condition
{
 public:
	bool Init(const char * con_line);
	bool is_valid(MODI_BotUser * p_client);
};

/**
 * @brief 概率计算
 *
 */
class MODI_Random_Condition : public MODI_Condition
{
	public:
		bool	Init(const char * con_line);
		bool	is_valid(MODI_BotUser * p_client);
	private:
		WORD	m_nRands;

};

/**
 * @brief 用户当前的状态 
 *
 */

class MODI_State_Condition : public MODI_Condition
{
	public:
		bool	Init(const char * con_line);
		bool	is_valid(MODI_BotUser * p_client);
	private:
		std::string 	m_sCondition;
		std::string  m_sOpt;
};


/**
 * @brief 当前的房间总数 
 *
 */

class MODI_RoomNum_Condition: public MODI_Condition
{ 
public:
	bool	Init(const char * con_line);
	bool	is_valid(MODI_BotUser * p_client);
private:
	int	m_nNum;
	std::string	m_sOpt;
};


/**
 * @brief 当前的房间总数 
 *
 */
class MODI_Ticks_Condition : public MODI_Condition
{
	public:
		bool	Init(const char * con_line);
		bool	is_valid(MODI_BotUser * p_client);

	private:
		int	 m_nNum;
		std::string	m_sOpt;

};

#endif
