/**
 * @file   Condition.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 16:59:06 2011
 * 
 * @brief  条件实现
 * 
 * 
 */

#include "Global.h"
#include "SplitString.h"
#include "Condition.h"
#include "BotUser.h"
#include "BotUserVar.h"

/** 
 * @brief 条件变量初始化
 * 
 * @param con_line 内容
 * 
 * @return 成功true
 *
 */
bool MODI_VarCondition::Init(const char * con_line)
{
	if(! con_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(con_line);
	m_strName = split["name"];
	m_strAttribute = split["attribute"];
	m_strOp = split["op"];
	m_wdCondition = atoi(split["condition"]);
	return true;
}


/** 
 * @brief 条件变量判断 
 * 
 * @param p_client 该用户的变量
 * 
 * @return 有效true,无效false
 *
 */
bool MODI_VarCondition::is_valid(MODI_BotUser * p_client)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}

	if(m_strName == "" || m_strOp == "")
	{
		Global::logger->fatal("[check_condition] condition isvalid <name=%s,op=%s>", m_strName.c_str(), m_strOp.c_str());
		MODI_ASSERT(0);
		return false;
	}

	MODI_BotUserVar * p_var = p_client->m_stVarManager.GetVar(m_strName.c_str());
	
	if(! p_var)
	{
		p_var = new MODI_BotUserVar(m_strName.c_str());
		if(p_var->Init(p_client, m_strAttribute.c_str()))
		{
			if(! p_client->m_stVarManager.AddVar(p_var))
			{
				delete p_var;
				p_var = NULL;
				MODI_ASSERT(0);
			}
			else
			{
				p_var->SaveToDB();
			}
		}
		else
		{
			delete p_var;
			p_var = NULL;
			MODI_ASSERT(0);
		}
	}

	if(!p_var)
	{
		MODI_ASSERT(0);
		return false;
	}
#ifdef _VAR_DEBUG
	//	Global::logger->debug("[check_condition] check condition <name=%s,op=%s,condition=%u,value=%u>", p_var->GetName(),
	//				  m_strOp.c_str(), m_wdCondition, p_var->GetValue());
#endif	
	if(m_strOp == "great")
	{
		return   p_var->GetValue() > m_wdCondition;
	}
	else if(m_strOp == "less")
	{
		return p_var->GetValue() < m_wdCondition;
	}
	else if(m_strOp == "equ")
	{
		return m_wdCondition == p_var->GetValue();
	}
	else if(m_strOp == "diff")
	{
		return m_wdCondition != p_var->GetValue();
	}
	else
	{
		return false;
	}
}



bool MODI_IsConnectLogin_Condition::Init(const char * con_line)
{
	return true;
}


/** 
 * @brief 条件变量判断 
 * 
 * @param p_client 该用户的变量
 * 
 * @return 有效true,无效false
 *
 */
bool MODI_IsConnectLogin_Condition::is_valid(MODI_BotUser * p_client)
{
	if(! p_client)
	{
		return false;
	}
	
	return ( (p_client->IsALogin() == false) && (p_client->IsConnected()) );
}


/**
 * @brief random calculate
 *
 */

bool	MODI_Random_Condition::Init(const char * con_line)
{

	if(! con_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(con_line);

	m_nRands = atoi(split["rands"]);

	return true;
}


bool	MODI_Random_Condition::is_valid(MODI_BotUser * p_client)
{
	srand(time(NULL));	
	WORD	 i = rand()%1000+1;
	if( i > m_nRands)
		return false;
	return true;
}

bool	MODI_State_Condition::Init(const char * con_line)
{
	if(! con_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(con_line);

	m_sCondition = split["condition"];
	m_sOpt = split["opt"];
	return true;
}

bool	MODI_State_Condition::is_valid(MODI_BotUser * p_client)
{

	enBotUserState  _state=enBotUserState_Login;
	if( "" == m_sOpt  ||  "" == m_sCondition)
		return false;
	if( "login" == m_sCondition)
	{
		_state = enBotUserState_Login;	
	}
	else if( "lobby" == m_sCondition)
	{
		_state = enBotUserState_Lobby;
	}
	else if( "room" == m_sCondition )
	{
		_state = enBotUserState_Room;
	}
	else if( "shop" == m_sCondition)
	{
		_state = enBotUserState_Shop;
	}
	else
	{
	////	Unkown error	
	}	

	if( "equ" == m_sOpt)
	{
		return ( p_client->m_eState == _state);
	}
	else if( "none" == m_sOpt)
	{
		return (_state != p_client->m_eState);
	}
	else
	{
		return false;
	}
	
	return true;
}

bool MODI_RoomNum_Condition::Init(const char * con_line)
{
	if(! con_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(con_line);

	m_sOpt = split["opt"];
	m_nNum = atoi(split["num"]);

	return true;
}


bool MODI_RoomNum_Condition::is_valid(MODI_BotUser *p_client)
{
	if( m_sOpt == "less")
	{
		return (p_client->m_pRoomMgr->GetRoomCount()  < m_nNum);
	}
	else if( m_sOpt == "equ")
	{
		return (p_client->m_pRoomMgr->GetRoomCount()  == m_nNum);
	}
	else if( m_sOpt == "great")
	{
		return (p_client->m_pRoomMgr->GetRoomCount()  > m_nNum);
	}
	else
	{
		return false;
	}
}

bool	MODI_Ticks_Condition::Init(const char * con_line)
{
	if(! con_line)
		return false;
	
	MODI_SplitString split;
	split.InitXML(con_line);

	m_sOpt = split["opt"];
	m_nNum = atoi(split["num"]);

	return true;

}

bool	MODI_Ticks_Condition::is_valid(MODI_BotUser * p_client)
{
	p_client->m_nTicks=p_client->m_nTicks%100;

	if( m_sOpt == "less")
	{
		return (p_client->m_nTicks  < m_nNum);
	}
	else if( m_sOpt == "equ")
	{
		return (p_client->m_nTicks  == m_nNum);
	}
	else if( m_sOpt == "great")
	{
		return (p_client->m_nTicks  > m_nNum);
	}
	else
	{
		return false;
	}

}
