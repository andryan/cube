/**
 * @file   Action.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 14:04:28 2011
 * 
 * @brief  脚本动作
 * 
 * 
 */

#ifndef _MD_ACTION_H
#define _MD_ACTION_H


#include "Global.h"
class MODI_BotUser;
//#include "BotUser.h"

/**
 * @brief 脚本动作
 * 
 */
class MODI_Action
{
 public:
	virtual ~MODI_Action(){}
	virtual bool Init(const char * action_line) = 0;
	virtual bool do_it(MODI_BotUser * p_client) = 0;
};

/** 
 * @brief 动作变量
 * 
 */
class MODI_VarAction: public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_BotUser * p_client);
 private:
	std::string m_strName;
	std::string m_strAttribute;
	WORD m_wdValue;
	std::string m_strOp;
};


/** 
 * @brief 触发机器人登陆
 * 
 */
class MODI_Trigger_Login_Action: public MODI_Action
{
 public:
	bool Init(const char * action_line);
	bool do_it(MODI_BotUser * p_client);
};

/**
 * @brief 随即说话
 *
 */

class	MODI_Speak_Action : public MODI_Action
{
	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);
	private:
		std::string	m_sContext;
		BYTE		m_nChannel;

};

/**
 * @brief 进入商城
 *
 */

class MODI_EnterShop_Action : public MODI_Action
{
	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);
	private:

};


/**
 * @brief 离开商城
 *
 */

class MODI_LeaveShop_Action : public MODI_Action
{
	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);
	private:

};


/**
 * @brief 随即进入房间 
 *
 */

class MODI_RandJoin_Action : public MODI_Action
{
	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);
	private:

};

/**
 * @brief 离开房间 
 *
 */
class	MODI_LeaveRoom_Action : public MODI_Action
{

	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);

	private:
};


/**
 * @brief 离开房间 
 *
 */

class 	MODI_CreateRoom_Action: public MODI_Action
{
	public:
		bool	Init(const char * action_line);
		bool	do_it(MODI_BotUser * p_client);

	private:
};
#endif



