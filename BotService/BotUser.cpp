/**
 * @file   BotUser.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 14:45:58 2011
 * 
 * @brief  机器人
 * 
 * 
 */

#include "protocol/c2ls_logincmd.h"
#include "protocol/c2gs.h"
#include "protocol/gamedefine.h"
#include "protocol/gamedefine2.h"
#include "GameHelpFun.h"
#include "md5.h"

#include "Share.h"
#include "HelpFuns.h"
#include "BotUser.h"
#include "BotGlobal.h"
#include "ScriptManager.h"
#include "c2gs_PackageHandler.h"

#include "Timer.h"

void	MODI_BotUser::RefreshRoomList(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	MODI_GS2C_Notify_Roomlist *pRoomlistpacket = (MODI_GS2C_Notify_Roomlist *)pt_null_cmd;
	if( pRoomlistpacket )
	{
		m_pRoomMgr->UpdateRoomInfoList(pRoomlistpacket);
	}
	std::cout<<" RefreshRoom list "<<m_pRoomMgr->GetRoomCount()<<std::endl;
}


void	MODI_BotUser::AddRoom(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	MODI_GS2C_Notify_AddRoom * padd = (MODI_GS2C_Notify_AddRoom *)pt_null_cmd;
	if( padd)
	{
		m_pRoomMgr->CreateRoom<MODI_GameRoom>(&padd->stRoom);
	}
	std::cout<<"Add room  room number = "<<m_pRoomMgr->GetRoomCount()<<std::endl;
}


void 	MODI_BotUser::DelRoom(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	MODI_GS2C_Notify_DelRoom * pdel =(MODI_GS2C_Notify_DelRoom *)pt_null_cmd;
	if( pdel)
	{
		m_pRoomMgr->DeleteRoom(pdel->roomID);
	}
	std::cout<<"Delete a room"<<m_pRoomMgr->GetRoomCount()<<std::endl;
}
/** 
 * @brief 机器人初始化
 * 
 */
bool MODI_BotUser::Init(const DWORD & account_id)
{
	m_dwAccountID = account_id;
	std::ostringstream os;
	
#ifndef _DEBUG
	if(m_dwAccountID < 10)
	{
		os << "starcs000" << account_id;
	}
	else if(m_dwAccountID > 9 && m_dwAccountID < 100)
	{
		os << "starcs00" << account_id;
	}
	else if(m_dwAccountID > 99 && m_dwAccountID < 1000)
	{
		os << "starcs0" << account_id;
	}
	else
	{
		os << "starcs" << account_id;
	}
	
#else	
	os<< "guest" << account_id;
#endif	
	
	// if( OpenMusicFile("music00.bin"))
// 	{
// 		printf("open music file ok \n ");
// 	}
	
	m_strAccountName = os.str();

	//m_pLobbyPlayerMgr = new MODI_LobbyPlayerMgr();
	//m_pPlayerMgr = new MODI_PlayerMgr();
	//m_pRoomMgr = new MODI_RoomMgr();

	if(! MODI_RClientTask::Init())
	{
		Global::logger->fatal("[%s] Unable Init clienttask", "SysInit");
		return false;
	}

	m_pUserTick = new MODI_UserTick(this, m_strAccountName.c_str());
	
	if(! m_pUserTick->Init())
	{
		Global::logger->fatal("[%s] Unable Init usertick", "SysInit");
		return false;
	}
		Global::logger->fatal(" Unable Init clienttask accountname=%s ", m_strAccountName.c_str());

	return true;
}

MODI_RTime	m_stTimes;
//#ifdef	_DEBUG

MODI_Timer	m_stOneminute(2000);
//#else
//MODI_Timer	m_stOneminute(60*1000);
//#endif
/** 
 * @brief 机器人更新
 * 
 */
void MODI_BotUser::UpDate()
{
	m_stTimes.GetNow();
	m_nTicks++;	
	Get(100);
	if(m_blIsRecvGatewayInfo && IsConnected())
	{
		m_blIsRecvGatewayInfo = false;
		MODI_C2LS_Request_Auth reqAuth;
		reqAuth.m_Passport.m_byRechangeChannel = false;
		reqAuth.m_Passport.m_nAccountID = m_dwAccountID;
		reqAuth.m_Passport.m_nCharID = m_CharId;
		MODI_LoginKey  LoginKey = m_LoginKey;
		std::string  AccountName = GetAccountName();
		int AccountNameLength = AccountName.length();
		char lowerAccountName[MAX_ACCOUNT_LEN];
		memset(lowerAccountName,'\0',MAX_ACCOUNT_LEN);
		bool result = MODI_GameHelpFun::StringToLower(AccountName.c_str(),AccountNameLength+1,lowerAccountName);
		if (result)
		{
			char Content[256];
			memset(Content,'0',sizeof(Content));
			memcpy( Content , LoginKey.m_szContnet, sizeof(LoginKey.m_szContnet));
			memcpy( Content + sizeof(LoginKey.m_szContnet),lowerAccountName, AccountNameLength);

			unsigned char md5_out[17];
			memset(md5_out, 0 , sizeof(md5_out));
			char final_out[33];
			memset(final_out, 0, sizeof(final_out));
	
			MD5Context check_str;
			MD5Init( &check_str );
			MD5Update( &check_str , (const unsigned char*)(Content) , sizeof(LoginKey.m_szContnet) + AccountNameLength);
			MD5Final(md5_out , &check_str );
			//Binary2String((const char *)md5_out, 16, final_out);
			//std::string recv_check_str = final_out;
			//transform(recv_check_str.begin(), recv_check_str.end(), recv_check_str.begin(), ::tolower);
	
			//MD5_CTX context = {0};
			//MD5Init (&context);
			//MD5Update (&context, (unsigned char*)Content, sizeof(LoginKey.m_szContnet) + AccountNameLength);
			//MD5Final (&context);
			//memcpy(reqAuth.m_Passport.m_szContent,(char*)recv_check_str.c_str(),sizeof(reqAuth.m_Passport.m_szContent));
			memcpy(reqAuth.m_Passport.m_szContent,(char*)md5_out,sizeof(reqAuth.m_Passport.m_szContent));
			SendCmd(&reqAuth,sizeof(MODI_C2LS_Request_Auth));
		}
	}
	MODI_TriggerEnter script(1);
	MODI_TriggerEnter  script2(2);
	MODI_TriggerEnter script3(3);
	MODI_TriggerEnter  script4(4);
	
	if( m_eState == enBotUserState_Login)
	{
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(this, script);
	}
	if(! m_stOneminute(m_stTimes))
	{
		return;
	}
	std::cout<<"one minute come on ...."<<std::endl;
	switch( m_eState )
	{
	
		case  enBotUserState_Lobby :
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(this,script2);
			break;
		case 	enBotUserState_Room:
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(this,script3);
			break;
		case	enBotUserState_Shop:
			MODI_ScriptManager<MODI_TriggerEnter>::GetInstance().Execute(this,script4);
			break;	
		default:
			break;
	}
}



/** 
 * @brief 发送命令
 * 
 * @param pt_null_cmd 要发送的命令
 * @param cmd_size 命令大小
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BotUser::SendCmd(const stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size == 0))
	{
		return false;
	}
	return MODI_RClientTask::SendCmdInConnect(pt_null_cmd, cmd_size);
}


/** 
 * @brief 命令处理
 * 
 * @param pt_null_cmd 要处理的命令
 * @param cmd_size 命令大小
 * 
 * @return 
 */
bool MODI_BotUser::CmdParse(const Cmd::stNullCmd * pt_null_cmd, const int cmd_size)
{
	if((pt_null_cmd == NULL) || (cmd_size < (int)(sizeof(Cmd::stNullCmd))))
    {
        return false;
    }
	this->Put( pt_null_cmd , cmd_size );
	return true;
}

/** 
 * @brief 队列内的命令
 * 
 * @param pt_null_cmd 获取的命令
 * @param dwCmdLen 命令长度
 * 
 * @return 成功true,失败false
 *
 */
bool MODI_BotUser::CmdParseQueue(const Cmd::stNullCmd * pt_null_cmd, const unsigned int dwCmdLen)
{
	if(pt_null_cmd == NULL || dwCmdLen < 2)
	{
		return false;
	}
	const Cmd::stNullCmd * pCmd = pt_null_cmd;
#ifdef _PACKGE_HANDLER	
	MODI_PackageHandler::GetInstance()->DoHandlePackage(pt_null_cmd->byCmd,pt_null_cmd->byParam,this,pt_null_cmd,dwCmdLen);

	return true;
#endif
	

	/// 收到角色信息
	if(pt_null_cmd->byCmd == MAINCMD_LOGIN && pt_null_cmd->byParam == MODI_LS2C_Notify_Charlist::ms_SubCmd)
	{
		const MODI_LS2C_Notify_Charlist * pCharList = (MODI_LS2C_Notify_Charlist * )pCmd;
		if(pCharList->m_nRoleCount > 0)
		{
			const MODI_CharInfo * pCharinfo = &pCharList->m_pChars[0];
			m_CharId = pCharinfo->m_nCharID;
		}
		else
		{
			ClientFinal();
		}
	}
	
	/// 机器人不切换频道，所有此为登陆发送的频道列表
	else if(pt_null_cmd->byCmd == MAINCMD_LOGIN && pt_null_cmd->byParam == MODI_LS2C_Notify_Channellist::ms_SubCmd)
	{
		MODI_C2LS_Request_SelChannel SelectChannel;
		strncpy(SelectChannel.m_szNetType,"dx", sizeof(SelectChannel.m_szNetType));
		SelectChannel.m_nChannelID = Global::g_wdChannelId;
		SendCmd(&SelectChannel, sizeof(SelectChannel));
	}

	/// login发送了网关信息
	else if(pt_null_cmd->byCmd == MAINCMD_LOGIN && pt_null_cmd->byParam == MODI_LS2C_Notify_GatewayInfo::ms_SubCmd)
	{
		const MODI_LS2C_Notify_GatewayInfo* pGwInfo = (MODI_LS2C_Notify_GatewayInfo*) pCmd;
		m_LoginKey = pGwInfo->m_Key;
		m_dwAccountID = pGwInfo->m_nAccountID;
		m_strGateIP = PublicFun::NumIP2StrIP(pGwInfo->m_nIP);
		m_wdGatePort = pGwInfo->m_nPort;
		m_blIsRecvGatewayInfo = true;
		
		/// 关闭login,重新连接gate
		SetServiceIP(m_strGateIP.c_str());
		SetServicePort(m_wdGatePort);
		ReadyReConnect();
	}


	/// 服务器返回操作结果
	else if(pt_null_cmd->byCmd == MAINCMD_OPTRESULT && pt_null_cmd->byParam == SUBCMD_OPTRESULT)
	{
		const MODI_GS2C_Respone_OptResult* pOptresult = (MODI_GS2C_Respone_OptResult*)pCmd;
		switch(pOptresult->m_nResult)
		{
			/// 登陆成功
			case SvrResult_Login_OK:
			{
				//修改状态为再大厅中
				m_eState = enBotUserState_Lobby;
				Global::logger->debug("[login_successful] <name=%s>", GetAccountName());
				char szPackageBuf[Skt::MAX_PACKETSIZE];
				memset( szPackageBuf , 0 , sizeof(szPackageBuf) );
				MODI_C2GS_Request_MusicHeaderData * pMusicHeadData = (MODI_C2GS_Request_MusicHeaderData *)(szPackageBuf);
				AutoConstruct( pMusicHeadData );
				pMusicHeadData->m_nMusicDataHeaderSize=100;
				int iSendSize = sizeof(MODI_C2GS_Request_MusicHeaderData) + pMusicHeadData->m_nMusicDataHeaderSize ;
				SendCmd(pMusicHeadData,iSendSize);
				
				/// 临时创建一个房间
			/*	MODI_C2GS_Request_CreateRoom req_createroom;
				req_createroom.m_roomType = RoomHelpFuns::MakeRoomType(false,ROOM_TYPE_NORMAL,ROOM_STYPE_NORMAL);
				req_createroom.m_MapID = INVAILD_CONFIGID;  //随机地图ID
				req_createroom.m_MusicID = INVAILD_CONFIGID; //随机音乐ID
				//strncpy(req_createroom.m_cstrPassword, roompsd.c_str());
				strncpy(req_createroom.m_cstrRoomName, GetAccountName(), sizeof(req_createroom.m_cstrRoomName) );
				req_createroom.m_bIsOffSpectator = false;
				SendCmd(&req_createroom,sizeof(MODI_C2GS_Request_CreateRoom));
				*/
			}
			///	创建房间成功
			break;
			
			default:
			break;
		}
	}
	else if( pt_null_cmd->byCmd == MAINCMD_SHOP  )
	{
		if(  pt_null_cmd->byParam == 2)
		{
			MODI_GS2C_Notify_EnterShopResult * res = (MODI_GS2C_Notify_EnterShopResult *)pt_null_cmd;
			///	如果进入商城成功
			if( res->m_result == kIO_Shop_Successful )
			{
				m_eState = enBotUserState_Shop;
			}	
			else if( res->m_result == kIO_Shop_UnknowError )
			{
			}
			else
			{
			}
		}
		else if( pt_null_cmd->byParam == 101)
		{
			MODI_GS2C_Notify_LeaveShopResult * res = (MODI_GS2C_Notify_LeaveShopResult *) pt_null_cmd;
			if( kIO_Shop_Successful == res->m_result)
			{
					///	离开商城后就是大厅
				m_eState  = enBotUserState_Lobby;
			}
			else
			{
			}
		}
		else
		{
		}
	}
	else if(pt_null_cmd->byCmd == MAINCMD_ROOM )
	{
		if( pt_null_cmd->byParam == MODI_GS2C_Notify_Roomlist::ms_SubCmd)
		{
			//RefreshRoomList(pt_null_cmd,dwCmdLen);
			m_eState = enBotUserState_Lobby;
			return true;
		}	
		else if( pt_null_cmd->byParam == MODI_GS2C_Notify_AddRoom::ms_SubCmd)
		{
			//AddRoom(pt_null_cmd,dwCmdLen);
			return true;
		}
		else if( pt_null_cmd->byParam == MODI_GS2C_Notify_DelRoom::ms_SubCmd)
		{
		//	DelRoom(pt_null_cmd,dwCmdLen);
			return true;
		}
		else if ( pt_null_cmd->byParam == MODI_C2GS_Response_CreateRoom::ms_SubCmd)
		{
			///创建房间成功
			m_eState = enBotUserState_Room;
			std::cout<<"I'm  now create room successful"<<std::endl;
			return true;
		}
		else if( pt_null_cmd->byParam == MODI_GS2C_Notify_JoinRoomSuccessful::ms_SubCmd ||
				pt_null_cmd->byParam == MODI_C2GS_Response_CreateRoom::ms_SubCmd)
		{
			m_eState = enBotUserState_Room;
			std::cout<<"I'm  now join room successful"<<std::endl;
		}
		else
		{
			return true;
		}
	}
	else
	{
		/// do nothing...
	}
	
    return true;
}


/** 
 * @brief 释放线程
 * 
 */
void MODI_BotUser::ClientFinal()
{
	m_pUserTick->TTerminate();
	m_pUserTick->Join();
	Terminate(true);
	TTerminate();
	Join();
}

