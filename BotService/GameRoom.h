/**
  	* @file GameRoom.h
	* @date 5:7:2010  13:42
	* @version Ver
	* @author calendar gongliang@modi.com
	* @brief 
	*
*/

#ifndef _GameRoom_h_2010_07_05_13_44
#define _GameRoom_h_2010_07_05_13_44

#include "Room.h"
//游戏房间
class MODI_GameRoom : public MODI_Room
{
public:
    ~MODI_GameRoom();

	bool StartGame();

     /**
     * @brief 发送每句的得分给服务器
     *
     * @return: void
     */
    virtual void SendOneSentenceResult(int sentence, unsigned int score,unsigned char type,
                                        int comboNum,float exact,float second);

     /**
     * @brief 发送每一次按键得分给服务器
     *
     * @return: void
     */
    virtual void SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever);

    /**
     * @brief 发送口型数据给服务器
     *
     * @return: void
     */
    virtual void SendMicInput(bool input, float time);

    /**
     * @brief 发送fevertime 给服务器
     *
     * @param:   float time
     * @return: void
     */
    virtual void SendFeverTime(float time);

    /**
     * @brief 发送fever 结束时间给服务器
     *
     * @param:   float time
     * @return: void
     */
    virtual void SendFeverTimeOver(float time);

    /**
     * @brief 通知服务器游戏结束
     *
     * @return: void
     */
    virtual void SendGameOver();

protected:
    MODI_GameRoom();

    MODI_GameRoom(MODI_RoomInfo* pRoomInfo);

    friend class MODI_RoomMgr;
};

///单人房间

class MODI_SingleRoom : public MODI_Room
{
public:
    ~MODI_SingleRoom();

	bool StartGame();

       /**
     * @brief 发送每句的得分给服务器
     *
     * @return: void
     */
    void SendOneSentenceResult(int sentence, unsigned int score,unsigned char type,
                               int comboNum,float exact,float second);

     /**
     * @brief 发送每一次按键得分给服务器
     *
     * @return: void
     */
    void SendOneKeyResult(unsigned int score,unsigned char type,int comboNum,float exact,float time, float ftotalFever);

    /**
     * @brief 通知服务器游戏结束
     *
     * @return: void
     */
    void SendGameOver();

	unsigned int GetPlayerNumInPlaying();

protected:
    MODI_SingleRoom();
    MODI_SingleRoom(MODI_RoomInfo* pRoomInfo);

    bool Init();

	void Reset();
    
    unsigned int m_totalScore;
    unsigned int m_SenctenceCount;
    float m_Exact;
    
#define SCORE_TYPE_LENGTH	10
    unsigned short m_Evaluation[SCORE_TYPE_LENGTH];

    friend class MODI_RoomMgr;
};


#endif
