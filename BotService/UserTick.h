/**
 * @file   UserTick.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 14:55:22 2011
 * 
 * @brief  机器人时钟线程
 * 
 * 
 */


#ifndef _MD_USERTICK_H
#define _MD_USERTICK_H

#include "Global.h"
#include "Thread.h"
#include "Timer.h"

class MODI_BotUser;

class MODI_UserTick: public MODI_Thread
{
public:
	MODI_UserTick(MODI_BotUser * bot_user, const char * name = "usertick"): MODI_Thread(name)
	{
		m_pBotUser = bot_user;
	}

	bool Init();
	void Run();
	void Final();

	MODI_BotUser * m_pBotUser;
	
 private:
	
	MODI_RTime m_stRTime;
	MODI_TTime m_stTTime;
};

#endif
