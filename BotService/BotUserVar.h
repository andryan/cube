/**
 * @file   BotUserVar.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 15:20:52 2011
 * 
 * @brief  机器人相关变量
 * 
 * 
 */


#ifndef _MD_BOTUSERVAR_H
#define _MD_BOTUSERVAR_H

class MODI_BotUser;

#include "UserVar.h"
#include "GlobalDB.h"


/**
 * @brief onlineclient用户变量
 * 
 */
class MODI_BotUserVar: public MODI_UserVar<MODI_BotUser>
{
 public:
	MODI_BotUserVar(const char * var_name, MODI_TableStruct * p_table = GlobalDB::m_pZoneUserVarTbl):
		MODI_UserVar<MODI_BotUser>(var_name,p_table)
	{
			
	}

		
	virtual ~MODI_BotUserVar(){}

	/** 
	 * @brief 变量保存到数据库
	 * 
	 */
	virtual void SaveToDB()
	{
		/// 机器人不保存数据
	}

	/** 
	 * @brief 从数据库中删除
	 * 
	 */
	virtual void DelFromDB()
	{
		/// 机器人不保存数据
	}

	/** 
	 * @brief 更新数据库中的变量
	 * 
	 */
	virtual void UpDateToDB()
	{
		/// 机器人不保存数据
	}
	
	/** 
	 * @brief  执行数据库sql语句
	 * 
	 * @param sql 
	 * @param sql_size 
	 */
	virtual void ExecSqlToDB(const char * sql, const WORD sql_size);

	/** 
	 * @brief  更新
	 * 
	 */
	virtual void UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime);
};

typedef MODI_UserVarManager<MODI_BotUserVar> MODI_BotUserVarMgr;

#endif
