/**
 * @file   Task.h
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 10:20:18 2011
 * 
 * @brief  任务
 * 
 * 
 */

#ifndef _MD_TASK_H
#define _MD_TASK_H

#include "Global.h"


/**
 * @brief 任务类
 * 
 */
class MODI_Task
{
 public:
	MODI_Task(WORD & task_id, const char * name, const char * des):
		m_wdTaskID(task_id), m_strName(name), m_strDescription(des){}

	~MODI_Task(){}

	/** 
	 * @brief 获取任务id
	 * 
	 * @return
	 *
	 */
	const WORD & GetTaskID()
	{
		return m_wdTaskID;
	}
	
 private:
	/// 任务id
	WORD m_wdTaskID;

	std::string m_strName;

	/// 任务描述
	std::string m_strDescription;
};

#endif
