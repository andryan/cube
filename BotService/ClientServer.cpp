/**
 * @file   ClientServer.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 11:58:04 2011
 * 
 * @brief  linux 下机器人 
 * 
 * 
 */

#include "Global.h"
#include "XMLParser.h"
#include "BotUser.h"
#include "BotGlobal.h"
#include "TaskManager.h"
#include "c2gs_PackageHandler.h"

/**
 * 主程序
 * 
 */
int main()
{
	/// 初始化日志
	Global::logger = new MODI_Logger("bot");
	Global::net_logger = Global::logger;
	std::string path = "./botlog/bot.log";
	Global::logger->AddLocalFileLog(path.c_str());

	/// 读取配置文件
	MODI_XMLParser xml;
	const char * file_name = "./Config/botconfig.xml";
	if(! xml.InitFile(file_name))
	{
		Global::logger->fatal("[system_init] unable load botconfig.xml");
		return 0;
	}
	xmlNodePtr p_root = xml.GetRootNode("botconfig");
	if(! p_root)
	{
		Global::logger->fatal("[system_init] unable get root");
		return 0;
	}
	xmlNodePtr p_server = xml.GetChildNode(p_root, "server");
	if(!p_server)
	{
		Global::logger->fatal("[system_init] unable get server node");
		return 0;
	}
	std::string server_ip;
	WORD server_port = 0;
	WORD client_num = 0;
	DWORD begin_account_id = 0;
	WORD channel_id = 0;

	xml.GetNodeStr(p_server, "ip", server_ip);
	xml.GetNodeNum(p_server, "port", &server_port, sizeof(server_port));
	xml.GetNodeNum(p_server, "clientnum", &client_num, sizeof(client_num));
	xml.GetNodeNum(p_server, "account", &begin_account_id, sizeof(begin_account_id));
	xml.GetNodeNum(p_server, "channel", &channel_id, sizeof(channel_id));

	Global::logger->debug("[config_info] <ip=%s,port=%u,clientnum=%u,account=%u,channel=%d>", server_ip.c_str(),
						  server_port, client_num, begin_account_id, channel_id);

	Global::g_wdChannelId = channel_id;
	Global::logger->RemoveConsoleLog();

	MODI_TaskManager::GetInstance().Init();

#ifdef _PACKAGE_HANDLER
	MODI_PackageHandler::GetInstance()->Initialization();
#endif
	
	/// 开始创建机器人线程
	for(WORD i=0; i<client_num; i++)
	{
		::usleep(1000);
		std::ostringstream os;
		os<< begin_account_id;
		MODI_BotUser * p_user = new MODI_BotUser(os.str().c_str(), server_ip.c_str(), server_port);
		if(! p_user->Init(begin_account_id))
		{
			Global::logger->fatal("[system_init] init bot user failed");
		}
		begin_account_id++;
	}


	while(1)
	{
		::usleep(1000);
	}

	return 0;
}


