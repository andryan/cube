/**
 * @file   Script.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Mar 18 12:23:44 2011
 * 
 * @brief  任务脚本
 * 
 * 
 */


#include "Global.h"
#include "XMLParser.h"
#include "Script.h"
#include "Process.h"
#include "BotUser.h"
#include "MakeManager.h"

/** 
 * @brief 增加一个过程
 * 
 * @param p_process 过程
 * 
 */
bool MODI_Script::AddProcess(MODI_Process * p_process)
{
	std::pair<defProcessMapIter, bool> ret_code;
	ret_code = m_stProcessMap.insert(defProcessMapValue(p_process->GetProcessID(), p_process));
	
#ifdef _HRX_DEBUG
	if(! ret_code.second)
	{
		Global::logger->fatal("[load_process] process failed <id=%u>", p_process->GetProcessID());
		MODI_ASSERT(0);
	}
#endif	
	return ret_code.second;
}


/** 
 * @brief 获取一个过程
 * 
 * @param process_id 过程id
 * 
 */
MODI_Process * MODI_Script::GetProcess(const WORD & process_id)
{
   	MODI_Process * p_process = NULL;
	defProcessMapIter iter = m_stProcessMap.find(process_id);
	if(iter != m_stProcessMap.end())
		p_process = iter->second;
	return p_process;
}


/** 
 * @brief 执行脚本
 * 
 * @param p_client 执行人
 * @param process_id 执行id =0则全部执行
 * 
 */
bool MODI_Script::Execute(MODI_BotUser * p_client, const WORD & process_id)
{
	if(! p_client)
	{
		MODI_ASSERT(0);
		return false;
	}
	
	/// 全部执行
	if(! process_id)
	{
		defProcessMapIter iter = m_stProcessMap.begin();
		for(; iter != m_stProcessMap.end(); iter++)
		{
			if(iter->second)
			{
				(iter->second)->Execute(p_client);
			}
			else
			{
				MODI_ASSERT(0);
			}
		}
	}
	/// 执行指定的process
	else
	{
		MODI_Process * p_process = GetProcess(process_id);
		if(p_process)
		{
			p_process->Execute(p_client);
		}
		else
		{
			Global::logger->fatal("[execute_process] Unable find process <scriptid=%u,processid=%u>", GetScriptID(), process_id);
			MODI_ASSERT(0);
		}
	}
	return true;
}


/** 
 * @brief 加载脚本
 * 
 */
bool MODI_Script::Load(const char * files)
{
	MODI_XMLParser xml;
	if(! xml.InitFile(files))
	{
		Global::logger->fatal("[load_script] Unable load file %s", files);
		MODI_ASSERT(0);
		return false;
	}

	xmlNodePtr p_root = xml.GetRootNode("script");
	if(!p_root)
	{
		Global::logger->fatal("[load_script] not find a root node scripts");
		MODI_ASSERT(0);
		return false;
	}

	WORD script_id = 0;
	if(! xml.GetNodeNum(p_root, "id", &script_id, sizeof(script_id)))
	{
		Global::logger->fatal("[load_script] Unable load script id");
		MODI_ASSERT(0);
		return false;
	}

	SetScriptID(script_id);
	xmlNodePtr p_process = xml.GetChildNode(p_root, "process");
	while(p_process)
	{
		WORD process_id = 0;
		if(! xml.GetNodeNum(p_process, "id", &process_id, sizeof(process_id)))
		{
			Global::logger->fatal("[load_script] Unable load process id");
			MODI_ASSERT(0);
			return false;
		}

		MODI_Process * p_new_process = new MODI_Process(process_id);
		
#ifdef _HRX_DEBUG
		Global::logger->debug("[new_process] new process <scriptid=%d,processid=%d>", script_id, process_id);
#endif		

		/// 条件
		xmlNodePtr p_conditions = xml.GetChildNode(p_process, "conditions");
		if(p_conditions)
		{
			xmlNodePtr condition_node = xml.GetChildNode(p_conditions, NULL);
			while(condition_node)
			{
				std::string condition_name = (char *)condition_node->name;
				if(condition_name == "comment")
				{
					condition_node = xml.GetNextNode(condition_node, NULL);
					continue;
				}
				
				MODI_Make<MODI_Condition> * p_make = MODI_MakeConditionManager::GetInstance().GetMake(condition_name.c_str());
				if(p_make)
				{
					MODI_Condition * new_con = p_make->Make();
					if(new_con)
					{
						std::string node_content;
						xml.GetNodeContent(condition_node, node_content, false);
						new_con->Init(node_content.c_str());
						p_new_process->AddCondition(new_con);
#ifdef _HRX_DEBUG
						Global::logger->debug("[new_condition] new condition <name=%s>", condition_name.c_str());
#endif						
					}
				}
				else
				{
					Global::logger->fatal("[load_condition] condition no reg <name=%s>", condition_name.c_str());
					MODI_ASSERT(0);
				}
				condition_node = xml.GetNextNode(condition_node, NULL);
			}
			
		}

		/// 动作
		xmlNodePtr p_actions = xml.GetChildNode(p_process, "actions");
		if(p_actions)
		{
			xmlNodePtr action_node = xml.GetChildNode(p_actions, NULL);
			while(action_node)
			{
				std::string action_name = (char *)action_node->name;
				if(action_name == "comment")
				{
					action_node = xml.GetNextNode(action_node, NULL);
					continue;
				}
				
				MODI_Make<MODI_Action> * p_make = MODI_MakeActionManager::GetInstance().GetMake(action_name.c_str());
				if(p_make)
				{
					MODI_Action * new_action = p_make->Make();
					if(new_action)
					{
						std::string node_content;
						xml.GetNodeContent(action_node, node_content, false);
						new_action->Init(node_content.c_str());
						p_new_process->AddAction(new_action);
#ifdef _HRX_DEBUG
						Global::logger->debug("[new_action] new action <name=%s>", action_name.c_str());
#endif						
					}
				}
				else
				{
					Global::logger->fatal("[load_action] action no reg <name=%s>", action_name.c_str());
					MODI_ASSERT(0);
				}
				action_node = xml.GetNextNode(action_node, NULL);
			}
			
		}
		
		AddProcess(p_new_process);
		/// 下一个过程
		p_process = xml.GetNextNode(p_process, "process");
	}
	return true;
}


/** 
 * @brief 卸载脚本
 * 
 */
void MODI_Script::Unload()
{
	MODI_Process * p_process = NULL;
	defProcessMapIter iter = m_stProcessMap.begin();
	for(; iter != m_stProcessMap.end(); iter++)
	{
		p_process = iter->second;
		if(p_process)
		{
			delete p_process;
		}
	}
	m_stProcessMap.clear();
}
