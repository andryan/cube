/**
 * @file   BotUserVar.cpp
 * @author  <hrx@localhost.localdomain>
 * @date   Fri Aug 12 15:22:15 2011
 * 
 * @brief  机器人变量
 * 
 * 
 */


#include "BotUserVar.h"
#include "BotUser.h"


/** 
 * @brief BotUser用户变量
 * 
 * @param sql 要执行的sql
 * @param sql_size sql的大小
 *
 */
void MODI_BotUserVar::ExecSqlToDB(const char * sql, const WORD sql_size)
{
	return;
}



/** 
 * @brief 更新
 * 
 */
void MODI_BotUserVar::UpDate(const MODI_RTime & cur_rtime, const MODI_TTime & cur_ttime)
{
	MODI_UserVar<MODI_BotUser>::UpDate(cur_rtime, cur_ttime);
}

