/**
 * @file RoomMgr.cpp
 * @date 2010-2-20 CST
 * @version Ver
 * @author calendar gongliang@modi.com
 * @brief 房间管理器类
 */
#include "RoomMgr.h"
#include "GameRoom.h"
////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------//
MODI_RoomMgr* MODI_RoomMgr::s_pRoomMgr = NULL;

//----------------------------------------------------------------------------//
MODI_RoomMgr* MODI_RoomMgr::GetInstance()
{
    if(NULL == s_pRoomMgr)
    {
        s_pRoomMgr = new MODI_RoomMgr();
    }

    return s_pRoomMgr;
}
//----------------------------------------------------------------------------//
void MODI_RoomMgr::Release()
{
    if (s_pRoomMgr)
    {
        delete s_pRoomMgr;
        s_pRoomMgr = NULL;
    }
}
//----------------------------------------------------------------------------//
MODI_RoomMgr::MODI_RoomMgr()
{
	for ( unsigned short i = 0; i<MAX_ROOM_COUNT; i++ )
	{
		m_iDanrenjinggeRooms[i] = MAX_ROOM_COUNT;
		m_iZuduijinggeRooms[i] = MAX_ROOM_COUNT;
		m_iDanrenjianpan2Rooms[i] = MAX_ROOM_COUNT;
	}
	m_iDanrenjinggeRoomsCnt = 0;
	m_iZuduijinggeRoomsCnt = 0;
	m_iDanrenjianpan2RoomsCnt = 0;
}
//----------------------------------------------------------------------------//
MODI_RoomMgr::~MODI_RoomMgr()
{
    ClearAllRoom();
}

////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------//
int MODI_RoomMgr::GetRoomCount(ROOM_SUB_TYPE roomType /*= ROOM_STYPE_END*/)
{
	if ( roomType == ROOM_STYPE_END )
	{
		return m_RoomVec.size();
	}
	else if ( roomType == ROOM_STYPE_NORMAL )
	{
		return m_iDanrenjinggeRoomsCnt;
	}
	else if ( roomType == ROOM_STYPE_TEAM )
	{
		return m_iZuduijinggeRoomsCnt;
	}
	else if ( roomType == ROOM_STYPE_KEYBOARD )
	{
		return m_iDanrenjianpan2RoomsCnt;
	}
	return 0;
}
//----------------------------------------------------------------------------//
MODI_Room* MODI_RoomMgr::GetRoomById(unsigned char roomid)
{
	defRoomVec::iterator itr = m_RoomVec.begin();
	defRoomVec::iterator _end = m_RoomVec.end();

	for(; itr!=_end; ++itr)
	{
		if( (*itr)->GetRoomId() == roomid )
		{
			return *itr;
		}
	}

    return NULL;
}
//----------------------------------------------------------------------------//
MODI_Room* MODI_RoomMgr::GetRoomByIdx(unsigned int index, ROOM_SUB_TYPE roomType /*= ROOM_STYPE_END*/ )
{
	unsigned int len = m_RoomVec.size();
	if( index >= len )
	{
		return NULL;
	}

	if ( roomType == ROOM_STYPE_END )
	{
 		return m_RoomVec.at(index);
	}
	else if ( roomType == ROOM_STYPE_NORMAL )
	{
		if ( m_iDanrenjinggeRooms[index] != MAX_ROOM_COUNT )
		{
			return GetRoomById(m_iDanrenjinggeRooms[index]);
		}
	}
	else if ( roomType == ROOM_STYPE_TEAM)
	{
		if ( m_iZuduijinggeRooms[index] != MAX_ROOM_COUNT )
		{
			return GetRoomById(m_iZuduijinggeRooms[index]);
		}
	}
	else if ( roomType ==  ROOM_STYPE_KEYBOARD )
	{
		if ( m_iDanrenjianpan2Rooms[index] != MAX_ROOM_COUNT )
		{
			return GetRoomById(m_iDanrenjianpan2Rooms[index]);
		}
	}
	else
	{
		return NULL;
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------//
bool MODI_RoomMgr::AddRoomByRoomInfo(MODI_RoomInfo* pRoominfo)
{
	if (pRoominfo)
	{
		//通知界面增加一个房间的页面
		return CreateRoom<MODI_GameRoom>(pRoominfo);
	}

	return false;
}
//----------------------------------------------------------------------------//
bool MODI_RoomMgr::DeleteRoom(unsigned char roomid)
{
	defRoomVec::iterator itr = m_RoomVec.begin();
	defRoomVec::iterator _end = m_RoomVec.end();
	for(; itr!=_end; ++itr)
	{
		if( (*itr)->GetRoomId() == roomid )
		{
			if ( (*itr)->GetRoomSubType() == ROOM_STYPE_NORMAL )
			{
				for ( int i=0; i<MAX_ROOM_COUNT; i++ )
				{
					if ( m_iDanrenjinggeRooms[i] == roomid )
					{
						m_iDanrenjinggeRooms[i] = MAX_ROOM_COUNT;
						m_iDanrenjinggeRoomsCnt--;
						break;
					}
				}
			}
			else if ( (*itr)->GetRoomSubType() == ROOM_STYPE_TEAM )
			{
				for ( int i=0; i<MAX_ROOM_COUNT; i++ )
				{
					if ( m_iZuduijinggeRooms[i] == roomid )
					{
						m_iZuduijinggeRooms[i] = MAX_ROOM_COUNT;
						m_iZuduijinggeRoomsCnt--;
						break;
					}
				}
			}
			else if ( (*itr)->GetRoomSubType() == ROOM_STYPE_KEYBOARD )
			{
				for ( int i=0; i<MAX_ROOM_COUNT; i++ )
				{
					if ( m_iDanrenjianpan2Rooms[i] == roomid )
					{
						m_iDanrenjianpan2Rooms[i] = MAX_ROOM_COUNT;
						m_iDanrenjianpan2RoomsCnt--;
						break;
					}
				}
			}
			delete *itr;
			m_RoomVec.erase(itr);
			return true;
		}
	}

	return false;
}
//----------------------------------------------------------------------------//
void MODI_RoomMgr::UpdateRoomInfoList(MODI_GS2C_Notify_Roomlist* pRoomList)
{
    ClearAllRoom();
    // 通知界面刷新房间列表
    int roomcount = pRoomList->m_nCount;
    for (int i=0;i<roomcount;++i)
        AddRoomByRoomInfo(&pRoomList->m_pRooms[i]);
}
//----------------------------------------------------------------------------//
void MODI_RoomMgr::UpdateRoomInfo(unsigned char size,char* updateinfos)
{
	MODI_Room* pRoom = NULL;
	// Header
	char room_name[ROOM_NAME_MAX_LEN + 1];
	defRoomID room_id = INVAILD_ROOM_ID;
	// Body
	defMusicID music_id = RANDOM_MUSIC_ID;
	defMapID map_id = RANDOM_MAP_ID;
	defRoomType room_type = INVAILD_ROOM_TYPE;
	defRoomStateMask state_mask = 0;
	bool room_isplaying = false;

	defRoomUpdateMask mask = 0;

	// 获取信息
    for (int i=0;i<size;)
    {
		// 消息结构头信息
		memcpy( &room_id, updateinfos + i, sizeof(defRoomID) );
		i += sizeof(defRoomID);
		memcpy( &mask, updateinfos + i, sizeof(defRoomUpdateMask) );
		i += sizeof(defRoomUpdateMask);

		// 消息结构数据
		if( mask & enRoomUP_Name ) // 房间名称
		{
			memcpy( room_name, updateinfos + i, sizeof(room_name));
			i += sizeof(room_name);
		}
		if( mask & enRoomUP_MusicID ) // 音乐 ID
		{
			memcpy( &music_id, updateinfos + i, sizeof(defMusicID) );
			i += sizeof(defMusicID);			
		}
		if( mask & enRoomUP_MapID ) // 地图 ID
		{
			memcpy( &map_id, updateinfos + i, sizeof(defMapID) );
			i += sizeof(defMapID);
		}
		if( mask & enRoomUP_PosState ) // 位置状态
		{
			memcpy( &state_mask, updateinfos + i, sizeof(defRoomStateMask) );
			i += sizeof(defRoomStateMask);
		}
		if( mask & enRoomUP_RoomType ) // 房间类型
		{
			memcpy( &room_type, updateinfos + i, sizeof(defRoomType) );
			i += sizeof(defRoomType);
		}
		if ( mask & enRoomUP_IsPlaying ) // 游戏状态
		{
			memcpy( &room_isplaying, updateinfos + i, sizeof(bool) );
			i += sizeof(bool);
		}

		// 修改房间信息
		pRoom = GetRoomById((unsigned char)room_id);
		if( !pRoom ) { continue; }

		if( mask & enRoomUP_Name )
		{
			memcpy( pRoom->m_RoomInfo.m_cstrRoomName, room_name, sizeof(room_name) );
		}
		if( mask & enRoomUP_MusicID )
		{
			pRoom->m_RoomInfo.m_detailNormalRoom.m_nMusicId = music_id;
		}
		if( mask & enRoomUP_MapID )
		{
			pRoom->m_RoomInfo.m_detailNormalRoom.m_nMapId = map_id;
		}
		if( mask & enRoomUP_PosState )
		{
			pRoom->m_RoomInfo.m_detailNormalRoom.m_nStateMask = state_mask;
		}
		if( mask & enRoomUP_RoomType )
		{
			defRoomType oldDefRoomType = pRoom->m_RoomInfo.m_roomType;
			ROOM_SUB_TYPE oldRoomType = pRoom->GetRoomSubType();		
			pRoom->m_RoomInfo.m_roomType = room_type;

			//加一个房间
			if (oldDefRoomType != room_type && oldDefRoomType != INVAILD_ROOM_TYPE)
			{	
				if ( pRoom->GetRoomSubType() == ROOM_STYPE_NORMAL )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iDanrenjinggeRooms[i] == MAX_ROOM_COUNT )
						{
							m_iDanrenjinggeRooms[i] = pRoom->GetRoomId();
							m_iDanrenjinggeRoomsCnt++;
							break;
						}
					}
				}
				else if ( pRoom->GetRoomSubType() == ROOM_STYPE_TEAM )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iZuduijinggeRooms[i] == MAX_ROOM_COUNT )
						{
							m_iZuduijinggeRooms[i] = pRoom->GetRoomId();
							m_iZuduijinggeRoomsCnt++;
							break;
						}
					}
				}
				else if ( pRoom->GetRoomSubType() == ROOM_STYPE_KEYBOARD )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iDanrenjianpan2Rooms[i] == MAX_ROOM_COUNT )
						{
							m_iDanrenjianpan2Rooms[i] = pRoom->GetRoomId();
							m_iDanrenjianpan2RoomsCnt++;
							break;
						}
					}
				}


				//减一个房间
				if ( oldRoomType == ROOM_STYPE_NORMAL )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iDanrenjinggeRooms[i] == room_id )
						{
							m_iDanrenjinggeRooms[i] = MAX_ROOM_COUNT;
							m_iDanrenjinggeRoomsCnt--;
							break;
						}
					}
				}
				else if ( oldRoomType == ROOM_STYPE_TEAM )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iZuduijinggeRooms[i] == room_id )
						{
							m_iZuduijinggeRooms[i] = MAX_ROOM_COUNT;
							m_iZuduijinggeRoomsCnt--;
							break;
						}
					}
				}
				else if ( oldRoomType == ROOM_STYPE_KEYBOARD )
				{
					for ( int i=0; i<MAX_ROOM_COUNT; i++ )
					{
						if ( m_iDanrenjianpan2Rooms[i] == room_id )
						{
							m_iDanrenjianpan2Rooms[i] = MAX_ROOM_COUNT;
							m_iDanrenjianpan2RoomsCnt--;
							break;
						}
					}
				}



			}
		}
		if ( mask & enRoomUP_IsPlaying )
		{
			pRoom->m_RoomInfo.m_bPlaying = room_isplaying;
		}
    }
}

////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------//
bool MODI_RoomMgr::AddRoom(MODI_Room* pRoom)
{
	defRoomVec::iterator res = find(m_RoomVec.begin(),m_RoomVec.end(),pRoom);
	if( res == m_RoomVec.end() )
	{
 		m_RoomVec.push_back(pRoom);

		if ( pRoom->GetRoomSubType() == ROOM_STYPE_NORMAL )
		{
			for ( int i=0; i<MAX_ROOM_COUNT; i++ )
			{
				if ( m_iDanrenjinggeRooms[i] == MAX_ROOM_COUNT )
				{
					m_iDanrenjinggeRooms[i] = pRoom->GetRoomId();
					m_iDanrenjinggeRoomsCnt++;
					return true;
				}
			}
		}
		else if ( pRoom->GetRoomSubType() == ROOM_STYPE_TEAM )
		{
			for ( int i=0; i<MAX_ROOM_COUNT; i++ )
			{
				if ( m_iZuduijinggeRooms[i] == MAX_ROOM_COUNT )
				{
					m_iZuduijinggeRooms[i] = pRoom->GetRoomId();
					m_iZuduijinggeRoomsCnt++;
					return true;
				}
			}
		}
		else if ( pRoom->GetRoomSubType() == ROOM_STYPE_KEYBOARD )
		{
			for ( int i=0; i<MAX_ROOM_COUNT; i++ )
			{
				if ( m_iDanrenjianpan2Rooms[i] == MAX_ROOM_COUNT )
				{
					m_iDanrenjianpan2Rooms[i] = pRoom->GetRoomId();
					m_iDanrenjianpan2RoomsCnt++;
					return true;
				}
			}
		}

		return true;
	}
    return false;
}
//----------------------------------------------------------------------------//
void MODI_RoomMgr::ClearAllRoom()
{
	for ( unsigned short i = 0; i<MAX_ROOM_COUNT; i++ )
	{
		m_iDanrenjinggeRooms[i] = MAX_ROOM_COUNT;
		m_iZuduijinggeRooms[i] = MAX_ROOM_COUNT;
		m_iDanrenjianpan2Rooms[i] = MAX_ROOM_COUNT;
	}
	m_iDanrenjinggeRoomsCnt = 0;
	m_iZuduijinggeRoomsCnt = 0;
	m_iDanrenjianpan2RoomsCnt = 0;

	defRoomVec::iterator itr = m_RoomVec.begin();
	defRoomVec::iterator _end = m_RoomVec.end();
    for (;itr != _end;++itr)
    {
        delete *itr;
    }

    m_RoomVec.clear();
}
